<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="VariationGroupEdit.aspx.cs"
	Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Variations.VariationGroupEdit" %>

<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="uc" TagName="VariationTypeList" Src="~/UserControls/Assortment/VariationTypeList.ascx" %>
<%@ Register TagPrefix="uc" TagName="VariantsList" Src="~/UserControls/Assortment/VariantsList.ascx" %>
<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<asp:Content ID="AdvancedSearchTab" ContentPlaceHolderID="body" runat="server">

	<script type="text/javascript">
		function confirmProductDelete() {
			return confirm("<%= Resources.ProductMessage.ProductVariationsConfirmDelete %>");
		}
	</script>

	<link href="<%=ResolveUrl("~/Media/Css/product-tabs.css") %>" rel="stylesheet" type="text/css" />
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div id="variation-group-create">
			<Scensum:ToolBoxPanel ID="VariationPanelToolBoxPanel" runat="server" Text="Variations"
				ShowSeparator="true">
				<div class="item" style="width: 100px; color: #CBCBCB; float: left; padding-bottom: 8px;
					padding-top: 10px; padding-left: 15px;">
					<%= Resources.General.Literal_Create %></div>
				<div style="float: left; padding-bottom: 8px; padding-top: 8px; font-size: 0.7em;
					font-weight: normal;">
					<uc:ImageLinkButton ID="CreateVariationGroupButton" runat="server" Text="<%$ REsources:Product, Button_VariationGroup %>"
						ImageUrl="~/Media/Images/Assortment/variation.gif" SkinID="HeaderPanelButton"
						UseSubmitBehaviour="false" />
				</div>
			</Scensum:ToolBoxPanel>
			<div style="float: left; width:984px; margin-bottom:-10px; ">
    		<asp:UpdatePanel ID="ErrorsUpdatePanel" runat="server" UpdateMode="Always">
					<ContentTemplate>						
						<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton"
							runat="server" />
						<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
							ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgVariationGroup" />
					</ContentTemplate>
				</asp:UpdatePanel>
		</div>
			<div id="variation-block" class="main">
			    <asp:UpdatePanel ID="CaptionUpdatePanel" runat="server" UpdateMode="Conditional">
			        <Triggers>
			            <asp:AsyncPostBackTrigger ControlID="SaveButton" EventName="Click" />
			        </Triggers>
			        <ContentTemplate>
						<label class="variation-header">
						<uc:LiteralEncoded ID="CaptionLabel" runat="server" />
						</label>
		                <br />
				    </ContentTemplate>
				</asp:UpdatePanel>
				<div class="first-column">
					<div class="input-box">
						<span>
							<%= Resources.General.Literal_Title %>&nbsp; *</span>&nbsp
						<asp:RequiredFieldValidator runat="server" ID="VariationGroupTitleValidator" ControlToValidate="VariationGroupTitle"
							ErrorMessage="<%$ Resources:GeneralMessage, TitleEmpty %>" Display="None" ValidationGroup="vgVariationGroup" />
						<br />
						<asp:TextBox ID="VariationGroupTitle" runat="server" CssClass="variation-field" MaxLength="256" />
					</div>
				</div>
				<div class="column">
					<div class="input-box">
						<span>
							<%= Resources.General.Literal_Status %></span>
						<br />
						<asp:DropDownList ID="StatusList" DataTextField="Title" CssClass="variation-field"
							DataValueField="Id" runat="server">
						</asp:DropDownList>
					</div>
				</div>
			
				<asp:UpdatePanel ID="VariationTabsUpdatePanel" runat="server" UpdateMode="Conditional"
					ChildrenAsTriggers="true">
					<ContentTemplate>
						<div id="TabsVariationDiv" runat="server" class="main-tabs">
							<ul runat="server" class="ui-tabs-nav">
								<li runat="server" id="CriteriaLi" class="ui-tabs-selected">
									<asp:LinkButton ID="CriteriaTabButton" runat="server" Text="<%$Resources:Product, Button_Criteria %>" />
								</li>
								<li runat="server" id="VariationLi">
									<asp:LinkButton ID="VariantsTabButton" runat="server" Text="<%$Resources:Product, Button_Variants %>" />
								</li>
							</ul>
							<div id="CriteriaDiv" class="variation-group-criteria tab ui-tabs-panel ui-tabs-panel-variations"
								runat="server" visible="true">
								<uc:VariationTypeList ID="VariationTypeListControl" runat="server" ValidationGroup="vgVariationGroup" />
							</div>
							<div id="VariantsDiv" class="variation-group-variants tab ui-tabs-panel ui-tabs-panel-variations"
								runat="server" visible="false">
								<uc:VariantsList runat="server" ID="VariantsListControl" />
							</div>
						</div>
					</ContentTemplate>
					<Triggers>
						<asp:AsyncPostBackTrigger ControlID="CriteriaTabButton" EventName="Click" />
						<asp:AsyncPostBackTrigger ControlID="VariantsTabButton" EventName="Click" />
					</Triggers>
				</asp:UpdatePanel>
				<asp:UpdatePanel ID="VariationGroupUpdatePanel" runat="server" UpdateMode="Conditional">
				    <ContentTemplate>
					<div class="buttons left">
					<uc:ImageLinkButton UseSubmitBehaviour="true" ID="DeleteButton" runat="server" Text="<%$ Resources:General,Button_Delete %>"  SkinID="DefaultButton" OnClientClick="javascript: return DeleteConfirmation('variation group');" />
				</div>
						<div id="variant-action-buttons" class="buttons right">
							<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="SaveButton"
								runat="server" Text="<%$ Resources:General, Button_Save %>" ValidationGroup="vgVariationGroup" />
							<uc:ImageLinkButton SkinID="DefaultButton" UseSubmitBehaviour="true" ID="CancelButton"
								runat="server" Text="<%$ Resources:General, Button_Cancel %>" />
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
			</div>
		</div>
	</asp:Panel>
</asp:Content>
