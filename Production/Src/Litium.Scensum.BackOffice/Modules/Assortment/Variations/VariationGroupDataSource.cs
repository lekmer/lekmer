using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Scensum.BackOffice.Modules.Assortment.Variations
{
	public class VariationGroupDataSource
	{
		private int _rowCount;

		public int RelationListId { get; set; }

		public int SelectCount()
		{
			return _rowCount;
		}
		public Collection<IVariationGroup> SelectMethod(int maximumRows, int startRowIndex)
		{
			var variationGroupSecureService = IoC.Resolve<IVariationGroupSecureService>();
			var groups = variationGroupSecureService.GetAll(startRowIndex / maximumRows + 1, maximumRows);
			_rowCount = groups.TotalCount;
			return groups;
		}
	}
}
