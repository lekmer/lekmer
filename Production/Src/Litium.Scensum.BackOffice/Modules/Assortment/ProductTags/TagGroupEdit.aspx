﻿<%@ Page Language="C#" MasterPageFile="~/Master/Main.Master" CodeBehind="TagGroupEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Assortment.Tags.TagGroupEdit" %>

<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2" Assembly="Litium.Scensum.BackOffice" %>

<asp:Content ID="TagGroupEditTab" ContentPlaceHolderID="body" runat="server">
	<sc:ToolBoxPanel ID="TagGroupToolBoxPanel" runat="server" Text="Tag Group" />

	<div class="stores-place-holder">
		<div class="main-caption">
			<uc:LiteralEncoded ID="TagGroupHeader" runat="server" />
		</div>
	
		<span class="bold"><%=Resources.Lekmer.Tab_ProductEdit_Tags%></span>

		<sc:GridViewWithCustomPager
			ID="TagsGrid"
			SkinID="grid"
			AutoGenerateColumns="false"
			Width="100%"
			AllowPaging="true"
			PageSize="<%$AppSettings:DefaultGridPageSize%>"
			PagerSettings-Mode="NumericFirstLast"
			runat="server">
			<Columns>
				<asp:TemplateField HeaderText="<%$ Resources:General,Literal_Title %>" ItemStyle-Width="40%">
					<ItemTemplate>
						<asp:HiddenField ID="IdHiddenField" Value='<%#Eval("Id") %>' runat="server" />
						<uc:HyperLinkEncoded ID="TagLink" runat="server" Text='<%# Eval("Value")%>' NavigateUrl='<%# GetEditUrl(Eval("Id")) %>' />
					</ItemTemplate>
				</asp:TemplateField>
				<asp:BoundField HeaderText="<%$ Resources:General,Literal_CommonName %>" DataField="CommonName" ItemStyle-Width="30%" />
				<asp:TemplateField HeaderText="<%$ Resources:Campaign,Literal_Flag %>" ItemStyle-Width="30%">
					<ItemTemplate>
						<asp:Label id="FlagLabel" runat="server" />
					</ItemTemplate>
				</asp:TemplateField>
			</Columns>
		</sc:GridViewWithCustomPager>

		<br class="clear">
		<div class="buttons right">
			<uc:ImageLinkButton ID="CancelButton" runat="server" Text="<%$ Resources:General,Button_Cancel %>" SkinID="DefaultButton" />
		</div>
	</div>
</asp:Content>