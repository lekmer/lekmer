﻿using System;
using System.Web.UI;
using Litium.Scensum.BackOffice.Base;

namespace Litium.Scensum.BackOffice.Modules.Media.Icons
{
	public partial class IconsMaster : MasterPage
	{
		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			CreateIconButton.Click += CreatePackageClick;
		}

		protected virtual void CreatePackageClick(object sender, EventArgs e)
		{
			Response.Redirect(LekmerPathHelper.Icon.GetEditUrl());
		}

		public virtual void SetupTabAndPanel()
		{
			if (Master != null)
			{
				var topMaster = (Master.Start)(Master).Master;
				if (topMaster != null)
				{
					topMaster.SetActiveTab("Media", "Icons");
				}
			}
		}
	}
}