﻿using System;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls.Media.Events;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Scensum.BackOffice.Modules.Media.Icons
{
	public partial class IconMedia : UserControlController
	{
		public event EventHandler<ImageSelectEventArgs> Selected;
		public event EventHandler FileUploaded;
		public event EventHandler DeleteImage;

		public int? ImageId { get; set; }

		protected override void SetEventHandlers()
		{
			ImageSelectControl.Selected += ImageSelected;
			ImageSelectControl.FileUploaded += ImageUnloaded;
			ImageSelectControl.Canceled += ImageSelectCanceled;
			DeleteImageButton.Click += DeleteImageButtonClick;
		}

		protected override void PopulateControl()
		{
			PopulateImage(ImageId);
		}

		protected void PopulateImage(IImage image)
		{
			DefaultImage.Visible = true;
			DefaultImage.ImageUrl = ResolveUrl(PathHelper.Media.GetImageMainLoaderUrl(image.Id, image.FormatExtension));
		}

		protected void PopulateImage(int? imageId)
		{
			DefaultImage.Visible = false;

			if (imageId.HasValue)
			{
				var imageService = IoC.Resolve<IImageSecureService>();
				IImage image = imageService.GetById((int)imageId);
				PopulateImage(image);
			}
		}

		private void ImageSelectCanceled(object sender, EventArgs e)
		{
			ImagesPopup.Hide();
		}

		private void ImageUnloaded(object sender, EventArgs e)
		{
			ImagesPopup.X = 227;
			ImagesPopup.Show();
			if (FileUploaded != null)
			{
				FileUploaded(this, EventArgs.Empty);
			}
		}

		private void ImageSelected(object sender, ImageSelectEventArgs e)
		{
			ImagesPopup.Hide();
			PopulateImage(e.Image);

			if (Selected != null)
			{
				Selected(this, e);
			}
		}

		protected virtual void DeleteImageButtonClick(object sender, EventArgs e)
		{
			PopulateImage((int?) null);

			if (DeleteImage != null)
			{
				DeleteImage(this, EventArgs.Empty);
			}
		}
	}
}