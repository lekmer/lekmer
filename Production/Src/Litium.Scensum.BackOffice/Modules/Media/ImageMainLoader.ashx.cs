using System.IO;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using MediaBackOfficeSetting = Litium.Scensum.BackOffice.Setting.MediaSetting;

namespace Litium.Scensum.BackOffice.Modules.Media
{
	public class ImageMainLoader : ImageLoaderResizableBase
	{
		protected override int Width
		{
			get { return MediaBackOfficeSetting.Instance.NormalWidth; }
		}

		protected override int Height
		{
			get { return MediaBackOfficeSetting.Instance.NormalHeight; }
		}

		protected override int Quality
		{
			get { return MediaBackOfficeSetting.Instance.NormalQuality; }
		}

		protected override Stream GetMedia(string mediaId, string extension, out string mime)
		{
			if (string.IsNullOrEmpty(mediaId))
			{
				return IoC.Resolve<IImageSecureService>().LoadDummyImage(out mime);
			}

			int id;
			if (!int.TryParse(mediaId, out id))
			{
				mime = string.Empty;
				return null;
			}
			var service = IoC.Resolve<IImageSecureService>();
			return IsImage(extension) 
				? service.LoadImage(id, extension, Width, Height, Quality, out mime)
				: service.LoadMediaTypeSpecificImage(extension, out mime);
		}
	}
}