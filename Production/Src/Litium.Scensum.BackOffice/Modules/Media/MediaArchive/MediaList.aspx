<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile ="~/Modules/Media/MediaArchive/Media.Master"  CodeBehind="MediaList.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Media.MediaArchive.MediaList" %>
<%@ Register Src="../../../UserControls/Media/MediaList.ascx" TagName="MediaList" TagPrefix="uc7" %>
<asp:Content ID="MediaTabContent" ContentPlaceHolderID="MediaContent"  runat="server">	       
				    <div id="media-display">	
					    <uc7:MediaList ID="MediaListControl" DisplayMode="List" runat="server"/>
				    </div>
</asp:Content>
