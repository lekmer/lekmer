<%@ Page Title="" Language="C#" MasterPageFile="~/Modules/Media/MediaArchive/Media.Master" AutoEventWireup="false" CodeBehind="Upload.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Media.MediaArchive.Upload" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register Assembly="Litium.Scensum.Web.Controls" Namespace="Litium.Scensum.Web.Controls.Tree"
    TagPrefix="CustomControls" %>
<%@ Register Src="~/UserControls/Tree/NodeSelector.ascx" TagName="NodeSelect" TagPrefix="uc" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="MessageContainer" runat="server">	
		<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="UploadButton" runat="server" />
</asp:Content>
<asp:Content ID="UploadContent" ContentPlaceHolderID="MediaContent" runat="server">
	<asp:Panel ID="UploadPanel" runat="server">
		<br />
		<div id="upload-folder-select" class="upload-folder-select">
			<span><asp:Literal runat="server" Text="<%$ Resources:General, Literal_PlaceInFolder%>" /> *</span><br />
			<span class="media-item-information-value">
				<uc:NodeSelect ID="NodeSelectorControl" runat="server" UseRootNodeInPath="true" />
			</span>
		</div>
		<br /><br />
		<div class="column">
			<span><asp:Literal runat="server" Text="<%$ Resources:Media, Literal_Path%>" /> *</span>
			<br />
			<asp:FileUpload ID="FileUploadControl" runat="server" ContentEditable="false" size="62" CssClass="media-file-upload"/>
			<br />
			<div id="media-upload-zip">
				<asp:CheckBox ID="ZipCheckBox" runat="server" />
				<span><asp:Literal runat="server" Text="<%$ Resources:Media, Literal_ResolveZipArchive%>" /></span>
			</div>
			<div id="media-upload-files">
				<uc:ImageLinkButton ID="UploadButton" runat="server" Text="<%$ Resources:Media, Button_Upload%>" SkinID="DefaultButton" />
			</div>
		</div>
		<div class="column">
			<br />
			<asp:Label ID="InfoLabel" runat="server" Text="<%$ Resources:Media, Label_CanUploadSingleFilesInZipArchive%>" CssClass="media-info"></asp:Label>
		</div>
	</asp:Panel>
</asp:Content>
