using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;
using Litium.Scensum.Web.Controls.Tree.TemplatedTree;

namespace Litium.Scensum.BackOffice.Modules.Customers.CustomerGroups
{
	public partial class CustomerGroupsMaster : MasterPage
	{
		private const string _selectedNode = "SelectedCustomerGroupFolderId";
		private const string _breadcrumbsSeparator = " > ";
        
		private Collection<string> _breadcrumbAppend;

		public virtual Collection<string> BreadcrumbAppend
		{
			get
			{
				if (_breadcrumbAppend == null)
				{
					_breadcrumbAppend = new Collection<string>();
				}
				return _breadcrumbAppend;
			}
		}

		public virtual int? SelectedFolderId
		{
			get
			{
				return (int?)Session[_selectedNode];
			}
			set
			{
				Session[_selectedNode] = value;
			}
		}

		public virtual bool DenySelection
		{
			get
			{
				return CustomerGroupsTree.DenySelection;
			}
			set
			{
				CustomerGroupsTree.DenySelection = value;
			}
		}

		public virtual int RootNodeId
		{
			get
			{
				return CustomerGroupsTree.RootNodeId;
			}
		}

		public virtual string RootNodeTitle
		{
			get
			{
				return CustomerGroupsTree.RootNodeTitle;
			}
		}

		protected override void OnInit(EventArgs e)
		{
			base.OnInit(e);
			SearchButton.Click += OnSearch;
			CreateButton.Click += OnCreate;
			CustomerGroupsTree.NodeCommand += OnNodeCommand;
		}

		protected override void OnPreRender(EventArgs e)
		{
			base.OnPreRender(e);

			if (SelectedFolderId == null)
			{
				SelectedFolderId = RootNodeId;
			}

			if (!IsPostBack)
			{
				PopulateTree(SelectedFolderId);
				RestoreSearchFields();
				BuildBreadcrumbs();
			}

			if (CustomerGroupsTree.SelectedNodeId != SelectedFolderId)
			{
				DenySelection = true;
			}

            ScriptManager.RegisterStartupScript(LeftUpdatePanel, LeftUpdatePanel.GetType(), "root menu", string.Format(CultureInfo.CurrentCulture, "PrepareRootMenu('{0}'); HideRootExpander('{0}');", CustomerGroupsTree.ClientID), true);
		}

		protected virtual void OnNodeCommand(object sender, TreeViewEventArgs e)
		{
			PopulateTree(e.Id);
			switch (e.EventName)
			{
				case "Expand":
					DenySelection = true;
					break;
				case "Navigate":
					SelectedFolderId = e.Id;
					Response.Redirect(PathHelper.Customer.CustomerGroup.GetDefaultUrl());
					break;
			}
		}

		protected virtual void OnCreate(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			Response.Redirect(IsSearchResult
				? PathHelper.Customer.CustomerGroup.GetCreateUrlForSearch(RootNodeId)
				: PathHelper.Customer.CustomerGroup.GetCreateUrlForDefault(RootNodeId));
		}

		protected virtual void OnSearch(object sender, EventArgs e)
		{
			SelectedFolderId = null;
			SearchCriteriaState<string>.Instance.Save(PathHelper.Customer.CustomerGroup.GetSearchResultUrl(), SearchTextBox.Text);
			Response.Redirect(PathHelper.Customer.CustomerGroup.GetSearchResultUrl());
		}

		protected virtual void LbtnCreateGroup_Click(object sender, EventArgs e)
		{
			SelectedFolderId = CustomerGroupsTree.MenuLastClickedNodeId;
			Response.Redirect(IsSearchResult
				? PathHelper.Customer.CustomerGroup.GetCreateUrlForSearch(SelectedFolderId.Value)
				: PathHelper.Customer.CustomerGroup.GetCreateUrlForDefault(SelectedFolderId.Value));
		}

		protected virtual void LbtnCreateFolder_Click(object sender, EventArgs e)
		{
			SelectedFolderId = CustomerGroupsTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Customer.CustomerGroup.GetFolderCreateUrl(SelectedFolderId.Value));
		}

		protected virtual void LbtnEditFolder_Click(object sender, EventArgs e)
		{
			SelectedFolderId = CustomerGroupsTree.MenuLastClickedNodeId;
			Response.Redirect(PathHelper.Customer.CustomerGroup.GetFolderEditUrl(SelectedFolderId.Value));
		}

		protected virtual void LbtnDelete_Click(object sender, EventArgs e)
		{
			var service = IoC.Resolve<ICustomerGroupFolderSecureService>();
			int? parentId = service.GetById(CustomerGroupsTree.MenuLastClickedNodeId.Value).ParentId;
			service.Delete(SignInHelper.SignedInSystemUser, CustomerGroupsTree.MenuLastClickedNodeId.Value);
			SystemMessageContainer.MessageType = InfoType.Success;
			SystemMessageContainer.Add(Resources.GeneralMessage.DeleteSeccessful);
			SelectedFolderId = parentId;
			PopulateTree(null);
			Response.Redirect(PathHelper.Customer.CustomerGroup.GetDefaultUrl());
		}
		
		public virtual void PopulateTree(int? folderId)
		{
			CustomerGroupsTree.DataSource = GetNodes(folderId);
			CustomerGroupsTree.RootNodeTitle = Resources.Customer.Label_CustomerGroups;
			CustomerGroupsTree.DataBind();
			LeftUpdatePanel.Update();
			CustomerGroupsTree.SelectedNodeId = folderId ?? SelectedFolderId;
		}

		public virtual Collection<INode> GetNodes(int? folderId)
		{
			var definedId = folderId ?? SelectedFolderId;
			var actualId = definedId != RootNodeId ? definedId : null;
			return IoC.Resolve<ICustomerGroupFolderSecureService>().GetTree(actualId);
		}

		public virtual void UpdateSelection(int nodeId)
		{
			CustomerGroupsTree.SelectedNodeId = SelectedFolderId = nodeId;
			LeftUpdatePanel.Update();
		}

		protected virtual void RestoreSearchFields()
		{
			string searchName = SearchCriteriaState<string>.Instance.Get(PathHelper.Customer.CustomerGroup.GetSearchResultUrl());
			if (!string.IsNullOrEmpty(searchName))
			{
				SearchTextBox.Text = searchName;
			}
		}

		protected virtual void BuildBreadcrumbs()
		{
			if (IsSearchResult)
			{
				Breadcrumbs.Text = Resources.Customer.Literal_SearchResult;
				return;
			}
			
			string breadcrumbs = string.Empty;
			
			if (SelectedFolderId.HasValue && SelectedFolderId.Value != RootNodeId)
			{
				Collection<ICustomerGroupFolder> folders = IoC.Resolve<ICustomerGroupFolderSecureService>().GetAll();
				var folder = folders.FirstOrDefault(item => item.Id == SelectedFolderId);
                while (folder != null)
				{
					breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", _breadcrumbsSeparator, folder.Title,
					                            breadcrumbs);
					int? parentNodeId = folder.ParentId;
					folder = parentNodeId != null ? folders.First(item => item.Id == parentNodeId) : null;
				}
			}

			if (BreadcrumbAppend != null && BreadcrumbAppend.Count > 0)
			{
				foreach (string crumb in BreadcrumbAppend)
				{
					if (!string.IsNullOrEmpty(crumb))
					{
						breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}{2}", breadcrumbs, _breadcrumbsSeparator, crumb);
					}
				}
			}

			breadcrumbs = string.Format(CultureInfo.CurrentCulture, "{0}{1}", RootNodeTitle, breadcrumbs);
            Breadcrumbs.Text = breadcrumbs;
		}

		protected virtual bool IsSearchResult
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("IsSearchResult") ?? false;
			}
		}
	}
}
