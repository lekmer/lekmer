using System.Collections.ObjectModel;
using Litium.Lekmer.Order;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Scensum.BackOffice.Modules.Customers.Orders
{
	public class OrderDataSource
	{
		private int _rowCount;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public Collection<IOrder> GetMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "socialSecurityNumber"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "customerLastName"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "customerFirstName"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "customerEmail")]
		public int SelectCount(int maximumRows, int startRowIndex, int orderId, string customerFirstName, string customerLastName, string customerEmail, string socialSecurityNumber)
		{
			return _rowCount;
		}

		public Collection<IOrder> GetMethod(int maximumRows, int startRowIndex, int orderId, string customerFirstName, string customerLastName, string customerEmail, string socialSecurityNumber)
		{
			var searchCriteria = IoC.Resolve<ILekmerOrderSearchCriteria>();

			searchCriteria.OrderId = orderId;
			searchCriteria.FirstName = customerFirstName;
			searchCriteria.LastName = customerLastName;
			searchCriteria.Email = customerEmail;
			searchCriteria.Number = socialSecurityNumber;

			SearchCriteriaState<ILekmerOrderSearchCriteria>.Instance.Save(Controller.PathHelper.Order.GetDefaultUrl(), searchCriteria);

			var orderSecureService = IoC.Resolve<IOrderSecureService>();

			return ((ILekmerOrderSecureService)orderSecureService).Search(searchCriteria, startRowIndex / maximumRows + 1, maximumRows, out _rowCount);
		}
	}
}