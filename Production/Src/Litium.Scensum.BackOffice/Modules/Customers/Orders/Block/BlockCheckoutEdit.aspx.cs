using System;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Customers.Orders.Block
{
	public partial class BlockCheckoutEdit : LekmerPageController,IEditor 
	{
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}
		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
			if (!Page.IsPostBack)
			{
				BlockTitleTextBox.Text = IoC.Resolve<IBlockCheckoutSecureService>().GetById(GetBlockId()).Title;
			}
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			SiteStructureTree.NodeCommand += OnSiteStructureTreeNodeCommand;
		}
		protected virtual void OnSiteStructureTreeNodeCommand(object sender, Web.Controls.Tree.TemplatedTree.TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}
		protected override void PopulateForm()
		{
            PopulateData();
			PopulateTranslation(GetBlockId());

            var master = Master as Pages;
            if (master == null) return;
			master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(Resources.Customer.Literal_EditCheckoutBlock);
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockCheckout blockCheckout = IoC.Resolve<IBlockCheckoutSecureService>().GetById(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockCheckout.ContentNodeId));
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			var service = IoC.Resolve<IBlockCheckoutSecureService>();
			IBlockCheckout blockCheckout = service.GetById(GetBlockId());
			if (blockCheckout == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}
			int templateId;
			blockCheckout.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockCheckout.RedirectContentNodeId = SiteStructureTree.SelectedNodeId != -1 ? SiteStructureTree.SelectedNodeId : null;
			blockCheckout.Title = BlockTitleTextBox.Text;

			service.Save(SignInHelper.SignedInSystemUser, blockCheckout);
			if (blockCheckout.Id == -1)
			{
				SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
			}
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockCheckout);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
		}

		protected void PopulateData()
		{
			IBlockCheckout blockCheckout = IoC.Resolve<IBlockCheckoutSecureService>().GetById(GetBlockId());
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockCheckout");
			TemplateList.DataBind();
			ListItem listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);
			ListItem item = TemplateList.Items.FindByValue(blockCheckout.TemplateId.ToString());
			if (item != null)
				item.Selected = true;

			IContentNode node = IoC.Resolve<IContentNodeSecureService>().GetById(blockCheckout.ContentNodeId);
			PopulateTreeView(blockCheckout.RedirectContentNodeId ?? -1 * node.SiteStructureRegistryId);
		}

		protected void PopulateTreeView(int id)
		{
			var master = Master as Pages;
			if (master == null) return;

			SiteStructureTree.SelectedNodeId = id;
			SiteStructureTree.DataSource = master.GetTreeNodes(id, true);
			SiteStructureTree.DataBind();
			SiteStructureTree.DenySelection = SiteStructureTree.SelectedNodeId < 0;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}
