﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated. 
// </auto-generated>
//------------------------------------------------------------------------------

namespace Litium.Scensum.BackOffice.Modules.Customers.DropShip {
    
    
    public partial class DropShipInfoEdit {
        
        /// <summary>
        /// Messager control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.MessageContainer Messager;
        
        /// <summary>
        /// DropShipInfoToolBoxPanel control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.ToolBoxPanel DropShipInfoToolBoxPanel;
        
        /// <summary>
        /// OrdernummerLink control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Common.HyperLinkEncoded OrdernummerLink;
        
        /// <summary>
        /// SupplierNameLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal SupplierNameLiteral;
        
        /// <summary>
        /// RegisteradLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal RegisteradLiteral;
        
        /// <summary>
        /// KundnummerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal KundnummerLiteral;
        
        /// <summary>
        /// AviseringsnummerLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal AviseringsnummerLiteral;
        
        /// <summary>
        /// BetalningsmetodLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BetalningsmetodLiteral;
        
        /// <summary>
        /// BillingFirstNameLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingFirstNameLiteral;
        
        /// <summary>
        /// BillingLastNameLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingLastNameLiteral;
        
        /// <summary>
        /// BillingCompanyLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingCompanyLiteral;
        
        /// <summary>
        /// BillingCoLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingCoLiteral;
        
        /// <summary>
        /// BillingStreetLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingStreetLiteral;
        
        /// <summary>
        /// BillingStreetNrLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingStreetNrLiteral;
        
        /// <summary>
        /// BillingEntranceLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingEntranceLiteral;
        
        /// <summary>
        /// BillingFloorLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingFloorLiteral;
        
        /// <summary>
        /// BillingApartmentNoLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingApartmentNoLiteral;
        
        /// <summary>
        /// BillingZipCodeLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingZipCodeLiteral;
        
        /// <summary>
        /// BillingCityLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingCityLiteral;
        
        /// <summary>
        /// BillingCountryLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingCountryLiteral;
        
        /// <summary>
        /// BillingCellphoneLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingCellphoneLiteral;
        
        /// <summary>
        /// BillingEmailLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal BillingEmailLiteral;
        
        /// <summary>
        /// DeliveryFirstNameLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryFirstNameLiteral;
        
        /// <summary>
        /// DeliveryLastNameLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryLastNameLiteral;
        
        /// <summary>
        /// DeliveryCompanyLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryCompanyLiteral;
        
        /// <summary>
        /// DeliveryCoLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryCoLiteral;
        
        /// <summary>
        /// DeliveryStreetLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryStreetLiteral;
        
        /// <summary>
        /// DeliveryStreetNrLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryStreetNrLiteral;
        
        /// <summary>
        /// DeliveryEntranceLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryEntranceLiteral;
        
        /// <summary>
        /// DeliveryFloorLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryFloorLiteral;
        
        /// <summary>
        /// DeliveryApartmentNoLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryApartmentNoLiteral;
        
        /// <summary>
        /// DeliveryZipCodeLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryZipCodeLiteral;
        
        /// <summary>
        /// DeliveryCityLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryCityLiteral;
        
        /// <summary>
        /// DeliveryCountryLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryCountryLiteral;
        
        /// <summary>
        /// DeliveryCellphoneLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryCellphoneLiteral;
        
        /// <summary>
        /// DeliveryEmailLiteral control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Literal DeliveryEmailLiteral;
        
        /// <summary>
        /// DropShipOrderInfoGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.GridView2.GridViewWithCustomPager DropShipOrderInfoGrid;
        
        /// <summary>
        /// DropShipOrderInfoSummaryGrid control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.BackOffice.UserControls.GridView2.GridViewWithCustomPager DropShipOrderInfoSummaryGrid;
        
        /// <summary>
        /// SendFilesButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Button.ImageLinkButton SendFilesButton;
        
        /// <summary>
        /// ViewPdfButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Button.ImageLinkButton ViewPdfButton;
        
        /// <summary>
        /// CancelButton control.
        /// </summary>
        /// <remarks>
        /// Auto-generated field.
        /// To modify move field declaration from designer file to code-behind file.
        /// </remarks>
        protected global::Litium.Scensum.Web.Controls.Button.ImageLinkButton CancelButton;
    }
}
