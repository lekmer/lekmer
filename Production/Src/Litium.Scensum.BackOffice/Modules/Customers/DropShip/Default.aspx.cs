﻿using System.Globalization;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.DropShip;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Base;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Customers.DropShip
{
	public partial class Default : LekmerPageController
	{
		private IDropShipSecureService _dropShipService;
		protected IDropShipSecureService DropShipService
		{
			get { return _dropShipService ?? (_dropShipService = IoC.Resolve<IDropShipSecureService>()); }
		}

		private ISupplierSecureService _supplierService;
		protected ISupplierSecureService SupplierService
		{
			get { return _supplierService ?? (_supplierService = IoC.Resolve<ISupplierSecureService>()); }
		}

		protected override void SetEventHandlers()
		{
			DropShipInfoGrid.PageIndexChanging += OnPageIndexChanging;
		}

		protected override void PopulateForm()
		{
			PopulateDropShipInformation();
		}

		protected virtual void OnPageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			DropShipInfoGrid.PageIndex = e.NewPageIndex;
			PopulateDropShipInformation();
		}

		protected virtual void PopulateDropShipInformation()
		{
			DropShipInfoGrid.DataSource = DropShipService.GetAllGroupByOrder();
			DropShipInfoGrid.DataBind();
		}

		protected virtual string GetEditUrl(object dropShipOrderId)
		{
			return LekmerPathHelper.DropShip.GetEditUrlForDefault(System.Convert.ToInt32(dropShipOrderId, CultureInfo.CurrentCulture));
		}

		protected string GetSupplier(string supplierNo)
		{
			var supplier = SupplierService.GetByNo(supplierNo);
			return supplier != null ? supplier.Name : string.Empty;
		}
	}
}