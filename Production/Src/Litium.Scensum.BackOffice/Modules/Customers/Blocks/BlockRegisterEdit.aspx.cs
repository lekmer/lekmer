using System;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.Modules.SiteStructure.Pages;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.Template;

namespace Litium.Scensum.BackOffice.Modules.Customers.Blocks
{
	public partial class BlockRegisterEdit : LekmerPageController, IEditor
	{
		protected virtual int GetBlockId()
		{
			return Request.QueryString.GetInt32("BlockId");
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			((Master.Start)(Master).Master.Master).SetActiveTab("SiteStructure", "Pages");
		}
		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			SiteStructureTree.NodeCommand += OnSiteStructureTreeNodeCommand;
		}

		protected virtual void OnSiteStructureTreeNodeCommand(object sender, Web.Controls.Tree.TemplatedTree.TreeViewEventArgs e)
		{
			PopulateTreeView(e.Id);
		}

		protected override void PopulateForm()
		{
			PopulateData();
			PopulateTranslation(GetBlockId());

			var master = Master as Pages;
			if (master == null) return;
            master.Breadcrumbs.Clear();
			master.Breadcrumbs.Add(Resources.Customer.Literal_EditRegisterBlock);
		}
		public virtual void OnCancel(object sender, EventArgs e)
		{
			IBlockRegister blockRegister = IoC.Resolve<IBlockRegisterSecureService>().GetById(GetBlockId());
			Response.Redirect(PathHelper.SiteStructure.Page.GetPageEditUrl(blockRegister.ContentNodeId));
		}
		public virtual void OnSave(object sender, EventArgs e)
		{
			IBlockRegister blockRegister = IoC.Resolve<IBlockRegisterSecureService>().GetById(GetBlockId());
			if (blockRegister == null)
			{
				throw new BusinessObjectNotExistsException(GetBlockId());
			}
			int templateId;
			blockRegister.TemplateId = int.TryParse(TemplateList.SelectedValue, out templateId) ? (int?)templateId : null;
			blockRegister.Title = BlockTitleTextBox.Text;
			blockRegister.RedirectContentNodeId = SiteStructureTree.SelectedNodeId <= 0 ? null : SiteStructureTree.SelectedNodeId;

            var blockregisterSecureService = IoC.Resolve<IBlockRegisterSecureService>();
            blockregisterSecureService.Save(SignInHelper.SignedInSystemUser, blockRegister);
            if (blockRegister.Id == -1)
            {
                SystemMessageContainer.Add(Resources.GeneralMessage.BlockTitleExist);
            }
			else
			{
				var translations = Translator.GetTranslations();
				IoC.Resolve<IBlockTitleTranslationSecureService>().Save(SignInHelper.SignedInSystemUser, translations);

				SystemMessageContainer.Add(Resources.GeneralMessage.SaveSuccessBlockRegister);
				SystemMessageContainer.MessageType = InfoType.Success;
			}
        }

		protected void PopulateData()
		{
			var templateSecureService = IoC.Resolve<ITemplateSecureService>();
			TemplateList.DataSource = templateSecureService.GetAllByModel("BlockRegister");
			TemplateList.DataBind();
			ListItem listItem = new ListItem(Resources.SiteStructure.Literal_UseTheme, string.Empty);
			listItem.Attributes.Add("class", "use-theme");
			TemplateList.Items.Insert(0, listItem);

			IBlockRegister blockRegister = IoC.Resolve<IBlockRegisterSecureService>().GetById(GetBlockId());
			TemplateList.SelectedValue = blockRegister.TemplateId.ToString();
            
			BlockTitleTextBox.Text = blockRegister.Title;

			IContentNode node = IoC.Resolve<IContentNodeSecureService>().GetById(blockRegister.ContentNodeId);
			PopulateTreeView(blockRegister.RedirectContentNodeId ?? -1 * node.SiteStructureRegistryId);
		}

		protected void PopulateTreeView(int id)
		{
			var master = Master as Pages;
			if (master == null) return;

			SiteStructureTree.SelectedNodeId = id;
			SiteStructureTree.DataSource = master.GetTreeNodes(id, true);
			SiteStructureTree.DataBind();
			SiteStructureTree.DenySelection = SiteStructureTree.SelectedNodeId < 0;
		}

		protected virtual void PopulateTranslation(int blockId)
		{
			Translator.DefaultValueControlClientId = BlockTitleTextBox.ClientID;
			Translator.BusinessObjectId = blockId;
			Translator.DataSource = IoC.Resolve<IBlockTitleTranslationSecureService>().GetAllByBlock(blockId);
			Translator.DataBind();
		}
	}
}
