<%@ Import Namespace="Litium.Scensum.Order"%>
<%@ Page Language="C#" MasterPageFile="~/Modules/Customers/CustomerSearch/Customers.Master"
	CodeBehind="CustomerEdit.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch.CustomerEdit" %>

<%@ Import Namespace="Litium.Scensum.BackOffice.Controller" %>
<%@ Register TagPrefix="uc" Namespace="Litium.Scensum.Web.Controls.Common" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="ajaxToolkit" Namespace="AjaxControlToolkit" Assembly="AjaxControlToolkit" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls" Assembly="Litium.Scensum.BackOffice" %>
<%@ Register TagPrefix="sc" Namespace="Litium.Scensum.BackOffice.UserControls.GridView2"
	Assembly="Litium.Scensum.BackOffice" %>
<%@ Register Src="../../../UserControls/Customer/CustomerEdit/CustomerAddresses.ascx"
	TagName="CustomerAddresses" TagPrefix="uc1" %>
<%@ Register Src="../../../UserControls/Customer/CustomerEdit/CustomerInformationControl.ascx"
	TagName="CustomerInformation" TagPrefix="uc1" %>
<%@ Register Src="../../../UserControls/Customer/CustomerEdit/CustomerCommentControl.ascx"
	TagName="CustomerCommentControl" TagPrefix="uc1" %>
<%@ Register src="../../../UserControls/Customer/CustomerEdit/CustomerUserControl.ascx" tagname="CustomerUserControl" tagprefix="uc1" %>
<asp:Content ID="CustomerEditTab" ContentPlaceHolderID="CustomerPlaceHolder" runat="server">
	<asp:Panel ID="EditPanel" runat="server" DefaultButton="SaveButton">
		<div style="width: 979px; ">
			<asp:UpdatePanel ID="MessagesContainerUpdatePanel" runat="server">
				<ContentTemplate>
					<uc:MessageContainer ID="SystemMessageContainer" MessageType="Failure" HideMessagesControlId="SaveButton"
						runat="server" />
					<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
						ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgCustomerEdit" />
				</ContentTemplate>
			</asp:UpdatePanel>
		</div>

		<br class="clear" />

		<div id="customer-edit-header">
			<asp:Literal runat="server" ID="CustomerNameLiteral" Mode="Encode"></asp:Literal>
		</div>
		<div id="customer-edit">
			<div id="customer-edit-container-left">
				<uc1:CustomerInformation ID="CustomerInformation" ValidationGroup="vgCustomerEdit" runat="server" />

				<br />
				<br />

				<uc1:CustomerAddresses ID="CustomerAddresses" runat="server" />

				<br clear="all" />
				<br clear="all" />

				<label class="assortment-header"><%= Resources.Customer.Label_OrderHistory %></label>

				<asp:UpdatePanel ID="CustomerOrdersUpdatePanel" runat="server" UpdateMode="Conditional">
					<ContentTemplate>
						<div  class="order-history-grid">
							<sc:GridViewWithCustomPager ID="OrdersGrid" SkinID="grid" runat="server" AutoGenerateColumns="false"
								DataSourceID="CustomerObjectDataSource" AllowPaging="true" PageSize="<%$AppSettings:DefaultGridPageSize%>"
								Width="100%">
								<Columns>
									<asp:TemplateField ItemStyle-Width="10%" HeaderText="<%$ Resources:Customer, ColumnHeader_OrderNumber %>">
										<ItemTemplate>
											<asp:HiddenField runat="server" ID="OrderIdHiddenField" Value='<%# Eval("Id") %>' />
											<uc:HyperLinkEncoded runat="server" ID="EditLink" Text='<%# Eval("Number")%>' />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField ItemStyle-Width="10%"  HeaderText="<%$ Resources:Customer, ColumnHeader_Status %>"> <ItemTemplate > <%# Eval("OrderStatus.Title") %></ItemTemplate></asp:TemplateField>
									<asp:TemplateField ItemStyle-Width="25%" ItemStyle-CssClass="customer-order-history" HeaderText="<%$ Resources:Customer, ColumnHeader_DeliveryStreetAddress %>">
										<ItemTemplate>
											<asp:Label ID="lDeliverSstreetAddress" runat="server" ></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField ItemStyle-Width="13%" HeaderText="<%$ Resources:Customer, Literal_PostalCode %>"><ItemTemplate><%# EncodeHelper.HtmlEncode((string)Eval("DeliveryAddress.PostalCode"))%></ItemTemplate></asp:TemplateField>
									<asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Customer, ColumnHeader_OrderValue %>">
										<ItemTemplate>
											<asp:Label ID="lOrderValue" runat="server" ></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField ItemStyle-Width="13%" ItemStyle-HorizontalAlign="Right" HeaderText="<%$ Resources:Customer, ColumnHeader_NoArticles %>">
										<ItemTemplate>
											<asp:Label ID="lNoArticles" runat="server" ></asp:Label>
										</ItemTemplate>
									</asp:TemplateField>
									
									<asp:TemplateField ItemStyle-Width="20%"  HeaderText="<%$ Resources:Customer, ColumnHeader_CreatedDate %>">
										<ItemTemplate>
											<span class="date"><%#((DateTime)Eval("CreatedDate")).ToString("yyyy-MM-dd HH:mm")%></span>
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</sc:GridViewWithCustomPager>
							<asp:ObjectDataSource ID="CustomerObjectDataSource" runat="server" SelectCountMethod="SelectCount"
								SelectMethod="GetOrders" TypeName="Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch.CustomerDataSource" />
						</div>
					</ContentTemplate>
				</asp:UpdatePanel>
				<br />
			</div>
			<div id="customer-edit-container-right">
				<div class="control-container">
					<span><%= Resources.General.Literal_ErpId %></span>
					<br />
					<asp:Literal ID="ErpIdLiteral" runat="server" />
				</div>
				<br clear="all" />
				<div class="control-container">
					<span><%= Resources.Customer.Label_ApplyToChannel%></span>
					<br />
					<asp:DropDownList ID="RegistryList" DataTextField="Title" DataValueField="Id" runat="server"></asp:DropDownList>
				</div>
				<div class="control-container">
					<span><%= Resources.General.Literal_Status %></span><br />
					<asp:DropDownList ID="StatusList" DataTextField="Title" DataValueField="Id" runat="server" />
				</div>
				<br clear="all" />
				<div class="customer-horizontal-separator-right-area"></div>
				<uc1:CustomerUserControl ID="CustomerUserControl"  ValidationGroup="vgCustomerEdit"  runat="server" />
				<br clear="all" />
				<div class="customer-horizontal-separator-right-area"></div>
			
				<div class="control-container">
					<span><%= Resources.Customer.Label_CustomerGroups %></span>
					<div runat="server" id="CustomerGroupsNotFoundDiv" class="customer-groups-not-found" Visible="false">
						<asp:Literal runat="server" ID="CustomerGroupsNotFoundLiteral" Mode="Encode"></asp:Literal>
					</div>
					<div id="customer-groups">
						<asp:Repeater runat="server" ID="GroupGrid">
							<ItemTemplate>
								<uc:HyperLinkEncoded ID="GroupTitleLink" runat="server" Text='<%# Eval("Title") %>'
									NavigateUrl='<%# GetCustomerGroupEditUrl(Eval("Id")) %>'></uc:HyperLinkEncoded>
								<br />
							</ItemTemplate>
						</asp:Repeater>
					</div>
				</div>
				<br clear="all" />
				<div class="customer-horizontal-separator-right-area"></div>
				<div class="control-container">
					<uc1:CustomerCommentControl ID="CustomerCommentControl" runat="server" />
				</div>
			</div>
		</div>
		<br class="clear" />
		<br />
		<div class="customer-edit-action-buttons">
			<uc:ImageLinkButton UseSubmitBehaviour="true" ValidationGroup="vgCustomerEdit" ID="SaveButton"
				runat="server" Text="<%$ Resources:General, Button_Save %>" SkinID="DefaultButton" />
			<uc:ImageLinkButton UseSubmitBehaviour="true" ID="CancelButton" runat="server" Text="<%$ Resources:General, Button_Cancel %>"
				SkinID="DefaultButton" />
		</div>
		<br clear="all"/>
		<br clear="all"/>
	</asp:Panel>

</asp:Content>
