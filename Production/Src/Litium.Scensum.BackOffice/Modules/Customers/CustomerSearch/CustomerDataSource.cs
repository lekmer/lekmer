using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Scensum.Foundation;
using Litium.Scensum.Customer;
using Litium.Scensum.Order;

namespace Litium.Scensum.BackOffice.Modules.Customers.CustomerSearch
{
	public class CustomerDataSource
	{
		private int _rowCount;
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters",MessageId = "startRowIndex"), 
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public Collection<ICustomer> SearchMethod(int maximumRows, int startRowIndex)
		{
			return null;
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "zip"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "phoneMobile"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "phoneHome"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "lastName"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "firstName"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "erpId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "email"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "customerStatusId"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "createdTo"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "createdFrom"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "civicNumber"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "city"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "address"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public int SelectCount(int maximumRows, int startRowIndex, string erpId, string firstName, string civicNumber, string address, string zip, string city, string email, string phoneHome, string phoneMobile, string createdFrom, string createdTo, string lastName, string customerStatusId)
		{
			return _rowCount;
		}
		public Collection<ICustomer> SearchMethod(int maximumRows, int startRowIndex, string erpId, string firstName, string civicNumber, string address, string zip, string city, string email, string phoneHome, string phoneMobile, string createdFrom, string createdTo, string lastName, string customerStatusId)
		{
			ICustomerSearchCriteria searchCriteria = IoC.Resolve<ICustomerSearchCriteria>();

			searchCriteria.ErpId = erpId;
			searchCriteria.FirstName = firstName;
			searchCriteria.LastName = lastName;
			searchCriteria.Number = civicNumber;
			searchCriteria.Address = address;
			searchCriteria.Zip = zip;
			searchCriteria.City = city;
			searchCriteria.Email = email;
			searchCriteria.PhoneHome = phoneHome;
			searchCriteria.PhoneMobile = phoneMobile;
			searchCriteria.CreatedFrom = createdFrom;
			searchCriteria.CreatedTo = createdTo;
			searchCriteria.CustomerStatusId = customerStatusId;

			ICustomerSecureService customerSecureService = IoC.Resolve<ICustomerSecureService>();
			return customerSecureService.Search(searchCriteria, maximumRows == 0 ? 0 : startRowIndex / maximumRows + 1, maximumRows, out _rowCount);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "startRowIndex"),
		System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "maximumRows")]
		public static Collection<IOrder> GetOrders(int maximumRows, int startRowIndex)
		{
			return null;
		}
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA1801:ReviewUnusedParameters", MessageId = "customerId")]
		public int SelectCount(string customerId)
		{
			return _rowCount;
		}
		public Collection<IOrderFull> GetOrders(string customerId)
		{
			IOrderSecureService orderSecureService = IoC.Resolve<IOrderSecureService>();
			Collection<IOrderFull> orders = orderSecureService.GetAllFullByCustomer(int.Parse(customerId, CultureInfo.CurrentCulture));
			_rowCount = orders.Count;
			return orders;
		}
	}
}
