﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Master/Main.Master"
	CodeBehind="Default.aspx.cs" Inherits="Litium.Scensum.BackOffice.Modules.Security.ChangeOwnPassword.Default" %>

<%@ Register TagPrefix="Scensum" Namespace="Litium.Scensum.Web.Controls" Assembly="Litium.Scensum.Web.Controls" %>
<%@ Register TagPrefix="uc" TagName="UserCredential" Src="~/UserControls/Security/UserCredential.ascx" %>
<asp:Content ID="ChangePasswordContent" ContentPlaceHolderID="body" runat="server">
	<Scensum:ToolBoxPanel ID="ChangePasswordToolBoxPanel" runat="server" Text="<%$ Resources:User, Literal_ChangeOwnPassword %>"
		ShowSeparator="true">
	</Scensum:ToolBoxPanel>	<asp:UpdatePanel ID="ChangePasswordUpdatePanel" runat="server">
			<ContentTemplate>
			<div style="  float:left; width:984px; ">
			<uc:MessageContainer ID="messager" MessageType="Failure" HideMessagesControlId="SaveButton"
					runat="server" />
				<uc:ScensumValidationSummary ForeColor="Black" runat="server" CssClass="advance-validation-summary"
					ID="ValidationSummary" DisplayMode="List" ValidationGroup="vgUserEdit" />
	</div>
	<div id="user-edit">
	
				<uc:UserCredential ID="UserCredentialControl" runat="server" />
				<br class="clear" />
				<br />
				<div class="buttons right">
					<uc:ImageLinkButton ID="SaveButton" runat="server" Text="<%$ Resources:General, Button_Change %>"
						ValidationGroup="vgUserEdit" SkinID="DefaultButton" />
				</div>
		
	</div>	</ContentTemplate>
		</asp:UpdatePanel>
</asp:Content>
