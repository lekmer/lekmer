﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Lekmer.Product;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.BackOffice.Modules.Tools.Sizes
{
	public partial class Default : LekmerPageController
	{
		private ISizeSecureService _sizeSecureService;
		protected ISizeSecureService SizeSecureService
		{
			get { return _sizeSecureService ?? (_sizeSecureService = IoC.Resolve<ISizeSecureService>()); }
		}

		private Collection<ISize> _sizes;
		protected Collection<ISize> Sizes
		{
			get { return _sizes ?? (_sizes = SizeSecureService.GetAll()); }
		}

		protected override void SetEventHandlers()
		{
			ImportSizeButton.Click += ImportSize;
		}

		protected override void PopulateForm()
		{
		}

		protected virtual void ImportSize(object sender, EventArgs e)
		{
			try
			{
				var erpId = ErpIdTextBox.Text.Trim();
				var value = ValueTextBox.Text.Trim();
				if (erpId.IsNullOrTrimmedEmpty() || value.IsNullOrTrimmedEmpty())
				{
					Messager.Add("ERP ID and Value cannot be empty", InfoType.Warning);
					return;
				}

				var existingSize = Sizes.FirstOrDefault(s => s.ErpId == erpId);
				if (existingSize != null)
				{
					Messager.Add("Size with the same ERP ID already exists.", InfoType.Warning);
					return;
				}

				var newSize = SizeSecureService.Create();
				newSize.ErpId = erpId;
				newSize.ErpTitle = value;
				newSize.Eu = decimal.Parse(erpId);
				newSize.EuTitle = value;
				newSize.UsMale = 0;
				newSize.UsFemale = 0;
				newSize.UkMale = 0;
				newSize.UkFemale = 0;
				newSize.Millimeter = 0;

				SizeSecureService.Save(SignInHelper.SignedInSystemUser, newSize);

				Messager.Add(Resources.Tools.Message_FileProcessed, InfoType.Success);

				ErpIdTextBox.Text = string.Empty;
				ValueTextBox.Text = string.Empty;
			}
			catch (Exception exception)
			{
				Messager.Add(string.Format("ERROR: {0}", exception.Message), InfoType.Warning);
			}
		}
	}
}