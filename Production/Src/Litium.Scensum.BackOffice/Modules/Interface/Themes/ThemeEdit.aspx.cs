﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;
using Litium.Lekmer.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller;
using Litium.Scensum.BackOffice.Controller.Contract;
using Litium.Scensum.BackOffice.UserControls;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using ITemplate = Litium.Scensum.Template.ITemplate;

namespace Litium.Scensum.BackOffice.Modules.Interface.Themes
{
	public partial class ThemeEdit : LekmerPageController, IEditor
	{
		private const int _inheritedTemplateId = 0;
		private const int _defaultThemeId = 0;

		private Collection<IModel> _models;
        private Collection<ITemplate> _templates;

		private bool IsCopy
		{
			get
			{
				return Request.QueryString.GetBooleanOrNull("Copy") ?? false;
			}
		}

		protected override void SetEventHandlers()
		{
			CancelButton.Click += OnCancel;
			SaveButton.Click += OnSave;
			DeleteButton.Click += OnDelete;
			ModelFolderRepeater.ItemDataBound += ModelFolderRepeaterBound;
		}

		protected override void PopulateForm()
		{
			// If redirected after successful saving new theme -> display success message
			if (Request.QueryString.GetBooleanOrNull("SaveSuccess") ?? false)
			{
				Messager.Add(Resources.GeneralMessage.SaveSuccessTheme);
				Messager.MessageType = InfoType.Success;
			}

			PopulateTheme();
            PopulateModelFolders();
		}

		public virtual void OnCancel(object sender, EventArgs e)
		{
			Response.Redirect(PathHelper.Template.Theme.GetDefaultUrl());
		}

		protected virtual void OnDelete(object sender, EventArgs e)
		{
			int? themeId = GetIdOrNull();
			if (!themeId.HasValue || themeId.Value == _defaultThemeId) return;
			
			IThemeSecureService themeSecureService = IoC.Resolve<IThemeSecureService>();
			themeSecureService.Delete(SignInHelper.SignedInSystemUser, themeId.Value);
			Response.Redirect(PathHelper.Template.Theme.GetDefaultUrl());
		}

		public virtual void OnSave(object sender, EventArgs e)
		{
			Dictionary<int, int?> modelTemplateDictionary = CollectModelTemplateDictionary();
            int? themeId = GetIdOrNull();
			int result = 0;
			//Save default theme (set default template for each model)
			if (themeId.HasValue && themeId.Value == _defaultThemeId && !IsCopy)
			{
				SetDefaultTemplates(modelTemplateDictionary);
			}
			//Save theme
			else
			{
				result = SaveTheme(themeId, modelTemplateDictionary);
				if (result == -1)
				{
					Messager.Add(Resources.InterfaceMessage.ThemeTitleExist);
					return;
				}
			}

			if ((!themeId.HasValue || IsCopy) && result > 0)
			{
				string editUrl = string.Format(CultureInfo.CurrentCulture, "{0}&SaveSuccess=true", PathHelper.Template.Theme.GetEditUrl(result));
				Response.Redirect(editUrl);
				return;
			}

			Messager.Add(Resources.GeneralMessage.SaveSuccessTheme);
			Messager.MessageType = InfoType.Success;
		}

		protected virtual void SetDefaultTemplates(Dictionary<int, int?> modelTemplateDictionary)
		{
			Dictionary<int, int> dictionary = new Dictionary<int, int>();
			foreach (var item in modelTemplateDictionary)
			{
				if (item.Value.HasValue)
					dictionary.Add(item.Key, item.Value.Value);
			}

			ITemplateSecureService templateSecureService = IoC.Resolve<ITemplateSecureService>();
			templateSecureService.SetDefault(SignInHelper.SignedInSystemUser, dictionary);
		}

		protected virtual int SaveTheme(int? themeId, Dictionary<int, int?> modelTemplateDictionary)
		{
			if (ThemeTitleBox.Text.Trim() == "Default")
				return -1;
			IThemeSecureService themeSecureService = IoC.Resolve<IThemeSecureService>();
			ITheme theme = (themeId.HasValue && !IsCopy) ? themeSecureService.GetById(themeId.Value) : themeSecureService.Create();
			theme.Title = ThemeTitleBox.Text;
			return themeSecureService.Save(SignInHelper.SignedInSystemUser, theme, modelTemplateDictionary);
		}

		protected virtual Dictionary<int, int?> CollectModelTemplateDictionary()
		{
			Dictionary<int, int?> modelTemplateDictionary = new Dictionary<int, int?>();

			foreach (RepeaterItem modelFolderItem in ModelFolderRepeater.Items)
			{
				Repeater modelRepeater = modelFolderItem.FindControl("ModelRepeater") as Repeater;
				if (modelRepeater == null) continue;

				foreach (RepeaterItem modelItem in modelRepeater.Items)
				{
					HiddenField modelIdHidden = modelItem.FindControl("ModelIdHidden") as HiddenField;
					DropDownList templateList = modelItem.FindControl("TemplateList") as DropDownList;
					if (templateList == null || modelIdHidden == null) continue;
					if (templateList.SelectedItem == null) continue;

					int modelId;
					int templateId;
					if (!int.TryParse(modelIdHidden.Value, out modelId) || !int.TryParse(templateList.SelectedItem.Value, out templateId)) continue;

					modelTemplateDictionary.Add(modelId, templateId != _inheritedTemplateId ? (int?)templateId : null);
				}
			}
			return modelTemplateDictionary;
		}

		protected virtual void ModelRepeaterBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

			IModel model = e.Item.DataItem as IModel;
			if (model == null) return;

			DropDownList templateList = e.Item.FindControl("TemplateList") as DropDownList;
			if (templateList == null) return;

			PopulateTemplates(model, templateList);
		}

		protected virtual void PopulateTemplates(IModel model, DropDownList templateList)
		{
			IEnumerable<ITemplate> modelTemplates = Templates().Where(template => template.ModelId == model.Id);
			templateList.DataSource = modelTemplates;
			templateList.DataBind();

			int? themeId = GetIdOrNull();
			
			//Create mode
			if (!themeId.HasValue)
			{
				AddInheritedItem(model, templateList);
			}
			//Edit mode - Defaul theme 
			else if (themeId.Value == _defaultThemeId)
			{
				if (model.DefaultTemplateId.HasValue)
					templateList.Items.FindByValue(model.DefaultTemplateId.Value.ToString(CultureInfo.CurrentCulture)).Selected = true;

				if (IsCopy)
					AddInheritedItem(model, templateList);
			}
			//Edit mode
			else
			{
				AddInheritedItem(model, templateList);
				SelectThemeTemplate(model, templateList, themeId);
			}
		}

		protected virtual void AddInheritedItem(IModel model, DropDownList templateList)
		{
			ITemplate defaultTemplate = model.DefaultTemplateId.HasValue
				? Templates().FirstOrDefault(t => t.Id == model.DefaultTemplateId.Value)
				: null;
			string inheritedItemTitle = string.Format(CultureInfo.CurrentCulture, "Inherited ({0})",
				defaultTemplate != null ? defaultTemplate.Title : "<none>");
			string inheritedItemValue = _inheritedTemplateId.ToString(CultureInfo.CurrentCulture);
			templateList.Items.Insert(0, new ListItem(inheritedItemTitle, inheritedItemValue) { Selected = !GetIdOrNull().HasValue });
		}

		protected virtual void SelectThemeTemplate(IModel model, DropDownList templateList, int? themeId)
		{
			ITemplateSecureService service = IoC.Resolve<ITemplateSecureService>();
			ITemplate themeTemplate = service.GetDefaultByModelAndTheme(model.CommonName, themeId.Value);
			string selectedId = themeTemplate != null
			            	? themeTemplate.Id.ToString(CultureInfo.CurrentCulture)
							: _inheritedTemplateId.ToString(CultureInfo.CurrentCulture);
			templateList.Items.FindByValue(selectedId).Selected = true;
		}

		protected virtual void ModelFolderRepeaterBound(object sender, RepeaterItemEventArgs e)
		{
			if (e.Item.ItemType != ListItemType.Item && e.Item.ItemType != ListItemType.AlternatingItem) return;

			IModelFolder modelFolder = e.Item.DataItem as IModelFolder;
			if (modelFolder == null) return;

			Repeater modelRepeater = e.Item.FindControl("ModelRepeater") as Repeater;
			if (modelRepeater == null) return;
			modelRepeater.DataSource = Models().Where(model => model.ModelFolderId == modelFolder.Id);
			modelRepeater.DataBind();
		}

		protected void PopulateModelFolders()
		{
			ModelFolderRepeater.DataSource = IoC.Resolve<IModelFolderSecureService>().GetAll();
			ModelFolderRepeater.DataBind();
		}

		protected Collection<IModel> Models()
		{
			if (_models == null) 
				_models = IoC.Resolve<IModelSecureService>().GetAll();
			return _models;
		}

		protected Collection<ITemplate> Templates()
		{
			if (_templates == null) 
				_templates = IoC.Resolve<ITemplateSecureService>().GetAll();
			return _templates;
		}

		protected void PopulateTheme()
		{
			int? themeId = GetIdOrNull();
			
			//Create mode
			if (!themeId.HasValue)
			{
				ThemeTitleBox.Text = string.Empty;
				ThemeTitleLabel.Text = "<New theme>";
			}
			//Edit mode - Defaul theme 
			else if (themeId.Value == _defaultThemeId)
			{
				ThemeTitleBox.Text = ThemeTitleLabel.Text = "Default";
				ThemeTitleBox.Enabled = false;
			}
			//Edit mode
			else
			{
				ThemeTitleBox.Text = ThemeTitleLabel.Text = IoC.Resolve<IThemeSecureService>().GetById(themeId.Value).Title;
			}
			//Copy mode
			if (IsCopy)
			{
				ThemeTitleLabel.Text = "<New theme>";
				ThemeTitleBox.Text = "Copy of " + ThemeTitleBox.Text;
				ThemeTitleBox.Enabled = true;
			}

			DeleteButton.Visible = themeId.HasValue && themeId.Value != _defaultThemeId && !IsCopy;
		}
	}
}