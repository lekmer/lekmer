﻿$(document).ready(function() {
    $("div#treeview").find("div.tree-item-childs").hide();
    $('a.node-select').click(function() { Node_Action(this); });
    $('img.tree-icon').attr({ src: treeiconscollapsedurl });
    UnmarkAllSelected();
});

function UnmarkAllSelected() {
    $('a.node-selector-active').attr({ 'class': 'node-selector' });
    $("div.tree-item-parent").find('img.tree-icon:first').attr({ src: treeiconscollapsedurl });
    
}

function test(obj) {
    obj.src = treeiconscollapsedurl;
 }

function Node_Action(obj) {
    if (obj.parentElement.parentElement.id == 'node-selected')//unselect
     {
         Collapse(obj.parentElement.parentElement);
         obj.parentElement.parentElement.id = '';
    }
    else//select
    {
        Expand(obj.parentElement.parentElement);
        obj.parentElement.parentElement.id = 'node-selected';
    }
}

function ExpandNode(node, oneLevel) {
    
//    var nodes = $("#treeview .node-selector");
//    for (var i = 0; i < nodes.length; i++) {
//        if (nodes[i].accessKey == key) {
//            Expand(GetParentNode(nodes[i]), false);
//            //if (!oneLevel) $(nodes[i]).parents(".tree-item-parent").find('img:first').attr({ src: treeiconsexpandedurl });
//            if (oneLevel) $(nodes[i]).attr({ 'class': 'node-selector-active' });
//        }
    //    }

    $(node).find('div.tree-item-parent:first').find('div.tree-item-cell-expand:first').find('a.node-expand:first').find('img.tree-icon:first').attr({ src: treeiconsexpandedurl });
    var childNodes = GetChildNodes(node);
    for (var i = 0; i < childNodes.length; i++) {
        $(childNodes[i]).show('fast');
    }

    var parentElem;
    //if (afterPostback)
    parentElem = $(node).parent('div').parent('div').get(0);
    //    else
    //        parentElem = $(node).parent('div').get(0);

    if (null != parentElem && parentElem.tagName == 'DIV' && parentElem.className == 'tree-item-row') Expand(parentElem)
}

function GetParentNode(node) {
    return node.parentNode.parentNode;
 }

function GetChildNodes(node) {
    var childNodes = new Array();
    var previousId = node.id;
    node.id = 'node-temp';
    
    var children = $("#node-temp").children();
    for (var i = 0; i < children.length; i++) {
        if (children[i].className == 'tree-item-childs') {
            childNodes.push(children[i]);
        }
    }
    node.id = previousId;
    
    return childNodes;
}

function Expand(parent) {
    $(parent).find('img.tree-icon:first').attr({ src: treeiconsexpandedurl });
        var childNodes = GetChildNodes(parent);
    for (var i = 0; i < childNodes.length; i++) {
        $(childNodes[i]).show();
    }
    
    if (null != parent.parentNode && parent.parentNode.tagName == 'DIV'
    && (parent.parentNode.className == 'tree-item-childs' 
    | parent.parentNode.className == 'tree-item-parent')) Expand(parent.parentNode)
}

function Collapse(parent) {
    $(parent).find('img.tree-icon:first').attr({ src: treeiconscollapsedurl });
    var previousId = parent.id;
    parent.id = 'node-temp';
    $('#node-temp').find(".tree-item-childs", 1).hide();
    parent.id = previousId;
}

function DoPostback(sender) {
    var key = sender.accessKey;
    var hidden = document.getElementById(hiddenId);
    if (hidden.value != key) {
        hidden.value = key;
        $("#treeview").find(".tree-item-childs").hide(0);
        $('a.node-selector-active').attr({ 'class': 'node-selector' });
    
    if (hidden != null) {
        __doPostBack(hiddenId, '_inpTVArg_ServerChange');
        return false;}
    }
}

