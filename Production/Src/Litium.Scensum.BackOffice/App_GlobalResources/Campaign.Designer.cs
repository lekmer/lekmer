//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18052
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option or rebuild the Visual Studio project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("Microsoft.VisualStudio.Web.Application.StronglyTypedResourceProxyBuilder", "11.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Campaign {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Campaign() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Resources.Campaign", global::System.Reflection.Assembly.Load("App_GlobalResources"));
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit landing page in new window.
        /// </summary>
        internal static string Label_ViewLandingPage {
            get {
                return ResourceManager.GetString("Label_ViewLandingPage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Description for content page.
        /// </summary>
        internal static string Label_WebDescription {
            get {
                return ResourceManager.GetString("Label_WebDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Title for content page.
        /// </summary>
        internal static string Label_WebTitle {
            get {
                return ResourceManager.GetString("Label_WebTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to action.
        /// </summary>
        internal static string Literal_action {
            get {
                return ResourceManager.GetString("Literal_action", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Actions.
        /// </summary>
        internal static string Literal_Actions {
            get {
                return ResourceManager.GetString("Literal_Actions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All batches in range.
        /// </summary>
        internal static string Literal_AllBatchesInRange {
            get {
                return ResourceManager.GetString("Literal_AllBatchesInRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to All products in range.
        /// </summary>
        internal static string Literal_AllProductsInRange {
            get {
                return ResourceManager.GetString("Literal_AllProductsInRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apply action for.
        /// </summary>
        internal static string Literal_ApplyActionFor {
            get {
                return ResourceManager.GetString("Literal_ApplyActionFor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apply as.
        /// </summary>
        internal static string Literal_ApplyAs {
            get {
                return ResourceManager.GetString("Literal_ApplyAs", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apply to.
        /// </summary>
        internal static string Literal_ApplyToCampaignRegistry {
            get {
                return ResourceManager.GetString("Literal_ApplyToCampaignRegistry", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to selected brands.
        /// </summary>
        internal static string Literal_BrandsConfirmRemove {
            get {
                return ResourceManager.GetString("Literal_BrandsConfirmRemove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bring online.
        /// </summary>
        internal static string Literal_BringOnline {
            get {
                return ResourceManager.GetString("Literal_BringOnline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to campaign.
        /// </summary>
        internal static string Literal_campaign {
            get {
                return ResourceManager.GetString("Literal_campaign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campaign.
        /// </summary>
        internal static string Literal_CampaignCapital {
            get {
                return ResourceManager.GetString("Literal_CampaignCapital", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campaign flag.
        /// </summary>
        internal static string Literal_CampaignFlag {
            get {
                return ResourceManager.GetString("Literal_CampaignFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to campaign folder.
        /// </summary>
        internal static string Literal_campaignFolder {
            get {
                return ResourceManager.GetString("Literal_campaignFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campaign level.
        /// </summary>
        internal static string Literal_CampaignLevel {
            get {
                return ResourceManager.GetString("Literal_CampaignLevel", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campaign price type.
        /// </summary>
        internal static string Literal_CampaignPriceType {
            get {
                return ResourceManager.GetString("Literal_CampaignPriceType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Campaigns.
        /// </summary>
        internal static string Literal_Campaigns {
            get {
                return ResourceManager.GetString("Literal_Campaigns", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cart campaign.
        /// </summary>
        internal static string Literal_CartCampaign {
            get {
                return ResourceManager.GetString("Literal_CartCampaign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cart campaigns.
        /// </summary>
        internal static string Literal_CartCampaigns {
            get {
                return ResourceManager.GetString("Literal_CartCampaigns", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to selected categories.
        /// </summary>
        internal static string Literal_CategoriesConfirmRemove {
            get {
                return ResourceManager.GetString("Literal_CategoriesConfirmRemove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to cheapest product.
        /// </summary>
        internal static string Literal_CheapestProduct {
            get {
                return ResourceManager.GetString("Literal_CheapestProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Class.
        /// </summary>
        internal static string Literal_Class {
            get {
                return ResourceManager.GetString("Literal_Class", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Conditions.
        /// </summary>
        internal static string Literal_Conditions {
            get {
                return ResourceManager.GetString("Literal_Conditions", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Condition type.
        /// </summary>
        internal static string Literal_ConditionType {
            get {
                return ResourceManager.GetString("Literal_ConditionType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configure action.
        /// </summary>
        internal static string Literal_ConfigureAction {
            get {
                return ResourceManager.GetString("Literal_ConfigureAction", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Configure condition.
        /// </summary>
        internal static string Literal_ConfigureCondition {
            get {
                return ResourceManager.GetString("Literal_ConfigureCondition", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to AmountCampaign.
        /// </summary>
        internal static string Literal_CreateAmountCampaign {
            get {
                return ResourceManager.GetString("Literal_CreateAmountCampaign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create campaign.
        /// </summary>
        internal static string Literal_CreateCampaign {
            get {
                return ResourceManager.GetString("Literal_CreateCampaign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create flag.
        /// </summary>
        internal static string Literal_CreateFlag {
            get {
                return ResourceManager.GetString("Literal_CreateFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Percentage campaign.
        /// </summary>
        internal static string Literal_CreatePercentageCampaign {
            get {
                return ResourceManager.GetString("Literal_CreatePercentageCampaign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to selected customer groups.
        /// </summary>
        internal static string Literal_CustomerGroupsConfirmRemove {
            get {
                return ResourceManager.GetString("Literal_CustomerGroupsConfirmRemove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delivery methods.
        /// </summary>
        internal static string Literal_DeliveryMethodSelector {
            get {
                return ResourceManager.GetString("Literal_DeliveryMethodSelector", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Discount type.
        /// </summary>
        internal static string Literal_DiscountType {
            get {
                return ResourceManager.GetString("Literal_DiscountType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit flag.
        /// </summary>
        internal static string Literal_EditFlag {
            get {
                return ResourceManager.GetString("Literal_EditFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Exclude.
        /// </summary>
        internal static string Literal_Exclude {
            get {
                return ResourceManager.GetString("Literal_Exclude", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Apply as selective.
        /// </summary>
        internal static string Literal_Exclusive {
            get {
                return ResourceManager.GetString("Literal_Exclusive", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to fixed discount.
        /// </summary>
        internal static string Literal_FixedDiscount {
            get {
                return ResourceManager.GetString("Literal_FixedDiscount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Flag.
        /// </summary>
        internal static string Literal_Flag {
            get {
                return ResourceManager.GetString("Literal_Flag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Flags.
        /// </summary>
        internal static string Literal_Flags {
            get {
                return ResourceManager.GetString("Literal_Flags", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to flag.
        /// </summary>
        internal static string Literal_FlagSmall {
            get {
                return ResourceManager.GetString("Literal_FlagSmall", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sending interval (in days).
        /// </summary>
        internal static string Literal_GiftCardViaEmailSendingInterval {
            get {
                return ResourceManager.GetString("Literal_GiftCardViaEmailSendingInterval", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Action type.
        /// </summary>
        internal static string Literal_GiftCardViaEmailType {
            get {
                return ResourceManager.GetString("Literal_GiftCardViaEmailType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Include.
        /// </summary>
        internal static string Literal_Include {
            get {
                return ResourceManager.GetString("Literal_Include", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Whole range.
        /// </summary>
        internal static string Literal_IncludeAllBatches {
            get {
                return ResourceManager.GetString("Literal_IncludeAllBatches", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Whole range.
        /// </summary>
        internal static string Literal_IncludeAllProducts {
            get {
                return ResourceManager.GetString("Literal_IncludeAllProducts", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Inherit from folder.
        /// </summary>
        internal static string Literal_InheritFromFolder {
            get {
                return ResourceManager.GetString("Literal_InheritFromFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to most expensive product.
        /// </summary>
        internal static string Literal_MostExpensiveProduct {
            get {
                return ResourceManager.GetString("Literal_MostExpensiveProduct", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Move.
        /// </summary>
        internal static string Literal_Move {
            get {
                return ResourceManager.GetString("Literal_Move", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No flag.
        /// </summary>
        internal static string Literal_NoFlag {
            get {
                return ResourceManager.GetString("Literal_NoFlag", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to percentage discount.
        /// </summary>
        internal static string Literal_PercentageDiscount {
            get {
                return ResourceManager.GetString("Literal_PercentageDiscount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value.
        /// </summary>
        internal static string Literal_PercentageDiscountValue {
            get {
                return ResourceManager.GetString("Literal_PercentageDiscountValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Price type.
        /// </summary>
        internal static string Literal_PriceType {
            get {
                return ResourceManager.GetString("Literal_PriceType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product campaign.
        /// </summary>
        internal static string Literal_ProductCampaign {
            get {
                return ResourceManager.GetString("Literal_ProductCampaign", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Product campaigns.
        /// </summary>
        internal static string Literal_ProductCampaigns {
            get {
                return ResourceManager.GetString("Literal_ProductCampaigns", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select page size for Include/Exclude product grids.
        /// </summary>
        internal static string Literal_ProductGridPageSize {
            get {
                return ResourceManager.GetString("Literal_ProductGridPageSize", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to selected products.
        /// </summary>
        internal static string Literal_ProductsConfirmRemove {
            get {
                return ResourceManager.GetString("Literal_ProductsConfirmRemove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantity.
        /// </summary>
        internal static string Literal_Quantity {
            get {
                return ResourceManager.GetString("Literal_Quantity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to If Quantity = 0 action will be applied for all items of one product (cheapest or most expensive).
        /// </summary>
        internal static string Literal_QuantityMessage {
            get {
                return ResourceManager.GetString("Literal_QuantityMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantity of items of one product (cheapest or most expensive) to which will be applied action.
        /// </summary>
        internal static string Literal_QuantityOfItems {
            get {
                return ResourceManager.GetString("Literal_QuantityOfItems", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select campaign folder.
        /// </summary>
        internal static string Literal_SelectFolder {
            get {
                return ResourceManager.GetString("Literal_SelectFolder", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set campaign cardinality.
        /// </summary>
        internal static string Literal_SetCampaignCardinality {
            get {
                return ResourceManager.GetString("Literal_SetCampaignCardinality", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set cardinality.
        /// </summary>
        internal static string Literal_SetCardinality {
            get {
                return ResourceManager.GetString("Literal_SetCardinality", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set offline.
        /// </summary>
        internal static string Literal_SetOffline {
            get {
                return ResourceManager.GetString("Literal_SetOffline", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Status....
        /// </summary>
        internal static string Literal_StatusEmpty {
            get {
                return ResourceManager.GetString("Literal_StatusEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Always.
        /// </summary>
        internal static string Literal_TypeAlways {
            get {
                return ResourceManager.GetString("Literal_TypeAlways", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Type....
        /// </summary>
        internal static string Literal_TypeEmpty {
            get {
                return ResourceManager.GetString("Literal_TypeEmpty", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selective.
        /// </summary>
        internal static string Literal_TypeSelective {
            get {
                return ResourceManager.GetString("Literal_TypeSelective", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value From.
        /// </summary>
        internal static string Literal_ValueFrom {
            get {
                return ResourceManager.GetString("Literal_ValueFrom", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value To.
        /// </summary>
        internal static string Literal_ValueTo {
            get {
                return ResourceManager.GetString("Literal_ValueTo", resourceCulture);
            }
        }
    }
}
