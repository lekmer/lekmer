﻿using System.Configuration;
using System.Web.Configuration;

namespace Litium.Scensum.BackOffice.Setting
{
	public static class HttpRuntimeSetting
	{
		public static int MaxRequestLength
		{
			get
			{
				HttpRuntimeSection section = ConfigurationManager.GetSection("system.web/httpRuntime") as HttpRuntimeSection;
				if (section != null)
				{
					return section.MaxRequestLength;
				}
				return -1;
			}
		}
	}
}