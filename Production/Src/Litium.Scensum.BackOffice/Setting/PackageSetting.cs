using Litium.Framework.Setting;

namespace Litium.Scensum.BackOffice.Setting
{
	public class PackageSetting : SettingBase
	{
		#region Singleton

		private static readonly PackageSetting _instance = new PackageSetting();

		private PackageSetting()
		{
		}

		public static PackageSetting Instance
		{
			get { return _instance; }
		}

		#endregion

		private const string _groupName = "Default";

		protected override string StorageName
		{
			get { return "Package.BackOffice"; }
		}

		public int PackageAliasFolderId
		{
			get { return GetInt32(_groupName, "PackageAliasFolderId"); }
		}

		public int PackageAliasTypeId
		{
			get { return GetInt32(_groupName, "PackageAliasTypeId"); }
		}
	}
}