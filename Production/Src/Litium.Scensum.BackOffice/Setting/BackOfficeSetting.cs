using Litium.Framework.Setting;

namespace Litium.Scensum.BackOffice.Setting
{
	public sealed class BackOfficeSetting : SettingBase
	{
		//Singleton

		private static readonly BackOfficeSetting _instance = new BackOfficeSetting();

		private BackOfficeSetting()
		{
		}

		public static BackOfficeSetting Instance
		{
			get { return _instance; }
		}

		//

		private const string _groupName = "BackOffice";

		protected override string StorageName
		{
			get { return "BackOffice"; }
		}


		public string ChannelCommonNamesToIgnore
		{
			get { return GetString(_groupName, "ChannelCommonNamesToIgnore"); }
		}

		public string CountryIdsToIgnore
		{
			get { return GetString(_groupName, "CountryIdsToIgnore"); }
		}

		public string CurrencyIdsToIgnore
		{
			get { return GetString(_groupName, "CurrencyIdsToIgnore"); }
		}

		public string ProductRegistryIdsToIgnore
		{
			get { return GetString(_groupName, "ProductRegistryIdsToIgnore"); }
		}


		public string TempReviewsExcelFilePathExport
		{
			get { return GetString(_groupName, "TempReviewsExcelFilePathExport"); }
		}

		public string TempReviewsExcelFilePathImport
		{
			get { return GetString(_groupName, "TempReviewsExcelFilePathImport"); }
		}

		public string ReviewsExcelFileName
		{
			get { return GetString(_groupName, "ReviewsExcelFileName"); }
		}


		public string TempProductDiscountPricesExcelFilePathImport
		{
			get { return GetString(_groupName, "TempProductDiscountPricesExcelFilePathImport"); }
		}


		public string TitlesFilePath
		{
			get { return GetString(_groupName, "TitlesFilePath"); }
		}
		public int LogExpirationTime
		{
			get { return GetInt32(_groupName, "LogExpirationTime", 7); }
		}
		public string EncodingType
		{
			get { return GetString(_groupName, "EncodingType", "ISO-8859-1"); }
		}


		public int BlockAliasFolderId
		{
			get { return GetInt32(_groupName, "BlockAliasFolderId"); }
		}
		public int BlockAliasTypeId
		{
			get { return GetInt32(_groupName, "BlockAliasTypeId", 1); }
		}
	}
}