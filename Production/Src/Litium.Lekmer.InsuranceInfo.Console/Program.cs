﻿using System;
using System.Diagnostics;
using System.Reflection;
using Litium.Lekmer.InsuranceInfo.Contract;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.InsuranceInfo.Console
{
	class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static void Main()
		{
			try
			{
				Stopwatch stopwatch = Stopwatch.StartNew();
				_log.InfoFormat("InsuranceInfo.Console started.");

				var exporter = IoC.Resolve<IExporter>("InsuranceInfoExporter");
				exporter.Execute();
				_log.InfoFormat("InsuranceInfo.Console completed.");

				stopwatch.Stop();
				_log.InfoFormat("InsuranceInfo.Console elapsed time - {0}", stopwatch.Elapsed);
			}
			catch (Exception ex)
			{
				_log.Error("InsuranceInfo.Console failed.", ex);
			}
		}
	}
}