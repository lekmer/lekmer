﻿using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Avail.Cache
{
	public sealed class BlockAvailCombineCache : ScensumCacheBase<BlockAvailCombineKey, IBlockAvailCombine>
	{
		private static readonly BlockAvailCombineCache _instance = new BlockAvailCombineCache();

		private BlockAvailCombineCache ()
		{
		}

		public static BlockAvailCombineCache Instance
		{
			get { return _instance; }
		}
	}

	public class BlockAvailCombineKey : ICacheKey
	{
		public BlockAvailCombineKey(int blockId)
		{
			BlockId = blockId;
		}

		public int BlockId { get; set; }

		public string Key
		{
			get { return string.Format("BlockId_{0}", BlockId); }
		}
	}
}
