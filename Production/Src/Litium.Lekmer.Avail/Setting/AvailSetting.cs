﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.Avail.Setting
{
	public class AvailSetting : SettingBase
	{
		private const string _storagename = "Avail";

		protected override string StorageName
		{
			get { return _storagename; }
		}

		public static string GetStorageName()
		{
			return _storagename;
		}

		private readonly string _groupName;
		public AvailSetting(string groupName)
		{
			_groupName = groupName;
		}

		public string CustomerId()
		{
			return GetString(_groupName, "CustomerId");
		}

		public string UseServerLogging()
		{
			return GetString(_groupName, "UseServerLogging");
		}
	}
}