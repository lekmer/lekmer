﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopListBrandService
	{
		Collection<IBlockBrandTopListBrand> GetAllByBlock(IUserContext context, IBlockBrandTopList block);
	}
}