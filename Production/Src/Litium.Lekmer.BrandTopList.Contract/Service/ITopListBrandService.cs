using Litium.Lekmer.Product;
using Litium.Scensum.Core;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface ITopListBrandService
	{
		BrandCollection GetAllByBlock(IUserContext context, IBlockBrandTopList block, int pageNumber, int pageSize);
	}
}