using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.BrandTopList.Contract
{
	public interface IBlockBrandTopListCategory : IBusinessObjectBase
	{
		int BlockId { get; set; }
		bool IncludeSubcategories { get; set; }
		ICategory Category { get; set; }
	}
}