﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using log4net;

namespace Litium.Lekmer.MediaArchive.Cleanup.Console
{
	public class CleanupHelper
	{
		private readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
		private readonly MediaArchiveCleanupSetting _mediaArchiveCleanupSetting;
		private readonly MediaArchiveCleanupRepository _mediaArchiveCleanupRepository;
		private readonly FileSystem _fileSystem;

		private ICollection<string> _excludeDirs;
		private ICollection<string> ExcludeDirs
		{
			get { return _excludeDirs ?? (_excludeDirs = _mediaArchiveCleanupSetting.GetExcludeDirs); }
		}

		private ICollection<string> _fullCleanupDirs;
		private ICollection<string> FullCleanupDirs
		{
			get { return _fullCleanupDirs ?? (_fullCleanupDirs = _mediaArchiveCleanupSetting.GetFullCleanupDirs); }
		}

		public CleanupHelper(MediaArchiveCleanupSetting mediaArchiveCleanupSetting)
		{
			_mediaArchiveCleanupSetting = mediaArchiveCleanupSetting;
			_mediaArchiveCleanupRepository = new MediaArchiveCleanupRepository();
			_fileSystem = new FileSystem();
		}

		public void Cleanup()
		{
			var notExistsFilesCount = 0;
			long notExistsFilesSizeSum = 0;
			var notUsedFilesCount = 0;
			long notUsedFilesSizeSum = 0;

			foreach (var directory in Directory.GetDirectories(_mediaArchiveCleanupSetting.MediaArchivePath))
			{
				var directoryInfo = new DirectoryInfo(directory);
				_log.InfoFormat("Process folder {0}", directoryInfo.Name);

				if (IsFullCleanupDir(directoryInfo.Name))
				{
					DeleteDirectoryContent(directoryInfo);
					continue;
				}

				if (!IsDirAllowToCheck(directoryInfo.Name)) continue;

				foreach (var subDirectory in Directory.GetDirectories(directory))
				{
					var subDirectoryInfo = new DirectoryInfo(subDirectory);
					if (!IsDirAllowToCheck(subDirectoryInfo.Name)) continue;

					var mediaId = directoryInfo.Name + subDirectoryInfo.Name;
					int id;
					if (int.TryParse(mediaId, out id))
					{
						var isMediaFileUsed = _mediaArchiveCleanupRepository.IsMediaFileUsed(id);
						switch (isMediaFileUsed)
						{
							case MediaArchiveCleanupType.NOT_EXIST:
								if(CalculateStatistic(subDirectory, ref notExistsFilesCount, ref notExistsFilesSizeSum, MediaArchiveCleanupType.NOT_EXIST))
								{
									MoveDirectory(subDirectory);
								}
								break;

							case MediaArchiveCleanupType.NOT_USED:
								if(CalculateStatistic(subDirectory, ref notUsedFilesCount, ref notUsedFilesSizeSum, MediaArchiveCleanupType.NOT_USED))
								{
									MoveDirectory(subDirectory);
								}
								break;

							case MediaArchiveCleanupType.UNKNOWN:
							case MediaArchiveCleanupType.USED:
								break;
						}
					}
				}

				//delete parent directory
				if (Directory.GetDirectories(directory).Length <= 0)
				{
					DeleteDirectory(directory);
				}
			}

			_log.InfoFormat("Not exists files count {0}", notExistsFilesCount);
			_log.InfoFormat("Not exists files size sum {0}", notExistsFilesSizeSum);
			_log.InfoFormat("Not used files count {0}", notUsedFilesCount);
			_log.InfoFormat("Not used files size sum {0}", notUsedFilesSizeSum);
		}

		private bool IsDirAllowToCheck(string dir)
		{
			int mediaId;
			return !ExcludeDirs.Contains(dir) && int.TryParse(dir, out mediaId);
		}

		private bool IsFullCleanupDir(string dir)
		{
			return FullCleanupDirs.Contains(dir);
		}

		private bool CalculateStatistic(string dir, ref int filesCount, ref long filesSizeSum, MediaArchiveCleanupType mediaArchiveCleanupType)
		{
			bool canBeMoved = false;

			var files = Directory.GetFiles(dir, "data.*");

			if (mediaArchiveCleanupType == MediaArchiveCleanupType.NOT_EXIST)
			{
				if (files.Length > 0)
				{
					foreach (var file in files)
					{
						var fileInfo = new FileInfo(file);
						filesSizeSum += fileInfo.Length;
					}
					filesCount++;
				}

				canBeMoved = true;
			}

			if (mediaArchiveCleanupType == MediaArchiveCleanupType.NOT_USED)
			{
				if (files.Length == 0)
				{
					canBeMoved = true;
				}
				else if (files.Length == 1)
				{
					var fileInfo = new FileInfo(files[0]);

					if (fileInfo.CreationTime < DateTime.Now.AddDays(-_mediaArchiveCleanupSetting.NotUsedPeriod))
					{
						filesSizeSum += fileInfo.Length;
						filesCount++;
						canBeMoved = true;
					}
				}
				else
				{
					_log.ErrorFormat("Incorrect count of images by path {0}", dir);
				}
				
			}

			return canBeMoved;
		}

		private void DeleteDirectory(string path)
		{
			try
			{
				_fileSystem.DeleteDir(path);
				_log.InfoFormat("Delete folder {0}", path);
			}
			catch (Exception ex)
			{
				_log.ErrorFormat("Error occurred while deleting folder {0} - {1}", path, ex);
			}
		}

		private void DeleteDirectoryContent(DirectoryInfo directoryInfo)
		{
			try
			{
				_fileSystem.DeleteDirContent(directoryInfo);
				_log.InfoFormat("Delete content of folder {0}", directoryInfo.FullName);
			}
			catch (Exception ex)
			{
				_log.ErrorFormat("Error occurred while deleting content of folder {0} - {1}", directoryInfo.FullName, ex);
			}
		}

		private void MoveDirectory(string sourcePath)
		{
			try
			{
				_fileSystem.MoveDir(sourcePath, _mediaArchiveCleanupSetting.BackupPath);
				_log.InfoFormat("Folder moved {0}", sourcePath);
			}
			catch (Exception ex)
			{
				_log.ErrorFormat("Error occurred while moving folder {0} - {1}", sourcePath, ex);
			}
		}
	}
}