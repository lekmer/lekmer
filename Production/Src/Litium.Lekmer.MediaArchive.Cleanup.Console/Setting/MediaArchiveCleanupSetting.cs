﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;

namespace Litium.Lekmer.MediaArchive.Cleanup.Console
{
	public class MediaArchiveCleanupSetting : SettingBase
	{
		protected override string StorageName
		{
			get { return "MediaArchiveCleanup"; }
		}

		protected virtual string GroupName
		{
			get { return "MediaArchiveCleanupSettings"; }
		}


		// Properties

		public int NotUsedPeriod
		{
			get { return GetInt32(GroupName, "NotUsedPeriod", 20); }
		}

		public bool RemoveNotExistsInDbFiles
		{
			get { return GetBoolean(GroupName, "RemoveNotExistsInDbFiles"); }
		}

		public string BackupPath
		{
			get { return GetString(GroupName, "BackupPath"); }
		}

		public string MediaArchivePath
		{
			get { return GetString(GroupName, "MediaArchivePath"); }
		}

		public string ExcludeDir
		{
			get { return GetString(GroupName, "ExcludeDir"); }
		}

		public string FullCleanupDir
		{
			get { return GetString(GroupName, "FullCleanupDir"); }
		}

		public ICollection<string> GetExcludeDirs
		{
			get { return ConvertToList(ExcludeDir); }
		}

		public ICollection<string> GetFullCleanupDirs
		{
			get { return ConvertToList(FullCleanupDir); }
		}

		// value1,value2, ... , valueN => List<value>
		protected virtual ICollection<string> ConvertToList(string textValue)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}
	}
}