using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.TwosellExport
{
	public class TwosellExportRepository
	{
		protected virtual DataMapperBase<IOrderInfo> CreateOrderInfoDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IOrderInfo>(dataReader);
		}

		public virtual Collection<IOrderInfo> GetAllOrderInfoByDate(int daysAfterPurchase)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("DaysAfterPurchase", daysAfterPurchase, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("TwosellExportRepository.GetAllOrderInfoByDate");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[export].[pOrderInfoGetForTwosell]", parameters, dbSettings))
			{
				return CreateOrderInfoDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}