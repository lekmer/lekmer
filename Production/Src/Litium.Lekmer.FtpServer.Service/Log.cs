﻿using System;
using System.Reflection;
using Litium.Framework.FtpServer;
using log4net;

namespace Litium.Lekmer.FtpServer.Service
{
	public class Log : ILogOutput
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		public void Write(string output)
		{
			_log.Info(output);
		}

		public void LogException(Exception exception)
		{
			_log.Error(exception);
		}

		public void WriteSentResponse(FtpAuthentication authentication, string response)
		{
			_log.InfoFormat("{0}\t{1}\t{2}", DateTime.Now, PadUserName(authentication.UserName), response);
		}

		public void WriteReceivedCommand(FtpAuthentication authentication, string command)
		{
			_log.InfoFormat("{0}\t{1}\t{2}", DateTime.Now, PadUserName(authentication.UserName), command);
		}

		private static string PadUserName(string userName)
		{
			if (userName == null) throw new ArgumentNullException("userName");

			const int length = 30;
			if (userName.Length > length)
			{
				return userName;
			}

			return userName + new string(' ', length - userName.Length);
		}
	}
}