﻿namespace Litium.Lekmer.Product.Repository
{
	public class ProductSeoSettingTitleTranslationRepository : ProductTranslationGenericRepository
	{
		public override string GetAllByProductStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductSeoSettingTitleTranslationGetAllByProduct]";
			}
		}
		public override string SaveStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductSeoSettingTitleTranslationSave]";
			}
		}
	}
}
