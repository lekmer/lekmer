﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class MonitorProductRepository
	{
		protected DataMapperBase<IMonitorProduct> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IMonitorProduct>(dataReader);
		}

		public virtual int Save(IMonitorProduct monitorProduct)
		{
			if (monitorProduct == null) throw new ArgumentNullException("monitorProduct");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", monitorProduct.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", monitorProduct.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Email", monitorProduct.Email, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@CreatedDate", monitorProduct.CreatedDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("@SizeId", monitorProduct.SizeId, SqlDbType.Int),
				};
			DatabaseSetting dbSettings = new DatabaseSetting("MonitorProductRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue(
				"[lekmer].[pMonitorProductSave]", parameters, dbSettings);
		}

		public virtual Collection<IMonitorProduct> GetNextPortion(int previousMaxId, int quantity)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@PreviousMaxId", previousMaxId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Quantity", quantity, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("MonitorProductRepository.GetNextPortion");
			using (
				IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pMonitorProductGetNextPortion]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Delete(int monitorProductId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@MonitorProductId", monitorProductId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("MonitorProductRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pMonitorProductDelete]", parameters, dbSettings);
		}

		public virtual void Delete(int channelId, string email)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Email", email, SqlDbType.VarChar)
				};

			var dbSettings = new DatabaseSetting("MonitorProductRepository.Delete");

			new DataHandler().ExecuteCommand("[lekmer].[pMonitorProductDeleteByEmail]", parameters, dbSettings);
		}
	}
}
