﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockBrandProductListBrandRepository
	{
		protected virtual DataMapperBase<IBlockBrandProductListBrand> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockBrandProductListBrand>(dataReader);
		}


		public virtual Collection<IBlockBrandProductListBrand> GetAllByBlock(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting(
				"BlockBrandProductListBrandRepository.GetAllByBlock");
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect(
						"[lekmer].[pBlockBrandProductListBrandGetAllByBlock]",
						parameters,
						dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IBrand> GetAllByBlockSecure(int channelId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting(
				"BlockBrandProductListBrandRepository.GetAllByBlockSecure");
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect(
						"[lekmer].[pBlockBrandProductListBrandGetAllByBlockSecure]",
						parameters,
						dbSettings))
			{
				return DataMapperResolver.Resolve<IBrand>(dataReader).ReadMultipleRows();
			}
		}


		public virtual Collection<IBrand> GetAllByBlockInverted(int channelId, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting(
				"BlockBrandProductListBrandRepository.GetAllByBlockInvertedSecure");
			using (
				IDataReader dataReader =
					new DataHandler().ExecuteSelect(
						"[lekmer].[pBlockBrandGetAllByBlockInvertedSecure]",
						parameters,
						dbSettings))
			{
				return DataMapperResolver.Resolve<IBrand>(dataReader).ReadMultipleRows();
			}
		}


		public virtual void Save(IBlockBrandProductListBrand blockBrand)
		{
			if (blockBrand == null) throw new ArgumentNullException("blockBrand");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockBrand.BlockId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", blockBrand.BrandId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting(
				"BlockBrandProductListBrandRepository.Save");

			new DataHandler().ExecuteCommand(
				"[lekmer].[pBlockBrandProductListBrandInsert]",
				parameters,
				dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting(
				"BlockBrandProductListBrandRepository.Delete");

			new DataHandler().ExecuteCommand(
				"[lekmer].[pBlockBrandProductListBrandRemoveAll]",
				parameters,
				dbSettings);
		}
	}
}