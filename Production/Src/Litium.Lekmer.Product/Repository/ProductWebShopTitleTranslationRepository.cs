﻿namespace Litium.Lekmer.Product.Repository
{
	public class ProductWebShopTitleTranslationRepository : ProductTranslationGenericRepository
	{
		public override string GetAllByProductStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductWebShopTitleTranslationGetAllByProduct]";
			}
		}
		public override string SaveStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductWebShopTitleTranslationSave]";
			}
		}
	}
}
