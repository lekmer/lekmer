﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ContentMessageRepository 
	{
		protected virtual DataMapperBase<IContentMessage> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IContentMessage>(dataReader);
		}

		protected virtual DataMapperBase<IContentMessageTranslation> CreateTranslationDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IContentMessageTranslation>(dataReader);
		}

		public virtual int Save(IContentMessage contentMessage)
		{
			if (contentMessage == null)
			{
				throw new ArgumentNullException("contentMessage");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageId", contentMessage.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ContentMessageFolderId", contentMessage.ContentMessageFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CommonName", contentMessage.CommonName, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Message", contentMessage.Message, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("IsDropShip", contentMessage.IsDropShip, SqlDbType.Bit),
					ParameterHelper.CreateParameter("StartDate", contentMessage.StartDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("EndDate", contentMessage.EndDate, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("StartDailyIntervalMinutes", contentMessage.StartDailyInterval.HasValue ? (int?)contentMessage.StartDailyInterval.Value.TotalMinutes : null, SqlDbType.Int),
					ParameterHelper.CreateParameter("EndDailyIntervalMinutes", contentMessage.EndDailyInterval.HasValue ? (int?)contentMessage.EndDailyInterval.Value.TotalMinutes : null, SqlDbType.Int),
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pContentMessageSave]", parameters, dbSettings);
		}

		public virtual void Delete(int contentMessageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageId", contentMessageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pContentMessageDelete]", parameters, dbSettings);
		}

		public virtual IContentMessage GetByIdSecure(int contentMessageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageId", contentMessageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<IContentMessage> GetAllByProduct(int productId, int languageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.GetByProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IContentMessage> GetAllByFolder(int contentMessageFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageFolderId", contentMessageFolderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.GetAllByFolder");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageGetAllByFolder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IContentMessage> Search(string searchCriteria)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SearchCriteria", searchCriteria, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.Search");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageSearch]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		// Includes.

		public virtual void SaveIncludes(int contentMessageId, string productIdList, string categoryIdList, string brandIdList, string supplierIdList, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageId", contentMessageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductIds", productIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CategoryIds", categoryIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("BrandIds", brandIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("SupplierIds", supplierIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.SaveIncludes");
			new DataHandler().ExecuteCommand("[lekmer].[pContentMessageIncludesSave]", parameters, dbSettings);
		}

		public virtual IContentMessage GetAllIncludes(IContentMessage contentMessage)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageId", contentMessage.Id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.GetAllIncludes");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageIncludesGetAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					contentMessage.IncludeProducts.Add(dataReader.GetInt32(0));
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						contentMessage.IncludeCategories.Add(dataReader.GetInt32(0));
					}
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						contentMessage.IncludeBrands.Add(dataReader.GetInt32(0));
					}
				}
				if (dataReader.NextResult())
				{
					while (dataReader.Read())
					{
						contentMessage.IncludeSuppliers.Add(dataReader.GetInt32(0));
					}
				}
			}
			return contentMessage;
		}

		public virtual void ClearIncludes(string productIdList, string categoryIdList, string brandIdList, string supplierIdList, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductIds", productIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("CategoryIds", categoryIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("BrandIds", brandIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("SupplierIds", supplierIdList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.ClearIncludes");
			new DataHandler().ExecuteCommand("[lekmer].[pContentMessageIncludesClear]", parameters, dbSettings);
		}

		// Translations.

		public virtual void SaveTranslations(IContentMessageTranslation translation)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageId", translation.ContentMessageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", translation.LanguageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Message", translation.Message, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.SaveTranslations");
			new DataHandler().ExecuteCommand("[lekmer].[pContentMessageTranslationSave]", parameters, dbSettings);
		}

		public virtual Collection<IContentMessageTranslation> GetAllTranslations(int contentMessageId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageId", contentMessageId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageRepository.GetAllTranslations");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageTranslationGetAll]", parameters, dbSettings))
			{
				return CreateTranslationDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}