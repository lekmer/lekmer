﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.Product.Repository
{
	public class ContentMessageFolderRepository
	{
		protected virtual DataMapperBase<IContentMessageFolder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IContentMessageFolder>(dataReader);
		}

		protected virtual DataMapperBase<INode> CreateDataMapperNode(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INode>(dataReader);
		}

		public virtual int Save(IContentMessageFolder contentMessageFolder)
		{
			if (contentMessageFolder == null)
			{
				throw new ArgumentNullException("contentMessageFolder");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageFolderId", contentMessageFolder.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ParentContentMessageFolderId", contentMessageFolder.ParentFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", contentMessageFolder.Title, SqlDbType.NVarChar)
				};
			var dbSettings = new DatabaseSetting("ContentMessageFolderRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pContentMessageFolderSave]", parameters, dbSettings);
		}

		public virtual void Delete(int contentMessageFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageFolderId", contentMessageFolderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageFolderRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pContentMessageFolderDelete]", parameters, dbSettings);
		}

		public virtual IContentMessageFolder GetById(int contentMessageFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentMessageFolderId", contentMessageFolderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageFolderRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageFolderGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<IContentMessageFolder> GetAll()
		{
			var dbSettings = new DatabaseSetting("ContentMessageFolderRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageFolderGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SelectedId", selectedId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageFolderRepository.GetTree");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageFolderGetTree]", parameters, dbSettings))
			{
				return CreateDataMapperNode(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IContentMessageFolder> GetAllByParent(int parentId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ParentId", parentId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ContentMessageFolderRepository.GetAllByParent");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pContentMessageFolderGetAllByParent]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}