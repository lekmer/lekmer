using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ColorRepository
	{
		protected virtual DataMapperBase<IColor> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IColor>(dataReader);
		}


		public virtual Collection<IColor> GetAll(int languageId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("ColorRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[productlek].[pColorGetAll]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}