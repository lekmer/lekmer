﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class RecommendedPriceRepository
	{
		protected DataMapperBase<IRecommendedPrice> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRecommendedPrice>(dataReader);
		}

		public void DeleteAllByProduct(int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RecommendedPriceRepository.DeleteAllByProduct");
			new DataHandler().ExecuteSelect("[lekmer].[pRecommendedPriceDeleteAllByProduct]", parameters, dbSettings);
		}

		public Collection<IRecommendedPrice> GetAllByProduct(int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RecommendedPriceRepository.GetAllByProduct");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pRecommendedPriceGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public void Save(IRecommendedPrice recommendedPrice)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ProductId", recommendedPrice.ProductId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ChannelId", recommendedPrice.ChannelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@Price", recommendedPrice.Price, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("RecommendedPriceRepository.Save");
			new DataHandler().ExecuteSelect("[lekmer].[pRecommendedPriceSave]", parameters, dbSettings);
		}
	}
}