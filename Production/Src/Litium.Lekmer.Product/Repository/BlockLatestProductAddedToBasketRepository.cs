﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BlockLatestProductAddedToBasketRepository
	{
		protected virtual DataMapperBase<IBlockLatestProductAddedToBasket> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockLatestProductAddedToBasket>(dataReader);
		}

		public virtual IBlockLatestProductAddedToBasket GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
				 ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockLatestProductAddedToBasketRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBlockLatestProductAddedToBasketGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public IBlockLatestProductAddedToBasket GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockLatestProductAddedToBasketRepository.GetByIdSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(
				"[lekmer].[pBlockLatestProductAddedToBasketGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void Save(IBlockLatestProductAddedToBasket block)
		{
			if (block == null) throw new ArgumentNullException("block");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", block.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("RedirectToCategory", block.RedirectToCategory, SqlDbType.Bit),
					ParameterHelper.CreateParameter("RedirectToMainCategory", block.RedirectToMainCategory, SqlDbType.Bit),
					ParameterHelper.CreateParameter("RedirectToStartPage", block.RedirectToStartPage, SqlDbType.Bit)
				
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockLatestProductAddedToBasketRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockLatestProductAddedToBasketSave]", parameters, dbSettings);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BlockLatestProductAddedToBasketRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pBlockLatestProductAddedToBasketDelete]", parameters, dbSettings);
		}
	}
}