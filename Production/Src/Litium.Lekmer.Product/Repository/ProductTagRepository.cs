using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductTagRepository
	{
		protected virtual DataMapperBase<IProductTag> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductTag>(dataReader);
		}

		public Collection<IProductTag> GetAll()
		{
			var dbSettings = new DatabaseSetting("ProductTagRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductTagGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}