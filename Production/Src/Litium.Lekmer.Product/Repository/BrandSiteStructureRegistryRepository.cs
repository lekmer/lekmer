using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class BrandSiteStructureRegistryRepository
	{
		protected virtual DataMapperBase<IBrandSiteStructureRegistry> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBrandSiteStructureRegistry>(dataReader);
		}

		public virtual Collection<IBrandSiteStructureRegistry> GetAllByBrand(int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BrandId", brandId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BrandSiteStructureRegistryRepository.GetAllByBrand");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandSiteStructureRegistryGetAllByBrand]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
		

		public virtual Collection<IBrandSiteStructureRegistry> GetAll()
		{

			DatabaseSetting dbSettings = new DatabaseSetting("BrandSiteStructureRegistryRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pBrandSiteStructureRegistryGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Save(IBrandSiteStructureRegistry registry)
		{
			if (registry == null) throw new ArgumentNullException("registry");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", registry.BrandId, SqlDbType.Int),
					ParameterHelper.CreateParameter("SiteStructureRegistryId", registry.SiteStructureRegistryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ContentNodeId", registry.ContentNodeId, SqlDbType.Int)
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BrandSiteStructureRegistryRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pBrandSiteStructureRegistrySave]", parameters, dbSettings);
		}

		public virtual void DeleteByContentNode(int contentNodeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ContentNodeId", contentNodeId, SqlDbType.Int),
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BrandSiteStructureRegistryRepository.DeleteByContentNode");
			new DataHandler().ExecuteCommand("[lekmer].[pBrandSiteStructureRegistryDeleteByContentNode]", parameters, dbSettings);
		}

		public virtual void DeleteByBrand(int contentNodeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BrandId", contentNodeId, SqlDbType.Int),
				};
			DatabaseSetting dbSettings = new DatabaseSetting("BrandSiteStructureRegistryRepository.DeleteByBrand");
			new DataHandler().ExecuteCommand("[lekmer].[pBrandSiteStructureRegistryDeleteByBrand]", parameters, dbSettings);
		}
	}
}