﻿namespace Litium.Lekmer.Product.Repository
{
	public class ProductSeoSettingKeywordsTranslationRepository : ProductTranslationGenericRepository
	{
		public override string GetAllByProductStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductSeoSettingKeywordsTranslationGetAllByProduct]";
			}
		}
		public override string SaveStoredProcedureName
		{
			get
			{
				return "[lekmer].[pProductSeoSettingKeywordsTranslationSave]";
			}
		}
	}
}
