using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class ProductRegistryRepository
	{
		protected virtual DataMapperBase<IProductRegistry> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductRegistry>(dataReader);
		}

		public virtual Collection<IProductRegistry> GetAllSecure()
		{
			var dbSettings = new DatabaseSetting("ProductRegistryRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductRegistryGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}