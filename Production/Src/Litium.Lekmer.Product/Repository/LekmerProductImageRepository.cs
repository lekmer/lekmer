using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product.Repository
{
	public class LekmerProductImageRepository : ProductImageRepository
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		public virtual Collection<IProductImage> GetAllByProduct(int productId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("LekmerProductImageRepository.GetAllByProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductImageGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}