using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Repository
{
	public class SizeRepository
	{
		protected virtual DataMapperBase<ISize> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ISize>(dataReader);
		}

		public virtual Collection<ISize> GetAll()
		{
			var dbSettings = new DatabaseSetting("SizeRepository.GetAll");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual ISize GetById(int sizeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeId", sizeId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pSizeGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual void SetOrdinals(IEnumerable<ISize> sizes)
		{
			string idList = Convert.ToStringIdentifierList(sizes.Select(s => s.Id), Convert.DefaultListSeparator);

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@SizeIdList", idList, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("@Separator", Convert.DefaultListSeparator, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("SizeRepository.SetOrdinals");

			new DataHandler().ExecuteCommand("[lekmer].[pSizeSetOrdinals]", parameters, dbSettings);
		}

		public virtual int Save(ISize size)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeId", size.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ErpId", size.ErpId, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("ErpTitle", size.ErpTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("EU", size.Eu, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("EUTitle", size.EuTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("USMale", size.UsMale, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("USFemale", size.UsFemale, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("UKMale", size.UkMale, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("UKFemale", size.UkFemale, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("Millimeter", size.Millimeter, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeRepository.Save");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pSizeSave]", parameters, dbSettings);
		}

		public virtual int Delete(int sizeId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeId", sizeId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("SizeRepository.Delete");
			return new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pSizeDelete]", parameters, dbSettings);
		}
	}
}