﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockPackageProductListService : IBlockPackageProductListService
	{
		protected BlockPackageProductListRepository Repository { get; private set; }

		public BlockPackageProductListService(BlockPackageProductListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockPackageProductList GetById(IUserContext context, int id)
		{
			return BlockPackageProductListCache.Instance.TryGetItem(
				new BlockPackageProductListKey(context.Channel.Id, id),
				() => GetByIdCore(context, id));
		}

		protected virtual IBlockPackageProductList GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}