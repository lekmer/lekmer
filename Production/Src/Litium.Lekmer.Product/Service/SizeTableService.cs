using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Media;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	public class SizeTableService : ISizeTableService
	{
		protected SizeTableRepository Repository { get; private set; }
		protected ILekmerImageService ImageService { get; private set; }
		protected ILekmerMediaItemService MediaItemService { get; private set; }

		public SizeTableService(
			SizeTableRepository repository,
			IImageService imageService,
			IMediaItemService mediaItemService)
		{
			Repository = repository;
			ImageService = (ILekmerImageService)imageService;
			MediaItemService = (ILekmerMediaItemService)mediaItemService;
		}

		public virtual ISizeTable GetByProduct(IUserContext userContext, int productId)
		{
			if (userContext == null)
			{
				throw new ArgumentNullException("userContext");
			}

			ISizeTable sizeTable = null;

			var sizeTableId = ProductSizeTableIdCache.Instance.TryGetItem(
				new ProductSizeTableIdCacheKey(productId),
				() => Repository.GetByProduct(productId));

			var id = (int) sizeTableId;
			if (id > 0)
			{
				sizeTable = SizeTableCache.Instance.TryGetItem(
					new SizeTableCacheKey(userContext.Channel.Id, id),
					() => GetByIdCore(userContext, id));
			}

			return sizeTable;
		}

		protected virtual ISizeTable GetByIdCore(IUserContext userContext, int sizeTableId)
		{
			MediaItemService.EnsureNotNull();
			
			ISizeTable sizeTable = Repository.GetById(userContext.Channel.Language.Id, sizeTableId);

			Collection<IMediaItem> mediaItems = MediaItemService.GetAllBySizeTable(userContext, sizeTableId);
			Collection<IImage> images = ImageService.GetAllBySizeTable(userContext, sizeTableId);

			foreach (IMediaItem mediaItem in mediaItems)
			{
				var sizeTableMediaItem = IoC.Resolve<ISizeTableMediaItem>();

				sizeTableMediaItem.MediaItem = mediaItem;
				sizeTableMediaItem.Image = images.FirstOrDefault(i => i.Id == mediaItem.Id);

				sizeTable.MediaItems.Add(sizeTableMediaItem);
			}

			return sizeTable;
		}
	}
}