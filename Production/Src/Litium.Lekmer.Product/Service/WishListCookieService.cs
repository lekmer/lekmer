﻿using System;
using System.Web;
using Litium.Lekmer.Common;

namespace Litium.Lekmer.Product
{
	public class WishListCookieService : IWishListCookieService
	{
		private const string WishListCookieKey = "LekmerWishList";

		protected ICookieService CookieService { get; private set; }


		public WishListCookieService(ICookieService cookieService)
		{
			CookieService = cookieService;
		}


		/// <summary>
		/// Creates new wish list key.
		/// </summary>
		/// <returns></returns>
		public virtual Guid CreateNewWishListKey()
		{
			return Guid.NewGuid();
		}

		/// <summary>
		/// Gets the wish list key from cookie.
		/// </summary>
		/// <returns>Wish list key or Guid.Empty</returns>
		public virtual Guid GetWishListKey()
		{
			if (CookieService == null)
			{
				throw new InvalidOperationException("CookieService must be set before calling GetWishListKey.");
			}

			HttpCookie cookie = CookieService.GetRequestCookie(WishListCookieKey);
			if (cookie == null)
			{
				return Guid.Empty;
			}

			try
			{
				return new Guid(cookie.Value);
			}
			catch
			{
				// Invalid cookie value.
				RemoveWishListKey();
				return Guid.Empty;
			}
		}

		/// <summary>
		/// Sets the wish list key to response cookie.
		/// </summary>
		/// <returns></returns>
		public virtual string SetWishListKey(Guid wishListKey)
		{
			if (CookieService == null)
			{
				throw new InvalidOperationException("CookieService must be set before calling SetWishListKey.");
			}

			var cookie = CookieService.GetRequestCookie(WishListCookieKey) ?? new HttpCookie(WishListCookieKey);

			cookie.Value = wishListKey.ToString();
			cookie.Expires = DateTime.Now.AddYears(1);

			CookieService.SetResponseCookie(cookie);

			return cookie.Value;
		}

		/// <summary>
		/// Removes the wish list key cookie from client.
		/// </summary>
		/// <returns></returns>
		public virtual void RemoveWishListKey()
		{
			if (CookieService == null)
			{
				throw new InvalidOperationException("CookieService must be set before calling RemoveWishListKey.");
			}

			var cookie = CookieService.GetRequestCookie(WishListCookieKey);
			if (cookie != null)
			{
				cookie.Value = Guid.Empty.ToString();
				cookie.Expires = DateTime.Now.AddYears(-1);

				CookieService.SetResponseCookie(cookie);
			}
		}
	}
}