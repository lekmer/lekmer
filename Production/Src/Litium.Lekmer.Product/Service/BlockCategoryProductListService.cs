﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockCategoryProductListService : IBlockCategoryProductListService
	{
		protected BlockCategoryProductListRepository Repository { get; private set; }

		public BlockCategoryProductListService(BlockCategoryProductListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockCategoryProductList GetById(IUserContext context, int blockId)
		{
			return Repository.GetById(context.Channel, blockId);
		}
	}
}
