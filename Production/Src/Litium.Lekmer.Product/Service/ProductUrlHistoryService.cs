﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ProductUrlHistoryService : IProductUrlHistoryService
	{
		protected ProductUrlHistoryRepository Repository { get; private set; }

		public ProductUrlHistoryService(ProductUrlHistoryRepository repository)
		{
			Repository = repository;
		}

		public Collection<IProductUrlHistory> GetAllByUrlTitle(IUserContext context, string urlTitle)
		{
			return ProductUrlHistoryCache.Instance.TryGetItem(
				new ProductUrlHistoryKey(context.Channel.Language.Id, urlTitle),
				delegate { return Repository.GetAllByUrlTitle(context.Channel.Language.Id, urlTitle); });
		}

		public Collection<IProductUrlHistory> GetAllByLanguage(IUserContext context)
		{
			return Repository.GetAllByLanguage(context.Channel.Language.Id);
		}

		public void Update(int productUrlHistoryId)
		{
			Repository.Update(productUrlHistoryId);
		}

		public void DeleteExpiredItems()
		{
			Repository.DeleteExpiredItems();

			ProductUrlHistoryCache.Instance.Flush();
		}

		public void CleanUp(int maxCount)
		{
			Repository.CleanUp(maxCount);

			ProductUrlHistoryCache.Instance.Flush();
		}
	}
}