using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ProductPopularityService : IProductPopularityService
	{
		protected ProductPopularityRepository Repository { get; private set; }

		public ProductPopularityService(ProductPopularityRepository repository)
		{
			Repository = repository;
		}

		public Collection<IProductPopularity> GetAll(IUserContext context)
		{
			context.EnsureNotNull();

			return Repository.GetAll(context.Channel.Id);
		}
	}
}