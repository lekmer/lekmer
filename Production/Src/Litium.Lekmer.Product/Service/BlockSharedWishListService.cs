﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockSharedWishListService : IBlockSharedWishListService
	{
		protected BlockSharedWishListRepository Repository { get; private set; }

		public BlockSharedWishListService(
			BlockSharedWishListRepository repository)
		{
			Repository = repository;
		}

		public IBlockSharedWishList GetById(IUserContext context, int id)
		{
			return BlockSharedWishListCache.Instance.TryGetItem(
				new BlockSharedWishListKey(context.Channel.Id, id),
				delegate { return Repository.GetById(context.Channel, id); });
		}
	}
}