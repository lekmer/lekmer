﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockProductListService : IBlockProductListService
	{
		protected BlockProductListRepository Repository { get; private set; }

		public BlockProductListService(BlockProductListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductList GetById(IUserContext context, int id)
		{
			return BlockProductListCache.Instance.TryGetItem(
				new BlockProductListKey(context.Channel.Id, id),
				delegate { return GetByIdCore(context, id); });
		}

		protected virtual IBlockProductList GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}
