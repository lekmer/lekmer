﻿using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class BlockProductImageListService : IBlockProductImageListService
	{
		protected BlockProductImageListRepository Repository { get; private set; }

		public BlockProductImageListService(BlockProductImageListRepository repository)
		{
			Repository = repository;
		}

		public virtual IBlockProductImageList GetById(IUserContext context, int id)
		{
			return BlockProductImageListCache.Instance.TryGetItem(
				new BlockProductImageListKey(context.Channel.Id, id),
				delegate { return GetByIdCore(context, id); });
		}

		protected virtual IBlockProductImageList GetByIdCore(IUserContext context, int id)
		{
			return Repository.GetById(context.Channel, id);
		}
	}
}
