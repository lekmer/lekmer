﻿using Litium.Lekmer.Product.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class LekmerProductSeoSettingService : ProductSeoSettingService
	{
		public LekmerProductSeoSettingService(ProductSeoSettingRepository productSeoSettingRepository)
			: base(productSeoSettingRepository)
		{
		}

		public override IProductSeoSetting GetByProductId(IUserContext context, int productId)
		{
			return ProductSeoSettingCache.Instance.TryGetItem(
				new ProductSeoSettingKey(context.Channel.Id, productId),
				() => base.GetByProductId(context, productId));
		}
	}
}