﻿namespace Litium.Lekmer.Product
{
	public static class ProductUrlExtensions
	{
		/// <summary>
		/// Determines whether specified product URLs are the same.
		/// </summary>
		public static bool IsSameAs(this IProductUrl productUrlLeft, IProductUrl productUrlRight)
		{
			return productUrlLeft.UrlTitle   == productUrlRight.UrlTitle
				&& productUrlLeft.LanguageId == productUrlRight.LanguageId;
		}

		/// <summary>
		/// Determines whether specified product URL and product old URL are the same.
		/// </summary>
		public static bool IsSameAs(this IProductUrl productUrl, IProductUrlHistory productUrlHistory)
		{
			return productUrl.UrlTitle   == productUrlHistory.UrlTitleOld
				&& productUrl.LanguageId == productUrlHistory.LanguageId;
		}
	}
}
