﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockProductSearchEsalesResultSecureService : IBlockProductSearchEsalesResultSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockProductSearchEsalesResultRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }

		public BlockProductSearchEsalesResultSecureService(
			IAccessValidator accessValidator,
			BlockProductSearchEsalesResultRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
		}

		public virtual IBlockProductSearchEsalesResult Create()
		{
			if (AccessSecureService == null)
			{
				throw new InvalidOperationException("AccessSecureService must be set before calling Create.");
			}

			var blockProductSearchEsalesResult = IoC.Resolve<IBlockProductSearchEsalesResult>();

			blockProductSearchEsalesResult.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockProductSearchEsalesResult.Status = BusinessObjectStatus.New;

			return blockProductSearchEsalesResult;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockProductSearchEsalesResult block = Create();

			block.ContentNodeId = contentNodeId;
			block.ContentAreaId = contentAreaId;
			block.BlockTypeId = blockTypeId;
			block.BlockStatusId = (int)BlockStatusInfo.Offline;
			block.Title = title;
			block.TemplateId = null;
			block.SecondaryTemplateId = null;

			block.Id = Save(systemUserFull, block);

			return block;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockProductSearchEsalesResult block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			if (BlockSecureService == null)
			{
				throw new InvalidOperationException("BlockSecureService must be set before calling Save.");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				transactedOperation.Complete();
			}

			BlockProductSearchEsalesResultCache.Instance.Remove(block.Id);

			return block.Id;
		}

		public virtual IBlockProductSearchEsalesResult GetById(int blockId)
		{
			return Repository.GetByIdSecure(blockId);
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(blockId);
				transactedOperation.Complete();
			}

			BlockProductSearchEsalesResultCache.Instance.Remove(blockId);
		}
	}
}