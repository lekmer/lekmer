﻿using System;
using System.Collections.Generic;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	public class LekmerCategoryTree : CategoryTree, ILekmerCategoryTree
	{
		private readonly Dictionary<string, ICategoryTreeItem> _titleIndex = new Dictionary<string, ICategoryTreeItem>();

		protected override void AddToIndex(ICategoryTreeItem treeItem)
		{
			if (treeItem == null) throw new ArgumentNullException("treeItem");

			AddToIdIndex(treeItem);
			AddToTitleIndex(treeItem);
		}

		protected void AddToTitleIndex(ICategoryTreeItem treeItem)
		{
			if (treeItem.Id <= 0 || treeItem.Category == null) return;

			var key = treeItem.Category.Title.ToLower();
			if (!_titleIndex.ContainsKey(key))
			{
				_titleIndex.Add(key, treeItem);
			}
		}

		public ICategoryTreeItem FindItemByTitle(string title)
		{
			ICategoryTreeItem treeItem;
			_titleIndex.TryGetValue(title.ToLower(), out treeItem);
			return treeItem;
		}
	}
}