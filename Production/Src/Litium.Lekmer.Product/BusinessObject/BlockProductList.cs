﻿using System;
using Litium.Lekmer.SiteStructure;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockProductList : LekmerBlockBase, IBlockProductList
	{
		private IBlockSetting _setting;

		public IBlockSetting Setting
		{
			get { return _setting; }
			set
			{
				CheckChanged(_setting, value);
				_setting = value;
			}
		}
	}
}