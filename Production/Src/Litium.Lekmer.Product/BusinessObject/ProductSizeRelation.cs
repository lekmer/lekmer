﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ProductSizeRelation : BusinessObjectBase, IProductSizeRelation
	{
		private int _productId;
		private int _sizeId;

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int SizeId
		{
			get { return _sizeId; }
			set
			{
				CheckChanged(_sizeId, value);
				_sizeId = value;
			}
		}
	}
}