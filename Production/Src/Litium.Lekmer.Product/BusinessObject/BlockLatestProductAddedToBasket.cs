﻿using System;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class BlockLatestProductAddedToBasket : BlockBase, IBlockLatestProductAddedToBasket
	{
		private bool _redirectToStartPage;
		private bool _redirectToMainCategory;

		private bool _redirectToCategory;

		public bool RedirectToStartPage
		{
			get { return _redirectToStartPage; }
			set
			{
				CheckChanged(_redirectToStartPage, value);
				_redirectToStartPage = value;
			}
		}

		public bool RedirectToMainCategory
		{
			get { return _redirectToMainCategory; }
			set
			{
				CheckChanged(_redirectToMainCategory, value);
				_redirectToMainCategory = value;
			}
		}
		public bool RedirectToCategory
		{
			get { return _redirectToCategory; }
			set
			{
				CheckChanged(_redirectToCategory, value);
				_redirectToCategory = value;
			}
		}
	}
}
