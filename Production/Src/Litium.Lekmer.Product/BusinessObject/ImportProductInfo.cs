﻿using System;
using System.Collections.Generic;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ImportProductInfo : BusinessObjectBase, IImportProductInfo
	{
		public string HYErpId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public List<int> TagIdCollection { get; set; }
	}
}