﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerProductViewCollection : Collection<IProductView>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ProductCollection"/> class that is empty.
		/// </summary>
		public LekmerProductViewCollection()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProductCollection"/> class as a wrapper for the specified list.
		/// </summary>
		/// <param name="list">The list that is wrapped by the new collection.</param>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
		public LekmerProductViewCollection(IList<IProductView> list)
			: base(list)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="ProductCollection"/> class as a wrapper for the specified list.
		/// </summary>
		/// <param name="list">The list that is wrapped by the new collection.</param>
		/// <exception cref="T:System.ArgumentNullException"><paramref name="list"/> is null.</exception>
		public LekmerProductViewCollection(IEnumerable<IProductView> list)
			: base(new List<IProductView>(list))
		{
		}

		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// </summary>
		public int TotalCount { get; set; }
	}
}