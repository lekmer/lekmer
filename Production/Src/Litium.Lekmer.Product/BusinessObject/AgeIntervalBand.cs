using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class AgeIntervalBand : BusinessObjectBase, IAgeIntervalBand
	{
		private int? _fromMonth;
		private int? _toMonth;

		public int? FromMonth
		{
			get { return _fromMonth; }
			set
			{
				CheckChanged(_fromMonth, value);
				_fromMonth = value;
			}
		}

		public int? ToMonth
		{
			get { return _toMonth; }
			set
			{
				CheckChanged(_toMonth, value);
				_toMonth = value;
			}
		}
	}
}