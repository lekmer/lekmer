﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeTable : BusinessObjectBase, ISizeTable
	{
		private int _id;
		private int _sizeTableFolderId;
		private string _commonName;
		private string _title;
		private string _description;
		private string _column1Title;
		private string _column2Title;
		private ProductIdDictionary _includeProducts;
		private Collection<ISizeTableCategoryBrandPair> _includeCategoryBrandPairs;
		private Collection<ISizeTableRow> _sizeTableRows;
		private Collection<ISizeTableMediaItem> _sizeTableMediaItems;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int SizeTableFolderId
		{
			get { return _sizeTableFolderId; }
			set
			{
				CheckChanged(_sizeTableFolderId, value);
				_sizeTableFolderId = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}

		public string Column1Title
		{
			get { return _column1Title; }
			set
			{
				CheckChanged(_column1Title, value);
				_column1Title = value;
			}
		}

		public string Column2Title
		{
			get { return _column2Title; }
			set
			{
				CheckChanged(_column2Title, value);
				_column2Title = value;
			}
		}

		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts ?? (_includeProducts = new ProductIdDictionary()); }
			set { _includeProducts = value; }
		}

		public Collection<ISizeTableCategoryBrandPair> IncludeCategoryBrandPairs
		{
			get { return _includeCategoryBrandPairs ?? (_includeCategoryBrandPairs = new Collection<ISizeTableCategoryBrandPair>()); }
			set { _includeCategoryBrandPairs = value; }
		}

		public Collection<ISizeTableRow> Rows
		{
			get { return _sizeTableRows ?? (_sizeTableRows = new Collection<ISizeTableRow>()); }
			set { _sizeTableRows = value; }
		}

		public Collection<ISizeTableMediaItem> MediaItems
		{
			get { return _sizeTableMediaItems ?? (_sizeTableMediaItems = new Collection<ISizeTableMediaItem>()); }
			set { _sizeTableMediaItems = value; }
		}
	}
}