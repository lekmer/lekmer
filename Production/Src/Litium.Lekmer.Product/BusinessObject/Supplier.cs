﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class Supplier : BusinessObjectBase, ISupplier
	{
		private int _id;
		private string _supplierNo;
		private string _activePassiveCode;
		private string _name;
		private string _address;
		private string _zipArea;
		private string _city;
		private string _countryIso;
		private string _languageIso;
		private string _vatRate;
		private string _phone1;
		private string _phone2;
		private string _email;
		private bool _isDropShip;
		private string _dropShipEmail;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public string SupplierNo
		{
			get { return _supplierNo; }
			set
			{
				CheckChanged(_supplierNo, value);
				_supplierNo = value;
			}
		}

		public string ActivePassiveCode
		{
			get { return _activePassiveCode; }
			set
			{
				CheckChanged(_activePassiveCode, value);
				_activePassiveCode = value;
			}
		}

		public string Name
		{
			get { return _name; }
			set
			{
				CheckChanged(_name, value);
				_name = value;
			}
		}

		public string Address
		{
			get { return _address; }
			set
			{
				CheckChanged(_address, value);
				_address = value;
			}
		}

		public string ZipArea
		{
			get { return _zipArea; }
			set
			{
				CheckChanged(_zipArea, value);
				_zipArea = value;
			}
		}

		public string City
		{
			get { return _city; }
			set
			{
				CheckChanged(_city, value);
				_city = value;
			}
		}

		public string CountryIso
		{
			get { return _countryIso; }
			set
			{
				CheckChanged(_countryIso, value);
				_countryIso = value;
			}
		}

		public string LanguageIso
		{
			get { return _languageIso; }
			set
			{
				CheckChanged(_languageIso, value);
				_languageIso = value;
			}
		}

		public string VatRate
		{
			get { return _vatRate; }
			set
			{
				CheckChanged(_vatRate, value);
				_vatRate = value;
			}
		}

		public string Phone1
		{
			get { return _phone1; }
			set
			{
				CheckChanged(_phone1, value);
				_phone1 = value;
			}
		}

		public string Phone2
		{
			get { return _phone2; }
			set
			{
				CheckChanged(_phone2, value);
				_phone2 = value;
			}
		}

		public string Email
		{
			get { return _email; }
			set
			{
				CheckChanged(_email, value);
				_email = value;
			}
		}

		public bool IsDropShip
		{
			get { return _isDropShip; }
			set
			{
				CheckChanged(_isDropShip, value);
				_isDropShip = value;
			}
		}

		public string DropShipEmail
		{
			get { return _dropShipEmail; }
			set
			{
				CheckChanged(_dropShipEmail, value);
				_dropShipEmail = value;
			}
		}
	}
}