﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeTableCategoryBrandPair : BusinessObjectBase, ISizeTableCategoryBrandPair
	{
		private string _id;
		private int _categoryId;
		private int? _brandId;

		public string Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int CategoryId
		{
			get { return _categoryId; }
			set
			{
				CheckChanged(_categoryId, value);
				_categoryId = value;
			}
		}

		public int? BrandId
		{
			get { return _brandId; }
			set
			{
				CheckChanged(_brandId, value);
				_brandId = value;
			}
		}

		public ICategory Category { get; set; }
		public IBrand Brand { get; set; }
	}
}