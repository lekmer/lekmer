using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class PriceInterval : BusinessObjectBase, IPriceInterval
	{
		private int _id;
		private decimal _from;
		private decimal _to;
		private string _title;
		private ICurrency _currency;
		private int? _contentNodeId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}
		public decimal From
		{
			get { return _from; }
			set
			{
				CheckChanged(_from, value);
				_from = value;
			}
		}
		public decimal To
		{
			get { return _to; }
			set
			{
				CheckChanged(_to, value);
				_to = value;
			}
		}
		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}
		public ICurrency Currency
		{
			get { return _currency; }
			set
			{
				CheckChanged(_currency, value);
				_currency = value;
			}
		}
		public int? ContentNodeId
		{
			get { return _contentNodeId; }
			set
			{
				CheckChanged(_contentNodeId, value);
				_contentNodeId = value;
			}
		}
	}
}