using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class PriceIntervalBand : BusinessObjectBase, IPriceIntervalBand
	{
		private decimal? _from;
		private decimal? _to;

		public decimal? From
		{
			get { return _from; }
			set
			{
				CheckChanged(_from, value);
				_from = value;
			}
		}

		public decimal? To
		{
			get { return _to; }
			set
			{
				CheckChanged(_to, value);
				_to = value;
			}
		}
	}
}