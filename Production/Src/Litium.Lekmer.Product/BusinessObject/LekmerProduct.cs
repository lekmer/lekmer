﻿using System;
using Litium.Scensum.Foundation.Utilities;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class LekmerProduct : Scensum.Product.Product, ILekmerProduct
	{
		private int? _brandId;
		private IBrand _brand;
		private bool _isBookable;
		private DateTime? _isNewFrom;
		private DateTime? _isNewTo;
		private DateTime? _createdDate;
		private string _lekmerErpId;
		private string _urlTitle;
		private bool _hasSizes;
		private int? _parentContentNodeId;
		private bool _showVariantRelations;
		private decimal? _weight;
		private int _productTypeId;
		private bool _isAbove60L;
		private int _stockStatusId;
		private int? _monitorThreshold;
		private int? _maxQuantityPerOrder;
		private int? _deliveryTimeId;
		private IDeliveryTime _deliveryTime;
		private bool _sellOnlyInPackage;
		private bool _isDropShip;

		public int? BrandId
		{
			get { return _brandId; }
			set
			{
				CheckChanged(_brandId, value);
				_brandId = value;
			}
		}
		public IBrand Brand
		{
			get { return _brand; }
			set
			{
				CheckChanged(_brand, value);
				_brand = value;
			}
		}
		public bool IsBookable
		{
			get { return _isBookable; }
			set
			{
				CheckChanged(_isBookable, value);
				_isBookable = value;
			}
		}
		public new bool IsBuyable
		{
			get { return NumberInStock > 0; }
		}
		public bool IsMonitorable
		{
			get { return !IsBookable && NumberInStock == 0; }
		}
		public DateTime? IsNewFrom
		{
			get { return _isNewFrom; }
			set
			{
				CheckChanged(_isNewFrom, value);
				_isNewFrom = value;
			}
		}
		public DateTime? IsNewTo
		{
			get { return _isNewTo; }
			set
			{
				CheckChanged(_isNewTo, value);
				_isNewTo = value;
			}
		}
		public bool IsNewProduct
		{
			get
			{
				return
					(IsNewFrom.HasValue || IsNewTo.HasValue) &&
					(!IsNewFrom.HasValue || DateTime.Now >= IsNewFrom) &&
					(!IsNewTo.HasValue || DateTime.Now <= IsNewTo);
			}
		}
		public DateTime? CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}
		public string LekmerErpId
		{
			get { return _lekmerErpId; }
			set
			{
				CheckChanged(_lekmerErpId, value);
				_lekmerErpId = value;
			}
		}
		public string UrlTitle
		{
			get { return _urlTitle; }
			set
			{
				CheckChanged(_urlTitle, value);
				_urlTitle = value;
			}
		}
		public string LekmerUrl
		{
			get { return PrepareLekmerUrl(false); }
		}
		public string LekmerUrlEncode
		{
			get { return PrepareLekmerUrl(true); }
		}
		public string ParentPageUrl { get; set; }
		public bool HasSizes
		{
			get { return _hasSizes; }
			set
			{
				CheckChanged(_hasSizes, value);
				_hasSizes = value;
			}
		}
		public int? ParentContentNodeId
		{
			get { return _parentContentNodeId; }
			set
			{
				CheckChanged(_parentContentNodeId, value);
				_parentContentNodeId = value;
			}
		}
		public bool ShowVariantRelations
		{
			get { return _showVariantRelations; }
			set
			{
				CheckChanged(_showVariantRelations, value);
				_showVariantRelations = value;
			}
		}
		public decimal? Weight
		{
			get { return _weight; }
			set
			{
				CheckChanged(_weight, value);
				_weight = value;
			}
		}
		public int ProductTypeId
		{
			get { return _productTypeId; }
			set
			{
				CheckChanged(_productTypeId, value);
				_productTypeId = value;
			}
		}
		public bool IsAbove60L
		{
			get { return _isAbove60L; }
			set
			{
				CheckChanged(_isAbove60L, value);
				_isAbove60L = value;
			}
		}
		public int StockStatusId
		{
			get { return _stockStatusId; }
			set
			{
				CheckChanged(_stockStatusId, value);
				_stockStatusId = value;
			}
		}
		public int? MonitorThreshold
		{
			get { return _monitorThreshold; }
			set
			{
				CheckChanged(_monitorThreshold, value);
				_monitorThreshold = value;
			}
		}
		public int? MaxQuantityPerOrder
		{
			get { return _maxQuantityPerOrder; }
			set
			{
				CheckChanged(_maxQuantityPerOrder, value);
				_maxQuantityPerOrder = value;
			}
		}
		public int? DeliveryTimeId
		{
			get { return _deliveryTimeId; }
			set
			{
				CheckChanged(_deliveryTimeId, value);
				_deliveryTimeId = value;
			}
		}
		public IDeliveryTime DeliveryTime
		{
			get { return _deliveryTime; }
			set
			{
				CheckChanged(_deliveryTime, value);
				_deliveryTime = value;
			}
		}
		public bool SellOnlyInPackage
		{
			get { return _sellOnlyInPackage; }
			set
			{
				CheckChanged(_sellOnlyInPackage, value);
				_sellOnlyInPackage = value;
			}
		}
		public bool IsDropShip
		{
			get { return _isDropShip; }
			set
			{
				CheckChanged(_isDropShip, value);
				_isDropShip = value;
			}
		}

		public string EsalesTicket { get; set; }
		public decimal? RecommendedPrice { get; set; }
		public virtual bool IsPriceAffectedByCampaign
		{
			get
			{
				return CampaignInfo != null && CampaignInfo.Price.IncludingVat != Price.PriceIncludingVat;
			}
		}
		public decimal? AbsoluteSave
		{
			get
			{
				if (!RecommendedPrice.HasValue)
					return null;

				decimal price = CampaignInfo != null ? CampaignInfo.Price.IncludingVat : Price.PriceIncludingVat;
				decimal? absoluteSave = price < RecommendedPrice.Value ? RecommendedPrice.Value - price : (decimal?)null;
				return absoluteSave;
			}
		}
		public decimal? PercentageSave
		{
			get
			{
				if (!RecommendedPrice.HasValue || !AbsoluteSave.HasValue)
					return null;

				decimal percentageSave = (AbsoluteSave.Value / RecommendedPrice.Value) * 100;
				return percentageSave;
			}
		}

		protected string PrepareLekmerUrl(bool isNeedEncode)
		{
			string url = Encoder.EncodeUrl(UrlTitle.ToLowerInvariant()).Replace('+', '-');

			string parentPageUrl = ParentPageUrl;
			if (!string.IsNullOrEmpty(parentPageUrl))
			{
				if (parentPageUrl.Length > 1)
				{
					parentPageUrl = parentPageUrl.Substring(2);
				}

				if (isNeedEncode)
				{
					string[] parentPageUrlSplitted = parentPageUrl.Split('/');
					for (int index = 0; index < parentPageUrlSplitted.Length; index++)
					{
						parentPageUrlSplitted[index] = Encoder.EncodeUrl(parentPageUrlSplitted[index]);
					}
					parentPageUrl = string.Join("/", parentPageUrlSplitted);
				}

				url = parentPageUrl + url;
			}

			return url;
		}
	}
}