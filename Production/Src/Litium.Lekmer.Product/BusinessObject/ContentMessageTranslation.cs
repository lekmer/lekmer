﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class ContentMessageTranslation : BusinessObjectBase, IContentMessageTranslation
	{
		private int _contentMessageId;
		private int _languageId;
		private string _message;

		public int ContentMessageId
		{
			get { return _contentMessageId; }
			set
			{
				CheckChanged(_contentMessageId, value);
				_contentMessageId = value;
			}
		}

		public int LanguageId
		{
			get { return _languageId; }
			set
			{
				CheckChanged(_languageId, value);
				_languageId = value;
			}
		}

		public string Message
		{
			get { return _message; }
			set
			{
				CheckChanged(_message, value);
				_message = value;
			}
		}
	}
}