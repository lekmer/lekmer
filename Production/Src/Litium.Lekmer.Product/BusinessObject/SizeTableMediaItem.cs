﻿using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Product
{
	[Serializable]
	public class SizeTableMediaItem : BusinessObjectBase, ISizeTableMediaItem
	{
		private IMediaItem _mediaItem;
		private IImage _image;

		public IMediaItem MediaItem 
		{
			get { return _mediaItem; }
			set
			{
				CheckChanged(_mediaItem, value);
				_mediaItem = value;
			}
		}

		public IImage Image
		{
			get { return _image; }
			set
			{
				CheckChanged(_image, value);
				_image = value;
			}
		}
	}
}