﻿using Litium.Scensum.Core;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Messenger
{
	public class MonitorProductEmailTemplate : Template
	{
		public MonitorProductEmailTemplate(int templateId, IChannel channel) : base(templateId, channel, false)
		{
		}

		public MonitorProductEmailTemplate(IChannel channel) : base("MonitorProductEmail", channel, false)
		{
		}
	}
}