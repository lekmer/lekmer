using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class EsalesDidYouMeanService : IEsalesDidYouMeanService
	{
		protected IEsalesService EsalesService { get; private set; }

		public EsalesDidYouMeanService(IEsalesService esalesService)
		{
			EsalesService = esalesService;
		}

		public Collection<string> FindCorrections(IUserContext context, string text, int numberOfItems)
		{
			EsalesService.EnsureNotNull();

			IDidYouMeanPanelSetting panelSetting = new DidYouMeanPanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(1, numberOfItems); // first X items

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.DidYouMeanPanelName);
			if (panelContent != null)
			{
				var correctionsResult = panelContent.FindResult(EsalesResultType.Corrections) as ICorrectionHits;

				if (correctionsResult != null)
				{
					return new Collection<string>(correctionsResult.CorrectionsInfo.Select(c => c.Text).ToList());
				}
			}

			return new Collection<string>();
		}
	}
}