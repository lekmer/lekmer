using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Esales;
using Litium.Lekmer.Esales.Setting;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class EsalesAutoCompleteService : IEsalesAutoCompleteService
	{
		protected IEsalesService EsalesService { get; private set; }

		public EsalesAutoCompleteService(IEsalesService esalesService)
		{
			EsalesService = esalesService;
		}

		public Collection<string> FindSuggestions(IUserContext context, string text, int numberOfItems)
		{
			EsalesService.EnsureNotNull();

			IAutoCompletePanelSetting panelSetting = new AutoCompletePanelSetting(context.Channel.CommonName);

			var searchRequest = IoC.Resolve<ISearchRequest>();
			searchRequest.PanelPath = panelSetting.PanelPath;
			searchRequest.SearchPhrase = text;
			searchRequest.Paging = new Paging(1, numberOfItems); // first X items

			IEsalesResponse esalesResponse = EsalesService.RetrieveContent(searchRequest);

			IPanelContent panelContent = esalesResponse.FindPanel(panelSetting.AutoCompletePanelName);
			if (panelContent != null)
			{
				var completionsResult = panelContent.FindResult(EsalesResultType.Completions) as ICompletionHits;

				if (completionsResult != null)
				{
					return new Collection<string>(completionsResult.CompletionsInfo.Select(c => c.Text).ToList());
				}
			}

			return new Collection<string>();
		}
	}
}