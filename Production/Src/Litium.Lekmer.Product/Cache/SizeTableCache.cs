﻿using System.Collections.Generic;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product
{
	public sealed class SizeTableCache : ScensumCacheBase<SizeTableCacheKey, ISizeTable>
	{
		private static readonly SizeTableCache _instance = new SizeTableCache();

		private SizeTableCache()
		{
		}

		public static SizeTableCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int sizeTableId, IEnumerable<IChannel> channels)
		{
			foreach (IChannel channel in channels)
			{
				Remove(new SizeTableCacheKey(channel.Id, sizeTableId));
			}
		}
	}

	public class SizeTableCacheKey : ICacheKey
	{
		public SizeTableCacheKey(int channelId, int sizeTableId)
		{
			ChannelId = channelId;
			SizeTableId = sizeTableId;
		}

		public int ChannelId { get; set; }
		public int SizeTableId { get; set; }

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + SizeTableId.ToString(CultureInfo.InvariantCulture); }
		}
	}

	public sealed class ProductSizeTableIdCache : ScensumCacheBase<ProductSizeTableIdCacheKey, object>
	{
		private static readonly ProductSizeTableIdCache _instance = new ProductSizeTableIdCache();

		private ProductSizeTableIdCache()
		{
		}

		public static ProductSizeTableIdCache Instance
		{
			get { return _instance; }
		}
	}

	public class ProductSizeTableIdCacheKey : ICacheKey
	{
		public ProductSizeTableIdCacheKey(int productId)
		{
			ProductId = productId;
		}

		public int ProductId { get; set; }

		public string Key
		{
			get { return ProductId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}