﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductUrlHistoryCache : ScensumCacheBase<ProductUrlHistoryKey, Collection<IProductUrlHistory>>
	{
		private static readonly ProductUrlHistoryCache _instance = new ProductUrlHistoryCache();

		private ProductUrlHistoryCache()
		{
		}

		public static ProductUrlHistoryCache Instance
		{
			get { return _instance; }
		}
	}

	public class ProductUrlHistoryKey : ICacheKey
	{
		public ProductUrlHistoryKey(int languageId, string urlTitle)
		{
			LanguageId = languageId;
			UrlTitle = urlTitle;
		}

		public int LanguageId { get; set; }
		public string UrlTitle { get; set; }

		public string Key
		{
			get { return LanguageId.ToString(CultureInfo.InvariantCulture) + "-" + UrlTitle; }
		}
	}
}