using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class TagGroupTagCache : ScensumCacheBase<TagGroupTagKey, Collection<ITag>>
	{
		#region Singleton

		private static readonly TagGroupTagCache _instance = new TagGroupTagCache();

		private TagGroupTagCache()
		{
		}

		public static TagGroupTagCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class TagGroupTagKey : ICacheKey
	{
		public int ChannelId { get; set; }
		public int TagGroupId { get; set; }

		public TagGroupTagKey(int channelId, int tagGroupId)
		{
			ChannelId = channelId;
			TagGroupId = tagGroupId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + TagGroupId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}