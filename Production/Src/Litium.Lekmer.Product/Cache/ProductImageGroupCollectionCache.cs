﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductImageGroupCollectionCache<T> : ScensumCacheBase<ProductImageGroupCollectionKey, Collection<T>> where T : IProductImageGroup
	{
		// Singleton

		private static readonly ProductImageGroupCollectionCache<T> _instance = new ProductImageGroupCollectionCache<T>();

		private ProductImageGroupCollectionCache()
		{
		}

		public static ProductImageGroupCollectionCache<T> Instance
		{
			get { return _instance; }
		}
	}

	public class ProductImageGroupCollectionKey : ICacheKey
	{
		public virtual string Key
		{
			get { return "ProductImageGroupCollection"; }
		}
	}

	public class ProductImageGroupFullWithoutProductImagesCollectionKey : ProductImageGroupCollectionKey
	{
		public override string Key
		{
			get { return "ProductImageGroupFullCollection"; }
		}
	}

	public class ProductImageGroupFullCollectionKey : ProductImageGroupCollectionKey
	{
		public int ProductId { get; set; }

		public ProductImageGroupFullCollectionKey(int productId)
		{
			ProductId = productId;
		}

		public override string Key
		{
			get { return "p" + ProductId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}