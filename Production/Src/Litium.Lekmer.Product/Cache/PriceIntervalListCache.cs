using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using System.Globalization;

namespace Litium.Lekmer.Product.Cache
{
	public class PriceIntervalListCache : ScensumCacheBase<PriceIntervalListKey, Collection<IPriceInterval>>
	{
		private static readonly PriceIntervalListCache _priceIntervalListCache = new PriceIntervalListCache();

		private PriceIntervalListCache()
		{
		}

		public static PriceIntervalListCache Instance
		{
			get { return _priceIntervalListCache; }
		}
	}

	public class PriceIntervalListKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public PriceIntervalListKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}