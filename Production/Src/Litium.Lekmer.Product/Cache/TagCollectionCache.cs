using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class TagCollectionCache : ScensumCacheBase<TagCollectionKey, Collection<ITag>>
	{
		#region Singleton

		private static readonly TagCollectionCache _instance = new TagCollectionCache();

		private TagCollectionCache()
		{
		}

		public static TagCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class TagCollectionKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public TagCollectionKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}