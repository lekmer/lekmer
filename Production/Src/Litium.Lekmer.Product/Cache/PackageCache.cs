﻿using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class PackageCache : ScensumCacheBase<PackageKey, IPackage>
	{
		// Singleton

		private static readonly PackageCache _instance = new PackageCache();

		private PackageCache()
		{
		}

		public static PackageCache Instance
		{
			get { return _instance; }
		}

		// End of Singleton

		public void Remove(int masterProductId)
		{
			var channels = IoC.Resolve<IChannelSecureService>().GetAll();
			foreach (var channel in channels)
			{
				Remove(new PackageKey(channel.Id, masterProductId));
			}
		}
	}

	public class PackageKey : ICacheKey
	{
		public PackageKey(int channelId, int masterProductId)
		{
			ChannelId = channelId;
			MasterProductId = masterProductId;
		}

		public int ChannelId { get; set; }

		public int MasterProductId { get; set; }

		public string Key
		{
			get { return string.Format("c_{0}-id_{1}", ChannelId, MasterProductId); }
		}
	}


	public sealed class PackageProductRelationCache : ScensumCacheBase<PackageProductRelationKey, ProductIdCollection>
	{
		// Singleton

		private static readonly PackageProductRelationCache _instance = new PackageProductRelationCache();

		private PackageProductRelationCache()
		{
		}

		public static PackageProductRelationCache Instance
		{
			get { return _instance; }
		}

		// End of Singleton
	}

	public class PackageProductRelationKey : ICacheKey
	{
		public PackageProductRelationKey(int productId)
		{
			ProductId = productId;
		}

		public int ProductId { get; set; }

		public string Key
		{
			get { return string.Format("PackageProductRelation_{0}", ProductId); }
		}
	}
}