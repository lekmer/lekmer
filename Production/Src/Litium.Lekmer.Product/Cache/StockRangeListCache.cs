using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public class StockRangeListCache : ScensumCacheBase<StockRangeListKey, Collection<IStockRange>>
	{
		private static readonly StockRangeListCache _stockRangeListCache = new StockRangeListCache();

		private StockRangeListCache()
		{
		}

		public static StockRangeListCache Instance
		{
			get { return _stockRangeListCache; }
		}
	}

	public class StockRangeListKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public StockRangeListKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}