﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product
{
	public sealed class BlockPackageProductListCache : ScensumCacheBase<BlockPackageProductListKey, IBlockPackageProductList>
	{
		private static readonly BlockPackageProductListCache _instance = new BlockPackageProductListCache();

		private BlockPackageProductListCache()
		{
		}

		public static BlockPackageProductListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockPackageProductListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockPackageProductListKey : ICacheKey
	{
		public BlockPackageProductListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}