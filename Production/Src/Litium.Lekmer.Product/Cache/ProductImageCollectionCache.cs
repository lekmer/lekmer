﻿using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductImageCollectionCache : ScensumCacheBase<ProductImageCollectionKey, Collection<IProductImage>>
	{
		// Singleton

		private static readonly ProductImageCollectionCache _instance = new ProductImageCollectionCache();

		private ProductImageCollectionCache()
		{
		}

		public static ProductImageCollectionCache Instance
		{
			get { return _instance; }
		}
	}

	public class ProductImageCollectionKey : ICacheKey
	{
		public int ProductId { get; set; }

		public ProductImageCollectionKey(int productId)
		{
			ProductId = productId;
		}

		public string Key
		{
			get { return "p" + ProductId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}