using System.Collections.ObjectModel;
using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Product.Cache
{
	public sealed class ProductTypeCollectionCache : ScensumCacheBase<ProductTypeCollectionKey, Collection<IProductType>>
	{
		#region Singleton

		private static readonly ProductTypeCollectionCache _instance = new ProductTypeCollectionCache();

		private ProductTypeCollectionCache()
		{
		}

		public static ProductTypeCollectionCache Instance
		{
			get { return _instance; }
		}

		#endregion
	}

	public class ProductTypeCollectionKey : ICacheKey
	{
		public int ChannelId { get; set; }

		public ProductTypeCollectionKey(int channelId)
		{
			ChannelId = channelId;
		}

		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}