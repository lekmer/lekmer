﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class WishListPackageItemDataMapper : DataMapperBase<IWishListPackageItem>
	{
		public WishListPackageItemDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IWishListPackageItem Create()
		{
			var wishListPackageItem = IoC.Resolve<IWishListPackageItem>();
			wishListPackageItem.Id = MapValue<int>("Id");
			wishListPackageItem.WishListItemId = MapValue<int>("WishListItemId");
			wishListPackageItem.ProductId = MapValue<int>("ProductId");
			wishListPackageItem.SizeId = MapNullableValue<int?>("SizeId");
			wishListPackageItem.Status = BusinessObjectStatus.Untouched;
			return wishListPackageItem;
		}
	}
}