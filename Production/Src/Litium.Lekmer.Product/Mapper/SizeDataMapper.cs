using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeDataMapper : DataMapperBase<ISize>
	{
		public SizeDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ISize Create()
		{
			var size = IoC.Resolve<ISize>();

			size.Id = MapValue<int>("Size.Id");
			size.ErpId = MapValue<string>("Size.ErpId");
			size.ErpTitle = MapNullableValue<string>("Size.ErpTitle");
			size.Eu = MapValue<decimal>("Size.EU");
			size.EuTitle = MapNullableValue<string>("Size.EUTitle");
			size.UsMale = MapValue<decimal>("Size.USMale");
			size.UsFemale = MapValue<decimal>("Size.USFemale");
			size.UkMale = MapValue<decimal>("Size.UKMale");
			size.UkFemale = MapValue<decimal>("Size.UKFemale");
			size.Millimeter = MapValue<int>("Size.Millimeter");
			size.Ordinal = MapValue<int>("Size.Ordinal");

			return size;
		}
	}
}