﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeTableDataMapper : DataMapperBase<ISizeTable>
	{
		public SizeTableDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISizeTable Create()
		{
			var sizeTable = IoC.Resolve<ISizeTable>();
			sizeTable.Id = MapValue<int>("SizeTable.SizeTableId");
			sizeTable.SizeTableFolderId = MapValue<int>("SizeTable.SizeTableFolderId");
			sizeTable.CommonName = MapValue<string>("SizeTable.CommonName");
			sizeTable.Title = MapValue<string>("SizeTable.Title");
			sizeTable.Description = MapNullableValue<string>("SizeTable.Description");
			sizeTable.Column1Title = MapValue<string>("SizeTable.Column1Title");
			sizeTable.Column2Title = MapValue<string>("SizeTable.Column2Title");
			sizeTable.SetUntouched();
			return sizeTable;
		}
	}
}