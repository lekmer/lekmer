﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class EntityTimeLimitDataMapper<T> : DataMapperBase<T>
		where T : class, IEntityTimeLimit
	{
		public EntityTimeLimitDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override T Create()
		{
			var entityTimeLimit = IoC.Resolve<T>();
			entityTimeLimit.StartDate = MapNullableValue<DateTime?>("EntityTimeLimit.StartDate");
			entityTimeLimit.EndDate = MapNullableValue<DateTime?>("EntityTimeLimit.EndDate");
			var startMinutes = MapNullableValue<int?>("EntityTimeLimit.StartDailyIntervalMinutes");
			entityTimeLimit.StartDailyInterval = startMinutes.HasValue ? (TimeSpan?)TimeSpan.FromMinutes(startMinutes.Value) : null;
			var endMinutes = MapNullableValue<int?>("EntityTimeLimit.EndDailyIntervalMinutes");
			entityTimeLimit.EndDailyInterval = endMinutes.HasValue ? (TimeSpan?)TimeSpan.FromMinutes(endMinutes.Value) : null;
			return entityTimeLimit;
		}
	}
}