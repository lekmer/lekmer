using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class TagDataMapper : DataMapperBase<ITag>
	{
		public TagDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ITag Create()
		{
			var tag = IoC.Resolve<ITag>();
			tag.Id = MapValue<int>("Tag.TagId");
			tag.TagGroupId = MapValue<int>("Tag.TagGroupId");
			tag.Value = MapValue<string>("Tag.Value");
			tag.CommonName = MapValue<string>("Tag.CommonName");
			tag.FlagId = MapNullableValue<int?>("Tag.FlagId");
			return tag;
		}
	}
}