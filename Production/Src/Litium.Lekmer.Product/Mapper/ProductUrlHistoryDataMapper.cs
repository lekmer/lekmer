﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class ProductUrlHistoryDataMapper : DataMapperBase<IProductUrlHistory>
	{
		public ProductUrlHistoryDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IProductUrlHistory Create()
		{
			var productUrlHistory = IoC.Resolve<IProductUrlHistory>();

			productUrlHistory.Id = MapValue<int>("ProductUrlHistory.Id");
			productUrlHistory.ProductId = MapValue<int>("ProductUrlHistory.ProductId");
			productUrlHistory.LanguageId = MapValue<int>("ProductUrlHistory.LanguageId");
			productUrlHistory.UrlTitleOld = MapValue<string>("ProductUrlHistory.UrlTitleOld");
			productUrlHistory.CreationDate = MapValue<DateTime>("ProductUrlHistory.CreationDate");
			productUrlHistory.LastUsageDate = MapValue<DateTime>("ProductUrlHistory.LastUsageDate");
			productUrlHistory.UsageAmount = MapValue<int>("ProductUrlHistory.UsageAmount");
			productUrlHistory.TypeId = MapValue<int>("ProductUrlHistory.HistoryLifeIntervalTypeId");

			productUrlHistory.SetUntouched();

			return productUrlHistory;
		}
	}
}