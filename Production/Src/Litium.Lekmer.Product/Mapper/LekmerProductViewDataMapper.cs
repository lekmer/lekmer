﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
	internal class LekmerProductViewDataMapper : ProductViewDataMapper
	{
		private DataMapperBase<IBatteryType> _batteryTypeDataMapper;
		private DataMapperBase<ISizeDeviation> _sizeDeviationDataMapper;
		private DataMapperBase<IDeliveryTime> _deliveryTimeDataMapper;

		public LekmerProductViewDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_batteryTypeDataMapper = DataMapperResolver.Resolve<IBatteryType>(DataReader);
			_sizeDeviationDataMapper = DataMapperResolver.Resolve<ISizeDeviation>(DataReader);
			_deliveryTimeDataMapper = DataMapperResolver.Resolve<IDeliveryTime>(DataReader);
		}

		protected override IProductView Create()
		{
			var product = (ILekmerProductView) base.Create();

			//ILekmerProduct
			product.BrandId = MapNullableValue<int?>("Lekmer.BrandId");
			product.IsBookable = MapValue<bool>("Lekmer.IsBookable");
			product.IsNewFrom = MapNullableValue<DateTime?>("Lekmer.IsNewFrom");
			product.IsNewTo = MapNullableValue<DateTime?>("Lekmer.IsNewTo");
			product.CreatedDate = MapNullableValue<DateTime?>("Lekmer.CreatedDate");
			product.LekmerErpId = MapNullableValue<string>("Lekmer.LekmerErpId");
			product.UrlTitle = MapValue<string>("Lekmer.UrlTitle");
			product.HasSizes = MapValue<bool>("Lekmer.HasSizes");
			product.ShowVariantRelations = MapValue<bool>("Lekmer.ShowVariantRelations");
			product.Weight = MapNullableValue<decimal?>("Lekmer.Weight");
			product.ProductTypeId = MapValue<int>("Lekmer.ProductTypeId");
			product.IsAbove60L = MapValue<bool>("Lekmer.IsAbove60L");
			product.StockStatusId = MapValue<int>("Lekmer.StockStatusId");
			product.MonitorThreshold = MapNullableValue<int?>("Lekmer.MonitorThreshold");
			product.MaxQuantityPerOrder = MapNullableValue<int?>("Lekmer.MaxQuantityPerOrder");
			product.DeliveryTimeId = MapNullableValue<int?>("Lekmer.DeliveryTimeId");
			product.DeliveryTime = product.DeliveryTimeId.HasValue ? _deliveryTimeDataMapper.MapRow() : null;
			product.RecommendedPrice = MapNullableValue<decimal?>("Lekmer.RecommendedPrice");
			product.SellOnlyInPackage = MapValue<bool>("Lekmer.SellOnlyInPackage");
			product.IsDropShip = MapValue<bool>("Lekmer.IsDropShip");

			//ILekmerProductView
			product.AgeFromMonth = MapValue<int>("Lekmer.AgeFromMonth");
			product.AgeToMonth = MapValue<int>("Lekmer.AgeToMonth");
			product.Measurement = MapNullableValue<string>("Lekmer.Measurement");
			var batteryTypeId = MapNullableValue<int?>("Lekmer.BatteryTypeId");
			product.BatteryType = batteryTypeId.HasValue ? _batteryTypeDataMapper.MapRow() : null;
			product.NumberOfBatteries = MapNullableValue<int?>("Lekmer.NumberOfBatteries");
			product.IsBatteryIncluded = MapValue<bool>("Lekmer.IsBatteryIncluded");
			product.ExpectedBackInStock = MapNullableValue<DateTime?>("Lekmer.ExpectedBackInStock");
			var sizeDeviationId = MapNullableValue<int?>("Lekmer.SizeDeviationId");
			product.SizeDeviation = sizeDeviationId.HasValue ? _sizeDeviationDataMapper.MapRow() : null;

			return product;
		}
	}
}