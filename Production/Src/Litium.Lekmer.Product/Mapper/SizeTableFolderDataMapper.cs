﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeTableFolderDataMapper : DataMapperBase<ISizeTableFolder>
	{
		public SizeTableFolderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISizeTableFolder Create()
		{
			var sizeTableFolder = IoC.Resolve<ISizeTableFolder>();

			sizeTableFolder.Id = MapValue<int>("SizeTableFolder.SizeTableFolderId");
			sizeTableFolder.ParentFolderId = MapNullableValue<int?>("SizeTableFolder.ParentSizeTableFolderId");
			sizeTableFolder.Title = MapValue<string>("SizeTableFolder.Title");

			sizeTableFolder.SetUntouched();

			return sizeTableFolder;
		}
	}
}