﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockPackageProductListDataMapper : DataMapperBase<IBlockPackageProductList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockPackageProductListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockPackageProductList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockPackageProductList = IoC.Resolve<IBlockPackageProductList>();

			block.ConvertTo(blockPackageProductList);
			blockPackageProductList.Setting = _blockSettingDataMapper.MapRow();
			blockPackageProductList.SetUntouched();

			return blockPackageProductList;
		}
	}
}