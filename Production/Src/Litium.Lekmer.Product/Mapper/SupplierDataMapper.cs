﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SupplierDataMapper : DataMapperBase<ISupplier>
	{
		public SupplierDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISupplier Create()
		{
			var supplier = IoC.Resolve<ISupplier>();
			supplier.Id = MapValue<int>("Supplier.Id");
			supplier.SupplierNo = MapValue<string>("Supplier.SupplierNo");
			supplier.ActivePassiveCode = MapValue<string>("Supplier.ActivePassiveCode");
			supplier.Name = MapValue<string>("Supplier.Name");
			supplier.Address = MapNullableValue<string>("Supplier.Address");
			supplier.ZipArea = MapNullableValue<string>("Supplier.ZipArea");
			supplier.City = MapNullableValue<string>("Supplier.City");
			supplier.CountryIso = MapNullableValue<string>("Supplier.CountryIso");
			supplier.LanguageIso = MapNullableValue<string>("Supplier.LanguageIso");
			supplier.VatRate = MapNullableValue<string>("Supplier.VatRate");
			supplier.Phone1 = MapNullableValue<string>("Supplier.Phone1");
			supplier.Phone2 = MapNullableValue<string>("Supplier.Phone2");
			supplier.Email = MapNullableValue<string>("Supplier.Email");
			supplier.IsDropShip = MapNullableValue<bool>("Supplier.IsDropShip");
			supplier.DropShipEmail = MapNullableValue<string>("Supplier.DropShipEmail");
			supplier.SetUntouched();
			return supplier;
		}
	}
}