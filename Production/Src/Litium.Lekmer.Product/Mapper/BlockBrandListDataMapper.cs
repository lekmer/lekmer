﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockBrandListDataMapper : DataMapperBase<IBlockBrandList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		private DataMapperBase<IBlockSetting> _blockSettingDataMapper;

		public BlockBrandListDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
			_blockSettingDataMapper = DataMapperResolver.Resolve<IBlockSetting>(DataReader);
		}

		protected override IBlockBrandList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockBrandList = IoC.Resolve<IBlockBrandList>();
			block.ConvertTo(blockBrandList);
			blockBrandList.IncludeAllBrands = MapValue<bool>("BlockBrandList.IncludeAllBrands");
			blockBrandList.LinkContentNodeId = MapNullableValue<int?>("BlockBrandList.LinkContentNodeId");
			blockBrandList.Setting = _blockSettingDataMapper.MapRow();
			return blockBrandList;
		}
	}
}