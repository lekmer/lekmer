using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class TagGroupDataMapper : DataMapperBase<ITagGroup>
	{
		public TagGroupDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ITagGroup Create()
		{
			var tagGroup = IoC.Resolve<ITagGroup>();
			tagGroup.Id = MapValue<int>("TagGroup.TagGroupId");
			tagGroup.Title = MapValue<string>("TagGroup.Title");
			tagGroup.CommonName = MapValue<string>("TagGroup.CommonName");
			return tagGroup;
		}
	}
}