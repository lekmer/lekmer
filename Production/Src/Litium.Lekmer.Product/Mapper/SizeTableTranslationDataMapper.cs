﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product.Mapper
{
	public class SizeTableTranslationDataMapper : DataMapperBase<ISizeTableTranslation>
	{
		public SizeTableTranslationDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ISizeTableTranslation Create()
		{
			var sizeTableTranslation = IoC.Resolve<ISizeTableTranslation>();
			sizeTableTranslation.SizeTableId = MapValue<int>("SizeTableTranslation.SizeTableId");
			sizeTableTranslation.LanguageId = MapValue<int>("SizeTableTranslation.LanguageId");
			sizeTableTranslation.Title = MapNullableValue<string>("SizeTableTranslation.Title");
			sizeTableTranslation.Description = MapNullableValue<string>("SizeTableTranslation.Description");
			sizeTableTranslation.Column1Title = MapNullableValue<string>("SizeTableTranslation.Column1Title");
			sizeTableTranslation.Column2Title = MapNullableValue<string>("SizeTableTranslation.Column2Title");
			sizeTableTranslation.SetUntouched();
			return sizeTableTranslation;
		}
	}
}