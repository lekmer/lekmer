﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
	public class LekmerProductRecordDataMapper : ProductRecordDataMapper
	{
		private DataMapperBase<IDeliveryTime> _deliveryTimeDataMapper;

		public LekmerProductRecordDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_deliveryTimeDataMapper = DataMapperResolver.Resolve<IDeliveryTime>(DataReader);
		}

		protected override IProductRecord Create()
		{
			var product = (ILekmerProductRecord) base.Create();

			product.BrandId = MapNullableValue<int?>("Lekmer.BrandId");
			product.IsBookable = MapValue<bool>("Lekmer.IsBookable");
			product.AgeFromMonth = MapValue<int>("Lekmer.AgeFromMonth");
			product.AgeToMonth = MapValue<int>("Lekmer.AgeToMonth");
			product.IsNewFrom = MapNullableValue<DateTime?>("Lekmer.IsNewFrom");
			product.IsNewTo = MapNullableValue<DateTime?>("Lekmer.IsNewTo");
			product.Measurement = MapNullableValue<string>("Lekmer.Measurement");
			product.BatteryTypeId = MapNullableValue<int?>("Lekmer.BatteryTypeId");
			product.NumberOfBatteries = MapNullableValue<int?>("Lekmer.NumberOfBatteries");
			product.IsBatteryIncluded = MapValue<bool>("Lekmer.IsBatteryIncluded");
			product.ExpectedBackInStock = MapNullableValue<DateTime?>("Lekmer.ExpectedBackInStock");
			product.CreatedDate = MapNullableValue<DateTime?>("Lekmer.CreatedDate");
			product.SizeDeviationId = MapNullableValue<int?>("Lekmer.SizeDeviationId");
			product.LekmerErpId = MapNullableValue<string>("Lekmer.LekmerErpId");
			product.ShowVariantRelations = MapValue<bool>("Lekmer.ShowVariantRelations");
			product.Weight = MapNullableValue<decimal?>("Lekmer.Weight");
			product.ProductTypeId = MapValue<int>("Lekmer.ProductTypeId");
			product.IsAbove60L = MapValue<bool>("Lekmer.IsAbove60L");
			product.StockStatusId = MapValue<int>("Lekmer.StockStatusId");
			product.MonitorThreshold = MapNullableValue<int?>("Lekmer.MonitorThreshold");
			product.MaxQuantityPerOrder = MapNullableValue<int?>("Lekmer.MaxQuantityPerOrder");
			product.DeliveryTimeId = MapNullableValue<int?>("Lekmer.DeliveryTimeId");
			product.DeliveryTime = product.DeliveryTimeId.HasValue ? _deliveryTimeDataMapper.MapRow() : null;
			product.SellOnlyInPackage = MapValue<bool>("Lekmer.SellOnlyInPackage");
			product.SupplierId = MapNullableValue<string>("Lekmer.SupplierId");
			product.PurchasePrice = MapNullableValue<decimal?>("Lekmer.PurchasePrice");
			product.PurchaseCurrencyId = MapNullableValue<int?>("Lekmer.PurchaseCurrencyId");
			product.SupplierArticleNumber = MapNullableValue<string>("Lekmer.SupplierArticleNumber");
			product.IsDropShip = MapValue<bool>("Lekmer.IsDropShip");
			product.AveragePrice = MapNullableValue<decimal?>("Lekmer.AveragePrice");

			return product;
		}
	}
}