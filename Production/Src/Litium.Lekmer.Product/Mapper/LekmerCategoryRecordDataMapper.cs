﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Mapper;

namespace Litium.Lekmer.Product.Mapper
{
	public class LekmerCategoryRecordDataMapper : CategoryDataMapper<ICategoryRecord>
	{
		private DataMapperBase<IDeliveryTime> _deliveryTimeDataMapper;

		public LekmerCategoryRecordDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_deliveryTimeDataMapper = DataMapperResolver.Resolve<IDeliveryTime>(DataReader);
		}

		protected override ICategoryRecord Create()
		{
			var category = (ILekmerCategoryRecord)base.Create();
			category.AllowMultipleSizesPurchase = MapNullableValue<bool?>("LekmerCategory.AllowMultipleSizesPurchase");
			category.PackageInfo = MapNullableValue<string>("LekmerCategory.PackageInfo");
			category.MonitorThreshold = MapNullableValue<int?>("LekmerCategory.MonitorThreshold");
			category.MaxQuantityPerOrder = MapNullableValue<int?>("LekmerCategory.MaxQuantityPerOrder");
			category.DeliveryTimeId = MapNullableValue<int?>("LekmerCategory.DeliveryTimeId");
			if (category.DeliveryTimeId.HasValue)
			{
				category.DeliveryTime = _deliveryTimeDataMapper.MapRow();
			}
			return category;
		}
	}
}