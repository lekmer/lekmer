﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product.Mapper
{
	public class BlockLatestProductAddedToBasketDataMapper : DataMapperBase<IBlockLatestProductAddedToBasket>
	{
		private DataMapperBase<IBlock> _blockDataMapper;
		public BlockLatestProductAddedToBasketDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}
		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}
		protected override IBlockLatestProductAddedToBasket Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockBlockLatestProductAddedToBasket = IoC.Resolve<IBlockLatestProductAddedToBasket>();
			block.ConvertTo(blockBlockLatestProductAddedToBasket);
			blockBlockLatestProductAddedToBasket.RedirectToCategory = MapNullableValue<bool>("RedirectToCategory");
			blockBlockLatestProductAddedToBasket.RedirectToMainCategory = MapNullableValue<bool>("RedirectToMainCategory");
			blockBlockLatestProductAddedToBasket.RedirectToStartPage = MapNullableValue<bool>("RedirectToStartPage");
			blockBlockLatestProductAddedToBasket.Status = BusinessObjectStatus.Untouched;
			return blockBlockLatestProductAddedToBasket;
		}
	}
}