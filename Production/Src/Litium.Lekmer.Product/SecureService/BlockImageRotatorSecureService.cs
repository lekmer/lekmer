﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockImageRotatorSecureService : IBlockImageRotatorSecureService, IBlockCreateSecureService,
											   IBlockDeleteSecureService
	{
		protected BlockImageRotatorRepository Repository { get; private set; }
		protected IImageRotatorGroupSecureService ImageRotatorGroupSecureService { get; private set; }

		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		public BlockImageRotatorSecureService(IAccessSecureService accessSecureService, BlockImageRotatorRepository repository, IBlockSecureService blockSecureService, IImageRotatorGroupSecureService iconSecureService)
		{
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			ImageRotatorGroupSecureService = iconSecureService;
		}

		public IBlockImageRotator GetById(int blockId)
		{
			return Repository.GetByIdSecure(blockId);
		}

		public void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			Repository.Delete(blockId);
			BlockBrandListBrandCollectionCache.Instance.Flush();
			BlockBrandListCache.Instance.Remove(blockId);
		}

		public int Save(ISystemUserFull systemUserFull, IBlockImageRotator block, Collection<IImageRotatorGroup> imageRotatorGroups)
		{
			if (block == null) throw new ArgumentNullException("block");
			using (var transaction = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;
				Repository.Save(block);
				ImageRotatorGroupSecureService.Save(systemUserFull, block.Id, imageRotatorGroups);
				transaction.Complete();
			}

			BlockImageRotatorCollectionCache.Instance.Flush();
			BlockImageRotatorCache.Instance.Remove(block.Id);
			return block.Id;
		}

		public IBlockImageRotator Create()
		{
			var blockImageRotator = IoC.Resolve<IBlockImageRotator>();
			blockImageRotator.SetNew();
			blockImageRotator.AccessId = AccessSecureService.GetByCommonName("All").Id;
			return blockImageRotator;
		}

		public IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			var blockBrandList = Create();
			blockBrandList.ContentNodeId = contentNodeId;
			blockBrandList.ContentAreaId = contentAreaId;
			blockBrandList.BlockTypeId = blockTypeId;
			blockBrandList.BlockStatusId = (int)BlockStatusInfo.Offline;
			blockBrandList.Title = title;
			blockBrandList.TemplateId = null;
			blockBrandList.Id = Save(systemUserFull, blockBrandList, new Collection<IImageRotatorGroup>());
			return blockBrandList;
		}
	}
}