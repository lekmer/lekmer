﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class SizeDeviationSecureService : ISizeDeviationSecureService
	{
		protected SizeDeviationRepository Repository { get; private set; }

		public SizeDeviationSecureService(SizeDeviationRepository repository)
		{
			Repository = repository;
		}

		public Collection<ISizeDeviation> GetAll()
		{
			return Repository.GetAll();
		}
	}
}
