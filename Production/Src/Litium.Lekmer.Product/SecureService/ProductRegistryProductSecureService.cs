﻿using System.Collections.Generic;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Product
{
	public class ProductRegistryProductSecureService : IProductRegistryProductSecureService
	{
		private const char DELIMITER = ',';

		protected ProductRegistryProductRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public ProductRegistryProductSecureService(ProductRegistryProductRepository repository, IAccessValidator accessValidator)
		{
			Repository = repository;
			AccessValidator = accessValidator;
		}

		public void Insert(ISystemUserFull systemUserFull, int? productId, int? brandId, int? categoryId, List<int> productRegistryIds)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);

			Repository.Insert(productId, brandId, categoryId, Scensum.Foundation.Convert.ToStringIdentifierList(productRegistryIds), DELIMITER);
		}

		public void DeleteByProduct(ISystemUserFull systemUserFull, int productId, List<int> productRegistryIds)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);

			Repository.DeleteByProduct(productId, Scensum.Foundation.Convert.ToStringIdentifierList(productRegistryIds), DELIMITER);
		}

		public void DeleteByBrand(ISystemUserFull systemUserFull, int brandId, List<int> productRegistryIds)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);

			Repository.DeleteByBrand(brandId, Scensum.Foundation.Convert.ToStringIdentifierList(productRegistryIds), DELIMITER);
		}

		public void DeleteByCategory(ISystemUserFull systemUserFull, int categoryId, List<int> productRegistryIds)
		{
			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentBrand);

			Repository.DeleteByCategory(categoryId, Scensum.Foundation.Convert.ToStringIdentifierList(productRegistryIds), DELIMITER);
		}

		public virtual List<int> GetRestrictionsByProductId(int productId)
		{
			return Repository.GetRestrictionsByProductId(productId);
		}
	}
}