﻿using Litium.Lekmer.Product.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class LekmerProductSeoSettingSecureService : ProductSeoSettingSecureService
	{
		public LekmerProductSeoSettingSecureService(ProductSeoSettingRepository productSeoSettingRepository)
			: base(productSeoSettingRepository)
		{
		}

		public override void Save(ISystemUserFull systemUserFull, IProductSeoSetting productSeoSetting)
		{
			base.Save(systemUserFull, productSeoSetting);

			ProductSeoSettingCache.Instance.Remove(productSeoSetting.Id);
		}
	}
}