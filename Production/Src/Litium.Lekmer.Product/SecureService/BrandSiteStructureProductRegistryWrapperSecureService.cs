using System.Collections.ObjectModel;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Product
{
	public class BrandSiteStructureProductRegistryWrapperSecureService : IBrandSiteStructureProductRegistryWrapperSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BrandSiteStructureProductRegistryWrapperRepository Repository { get; private set; }

		public BrandSiteStructureProductRegistryWrapperSecureService(IAccessValidator accessValidator, BrandSiteStructureProductRegistryWrapperRepository repository)
		{
			AccessValidator = accessValidator;
			Repository = repository;
		}

		public virtual IBrandSiteStructureProductRegistryWrapper Create()
		{
			var brandSiteStructureProductRegistryWrapper = IoC.Resolve<IBrandSiteStructureProductRegistryWrapper>();
			brandSiteStructureProductRegistryWrapper.Status = BusinessObjectStatus.New;
			return brandSiteStructureProductRegistryWrapper;
		}

		public virtual Collection<IBrandSiteStructureProductRegistryWrapper> GetAllByBrand(int brandId)
		{
			return Repository.GetAllByBrand(brandId);
		}
	}
}