﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Product
{
	public class BlockCategoryProductListSecureService : IBlockCategoryProductListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected IAccessSecureService AccessService { get; private set; }
		protected BlockCategoryProductListRepository Repository { get; private set; }
		protected IBlockSecureService BlockService { get; private set; }
		protected IBlockCategoryProductListCategorySecureService CategoryBlockCategoryService { get; private set; }
		protected IBlockSettingSecureService BlockSettingSecureService { get; private set; }

		public BlockCategoryProductListSecureService(
			IAccessValidator accessValidator,
			IAccessSecureService accessSecureService,
			BlockCategoryProductListRepository repository,
			IBlockSecureService blockSecureService,
			IBlockCategoryProductListCategorySecureService categoryBlockCategoryService,
			IBlockSettingSecureService blockSettingSecureService)
		{
			AccessValidator = accessValidator;
			AccessService = accessSecureService;
			Repository = repository;
			BlockService = blockSecureService;
			CategoryBlockCategoryService = categoryBlockCategoryService;
			BlockSettingSecureService = blockSettingSecureService;
		}

		public virtual IBlockCategoryProductList Create()
		{
			if (AccessService == null) throw new InvalidOperationException("AccessService must be set before calling Create.");

			var block = IoC.Resolve<IBlockCategoryProductList>();
			block.AccessId = AccessService.GetByCommonName("All").Id;
			block.Setting = BlockSettingSecureService.Create();
			block.Status = BusinessObjectStatus.New;
			return block;
		}

		public virtual int Save(
			ISystemUserFull systemUserFull,
			IBlockCategoryProductList block,
			Collection<IBlockCategoryProductListCategory> blockCategories)
		{
			if (block == null) throw new ArgumentNullException("block");
			if (blockCategories == null) throw new ArgumentNullException("blockCategories");

			BlockService.EnsureNotNull();
			CategoryBlockCategoryService.EnsureNotNull();
			BlockSettingSecureService.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);

			using (var transaction = new TransactedOperation())
			{
				block.Id = BlockService.Save(systemUserFull, block);
				if (block.Id == -1)
					return block.Id;

				Repository.Save(block);
				CategoryBlockCategoryService.Save(block.Id, blockCategories);
				block.Setting.BlockId = block.Id;
				BlockSettingSecureService.Save(block.Setting);

				transaction.Complete();
			}
			BlockCategoryProductCollectionCache.Instance.Flush();
			return block.Id;
		}


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1711:IdentifiersShouldNotHaveIncorrectSuffix")]
		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null) throw new ArgumentNullException("title");

			IBlockCategoryProductList block = Create();
			block.ContentNodeId = contentNodeId;
			block.ContentAreaId = contentAreaId;
			block.BlockTypeId = blockTypeId;
			block.BlockStatusId = (int)BlockStatusInfo.Offline;
			block.Title = title;
			block.TemplateId = null;
			block.ProductSortOrderId = (int)ProductSortOrderConstant.TitleAscending;
			block.Id = Save(systemUserFull, block, new Collection<IBlockCategoryProductListCategory>());
			return block;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.SiteStructurePages);
			using (var transaction = new TransactedOperation())
			{
				BlockSettingSecureService.Delete(blockId);
				Repository.Delete(blockId);
				transaction.Complete();
			}
			BlockCategoryProductCollectionCache.Instance.Flush();
		}

		public virtual IBlockCategoryProductList GetById(int blockId)
		{
			return Repository.GetByIdSecure(blockId);
		}
	}
}
