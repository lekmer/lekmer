﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Cache;
using Litium.Lekmer.Product.Cache;
using Litium.Lekmer.Product.Repository;
using Litium.Lekmer.ProductFilter;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Convert = Litium.Scensum.Foundation.Convert;

namespace Litium.Lekmer.Product
{
	public class TagSecureService : ITagSecureService
	{
		protected TagRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }
		protected IBlockProductFilterSecureService BlockProductFilterSecureService { get; private set; }

		public TagSecureService(TagRepository repository, IAccessValidator accessValidator, IBlockProductFilterSecureService blockProductFilterSecureService)
		{
			Repository = repository;
			AccessValidator = accessValidator;
			BlockProductFilterSecureService = blockProductFilterSecureService;
		}


		public ITag GetById(int id)
		{
			return Repository.GetById(id);
		}

		public Collection<ITag> GetAll()
		{
			return Repository.GetAllSecure();
		}

		public Collection<ITag> GetAllByTagGroup(int tagGroupId)
		{
			return Repository.GetAllByTagGroupSecure(tagGroupId);
		}

		public Collection<ITag> GetAllByProductAndTagGroup(int productId, int tagGroupId)
		{
			return Repository.GetAllByProductAndTagGroupSecure(productId, tagGroupId);
		}

		public Collection<ITranslationGeneric> GetAllTranslationsByTag(int tagId)
		{
			return Repository.GetAllTranslationsByTag(tagId);
		}


		public ITag Create(int groupId, string value, string commonName)
		{
			var tag = IoC.Resolve<ITag>();
			tag.TagGroupId = groupId;
			tag.Value = value;
			tag.CommonName = commonName;
			return tag;
		}

		public int Save(ISystemUserFull systemUserFull, ITag tag, IEnumerable<ITranslationGeneric> translations)
		{
			if (translations == null)
			{
				throw new ArgumentNullException("translations");
			}

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transactedOperation = new TransactedOperation())
			{
				tag.Id = Repository.Save(tag);
				if (tag.Id > 0)
				{
					foreach (var translation in translations)
					{
						translation.Id = tag.Id;
						Repository.SaveTranslation(translation);
					}
				}
				transactedOperation.Complete();
			}

			TagGroupTagCache.Instance.Flush();
			ProductTagGroupCollectionCache.Instance.Flush();
			ProductFlagListCache.Instance.Flush();

			return tag.Id;
		}

		public void Delete(ISystemUserFull systemUserFull, int tagId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transactedOperation = new TransactedOperation())
			{
				BlockProductFilterSecureService.DeleteTag(systemUserFull, tagId);
				Repository.Delete(tagId);

				transactedOperation.Complete();
			}

			ProductTagGroupCollectionCache.Instance.Flush();
			ProductFlagListCache.Instance.Flush();
		}


		/// <summary>
		/// Add new tag but keep the current 
		/// </summary>
		public void AddAutoCampaignTag(ISystemUserFull systemUserFull, int productId, int tagId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.AddAutoCampaignTag(productId, tagId);
				transactedOperation.Complete();
			}

			ProductTagGroupCollectionCache.Instance.Remove(productId);
		}


		public virtual void UpdateProductTags(int productId, Collection<int> tagsToAdd, Collection<int> tagsToRemove, int objectId)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				Repository.DeleteProductTagsByProduct(productId, Convert.ToStringIdentifierList(tagsToRemove), objectId, Convert.DefaultListSeparator);
				foreach (var tagId in tagsToAdd)
				{
					Repository.SaveProductTag(productId, tagId, objectId);
				}

				transactedOperation.Complete();
			}
			ProductFlagListCache.Instance.Remove(productId);
		}


		// Tag-Flag
		public virtual void TagFlagSave(ISystemUserFull systemUserFull, int tagId, int flagId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			Repository.TagFlagSave(tagId, flagId);
			FlushTagCache();
		}
		public virtual void TagFlagDeleteByTag(ISystemUserFull systemUserFull, int tagId)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.AssortmentProduct);
			Repository.TagFlagDeleteByTag(tagId);
			FlushTagCache();
		}

		protected virtual void FlushTagCache()
		{
			TagCollectionCache.Instance.Flush();
			TagGroupCollectionCache.Instance.Flush();
			TagGroupTagCache.Instance.Flush();
			ProductTagCollectionCache.Instance.Flush();
			ProductTagGroupCollectionCache.Instance.Flush();
			ProductFlagListCache.Instance.Flush();
		}
	}
}