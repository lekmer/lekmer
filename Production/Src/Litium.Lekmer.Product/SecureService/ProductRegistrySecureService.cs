﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Repository;

namespace Litium.Lekmer.Product
{
	public class ProductRegistrySecureService : IProductRegistrySecureService
	{
		protected ProductRegistryRepository Repository { get; private set; }

		public ProductRegistrySecureService(ProductRegistryRepository repository)
		{
			Repository = repository;
		}

		public virtual Collection<IProductRegistry> GetAll()
		{
			return Repository.GetAllSecure();
		}

		public virtual Collection<IProductRegistry> GetAll(string productRegistryIdsToIgnore)
		{
			Collection<IProductRegistry> productRegistries = Repository.GetAllSecure();

			if (productRegistryIdsToIgnore.IsEmpty())
			{
				return productRegistries;
			}

			List<int> idsToIgnore = productRegistryIdsToIgnore.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(int.Parse).ToList();

			var productRegistriesFiltered = new Collection<IProductRegistry>();

			foreach (IProductRegistry productRegistry in productRegistries)
			{
				if (idsToIgnore.Contains(productRegistry.ProductRegistryId))
				{
					continue;
				}

				productRegistriesFiltered.Add(productRegistry);
			}

			return productRegistriesFiltered;
		}
	}
}