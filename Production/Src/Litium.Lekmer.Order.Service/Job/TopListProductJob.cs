﻿using System;
using Litium.Lekmer.Common.Job;
using Litium.Scensum.Foundation;
using Litium.Scensum.TopList;

namespace Litium.Lekmer.Order.Service
{
	public class TopListProductJob : BaseJob
	{
		private TopListProductScheduleSetting _setting;
		protected TopListProductScheduleSetting Setting
		{
			get { return _setting ?? (_setting = new TopListProductScheduleSetting()); }
		}

		public override string Group
		{
			get { return "Order"; }
		}

		public override string Name
		{
			get { return "TopListProductJob"; }
		}

		protected override void ExecuteAction()
		{
			IoC.Resolve<ITopListProductService>().PopulateTopListProductStatistic(DateTime.Now.AddDays(-1), Setting.KeepOrderStatisticDays);
		}
	}
}
