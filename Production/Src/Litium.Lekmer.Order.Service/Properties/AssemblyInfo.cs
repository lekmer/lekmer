﻿using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Litium.Lekmer.Order.Service")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("7f932f79-e536-4112-9bc8-09d534207900")]

// Configure log4net using the .config file
[assembly: log4net.Config.XmlConfigurator(Watch = true)]
