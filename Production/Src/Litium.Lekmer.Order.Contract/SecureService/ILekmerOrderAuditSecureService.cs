﻿using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderAuditSecureService : IOrderAuditSecureService
	{
		void DeleteByOrder(ISystemUserFull systemUserFull, int orderId);
	}
}