﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.Order
{
	public interface IOrderVoucherInfoSecureService
	{
		IOrderVoucherInfo GetByOrderId(int orderId);
		void Delete(ISystemUserFull systemUserFull, int orderId);
	}
}