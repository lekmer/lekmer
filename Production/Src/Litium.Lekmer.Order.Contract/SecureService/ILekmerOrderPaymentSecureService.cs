﻿using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerOrderPaymentSecureService : IOrderPaymentSecureService
	{
		void DeleteByOrder(ISystemUserFull systemUserFull, int orderId);
	}
}