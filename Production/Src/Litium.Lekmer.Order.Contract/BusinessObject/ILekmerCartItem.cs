using System.Collections.ObjectModel;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerCartItem : ICartItem
	{
		int? SizeId { get; set; }
		bool IsAffectedByCampaign { get; set; }
		string IPAddress { get; set; }
		Collection<ICartItemPackageElement> PackageElements { get; set; }

		void SplitPackageElements(ref Collection<int> itemWithoutSizes, ref Collection<IPackageElement> itemWithSizes);
		void ChangeQuantityForFreeProduct(int quantity);
		decimal GetDiscountPercent(bool isDiscountIncludingVat);
		void CleanPackageElementsId();
	}
}