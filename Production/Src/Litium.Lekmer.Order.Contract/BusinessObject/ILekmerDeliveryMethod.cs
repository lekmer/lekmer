﻿using System.Collections.ObjectModel;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerDeliveryMethod : IDeliveryMethod
	{
		bool ChannelDefault { get; set; }
		int Priority { get; set; }
		bool IsCompany { get; set; }
		Collection<IDeliveryMethodWeightPrice> WeightPrices { get; set; }
		bool FreightCostDependOnWeight { get; }
		int DeliveryMethodTypeId { get; set; }
	}
}