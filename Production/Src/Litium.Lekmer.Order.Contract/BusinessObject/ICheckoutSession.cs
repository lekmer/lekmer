using System;

namespace Litium.Lekmer.Order
{
	public interface ICheckoutSession
	{
		ICheckoutProfile Profile { get; set; }
		Uri KlarnaCheckoutId { get; set; }

		void CleanProfile();
		void InitializeProfile();
	}
}