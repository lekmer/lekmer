﻿using Litium.Lekmer.Payment.Collector;

namespace Litium.Lekmer.Order
{
	public interface ICollectorTimeoutOrder
	{
		int TransactionId { get; set; }
		CollectorTransactionStatus? StatusCode { get; set; }
		CollectorInvoiceStatus? InvoiceStatus { get; set; }
		int OrderId { get; set; }
		string InvoiceNo { get; set; }
		int ChannelId { get; set; }
	}
}
