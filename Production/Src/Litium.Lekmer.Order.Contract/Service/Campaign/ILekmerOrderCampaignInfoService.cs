﻿using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Order.Campaign
{
	public interface ILekmerOrderCampaignInfoService
	{
		IOrderCampaignInfo GetByOrder(int orderId);
	}
}
