﻿using System;
using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerCartService : ICartService
	{
		ICartFull Create(IUserContext context, string ipAddress);
		void RetrivePreviousShoppingCard();
		void Update(Guid cartGuid, int removedItemsCount);
		void DeleteExpiredItems();
		ILekmerCartFull CloneCart(ILekmerCartFull cart);
		int SaveCloneCart(IUserContext context, ILekmerCartFull cart);
		ICartFull GetByIdAndCartGuid(IUserContext context, int id, Guid cartGuid);
	}
}