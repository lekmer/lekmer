﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.Order
{
	public interface IPackageOrderItemService
	{
		int Save(IPackageOrderItem packageOrderItem);
		Collection<IPackageOrderItem> GetAllByOrder(int orderId);
		Collection<IPackageOrderItem> GetAllByOrderItem(int orderItemId);
	}
}