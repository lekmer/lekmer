﻿using Litium.Scensum.Core;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order
{
	public interface ILekmerDeliveryMethodService : IDeliveryMethodService
	{
		IDeliveryMethodWeightPrice GetMatchingWeightPrice(ILekmerDeliveryMethod deliveryMethod, decimal cartWeight);
		IDeliveryMethod GetOptionalByDeliveryMethod(IUserContext context, int deliveryMethodId);
	}
}
