using System;
using System.Collections.Specialized;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Core;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Order;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;
using IFormatter = Litium.Scensum.Core.IFormatter;

namespace Litium.Lekmer.Voucher.Web
{
	public class VoucherDiscountForm : ControlBase
	{
		private ILekmerFormatter _formatter;

		public ILekmerFormatter Formatter
		{
			get { return _formatter ?? (_formatter = (ILekmerFormatter)IoC.Resolve<IFormatter>()); }
		}

		public virtual string VoucherCode { get; set; }

		public string VoucherCodeFormName
		{
			get { return "voucher-code"; }
		}

		public string UseVoucherFormName
		{
			get { return "action"; }
		}

		public string UseVoucherFormValue
		{
			get { return "voucher"; }
		}

		public string UseVoucherFormText
		{
			get { return AliasHelper.GetAliasValue("Order.Checkout.Voucher.UseVoucher.Button.Value"); }
		}

		public void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			VoucherCode = form[VoucherCodeFormName].NullWhenTrimmedEmpty();
		}

		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.VoucherCodeTextBox.Name", VoucherCodeFormName);
			fragment.AddVariable("Form.UseVoucherButton.Name", UseVoucherFormName);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.UseVoucherButton.Value", UseVoucherFormValue);

			var voucher = IoC.Resolve<IVoucherSession>().Voucher;
			if (voucher != null)
			{
				fragment.AddCondition("IsVoucherUsed", true);
				fragment.AddCondition("IsVoucherWithoutDiscount", IsVoucherWithoutDiscount(voucher));
				fragment.AddVariable("Form.VoucherCodeTextBox.Value", voucher.VoucherCode);
			}
			else
			{
				fragment.AddCondition("IsVoucherUsed", false);
				fragment.AddCondition("IsVoucherWithoutDiscount", false);
				fragment.AddVariable("Form.VoucherCodeTextBox.Value", string.Empty);
			}

			ILekmerCartCampaignInfo campaignInfo = CheckCartCampaignInfo();
			ICartVoucherInfo voucherInfo = CheckVoucherInfo();

			if (voucherInfo != null && voucherInfo.DiscountValue != null)
			{
				fragment.AddCondition("IsVoucherApplied", true);
				fragment.AddVariable("UsedVoucherInfo", ComposeVoucherInfo(voucherInfo));
			}
			else
			{
				fragment.AddCondition("IsVoucherApplied", false);
				fragment.AddVariable("UsedVoucherInfo", string.Empty);
			}

			if (campaignInfo != null )
			{
				fragment.AddCondition("Voucher.IsApplied",
					(voucherInfo != null && voucherInfo.DiscountValue != null)
					|| campaignInfo.VoucherActualFixedDiscountOnGroupOfCartItems > 0
					|| campaignInfo.VoucherDiscountOnCartItemPercentageDiscountActionItems > 0
					|| campaignInfo.VoucherDiscountOnCartItemFixedDiscountActionItems > 0);

				fragment.AddCondition(
					"Voucher.FixedDiscountOnGroupOfCartItems.IsApplied",
					campaignInfo.VoucherActualFixedDiscountOnGroupOfCartItems > 0);

				fragment.AddCondition(
					"Voucher.TotalFixedDiscountOnGroupOfCartItems.HasValue",
					campaignInfo.VoucherTotalFixedDiscountOnGroupOfCartItems > 0);

				fragment.AddCondition(
					"Voucher.ActualFixedDiscountOnGroupOfCartItems.HasValue",
					campaignInfo.VoucherActualFixedDiscountOnGroupOfCartItems > 0);

				decimal totalFixedDiscount = Math.Round(campaignInfo.VoucherTotalFixedDiscountOnGroupOfCartItems, 0, MidpointRounding.AwayFromZero);
				decimal actualFixedDiscount = Math.Round(campaignInfo.VoucherActualFixedDiscountOnGroupOfCartItems, 0, MidpointRounding.AwayFromZero);

				fragment.AddVariable(
					"Voucher.TotalFixedDiscountOnGroupOfCartItems",
					Formatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, totalFixedDiscount));

				fragment.AddVariable(
					"Voucher.ActualFixedDiscountOnGroupOfCartItems",
					Formatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, actualFixedDiscount));

				// Cart item percentage discount
				fragment.AddCondition("Voucher.CartItemPercentageDiscount.IsApplied", campaignInfo.VoucherDiscountOnCartItemPercentageDiscountActionItems > 0);
				decimal cartItemPercentageDiscount = Math.Round(campaignInfo.VoucherDiscountOnCartItemPercentageDiscountActionItems, 0, MidpointRounding.AwayFromZero);
				fragment.AddVariable(
					"Voucher.CartItemPercentageDiscount",
					Formatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, cartItemPercentageDiscount));

				// Cart item fixed discount
				fragment.AddCondition("Voucher.CartItemFixedDiscount.IsApplied", campaignInfo.VoucherDiscountOnCartItemFixedDiscountActionItems > 0);
				decimal actualCartItemFixedDiscount = Math.Round(campaignInfo.VoucherDiscountOnCartItemFixedDiscountActionItems, 0, MidpointRounding.AwayFromZero);
				fragment.AddVariable(
					"Voucher.CartItemFixedDiscount",
					Formatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, actualCartItemFixedDiscount));
			}
			else
			{
				fragment.AddCondition("Voucher.IsApplied", false);
				fragment.AddCondition("Voucher.FixedDiscountOnGroupOfCartItems.IsApplied", false);
				fragment.AddCondition("Voucher.TotalFixedDiscountOnGroupOfCartItems.HasValue", false);
				fragment.AddCondition("Voucher.ActualFixedDiscountOnGroupOfCartItems.HasValue", false);
				fragment.AddVariable("Voucher.TotalFixedDiscountOnGroupOfCartItems", string.Empty);
				fragment.AddVariable("Voucher.ActualFixedDiscountOnGroupOfCartItems", string.Empty);
				fragment.AddCondition("Voucher.CartItemPercentageDiscount.IsApplied", false);
				fragment.AddVariable("Voucher.CartItemPercentageDiscount", string.Empty);
				fragment.AddCondition("Voucher.CartItemFixedDiscount.IsApplied", false);
				fragment.AddVariable("Voucher.CartItemFixedDiscount", string.Empty);
			}
		}

		private static ILekmerCartCampaignInfo CheckCartCampaignInfo()
		{
			var cart = (ILekmerCartFull)IoC.Resolve<ICartSession>().Cart;
			if (cart != null)
			{
				return (ILekmerCartCampaignInfo)cart.CampaignInfo;
			}

			return null;
		}
		
		private static ICartVoucherInfo CheckVoucherInfo()
		{
			var cart = (ILekmerCartFull)IoC.Resolve<ICartSession>().Cart;
			if (cart != null)
			{
				return cart.VoucherInfo;
			}

			return null;
		}

		private string ComposeVoucherInfo(ICartVoucherInfo voucherInfo)
		{
			if (voucherInfo.DiscountType == VoucherValueType.Percentage)
			{
				return string.Format("- {0} %", voucherInfo.DiscountValue);
			}

			if (voucherInfo.DiscountType == VoucherValueType.Price)
			{
				decimal discountValue = voucherInfo.DiscountValue.HasValue ? voucherInfo.DiscountValue.Value : 0m;
				return string.Format("- {0}", Formatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, discountValue));
			}

			if (voucherInfo.DiscountType == VoucherValueType.GiftCard)
			{
				decimal amountUsed = voucherInfo.AmountUsed.HasValue ? voucherInfo.AmountUsed.Value : 0m;
				return string.Format("- {0}", Formatter.FormatPriceTwoOrLessDecimals(UserContext.Current.Channel, amountUsed));
			}

			throw new NotSupportedException("VoucherValueType '" + voucherInfo.DiscountType + "' not supported.");
		}

		private static bool IsVoucherWithoutDiscount(IVoucherCheckResult voucherCheckResult)
		{
			if (voucherCheckResult.ValueType == VoucherValueType.Percentage
				|| voucherCheckResult.ValueType == VoucherValueType.Price)
			{
				return voucherCheckResult.DiscountValue <= 0;
			}

			if (voucherCheckResult.ValueType == VoucherValueType.Code)
			{
				return true;
			}

			return false;
		}
	}
}