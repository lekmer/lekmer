using Litium.Lekmer.Core.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using IUserContextFactory = Litium.Scensum.Core.IUserContextFactory;

namespace Litium.Lekmer.Voucher.Web
{
	public class BlockVoucherDiscountControl : BlockControlBase
	{
		protected IVoucherSession VoucherSession { get; private set; }
		protected IVoucherService VoucherService { get; private set; }
		protected int? SignedInCustomerId
		{
			get { return CustomerSession.IsSignedIn ? (int?)CustomerSession.SignedInCustomer.Id : null; }
		}

		private VoucherDiscountForm _voucherDiscountForm;
		public VoucherDiscountForm VoucherDiscountForm
		{
			get
			{
				return _voucherDiscountForm ?? (_voucherDiscountForm = new VoucherDiscountForm());
			}
		}

		public ICustomerSession CustomerSession { get; set; }


		public BlockVoucherDiscountControl(
			ICustomerSession customerSession,
			ITemplateFactory templateFactory,
			IBlockService blockService,
			IVoucherSession voucherSession,
			IVoucherService voucherService)
			: base(templateFactory, blockService)
		{
			CustomerSession = customerSession;
			VoucherSession = voucherSession;
			VoucherService = voucherService;
		}

		protected override BlockContent RenderCore()
		{
			VoucherSession.VoucherCode = null;
			VoucherSession.WasUsed = false;

			if (IsVoucherPost())
			{
				VoucherDiscountForm.MapFromRequest();

				var voucherCode = VoucherDiscountForm.VoucherCode;
				if (string.IsNullOrEmpty(voucherCode))
				{
					VoucherSession.Voucher = null;
				}
				else
				{
					var voucherCheckResult = VoucherService.VoucherCheck(voucherCode, UserContext.Current.Channel.ApplicationName);
					VoucherSession.Voucher = voucherCheckResult.IsValid ? voucherCheckResult : null;
				}

				IoC.Resolve<IUserContextFactory>().FlushCache();
			}
			else if (PostMode == "checkout" && Request.Form["GetAddress"].IsNullOrEmpty())
			{
				CheckVoucher();
			}

			return RenderForm();
		}

		private BlockContent RenderForm()
		{
			Fragment fragmentContent = Template.GetFragment("Content");
			VoucherDiscountForm.MapFieldNamesToFragment(fragmentContent);
			VoucherDiscountForm.MapFieldValuesToFragment(fragmentContent);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		protected void CheckVoucher()
		{
			if (VoucherSession.Voucher != null && VoucherSession.Voucher.IsValid)
			{
				var voucherCheckResult = VoucherService.VoucherCheck(VoucherSession.Voucher.VoucherCode, UserContext.Current.Channel.ApplicationName);

				if (!voucherCheckResult.IsValid)
				{
					VoucherSession.WasUsed = true;
					VoucherSession.VoucherCode = VoucherSession.Voucher.VoucherCode;
					VoucherSession.Voucher = null;

					IoC.Resolve<IUserContextFactory>().FlushCache();
				}
			}
		}

		private bool IsVoucherPost()
		{
			string useVoucherValue = Request.Form[VoucherDiscountForm.UseVoucherFormName];
			return useVoucherValue == VoucherDiscountForm.UseVoucherFormValue || useVoucherValue == VoucherDiscountForm.UseVoucherFormText;
		}

		//[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		//protected virtual ICustomer GetCustomer()
		//{
		//    var customerService = IoC.Resolve<ICustomerService>();
		//    ICustomer customer = null;
		//    if (SignedInCustomerId.HasValue)
		//    {
		//        customer = customerService.GetById(UserContext.Current, SignedInCustomerId.Value);
		//    }
		//    if (customer == null)
		//    {
		//        customer = customerService.Create(UserContext.Current, true, false);
		//    }

		//    if (customer.CustomerInformation == null)
		//    {
		//        customer.CustomerInformation = IoC.Resolve<ICustomerInformationService>().Create(UserContext.Current);
		//    }

		//    if (customer.CustomerInformation.DefaultBillingAddress == null)
		//    {
		//        customer.CustomerInformation.DefaultBillingAddress = IoC.Resolve<IAddressService>().Create(UserContext.Current);
		//    }
		//    customer.CustomerInformation.DefaultBillingAddress.CountryId = Channel.Current.Country.Id;


		//    if (customer.CustomerInformation.DefaultDeliveryAddress == null)
		//    {
		//        customer.CustomerInformation.DefaultDeliveryAddress = IoC.Resolve<IAddressService>().Create(UserContext.Current);
		//    }
		//    customer.CustomerInformation.DefaultDeliveryAddress.CountryId = Channel.Current.Country.Id;

		//    return customer;
		//}
	}
}