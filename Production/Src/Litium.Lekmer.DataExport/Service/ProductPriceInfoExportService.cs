﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.DataExport
{
	public class ProductPriceInfoExportService : IProductPriceInfoExportService
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private IEnumerable<IUserContext> _userContexts;

		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected ILekmerProductService LekmerProductService { get; private set; }
		protected IProductPriceInfoService ProductPriceInfoService { get; private set; }

		public ProductPriceInfoExportService(ILekmerChannelService lekmerChannelService, ILekmerProductService lekmerProductService, IProductPriceInfoService productPriceInfoService)
		{
			LekmerChannelService = lekmerChannelService;
			LekmerProductService = lekmerProductService;
			ProductPriceInfoService = productPriceInfoService;
		}


		public virtual void ExportProductPriceInfo()
		{
			_log.Info("ExportProductPriceInfo execution is started.");

			_userContexts = GetUserContextForAllChannels();

			foreach (IUserContext context in _userContexts)
			{
				_log.InfoFormat("Reading complete product data is started. //{0}", context.Channel.CommonName);
				var products = GetCompleteProductListWithoutAnyFilter(context);

				_log.InfoFormat("Reading current product price info data is started. //{0}", context.Channel.CommonName);
				var productPriceInfoList = ProductPriceInfoService.GetAll(context);

				_log.InfoFormat("Updating product price info data is started. //{0}", context.Channel.CommonName);
				RefreshProductPriceInfo(context, products, productPriceInfoList);

				_log.InfoFormat("Storing product price info data is started. //{0}", context.Channel.CommonName);
				ProductPriceInfoService.Save(context, productPriceInfoList);
			}

			_log.Info("ExportProductPriceInfo is executed.");
		}


		protected virtual IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			Collection<IChannel> channels = LekmerChannelService.GetAll();

			return channels
				.Select(channel =>
				{
					var context = IoC.Resolve<IUserContext>();
					context.Channel = channel;
					return context;
				}
			);
		}

		protected virtual ProductCollection GetCompleteProductList(IUserContext context)
		{
			var completeList = new ProductCollection();

			var pages = IoC.Resolve<IPageTracker>().Initialize(100, int.MaxValue);

			while (pages.HasNext)
			{
				ProductCollection products = LekmerProductService.GetAll(context, pages.Current, pages.PageSize);

				completeList.AddRange(products);

				pages.MoveToNext(products.TotalCount);
			}

			return completeList;
		}

		protected virtual ProductCollection GetCompleteProductListWithoutAnyFilter(IUserContext context)
		{
			var completeList = new ProductCollection();

			var pages = IoC.Resolve<IPageTracker>().Initialize(100, int.MaxValue);

			while (pages.HasNext)
			{
				ProductCollection products = LekmerProductService.GetAllWithoutAnyFilter(context, pages.Current, pages.PageSize, null);

				completeList.AddRange(products);

				pages.MoveToNext(products.TotalCount);
			}

			return completeList;
		}

		protected virtual void RefreshProductPriceInfo(IUserContext context, ProductCollection products, Collection<IProductPriceInfo> productPriceInfoList)
		{
			var productDict = new Dictionary<int, IProduct>();
			foreach (IProduct product in products) //eliminate exception of type "An item with the same key has already been added."
			{
				productDict[product.Id] = product;
			}

			var productPriceInfoDict = new Dictionary<int, IProductPriceInfo>();
			foreach (IProductPriceInfo productPriceInfo in productPriceInfoList) //eliminate exception of type "An item with the same key has already been added."
			{
				productPriceInfoDict[productPriceInfo.ProductId] = productPriceInfo;
			}

			var newProductPriceInfoList = new Collection<IProductPriceInfo>();

			foreach (IProduct product in products)
			{
				if (productPriceInfoDict.ContainsKey(product.Id))
				{
					UpdateProductPriceInfo(product, productPriceInfoDict[product.Id]);
				}
				else
				{
					newProductPriceInfoList.Add(CreateNewProductPriceInfo(context, product));
				}
			}

			foreach (IProductPriceInfo productPriceInfo in productPriceInfoList)
			{
				if (!productDict.ContainsKey(productPriceInfo.ProductId))
				{
					productPriceInfo.SetDeleted();
				}
			}

			productPriceInfoList.AddRange(newProductPriceInfoList);
		}

		protected virtual void UpdateProductPriceInfo(IProduct product, IProductPriceInfo productPriceInfo)
		{
			productPriceInfo.PriceIncludingVat = product.Price.PriceIncludingVat;
			productPriceInfo.PriceExcludingVat = product.Price.PriceExcludingVat;

			var lekmerProduct = product as ILekmerProduct;
			if (lekmerProduct != null && lekmerProduct.IsPriceAffectedByCampaign)
			{
				productPriceInfo.DiscountPriceIncludingVat = product.CampaignInfo.Price.IncludingVat;
				productPriceInfo.DiscountPriceExcludingVat = product.CampaignInfo.Price.ExcludingVat;
			}
			else
			{
				productPriceInfo.DiscountPriceIncludingVat = null;
				productPriceInfo.DiscountPriceExcludingVat = null;
			}
		}

		protected virtual IProductPriceInfo CreateNewProductPriceInfo(IUserContext context, IProduct product)
		{
			var productPriceInfo = IoC.Resolve<IProductPriceInfo>();

			productPriceInfo.ChannelId = context.Channel.Id;
			productPriceInfo.ProductId = product.Id;

			UpdateProductPriceInfo(product, productPriceInfo);

			return productPriceInfo;
		}
	}
}
