﻿using System.Globalization;
using System.Linq;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.NewsletterSubscriber.Web
{
	public class BlockNewsletterUnregistrationControl : BlockControlBase
	{
		private NewsletterUnregistrationForm _newsletterUnregistrationForm;

		public BlockNewsletterUnregistrationControl(ITemplateFactory templateFactory, IBlockService blockService)
			: base(templateFactory, blockService) {}

		protected virtual NewsletterUnregistrationForm NewsletterUnregistrationForm
		{
			get
			{
				return _newsletterUnregistrationForm ?? (_newsletterUnregistrationForm = new NewsletterUnregistrationForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			bool unregistered = false;
			if (!NewsletterUnregistrationForm.IsFormPostBack)
			{
				NewsletterUnregistrationForm.Email = GetEmailParameter();
			}
			else
			{
				NewsletterUnregistrationForm.MapFromRequest();
				validationResult = NewsletterUnregistrationForm.Validate();
				if (validationResult.IsValid)
				{
					unregistered = UnregisterNewsletterSubscriber();
				}
			}
			return RenderNewsletterUnregistration(validationResult, unregistered);
		}

		protected virtual BlockContent RenderNewsletterUnregistration(ValidationResult validationResult, bool unregistered)
		{
			Fragment fragmentContent = Template.GetFragment("Content");

			NewsletterUnregistrationForm.MapFieldNamesToFragment(fragmentContent);
			NewsletterUnregistrationForm.MapFieldValuesToFragment(fragmentContent);

			RenderValidationResult(fragmentContent, validationResult, unregistered);

			fragmentContent.AddCondition("WasUnregistered", unregistered);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);
			NewsletterUnregistrationForm.MapFieldNamesToFragment(fragment);
			NewsletterUnregistrationForm.MapFieldValuesToFragment(fragment);
			fragment.AddEntity(Block);
			return fragment.Render();
		}

		protected virtual void RenderValidationResult(Fragment fragmentContent, ValidationResult validationResult, bool unregistered)
		{
			string validationError = null;
			bool isValid = validationResult == null || validationResult.IsValid;
			if (!isValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			else if (validationResult != null && validationResult.IsValid && !unregistered)
			{
				validationError = AliasHelper.GetAliasValue("Email.NewsletterRegistration.SubscriberNotFound");
			}
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}

		protected virtual string GetEmailParameter()
		{
			return Request.QueryString["email"];
		}

		protected virtual bool UnregisterNewsletterSubscriber()
		{
			string email = NewsletterUnregistrationForm.Email.ToLower(CultureInfo.InvariantCulture);

			var subscriberService = IoC.Resolve<INewsletterSubscriberService>();
			var unsubscriberService = IoC.Resolve<INewsletterUnsubscriberService>();
			var unsubscriberOptionService = IoC.Resolve<INewsletterUnsubscriberOptionService>();

			bool unregisterd = subscriberService.Delete(UserContext.Current, email);

			var unsubscriber = unsubscriberService.GetByEmail(UserContext.Current, email) ?? unsubscriberService.Create(UserContext.Current);

			unsubscriber.Email = email;

			ApplyCustomUnsubscriberOption(unsubscriber, (int) NewsletterType.MonitorProduct, NewsletterUnregistrationForm.UnregFromProductMonitor, unsubscriberOptionService);
			ApplyCustomUnsubscriberOption(unsubscriber, (int) NewsletterType.OrderReview, NewsletterUnregistrationForm.UnregFromOrderReview, unsubscriberOptionService);
			ApplyCustomUnsubscriberOption(unsubscriber, (int) NewsletterType.InterspireContactList, NewsletterUnregistrationForm.UnregFromInterspire, unsubscriberOptionService);

			int unsubscriberId = unsubscriberService.Save(UserContext.Current, unsubscriber);

			unregisterd = unregisterd || unsubscriberId > 0;

			if (NewsletterUnregistrationForm.UnregFromProductMonitor || NewsletterUnregistrationForm.UnregFromInterspire)
			{
				var monitorProductService = IoC.Resolve<IMonitorProductService>();
				monitorProductService.Delete(UserContext.Current, email);
			}

			return unregisterd;
		}

		protected virtual void ApplyCustomUnsubscriberOption(INewsletterUnsubscriber unsubscriber, int optionType, bool optionSelected, INewsletterUnsubscriberOptionService optionService)
		{
			if (optionSelected)
			{
				if (unsubscriber.UnsubscriberOptions.All(o => o.NewsletterTypeId != optionType))
				{
					INewsletterUnsubscriberOption unsubscriberOption = optionService.Create();
					unsubscriberOption.NewsletterTypeId = optionType;
					unsubscriber.UnsubscriberOptions.Add(unsubscriberOption);
				}
			}
			else
			{
				var optionsToDelete = unsubscriber.UnsubscriberOptions.Where(o => o.NewsletterTypeId == optionType);
				foreach (var optionToDelete in optionsToDelete)
				{
					optionToDelete.SetDeleted();
				}
			}
		}
	}
}