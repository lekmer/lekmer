﻿using System.Collections.Specialized;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.NewsletterSubscriber.Web
{
	public class NewsletterUnregistrationForm : ControlBase
	{
		private readonly string _postUrl;

		public NewsletterUnregistrationForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "newsletterunregistration"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string EmailFormName
		{
			get { return "newsletterunregistration-email"; }
		}

		public string UnregFromInterspireFormName
		{
			get { return "newsletterunregistration-all"; }
		}

		public string UnregFromInterspireFormValue
		{
			get { return "unreg"; }
		}

		public string UnregFromProductMonitorFormName
		{
			get { return "newsletterunregistration-productmonitor"; }
		}

		public string UnregFromProductMonitorFormValue
		{
			get { return "unreg"; }
		}

		public string UnregFromOrderReviewFormName
		{
			get { return "newsletterunregistration-orderreview"; }
		}

		public string UnregFromOrderReviewFormValue
		{
			get { return "unreg"; }
		}

		public string Email { get; set; }

		public bool UnregFromInterspire { get; set; }

		public bool UnregFromProductMonitor { get; set; }

		public bool UnregFromOrderReview { get; set; }


		public void MapFromRequest()
		{
			NameValueCollection form = Request.Form;

			Email = form[EmailFormName];

			UnregFromInterspire = form.GetBooleanValue(UnregFromInterspireFormName, UnregFromInterspireFormValue);
			UnregFromProductMonitor = form.GetBooleanValue(UnregFromProductMonitorFormName, UnregFromProductMonitorFormValue);
			UnregFromOrderReview = form.GetBooleanValue(UnregFromOrderReviewFormName, UnregFromOrderReviewFormValue);
		}

		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);

			fragment.AddVariable("Form.Email.Name", EmailFormName);

			fragment.AddVariable("Form.UnregFromInterspire.Name", UnregFromInterspireFormName);
			fragment.AddVariable("Form.UnregFromProductMonitor.Name", UnregFromProductMonitorFormName);
			fragment.AddVariable("Form.UnregFromOrderReview.Name", UnregFromOrderReviewFormName);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);

			fragment.AddVariable("Form.Email.Value", Email);

			fragment.AddVariable("Form.UnregFromInterspire.Value", UnregFromInterspireFormValue);
			fragment.AddVariable("Form.UnregFromProductMonitor.Value", UnregFromProductMonitorFormValue);
			fragment.AddVariable("Form.UnregFromOrderReview.Value", UnregFromOrderReviewFormValue);

			fragment.AddCondition("Form.UnregFromInterspire.Selected", UnregFromInterspire);
			fragment.AddCondition("Form.UnregFromProductMonitor.Selected", UnregFromProductMonitor);
			fragment.AddCondition("Form.UnregFromOrderReview.Selected", UnregFromOrderReview);
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();
			ValidateEmail(validationResult);
			ValidateEmptySelection(validationResult);
			return validationResult;
		}

		private void ValidateEmail(ValidationResult validationResult)
		{
			if (Email.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.NewsletterRegistration.Validation.EmailEmpty"));
			}
			if (ValidationUtil.IsValidEmail(Email) == false)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.NewsletterRegistration.Validation.EmailIncorrect"));
			}
		}

		private void ValidateEmptySelection(ValidationResult validationResult)
		{
			if (!(UnregFromInterspire || UnregFromProductMonitor || UnregFromOrderReview))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.NewsletterRegistration.Validation.NoOptionSelection"));
			}
		}
	}
}