﻿using System.Collections.Specialized;
using Litium.Lekmer.Common;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.NewsletterSubscriber.Web
{
	public class NewsletterRegistrationForm : ControlBase
	{
		private readonly string _postUrl;
		private CaptchaForm _captchaForm;

		public NewsletterRegistrationForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public CaptchaForm CaptchaForm
		{
			get { return _captchaForm ?? (_captchaForm = new CaptchaForm()); }
		}

		public string EmailFormName
		{
			get { return "newsletterregistration-email"; }
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "newsletterregistration"; }
		}

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public string Email { get; set; }

		public void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			Email = form[EmailFormName];
			CaptchaForm.MapFromRequestToForm();
		}

		public void ClearFrom()
		{
			Email = string.Empty;
			CaptchaForm.SetDefaultValues();
		}

		public void MapFieldNamesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostMode.Name", PostModeName);
			fragment.AddVariable("Form.Email.Name", EmailFormName);
			CaptchaForm.MapFieldNamesToFragment(fragment);
		}

		public void MapFieldValuesToFragment(Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.PostMode.Value", PostModeValue);
			fragment.AddVariable("Form.Email.Value", Email);
			CaptchaForm.MapFieldValuesToFragment(fragment);
		}

		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();       
			ValidateEmail(validationResult);
			validationResult.Errors.Append(CaptchaForm.Validate().Errors);
			return validationResult;
		}

		private void ValidateEmail(ValidationResult validationResult)
		{
			if (Email.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.NewsletterRegistration.Validation.EmailEmpty"));
			}
			else if (!ValidationUtil.IsValidEmail(Email))
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Email.NewsletterRegistration.Validation.EmailIncorrect"));
			}
		}
	}
}