﻿using System.Timers;
using Litium.Lekmer.Oculus.Setting;

namespace Litium.Lekmer.OculusService
{
	public class ScheduledTaskUtil
	{
		public static Timer SetupTimer()
		{
			OculusSetting oculusSetting = new OculusSetting();

			int interval = oculusSetting.TimerInterval();

			Timer timer = new Timer
			{
				Interval = interval * 60000, 
				Enabled = true, 
				AutoReset = true
			};

			return timer;
		}
	}
}