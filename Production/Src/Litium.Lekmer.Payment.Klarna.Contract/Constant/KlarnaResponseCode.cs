﻿namespace Litium.Lekmer.Payment.Klarna
{
	public enum KlarnaResponseCode
	{
		None = 0,
		Ok = 1,
		NoRisk = 2,
		TimeoutResponse = 3,
		SpecifiedError = 4,
		UnspecifiedError = 5,
		Denied = 6,
		Pending = 7
	}
}
