﻿using System;

namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaPendingOrder
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		int OrderId { get; set; }
		DateTime FirstAttempt { get; set; }
		DateTime LastAttempt { get; set; }
	}
}
