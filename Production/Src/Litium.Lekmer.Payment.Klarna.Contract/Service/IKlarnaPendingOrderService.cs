﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Klarna
{
	public interface IKlarnaPendingOrderService
	{
		IKlarnaPendingOrder Create();
		int Insert(IKlarnaPendingOrder klarnaPendingOrder);
		Collection<IKlarnaPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkFromDate);
		void Update(IKlarnaPendingOrder klarnaPendingOrder);
		void Delete(int klarnaPendingOrderId);
	}
}
