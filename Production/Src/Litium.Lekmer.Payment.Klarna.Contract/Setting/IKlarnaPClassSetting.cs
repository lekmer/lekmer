﻿namespace Litium.Lekmer.Payment.Klarna.Setting
{
	public interface IKlarnaPClassSetting
	{
		int PClassId { get; }
		int MinimumTotalCost { get; }
		int MonthlyFee { get; }
		int OneTimeFee { get; }
		int Rate { get; }
		int NoOfMonths { get; }
		string Description { get; }
		string EncodingIso { get; }
		int Currency { get; }
		int Type { get; }
	}
}
