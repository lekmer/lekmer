﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroPaymentTypeSecureService
	{
		Collection<IQliroPaymentType> GetAllByChannel(IChannel channel);

		int Save(ISystemUserFull systemUserFull, IQliroPaymentType qliroPaymentType);
		void Delete(ISystemUserFull systemUserFull, int qliroPaymentTypeId);
	}
}
