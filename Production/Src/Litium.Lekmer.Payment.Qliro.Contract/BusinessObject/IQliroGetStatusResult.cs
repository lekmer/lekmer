﻿namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroGetStatusResult : IQliroResult
	{
		QliroInvoiceStatus InvoiceStatus { get; set; }
	}
}
