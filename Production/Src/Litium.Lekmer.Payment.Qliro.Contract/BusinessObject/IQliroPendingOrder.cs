﻿using System;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroPendingOrder
	{
		int Id { get; set; }
		int ChannelId { get; set; }
		int OrderId { get; set; }
		DateTime FirstAttempt { get; set; }
		DateTime LastAttempt { get; set; }
	}
}
