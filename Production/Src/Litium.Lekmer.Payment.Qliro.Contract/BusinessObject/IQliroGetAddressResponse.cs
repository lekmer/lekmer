﻿using Litium.Lekmer.Payment.Qliro.Contract.Checkout;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroGetAddressResponse : IQliroResult
	{
		address[] Addresses { get; set; }
	}
}
