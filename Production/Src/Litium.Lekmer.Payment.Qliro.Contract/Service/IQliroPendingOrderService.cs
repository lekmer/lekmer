﻿using System;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.Payment.Qliro
{
	public interface IQliroPendingOrderService
	{
		IQliroPendingOrder Create();
		int Insert(IQliroPendingOrder qliroPendingOrder);
		Collection<IQliroPendingOrder> GetPendingOrdersForStatusCheck(DateTime checkFromDate);
		void Update(IQliroPendingOrder qliroPendingOrder);
		void Delete(int qliroPendingOrderId);
	}
}
