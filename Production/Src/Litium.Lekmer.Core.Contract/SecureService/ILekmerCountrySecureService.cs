﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface ILekmerCountrySecureService : ICountrySecureService
	{
		Collection<ICountry> GetAll(string countryIdsToIgnore);
	}
}
