﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Core
{
	public interface ILekmerChannelSecureService : IChannelSecureService
	{
		Collection<IChannel> GetAll(string channelCommonNamesToIgnore);
	}
}
