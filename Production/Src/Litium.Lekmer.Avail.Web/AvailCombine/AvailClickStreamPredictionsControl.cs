﻿using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web
{
	public class AvailClickStreamPredictionsControl
	{
		public void Render(Fragment fragmentHead, Fragment fragmentContent)
		{
			fragmentHead.AddCondition("UseClickStreamPredictions", true);
			fragmentContent.AddCondition("UseClickStreamPredictions", true);
		}
	}
}
