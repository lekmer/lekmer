﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Avail.Web.AvailCombine
{
	public class AvailPersonalPredictionsControl
	{
		public void Render(Fragment fragmentHead, Fragment fragmentContent)
		{
			ICustomerSession customerSession = IoC.Resolve<ICustomerSession>();
			string identificationId = CustomerIdentificationCookie.SetCustomerIdentificationId(customerSession);
			fragmentHead.AddVariable("Customer.Id", identificationId);
			fragmentHead.AddCondition("UsePersonalPredictions", true);
			fragmentContent.AddCondition("UsePersonalPredictions", true);
		}
	}
}
