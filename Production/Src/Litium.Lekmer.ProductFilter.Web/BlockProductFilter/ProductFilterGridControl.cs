﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Common.Web;
using Litium.Lekmer.Product;
using Litium.Lekmer.RatingReview;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.ProductFilter
{
	public class ProductFilterGridControl<TItem> : GridControl<TItem> where TItem : class
	{
		private GridControlHelper _gridControlHelper;

		public IBlockRating BlockRating { get; set; }

		public ILekmerProductService LekmerProductService { get; set; }
		public IRatingService RatingService { get; set; }
		public IRatingGroupService RatingGroupService { get; set; }
		public IRatingItemProductScoreService RatingItemProductScoreService { get; set; }

		public override string Render()
		{
			LekmerProductService.EnsureNotNull();

			_gridControlHelper = new GridControlHelper(BlockRating, Template, RatingService, RatingGroupService, RatingItemProductScoreService);

			return base.Render();
		}

		protected override string RenderItem(int columnIndex, TItem item)
		{
			Fragment fragmentItem = Template.GetFragment(ItemFragmentName);
			var fragmentVariableInfo = new FragmentVariableInfo(fragmentItem);

			AddPositionCondition(fragmentItem, columnIndex, ColumnCount);
			fragmentItem.AddEntity(item);

			var lekmerProduct = (ILekmerProduct)item;
			if (lekmerProduct.HasSizes)
			{
				if (fragmentVariableInfo.HasVariable("Iterate:SizeOption"))
				{
					fragmentItem.AddVariable("Iterate:SizeOption", RenderSizeOptions(lekmerProduct), VariableEncoding.None);
				}
			}
			else
			{
				fragmentItem.AddVariable("Iterate:SizeOption", string.Empty, VariableEncoding.None);
			}

			if (lekmerProduct.ShowVariantRelations)
			{
				ProductCollection colorVariants = GetColorVariants(lekmerProduct);
				if (colorVariants != null)
				{
					fragmentItem.AddCondition("Product.HasVariants", colorVariants.Count > 0);

					if (fragmentVariableInfo.HasVariable("Iterate:ColorOption"))
					{
						fragmentItem.AddVariable("Iterate:ColorOption", RenderColorOptions(lekmerProduct, colorVariants), VariableEncoding.None);
					}
				}
				else
				{
					fragmentItem.AddCondition("Product.HasVariants", false);
					fragmentItem.AddVariable("Iterate:ColorOption", string.Empty, VariableEncoding.None);
				}
			}
			else
			{
				fragmentItem.AddCondition("Product.HasVariants", false);
				fragmentItem.AddVariable("Iterate:ColorOption", string.Empty, VariableEncoding.None);
			}

			string ratingsSummary = RenderRatingSummary(lekmerProduct.Id, lekmerProduct.CategoryId);
			fragmentItem.AddCondition("Product.HasRatingSummary", !string.IsNullOrEmpty(ratingsSummary));
			fragmentItem.AddVariable("Rating", ratingsSummary, VariableEncoding.None);

			return fragmentItem.Render();
		}

		protected virtual string RenderSizeOptions(ILekmerProduct product)
		{
			if (product == null)
			{
				return string.Empty;
			}

			var optionBuilder = new StringBuilder();

			var productSizeService = IoC.Resolve<IProductSizeService>();
			Collection<IProductSize> productSizes = productSizeService.GetAllByProduct(product.Id);

			if (productSizes != null)
			{
				foreach (var size in productSizes)
				{
					var fragmentOption = Template.GetFragment("ProductSizeOption");
					fragmentOption.AddEntity(size);
					optionBuilder.AppendLine(fragmentOption.Render());
				}
			}

			return optionBuilder.ToString();
		}

		protected virtual string RenderColorOptions(ILekmerProduct product, ProductCollection colorVariants)
		{
			var optionBuilder = new StringBuilder();

			if (colorVariants != null)
			{
				foreach (var item in colorVariants)
				{
					var fragmentOption = Template.GetFragment("ProductColorOption");
					fragmentOption.AddEntity(item);
					optionBuilder.AppendLine(fragmentOption.Render());
				}
			}

			return optionBuilder.ToString();
		}

		protected virtual ProductCollection GetColorVariants(ILekmerProduct product)
		{
			if (product == null) return null;

			var id = product.Id;
			var variants = LekmerProductService.GetAllByProductAndRelationType(UserContext.Current, id, RelationListType.Variant.ToString()).ToDictionary(i => i.Id, i => i);
			var colorVariants = LekmerProductService.GetAllByProductAndRelationType(UserContext.Current, id, RelationListType.ColorVariant.ToString()).ToDictionary(i => i.Id, i => i);

			var results = new Dictionary<int, IProduct>(variants);
			foreach (var colorVariant in colorVariants)
			{
				if (results.ContainsKey(colorVariant.Key)) continue;

				results.Add(colorVariant.Key, colorVariant.Value);
			}
			return new ProductCollection(results.Values);
		}


		protected virtual string RenderRatingSummary(int productId, int categoryId)
		{
			return _gridControlHelper.RenderRating(productId, categoryId);
		}
	}
}