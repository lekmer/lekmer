using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product;

namespace Litium.Lekmer.ProductFilter.Web
{
	public class SelectedFilterOptions
	{
		private readonly Collection<int> _productIds = new Collection<int>();
		private readonly Collection<int> _brandIds = new Collection<int>();
		private readonly Collection<IEnumerable<int>> _groupedTagIds = new Collection<IEnumerable<int>>();
		private readonly Collection<int> _level1CategoryIds = new Collection<int>();
		private readonly Collection<int> _level2CategoryIds = new Collection<int>();
		private readonly Collection<int> _level3CategoryIds = new Collection<int>();
		private readonly Collection<int> _sizeIds = new Collection<int>();
		private readonly Collection<int> _productTypeIds = new Collection<int>();

		// Properties
		public Collection<int> ProductIds
		{
			get { return _productIds; }
		}

		public Collection<int> BrandIds
		{
			get { return _brandIds; }
		}

		public IAgeIntervalBand AgeInterval { get; set; }

		public IPriceIntervalBand PriceInterval { get; set; }

		[SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures")]
		public Collection<IEnumerable<int>> GroupedTagIds
		{
			get { return _groupedTagIds; }
		}

		public Collection<int> Level1CategoryIds
		{
			get { return _level1CategoryIds; }
		}

		public Collection<int> Level2CategoryIds
		{
			get { return _level2CategoryIds; }
		}

		public Collection<int> Level3CategoryIds
		{
			get { return _level3CategoryIds; }
		}

		public Collection<int> SizeIds
		{
			get { return _sizeIds; }
		}

		public Collection<int> ProductTypeIds
		{
			get { return _productTypeIds; }
		}

		public SortOption SortOption { get; set; }

		public bool UseSecondaryTemplate { get; set; }

		public int PageSize { get; set; }

		public SearchMethodType SearchMethodType { get; set; }

		// Methods
		public void SetSortOption(string value)
		{
			if (value == null || !Enum.IsDefined(typeof(SortOption), value))
				return;

			SortOption = (SortOption)Enum.Parse(typeof(SortOption), value);
		}

		public FilterQuery BuildQuery(AvailableFilterOptions availableFilterOptions)
		{
			if (availableFilterOptions == null) throw new ArgumentNullException("availableFilterOptions");

			var query = new FilterQuery();
			PopulateCollection(query.BrandIds, BrandIds);


			PopulateAgeInterval(query, availableFilterOptions);
			PopulatePriceInterval(query, availableFilterOptions);

			PopulateCollection(query.GroupedTagIds, GroupedTagIds);
			PopulateCollection(query.Level1CategoryIds, Level1CategoryIds);
			PopulateCollection(query.Level2CategoryIds, Level2CategoryIds);
			PopulateCollection(query.Level3CategoryIds, Level3CategoryIds);
			PopulateCollection(query.SizeIds, SizeIds);
			PopulateCollection(query.ProductIds, ProductIds);

			PopulateProductTypes(query, availableFilterOptions);

			query.SortOption = SortOption;

			return query;
		}
		public FilterQuery BuildEsalesQuery(AvailableFilterOptions availableFilterOptions)
		{
			var query = new FilterQuery();
			PopulateCollection(query.BrandIds, BrandIds);
			PopulateCollection(query.Level1CategoryIds, Level1CategoryIds);
			PopulateCollection(query.Level2CategoryIds, Level2CategoryIds);
			PopulateCollection(query.Level3CategoryIds, Level3CategoryIds);
			PopulateAgeInterval(query, availableFilterOptions);
			PopulatePriceInterval(query, availableFilterOptions);
			PopulateCollection(query.SizeIds, SizeIds);
			PopulateCollection(query.GroupedTagIds, GroupedTagIds);
			query.SortOption = SortOption;
			return query;
		}

		private static void PopulateCollection<T>(ICollection<T> targetCollection, IEnumerable<T> source)
		{
			if (targetCollection == null) throw new ArgumentNullException("targetCollection");
			if (source == null) throw new ArgumentNullException("source");

			foreach (T item in source)
			{
				targetCollection.Add(item);
			}
		}

		protected virtual void PopulateAgeInterval(FilterQuery query, AvailableFilterOptions availableFilterOptions)
		{
			query.AgeInterval = AgeInterval;
			if (query.AgeInterval != null && availableFilterOptions.AgeIntervalOuter != null)
			{
				if (query.AgeInterval.FromMonth.HasValue
					&& availableFilterOptions.AgeIntervalOuter.FromMonth.HasValue
					&& query.AgeInterval.FromMonth <= availableFilterOptions.AgeIntervalOuter.FromMonth)
				{
					query.AgeInterval.FromMonth = null;
				}

				if (query.AgeInterval.ToMonth.HasValue
					&& availableFilterOptions.AgeIntervalOuter.ToMonth.HasValue
					&& query.AgeInterval.ToMonth >= availableFilterOptions.AgeIntervalOuter.ToMonth)
				{
					query.AgeInterval.ToMonth = null;
				}
			}
		}

		protected virtual void PopulatePriceInterval(FilterQuery query, AvailableFilterOptions availableFilterOptions)
		{
			query.PriceInterval = PriceInterval;
			if (query.PriceInterval != null && availableFilterOptions.PriceIntervalOuter != null)
			{
				if (query.PriceInterval.From.HasValue
					&& availableFilterOptions.PriceIntervalOuter.From.HasValue
					&& query.PriceInterval.From <= availableFilterOptions.PriceIntervalOuter.From)
				{
					query.PriceInterval.From = null;
				}

				if (query.PriceInterval.To.HasValue
					&& availableFilterOptions.PriceIntervalOuter.To.HasValue
					&& query.PriceInterval.To >= availableFilterOptions.PriceIntervalOuter.To)
				{
					query.PriceInterval.To = null;
				}
			}
		}

		protected virtual void PopulateProductTypes(FilterQuery query, AvailableFilterOptions availableFilterOptions)
		{
			var productTypeIdDictionary = availableFilterOptions.ProductTypes
				.ToDictionary(pt => pt.Id, pt => pt.Id);

			var selectedProductTypeIds = ProductTypeIds.Where(productTypeIdDictionary.ContainsKey).ToList();

			// if all types selected, we just keep list empty
			if (selectedProductTypeIds.Count != availableFilterOptions.ProductTypes.Count())
			{
				query.ProductTypeIds.AddRange(selectedProductTypeIds);
			}
		}
	}
}