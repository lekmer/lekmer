﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Litium.Lekmer.ServiceCenter.Contract.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using log4net;

namespace Litium.Lekmer.ServiceCenter.Web
{
	public class BlockSendCaseByEmailControl : BlockControlBase<IBlock>
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private CaseForm _caseForm;
		private bool _isError;
		private readonly IBlockService _blockService;
		private readonly IServiceCenterService _serviceCenterService;

		private CaseForm CaseForm
		{
			get
			{
				return _caseForm ?? (_caseForm = new CaseForm(ResolveUrl(ContentNodeTreeItem.Url)));
			}
		}

		public BlockSendCaseByEmailControl(ITemplateFactory factory, IBlockService blockService, IServiceCenterService serviceCenterService)
			: base(factory)
		{
			_blockService = blockService;
			_serviceCenterService = serviceCenterService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			ValidationResult validationResult = null;
			bool isSendCase = false;
			if (CaseForm.IsFormPostBack)
			{
				CaseForm.MapFromRequest();
				validationResult = CaseForm.Validate();
				if (validationResult.IsValid)
				{
					var categoryId = CaseForm.CategoryId;
					var categoryValue = Channel.Current.CommonName == "Netherlands" && categoryId != null && _categoriesNl.ContainsKey(categoryId)
							? _categoriesNl[categoryId]
							: string.Empty;

					try
					{
						_serviceCenterService.SendCaseByEmail(
							Channel.Current,
							categoryId,
							categoryValue,
							CaseForm.FirstName,
							CaseForm.LastName,
							CaseForm.Email,
							CaseForm.CustomerId,
							CaseForm.OrderNumber,
							string.Empty,
							string.Empty,
							CaseForm.Message);

						isSendCase = true;
					}
					catch (Exception ex)
					{
						_log.Error("Error occurred while sending case by email. ", ex);
						_isError = true;
					}
				}
			}

			return RenderCaseForm(validationResult, isSendCase);
		}

		protected virtual BlockContent RenderCaseForm(ValidationResult validationResult, bool isSendCase)
		{
			Fragment fragmentContent = Template.GetFragment("Content");

			CaseForm.MapToFragment(fragmentContent);
			RenderValidationResult(fragmentContent, validationResult);
			RenderError(fragmentContent);
			fragmentContent.AddVariable("Form.CategoryOptionsList", RenderOptionList(CaseForm.CategoryId), VariableEncoding.None);
			fragmentContent.AddCondition("is_confirmed", isSendCase);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		protected virtual void RenderValidationResult(Fragment fragmentContent, ValidationResult validationResult)
		{
			string validationError = null;
			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}
			fragmentContent.AddVariable("ValidationError", validationError, VariableEncoding.None);
		}
		protected virtual void RenderError(Fragment fragmentContent)
		{
			if (_isError)
			{
				fragmentContent.AddCondition("is_error", true);
				Fragment fragmentError = Template.GetFragment("Error");
				fragmentContent.AddVariable("Error", fragmentError.Render());
			}
			else
			{
				fragmentContent.AddCondition("is_error", false);
			}
		}

		protected virtual string RenderOptionList(string selectedCategory)
		{
			Dictionary<string, string> categories = Channel.Current.CommonName == "Netherlands"
				? new Dictionary<string, string>(_categoriesNl)
				: new Dictionary<string, string>();

			var rows = new StringBuilder();
			foreach (var category in categories)
			{
				Fragment fragmentOption = Template.GetFragment("Option");
				fragmentOption.AddVariable("value", category.Key);
				fragmentOption.AddVariable("text", category.Value);

				if (!selectedCategory.IsNullOrEmpty() && selectedCategory == category.Key)
				{
					fragmentOption.AddCondition("is_selected", true);
				}
				else
				{
					fragmentOption.AddCondition("is_selected", false);
				}

				rows.AppendLine(fragmentOption.Render());
			}
			return rows.ToString();
		}

		protected virtual string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		private Dictionary<string, string> _categoriesNl = new Dictionary<string, string>
			{
				{"01", "Order en levering"},
				{"02", "Bestelling veranderen"},
				{"03", "Ik heb geen orderbevestiging gekregen/ technische fout"},
				{"04", "Ik heb een verkeerde levering ontvangen"},
				{"05", "Retour en ruilen"},
				{"06", "Factuur"},
				{"07", "Betaalkaart"},
				{"08", "Product & assortiment"},
				{"09", "Reclamatie"},
				{"10", "Kortingscode"},
				{"11", "Sponsoring/samenwerking"},
				{"12", "Overige"}
			};

	}
}