using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Order
{
	public static class OrderRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null) throw new ArgumentNullException("routes");

			routes.Add(new Route(@"order/autocheckout", new RouteHandler("~/Order/AutoCheckout.ashx")));
			//routes.Add(new Route(@"cart/add", new RouteHandler("~/Order/CartAdd.ashx")));
			//routes.Add(new Route(@"cart/delete", new RouteHandler("~/Order/CartDelete.ashx")));
			routes.Add(new OrderRoute(@"{*path}", new RouteHandler("~/Order/OrderDetailPage.ashx")));
			routes.Add(new Route(@"ChangeCheckout", new RouteHandler("~/Order/ChangeCheckout.ashx")));
		}
	}
}