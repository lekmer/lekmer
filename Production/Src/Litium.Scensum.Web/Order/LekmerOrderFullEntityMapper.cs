using Litium.Lekmer.Core;
using Litium.Lekmer.Order;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Template.Engine;
using Channel = Litium.Scensum.Core.Web.Channel;

namespace Litium.Scensum.Web.Order
{
	public class LekmerOrderFullEntityMapper : OrderFullEntityMapper
	{
		public LekmerOrderFullEntityMapper(IFormatter formatter, ICountryService countryService) : base(formatter, countryService)
		{
		}

		public override void AddEntityVariables(Fragment fragment, IOrderFull item)
		{
			base.AddEntityVariables(fragment, item);

			var lekmerOrder = (ILekmerOrderFull)item;
			var formatter = IoC.Resolve<IFormatter>();
			fragment.AddVariable("Order.FreightVat", formatter.FormatPrice(Channel.Current, lekmerOrder.GetActualFreightCostVat()));

			// Optional freight
			fragment.AddCondition("Order.HasOptionalFreightCost", lekmerOrder.OptionalFreightCost.HasValue);
			fragment.AddVariable("Order.OptionalFreightCost", formatter.FormatPrice(Channel.Current, lekmerOrder.GetOptionalFreightCost()));
			fragment.AddVariable("Order.OptionalFreightCostVat", formatter.FormatPrice(Channel.Current, lekmerOrder.GetOptionalFreightCostVat()));

			// Diapers freight
			fragment.AddCondition("Order.HasDiapersFreightCost", lekmerOrder.DiapersFreightCost.HasValue);
			fragment.AddVariable("Order.DiapersFreightCost", formatter.FormatPrice(Channel.Current, lekmerOrder.GetDiapersFreightCost()));
			fragment.AddVariable("Order.DiapersFreightCostVat", formatter.FormatPrice(Channel.Current, lekmerOrder.GetDiapersFreightCostVat()));

			fragment.AddVariable("Order.PaymentCost", formatter.FormatPrice(Channel.Current, lekmerOrder.PaymentCost));
			fragment.AddVariable("Order.PaymentVat", formatter.FormatPrice(Channel.Current, lekmerOrder.GetPaymentCostVat()));
			fragment.AddVariable("Order.Status", AliasHelper.GetAliasValue("Order.Status." + lekmerOrder.OrderStatus.CommonName), VariableEncoding.None);

			var orderHelper = new OrderHelper();
			orderHelper.RenderOrderPrices(fragment, lekmerOrder, (ILekmerFormatter)formatter, Channel.Current);
		}

		protected override void AddAddressVariables(Fragment fragment, string prefix, IOrderAddress address)
		{
			base.AddAddressVariables(fragment, prefix, address);

			var lekmerOrderAddress = address as ILekmerOrderAddress;

			if (lekmerOrderAddress != null)
			{
				fragment.AddVariable(prefix + ".HouseNumber", lekmerOrderAddress.HouseNumber);
				fragment.AddVariable(prefix + ".HouseExtension", lekmerOrderAddress.HouseExtension);
			}
		}
	}
}