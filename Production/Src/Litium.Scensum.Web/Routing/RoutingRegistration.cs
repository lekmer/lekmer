using System.Web.Routing;
using Litium.Scensum.Web.Avail;
using Litium.Scensum.Web.Error;
using Litium.Scensum.Web.Lekmer;
using Litium.Scensum.Web.Media;
using Litium.Scensum.Web.Order;
using Litium.Scensum.Web.Search;
using Litium.Scensum.Web.SiteStructure;

namespace Litium.Scensum.Web
{
	public static class RoutingRegistration
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			if (routes == null)
			{
				return;
			}

			routes.RouteExistingFiles = true;

			routes.Add(new RootRoute(@"", null));

			/*
			routes.Add(new BlockRoute(@"{*url}", new RouteHandler("~/Lekmer/Block.ashx")));
			routes.Add(new Route(@"search/incremental-v2", new RouteHandler("~/Lekmer/IncrementalEsalesSearchV2.ashx")));
			routes.Add(new Route(@"search/incremental", new RouteHandler("~/Lekmer/IncrementalEsalesSearch.ashx")));
			routes.Add(new Route(@"search/incremental-old", new RouteHandler("~/Lekmer/IncrementalSearch.ashx")));
			routes.Add(new Route(@"cart/add", new RouteHandler("~/Lekmer/CartAdd.ashx")));
			routes.Add(new Route(@"cart/delete", new RouteHandler("~/Lekmer/CartDelete.ashx")));
			routes.Add(new Route(@"affiliate/tradedoubler", new RouteHandler("~/Lekmer/TradeDoubler.ashx")));
			routes.Add(new DeviceDetectionRoute(@"device/{action}", new RouteHandler("~/Lekmer/DeviceDetectionHandler.ashx")));
			routes.Add(new DeviceDetectionRoute(@"device/{action}/{type}", new RouteHandler("~/Lekmer/DeviceDetectionHandler.ashx")));
			routes.Add(new Route(@"robots.txt", new StopRoutingHandler()));
			routes.Add(new Route(@"Lekmer/ProductCacheCleaner.asmx", new StopRoutingHandler()));
			routes.Add(new Route(@"Lekmer/ProductCacheCleaner.asmx/{*command}", new StopRoutingHandler()));
			routes.Add(new OldBrandRoute(@"varumarke/{brand}", new RouteHandler("~/Lekmer/OldBrandRedirect.ashx")));*/
			LekmerRoutingRegistration.RegisterRoutes(routes);

			/*routes.Add(new Route("media/{*pathInfo}", new StopRoutingHandler()));*/
			MediaRoutingRegistration.RegisterRoutes(routes);

			/*
			routes.Add(new ProductRoute(@"{title}-{id}", null, new RouteValueDictionary {{"id", @"\d+"}}, new RouteHandler("~/Product/ProductPage.ashx")));
			routes.Add(new ProductRoute(@"{title}-{id}/{sitestructurecommonname}", null, new RouteValueDictionary { { "id", @"\d+" } }, new RouteHandler("~/Product/ProductPage.ashx")));*/
			//ProductRoutingRegistration.RegisterRoutes(routes);

			/*
			routes.Add(new Route(@"order/autocheckout", new RouteHandler("~/Order/AutoCheckout.ashx")));
			//routes.Add(new Route(@"cart/add", new RouteHandler("~/Order/CartAdd.ashx")));
			//routes.Add(new Route(@"cart/delete", new RouteHandler("~/Order/CartDelete.ashx")));
			routes.Add(new OrderRoute(@"{*path}", new RouteHandler("~/Order/OrderDetailPage.ashx")));
			routes.Add(new Route(@"ChangeCheckout", new RouteHandler("~/Order/ChangeCheckout.ashx")));*/
			OrderRoutingRegistration.RegisterRoutes(routes);

			/*routes.Add(new Route(@"signout", new RouteHandler("~/Customer/SignOut.ashx")));*/
			CustomerRoutingRegistration.RegisterRoutes(routes);

			/*
			routes.Add(new Route(@"search/autocomplete", new RouteHandler("~/Search/AutoCompleteEsales.ashx")));
			routes.Add(new Route(@"search/autocomplete-old", new RouteHandler("~/Search/AutoComplete.ashx")));
			routes.Add(new Route(@"search/didyoumean", new RouteHandler("~/Search/DidYouMeanEsales.ashx")));*/
			SearchRoutingRegistration.RegisterRoutes(routes);

			/*routes.Add(new ContentPageRoute(@"{*url}", new RouteHandler("~/SiteStructure/ContentPage.ashx")));*/
			SiteStructureRoutingRegistration.RegisterRoutes(routes);

			/*routes.Add(new LekmerProductRoute(@"{*url}", new RouteHandler("~/Product/ProductPage.ashx")));*/
			LekmerProductRoutingRegistration.RegisterRoutes(routes);

			/*routes.Add(new ContentPageRedirectRoute(@"{*url}", null));*/
			SiteStructureRoutingRegistration.RegisterRedirectRoutes(routes);

			/*
			routes.Add(new Route(@"avail", new RouteHandler("~/Avail/AvailCombine.ashx")));
			routes.Add(new Route(@"oldavail", new RouteHandler("~/Avail/AvailHandler.ashx")));*/
			AvailRoutingRegistration.RegisterRoutes(routes);

			routes.Add(new Route("trace.axd", new StopRoutingHandler()));
			routes.Add(new Route(@"wishlist", new RouteHandler("~/Lekmer/WishList.ashx")));
			routes.Add(new Route(@"wishlisttocart", new RouteHandler("~/Lekmer/WishListToCart.ashx")));
			routes.Add(new Route("diagnostics/cachediagnostics.aspx", new StopRoutingHandler()));
			routes.Add(new Route("crossdomain.xml", new StopRoutingHandler()));

			routes.Add(new ProductUrlHistoryRoute(@"{*url}", new RouteHandler("~/Product/ProductPage.ashx")));
			routes.Add(new ContentPageUrlHistoryRoute(@"{*url}", null));

			/*routes.Add(new OldUrlRoute(@"{*url}", new RouteHandler("~/Lekmer/OldUrlRedirect.ashx")));*/
			LekmerOldUrlRoutingRegistration.RegisterRoutes(routes);

			/*
			routes.Add(new Route(@"exceptiontest", new RouteHandler("~/Error/ExceptionTest.ashx")));
			routes.Add(new Route(@"{*url}", new RouteHandler("~/Error/PageNotFound.ashx")));*/
			ErrorRoutingRegistration.RegisterRoutes(routes);
		}
	}
}