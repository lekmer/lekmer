﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using Litium.Lekmer.SiteStructure.Web;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;

namespace Litium.Scensum.Web.Lekmer
{
	/// <summary>
	/// Summary description for BrandSelection
	/// </summary>
	public class BrandSelection : ContentPage
	{

		protected override void ProcessRequest()
		{
			var brandId = Items["BrandId"] as int?;

			if (!brandId.HasValue)
			{
				PageNotFound();
				return;
			}


			base.ProcessRequest();
		}

		private static void PageNotFound()
		{
			throw new HttpException(404, "Not found");
		}

		protected override IContentPageFull GetContentPageCore()
		{
			var contentPage = (IContentPage)Items["ContentPage"];

			var contentPageService = IoC.Resolve<IContentPageService>();
			IContentPageFull contentPageFull = contentPageService.GetFullById(UserContext.Current, contentPage.Id);

			return contentPageFull;
		}

		protected override IContentNodeTreeItem GetContentNodeTreeItemCore()
		{
			return (IContentNodeTreeItem)Items["ContentNodeTreeItem"];
		}

	}

}