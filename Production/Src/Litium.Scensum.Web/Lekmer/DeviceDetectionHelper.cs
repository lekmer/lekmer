using Litium.Lekmer.Common;
using Litium.Scensum.Foundation;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Lekmer
{
	public static class DeviceDetectionHelper
	{
		public static void AddSiteStructureVariables(Fragment fragmentContent)
		{
			var commonSession = IoC.Resolve<ICommonSession>();

			fragmentContent.AddCondition("Device.IsDesktop", commonSession.IsDesktop);
			fragmentContent.AddCondition("Device.IsMobile", commonSession.IsMobile);
			fragmentContent.AddCondition("Device.IsViewPortDesktop", commonSession.IsViewportDesktop);
			fragmentContent.AddCondition("Device.IsViewPortMobile", commonSession.IsViewportMobile);
		}
	}
}