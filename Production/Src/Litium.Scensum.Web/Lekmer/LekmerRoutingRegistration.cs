using System;
using System.Web.Routing;

namespace Litium.Scensum.Web.Lekmer
{
	public static class LekmerRoutingRegistration
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			if (routes == null)
			{
				throw new ArgumentNullException("routes");
			}

			routes.Add(new BlockRoute(@"{*url}", new RouteHandler("~/Lekmer/Block.ashx")));
			routes.Add(new BrandRoute(@"{*url}", new RouteHandler("~/Lekmer/BrandSelection.ashx")));
			routes.Add(new Route(@"search/incremental-v2", new RouteHandler("~/Lekmer/IncrementalEsalesSearchV2.ashx")));
			routes.Add(new Route(@"search/incremental", new RouteHandler("~/Lekmer/IncrementalEsalesSearch.ashx")));
			routes.Add(new Route(@"search/incremental-old", new RouteHandler("~/Lekmer/IncrementalSearch.ashx")));
			routes.Add(new Route(@"cart/add", new RouteHandler("~/Lekmer/CartAdd.ashx")));
			routes.Add(new Route(@"cart/delete", new RouteHandler("~/Lekmer/CartDelete.ashx")));
			routes.Add(new Route(@"affiliate/tradedoubler", new RouteHandler("~/Lekmer/TradeDoubler.ashx")));
			routes.Add(new DeviceDetectionRoute(@"device/{action}", new RouteHandler("~/Lekmer/DeviceDetectionHandler.ashx")));
			routes.Add(new DeviceDetectionRoute(@"device/{action}/{type}", new RouteHandler("~/Lekmer/DeviceDetectionHandler.ashx")));
			routes.Add(new Route(@"robots.txt", new StopRoutingHandler()));
			routes.Add(new Route(@"Lekmer/ProductCacheCleaner.asmx", new StopRoutingHandler()));
			routes.Add(new Route(@"Lekmer/ProductCacheCleaner.asmx/{*command}", new StopRoutingHandler()));

			routes.Add(new OldBrandRoute(@"varumarke/{brand}", new RouteHandler("~/Lekmer/OldBrandRedirect.ashx")));
		}
	}
}