using System;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Routing;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Scensum.Web.Lekmer
{
	public class BlockRoute : Route
	{
		private static readonly Regex _blockRouteRegex = new Regex(@"(.+\/?)?block-(\d+)", RegexOptions.IgnoreCase | RegexOptions.Compiled);

		public BlockRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null) throw new ArgumentNullException("httpContext");

			RouteData data = base.GetRouteData(httpContext);
			string routeUrl = (data.Values["url"] ?? "").ToString();


			Match blockRouteMatch = _blockRouteRegex.Match(routeUrl);
			if (!blockRouteMatch.Success) return null;

			string url = blockRouteMatch.Groups[1].Value;
			string urlBlockId = blockRouteMatch.Groups[2].Value;

			int blockId;
			if (!int.TryParse(urlBlockId, out blockId))
			{
				return null;
			}

			var contentNodeService = IoC.Resolve<IContentNodeService>();
			IContentNodeTree contentNodeTree = contentNodeService.GetAllAsTree(UserContext.Current);

			IContentNodeTreeItem item;
			if (string.IsNullOrEmpty(url))
			{
				var siteStructureRegistryService = IoC.Resolve<ISiteStructureRegistryService>();
				ISiteStructureRegistry siteStructureRegistry =
					siteStructureRegistryService.GetByChannel(UserContext.Current);
				if (siteStructureRegistry == null || !siteStructureRegistry.StartContentNodeId.HasValue) return null;

				item = contentNodeTree.FindItemById(siteStructureRegistry.StartContentNodeId.Value);
			}
			else
			{
				item = contentNodeTree.FindItemByUrl(url);
			}

			if (item == null) return null;

			var contentPage = item.ContentNode as IContentPage;
			if (contentPage == null) return null;

			httpContext.Items["ContentPage"] = contentPage;
			httpContext.Items["ContentNodeTreeItem"] = item;
			httpContext.Items["BlockId"] = blockId;

			return data;
		}
	}
}