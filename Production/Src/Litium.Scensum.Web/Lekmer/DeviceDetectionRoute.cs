using System;
using System.Web;
using System.Web.Routing;

namespace Litium.Scensum.Web.Lekmer
{
	public class DeviceDetectionRoute : Route
	{
		public DeviceDetectionRoute(string url, IRouteHandler routeHandler)
			: base(url, routeHandler)
		{
		}

		public override RouteData GetRouteData(HttpContextBase httpContext)
		{
			if (httpContext == null)
			{
				throw new ArgumentNullException("httpContext");
			}

			RouteData data = base.GetRouteData(httpContext);
			if (data != null)
			{
				if (data.Values.ContainsKey("action"))
				{
					httpContext.Items["device-detection-action"] = data.Values["action"];
				}
				if (data.Values.ContainsKey("type"))
				{
					httpContext.Items["device-detection-type"] = data.Values["type"];
				}
			}

			return data;
		}
	}
}