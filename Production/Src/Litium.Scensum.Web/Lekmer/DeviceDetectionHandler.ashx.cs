﻿using Litium.Lekmer.Common;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;

namespace Litium.Scensum.Web.Lekmer
{
	public class DeviceDetectionHandler : HandlerBase
	{
		private ICommonSession _commonSession;

		protected ICommonSession CommonSession
		{
			get
			{
				return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>());
			}
		}

		protected override void ProcessRequest()
		{
			string action = (string)Items["device-detection-action"];
			if (action == "reset")
			{
				CommonSession.ResetDetectedDevice();
			}
			else if (action == "set")
			{
				string type = (string)Items["device-detection-type"];
				CommonSession.SetDevice(type);
			}

			Response.ContentType = "text/html";
			if (CommonSession.IsViewportDesktop)
			{
				Response.Write("<!-- Device: desktop -->");
			}
			else if (CommonSession.IsViewportMobile)
			{
				Response.Write("<!-- Device: mobile -->");
			}
			else
			{
				Response.Write("<!-- Device: unknown -->");
			}
		}
	}
}