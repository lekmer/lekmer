﻿using System;
using System.Globalization;
using System.Text;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.ProductSearch;
using Channel=Litium.Scensum.Core.Web.Channel;
using Encoder=Litium.Scensum.Foundation.Utilities.Encoder;
using UserContext=Litium.Scensum.Core.Web.UserContext;

namespace Litium.Scensum.Web.Lekmer
{
	public class IncrementalSearch : HandlerBase
	{
		private const string _imageSizeCommonName = "minithumbnail";
		private const int _defaultNumberOfItemsToShow = 10;
		private const int _maximumNumberOfItemsToShow = 100;

		private IMediaUrlService _mediaUrlService;

		protected int NumberOfItemsToShow
		{
			get
			{
				return Math.Min(Request.QueryString.GetInt32OrNull("show-items") ?? _defaultNumberOfItemsToShow, _maximumNumberOfItemsToShow);
			}
		}

		protected string SearchQuery
		{
			get
			{
				return Request.QueryString["q"];
			}
		}

		protected IMediaUrlService MediaUrlService
		{
			get
			{
				return _mediaUrlService ?? (_mediaUrlService = IoC.Resolve<IMediaUrlService>());
			}
		}

		protected override void ProcessRequest()
		{
			int showItems = NumberOfItemsToShow;
			string searchQuery = SearchQuery;

			ProductCollection products = searchQuery.HasValue() ? SearchProducts(searchQuery, showItems) : new ProductCollection();

			Response.ContentType = "application/json";
			Response.Write(BuildJsonResponse(products));
		}

		private string BuildJsonResponse(ProductCollection products)
		{
			if (products == null)
			{
				throw new ArgumentNullException("products");
			}

			var formatter = IoC.Resolve<IFormatter>();

			const string jsonProductFormat = "name: '{0}', price: '{1}', image: '{2}', productUrl: '{3}', campaignPrice: '{4}'";

			var json = new StringBuilder();
			json.Append("{");
			json.Append("productResults:[");
			foreach (IProduct product in products)
			{
				var lekmerProduct = (ILekmerProduct) product;

				string price = formatter.FormatPrice(Channel.Current, product.Price.PriceIncludingVat);
				
				string campaignPrice = price;
				if (product.CampaignInfo != null)
				{
					campaignPrice = formatter.FormatPrice(Channel.Current, product.CampaignInfo.Price.IncludingVat);
				}

				string imageUrl = GetImageUrl(product) ?? string.Empty;
				string productUrl = UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl);

				json.Append(
					"{" +
					string.Format(
						CultureInfo.CurrentCulture, jsonProductFormat,
						Encoder.EncodeJavaScript(product.DisplayTitle),
						Encoder.EncodeJavaScript(price),
						Encoder.EncodeJavaScript(imageUrl),
						Encoder.EncodeJavaScript(productUrl),
						Encoder.EncodeJavaScript(campaignPrice))
					+ "},");
			}

			if (products.TotalCount > 0)
			{
				json.Remove(json.Length - 1, 1);
			}
			json.Append("]");
			json.AppendFormat(",totalCount: {0}", products.TotalCount);
			json.Append("}");

			return json.ToString();
		}

		private string GetImageUrl(IProduct product)
		{
			if (product == null)
			{
				throw new ArgumentNullException("product");
			}

			string imageUrl = null;
			if (product.Image != null)
			{
				string mediaUrl = MediaUrlService.ResolveMediaArchiveUrl(Channel.Current, product.Image);
				imageUrl = MediaUrlFormer.ResolveCustomSizeImageUrl(mediaUrl, product.Image, product.DisplayTitle, _imageSizeCommonName);
			}
			return imageUrl;
		}

		private static ProductCollection SearchProducts(string searchQuery, int showItems)
		{
			var productSearchService = (LekmerProductSearchService)IoC.Resolve<IProductSearchService>();
			ProductCollection productCollection = productSearchService.Search(UserContext.Current, searchQuery, 1, showItems);
			return productCollection;
		}
	}
}