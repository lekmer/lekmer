using System;
using Litium.Scensum.Foundation;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.Web.Customer
{
	public static class CustomerMasterHelper
	{
		public static void AddCustomerUserVariables(Fragment fragmentContent)
		{
			if (fragmentContent == null) throw new ArgumentNullException("fragmentContent");

			var customerSession = IoC.Resolve<ICustomerSession>();

			if (customerSession.IsSignedIn)
			{
				fragmentContent.AddEntity(customerSession.SignedInCustomer);
			}
			fragmentContent.AddCondition("User.IsSignedIn", customerSession.IsSignedIn);
		}
	}
}