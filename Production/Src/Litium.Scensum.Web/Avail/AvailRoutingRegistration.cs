﻿using System;
using System.Collections.Generic;
using System.Web.Routing;

namespace Litium.Scensum.Web.Avail
{
	public static class AvailRoutingRegistration
	{
		public static void RegisterRoutes(ICollection<RouteBase> routes)
		{
			if (routes == null)
			{
				throw new ArgumentNullException("routes");
			}

			routes.Add(new Route(@"avail", new RouteHandler("~/Avail/AvailCombine.ashx")));
			routes.Add(new Route(@"oldavail", new RouteHandler("~/Avail/AvailHandler.ashx")));
		}
	}
}