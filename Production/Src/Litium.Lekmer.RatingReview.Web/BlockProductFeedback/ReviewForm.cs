using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using Litium.Lekmer.RatingReview;
using Litium.Lekmer.RatingReview.Contract.BusinessObject;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Customer;
using Litium.Scensum.Template.Engine;

namespace Litium.Scensum.RatingReview.Web
{
	public class ReviewForm : ControlBase
	{
		private readonly string _postUrl;

		public ReviewForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals("review"); }
		}

		public virtual string PostUrl
		{
			get { return _postUrl; }
		}

		public string AuthorName { get; set; }

		public string Title { get; set; }

		public string Message { get; set; }

		public List<IRatingItemProductVote> RatingItemProductVotes { get; set; }




		public ValidationResult Validate()
		{
			var validationResult = new ValidationResult();

			if (Author.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.AuthorNotProvided"));
			}
			if (Message.IsNullOrTrimmedEmpty())
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.MessageNotProvided"));
			}
			if (Message != null && Message.Length > 500)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.MessageTooLong"));
			}
			if (!Rating.HasValue || Rating.Value < 1 || Rating.Value > 5)
			{
				validationResult.Errors.Add(AliasHelper.GetAliasValue("Review.Validation.IncorrectRating"));
			}
			validationResult.Errors.Append(CaptchaForm.Validate().Errors);

			return validationResult;
		}

		public void SetDefaultValues(ICustomer customer)
		{
			if (customer != null && customer.CustomerInformation != null)
			{
				AuthorName = customer.CustomerInformation.FirstName;
			}
		}

		public void MapFromRequest()
		{
			NameValueCollection form = Request.Form;
			AuthorName = form["review-author"];
			Title = form["review-title"];
			Message = form["review-message"];
			//Rating = form.GetInt32OrNull("rating-id");
		}

		public void MapToReview(IRatingReviewFeedback ratingReviewFeedback)
		{
			if (!Validate().IsValid) throw new ValidationException("Couldn't validate the review.");

			ratingReviewFeedback.Author = Author;
			review.Message = Message;
			review.Rating = (byte) Rating;
		}

		public virtual void MapToFragment(Fragment fragment)
		{
			string rating = Rating.HasValue ? Rating.Value.ToString(CultureInfo.InvariantCulture) : string.Empty;

			fragment.AddVariable("Form.PostUrl", PostUrl);
			fragment.AddVariable("Form.Author.Value", Author);
			fragment.AddVariable("Form.Message.Value", Message);
			fragment.AddVariable("Form.Rating.Value", rating);

			for (int i = 1; i <= 5; i++)
			{
				fragment.AddCondition("Form.Rating.Equals:" + i, Rating.HasValue && Rating.Value == i);
			}
		}
	}
}