using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Foundation.QueryBuilder;

namespace Litium.Lekmer.RatingReview.Web
{
	public class BlockProductFeedbackControl : BlockProductPageControlBase<IBlockProductFeedback>
	{
		private static string _queryNameFeedbackAdded = "-feedback-added";

		private readonly IBlockProductFeedbackService _blockProductFeedbackService;
		private readonly IRatingGroupService _ratingGroupService;
		private readonly IRatingService _ratingService;
		private readonly IRatingReviewFeedbackService _ratingReviewFeedbackService;
		private readonly IRatingReviewUserLocatorService _ratingReviewUserLocatorService;
		private readonly ICustomerSession _customerSession;

		private Collection<IRatingGroup> _ratingGroups;
		private Dictionary<int, Collection<IRating>> _ratingGroupRatings;
		private Dictionary<int, IRating> _ratings;

		private FeedbackForm _feedbackForm;
		private bool _feedbackAdded;

		public BlockProductFeedbackControl(
			ITemplateFactory templateFactory,
			IBlockProductFeedbackService blockProductFeedbackService,
			IRatingGroupService ratingGroupService,
			IRatingService ratingService,
			IRatingReviewFeedbackService ratingReviewFeedbackService,
			IRatingReviewUserLocatorService ratingReviewUserLocatorService,
			ICustomerSession customerSession)
			: base(templateFactory)
		{
			_blockProductFeedbackService = blockProductFeedbackService;
			_ratingService = ratingService;
			_ratingGroupService = ratingGroupService;
			_ratingReviewFeedbackService = ratingReviewFeedbackService;
			_ratingReviewUserLocatorService = ratingReviewUserLocatorService;
			_customerSession = customerSession;
		}

		protected FeedbackForm FeedbackForm
		{
			get { return _feedbackForm ?? (_feedbackForm = CreateFeedbackForm()); }
		}

		protected override IBlockProductFeedback GetBlockById(int blockId)
		{
			return _blockProductFeedbackService.GetById(UserContext.Current, blockId);
		}


		protected override BlockContent RenderCore()
		{
			if (Product == null)
			{
				return new BlockContent(null, "[ Can't render rating&review feedback control on this page ]");
			}

			Initialize();

			var content = RenderContent();
			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new BlockContent(head, content.Render(), footer);
		}


		protected virtual string RenderFragment(string fragmentName)
		{
			var fragment = Template.GetFragment(fragmentName);

			fragment.AddEntity(Block);

			return fragment.Render();
		}

		protected virtual Fragment RenderContent()
		{
			Fragment fragment = Template.GetFragment("Content");

			fragment.AddEntity(Block);

			fragment.AddVariable("FeedbackForm", ProcessFeedbackForm, VariableEncoding.None);

			fragment.AddCondition("FeedBackAdded", _feedbackAdded);

			return fragment;
		}


		protected virtual string ProcessFeedbackForm()
		{
			var signedInCustomer = _customerSession.SignedInCustomer;

			ValidationResult validationResult = null;
			if (FeedbackForm.IsFormPostBack)
			{
				FeedbackForm.MapFromRequest();
				validationResult = FeedbackForm.Validate();
				if (validationResult.IsValid)
				{
					IRatingReviewFeedbackRecord feedback = _ratingReviewFeedbackService.CreateRecord(UserContext.Current);
					FeedbackForm.MapToFeedback(feedback);
					feedback.ProductId = Product.Id;
					feedback.RatingReviewUserId = _ratingReviewUserLocatorService.GetRatingReviewUser(UserContext.Current).Id;
					feedback.IPAddress = Request.UserHostAddress;

					_ratingReviewFeedbackService.Insert(UserContext.Current, feedback);

					_feedbackAdded = true;

					return RenderFeedbackSavedResponse();
				}
			}
			else
			{
				FeedbackForm.SetDefaultValues(signedInCustomer);
			}

			return RenderFeedbackForm(validationResult);
		}

		protected virtual string RenderFeedbackForm(ValidationResult validationResult)
		{
			string validationError = null;
			if (validationResult != null && !validationResult.IsValid)
			{
				var validationControl = new ValidationControl(validationResult.Errors);
				validationError = validationControl.Render();
			}

			Fragment fragment = Template.GetFragment("FeedbackForm");

			fragment.AddVariable("Review", RenderReview().Render(), VariableEncoding.None);
			fragment.AddVariable("RatingGroupList", RenderRatingGroupList().Render(), VariableEncoding.None);

			fragment.AddVariable("ValidationError", validationError, VariableEncoding.None);

			FeedbackForm.MapToFragmentForm(fragment);

			return fragment.Render();
		}

		protected virtual string RenderFeedbackSavedResponse()
		{
			var queryBuilder = new QueryBuilder(Request.QueryString);
			queryBuilder.ReplaceOrAdd(BlockId + _queryNameFeedbackAdded, "true");

			// Remove the page parameter to redirect to the first page.
			queryBuilder.Remove(BlockId + "-page");

			Response.Redirect("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);

			return null;
		}


		protected virtual Fragment RenderReview()
		{
			Fragment fragment = Template.GetFragment("Review");

			fragment.AddVariable("Review.MessageMaxLength", "3000");

			FeedbackForm.MapToFragmentReview(fragment);

			return fragment;
		}

		protected virtual Fragment RenderRatingGroupList()
		{
			Fragment fragment = Template.GetFragment("RatingGroupList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingGroup ratingGroup in _ratingGroups)
			{
				stringBuilder.AppendLine(RenderRatingGroup(ratingGroup).Render());
			}

			fragment.AddVariable("Iterate:RatingGroup", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingGroup(IRatingGroup ratingGroup)
		{
			Fragment fragment = Template.GetFragment("RatingGroup");

			fragment.AddEntity(ratingGroup);
			fragment.AddVariable("RatingList", RenderRatingList(ratingGroup).Render(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingList(IRatingGroup ratingGroup)
		{
			Fragment fragment = Template.GetFragment("RatingList");

			var stringBuilder = new StringBuilder();
			foreach (IRating rating in _ratingGroupRatings[ratingGroup.Id])
			{
				stringBuilder.AppendLine(RenderRating(ratingGroup, rating).Render());
			}

			fragment.AddVariable("Iterate:Rating", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRating(IRatingGroup ratingGroup, IRating rating)
		{
			Fragment fragment = Template.GetFragment("Rating");

			fragment.AddEntity(ratingGroup);
			fragment.AddEntity(rating);
			fragment.AddVariable("RatingItemList", RenderRatingItemList(rating).Render(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItemList(IRating rating)
		{
			Fragment fragment = Template.GetFragment("RatingItemList");

			var stringBuilder = new StringBuilder();
			foreach (IRatingItem ratingItem in rating.RatingItems)
			{
				stringBuilder.AppendLine(RenderRatingItem(rating, ratingItem).Render());
			}

			fragment.AddVariable("Iterate:RatingItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment;
		}

		protected virtual Fragment RenderRatingItem(IRating rating, IRatingItem ratingItem)
		{
			Fragment fragment = Template.GetFragment("RatingItem");

			fragment.AddEntity(rating);
			fragment.AddEntity(ratingItem);

			FeedbackForm.MapToFragmentRatingItem(fragment, ratingItem);

			return fragment;
		}


		protected virtual void Initialize()
		{
			_feedbackAdded = Request.QueryString.GetBooleanOrNull(BlockId + _queryNameFeedbackAdded) ?? false;

			_ratingGroupRatings = new Dictionary<int, Collection<IRating>>();
			_ratings = new Dictionary<int, IRating>();

			_ratingGroups = _ratingGroupService.GetAllByProduct(UserContext.Current, Product.Id, Product.CategoryId);

			foreach (IRatingGroup ratingGroup in _ratingGroups)
			{
				_ratingGroupRatings[ratingGroup.Id] = new Collection<IRating>();

				foreach (IRating rating in _ratingService.GetAllByGroup(UserContext.Current, ratingGroup.Id))
				{
					if (!_ratings.ContainsKey(rating.Id))
					{
						_ratings[rating.Id] = rating;
						_ratingGroupRatings[ratingGroup.Id].Add(rating);
					}
				}
			}

			FeedbackForm.Ratings = _ratings;
			FeedbackForm.RatingItems = _ratings.Values.SelectMany(r => r.RatingItems).ToDictionary(ri => ri.Id);
		}

		protected virtual FeedbackForm CreateFeedbackForm()
		{
			var queryBuilder = new QueryBuilder(Request.QueryString);
			queryBuilder.Remove(BlockId + _queryNameFeedbackAdded);

			string postUrl = UrlHelper.ResolveUrl("~" + Request.RelativeUrlWithoutQueryString() + queryBuilder);

			return new FeedbackForm(postUrl);
		}
	}
}