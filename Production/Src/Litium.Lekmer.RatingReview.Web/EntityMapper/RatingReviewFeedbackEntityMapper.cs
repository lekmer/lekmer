using System.Globalization;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class RatingReviewFeedbackEntityMapper : EntityMapper<IRatingReviewFeedback>
	{
		public override void AddEntityVariables(Fragment fragment, IRatingReviewFeedback item)
		{
			fragment.AddVariable("RatingReviewFeedback.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingReviewFeedback.LikeHit", item.LikeHit.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("RatingReviewFeedback.CreatedDate", item.CreatedDate.ToShortDateString());
			fragment.AddCondition("RatingReviewFeedback.HasOrder", item.OrderId.HasValue);
		}
	}
}