using System.Globalization;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class RatingEntityMapper : EntityMapper<IRating>
	{
		public override void AddEntityVariables(Fragment fragment, IRating item)
		{
			fragment.AddVariable("Rating.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Rating.CommonName", item.CommonName);
			fragment.AddVariable("Rating.Title", item.Title);
			fragment.AddVariable("Rating.Description", item.Description);
		}
	}
}