using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.RatingReview.Web
{
	public class LikeFeedbackForm : ControlBase
	{
		// Constants.
		private const string FORM_MODE_NAME = "mode";
		private const string SUBMIT_BUTTON_NAME = "submit-name-{0}-{1}";
		private const string SUBMIT_BUTTON_ID = "submit-id-{0}-{1}";


		// Private Members.
		private readonly string _postUrl;
		private readonly string _formModeValue;


		// Private Properties.
		private NameValueCollection RequestForm
		{
			get { return Request.Form; }
		}


		// Public Properties.
		public int PostBackFeedbackId { get; set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Dictionary<int, int> Feedbacks { get; set; }

		public virtual bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(_formModeValue); }
		}


		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="postUrl">Form post url.</param>
		/// <param name="formModeValue">Form mode value.</param>
		public LikeFeedbackForm(string postUrl, string formModeValue)
		{
			_postUrl = postUrl;
			_formModeValue = formModeValue;
		}


		public virtual void MapFromRequest(int blockId, int feedbackId)
		{
			NameValueCollection form = RequestForm;

			if (form.Get(string.Format(CultureInfo.InvariantCulture, SUBMIT_BUTTON_NAME, blockId, feedbackId)) != null)
			{
				PostBackFeedbackId = Feedbacks[feedbackId];
			}
		}

		public virtual void MapToForm(int blockId, int feedbackId, bool userCanLike, Fragment fragment)
		{
			fragment.AddVariable("Form.PostUrl", _postUrl);
			fragment.AddCondition("Form.UserCanLike", userCanLike);
			fragment.AddVariable("Form.Mode.Name", FORM_MODE_NAME);
			fragment.AddVariable("Form.Mode.Value", _formModeValue);
			fragment.AddVariable("Form.SubmitButton.Id", string.Format(CultureInfo.InvariantCulture, SUBMIT_BUTTON_ID, blockId, feedbackId));
			fragment.AddVariable("Form.SubmitButton.Name", string.Format(CultureInfo.InvariantCulture, SUBMIT_BUTTON_NAME, blockId, feedbackId));
		}
	}
}