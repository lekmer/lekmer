﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingGroupRating : BusinessObjectBase, IRatingGroupRating
	{
		private int _ratingGroupId;
		private int _ratingId;
		private int _ordinal;

		public int RatingGroupId
		{
			get { return _ratingGroupId; }
			set
			{
				CheckChanged(_ratingGroupId, value);
				_ratingGroupId = value;
			}
		}

		public int RatingId
		{
			get { return _ratingId; }
			set
			{
				CheckChanged(_ratingId, value);
				_ratingId = value;
			}
		}

		public int Ordinal
		{
			get { return _ordinal; }
			set
			{
				CheckChanged(_ordinal, value);
				_ordinal = value;
			}
		}
	}
}