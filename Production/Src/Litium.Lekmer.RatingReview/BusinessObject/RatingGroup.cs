﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingGroup : BusinessObjectBase, IRatingGroup
	{
		private int _id;
		private int _ratingGroupFolderId;
		private string _commonName;
		private string _title;
		private string _description;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int RatingGroupFolderId
		{
			get { return _ratingGroupFolderId; }
			set
			{
				CheckChanged(_ratingGroupFolderId, value);
				_ratingGroupFolderId = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}
	}
}