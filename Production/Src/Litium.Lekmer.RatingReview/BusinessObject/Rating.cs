﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class Rating : BusinessObjectBase, IRating
	{
		private int _id;
		private int _ratingFolderId;
		private int _ratingRegistryId;
		private string _commonName;
		private string _title;
		private string _description;
		private bool _commonForVariants;
		private Collection<IRatingItem> _ratingItems;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int RatingFolderId
		{
			get { return _ratingFolderId; }
			set
			{
				CheckChanged(_ratingFolderId, value);
				_ratingFolderId = value;
			}
		}

		public int RatingRegistryId
		{
			get { return _ratingRegistryId; }
			set
			{
				CheckChanged(_ratingRegistryId, value);
				_ratingRegistryId = value;
			}
		}

		public string CommonName
		{
			get { return _commonName; }
			set
			{
				CheckChanged(_commonName, value);
				_commonName = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}

		public bool CommonForVariants
		{
			get { return _commonForVariants; }
			set
			{
				CheckChanged(_commonForVariants, value);
				_commonForVariants = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<IRatingItem> RatingItems
		{
			get { return _ratingItems; }
			set
			{
				CheckChanged(_ratingItems, value);
				_ratingItems = value;
			}
		}
	}
}