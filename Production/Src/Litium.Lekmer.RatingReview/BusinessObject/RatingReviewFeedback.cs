﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingReviewFeedback : BusinessObjectBase, IRatingReviewFeedback
	{
		private int _id;
		private int _channelId;
		private int _productId;
		private int? _orderId;
		private int _ratingReviewStatusId;
		private int _likeHit;
		private bool _inappropriate;
		private string _ipAddress;
		private DateTime _createdDate;
		private int _ratingReviewUserId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		public int ProductId
		{
			get { return _productId; }
			set
			{
				CheckChanged(_productId, value);
				_productId = value;
			}
		}

		public int? OrderId
		{
			get { return _orderId; }
			set
			{
				CheckChanged(_orderId, value);
				_orderId = value;
			}
		}

		public int RatingReviewStatusId
		{
			get { return _ratingReviewStatusId; }
			set
			{
				CheckChanged(_ratingReviewStatusId, value);
				_ratingReviewStatusId = value;
			}
		}

		public int LikeHit
		{
			get { return _likeHit; }
			set
			{
				CheckChanged(_likeHit, value);
				_likeHit = value;
			}
		}

		public bool Inappropriate
		{
			get { return _inappropriate; }
			set
			{
				CheckChanged(_inappropriate, value);
				_inappropriate = value;
			}
		}

		public string IPAddress
		{
			get { return _ipAddress; }
			set
			{
				CheckChanged(_ipAddress, value);
				_ipAddress = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public int RatingReviewUserId
		{
			get { return _ratingReviewUserId; }
			set
			{
				CheckChanged(_ratingReviewUserId, value);
				_ratingReviewUserId = value;
			}
		}
	}
}