﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingItemProductScoreSummary : BusinessObjectBase, IRatingItemProductScoreSummary
	{
		private int _ratingId;
		private int _ratingItemId;
		private int _ratingItemScore;
		private int _totalHitCount;

		public int RatingId
		{
			get { return _ratingId; }
			set
			{
				CheckChanged(_ratingId, value);
				_ratingId = value;
			}
		}

		public int RatingItemId
		{
			get { return _ratingItemId; }
			set
			{
				CheckChanged(_ratingItemId, value);
				_ratingItemId = value;
			}
		}

		public int RatingItemScore
		{
			get { return _ratingItemScore; }
			set
			{
				CheckChanged(_ratingItemScore, value);
				_ratingItemScore = value;
			}
		}

		public int TotalHitCount
		{
			get { return _totalHitCount; }
			set
			{
				CheckChanged(_totalHitCount, value);
				_totalHitCount = value;
			}
		}
	}
}