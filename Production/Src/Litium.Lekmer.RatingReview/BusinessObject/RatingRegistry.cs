﻿using System;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingRegistry : IRatingRegistry
	{
		public int Id { get; set; }
		public string CommonName { get; set; }
		public string Title { get; set; }
	}
}