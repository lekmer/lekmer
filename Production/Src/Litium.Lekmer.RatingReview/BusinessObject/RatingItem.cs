﻿using System;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingItem : BusinessObjectBase, IRatingItem
	{
		private int _id;
		private int _ratingId;
		private string _title;
		private string _description;
		private int _score;
		private Collection<ITranslationGeneric> _titleTranslations;
		private Collection<ITranslationGeneric> _descriptionlations;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int RatingId
		{
			get { return _ratingId; }
			set
			{
				CheckChanged(_ratingId, value);
				_ratingId = value;
			}
		}

		public string Title
		{
			get { return _title; }
			set
			{
				CheckChanged(_title, value);
				_title = value;
			}
		}

		public string Description
		{
			get { return _description; }
			set
			{
				CheckChanged(_description, value);
				_description = value;
			}
		}

		public int Score
		{
			get { return _score; }
			set
			{
				CheckChanged(_score, value);
				_score = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ITranslationGeneric> TitleTranslations
		{
			get { return _titleTranslations; }
			set
			{
				CheckChanged(_titleTranslations, value);
				_titleTranslations = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public Collection<ITranslationGeneric> DescriptionTranslations
		{
			get { return _descriptionlations; }
			set
			{
				CheckChanged(_descriptionlations, value);
				_descriptionlations = value;
			}
		}
	}
}