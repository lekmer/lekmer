﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	[Serializable]
	public class RatingReviewFeedbackLike : BusinessObjectBase, IRatingReviewFeedbackLike
	{
		private int _id;
		private int _ratingReviewFeedbackId;
		private string _ipAddress;
		private DateTime _createdDate;
		private int _ratingReviewUserId;

		public int Id
		{
			get { return _id; }
			set
			{
				CheckChanged(_id, value);
				_id = value;
			}
		}

		public int RatingReviewFeedbackId
		{
			get { return _ratingReviewFeedbackId; }
			set
			{
				CheckChanged(_ratingReviewFeedbackId, value);
				_ratingReviewFeedbackId = value;
			}
		}

		public string IPAddress
		{
			get { return _ipAddress; }
			set
			{
				CheckChanged(_ipAddress, value);
				_ipAddress = value;
			}
		}

		public DateTime CreatedDate
		{
			get { return _createdDate; }
			set
			{
				CheckChanged(_createdDate, value);
				_createdDate = value;
			}
		}

		public int RatingReviewUserId
		{
			get { return _ratingReviewUserId; }
			set
			{
				CheckChanged(_ratingReviewUserId, value);
				_ratingReviewUserId = value;
			}
		}
	}
}