﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class RatingGroupCategoryService : IRatingGroupCategoryService
	{
		protected RatingGroupCategoryRepository Repository { get; private set; }

		public RatingGroupCategoryService(RatingGroupCategoryRepository ratingGroupCategoryRepository)
		{
			Repository = ratingGroupCategoryRepository;
		}

		public virtual Collection<IRatingGroupCategory> GetAllByCategory(int categoryId)
		{
			return RatingGroupCategoryListCache.Instance.TryGetItem(
				new RatingGroupCategoryListKey(categoryId),
				() => GetAllByCategoryCore(categoryId));
		}

		protected virtual Collection<IRatingGroupCategory> GetAllByCategoryCore(int categoryId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCategory(categoryId);
		}
	}
}