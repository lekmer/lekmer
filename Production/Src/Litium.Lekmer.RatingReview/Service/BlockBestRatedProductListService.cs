﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class BlockBestRatedProductListService : IBlockBestRatedProductListService
	{
		protected BlockBestRatedProductListRepository Repository { get; private set; }
		protected IBlockBestRatedProductListBrandService BlockBestRatedProductListBrandService { get; private set; }

		public BlockBestRatedProductListService(
			BlockBestRatedProductListRepository repository,
			IBlockBestRatedProductListBrandService blockBestRatedProductListBrandService)
		{
			Repository = repository;
			BlockBestRatedProductListBrandService = blockBestRatedProductListBrandService;
		}

		public virtual IBlockBestRatedProductList GetById(IUserContext context, int blockId)
		{
			Repository.EnsureNotNull();

			return BlockBestRatedProductListCache.Instance.TryGetItem(
				new BlockBestRatedProductListKey(context.Channel.Id, blockId),
				() => GetByIdCore(context, blockId));
		}

		protected virtual IBlockBestRatedProductList GetByIdCore(IUserContext context, int blockId)
		{
			var blockBestRatedProductList = Repository.GetById(context.Channel, blockId);

			blockBestRatedProductList.BlockBestRatedProductListBrands = BlockBestRatedProductListBrandService.GetAllByBlock(context, blockId);

			return blockBestRatedProductList;
		}
	}
}