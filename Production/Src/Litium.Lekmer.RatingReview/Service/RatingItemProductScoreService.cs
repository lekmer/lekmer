﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Product;

namespace Litium.Lekmer.RatingReview
{
	public class RatingItemProductScoreService : IRatingItemProductScoreService
	{
		private const char DELIMITER = ',';

		protected RatingItemProductScoreRepository Repository { get; private set; }
		protected IProductRatingSummaryProcessor ProductRatingSummaryProcessor { get; private set; }

		public RatingItemProductScoreService(RatingItemProductScoreRepository repository, IProductRatingSummaryProcessor productRatingSummaryProcessor)
		{
			Repository = repository;
			ProductRatingSummaryProcessor = productRatingSummaryProcessor;
		}

		public virtual IProductRatingSummaryInfo GetProductRatingSummary(IUserContext context, int ratingId, int productId)
		{
			Repository.EnsureNotNull();

			return ProductRatingSummaryInfoCache.Instance.TryGetItem(
				new ProductRatingSummaryInfoCacheKey(context.Channel.Id, ratingId, productId),
				() => GetProductRatingSummaryCore(context, ratingId, productId));
		}

		public ProductIdCollection GetBestRatedProductsByRating(IUserContext context, int ratingId, int? categoryId, Collection<int> brandIds, int numberOfItems)
		{
			var categoryIds = new Collection<int>();
			if (categoryId.HasValue)
			{
				var categoryUtility = new CategoryUtility();
				categoryIds = categoryUtility.ResolveCategoryWithChildren(context, categoryId.Value);
			}

			return Repository.GetBestRatedProductsByRating(
				context.Channel.Id,
				ratingId,
				Scensum.Foundation.Convert.ToStringIdentifierList(categoryIds, DELIMITER),
				Scensum.Foundation.Convert.ToStringIdentifierList(brandIds, DELIMITER),
				DELIMITER,
				numberOfItems,
				context.Customer != null ? (int?)context.Customer.Id : null);
		}

		public void Delete(int ratingReviewFeedbackId)
		{
			Repository.EnsureNotNull();
			Repository.Delete(ratingReviewFeedbackId);

			ProductRatingSummaryInfoCache.Instance.Flush();
		}

		protected virtual IProductRatingSummaryInfo GetProductRatingSummaryCore(IUserContext context, int ratingId, int productId)
		{
			Collection<IRatingItemProductScoreSummary> ratingItemSummaries = Repository.GetSummaryByRatingAndProduct(context.Channel.Id, ratingId, productId);

			return ProductRatingSummaryProcessor.CreateSummary(context, productId, ratingId, ratingItemSummaries);
		}
	}
}