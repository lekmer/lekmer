﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;

namespace Litium.Lekmer.RatingReview
{
	public class RatingGroupProductService : IRatingGroupProductService
	{
		protected RatingGroupProductRepository Repository { get; private set; }

		public RatingGroupProductService(RatingGroupProductRepository ratingGroupProductRepository)
		{
			Repository = ratingGroupProductRepository;
		}

		public virtual Collection<IRatingGroupProduct> GetAllByProduct(int productId)
		{
			return RatingGroupProductListCache.Instance.TryGetItem(
				new RatingGroupProductListKey(productId),
				() => GetAllByProductCore(productId));
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		protected virtual Collection<IRatingGroupProduct> GetAllByProductCore(int productId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByProduct(productId);
		}
	}
}