﻿using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingGroupRatingSecureService : IRatingGroupRatingSecureService
	{
		protected RatingGroupRatingRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public RatingGroupRatingSecureService(RatingGroupRatingRepository ratingGroupRatingRepository, IAccessValidator accessValidator)
		{
			Repository = ratingGroupRatingRepository;
			AccessValidator = accessValidator;
		}

		public virtual void Save(ISystemUserFull systemUserFull, IRatingGroupRating ratingGroupRating)
		{
			if (ratingGroupRating == null)
			{
				throw new ArgumentNullException("ratingGroupRating");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Save(ratingGroupRating);

				transaction.Complete();
			}
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int ratingGroupId, int ratingId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(ratingGroupId, ratingId);

				transaction.Complete();
			}
		}

		public virtual Collection<IRatingGroupRating> GetAllByGroup(int ratingGroupId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByGroup(ratingGroupId);
		}

		public virtual Collection<IRatingGroupRating> GetAllByRating(int ratingId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByRating(ratingId);
		}
	}
}