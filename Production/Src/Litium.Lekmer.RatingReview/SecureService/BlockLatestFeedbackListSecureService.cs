using System;
using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview
{
	public class BlockLatestFeedbackListSecureService : IBlockLatestFeedbackListSecureService, IBlockCreateSecureService, IBlockDeleteSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected BlockLatestFeedbackListRepository Repository { get; private set; }
		protected IAccessSecureService AccessSecureService { get; private set; }
		protected IBlockSecureService BlockSecureService { get; private set; }
		protected IChannelSecureService ChannelSecureService { get; private set; }
		protected IBlockLatestFeedbackListBrandSecureService BlockLatestFeedbackListBrandSecureService { get; private set; }
		

		public BlockLatestFeedbackListSecureService(
			IAccessValidator accessValidator,
			BlockLatestFeedbackListRepository repository,
			IAccessSecureService accessSecureService,
			IBlockSecureService blockSecureService,
			IChannelSecureService channelSecureService,
			IBlockLatestFeedbackListBrandSecureService blockLatestFeedbackListBrandSecureService)
		{
			AccessValidator = accessValidator;
			Repository = repository;
			AccessSecureService = accessSecureService;
			BlockSecureService = blockSecureService;
			ChannelSecureService = channelSecureService;
			BlockLatestFeedbackListBrandSecureService = blockLatestFeedbackListBrandSecureService;
		}

		public virtual IBlockLatestFeedbackList Create()
		{
			AccessSecureService.EnsureNotNull();

			var blockLatestFeedbackList = IoC.Resolve<IBlockLatestFeedbackList>();

			blockLatestFeedbackList.AccessId = AccessSecureService.GetByCommonName("All").Id;
			blockLatestFeedbackList.BlockLatestFeedbackListBrands = new Collection<IBlockLatestFeedbackListBrand>();

			blockLatestFeedbackList.Status = BusinessObjectStatus.New;

			return blockLatestFeedbackList;
		}

		public virtual IBlockLatestFeedbackList GetById(int blockId)
		{
			Repository.EnsureNotNull();

			var blockLatestFeedbackList = Repository.GetByIdSecure(blockId);

			blockLatestFeedbackList.BlockLatestFeedbackListBrands = BlockLatestFeedbackListBrandSecureService.GetAllByBlock(blockId);

			return blockLatestFeedbackList;
		}

		public virtual IBlock SaveNew(ISystemUserFull systemUserFull, int contentNodeId, int contentAreaId, int blockTypeId, string title)
		{
			if (title == null)
			{
				throw new ArgumentNullException("title");
			}

			IBlockLatestFeedbackList blockLatestFeedbackList = Create();

			blockLatestFeedbackList.ContentNodeId = contentNodeId;
			blockLatestFeedbackList.ContentAreaId = contentAreaId;
			blockLatestFeedbackList.BlockTypeId = blockTypeId;
			blockLatestFeedbackList.BlockStatusId = (int) BlockStatusInfo.Offline;
			blockLatestFeedbackList.CategoryId = null;
			blockLatestFeedbackList.NumberOfItems = 1;
			blockLatestFeedbackList.Title = title;
			blockLatestFeedbackList.TemplateId = null;
			blockLatestFeedbackList.Id = Save(systemUserFull, blockLatestFeedbackList);

			return blockLatestFeedbackList;
		}

		public virtual int Save(ISystemUserFull systemUserFull, IBlockLatestFeedbackList block)
		{
			if (block == null)
			{
				throw new ArgumentNullException("block");
			}

			AccessValidator.EnsureNotNull();
			BlockSecureService.EnsureNotNull();
			Repository.EnsureNotNull();
			
			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (var transactedOperation = new TransactedOperation())
			{
				block.Id = BlockSecureService.Save(systemUserFull, block);
				if (block.Id == -1)
				{
					return block.Id;
				}

				Repository.Save(block);

				BlockLatestFeedbackListBrandSecureService.Save(block.Id, block.BlockLatestFeedbackListBrands);

				transactedOperation.Complete();
			}

			ClearCache(block);

			return block.Id;
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, Scensum.Core.PrivilegeConstant.SiteStructurePages);

			using (TransactedOperation transactedOperation = new TransactedOperation())
			{
				Repository.Delete(blockId);

				transactedOperation.Complete();
			}

			BlockLatestFeedbackListCache.Instance.Remove(blockId, ChannelSecureService.GetAll());
		}


		protected virtual void ClearCache(IBlockLatestFeedbackList block)
		{
			Collection<IChannel> channels = ChannelSecureService.GetAll();

			BlockLatestFeedbackListCache.Instance.Remove(block.Id, channels);
			LatestRatingReviewFeedbackListCache.Instance.Remove(block.Id, channels);
		}
	}
}