﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Core;
using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class RatingGroupCategorySecureService : IRatingGroupCategorySecureService
	{
		protected RatingGroupCategoryRepository Repository { get; private set; }
		protected IAccessValidator AccessValidator { get; private set; }

		public RatingGroupCategorySecureService(RatingGroupCategoryRepository ratingGroupCategoryRepository, IAccessValidator accessValidator)
		{
			Repository = ratingGroupCategoryRepository;
			AccessValidator = accessValidator;
		}

		public virtual void Save(ISystemUserFull systemUserFull, IRatingGroupCategory ratingGroupCategory)
		{
			if (ratingGroupCategory == null)
			{
				throw new ArgumentNullException("ratingGroupCategory");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Save(ratingGroupCategory);

				transaction.Complete();
			}

			RatingGroupCategoryListCache.Instance.Remove(new RatingGroupCategoryListKey(ratingGroupCategory.CategoryId));
			RatingGroupListCache.Instance.Flush();
		}

		public virtual void SaveAll(ISystemUserFull systemUserFull, int categoryId, Collection<IRatingGroupCategory> ratingGroupCategories)
		{
			if (ratingGroupCategories == null)
			{
				throw new ArgumentNullException("ratingGroupCategories");
			}

			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			Collection<IRatingGroupCategory> existedCollection = Repository.GetAllByCategory(categoryId);

			using (var transaction = new TransactedOperation())
			{
				// Remove existed if necessary
				foreach (var existedItem in existedCollection)
				{
					IRatingGroupCategory ratingGroupCategory = ratingGroupCategories.FirstOrDefault(r => r.CategoryId == categoryId && r.RatingGroupId == existedItem.RatingGroupId);

					if (ratingGroupCategory == null)
					{
						Repository.Delete(existedItem.RatingGroupId, categoryId);
					}
				}

				// Update existed or save new if necessary
				foreach (var item in ratingGroupCategories)
				{
					Repository.Save(item);
				}

				transaction.Complete();
			}

			RatingGroupCategoryListCache.Instance.Remove(new RatingGroupCategoryListKey(categoryId));
			RatingGroupListCache.Instance.Flush();
		}

		public virtual void Delete(ISystemUserFull systemUserFull, int ratingGroupId, int categoryId)
		{
			AccessValidator.EnsureNotNull();
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, LekmerPrivilegeConstant.AssortmentRating);

			using (var transaction = new TransactedOperation())
			{
				Repository.Delete(ratingGroupId, categoryId);

				transaction.Complete();
			}

			RatingGroupCategoryListCache.Instance.Remove(new RatingGroupCategoryListKey(categoryId));
			RatingGroupListCache.Instance.Flush();
		}

		public virtual Collection<IRatingGroupCategory> GetAllByGroup(int ratingGroupId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByGroup(ratingGroupId);
		}

		public virtual Collection<IRatingGroupCategory> GetAllByCategory(int categoryId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByCategory(categoryId);
		}
	}
}