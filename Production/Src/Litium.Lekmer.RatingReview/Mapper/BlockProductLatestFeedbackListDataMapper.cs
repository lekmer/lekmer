﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockProductLatestFeedbackListDataMapper : DataMapperBase<IBlockProductLatestFeedbackList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;

		public BlockProductLatestFeedbackListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}

		protected override IBlockProductLatestFeedbackList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockLatestFeedbackList = IoC.Resolve<IBlockProductLatestFeedbackList>();

			block.ConvertTo(blockLatestFeedbackList);

			blockLatestFeedbackList.NumberOfItems = MapValue<int>("BlockProductLatestFeedbackList.NumberOfItems");

			blockLatestFeedbackList.SetUntouched();

			return blockLatestFeedbackList;
		}
	}
}