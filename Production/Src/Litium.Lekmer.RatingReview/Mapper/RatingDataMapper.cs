﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingDataMapper : DataMapperBase<IRating>
	{
		public RatingDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRating Create()
		{
			var rating = IoC.Resolve<IRating>();

			rating.Id = MapValue<int>("Rating.RatingId");
			rating.RatingFolderId = MapValue<int>("Rating.RatingFolderId");
			rating.RatingRegistryId = MapValue<int>("Rating.RatingRegistryId");
			rating.CommonName = MapValue<string>("Rating.CommonName");
			rating.Title = MapValue<string>("Rating.Title");
			rating.Description = MapValue<string>("Rating.Description");
			rating.CommonForVariants = MapValue<bool>("Rating.CommonForVariants");

			rating.RatingItems = new Collection<IRatingItem>();

			rating.SetUntouched();

			return rating;
		}
	}
}