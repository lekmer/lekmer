﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingGroupProductDataMapper : DataMapperBase<IRatingGroupProduct>
	{
		public RatingGroupProductDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingGroupProduct Create()
		{
			var ratingGroupProduct = IoC.Resolve<IRatingGroupProduct>();

			ratingGroupProduct.RatingGroupId = MapValue<int>("RatingGroupProduct.RatingGroupId");
			ratingGroupProduct.ProductId = MapValue<int>("RatingGroupProduct.ProductId");

			ratingGroupProduct.SetUntouched();

			return ratingGroupProduct;
		}
	}
}