﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockRatingGroupDataMapper : DataMapperBase<IBlockRatingGroup>
	{
		public BlockRatingGroupDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IBlockRatingGroup Create()
		{
			var blockRatingGroup = IoC.Resolve<IBlockRatingGroup>();

			blockRatingGroup.BlockId = MapValue<int>("BlockRatingGroup.BlockId");
			blockRatingGroup.RatingGroupId = MapValue<int>("BlockRatingGroup.RatingGroupId");

			blockRatingGroup.SetUntouched();

			return blockRatingGroup;
		}
	}
}