﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class RatingReviewUserDataMapper : DataMapperBase<IRatingReviewUser>
	{
		public RatingReviewUserDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IRatingReviewUser Create()
		{
			var ratingReviewUser = IoC.Resolve<IRatingReviewUser>();

			ratingReviewUser.Id = MapValue<int>("RatingReviewUser.RatingReviewUserId");
			ratingReviewUser.CustomerId = MapNullableValue<int>("RatingReviewUser.CustomerId");
			ratingReviewUser.Token = MapValue<string>("RatingReviewUser.Token");
			ratingReviewUser.CreatedDate = MapValue<DateTime>("RatingReviewUser.CreatedDate");

			ratingReviewUser.SetUntouched();

			return ratingReviewUser;
		}
	}
}