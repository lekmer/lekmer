﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.RatingReview.Mapper
{
	public class BlockLatestFeedbackListDataMapper : DataMapperBase<IBlockLatestFeedbackList>
	{
		private DataMapperBase<IBlock> _blockDataMapper;

		public BlockLatestFeedbackListDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override void Initialize()
		{
			base.Initialize();
			_blockDataMapper = DataMapperResolver.Resolve<IBlock>(DataReader);
		}

		protected override IBlockLatestFeedbackList Create()
		{
			var block = _blockDataMapper.MapRow();
			var blockLatestFeedbackList = IoC.Resolve<IBlockLatestFeedbackList>();

			block.ConvertTo(blockLatestFeedbackList);

			blockLatestFeedbackList.CategoryId = MapNullableValue<int?>("BlockLatestFeedbackList.CategoryId");
			blockLatestFeedbackList.NumberOfItems = MapValue<int>("BlockLatestFeedbackList.NumberOfItems");

			blockLatestFeedbackList.SetUntouched();

			return blockLatestFeedbackList;
		}
	}
}