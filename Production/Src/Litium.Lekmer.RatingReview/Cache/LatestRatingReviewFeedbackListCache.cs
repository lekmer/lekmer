﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class LatestRatingReviewFeedbackListCache : ScensumCacheBase<LatestRatingReviewFeedbackListKey, Collection<IRatingReviewFeedback>>
	{
		private static readonly LatestRatingReviewFeedbackListCache _instance = new LatestRatingReviewFeedbackListCache();

		private LatestRatingReviewFeedbackListCache()
		{
		}

		public static LatestRatingReviewFeedbackListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new LatestRatingReviewFeedbackListKey(channel.Id, blockId));
			}
		}
	}

	public class LatestRatingReviewFeedbackListKey : ICacheKey
	{
		public LatestRatingReviewFeedbackListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}