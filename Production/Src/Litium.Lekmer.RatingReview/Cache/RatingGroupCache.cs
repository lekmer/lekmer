﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingGroupCache : ScensumCacheBase<RatingGroupCacheKey, IRatingGroup>
	{
		private static readonly RatingGroupCache _instance = new RatingGroupCache();

		private RatingGroupCache()
		{
		}

		public static RatingGroupCache Instance
		{
			get { return _instance; }
		}
	}

	public class RatingGroupCacheKey : ICacheKey
	{
		public RatingGroupCacheKey(int ratingId)
		{
			RatingId = ratingId;
		}

		public int RatingId { get; set; }

		public string Key
		{
			get { return RatingId.ToString(CultureInfo.InvariantCulture); }
		}
	}
}