﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class BlockBestRatedProductListCache : ScensumCacheBase<BlockBestRatedProductListKey, IBlockBestRatedProductList>
	{
		private static readonly BlockBestRatedProductListCache _instance = new BlockBestRatedProductListCache();

		private BlockBestRatedProductListCache()
		{
		}

		public static BlockBestRatedProductListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockBestRatedProductListKey(channel.Id, blockId));
			}
		}
	}

	public class BlockBestRatedProductListKey : ICacheKey
	{
		public BlockBestRatedProductListKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}