﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class RatingItemListCache : ScensumCacheBase<RatingItemListKey, Collection<IRatingItem>>
	{
		private static readonly RatingItemListCache _instance = new RatingItemListCache();

		private RatingItemListCache()
		{
		}

		public static RatingItemListCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int ratingId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new RatingItemListKey(channel.Id, ratingId));
			}
		}
	}

	public class RatingItemListKey : ICacheKey
	{
		public RatingItemListKey(int channelId, int ratingId)
		{
			ChannelId = channelId;
			RatingId = ratingId;
		}

		public int ChannelId { get; set; }
		public int RatingId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + RatingId; }
		}
	}
}