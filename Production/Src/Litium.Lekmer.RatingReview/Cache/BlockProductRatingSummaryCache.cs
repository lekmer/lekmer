﻿using System.Collections.Generic;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.RatingReview
{
	public sealed class BlockProductRatingSummaryCache : ScensumCacheBase<BlockProductRatingSummaryKey, IBlockProductRatingSummary>
	{
		private static readonly BlockProductRatingSummaryCache _instance = new BlockProductRatingSummaryCache();

		private BlockProductRatingSummaryCache()
		{
		}

		public static BlockProductRatingSummaryCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId, IEnumerable<IChannel> channels)
		{
			foreach (var channel in channels)
			{
				Remove(new BlockProductRatingSummaryKey(channel.Id, blockId));
			}
		}
	}

	public class BlockProductRatingSummaryKey : ICacheKey
	{
		public BlockProductRatingSummaryKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get { return ChannelId + "-" + BlockId; }
		}
	}
}