﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingItemRepository
	{
		protected virtual DataMapperBase<IRatingItem> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingItem>(dataReader);
		}

		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

		public virtual int Save(IRatingItem ratingItem)
		{
			if (ratingItem == null)
			{
				throw new ArgumentNullException("ratingItem");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingItemId", ratingItem.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("RatingId", ratingItem.RatingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", ratingItem.Title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description ", ratingItem.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Score", ratingItem.Score, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pRatingItemSave]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingItemId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingItemId", ratingItemId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingItemDelete]", parameters, dbSettings);
		}

		public virtual void DeleteAll(int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemRepository.DeleteAll");

			new DataHandler().ExecuteCommand("[review].[pRatingItemDeleteAll]", parameters, dbSettings);
		}

		public virtual Collection<IRatingItem> GetAllByRating(int channelId, int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemRepository.GetAllByRating");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingItemGetAllByRating]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRatingItem> GetAllByRatingSecure(int ratingId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingId", ratingId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingItemRepository.GetAllByRatingSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingItemGetAllByRatingSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		// Translations.

		public virtual void SaveTranslations(int ratingItemId, int languageId, string title, string description)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingItemId", ratingItemId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", title, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", description, SqlDbType.NVarChar)
				};

			DatabaseSetting dbSettings = new DatabaseSetting("RatingItemRepository.SaveTranslations");

			new DataHandler().ExecuteCommand("[review].[pRatingItemTranslationSave]", parameters, dbSettings);
		}

		public virtual Collection<ITranslationGeneric> GetAllTitleTranslations(int ratingItemId)
		{
			return GetAllTranslations(ratingItemId, "[review].[pRatingItemTranslationTitleGetAll]");
		}

		public virtual Collection<ITranslationGeneric> GetAllDescriptionTranslations(int ratingItemId)
		{
			return GetAllTranslations(ratingItemId, "[review].[pRatingItemTranslationDescriptionGetAll]");
		}

		protected virtual Collection<ITranslationGeneric> GetAllTranslations(int ratingItemId, string storedProcedureName)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingItemId", ratingItemId, SqlDbType.Int)
				};

			DatabaseSetting dbSettings = new DatabaseSetting("RatingItemRepository.GetAllTranslations");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect(storedProcedureName, parameters, dbSettings))
			{
				return CreateTranslationDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}