﻿using System;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class BlockProductLatestFeedbackListRepository
	{
		protected virtual DataMapperBase<IBlockProductLatestFeedbackList> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IBlockProductLatestFeedbackList>(dataReader);
		}

		public virtual void Save(IBlockProductLatestFeedbackList blockProductLatestFeedbackList)
		{
			if (blockProductLatestFeedbackList == null)
			{
				throw new ArgumentNullException("blockProductLatestFeedbackList");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockProductLatestFeedbackList.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("NumberOfItems", blockProductLatestFeedbackList.NumberOfItems, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockProductLatestFeedbackListRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pBlockProductLatestFeedbackListSave]", parameters, dbSetting);
		}

		public virtual void Delete(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockProductLatestFeedbackListRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pBlockProductLatestFeedbackListDelete]", parameters, dbSettings);
		}

		public virtual IBlockProductLatestFeedbackList GetByIdSecure(int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("BlockProductLatestFeedbackListRepository.GetByIdSecure");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockProductLatestFeedbackListGetByIdSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual IBlockProductLatestFeedbackList GetById(IChannel channel, int blockId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("LanguageId", channel.Language.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("BlockId", blockId, SqlDbType.Int)
				};

			var dbSetting = new DatabaseSetting("BlockProductLatestFeedbackListRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pBlockProductLatestFeedbackListGetById]", parameters, dbSetting))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}
	}
}