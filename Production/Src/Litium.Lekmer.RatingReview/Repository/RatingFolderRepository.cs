﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingFolderRepository
	{
		protected virtual DataMapperBase<IRatingFolder> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingFolder>(dataReader);
		}

		protected virtual DataMapperBase<INode> CreateDataMapperNode(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<INode>(dataReader);
		}

		public virtual int Save(IRatingFolder ratingFolder)
		{
			if (ratingFolder == null)
			{
				throw new ArgumentNullException("ratingFolder");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingFolderId", ratingFolder.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("ParentRatingFolderId", ratingFolder.ParentRatingFolderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("Title", ratingFolder.Title, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("RatingFolderRepository.Save");

			return new DataHandler().ExecuteCommandWithReturnValue("[review].[pRatingFolderSave]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingFolderId", ratingFolderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingFolderRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingFolderDelete]", parameters, dbSettings);
		}

		public virtual IRatingFolder GetById(int ratingFolderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingFolderId", ratingFolderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingFolderRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingFolderGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IRatingFolder> GetAll()
		{
			var dbSettings = new DatabaseSetting("RatingFolderRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingFolderGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<INode> GetTree(int? selectedId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SelectedId", selectedId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingFolderRepository.GetTree");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingFolderGetTree]", parameters, dbSettings))
			{
				return CreateDataMapperNode(dataReader).ReadMultipleRows();
			}
		}

		public virtual Collection<IRatingFolder> GetAllByParent(int parentId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ParentId", parentId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingFolderRepository.GetAllByParent");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingFolderGetAllByParent]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}