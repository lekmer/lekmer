﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview.Repository
{
	public class RatingGroupProductRepository
	{
		protected virtual DataMapperBase<IRatingGroupProduct> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IRatingGroupProduct>(dataReader);
		}

		public virtual void Save(IRatingGroupProduct ratingGroupProduct)
		{
			if (ratingGroupProduct == null)
			{
				throw new ArgumentNullException("ratingGroupProduct");
			}

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupProduct.RatingGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", ratingGroupProduct.ProductId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupProductRepository.Save");

			new DataHandler().ExecuteCommand("[review].[pRatingGroupProductSave]", parameters, dbSettings);
		}

		public virtual void Delete(int ratingGroupId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupProductRepository.Delete");

			new DataHandler().ExecuteCommand("[review].[pRatingGroupProductDelete]", parameters, dbSettings);
		}

		public virtual Collection<IRatingGroupProduct> GetAllByGroup(int ratingGroupId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("RatingGroupId", ratingGroupId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupProductRepository.GetAllByGroup");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupProductGetAllByGroup]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		public virtual Collection<IRatingGroupProduct> GetAllByProduct(int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("RatingGroupProductRepository.GetAllByProduct");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[review].[pRatingGroupProductGetAllByProduct]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}