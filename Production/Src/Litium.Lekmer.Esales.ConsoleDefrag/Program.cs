﻿using System;
using System.Linq;
using System.Reflection;
using Litium.Lekmer.Esales.Connector;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.Esales.ConsoleDefrag
{
	internal class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		static void Main(string[] args)
		{
			if (args.Any(a => a == "help"))
			{
				Console.WriteLine("Posible arguments:");
				Console.WriteLine("    /s-true - runs esales synchronization with ignore=true;");
				Console.WriteLine("    /s-false - runs esales synchronization with ignore=false;");
				Console.WriteLine("    /d - runs esales defragmentation;");
				Console.WriteLine("If no arguments provides, all action applied.");
				return;
			}

			if (args.Any(a => a == "/s-true"))
			{
				EsalesSynchronize(true);
			}

			if (args.Any(a => a == "/s-false"))
			{
				EsalesSynchronize(false);
			}

			if (args.Any(a => a == "/d") || args.Length == 0)
			{
				EsalesDefragmentation();
			}
		}

		private static void EsalesSynchronize(bool ignore)
		{
			try
			{
				_log.Info("Synchronization...");

				var esalesConnector = IoC.Resolve<IEsalesConnector>();
				esalesConnector.Synchronize(ignore);

				_log.Info("Synchronization...Done");
			}
			catch (Exception ex)
			{
				_log.Error("Synchronization failed.", ex);
			}
		}

		private static void EsalesDefragmentation()
		{
			try
			{
				_log.Info("Defragmentation...");

				var esalesConnector = IoC.Resolve<IEsalesConnector>();
				esalesConnector.Defragmentation();

				_log.Info("Defragmentation...Done");
			}
			catch (Exception ex)
			{
				_log.Error("Defragmentation failed.", ex);
			}
		}
	}
}
