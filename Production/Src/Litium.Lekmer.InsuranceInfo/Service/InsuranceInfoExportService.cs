﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Linq;
using Litium.Lekmer.Core;
using Litium.Lekmer.InsuranceInfo.Contract;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.InsuranceInfo
{
	public class InsuranceInfoExportService : IInsuranceInfoExportService
	{
		public ILog Log { get; set; }

		protected IInsuranceInfoExportSetting Setting { get; private set; }
		protected ILekmerChannelService LekmerChannelService { get; private set; }
		protected ILekmerOrderService LekmerOrderService { get; private set; }
		protected ILekmerProductService LekmerProductService { get; private set; }
		protected ICategoryService CategoryService { get; private set; }

		public InsuranceInfoExportService(
			IInsuranceInfoExportSetting insuranceInfoExportSetting,
			ILekmerChannelService lekmerChannelService,
			IOrderService lekmerOrderService,
			ILekmerProductService lekmerProductService,
			ICategoryService categoryService)
		{
			Setting = insuranceInfoExportSetting;
			LekmerChannelService = lekmerChannelService;
			LekmerOrderService = (ILekmerOrderService)lekmerOrderService;
			LekmerProductService = lekmerProductService;
			CategoryService = categoryService;
		}

		public IEnumerable<IUserContext> GetUserContextForAllChannels()
		{
			var channels = LekmerChannelService.GetAll().Where(c => Setting.GetChannelNameReplacementExists(c.CommonName));

			return channels.Select(channel =>
			{
				var context = IoC.Resolve<IUserContext>();
				context.Channel = channel;
				return context;
			}).ToList();
		}

		public string GetFilePath(string channelCommonName)
		{
			string dirr = Setting.DestinationDirectory;
			string channelName = Setting.GetChannelNameReplacement(channelCommonName);

			string fileName = string.Format(Setting.FileName, DateTime.Now.Date.ToString("yyyyMMdd"));
			fileName = fileName.Replace("[ChannelName]", channelName);

			return Path.Combine(dirr, fileName);
		}

		public Collection<IInsuranceInfo> GetInsuranceInfoCollectionToExport(IUserContext context)
		{
			var insuranceInfoCollection = new Collection<IInsuranceInfo>();

			decimal? minPrice;
			decimal? maxPrice;
			GetPriceRange(context.Channel.CommonName, out minPrice, out maxPrice);

			bool needCheckPrice = minPrice != null || maxPrice != null;
			bool needCheckMinPrice = minPrice != null;
			bool needCheckMaxPrice = maxPrice != null;

			var allowedCategories = Setting.GetAllowedCategories();

			var orders = LekmerOrderService.GetToSendInsuranceInfo(context, Setting.MaxDaysAfterPurchase, Setting.MinDaysAfterPurchase - 1);
			foreach (IOrderFull order in orders)
			{
				var validItems = GetValidItems(needCheckPrice, needCheckMinPrice, needCheckMaxPrice, minPrice, maxPrice, order);
				foreach (var item in validItems)
				{
					var product = (ILekmerProduct)LekmerProductService.GetByIdWithoutStatusFilter(context, item.ProductId, true);
					if (product == null) continue;

					var category = CategoryService.GetViewById(context, product.CategoryId);
					if (category == null) continue;

					if (!allowedCategories.Contains(category.ErpId)) continue;

					ICategoryView parentCategory = null;
					if (category.ParentCategoryId.HasValue)
					{
						parentCategory = CategoryService.GetViewById(context, category.ParentCategoryId.Value);
					}

					for (int i = 0; i < item.Quantity; i++)
					{
						var insuranceInfo = CreateInsuranceInfo(order, category, parentCategory, product);
						insuranceInfoCollection.Add(insuranceInfo);
					}
				}
			}

			return insuranceInfoCollection;
		}

		public void UpdateSendInsuranceInfoFlag(List<int> orderIds)
		{
			LekmerOrderService.UpdateSendInsuranceInfoFlag(orderIds);
		}

		protected virtual Collection<IOrderItem> GetValidItems(bool needCheckPrice, bool needCheckMinPrice, bool needCheckMaxPrice, decimal? minPrice, decimal? maxPrice, IOrderFull order)
		{
			var validItems = new Collection<IOrderItem>();

			if (!needCheckPrice)
			{
				validItems = order.GetOrderItems();
			}
			else
			{
				foreach (var item in order.GetOrderItems())
				{
					var price = item.ActualPrice.IncludingVat;

					bool valid = true;
					if (needCheckMinPrice)
					{
						if (price < minPrice.Value)
						{
							valid = false;
						}
					}

					if (valid)
					{
						if (needCheckMaxPrice)
						{
							if (price > maxPrice.Value)
							{
								valid = false;
							}
						}
					}

					if (valid)
					{
						validItems.Add(item);
					}
				}
			}

			return validItems;
		}

		protected virtual void GetPriceRange(string channelCommonName, out decimal? minPrice, out decimal? maxPrice)
		{
			decimal min;
			minPrice = decimal.TryParse(Setting.GetChannelMinPriceReplacement(channelCommonName), out min) ? min : (decimal?)null;

			decimal max;
			maxPrice = decimal.TryParse(Setting.GetChannelMaxPriceReplacement(channelCommonName), out max) ? max : (decimal?)null;
		}

		protected virtual IInsuranceInfo CreateInsuranceInfo(IOrderFull order, ICategoryView category, ICategoryView parentCategory, ILekmerProduct product)
		{
			var insuranceInfo = IoC.Resolve<IInsuranceInfo>();

			if (order != null && order.Customer != null && order.Customer.CustomerInformation != null)
			{
				insuranceInfo.OrderId = order.Id;
				insuranceInfo.FirstName = order.Customer.CustomerInformation.FirstName;
				insuranceInfo.LastName = order.Customer.CustomerInformation.LastName;
				insuranceInfo.CivicNumber = order.Customer.CustomerInformation.CivicNumber;
				insuranceInfo.Adress = order.Customer.CustomerInformation.DefaultBillingAddress.StreetAddress;
				insuranceInfo.PostalCode = order.Customer.CustomerInformation.DefaultBillingAddress.PostalCode;
				insuranceInfo.City = order.Customer.CustomerInformation.DefaultBillingAddress.City;
				insuranceInfo.PhoneNumber = order.Customer.CustomerInformation.CellPhoneNumber;
				insuranceInfo.Email = order.Customer.CustomerInformation.Email;
				insuranceInfo.PurchaseDate = order.CreatedDate;
				insuranceInfo.ParentCategory = parentCategory != null ? parentCategory.Title : string.Empty;
				insuranceInfo.Category = category != null ? category.Title : string.Empty;
				insuranceInfo.CategoryId = category != null ? category.Id.ToString(CultureInfo.InvariantCulture) : string.Empty;
				insuranceInfo.Brand = product.Brand != null ? product.Brand.Title : string.Empty;
				insuranceInfo.Modell = product.DisplayTitle;
				insuranceInfo.PurchaseAmount = order.GetActualPriceTotal().IncludingVat;
				insuranceInfo.Premie = 0;
			}

			return insuranceInfo;
		}
	}
}