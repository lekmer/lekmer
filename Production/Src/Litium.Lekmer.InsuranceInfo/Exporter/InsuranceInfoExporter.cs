﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Litium.Lekmer.InsuranceInfo.Contract;
using Litium.Scensum.Core;
using log4net;

namespace Litium.Lekmer.InsuranceInfo
{
	public class InsuranceInfoExporter : IExporter
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		protected IInsuranceInfoExportSetting InsuranceInfoExportSetting { get; private set; }
		protected IInsuranceInfoExportService InsuranceInfoExportService { get; private set; }

		public InsuranceInfoExporter(IInsuranceInfoExportSetting insuranceInfoExportSetting, IInsuranceInfoExportService insuranceInfoExportService)
		{
			InsuranceInfoExportSetting = insuranceInfoExportSetting;
			InsuranceInfoExportService = insuranceInfoExportService;

			InsuranceInfoExportService.Log = _log;
		}

		public void Execute()
		{
			if (!InsuranceInfoExportSetting.ApplicationName.Equals(ExporterConstants.LekmerApplication))
			{
				return;
			}

			_log.Info("Execution started.");

			ExecuteInsuranceInfoExport();

			_log.Info("Execution complited.");
		}

		protected virtual void ExecuteInsuranceInfoExport()
		{
			var userContexts = InsuranceInfoExportService.GetUserContextForAllChannels();

			foreach (IUserContext context in userContexts)
			{
				Stopwatch stopwatch = Stopwatch.StartNew();

				string filePath = InsuranceInfoExportService.GetFilePath(context.Channel.CommonName);
				if (File.Exists(filePath))
				{
					File.Delete(filePath);
				}

				_log.InfoFormat("File preparing... //{0}", filePath);

				_log.Info("Get orders...");
				var insuranceInfoCollection = InsuranceInfoExportService.GetInsuranceInfoCollectionToExport(context);

				if (insuranceInfoCollection.Count <= 0)
				{
					stopwatch.Stop();
					_log.InfoFormat("There are no orders for channel {0}", context.Channel.CommonName);
					continue;
				}

				using (Stream stream = File.OpenWrite(filePath))
				{
					var insuranceInfoCsvFile = new InsuranceInfoCsvFile();
					insuranceInfoCsvFile.Initialize(InsuranceInfoExportSetting, insuranceInfoCollection);
					insuranceInfoCsvFile.Save(stream);
				}

				_log.InfoFormat("File saved... //{0}", filePath);

				_log.Info("File start uploaded to FTP...");
				UploadToFtp(context.Channel.CommonName, filePath);

				_log.InfoFormat("Update order SendInsuranceInfo status...");
				List<int> orderIds = insuranceInfoCollection.Select(o => o.OrderId).Distinct().ToList();
				InsuranceInfoExportService.UpdateSendInsuranceInfoFlag(orderIds);

				stopwatch.Stop();
				_log.InfoFormat("Elapsed time for {0} - {1}", context.Channel.CommonName, stopwatch.Elapsed);
			}
		}

		protected virtual void UploadToFtp(string channelCommonName, string filePath)
		{
			try
			{
				string channelName = InsuranceInfoExportSetting.GetChannelNameReplacement(channelCommonName);

				string fileName = string.Format(InsuranceInfoExportSetting.FileName, DateTime.Now.Date.ToString("yyyyMMdd"));
				fileName = fileName.Replace("[ChannelName]", channelName);

				var ftpFileUrl = string.Format(InsuranceInfoExportSetting.FtpUrl, fileName);
				var username = InsuranceInfoExportSetting.GetChannelUserNameReplacement(channelCommonName);
				var password = InsuranceInfoExportSetting.GetChannelPasswordReplacement(channelCommonName);

				var ftpRequest = (FtpWebRequest)FtpWebRequest.Create(new Uri(ftpFileUrl));
				ftpRequest.Method = WebRequestMethods.Ftp.UploadFile;
				ftpRequest.Proxy = null;
				ftpRequest.UseBinary = true;
				ftpRequest.Credentials = new NetworkCredential(username, password);

				var fileInfo = new FileInfo(filePath);
				var fileContents = new byte[fileInfo.Length];

				using (FileStream stream = fileInfo.OpenRead())
				{
					stream.Read(fileContents, 0, Convert.ToInt32(fileInfo.Length));
				}

				using (Stream writer = ftpRequest.GetRequestStream())
				{
					writer.Write(fileContents, 0, fileContents.Length);
				}
			}
			catch (WebException webex)
			{
				_log.Error(webex);
			}
		}
	}
}