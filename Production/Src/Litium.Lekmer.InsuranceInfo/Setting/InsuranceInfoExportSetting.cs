﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Setting;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.InsuranceInfo.Contract;

namespace Litium.Lekmer.InsuranceInfo
{
	public class InsuranceInfoExportSetting : SettingBase, IInsuranceInfoExportSetting
	{
		protected override string StorageName
		{
			get { return "InsuranceInfoExport"; }
		}
		protected virtual string GroupName
		{
			get { return "InsuranceInfoExportSettings"; }
		}

		public bool UseWeeklyExecution
		{
			get { return GetBoolean(GroupName, "UseWeeklyExecution", false); }
		}

		public int WeeklyExecutionHour
		{
			get { return GetInt32(GroupName, "WeeklyExecutionHour", 0); }
		}

		public int WeeklyExecutionMinute
		{
			get { return GetInt32(GroupName, "WeeklyExecutionMinute", 0); }
		}

		public bool WeeklyExecutionMonday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionMonday", false); }
		}

		public bool WeeklyExecutionTuesday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionTuesday", false); }
		}

		public bool WeeklyExecutionWednesday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionWednesday", false); }
		}

		public bool WeeklyExecutionThursday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionThursday", false); }
		}

		public bool WeeklyExecutionFriday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionFriday", false); }
		}

		public bool WeeklyExecutionSaturday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionSaturday", false); }
		}

		public bool WeeklyExecutionSunday
		{
			get { return GetBoolean(GroupName, "WeeklyExecutionSunday", false); }
		}


		public bool UseDailyExecution
		{
			get { return GetBoolean(GroupName, "UseDailyExecution", false); }
		}

		public int DailyExecutionHour
		{
			get { return GetInt32(GroupName, "DailyExecutionHour", 0); }
		}

		public int DailyExecutionMinute
		{
			get { return GetInt32(GroupName, "DailyExecutionMinute", 0); }
		}

		public string ApplicationName
		{
			get { return GetString(GroupName, "ApplicationName"); }
		}

		public string DestinationDirectory
		{
			get { return GetString(GroupName, "DestinationDirectory"); }
		}
		public string FileName
		{
			get { return GetString(GroupName, "FileName"); }
		}
		public string FileDelimiter
		{
			get { return GetString(GroupName, "FileDelimiter", ";"); }
		}

		public string CategoriesErpId
		{
			get { return GetString(GroupName, "CategoriesErpId"); }
		}
		public string MaxPrice
		{
			get { return GetString(GroupName, "MaxPrice"); }
		}
		public string MinPrice
		{
			get { return GetString(GroupName, "MinPrice"); }
		}

		public int MaxDaysAfterPurchase
		{
			get { return GetInt32(GroupName, "MaxDaysAfterPurchase", 1); }
		}
		public int MinDaysAfterPurchase
		{
			get { return GetInt32(GroupName, "MinDaysAfterPurchase", 1); }
		}

		public string ReplaceChannelNames
		{
			get { return GetString(GroupName, "ReplaceChannelNames"); }
		}

		public string Encoding
		{
			get { return GetString(GroupName, "Encoding"); }
		}

		public string FtpUrl
		{
			get { return GetString(GroupName, "FtpUrl"); }
		}

		public string Username
		{
			get { return GetString(GroupName, "Username"); }
		}

		public string Password
		{
			get { return GetString(GroupName, "Password"); }
		}

		public string GetChannelNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames, ',');
			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}

		public bool GetChannelNameReplacementExists(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(ReplaceChannelNames, ',');
			return dictionary.ContainsKey(channelName);
		}

		public string GetChannelMinPriceReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(MinPrice, ';');
			if (dictionary == null)
			{
				return null;
			}

			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}

		public string GetChannelMaxPriceReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(MaxPrice, ';');
			if (dictionary == null)
			{
				return null;
			}

			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}

		public string GetChannelUserNameReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(Username, ';');
			if (dictionary == null)
			{
				return null;
			}

			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}

		public string GetChannelPasswordReplacement(string channelName)
		{
			IDictionary<string, string> dictionary = ConvertToDictionary(Password, ';');
			if (dictionary == null)
			{
				return null;
			}

			return dictionary.ContainsKey(channelName) ? dictionary[channelName] : channelName;
		}

		public ICollection<string> GetAllowedCategories()
		{
			return ConvertToList(CategoriesErpId, ',');
		}

		// key1:value1,key2:value2, ... , keyN:valueN => Dictionary<key, value>
		protected virtual IDictionary<string, string> ConvertToDictionary(string textValue, char seperator)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			ICollection<string> pairs = ConvertToList(textValue, seperator);

			var dictionary = new Dictionary<string, string>();

			foreach (string pairString in pairs)
			{
				string[] pairValues = pairString.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
				if (pairValues.Length == 2)
				{
					dictionary[pairValues[0]] = pairValues[1];
				}
			}

			return dictionary;
		}

		// value1,value2, ... , valueN => List<value>
		public ICollection<string> ConvertToList(string textValue, char seperator)
		{
			if (textValue.IsEmpty())
			{
				return null;
			}

			return textValue.Split(new[] { seperator }, StringSplitOptions.RemoveEmptyEntries).ToList();
		}
	}
}