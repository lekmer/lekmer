using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Text;
using Litium.Lekmer.Common;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	/// <summary>
	/// Control for rendering product list.
	/// </summary>
	public class BlockProductListControl : BlockControlBase<IBlockProductList>
	{
		private readonly IBlockProductListService _blockProductListService;
		private readonly ILekmerProductService _productService;
		private ICommonSession _commonSession;

		[SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods"),
		 SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		protected ProductCollection Products { get; set; }

		protected IPagingControl PagingControl { get; set; }

		public ICommonSession CommonSession
		{
			get { return _commonSession ?? (_commonSession = IoC.Resolve<ICommonSession>()); }
			set { _commonSession = value; }
		}

		/// <summary>
		/// Initializes the <see cref="BlockProductListControl"/>.
		/// </summary>
		/// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
		/// <param name="blockProductListService"></param>
		/// <param name="productService"></param>
		public BlockProductListControl(ITemplateFactory templateFactory, IBlockProductListService blockProductListService, IProductService productService)
			: base(templateFactory)
		{
			_blockProductListService = blockProductListService;
			_productService = (ILekmerProductService) productService;
		}

		/// <summary>
		/// Gets the block by id.
		/// </summary>
		/// <param name="blockId">Id of the block.</param>
		/// <returns>An instance of <see cref="IBlockProductList"/>.</returns>
		protected override IBlockProductList GetBlockById(int blockId)
		{
			return _blockProductListService.GetById(UserContext.Current, blockId);
		}

		protected virtual void Initialize()
		{
			PagingControl = CreatePagingControl();
			Products = GetProducts();
		}

		/// <summary>
		/// Gets the products to list.
		/// </summary>
		/// <returns>Collection of products.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ProductCollection GetProducts()
		{
			return _productService.GetAllByBlock(UserContext.Current, Block, PagingControl.SelectedPage, PagingControl.PageSize);
		}

		/// <summary>
		/// Initializes the paging control.
		/// </summary>
		/// <returns>Instance of <see cref="IPagingControl"/>.</returns>
		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
			pagingControl.Initialize();
			return pagingControl;
		}

		/// <summary>
		/// Gets the base url for paging.
		/// </summary>
		/// <returns>Page url.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetPagingBaseUrl()
		{
			if (ContentNodeTreeItem.Url.HasValue())
			{
				return ResolveUrl(ContentNodeTreeItem.Url);
			}

			return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
		}

		/// <summary>
		/// Renders the control content.
		/// </summary>
		/// <returns>Rendered <see cref="BlockContent"/>.</returns>
		protected override BlockContent RenderCore()
		{
			Initialize();

			PagingControl.TotalCount = Products.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head;
			string footer;
			RenderHeadAndFooter(pagingContent, out head, out footer);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		protected virtual void RenderHeadAndFooter(PagingContent pagingContent, out string head, out string footer)
		{
			var idsBuilder = new StringBuilder();
			var erpIdsBuilder = new StringBuilder();
			var headItemBuilder = new StringBuilder();
			var footerItemBuilder = new StringBuilder();

			for (int i = 0; i < Products.Count; i++)
			{
				if (i > 2) break;

				var product = Products[i];

				Fragment headProductItem = Template.GetFragment("HeadProductItem");
				Fragment footerProductItem = Template.GetFragment("FooterProductItem");
				AddVariable("ProductItem.Id", product.Id.ToString(CultureInfo.InvariantCulture), headProductItem, footerProductItem);
				AddVariable("ProductItem.ErpId", product.ErpId, headProductItem, footerProductItem);
				headItemBuilder.AppendLine(headProductItem.Render());
				footerItemBuilder.AppendLine(footerProductItem.Render());

				if (i == 0)//first or the only item in array
				{
					erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "\'{0}\'", product.ErpId));
				}
				else
				{
					erpIdsBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",\'{0}\'", product.ErpId));
				}
			}

			for (int i = 0; i < Products.Count; i++)
			{
				var product = Products[i];

				if (i == 0)//first or the only item in array
				{
					idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0}", product.Id));
				}
				else
				{
					idsBuilder.Append(string.Format(CultureInfo.InvariantCulture, ",{0}", product.Id));
				}
			}

			var ids = idsBuilder.ToString();
			var erpIds = erpIdsBuilder.ToString();
			var headProductItems = headItemBuilder.ToString();
			var footerProductItems = footerItemBuilder.ToString();

			head = RenderFragment("Head", ids, erpIds, headProductItems, pagingContent);
			footer = RenderFragment("Footer", ids, erpIds, footerProductItems, pagingContent);
		}
		protected virtual void AddVariable(string name, string value, Fragment head, Fragment footer)
		{
			head.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
			footer.AddVariable(name, value, VariableEncoding.JavaScriptStringEncode);
		}

		protected virtual string RenderFragment(string fragmentName, string ids, string erpIds, string productItems, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			fragment.AddEntity(Block);
			fragment.AddVariable("Param.ProductIds", ids);
			fragment.AddVariable("Param.ProductErpIds", erpIds, VariableEncoding.None);
			fragment.AddVariable(fragmentName == "Head" ? "Iterate:HeadProductItem" : "Iterate:FooterProductItem", productItems, VariableEncoding.None);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		/// <summary>
		/// Renders the list of products.
		/// </summary>
		/// <returns>Rendered products.</returns>
		protected virtual string RenderProductList()
		{
			return CreateGridControl().Render();
		}

		/// <summary>
		/// Initializes the <see cref="GridControl{TItem}"/>.
		/// </summary>
		/// <returns>An instance of <see cref="GridControl{TItem}"/>.</returns>
		protected virtual GridControl<IProduct> CreateGridControl()
		{
			return new GridControl<IProduct>
				{
					Items = Products,
					ColumnCount = Block.Setting.ActualColumnCount,
					RowCount = Block.Setting.ActualRowCount,
					Template = Template,
					ListFragmentName = "ProductList",
					RowFragmentName = "ProductRow",
					ItemFragmentName = "Product",
					EmptySpaceFragmentName = "EmptySpace"
				};
		}
	}
}