﻿using System.Diagnostics.CodeAnalysis;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	/// <summary>
	/// Control for product image list.
	/// </summary>
	public class BlockProductImageListControl : BlockProductPageControlBase<IBlockProductImageList>
	{
		private readonly IBlockProductImageListService _blockProductImageListService;
		private readonly IImageService _imageService;

		[SuppressMessage("Microsoft.Naming", "CA1721:PropertyNamesShouldNotMatchGetMethods"),
		 SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		protected ImageCollection Images { get; set; }
		protected int ImageTotalCount { get; set; }
		protected IPagingControl PagingControl { get; set; }

		/// <summary>
		/// Initializes the <see cref="BlockProductImageListControl"/>.
		/// </summary>
		/// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
		/// <param name="blockProductImageListService"></param>
		/// <param name="imageService"></param>
		public BlockProductImageListControl(ITemplateFactory templateFactory,IBlockProductImageListService blockProductImageListService, IImageService imageService)
			: base(templateFactory)
		{
			_blockProductImageListService = blockProductImageListService;
			_imageService = imageService;
		}

		/// <summary>
		/// Gets the block by id.
		/// </summary>
		/// <param name="blockId">Id of the block.</param>
		/// <returns>An instance of <see cref="IBlockProductImageList"/>.</returns>
		protected override IBlockProductImageList GetBlockById(int blockId)
		{
			return _blockProductImageListService.GetById(UserContext.Current, blockId);
		}

		protected virtual void Initialize()
		{
			PagingControl = CreatePagingControl();
			Images = GetImages();
		}

		/// <summary>
		/// Gets product images to list.
		/// </summary>
		/// <returns></returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual ImageCollection GetImages()
		{
			if (!Block.ProductImageGroupId.HasValue) return new ImageCollection();

			return _imageService.GetAllByProductAndGroup(UserContext.Current, Product.Id, Block.ProductImageGroupId.Value, PagingControl.SelectedPage, PagingControl.PageSize);
		}

		/// <summary>
		/// Initializes the paging control.
		/// </summary>
		/// <returns>Instance of <see cref="IPagingControl"/>.</returns>
		protected virtual IPagingControl CreatePagingControl()
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = GetPagingBaseUrl();
			pagingControl.PageQueryStringParameterName = BlockId + "-page";
			pagingControl.PageSize = Block.Setting.ActualColumnCount * Block.Setting.ActualRowCount;
			pagingControl.Initialize();
			return pagingControl;
		}

		/// <summary>
		/// Gets the base url for paging.
		/// </summary>
		/// <returns>Product url.</returns>
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual string GetPagingBaseUrl()
		{
			return ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
		}

		/// <summary>
		/// Renders the control content.
		/// </summary>
		/// <returns>Rendered <see cref="BlockContent"/>.</returns>
		protected override BlockContent RenderCore()
		{
			if (Product == null) return new BlockContent(null, "[ Can't render product image list on this page ]");

			Initialize();

			PagingControl.TotalCount = Images.TotalCount;
			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("ImageList", RenderImageList(), VariableEncoding.None);
			fragmentContent.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);
			fragmentContent.AddEntity(Block);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		/// <summary>
		/// Renders the list of images.
		/// </summary>
		/// <returns>Rendered images.</returns>
		protected virtual string RenderImageList()
		{
			return CreateGridControl().Render();
		}

		/// <summary>
		/// Initializes the <see cref="GridControl{TItem}"/>.
		/// </summary>
		/// <returns>An instance of <see cref="GridControl{TItem}"/>.</returns>
		protected virtual GridControl<IImage> CreateGridControl()
		{
			return new GridControl<IImage>
			{
				Items = Images,
				ColumnCount = Block.Setting.ActualColumnCount,
				RowCount = Block.Setting.ActualRowCount,
				Template = Template,
				ListFragmentName = "ImageList",
				RowFragmentName = "ImageRow",
				ItemFragmentName = "Image",
				EmptySpaceFragmentName = "EmptySpace"
			};
		}
	}
}
