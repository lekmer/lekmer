﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Product.Web.ProductPage.Cookie
{
    public static class ClickHistoryCookie
    {
        public const string ClickHistoryKey = "ClickHistory";
        public const int MaxCountOfProducts = 10;

        public static IEnumerable<int> GetProductsFromClickHistory()
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[ClickHistoryKey];
            if (cookie == null || string.IsNullOrEmpty(cookie.Value))
            {
                return new Collection<int>();
            }
            string productTempIds = HttpUtility.UrlDecode(cookie.Value, Encoding.Default);

            IEnumerable<int> productIds = productTempIds
                                                     .Split(new[] { ',' })
                                                     .Select(value =>
                                                     {
                                                         int intValue;
                                                         return int.TryParse(value, out intValue) ? intValue : 0;
                                                     })
                                                     .Where(value => value != 0).Distinct();
            if (productIds.Count() < 1)
            {
                return new Collection<int>();
            }
            return productIds;

        }

        public static void AddProductToClickHistory(int productId)
        {
            DateTime dtNow = DateTime.Now;
            TimeSpan tsMinute = new TimeSpan(356, 0, 0, 0, 0);
            HttpCookie cookie = HttpContext.Current.Request.Cookies[ClickHistoryKey];
            if (cookie == null)
            {
                cookie = new HttpCookie(ClickHistoryKey);
                cookie.Value = productId.ToString();
            }
            else
            {
                string productTempIds = HttpUtility.UrlDecode(cookie.Value, Encoding.Default);
                if (string.IsNullOrEmpty(productTempIds))
                {
                    cookie.Value = productId.ToString();
                }
                else
                {
                    List<int> productIds = new List<int>(productTempIds
                        .Split(new[] { ',' })
                        .Select(value =>
                        {
                            int intValue;
                            return int.TryParse(value, out intValue) ? intValue : 0;
                        })
                        .Where(value => value != 0).Distinct());

                    if (productIds.Contains(productId))
                    {
                        productIds.Remove(productId);
                    }

                    productIds.Add(productId);
                    if (productIds.Count > MaxCountOfProducts)
                    {
                        productIds.RemoveRange(0, productIds.Count - MaxCountOfProducts);
                    }
                    var itemBuilder = new StringBuilder();
                    foreach (var item in productIds)
                    {
                        itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0},", item));
                    }
                    if (itemBuilder.Length > 0)
                        itemBuilder.Remove(itemBuilder.Length - 1, 1);
                    cookie.Value = itemBuilder.ToString();
                }

            }
            cookie.Expires = dtNow + tsMinute;
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        public static void AddProductToClickHistory(ProductCollection productIds)
        {
            HttpCookie cookie = HttpContext.Current.Request.Cookies[ClickHistoryKey];
            if (cookie == null)
            {
                return;
            }
            var itemBuilder = new StringBuilder();
            foreach (var item in productIds)
            {
                itemBuilder.Append(string.Format(CultureInfo.InvariantCulture, "{0},", item));
            }
            if (itemBuilder.Length > 0)
                itemBuilder.Remove(itemBuilder.Length - 1, 1);

            cookie.Value = itemBuilder.ToString();
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
    }
}
