﻿using Litium.Scensum.Core.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.BlockReviewMyWishList
{
	public class WishListPackagetUpdateSizeForm : ControlBase
	{
		private readonly string _postUrl;

		public WishListPackagetUpdateSizeForm(string postUrl)
		{
			_postUrl = postUrl;
		}

		public string PostUrl
		{
			get { return _postUrl; }
		}

		public string PostModeValue
		{
			get { return "wishlist_package_update_size"; }
		}

		public string WishListItemIdName
		{
			get { return "wishlistitem_id"; }
		}

		public string WishListItemProductIdsName
		{
			get { return "wishlistitem_product_ids"; }
		}

		public string WishListItemProductSizeIdsName
		{
			get { return "wishlistitem_product_size_ids"; }
		}

		public int WishListItemId { get; set; }
		public string ProductIds { get; set; }
		public string ProductSizeIds { get; set; }

		public bool IsFormPostBack
		{
			get { return IsPostBack && PostMode.Equals(PostModeValue); }
		}

		public void MapFieldsToFragment(Fragment fragment)
		{
			fragment.AddVariable("PackageUpdateSizeForm.PostMode.Name", PostModeName);
			fragment.AddVariable("PackageUpdateSizeForm.PostMode.Value", PostModeValue);
			fragment.AddVariable("PackageUpdateSizeForm.PostUrl", PostUrl);
			fragment.AddVariable("PackageUpdateSizeForm.WishListItemId.Name", WishListItemIdName);
			fragment.AddVariable("PackageUpdateSizeForm.ProductIds.Name", WishListItemProductIdsName);
			fragment.AddVariable("PackageUpdateSizeForm.ProductSizeIds.Name", WishListItemProductSizeIdsName);
		}

		public bool MapFromRequest()
		{
			int wishListItemId;
			if (int.TryParse(Request.Form[WishListItemIdName], out wishListItemId))
			{
				WishListItemId = wishListItemId;
			}
			else
			{
				return false;
			}

			ProductIds = Request.Form[WishListItemProductIdsName];
			if (string.IsNullOrEmpty(ProductIds))
			{
				return false;
			}

			ProductSizeIds = Request.Form[WishListItemProductSizeIdsName];
			if (string.IsNullOrEmpty(ProductSizeIds))
			{
				return false;
			}

			return true;
		}
	}
}