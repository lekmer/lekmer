﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Product.Web.BlockReviewMyWishList;
using Litium.Scensum.Core;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class ReviewMyWishListComponent : ComponentControl
	{
		private readonly ILekmerProductService _productService;
		private readonly IWishListService _wishListService;
		private readonly IWishListPackageItemService _wishListPackageItemService;
		private readonly IWishListCookieService _wishListCookieService;

		private Guid _wishListKey;
		private IWishList _wishList;
		private WishListForm _wishListForm;
		private WishListProductUpdateSizeForm _productUpdateSizeForm;
		private WishListPackagetUpdateSizeForm _packageUpdateSizeForm;
		private Collection<WishListProduct> _wishListProducts;
		private string _postUrl;

		public ReviewMyWishListComponent(
			ITemplateFactory templateFactory,
			IFormatter formatter,
			IProductService productService,
			IWishListService wishListService,
			IWishListPackageItemService wishListPackageItemService,
			IWishListCookieService wishListCookieService) : base(templateFactory)
		{
			_productService = (ILekmerProductService)productService;
			_wishListService = wishListService;
			_wishListCookieService = wishListCookieService;
			_wishListPackageItemService = wishListPackageItemService;
			Formatter = formatter;
		}

		protected IPagingControl PagingControl { get; set; }
		protected IFormatter Formatter { get; private set; }

		/// <summary>
		/// Id of the active content node.
		/// </summary>
		protected virtual int? ActiveContentNodeId
		{
			get { return Master.DynamicProperties["ActiveContentNodeId"] as int?; }
		}

		protected override string ModelCommonName
		{
			get { return "ReviewMyWishListComponent"; }
		}
		
		/// <summary>
		/// 'StartFromId' parameter.
		/// </summary>
		protected virtual int ColumnCount
		{
			get { return Parameters.GetInt32("ColumnCount"); }
		}

		/// <summary>
		/// 'StartFromCommonName' parameter.
		/// </summary>
		protected virtual int RowCount
		{
			get { return Parameters.GetInt32("RowCount"); }
		}

		protected virtual string DeleteProductModeValue
		{
			get { return "delete-product"; }
		}

		protected virtual string MoveProductModeValue
		{
			get { return "move-product"; }
		}

		protected virtual bool IsMode(string mode)
		{
			string modeValue = Request.QueryString[PostModeName];
			if (modeValue.IsEmpty())
			{
				return false;
			}

			return modeValue.Equals(mode, StringComparison.CurrentCultureIgnoreCase);
		}

		protected virtual bool IsPostMode(string mode)
		{
			return PostMode.Equals(mode, StringComparison.CurrentCultureIgnoreCase);
		}

		protected virtual string PostUrl
		{
			get
			{
				if (_postUrl.IsEmpty())
				{
					_postUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());
				}
				return _postUrl;
			}
		}

		protected virtual WishListForm WishListForm
		{
			get { return _wishListForm ?? (_wishListForm = new WishListForm(PostUrl)); }
		}

		protected virtual WishListProductUpdateSizeForm ProductUpdateSizeForm
		{
			get { return _productUpdateSizeForm ?? (_productUpdateSizeForm = new WishListProductUpdateSizeForm(PostUrl)); }
		}

		protected virtual WishListPackagetUpdateSizeForm PackageUpdateSizeForm
		{
			get { return _packageUpdateSizeForm ?? (_packageUpdateSizeForm = new WishListPackagetUpdateSizeForm(PostUrl)); }
		}
		
		public override ComponentContent Render()
		{
			Initialize();

			PagingContent pagingContent = PagingControl.Render();

			Fragment fragmentContent = Template.GetFragment("Content");

			ProcessForm(fragmentContent);

			RenderWishList(fragmentContent, pagingContent);

			fragmentContent.AddVariable("Mode.Name", PostModeName);
			fragmentContent.AddVariable("DeleteProduct.ModeValue", DeleteProductModeValue);
			fragmentContent.AddVariable("MoveProduct.ModeValue", MoveProductModeValue);

			string head = RenderFragment("Head", pagingContent);
			string footer = RenderFragment("Footer", pagingContent);
			return new ComponentContent(head, fragmentContent.Render(), footer);
		}

		protected virtual void RenderWishList(Fragment fragment, PagingContent pagingContent)
		{
			fragment.AddCondition("WishListIsEmpty", _wishListProducts.Count == 0);

			fragment.AddVariable("ProductList", RenderProductList(), VariableEncoding.None);

			fragment.AddVariable("Paging", pagingContent.Body, VariableEncoding.None);

			Price price = GetActualPriceSummary();
			fragment.AddVariable("WishList.PriceIncludingVatSummary", Formatter.FormatPrice(Channel.Current, price.IncludingVat));
			fragment.AddVariable("WishList.PriceExcludingVatSummary", Formatter.FormatPrice(Channel.Current, price.ExcludingVat));

			fragment.AddVariable("WishList.TotalProductCount", Formatter.FormatNumber(Channel.Current, _wishListProducts.Count));

			fragment.AddVariable("WishListKey", _wishListKey.ToString());

			fragment.AddCondition("WishList.HasTitle", _wishList != null && !string.IsNullOrEmpty(_wishList.Title));
			fragment.AddVariable("WishList.Title", _wishList != null ? _wishList.Title : null);
		}

		protected virtual string RenderFragment(string fragmentName, PagingContent pagingContent)
		{
			var fragment = Template.GetFragment(fragmentName);
			return (fragment.Render() ?? string.Empty) + (pagingContent.Head ?? string.Empty);
		}

		protected virtual string RenderProductList()
		{
			var items = new Collection<WishListProduct>();
			var products = _wishListProducts
				.Skip((PagingControl.SelectedPage - 1)*PagingControl.PageSize)
				.Take(PagingControl.PageSize);
			foreach (var wishListProduct in products)
			{
				items.Add(wishListProduct);
			}
			
			return new WishListGridControl
			{
				Items = items,
				ColumnCount = ColumnCount,
				RowCount = RowCount,
				Template = Template,
				ListFragmentName = "ProductList",
				RowFragmentName = "ProductRow",
				ItemFragmentName = "Product",
				EmptySpaceFragmentName = "EmptySpace",
				EditMode = true,
				WishListProductUpdateSizeForm = ProductUpdateSizeForm,
				WishListPackagetUpdateSizeForm = PackageUpdateSizeForm
			}.Render();
		}

		protected virtual void Initialize()
		{
			_wishListKey = _wishListCookieService.GetWishListKey();

			_wishList = _wishListKey != Guid.Empty ? _wishListService.GetByKey(UserContext.Current, _wishListKey) : null;
			
			if (_wishList == null)
			{
				_wishListCookieService.RemoveWishListKey();
			}
			else if (IsMode(DeleteProductModeValue))
			{
				ProcessDeleteMode();
			}
			else if (IsMode(MoveProductModeValue))
			{
				ProcessMoveMode();
			}
			else if (IsPostMode(ProductUpdateSizeForm.PostModeValue))
			{
				ProcessUpdateProductSizeMode();
			}
			else if (IsPostMode(PackageUpdateSizeForm.PostModeValue))
			{
				ProcessUpdatePackageSizesMode();
			}

			_wishListProducts = GetProducts();

			PagingControl = CreatePagingControl(_wishListProducts.Count);
		}

		protected virtual void ProcessDeleteMode()
		{
			if (_wishList == null)
				return;

			if (!_wishList.GetItems().Any())
				return;

			bool deleted = false;

			int? itemId = GetWishListItemId();
			if (itemId.HasValue)
			{
				_wishList.DeleteItem(itemId.Value);
				deleted = true;
			}
			else
			{
				int? productId = GetProductId();
				if (productId.HasValue)
				{
					_wishList.DeleteItem(productId.Value, GetSizeId(), GetPackageItemsHashCode());
					deleted = true;
				}
			}
			
			if (deleted)
			{
				if (_wishList.GetItems().Any())
				{
					//WishList still have any item, then save changes(delete item)
					_wishListService.Save(UserContext.Current, _wishList);
					_wishListCookieService.SetWishListKey(_wishListKey);
				}
				else
				{
					//WishList has no items, then delete it
					_wishListService.Delete(UserContext.Current, _wishList);
					_wishListCookieService.RemoveWishListKey();
					_wishList = null;
				}
			}
		}

		protected virtual void ProcessMoveMode()
		{
			if (_wishList == null)
				return;

			if (!_wishList.GetItems().Any())
				return;

			int? ordinal = GetOrdinal();
			if (!ordinal.HasValue)
				return;

			bool moved = false;

			int? itemId = GetWishListItemId();
			if (itemId.HasValue)
			{
				_wishList.SetItemOrdinal(itemId.Value, ordinal.Value);
				moved = true;
			}
			else
			{
				int? productId = GetProductId();
				if (productId.HasValue)
				{
					_wishList.SetItemOrdinal(productId.Value, GetSizeId(), GetPackageItemsHashCode(), ordinal.Value);
					moved = true;
				}
			}

			if (moved)
			{
				_wishListService.Save(UserContext.Current, _wishList);
				_wishListCookieService.SetWishListKey(_wishListKey);
			}
		}

		protected virtual void ProcessUpdateProductSizeMode()
		{
			if (_wishList == null)
				return;

			if (!_wishList.GetItems().Any())
				return;

			bool updated = false;
			if (ProductUpdateSizeForm.MapFromRequest())
			{
				int itemId = ProductUpdateSizeForm.WishListItemId;
				int productId = ProductUpdateSizeForm.ProductId;
				int sizeId = ProductUpdateSizeForm.ProductSizeId;

				updated = _wishList.UpdateProductSize(itemId, productId, sizeId);
			}

			if (updated)
			{
				_wishListService.Save(UserContext.Current, _wishList);
				_wishListCookieService.SetWishListKey(_wishListKey);
			}
		}

		protected virtual void ProcessUpdatePackageSizesMode()
		{
			if (_wishList == null)
				return;

			if (!_wishList.GetItems().Any())
				return;

			bool updated = false;
			IWishListItem wishListItem = null;
			if (PackageUpdateSizeForm.MapFromRequest())
			{
				int itemId = PackageUpdateSizeForm.WishListItemId;
				List<string> productIds = PackageUpdateSizeForm.ProductIds.Split(',').ToList();
				List<string> sizeIds = PackageUpdateSizeForm.ProductSizeIds.Split(',').ToList();

				updated = _wishList.UpdatePackageSize(itemId, productIds, sizeIds, out wishListItem);
			}

			if (updated)
			{
				foreach (var packageItem in wishListItem.PackageItems)
				{
					_wishListPackageItemService.Update(packageItem);
				}
				_wishListService.Save(UserContext.Current, _wishList);
				_wishListCookieService.SetWishListKey(_wishListKey);
			}
		}

		protected virtual void ProcessForm(Fragment fragmentContent)
		{
			if (_wishList != null)
			{
				if (WishListForm.IsFormPostBack)
				{
					WishListForm.MapFromRequest();
					_wishList.Title = WishListForm.WishListTitle;
					_wishListService.Save(UserContext.Current, _wishList);
				}
				else
				{
					WishListForm.WishListTitle = _wishList.Title;
				}
			}

			WishListForm.MapFieldsToFragment(fragmentContent);
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual Collection<WishListProduct> GetProducts()
		{
			var productCollection = new Collection<WishListProduct>();

			if (_wishList == null || !_wishList.GetItems().Any())
			{
				return productCollection;
			}

			var items = _wishList.GetItems();
			var wishListItems = items as List<IWishListItem> ?? items.ToList();

			var productIdCollection = new ProductIdCollection(wishListItems.Select(x => x.ProductId));
			var products = _productService.PopulateViewProducts(UserContext.Current, productIdCollection);
			foreach (var wishListItem in wishListItems)
			{
				var productView = products.FirstOrDefault(p => p.Id == wishListItem.ProductId);
				if (productView == null) continue;

				var product = (ILekmerProductView)ObjectCopier.Clone(productView);
				if (wishListItem.SizeId.HasValue)
				{
					var productSize = ((ILekmerProductView)productView).ProductSizes.FirstOrDefault(size => size.SizeInfo.Id == wishListItem.SizeId && size.NumberInStock > 0);
					product.DefaultProductSize = productSize;
					productCollection.Add(new WishListProduct(product, wishListItem));
				}
				else
				{
					productCollection.Add(new WishListProduct(product, wishListItem));
				}
			}

			return productCollection;
		}

		protected virtual IPagingControl CreatePagingControl(int totalCount)
		{
			var pagingControl = IoC.Resolve<IPagingControl>();
			pagingControl.PageBaseUrl = PostUrl;
			pagingControl.PageQueryStringParameterName = "wishlist-page";
			pagingControl.PageSize = ColumnCount * RowCount;
			pagingControl.Initialize();
			pagingControl.TotalCount = totalCount;
			return pagingControl;
		}

		protected virtual Price GetActualPriceSummary()
		{
			var priceSummary = new Price();
			foreach (var wishListProduct in _wishListProducts)
			{
				priceSummary += GetActualPrice(wishListProduct.Product);
			}
			return priceSummary;
		}

		protected virtual Price GetActualPrice(ILekmerProduct product)
		{
			decimal priceIncludingVat;
			decimal priceExcludingVat;

			if (product.CampaignInfo != null)
			{
				priceIncludingVat = product.CampaignInfo.Price.IncludingVat;
				priceExcludingVat = product.CampaignInfo.Price.ExcludingVat;
			}
			else
			{
				priceIncludingVat = product.Price.PriceIncludingVat;
				priceExcludingVat = product.Price.PriceExcludingVat;
			}

			var price = new Price(priceIncludingVat, priceExcludingVat);
			return price;
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetWishListItemId()
		{
			return Request.QueryString.GetInt32OrNull("itemid");
		}
		
		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetProductId()
		{
			return Request.QueryString.GetInt32OrNull("id");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetSizeId()
		{
			return Request.QueryString.GetInt32OrNull("sizeid");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetOrdinal()
		{
			return Request.QueryString.GetInt32OrNull("ordinal");
		}

		[SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		protected virtual int? GetPackageItemsHashCode()
		{
			return Request.QueryString.GetInt32OrNull("packageItemsHashCode");
		}
	}
}