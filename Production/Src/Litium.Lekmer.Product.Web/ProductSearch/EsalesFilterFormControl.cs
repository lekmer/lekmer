using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Campaign.Setting;
using Litium.Lekmer.ProductFilter.Web;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;
using QueryBuilder = Litium.Scensum.Core.Web.QueryBuilder;

namespace Litium.Lekmer.Product.Web
{
	public class EsalesFilterFormControl : ControlBase
	{
		public Template Template { get; set; }

		public SelectedFilterOptions SelectedFilterOptions { get; set; }

		public AvailableFilterOptions AvailableFilterOptions { get; set; }

		public ISearchFacet FilterStatistics { get; set; }

		public string ModeFormName
		{
			get { return "mode"; }
		}

		public string SearchMethodTypeFormName
		{
			get { return "sm"; }
		}

		public string ModeValueFilter
		{
			get { return "filter"; }
		}

		public string Render()
		{
			if (Template == null) throw new InvalidOperationException("Template is not set.");
			if (SelectedFilterOptions == null) throw new InvalidOperationException("SelectedFilterOptions is not set.");
			if (AvailableFilterOptions == null) throw new InvalidOperationException("AvailableFilterOptions is not set.");
			if (FilterStatistics == null) throw new InvalidOperationException("FilterStatistics is not set.");

			var fragmentFilterForm = Template.GetFragment("FilterForm");

			AddFormVariables(fragmentFilterForm);

			fragmentFilterForm.AddVariable("Iterate:BrandOption", RenderBrandOptions(), VariableEncoding.None);
			fragmentFilterForm.AddVariable("Iterate:AgeIntervalOption", RenderAgeIntervalOptions(), VariableEncoding.None);
			fragmentFilterForm.AddVariable("Iterate:PriceIntervalOption", RenderPriceIntervalOptions(), VariableEncoding.None);

			fragmentFilterForm.AddRegexVariable(@"\[TagGroup\(\""(.+)\""\)\]",
				delegate(Match match)
				{
					string tagGroupCommonName = match.Groups[1].Value;
					return RenderTagGroup(tagGroupCommonName);
				},
				VariableEncoding.None);

			fragmentFilterForm.AddVariable("Iterate:Level1CategoryOption", RenderLevel1CategoryOptions(), VariableEncoding.None);

			bool hasLevel2Categories = AvailableFilterOptions.Level2Categories != null && AvailableFilterOptions.Level2Categories.Count() > 0;
			fragmentFilterForm.AddCondition("HasLevel2CategoryOptions", hasLevel2Categories);
			if (hasLevel2Categories)
			{
				fragmentFilterForm.AddVariable("Iterate:Level2CategoryOption", RenderLevel2CategoryOptions(), VariableEncoding.None);
			}

			bool hasLevel3Categories = AvailableFilterOptions.Level3Categories != null && AvailableFilterOptions.Level3Categories.Count() > 0;
			fragmentFilterForm.AddCondition("HasLevel3CategoryOptions", hasLevel3Categories);
			if (hasLevel3Categories)
			{
				fragmentFilterForm.AddVariable("Iterate:Level3CategoryOption", RenderLevel3CategoryOptions(), VariableEncoding.None);
			}

			fragmentFilterForm.AddVariable("Iterate:SizeOption", RenderSizeOptions(), VariableEncoding.None);

			fragmentFilterForm.AddVariable("Iterate:SortOption", RenderSortOptions(), VariableEncoding.None);
			fragmentFilterForm.AddVariable("Iterate:PageSizeOption", RenderPageSizeOptions(), VariableEncoding.None);

			fragmentFilterForm.AddCondition("HasSecondaryTemplate", AvailableFilterOptions.HasSecondaryTemplate);
			fragmentFilterForm.AddCondition("UseSecondaryTemplate", SelectedFilterOptions.UseSecondaryTemplate);

			AddTemplateLinks(fragmentFilterForm);

			return fragmentFilterForm.Render();
		}

		private void AddTemplateLinks(Fragment fragment)
		{
			var queryBuilder = new QueryBuilder(Request.QueryString);

			queryBuilder.Remove("usesecondarytemplate");
			string primaryQuery = queryBuilder.ToString();

			queryBuilder.Add("usesecondarytemplate", "true");
			string secondaryQuery = queryBuilder.ToString();

			string baseUrl = ResolveUrl("~" + Request.RelativeUrlWithoutQueryString());

			fragment.AddVariable("PrimaryTemplateLink", baseUrl + primaryQuery);
			fragment.AddVariable("SecondaryTemplateLink", baseUrl + secondaryQuery);
		}

		private void AddFormVariables(Fragment fragment)
		{
			fragment.AddVariable("Form.Mode.Name", ModeFormName);
			fragment.AddVariable("Form.Mode.Value.Filter", ModeValueFilter);

			fragment.AddVariable("Form.SearchMethodType.Name", SearchMethodTypeFormName);
			fragment.AddVariable("Form.SearchMethodType.Value", SelectedFilterOptions.SearchMethodType.ToString("D"));
		}

		private string RenderTagGroup(string commonName)
		{
			if (commonName == null) throw new ArgumentNullException("commonName");

			var tagGroup = AvailableFilterOptions.TagGroups
				.FirstOrDefault(group => group.CommonName.Equals(commonName, StringComparison.OrdinalIgnoreCase));

			if (tagGroup == null)
			{
				return string.Format(CultureInfo.InvariantCulture, "[ TagGroup '{0}' not found ]", commonName);
			}

			var fragmentTagGroup = Template.GetFragment("TagGroup");
			fragmentTagGroup.AddVariable("Iterate:TagOption", RenderTagOptions(tagGroup), VariableEncoding.None);
			fragmentTagGroup.AddVariable("TagGroup.Id", tagGroup.Id.ToString(CultureInfo.InvariantCulture));
			return fragmentTagGroup.Render();
		}

		private string RenderTagOptions(ITagGroup tagGroup)
		{
			var tagBuilder = new StringBuilder();
			foreach (var tag in tagGroup.Tags)
			{
				//Only render campaign tags if campaign is active/valid/online/correct channel (since we dont have channel specific tags)
				if (tag.TagGroupId == AutoCampaignPageSetting.Instance.CampaignTagGroupId)
				{
					var campaign = IoC.Resolve<IProductCampaignService>().GetById(UserContext.Current, (int.Parse(tag.CommonName, CultureInfo.CurrentCulture)));
					if (campaign == null) continue;

					var registries = IoC.Resolve<ICampaignRegistryCampaignService>().GetRegistriesByCampaignId(campaign.Id);
					if (registries.FirstOrDefault(r => r.ChannelId == UserContext.Current.Channel.Id) == null ||
						campaign.CampaignStatus.Id != 0 ||
						(campaign.StartDate.HasValue && campaign.StartDate > DateTime.Now) ||
						(campaign.EndDate.HasValue && campaign.EndDate < DateTime.Now))
					{
						continue;
					}
				}

				int tagId = tag.Id;
				int count;
				FilterStatistics.TagsWithFilter.TryGetValue(tagId, out count);
				bool isSelected = SelectedFilterOptions.GroupedTagIds
					.Any(group => group != null && group.Any(id => id == tagId));
				tagBuilder.AppendLine(RenderOption("TagOption", tagId, tag.Value, isSelected, count));
			}
			return tagBuilder.ToString();
		}

		private string RenderSortOptions()
		{
			var sortOptionBuilder = new StringBuilder();
			foreach (var sortOption in AvailableFilterOptions.SortOptions)
			{
				var fragmentSortOption = Template.GetFragment("SortOption");
				fragmentSortOption.AddVariable("Name", sortOption.Value, VariableEncoding.None);
				fragmentSortOption.AddVariable("Value", sortOption.Key.ToString());
				fragmentSortOption.AddCondition("IsSelected", sortOption.Key == SelectedFilterOptions.SortOption);
				sortOptionBuilder.AppendLine(fragmentSortOption.Render());
			}
			return sortOptionBuilder.ToString();
		}

		private string RenderPageSizeOptions()
		{
			var pageSizeBuilder = new StringBuilder();
			foreach (var pageSize in AvailableFilterOptions.PageSizeOptions)
			{
				var fragmentPageSizeOption = Template.GetFragment("PageSizeOption");
				fragmentPageSizeOption.AddVariable("Name", pageSize.ToString(CultureInfo.InvariantCulture));
				fragmentPageSizeOption.AddVariable("Value", pageSize.ToString(CultureInfo.InvariantCulture));
				fragmentPageSizeOption.AddCondition("IsSelected", pageSize == SelectedFilterOptions.PageSize);
				pageSizeBuilder.AppendLine(fragmentPageSizeOption.Render());
			}
			return pageSizeBuilder.ToString();
		}

		private string RenderAgeIntervalOptions()
		{
			var ageIntervalAvailable = AvailableFilterOptions.AgeIntervalOuter;
			var ageIntervalFiltered = FilterStatistics.AgeInterval;
			var ageIntervalSelected = SelectedFilterOptions.AgeInterval ?? ageIntervalAvailable;

			var fragmentOption = Template.GetFragment("AgeIntervalOption");

			if (ageIntervalAvailable != null)
			{
				fragmentOption.AddVariable("AgeAvailable.FromMonth", RenderNullableVariable(ageIntervalAvailable.FromMonth));
				fragmentOption.AddVariable("AgeAvailable.ToMonth", RenderNullableVariable(ageIntervalAvailable.ToMonth));
				fragmentOption.AddCondition("AgeAvailable.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("AgeAvailable.Exist", false);
			}

			if (ageIntervalFiltered != null)
			{
				fragmentOption.AddVariable("AgeFiltered.FromMonth", RenderNullableVariable(ageIntervalFiltered.FromMonth));
				fragmentOption.AddVariable("AgeFiltered.ToMonth", RenderNullableVariable(ageIntervalFiltered.ToMonth));
				fragmentOption.AddCondition("AgeFiltered.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("AgeFiltered.Exist", false);
			}

			if (ageIntervalSelected != null)
			{
				fragmentOption.AddVariable("AgeSelected.FromMonth", RenderNullableVariable(ageIntervalSelected.FromMonth));
				fragmentOption.AddVariable("AgeSelected.ToMonth", RenderNullableVariable(ageIntervalSelected.ToMonth));
				fragmentOption.AddCondition("AgeSelected.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("AgeSelected.Exist", false);
			}

			return fragmentOption.Render();
		}

		private string RenderPriceIntervalOptions()
		{
			var priceIntervalAvailable = AvailableFilterOptions.PriceIntervalOuter;
			var priceIntervalFiltered = FilterStatistics.PriceInterval;
			var priceIntervalSelected = SelectedFilterOptions.PriceInterval ?? priceIntervalAvailable;

			var fragmentOption = Template.GetFragment("PriceIntervalOption");

			if (priceIntervalAvailable != null)
			{
				fragmentOption.AddVariable("PriceAvailable.From", RenderNullableVariable(priceIntervalAvailable.From));
				fragmentOption.AddVariable("PriceAvailable.To", RenderNullableVariable(priceIntervalAvailable.To));
				fragmentOption.AddCondition("PriceAvailable.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("PriceAvailable.Exist", false);
			}

			if (priceIntervalFiltered != null)
			{
				fragmentOption.AddVariable("PriceFiltered.From", RenderNullableVariable(priceIntervalFiltered.From));
				fragmentOption.AddVariable("PriceFiltered.To", RenderNullableVariable(priceIntervalFiltered.To));
				fragmentOption.AddCondition("PriceFiltered.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("PriceFiltered.Exist", false);
			}

			if (priceIntervalSelected != null)
			{
				fragmentOption.AddVariable("PriceSelected.From", RenderNullableVariable(priceIntervalSelected.From));
				fragmentOption.AddVariable("PriceSelected.To", RenderNullableVariable(priceIntervalSelected.To));
				fragmentOption.AddCondition("PriceSelected.Exist", true);
			}
			else
			{
				fragmentOption.AddCondition("PriceSelected.Exist", true);
			}

			return fragmentOption.Render();
		}

		private string RenderBrandOptions()
		{
			var brandBuilder = new StringBuilder();
			foreach (IBrand brand in AvailableFilterOptions.Brands)
			{
				int brandId = brand.Id;
				int count;
				FilterStatistics.BrandsWithFilter.TryGetValue(brandId, out count);
				bool isSelected = SelectedFilterOptions.BrandIds.Any(id => id == brandId);
				brandBuilder.AppendLine(RenderOption("BrandOption", brandId, brand.Title, isSelected, count));
			}
			return brandBuilder.ToString();
		}

		private string RenderLevel1CategoryOptions()
		{
			return RenderCategoryOptions("Level1CategoryOption", AvailableFilterOptions.Level1Categories, SelectedFilterOptions.Level1CategoryIds, FilterStatistics.MainCategoriesWithFilter);
		}

		private string RenderLevel2CategoryOptions()
		{
			return RenderCategoryOptions("Level2CategoryOption", AvailableFilterOptions.Level2Categories, SelectedFilterOptions.Level2CategoryIds, FilterStatistics.ParentCategoriesWithFilter);
		}

		private string RenderLevel3CategoryOptions()
		{
			return RenderCategoryOptions("Level3CategoryOption", AvailableFilterOptions.Level3Categories, SelectedFilterOptions.Level3CategoryIds, FilterStatistics.CategoriesWithFilter);
		}

		private string RenderCategoryOptions(string optionFragmentName, IEnumerable<ICategory> availableCategories, Collection<int> selectedCategoryIds, Dictionary<int, int> categoryStatistic)
		{
			if (optionFragmentName == null) throw new ArgumentNullException("optionFragmentName");
			if (availableCategories == null) throw new ArgumentNullException("availableCategories");
			if (selectedCategoryIds == null) throw new ArgumentNullException("selectedCategoryIds");

			var categoryBuilder = new StringBuilder();
			foreach (ICategory category in availableCategories.OrderBy(product => product.Title))
			{
				int categoryId = category.Id;
				int count;
				categoryStatistic.TryGetValue(categoryId, out count);
				bool isSelected = selectedCategoryIds.Any(id => id == categoryId);
				categoryBuilder.AppendLine(RenderOption(optionFragmentName, categoryId, category.Title, isSelected, count));
			}
			return categoryBuilder.ToString();
		}

		private string RenderSizeOptions()
		{
			var sizeBuilder = new StringBuilder();
			foreach (ISize size in AvailableFilterOptions.Sizes)
			{
				int sizeId = size.Id;
				int count;
				FilterStatistics.SizesWithFilter.TryGetValue(sizeId, out count);
				bool isSelected = SelectedFilterOptions.SizeIds.Any(id => id == sizeId);
				sizeBuilder.AppendLine(RenderOption("SizeOption", sizeId, size.EuTitle ?? size.Eu.ToString(CultureInfo.InvariantCulture), isSelected, count));
			}
			return sizeBuilder.ToString();
		}

		private string RenderOption(string optionFragmentName, int id, string name, bool isSelected, int count)
		{
			var fragmentOption = Template.GetFragment(optionFragmentName);
			fragmentOption.AddVariable("Name", name);
			fragmentOption.AddVariable("Value", id.ToString(CultureInfo.InvariantCulture));
			fragmentOption.AddCondition("IsSelected", isSelected);
			fragmentOption.AddVariable("Count", count.ToString(CultureInfo.InvariantCulture));
			fragmentOption.AddCondition("CountIsZero", count == 0);
			return fragmentOption.Render();
		}

		private static string RenderNullableVariable<T>(T? variable) where T : struct, IConvertible
		{
			if (variable.HasValue)
			{
				return variable.Value.ToString(CultureInfo.InvariantCulture);
			}

			return string.Empty;
		}
	}
}