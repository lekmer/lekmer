﻿using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.ProductSearch.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web.ProductSearch
{
	/// <summary>
	/// Search form control.
	/// </summary>
	public class LekmerSearchFormControl : SearchFormControl
	{
		public LekmerSearchFormControl(ITemplateFactory templateFactory)
			: base(templateFactory)
		{
		}

		public override ComponentContent Render()
		{
			string query = Request.QueryString[QueryParameterName()];

			Fragment fragmentContent = Template.GetFragment("Content");
			fragmentContent.AddVariable("Query", query.EncodeHtml(), VariableEncoding.JavaScriptStringEncode);

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			return new ComponentContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}
	}
}