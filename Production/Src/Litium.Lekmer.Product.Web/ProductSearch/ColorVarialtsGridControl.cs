﻿using Litium.Scensum.Core.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Lekmer.Common.Web;
using Litium.Lekmer.Product;
using Litium.Scensum.Product;
using Litium.Scensum.Template.Engine;
using RelationListType = Litium.Scensum.Product.RelationListType;

namespace Litium.Lekmer.ProductFilter.Web
{
	public class ColorVarialtsGridControl : GridControl<IProduct>
	{
		public ILekmerProductService LekmerProductService { get; set; }
		protected override string RenderItem(int columnIndex, IProduct item)
		{
			Fragment fragmentItem = Template.GetFragment(ItemFragmentName);
			var fragmentVariableInfo = new FragmentVariableInfo(fragmentItem);

			AddPositionCondition(fragmentItem, columnIndex, ColumnCount);
			fragmentItem.AddEntity(item);

			var lekmerProduct = (ILekmerProduct)item;

			if (lekmerProduct.ShowVariantRelations)
			{
				ProductCollection colorVariants = GetColorVariants(lekmerProduct);
				if (colorVariants != null)
				{
					fragmentItem.AddCondition("Product.HasVariants", colorVariants.Count > 0);

					if (colorVariants.Count > 0)
					{
						fragmentItem.AddVariable("Iterate:ColorOption", RenderColorOptions(lekmerProduct, colorVariants), VariableEncoding.None);
					}
				}
				else
				{
					fragmentItem.AddCondition("Product.HasVariants", false);
					fragmentItem.AddVariable("Iterate:ColorOption", string.Empty, VariableEncoding.None);
				}
			}
			else
			{
				fragmentItem.AddCondition("Product.HasVariants", false);
				fragmentItem.AddVariable("Iterate:ColorOption", string.Empty, VariableEncoding.None);
			}

			return fragmentItem.Render();
		}

		protected virtual string RenderColorOptions(ILekmerProduct product, ProductCollection colorVariants)
		{
			var optionBuilder = new StringBuilder();

			if (colorVariants != null)
			{
				foreach (var item in colorVariants)
				{
					var fragmentOption = Template.GetFragment("ProductColorOption");
					fragmentOption.AddEntity(item);
					optionBuilder.AppendLine(fragmentOption.Render());
				}
			}



			return optionBuilder.ToString();
		}

		protected virtual ProductCollection GetColorVariants(ILekmerProduct product)
		{
			if (product == null) return null;

			var id = product.Id;
			var variants = LekmerProductService.GetAllByProductAndRelationType(UserContext.Current, id, Product.RelationListType.Variant.ToString()).ToDictionary(i => i.Id, i => i);
			var colorVariants = LekmerProductService.GetAllByProductAndRelationType(UserContext.Current, id, Product.RelationListType.ColorVariant.ToString()).ToDictionary(i => i.Id, i => i);

			var results = new Dictionary<int, IProduct>(variants);
			foreach (var colorVariant in colorVariants)
			{
				if (results.ContainsKey(colorVariant.Key)) continue;

				results.Add(colorVariant.Key, colorVariant.Value);
			}
			return new ProductCollection(results.Values);
		}
	}
}
