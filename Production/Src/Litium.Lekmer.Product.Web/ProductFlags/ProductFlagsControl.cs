﻿using System;
using System.Collections.ObjectModel;
using System.Text;
using Litium.Lekmer.Campaign;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Template;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class ProductFlagsControl : ControlBase, IProductFlagsControl
	{
		private readonly ITemplateFactory _templateFactory;
		private readonly IIncludeSharedService _includeSharedService;

		/// <summary>
		/// Common name of the model for the template.
		/// </summary>
		protected virtual string ModelCommonName
		{
			get { return "CampaignFlags"; }
		}


		/// <summary>
		/// Initializes the <see cref="ProductFlagsControl"/>.
		/// </summary>
		/// <param name="templateFactory">Instance of <see cref="ITemplateFactory"/>.</param>
		/// <param name="includeSharedService">Instance of <see cref="IIncludeSharedService"/>.</param>
		public ProductFlagsControl(ITemplateFactory templateFactory, IIncludeSharedService includeSharedService)
		{
			_templateFactory = templateFactory;
			_includeSharedService = includeSharedService;
		}


		/// <summary>
		/// Render the component's body.
		/// </summary>
		public virtual string Render(Collection<IFlag> flags, int? templateId = null)
		{
			Template template = CreateTemplate(templateId);

			return RenderFlagList(flags, template);
		}

		/// <summary>
		/// Render the component's body.
		/// </summary>
		public virtual string RenderFromIncludes(Collection<IFlag> flags)
		{
			const string flagsContainerIncludeName = "product-flags-container";
			const string flagIncludeName = "product-flag-item";

			var flagsContainerInclude = _includeSharedService.GetByCommonName(flagsContainerIncludeName);
			var flagInclude = _includeSharedService.GetByCommonName(flagIncludeName);

			if (flagsContainerInclude == null || flagInclude == null)
			{
				return string.Empty;
			}

			var flagsContainerContent = flagsContainerInclude.Content;
			var flagContent = flagInclude.Content;

			if (flagsContainerContent.IsEmpty() || flagContent.IsEmpty())
			{
				return string.Empty;
			}

			var flagsIteration = new StringBuilder();
			foreach (var flag in flags)
			{
				var template = (string)flagContent.Clone();
				flagsIteration.AppendLine(template
					.Replace("[Flag.Title]", flag.Title)
					.Replace("[Flag.Class]", flag.Class));
			}

			flagsIteration.Length -= Environment.NewLine.Length;

			return flagsContainerContent.Replace("[Iterate:Flag]", flagsIteration.ToString());
		}


		protected virtual string RenderFlagList(Collection<IFlag> flags, Template template)
		{
			var list = new FlagListControl<IFlag>
			{
				Template = template,
				ListFragmentName = "FlagList",
				ItemFragmentName = "Flag",
				Items = flags
			};

			return list.Render();
		}


		/// <summary>
		/// Creates a new instance of <see cref="Scensum.Template.Engine.Template"/>.
		/// </summary>
		/// <returns>A new instance of <see cref="Scensum.Template.Engine.Template"/>.</returns>
		protected virtual Template CreateTemplate(int? templateId)
		{
			return _templateFactory.Create(ModelCommonName, templateId);
		}
	}
}
