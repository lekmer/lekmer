﻿using System.Text;
using Litium.Scensum.Core.Web;
using Litium.Scensum.Product;
using Litium.Scensum.Product.Web;
using Litium.Scensum.SiteStructure;
using Litium.Scensum.SiteStructure.Web;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class BlockProductPackageListControl : BlockProductPageControlBase<IBlock>
	{
		private readonly IBlockService _blockService;
		private readonly IPackageService _packageService;
		private readonly ILekmerProductService _productService;

		public BlockProductPackageListControl(ITemplateFactory templateFactory, IBlockService blockService, IPackageService packageService, IProductService productService)
			: base(templateFactory)
		{
			_blockService = blockService;
			_packageService = packageService;
			_productService = (ILekmerProductService) productService;
		}

		protected override IBlock GetBlockById(int blockId)
		{
			return _blockService.GetById(UserContext.Current, blockId);
		}

		protected override BlockContent RenderCore()
		{
			var lekmerProduct = (ILekmerProduct) Product;

			if (lekmerProduct == null)
			{
				return new BlockContent(null, "[ Can't render product on this page ]");
			}

			string head = RenderFragment("Head");
			string footer = RenderFragment("Footer");
			Fragment fragmentContent = Template.GetFragment("Content");

			var hasPackages = false;
			var packageList = string.Empty;
			if (lekmerProduct.SellOnlyInPackage)
			{
				var packageMasterProductIds = _packageService.GetMasterProductIdsByPackageProduct(UserContext.Current, Product.Id);
				var packageMasterProducts = _productService.PopulateProductsWithOnlyInPackage(UserContext.Current, packageMasterProductIds);
				if (packageMasterProducts.Count > 0)
				{
					hasPackages = true;
					packageList = RenderPackageList(packageMasterProducts);
				}
			}
			fragmentContent.AddCondition("Product.HasPackages", hasPackages);
			fragmentContent.AddVariable("PackageList", packageList, VariableEncoding.None);

			return new BlockContent(head, fragmentContent.Render(), footer);
		}

		private string RenderFragment(string fragmentName)
		{
			return Template.GetFragment(fragmentName).Render();
		}

		protected virtual string RenderPackageList(ProductCollection products)
		{
			Fragment fragment = Template.GetFragment("PackageList");

			var stringBuilder = new StringBuilder();
			foreach (var product in products)
			{
				stringBuilder.AppendLine(RenderPackageItem(product).Render());
			}
			fragment.AddVariable("Iterate:PackageItem", stringBuilder.ToString(), VariableEncoding.None);

			return fragment.Render();
		}

		protected virtual Fragment RenderPackageItem(IProduct product)
		{
			Fragment fragment = Template.GetFragment("PackageItem");

			var lekmerProduct = (ILekmerProduct) product;
			fragment.AddVariable("Product.Url", UrlHelper.ResolveUrlHttp("~/" + lekmerProduct.LekmerUrl));
			fragment.AddVariable("Product.Title", lekmerProduct.DisplayTitle);

			return fragment;
		}
	}
}