﻿using System.Globalization;
using Litium.Scensum.Template.Engine;

namespace Litium.Lekmer.Product.Web
{
	public class IconEntityMapper : EntityMapper<IIcon>
	{
		public override void AddEntityVariables(Fragment fragment, IIcon item)
		{
			fragment.AddVariable("Icon.Id", item.Id.ToString(CultureInfo.InvariantCulture));
			fragment.AddVariable("Icon.Title", item.Title);
			fragment.AddVariable("Icon.Description", item.Description);
			IconHelper.AddImageVariables(fragment, item.Image, item.Title);
		}
	}
}