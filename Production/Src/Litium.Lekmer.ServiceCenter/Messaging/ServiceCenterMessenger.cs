﻿using System;
using System.Globalization;
using System.Web;
using Litium.Framework.Messaging;
using Litium.Lekmer.ServiceCenter.Setting;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.ServiceCenter
{
	public class ServiceCenterMessenger : MessengerBase<ServiceCenterMessageArgs>
	{
		protected ServiceCenterEmailTemplate Template { get; set; }

		protected override string MessengerName
		{
			get { return "ServiceCenter"; }
		}

		protected override Message CreateMessage(ServiceCenterMessageArgs messageArgs)
		{
			if (messageArgs == null)
			{
				throw new ArgumentNullException("messageArgs");
			}

			string email = ServiceCenterSetting.Instance.GetChannelEmailTo(messageArgs.Channel.CommonName);
			if (email.IsNullOrTrimmedEmpty())
			{
				throw new ArgumentException("Empty email");
			}

			Template = new ServiceCenterEmailTemplate(messageArgs.Channel);

			var message = new EmailMessage
			{
				Body = RenderMessage(messageArgs),
				FromEmail = RenderFromEmail(messageArgs),
				FromName = RenderFromName(messageArgs),
				Subject = RenderSubject(messageArgs),
				ProviderName = "email",
				Type = MessageType.Single
			};

			message.Recipients.Add(new EmailRecipient
			{
				Name = email,
				Address = email
			});

			return message;
		}

		private string RenderMessage(ServiceCenterMessageArgs messageArgs)
		{
			var htmlFragment = Template.GetFragment("Content");
			var hasSenderOrderNo = messageArgs.SenderOrderNo.HasValue;
			htmlFragment.AddCondition("ServiceCenter.Case.HasOrderNo", hasSenderOrderNo);
			htmlFragment.AddVariable("ServiceCenter.Case.OrderNo", hasSenderOrderNo ? messageArgs.SenderOrderNo.Value.ToString(CultureInfo.InvariantCulture) : string.Empty);
			htmlFragment.AddVariable("ServiceCenter.Case.Message", messageArgs.Message);
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}

		private string RenderSubject(ServiceCenterMessageArgs messageArgs)
		{
			var htmlFragment = Template.GetFragment("Subject");
			htmlFragment.AddVariable("ServiceCenter.Case.CategoryId", messageArgs.CategoryId);
			htmlFragment.AddVariable("ServiceCenter.Case.CategoryValue", messageArgs.CategoryValue);
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}
		private string RenderFromEmail(ServiceCenterMessageArgs messageArgs)
		{
			var htmlFragment = Template.GetFragment("FromEmail");
			htmlFragment.AddVariable("ServiceCenter.Case.SenderEmail", messageArgs.SenderEmail);
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}
		private string RenderFromName(ServiceCenterMessageArgs messageArgs)
		{
			var htmlFragment = Template.GetFragment("FromName");
			htmlFragment.AddVariable("ServiceCenter.Case.SenderFirstName", messageArgs.SenderFirstName);
			htmlFragment.AddVariable("ServiceCenter.Case.SenderLastName", messageArgs.SenderLastName);
			return HttpUtility.HtmlDecode(htmlFragment.Render());
		}
	}
}