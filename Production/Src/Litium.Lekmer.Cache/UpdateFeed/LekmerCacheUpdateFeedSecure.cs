﻿using System;
using System.Collections.Generic;
using Litium.Framework.Cache.UpdateFeed;

namespace Litium.Lekmer.Cache.UpdateFeed
{
	public class LekmerCacheUpdateFeedSecure : ILekmerCacheUpdateFeedSecure
	{
		private readonly LekmerCacheUpdateFeedRepository _repository;

		public LekmerCacheUpdateFeedSecure()
		{
			_repository = new LekmerCacheUpdateFeedRepository();
		}

		public LekmerCacheUpdateFeedSecure(LekmerCacheUpdateFeedRepository repository)
		{
			_repository = repository;
		}

		/// <summary>
		/// Deletes update items older then <paramref name="toDate"/>.
		/// </summary>
		/// <param name="toDate">Items older then this should be removed.</param>
		public virtual void DeleteOldUpdateItems(DateTime toDate)
		{
			_repository.DeleteOldUpdateItems(toDate);
		}
	}
}