﻿using Litium.Framework.Cache.UpdateFeed;

namespace Litium.Lekmer.Cache.UpdateFeed
{
	/// <summary>
	/// Update feed item that contains information of what to be cleared from the cache.
	/// </summary>
	public interface ILekmerCacheUpdateFeedItem : ICacheUpdateFeedItem
	{
		/// <summary>
		/// Id of feed item.
		/// </summary>
		long Id { get; set; }
	}
}