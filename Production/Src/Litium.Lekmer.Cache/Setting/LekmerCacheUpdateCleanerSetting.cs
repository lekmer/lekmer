using Litium.Framework.Setting;

namespace Litium.Lekmer.Cache.Setting
{
	/// <summary>
	/// Settings for cache.
	/// </summary>
	public class LekmerCacheUpdateCleanerSetting : SettingBase
	{
		protected override string StorageName
		{
			get { return "Cache"; }
		}

		protected virtual string GroupName
		{
			get { return "CacheUpdateCleaner"; }
		}

		public int CacheUpdateIntervalSeconds
		{
			get { return GetInt32(GroupName, "CacheUpdateIntervalSeconds", 60); }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Ttl")]
		public int CacheUpdateTtlMinutes
		{
			get { return GetInt32(GroupName, "CacheUpdateTtlMinutes", 10); }
		}
	}
}