﻿namespace Litium.Lekmer.CdonExport
{
	public enum CdonProductStatus
	{
		ONLINE = 0,
		OFFLINE = 1,
		DELETED = 2
	}
}