﻿namespace Litium.Lekmer.CdonExport
{
	public enum CdonExportStatus
	{
		InProgress,
		Finished,
		Failed,
		InvalidXml
	}
}