﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionCategory : IBusinessObjectBase
	{
		int ProductRegistryId { get; set; }
		int CategoryId { get; set; }
		string RestrictionReason { get; set; }
		int? UserId { get; set; }
		DateTime CreatedDate { get; set; }
		int ChannelId { get; set; }
	}
}