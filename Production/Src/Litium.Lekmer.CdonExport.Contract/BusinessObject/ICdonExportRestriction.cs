﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestriction : IBusinessObjectBase
	{
		Collection<ICdonExportRestrictionItem> IncludeProducts { get; set; }
		Collection<ICdonExportRestrictionItem> ExcludeProducts { get; set; }

		Collection<ICdonExportRestrictionItem> IncludeCategories { get; set; }
		Collection<ICdonExportRestrictionItem> ExcludeCategories { get; set; }

		Collection<ICdonExportRestrictionItem> IncludeBrands { get; set; }
		Collection<ICdonExportRestrictionItem> ExcludeBrands { get; set; }
	}
}