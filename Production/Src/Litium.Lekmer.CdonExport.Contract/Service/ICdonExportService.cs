﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.CdonExport
{
	public interface ICdonExportService
	{
		string TagAttributeValuePrefix { get; }

		ILog Log { get; set; }

		Dictionary<int, ICategoryTree> GetAllCategoryTrees(Collection<int> topLevelCategoryIds);
		Dictionary<int, Dictionary<int, IBrand>> GetAllBrands();
		Dictionary<int, Dictionary<int, ITagGroup>> GetAllTagGroups();
		Collection<ISize> GetAllSizes();
		Dictionary<int, ICdonIcon> GetAllIcons();
		Dictionary<int, Dictionary<string, string>> GetAllTagAliases();
		Collection<ICdonProductInfo> GetAllProducts();
		Collection<ICdonProductInfo> GetChangedProducts();
		void PopulateVariantErpIds(Collection<ICdonProductInfo> cdonProductInfoCollection);
		Collection<ICdonProductInfo> PopulateVariants(Collection<ICdonProductInfo> cdonProductInfoCollection);
		Dictionary<int, Collection<int>> GetRestrictedBrands();
		Dictionary<int, Collection<int>> GetRestrictedCategories();
		Dictionary<int, Collection<int>> GetRestrictedProducts();
		Dictionary<int, Collection<int>> GetIncludeBrands();
		Dictionary<int, Collection<int>> GetIncludeCategories();
		Dictionary<int, Collection<int>> GetIncludeProducts();
		Dictionary<int, Collection<IProductRegistryRestrictionItem>> GetHeppoRestrictedBrands();
		Dictionary<int, Collection<IProductRegistryRestrictionItem>> GetHeppoRestrictedCategories();
		Dictionary<int, Collection<IProductRegistryRestrictionItem>> GetHeppoRestrictedProducts();

		void SetStatusForProductChanges(Collection<IProductChangeEvent> productChanges, ProductChangeEventStatus status);
		string GetCountryIsoCode(int channelId);
		IChannel GetChannel(int channelId);
		string GetChannelApplicationName(int channelId);
		string GetAlias(IChannel channel, string aliasCommonName);
		string GetAlias(int channelId, string aliasCommonName);
		Dictionary<int, Dictionary<int, ICdonTag>> GetAllTagsByTagGroup(Dictionary<int, ITagGroup> allTagGroups);
		
		string GetLastItem();
		int Save(string importId, CdonExportType type, CdonExportStatus status);
		void Update(int cdonExportId, CdonExportStatus status);


		Dictionary<int, int> ProductMappingGetAll();
		int ProductMappingSave(int cdonProductId, int cdonArticleId);
	}
}