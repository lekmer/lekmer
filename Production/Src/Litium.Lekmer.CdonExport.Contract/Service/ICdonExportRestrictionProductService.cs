﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.CdonExport.Contract
{
	public interface ICdonExportRestrictionProductService
	{
		Collection<ICdonExportRestrictionItem> GetIncludeAll();
		Collection<ICdonExportRestrictionItem> GetRestrictionAll();
	}
}