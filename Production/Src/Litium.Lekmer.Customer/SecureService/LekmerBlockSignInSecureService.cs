﻿using Litium.Lekmer.Customer.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.Customer.SecureService
{
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "InSecure")]
	public class LekmerBlockSignInSecureService : BlockSignInSecureService
	{
		public LekmerBlockSignInSecureService(IAccessValidator accessValidator, BlockSignInRepository blockSignInRepository, IAccessSecureService accessSecureService, IBlockSecureService blockService)
			: base(accessValidator, blockSignInRepository, accessSecureService, blockService)
		{
		}

		public override int Save(ISystemUserFull systemUserFull, IBlockSignIn block)
		{
			int blockId = base.Save(systemUserFull, block);
			BlockSignInCache.Instance.Remove(blockId);

			return blockId;
		}

		public override void Delete(ISystemUserFull systemUserFull, int blockId)
		{
			base.Delete(systemUserFull, blockId);
			BlockSignInCache.Instance.Remove(blockId);
		}
	}
}