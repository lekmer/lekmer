﻿using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;

namespace Litium.Lekmer.Customer
{
	public class LekmerAddressSecureService : AddressSecureService, ILekmerAddressSecureService
	{
		protected LekmerAddressRepository LekmerRepository { get; private set; }

		public LekmerAddressSecureService(AddressRepository repository)
			: base(repository)
		{
			LekmerRepository = (LekmerAddressRepository) repository;
		}

		public void DeleteByCustomer(ISystemUserFull systemUserFull, int customerId)
		{
			LekmerRepository.EnsureNotNull();
			LekmerRepository.DeleteByCustomer(customerId);
		}
	}
}