﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;

namespace Litium.Lekmer.Customer
{
	public class LekmerCustomerGroupSecureService : CustomerGroupSecureService, ILekmerCustomerGroupSecureService
	{
		public LekmerCustomerGroupSecureService(IAccessValidator accessValidator, CustomerGroupRepository customerGroupRepository)
			: base(accessValidator, customerGroupRepository)
		{
		}

		public virtual Collection<ICustomerGroup> GetAll()
		{
			Repository.EnsureNotNull();

			return ((LekmerCustomerGroupRepository) Repository).GetAll();
		}

		public override void Delete(ISystemUserFull user, int id)
		{
			base.Delete(user, id);
			CustomerGroupCollectionCache.Instance.Flush();
		}

		public override int Save(ISystemUserFull user, ICustomerGroup customerGroup, Collection<ICustomer> customers)
		{
			int customerGroupId = base.Save(user, customerGroup, customers);
			CustomerGroupCollectionCache.Instance.Flush();

			return customerGroupId;
		}
	}
}