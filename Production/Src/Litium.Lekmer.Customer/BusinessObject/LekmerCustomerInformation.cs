﻿using System;
using Litium.Scensum.Customer;

namespace Litium.Lekmer.Customer
{
	[Serializable]
	public class LekmerCustomerInformation : CustomerInformation, ILekmerCustomerInformation
	{
		private int _genderTypeId;
		private bool _isCompany;
		private int? _alternateAddressId;
		private ILekmerAddress _alternateAddress;

		public int GenderTypeId
		{
			get { return _genderTypeId; }
			set
			{
				CheckChanged(_genderTypeId, value);
				_genderTypeId = value;
			}
		}

		public bool IsCompany
		{
			get { return _isCompany; }
			set
			{
				CheckChanged(_isCompany, value);
				_isCompany = value;
			}
		}
		
		public int? AlternateAddressId
		{
			get { return _alternateAddressId; }
			set
			{
				CheckChanged(_alternateAddressId, value);
				_alternateAddressId = value;
			}
		}

		public ILekmerAddress AlternateAddress
		{
			get { return _alternateAddress; }
			set
			{
				CheckChanged(_alternateAddress, value);
				_alternateAddress = value;
			}
		}
	}
}