﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Cache;

namespace Litium.Lekmer.Customer.Cache
{
	public sealed class BlockSignInCache : ScensumCacheBase<BlockSignInKey, IBlockSignIn>
	{
		private static readonly BlockSignInCache _instance = new BlockSignInCache();

		private BlockSignInCache()
		{
		}

		public static BlockSignInCache Instance
		{
			get { return _instance; }
		}

		public void Remove(int blockId)
		{
			var channels = IoC.Resolve<IChannelSecureService>().GetAll();
			foreach (var channel in channels)
			{
				Remove(new BlockSignInKey(channel.Id, blockId));
			}
		}
	}

	public class BlockSignInKey : ICacheKey
	{
		public BlockSignInKey(int channelId, int blockId)
		{
			ChannelId = channelId;
			BlockId = blockId;
		}

		public int ChannelId { get; set; }
		public int BlockId { get; set; }

		public string Key
		{
			get
			{
				return string.Format(CultureInfo.InvariantCulture, "{0}-{1}", ChannelId, BlockId);
			}
		}
	}
}