﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Customer.Repository
{
	public class LekmerCustomerRepository : CustomerRepository
	{
		public virtual void Delete(int customerId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CustomerId", customerId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerCustomerRepository.Delete");
			new DataHandler().ExecuteCommand("[customerlek].[pCustomerDelete]", parameters, dbSettings);
		}
	}
}