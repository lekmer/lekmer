﻿using Litium.Lekmer.Customer.Cache;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;

namespace Litium.Lekmer.Customer.Service
{
	public class LekmerBlockSignInService : BlockSignInService
	{
		public LekmerBlockSignInService(BlockSignInRepository blockSignInRepository)
			: base(blockSignInRepository)
		{
		}

		public override IBlockSignIn GetById(IUserContext context, int id)
		{
			return BlockSignInCache.Instance.TryGetItem(
				new BlockSignInKey(context.Channel.Id, id),
				() => base.GetById(context, id));
		}
	}
}