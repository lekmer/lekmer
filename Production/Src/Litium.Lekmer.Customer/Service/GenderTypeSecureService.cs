﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Customer.Repository;

namespace Litium.Lekmer.Customer
{
	public class GenderTypeSecureService : IGenderTypeSecureService
	{
		protected GenderTypeRepository Repository { get; private set; }

		public GenderTypeSecureService(GenderTypeRepository customerStatusRepository)
		{
			Repository = customerStatusRepository;
		}

		public virtual Collection<IGenderType> GetAll()
		{
			return Repository.GetAll();
		}
	}
}