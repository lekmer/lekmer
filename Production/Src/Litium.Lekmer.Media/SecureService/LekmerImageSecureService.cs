﻿using System;
using Litium.Lekmer.Media.Cache;
using Litium.Lekmer.Media.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media
{
	public class LekmerImageSecureService : ImageSecureService, ILekmerImageSecureService
	{
		protected LekmerImageRepository LekmerImageRepository { get; private set; }
		protected MediaItemRepository MediaItemRepository { get; private set; }
		protected ImageRepository ImageRepository { get; private set; }
		/// <summary>
		/// Gets the <see cref="IChannelSecureService"/>.
		/// </summary>
		protected IChannelSecureService ChannelSecureService { get; private set; }

		public LekmerImageSecureService(IAccessValidator accessValidator, ImageRepository repository, IMediaItemSecureService mediaItemSecureService, IMediaFormatSecureService mediaFormatSecureService, IMediaItemSharedService mediaItemSharedService, IImageSharedService imageSharedService,
			MediaItemRepository mediaItemRepository, ImageRepository imageRepository, IChannelSecureService channelSecureService)
			: base(accessValidator, repository, mediaItemSecureService, mediaFormatSecureService, mediaItemSharedService, imageSharedService)
		{
			LekmerImageRepository = (LekmerImageRepository)repository;
			MediaItemRepository = mediaItemRepository;
			ImageRepository = imageRepository;
			ChannelSecureService = channelSecureService;
		}

		public void SaveMovie(ILekmerImage movie)
		{
			if (movie.Id <= 0)
			{
				MediaItemRepository.Insert(movie);
				ImageRepository.Insert(movie);
			}
			LekmerImageRepository.SaveMovie(movie);
		}

		/// <summary>
		/// Update image fields
		/// </summary>
		/// <param name="systemUserFull"></param>
		/// <param name="image"></param>
		/// <returns></returns>
		public override int Update(ISystemUserFull systemUserFull, IImage image)
		{
			int imageId = base.Update(systemUserFull, image);
			RemoveImagesFromCache(imageId);
			return imageId;
		}

		/// <summary>
		/// Deletes an image from the persistance layer.
		/// </summary>
		/// <param name="systemUserFull">An <see cref="ISystemUserFull"/> containing the security constraints to operate within.</param>
		/// <param name="id">Id of the Image to delete</param>
		/// <exception cref="ArgumentNullException">No user was specified for the security context.</exception>
		/// <exception cref="ArgumentOutOfRangeException">The requested identifier was less than or equal to 0.</exception>
		public override void Delete(ISystemUserFull systemUserFull, int id)
		{
			base.Delete(systemUserFull, id);
			RemoveImagesFromCache(id);
		}

		protected virtual void RemoveImagesFromCache(int imageId)
		{
			// The image could belong to few channels, so we need to remove image from cache for all channels.
			if (ChannelSecureService == null)
				throw new InvalidOperationException("ChannelSecureService must be set before calling RemoveImagesFromCache.");

			var channels = ChannelSecureService.GetAll();
			if (channels != null)
			{
				foreach (IChannel channel in channels)
				{
					LekmerImageCache.Instance.Remove(new LekmerImageKey(channel.Id, imageId));
				}
			}
		}
	}
}