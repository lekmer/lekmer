﻿using System;
using System.Collections.ObjectModel;
using Litium.Lekmer.Media.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;
using Litium.Scensum.Product.Cache;

namespace Litium.Lekmer.Media
{
	public class LekmerMediaItemSecureService : MediaItemSecureService, ILekmerMediaItemSecureService
	{
		protected LekmerMediaItemRepository LekmerMediaItemRepository { get; private set; }

		public LekmerMediaItemSecureService(
			IAccessValidator accessValidator,
			MediaItemRepository repository,
			IMediaFormatSecureService mediaFormatSecureService,
			IMediaFormatGroupSecureService mediaFormatGroupSecureService,
			IMediaItemSharedService mediaItemSharedService,
			IProductCacheCleaner productCacheCleaner)
			: base(accessValidator, repository, mediaFormatSecureService, mediaFormatGroupSecureService, mediaItemSharedService, productCacheCleaner)
		{
			LekmerMediaItemRepository = (LekmerMediaItemRepository) repository;
		}

		public virtual Collection<IMediaItem> GetAllBySizeTable(int sizeTableId)
		{
			if (sizeTableId <= 0)
			{
				throw new ArgumentOutOfRangeException("sizeTableId");
			}

			return LekmerMediaItemRepository.GetAllBySizeTableSecure(sizeTableId);
		}
	}
}