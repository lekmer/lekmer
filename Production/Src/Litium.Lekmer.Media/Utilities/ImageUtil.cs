﻿using System;
using System.IO;
using Litium.Framework.Image;
using SysImage = System.Drawing.Image;

namespace Litium.Lekmer.Media
{
	public class ImageUtil
	{
		/// <summary>
		/// Get image from stream.
		/// </summary>
		public static SysImage GetImage(Stream stream)
		{
			try
			{
				SysImage image = SysImage.FromStream(stream);
				stream.Seek(0L, SeekOrigin.Begin);
				return image;
			}
			catch (ArgumentException ex)
			{
				if ("Parameter is not valid.".Equals(ex.Message, StringComparison.OrdinalIgnoreCase))
				{
					throw new ImageFormatNotRecognizedException("Parameter format could not be recognized.", ex);
				}

				throw;
			}
		}

		/// <summary>
		///  Clones a stream into a <see cref="MemoryStream"/>.
		/// </summary>
		/// <param name="stream">The stream to clone.</param>
		/// <returns>A new <see ceref="MemoryStream"/> containing the stream data.</returns>
		/// <exception cref="ArgumentNullException">The supplied stream was null.</exception>
		/// <exception cref="ImageLoadException">The supplied image could not be loaded. (InnerException contains details).</exception>
		public static Stream CopyImageToMemoryStream(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}

			try
			{
				stream.Seek(0L, SeekOrigin.Begin);
				
				var memStream = new MemoryStream();
				var buffer = new byte[8192];
				int read;

				while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
				{
					memStream.Write(buffer, 0, read);
				}

				stream.Seek(0L, SeekOrigin.Begin);
				memStream.Seek(0L, SeekOrigin.Begin);

				return memStream;
			}
			catch (Exception ex)
			{
				throw new ImageLoadException("The image could not be copied to memory stream.", ex);
			}
		}
	}
}