﻿using System.Globalization;
using Litium.Framework.Cache;
using Litium.Scensum.Foundation.Cache;
using Litium.Scensum.Media;

namespace Litium.Lekmer.Media.Cache
{
	/// <summary>
	/// Handles caching of <see cref="IImage"/>.
	/// </summary>
	public sealed class LekmerImageCache : ScensumCacheBase<LekmerImageKey, IImage>
	{
		private static readonly LekmerImageCache _instance = new LekmerImageCache();

		private LekmerImageCache()
		{
		}

		/// <summary>
		/// Singleton instance.
		/// </summary>
		public static LekmerImageCache Instance
		{
			get { return _instance; }
		}
	}

	/// <summary>
	/// Key used for caching <see cref="ILekmerImage"/>.
	/// </summary>
	public class LekmerImageKey : ICacheKey
	{
		/// <summary>
		/// Creates a key.
		/// </summary>
		/// <param name="channelId">Channel id.</param>
		/// <param name="id">Id of the <see cref="ILekmerImage"/>.</param>
		public LekmerImageKey(int channelId, int id)
		{
			ChannelId = channelId;
			Id = id;
		}

		/// <summary>
		/// Id of the <see cref="ILekmerImage"/>.
		/// </summary>
		public int Id { get; set; }

		/// <summary>
		/// Channel id.
		/// </summary>
		public int ChannelId { get; set; }

		/// <summary>
		/// Key put together as a string.
		/// </summary>
		public string Key
		{
			get { return ChannelId.ToString(CultureInfo.InvariantCulture) + "-" + Id.ToString(CultureInfo.InvariantCulture); }
		}
	}
}