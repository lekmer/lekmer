﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Foundation;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Repository;

namespace Litium.Lekmer.Media.Repository
{
	public class LekmerMediaItemRepository : MediaItemRepository
	{
		/// <summary>
		///  Reads all entities for a specific size table.
		/// </summary>
		public virtual Collection<IMediaItem> GetAllBySizeTable(int channelId, int sizeTableId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int),
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerMediaItemRepository.GetAllBySizeTable");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pMediaItemGetAllBySizeTable]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		/// <summary>
		///  Reads all entities for a specific size table.
		/// </summary>
		public virtual Collection<IMediaItem> GetAllBySizeTableSecure(int sizeTableId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("SizeTableId", sizeTableId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerMediaItemRepository.GetAllBySizeTableSecure");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pMediaItemGetAllBySizeTableSecure]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}