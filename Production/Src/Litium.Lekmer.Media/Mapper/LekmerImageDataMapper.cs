﻿using System.Data;
using Litium.Scensum.Media;
using Litium.Scensum.Media.Mapper;

namespace Litium.Lekmer.Media.Mapper
{
	public class LekmerImageDataMapper : ImageDataMapper
	{
		public LekmerImageDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IImage Create()
		{
			var image = (ILekmerImage)base.Create();

			image.Link = MapNullableValue<string>("LekmerImage.Link");
			image.Parameter = MapNullableValue<string>("LekmerImage.Parameter");

			image.TumbnailImageUrl = MapNullableValue<string>("LekmerImage.TumbnailImageUrl");
			image.HasImage = MapNullableValue<bool>("LekmerImage.HasImage");

			image.SetUntouched();

			return image;
		}
	}
}
