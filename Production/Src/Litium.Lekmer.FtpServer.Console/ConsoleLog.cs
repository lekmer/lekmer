﻿using System;
using Litium.Framework.FtpServer;

namespace Litium.Lekmer.FtpServer.Console
{
	public class ConsoleLog : ILogOutput
	{
		public void Write(string output)
		{
			System.Console.WriteLine(output);
		}

		public void LogException(Exception exception)
		{
			System.Console.WriteLine(">>>>>>>>>>");
			System.Console.WriteLine(exception.ToString());
			System.Console.WriteLine(">>>>>>>>>>");
		}

		public void WriteSentResponse(FtpAuthentication authentication, string response)
		{
			System.Console.WriteLine("{0}\t{1}\t{2}", DateTime.Now, PadUserName(authentication.UserName), response);
		}

		public void WriteReceivedCommand(FtpAuthentication authentication, string command)
		{
			System.Console.WriteLine("{0}\t{1}\t{2}", DateTime.Now, PadUserName(authentication.UserName), command);
		}

		private static string PadUserName(string userName)
		{
			if (userName == null) throw new ArgumentNullException("userName");

			const int length = 30;
			if (userName.Length > length)
			{
				return userName;
			}

			return userName + new string(' ', length - userName.Length);
		}
	}
}