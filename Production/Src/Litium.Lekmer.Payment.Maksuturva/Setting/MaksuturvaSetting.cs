﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using Litium.Framework.Setting;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Maksuturva.Contract;
using log4net;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public class MaksuturvaSetting : SettingBase, IMaksuturvaSetting
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private string _groupName;

		protected override string StorageName
		{
			get { return "Maksuturva"; }
		}


		public string MaksuturvaTemporaryUrl
		{
			get { return GetString(_groupName, "MaksuturvaTemporaryUrl", null); }
		}
		public string MaksuturvaPaymentUrl
		{
			get { return GetString(_groupName, "MaksuturvaPaymentUrl", null); }
		}
		public string MaksuturvaStatusQueryUrl
		{
			get { return GetString(_groupName, "MaksuturvaStatusQueryUrl", null); }
		}
		public string MaksuturvaCompensationUrl
		{
			get { return GetString(_groupName, "MaksuturvaCompensationUrl", null); }
		}
		public string UserId
		{
			get { return GetString(_groupName, "UserId", null); }
		}
		public string SecretKey
		{
			get { return GetString(_groupName, "SecretKey", null); }
		}
		public string KeyGeneration
		{
			get { return GetString(_groupName, "KeyGeneration", null); }
		}
		public string OkReturnUrl
		{
			get { return GetString(_groupName, "OkReturnUrl", null); }
		}
		public string CancelReturnUrl
		{
			get { return GetString(_groupName, "CancelReturnUrl", null); }
		}
		public string ErrorReturnUrl
		{
			get { return GetString(_groupName, "ErrorReturnUrl", null); }
		}
		public string DelayedReturnUrl
		{
			get { return GetString(_groupName, "DelayedReturnUrl", null); }
		}
		public string Escrow
		{
			get { return GetString(_groupName, "Escrow", null); }
		}

		public string PendingOrderCheckInterval
		{
			get { return GetString(_groupName, "PendingOrderCheckInterval", "-1:1440"); }
		}
		public string RejectedOrderCheckInterval
		{
			get { return GetString(_groupName, "RejectedOrderCheckInterval", "-1:1440"); }
		}
		public string OnHoldOrderCheckInterval
		{
			get { return GetString(_groupName, "OnHoldOrderCheckInterval", "-1:1440"); }
		}
		public int DecisionGuaranteedInterval
		{
			get { return GetInt32(_groupName, "DecisionGuaranteedInterval", 30); }
		}
		public string DestinationDirectory
		{
			get { return GetString(_groupName, "DestinationDirectory", null); }
		}
		public string FileName
		{
			get { return GetString(_groupName, "FileName", null); }
		}
		public int DaysAfterOrder
		{
			get { return GetInt32(_groupName, "DaysAfterOrder", 3); }
		}
		public string FtpUrl
		{
			get { return GetString(_groupName, "FtpUrl", null); }
		}
		public string Username
		{
			get { return GetString(_groupName, "Username", null); }
		}
		public string Password
		{
			get { return GetString(_groupName, "Password", null); }
		}

		public virtual void Initialize(string channelCommonName)
		{
			_groupName = channelCommonName;
		}

		protected virtual Collection<CheckInterval> GetPendingOrderCheckIntervals()
		{
			var checkIntervals = new Collection<CheckInterval>();

			try
			{
				string[] intervals = PendingOrderCheckInterval.Split(';');
				foreach (var interval in intervals)
				{
					string[] periodInterval = interval.Split(':');
					checkIntervals.Add(new CheckInterval { Period = Convert.ToInt32(periodInterval[0]), Interval = Convert.ToInt32(periodInterval[1]) });
				}
			}
			catch (Exception ex)
			{
				checkIntervals.Add(new CheckInterval { Period = -1, Interval = 1440 });
				_log.Error(ex);
			}

			return checkIntervals;
		}
		protected virtual Collection<CheckInterval> GetRejectedOrderCheckIntervals()
		{
			var checkIntervals = new Collection<CheckInterval>();

			try
			{
				string[] intervals = RejectedOrderCheckInterval.Split(';');
				foreach (var interval in intervals)
				{
					string[] periodInterval = interval.Split(':');
					checkIntervals.Add(new CheckInterval { Period = Convert.ToInt32(periodInterval[0]), Interval = Convert.ToInt32(periodInterval[1]) });
				}
			}
			catch (Exception ex)
			{
				checkIntervals.Add(new CheckInterval { Period = -1, Interval = 1440 });
				_log.Error(ex);
			}

			return checkIntervals;
		}
		protected virtual Collection<CheckInterval> GetOnHoldOrderCheckIntervals()
		{
			var checkIntervals = new Collection<CheckInterval>();

			try
			{
				string[] intervals = OnHoldOrderCheckInterval.Split(';');
				foreach (var interval in intervals)
				{
					string[] periodInterval = interval.Split(':');
					checkIntervals.Add(new CheckInterval { Period = Convert.ToInt32(periodInterval[0]), Interval = Convert.ToInt32(periodInterval[1]) });
				}
			}
			catch (Exception ex)
			{
				checkIntervals.Add(new CheckInterval { Period = -1, Interval = 1440 });
				_log.Error(ex);
			}

			return checkIntervals;
		}

		public virtual DateTime GetOrderCheckDate(string orderStatus, DateTime? firstCheck, DateTime currentTime)
		{
			if (orderStatus == OrderStatusName.PaymentPending)
			{
				currentTime = GetOrderCheckDate(firstCheck, currentTime, GetPendingOrderCheckIntervals());
			}
			else if (orderStatus == OrderStatusName.RejectedByPaymentProvider)
			{
				currentTime = GetOrderCheckDate(firstCheck, currentTime, GetRejectedOrderCheckIntervals());
			}
			else if (orderStatus == OrderStatusName.PaymentOnHold)
			{
				currentTime = GetOrderCheckDate(firstCheck, currentTime, GetOnHoldOrderCheckIntervals());
			}

			return currentTime;
		}
		private DateTime GetOrderCheckDate(DateTime? firstCheck, DateTime currentTime, IEnumerable<CheckInterval> checkIntervals)
		{
			if (firstCheck == null)
			{
				currentTime = currentTime.AddMinutes(checkIntervals.First().Interval);
			}
			else
			{
				var tempTime = firstCheck.Value;
				foreach (var checkInterval in checkIntervals)
				{
					if (checkInterval.Period == -1)
					{
						currentTime = currentTime.AddMinutes(checkInterval.Interval);
						break;
					}

					tempTime = tempTime.AddMinutes(checkInterval.Period);
					if (tempTime < currentTime) continue;

					currentTime = currentTime.AddMinutes(checkInterval.Interval);
					break;
				}
			}
			return currentTime;
		}
	}

	public class CheckInterval
	{
		public int Period { get; set; }
		public int Interval { get; set; }
	}
}