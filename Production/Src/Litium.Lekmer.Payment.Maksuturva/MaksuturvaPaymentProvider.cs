﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Xml;
using Litium.Lekmer.Customer;
using Litium.Lekmer.Order;
using Litium.Lekmer.Payment.Maksuturva.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Product;
using log4net;

namespace Litium.Lekmer.Payment.Maksuturva
{
	public class MaksuturvaPaymentProvider : IMaksuturvaPaymentProvider
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private const string DefaultZero = "0,00";
		private const string DefaultVat = "25,00";
		private const string DefaultEmptyValue = "-";
		private const string HashAmpersand = "&";

		protected IChannelService ChannelService { get; private set; }
		protected ICountryService CountryService { get; private set; }
		protected IProductService ProductService { get; private set; }
		protected IMaksuturvaSetting Setting { get; private set; }

		private Dictionary<string, string> _parameters;

		public MaksuturvaPaymentProvider(IChannelService channelService, ICountryService countryService, IProductService productService, IMaksuturvaSetting maksuturvaSetting)
		{
			ChannelService = channelService;
			CountryService = countryService;
			ProductService = productService;
			Setting = maksuturvaSetting;

		}

		#region payment interface

		public string GetMaksuturvaTemporaryUrl(int channelId)
		{
			var channel = ChannelService.GetById(channelId);
			Setting.Initialize(channel.CommonName);
			return Setting.MaksuturvaTemporaryUrl;
		}
		public string GetMaksuturvaPaymentUrl(int channelId)
		{
			var channel = ChannelService.GetById(channelId);
			Setting.Initialize(channel.CommonName);
			return Setting.MaksuturvaPaymentUrl;
		}
		public Dictionary<string, string> GetMaksuturvaPaymentData(IUserContext context, IOrderFull order)
		{
			var hashBuilder = new StringBuilder();

			var orderId = order.Id.ToString(CultureInfo.InvariantCulture);
			var currentDate = DateToString(DateTime.Now);

			var lekmerOrder = (ILekmerOrderFull)order;
			var customer = order.Customer.CustomerInformation;
			var lekmerCustomer = (ILekmerCustomerInformation)customer;
			var maksuturvaCode = GetMaksuturvaCode(order.Payments);
			var channel = ChannelService.GetById(order.ChannelId);
			Setting.Initialize(channel.CommonName);

			_parameters = new Dictionary<string, string>();
			AddParameter("pmt_action", "NEW_PAYMENT_EXTENDED", true, hashBuilder);
			AddParameter("pmt_version", "0004", true, hashBuilder);
			AddParameter("pmt_sellerid", Setting.UserId, false, hashBuilder);
			AddParameter("pmt_id", orderId, true, hashBuilder);
			AddParameter("pmt_orderid", orderId, true, hashBuilder);
			AddParameter("pmt_reference", orderId + CalculateCheckDigit(order.Id), true, hashBuilder);
			AddParameter("pmt_duedate", currentDate, true, hashBuilder);
			AddParameter("pmt_userlocale", "fi_FI", false, hashBuilder);
			AddToHash("{pmt_amount}", true, hashBuilder); // pmt_amount
			AddParameter("pmt_currency", "EUR", true, hashBuilder);
			AddParameter("pmt_okreturn", Setting.OkReturnUrl, true, hashBuilder);
			AddParameter("pmt_errorreturn", Setting.ErrorReturnUrl, true, hashBuilder);
			AddParameter("pmt_cancelreturn", Setting.CancelReturnUrl, true, hashBuilder);
			AddParameter("pmt_delayedpayreturn", Setting.DelayedReturnUrl, true, hashBuilder);
			AddParameter("pmt_escrow", Setting.Escrow, true, hashBuilder);
			AddParameter("pmt_escrowchangeallowed", "N", true, hashBuilder);

			if (!string.IsNullOrEmpty(maksuturvaCode))
			{
				AddParameter("pmt_paymentmethod", maksuturvaCode, true, hashBuilder);
			}

			// Billing address
			var billingAddress = customer.DefaultBillingAddress;
			var countryCode = CountryService.GetById(billingAddress.CountryId).Iso;
			var customerName = customer.FirstName + " " + customer.LastName;
			AddParameter("pmt_buyername", customerName, true, 100, hashBuilder);
			AddParameter("pmt_buyeraddress", billingAddress.StreetAddress, true, 100, hashBuilder);
			AddParameter("pmt_buyerpostalcode", billingAddress.PostalCode, true, 20, hashBuilder);
			AddParameter("pmt_buyercity", billingAddress.City, true, 100, hashBuilder);
			AddParameter("pmt_buyercountry", countryCode, true, hashBuilder);
			AddParameter("pmt_buyerphone", customer.CellPhoneNumber, false, 40, hashBuilder);
			AddParameter("pmt_buyeremail", order.Email, false, 100, hashBuilder);

			// Delivery address
			var deliveryAddress = lekmerCustomer.AlternateAddressId != null ? lekmerCustomer.AlternateAddress : customer.DefaultDeliveryAddress;
			AddParameter("pmt_deliveryname", customerName, true, 100, hashBuilder);
			AddParameter("pmt_deliveryaddress", deliveryAddress.StreetAddress, true, 100, hashBuilder);
			AddParameter("pmt_deliverypostalcode", deliveryAddress.PostalCode, true, 20, hashBuilder);
			AddParameter("pmt_deliverycity", deliveryAddress.City, true, 100, hashBuilder);
			AddParameter("pmt_deliverycountry", countryCode, true, hashBuilder);

			// Items
			var orderItems = order.GetOrderItems();

			var freightCost = lekmerOrder.GetTotalFreightCost();
			var paymentCost = lekmerOrder.PaymentCost;

			var discountAmount = GetOrderTotalDiscountIncludingVat(lekmerOrder);

			AddParameter("pmt_sellercosts", freightCost + paymentCost, true, hashBuilder);

			int rowIndex = 1;
			var sum = 0m;
			foreach (var orderItem in orderItems)
			{
				var product = (ILekmerProductView)ProductService.GetViewById(context, orderItem.ProductId);
				var price = orderItem.ActualPrice;

				AddParameter("pmt_row_name" + rowIndex, product.DisplayTitle, true, 40, hashBuilder);
				AddParameter("pmt_row_desc" + rowIndex, product.Description ?? DefaultEmptyValue, true, 1000, hashBuilder);
				AddParameter("pmt_row_quantity" + rowIndex, orderItem.Quantity, true, hashBuilder);
				AddParameter("pmt_row_articlenr" + rowIndex, product.ErpId, true, hashBuilder);
				AddParameter("pmt_row_deliverydate" + rowIndex, currentDate, true, hashBuilder);
				AddParameter("pmt_row_price_gross" + rowIndex, price.IncludingVat, true, hashBuilder);
				var rowVat = price.ExcludingVat == 0 ? 0m : CalculateVatPercentage(price);
				AddParameter("pmt_row_vat" + rowIndex, rowVat, true, hashBuilder);
				AddParameter("pmt_row_discountpercentage" + rowIndex, DefaultZero, true, hashBuilder);
				AddParameter("pmt_row_type" + rowIndex, (int)MaksuturvaOrderRowType.Product, true, hashBuilder);

				sum = sum + orderItem.Quantity * price.IncludingVat;

				rowIndex++;
			}

			// Delivery
			AddParameter("pmt_row_name" + rowIndex, "postal cost", true, hashBuilder);
			AddParameter("pmt_row_desc" + rowIndex, DefaultEmptyValue, true, hashBuilder);
			AddParameter("pmt_row_quantity" + rowIndex, "1", true, hashBuilder);
			AddParameter("pmt_row_deliverydate" + rowIndex, currentDate, true, hashBuilder);
			AddParameter("pmt_row_price_gross" + rowIndex, freightCost, true, hashBuilder);
			AddParameter("pmt_row_vat" + rowIndex, DefaultVat, true, hashBuilder);
			AddParameter("pmt_row_discountpercentage" + rowIndex, DefaultZero, true, hashBuilder);
			AddParameter("pmt_row_type" + rowIndex, (int)MaksuturvaOrderRowType.PostalCost, true, hashBuilder);

			// Payment
			if (paymentCost > 0)
			{
				rowIndex++;
				AddParameter("pmt_row_name" + rowIndex, "payment cost", true, hashBuilder);
				AddParameter("pmt_row_desc" + rowIndex, DefaultEmptyValue, true, hashBuilder);
				AddParameter("pmt_row_quantity" + rowIndex, "1", true, hashBuilder);
				AddParameter("pmt_row_deliverydate" + rowIndex, currentDate, true, hashBuilder);
				AddParameter("pmt_row_price_gross" + rowIndex, paymentCost, true, hashBuilder);
				AddParameter("pmt_row_vat" + rowIndex, DefaultVat, true, hashBuilder);
				AddParameter("pmt_row_discountpercentage" + rowIndex, DefaultZero, true, hashBuilder);
				AddParameter("pmt_row_type" + rowIndex, (int)MaksuturvaOrderRowType.HandlingCost, true, hashBuilder);
			}

			// Discount
			if (discountAmount > 0)
			{
				rowIndex++;
				AddParameter("pmt_row_name" + rowIndex, "discount money amount", true, hashBuilder);
				AddParameter("pmt_row_desc" + rowIndex, DefaultEmptyValue, true, hashBuilder);
				AddParameter("pmt_row_quantity" + rowIndex, "1", true, hashBuilder);
				AddParameter("pmt_row_deliverydate" + rowIndex, currentDate, true, hashBuilder);
				AddParameter("pmt_row_price_gross" + rowIndex, -discountAmount, true, hashBuilder);
				AddParameter("pmt_row_vat" + rowIndex, DefaultZero, true, hashBuilder);
				AddParameter("pmt_row_discountpercentage" + rowIndex, DefaultZero, true, hashBuilder);
				AddParameter("pmt_row_type" + rowIndex, (int)MaksuturvaOrderRowType.DiscountMoneyAmount, true, hashBuilder);
			}

			sum = sum - discountAmount;
			decimal actualPriceSummary = lekmerOrder.GetActualPriceSummary().IncludingVat;
			if (sum != actualPriceSummary)
			{
				_log.ErrorFormat("Order actual price summary calculation failed: {0} != {1}", sum, actualPriceSummary);
			}

			AddParameter("pmt_amount", sum, false, hashBuilder);
			hashBuilder.Replace("{pmt_amount}", ConvertToCorrectFormat(sum));

			AddParameter("pmt_rows", rowIndex, false, hashBuilder);
			AddParameter("pmt_charset", "UTF-8", false, hashBuilder);
			AddParameter("pmt_charsethttp", "UTF-8", false, hashBuilder);
			AddParameter("pmt_hashversion", "SHA-1", false, hashBuilder);
			AddParameter("pmt_keygeneration", Setting.KeyGeneration, false, hashBuilder);

			hashBuilder.Append(Setting.SecretKey);
			hashBuilder.Append(HashAmpersand);

			string hash = hashBuilder.ToString();
			hash = CalculateSha1(hash, Encoding.GetEncoding("UTF-8"));
			AddParameter("pmt_hash", hash, false, hashBuilder);

			return _parameters;
		}

		private string GetMaksuturvaCode(Collection<IOrderPayment> payments)
		{
			var maksuturvaCode = string.Empty;
			if (payments != null && payments.Count > 0)
			{
				var orderPayment = (ILekmerOrderPayment)payments[0];
				if (!string.IsNullOrEmpty(orderPayment.MaksuturvaCode))
				{
					maksuturvaCode = orderPayment.MaksuturvaCode;
				}
			}
			return maksuturvaCode;
		}
		private int CalculateCheckDigit(int number)
		{
			var referenceNumber = new List<int>();
			while (number > 0)
			{
				referenceNumber.Add(number % 10);
				number = number / 10;
			}
			referenceNumber.Reverse();

			var weightsNumber = new List<int>();
			for (int i = 1; i <= referenceNumber.Count; i++)
			{
				switch (i % 3)
				{
					case 1:
						weightsNumber.Add(7);
						break;
					case 2:
						weightsNumber.Add(3);
						break;
					case 0:
						weightsNumber.Add(1);
						break;
				}
			}
			weightsNumber.Reverse();

			var sum = 0;
			for (int i = 0; i < referenceNumber.Count; i++)
			{
				sum = sum + referenceNumber[i] * weightsNumber[i];
			}

			int numberEndInZero = (10 - sum % 10) + sum;

			int difference = numberEndInZero - sum;

			int checkDigit = difference;
			if (difference == 10)
			{
				checkDigit = 0;
			}
			return checkDigit;
		}
		private string ConvertToCorrectFormat(decimal number)
		{
			return number.ToString("0.00", CultureInfo.InvariantCulture).Replace(".", ",");
		}
		private decimal CalculateVatPercentage(Price price)
		{
			return (price.IncludingVat - price.ExcludingVat) / price.ExcludingVat * 100;
		}

		private decimal GetOrderTotalDiscountIncludingVat(ILekmerOrderFull order)
		{
			Price basePriceSummary = order.GetBaseActualPriceSummary(); // SUM(item.ActualPrice * item.Quantity)
			Price actualPriceSummary = order.GetActualPriceSummary();   // SUM(item.ActualPrice * item.Quantity) - cart.Discount - voucher.Discount

			Price totalDiscount = basePriceSummary - actualPriceSummary; // cart.Discount + voucher.Discount

			return totalDiscount.IncludingVat;
		}

		#endregion

		#region status query interface

		public MaksuturvaResponseCode GetMaksuturvaPaymentStatus(string channelCommonName, int paymentId)
		{
			var hashBuilder = new StringBuilder();
			try
			{
				Setting.Initialize(channelCommonName);

				// Prepare request data.
				var parameters = PrepareParameters(Setting, paymentId, hashBuilder);
				byte[] data = BuildRequestData(parameters);

				ServicePointManager.CertificatePolicy = new CertificatesPolicy();

				// Open the request url for the Web Payment Frontend
				var request = GetRequest(Setting.MaksuturvaStatusQueryUrl);

				// Send the request data
				Stream input = request.GetRequestStream();
				input.Write(data, 0, data.Length);
				input.Close();

				// Get the answer from the frontend server
				string response = GetResponse(request, false);
				if (string.IsNullOrEmpty(response))
				{
					return MaksuturvaResponseCode.Unknown;
				}

				var node = GetResponseXml(response, "pmtq");
				if (node == null)
				{
					return MaksuturvaResponseCode.Unknown;
				}

				if (!ValidatePaymentStatusHash(Setting, node))
				{
					return MaksuturvaResponseCode.ValidationFailed;
				}

				return GetResponseCode(node);
			}
			catch (Exception exception)
			{
				string logMessage = string.Format("Maksuturva returned exception on check payment status. Reservation number: {0}", paymentId);
				_log.Error(logMessage, exception);

				return MaksuturvaResponseCode.Unknown;
			}
		}

		private IDictionary PrepareParameters(IMaksuturvaSetting setting, int paymentId, StringBuilder hashBuilder)
		{
			IDictionary parameters = new Hashtable();

			AddParameter("pmtq_action", "PAYMENT_STATUS_QUERY", true, parameters, hashBuilder);
			AddParameter("pmtq_version", "0004", true, parameters, hashBuilder);
			AddParameter("pmtq_sellerid", setting.UserId, true, parameters, hashBuilder);
			AddParameter("pmtq_id", paymentId.ToString(CultureInfo.InvariantCulture), true, parameters, hashBuilder);
			AddParameter("pmtq_resptype", "XML", false, parameters, hashBuilder);
			AddParameter("pmtq_hashversion", "SHA-1", false, parameters, hashBuilder);

			hashBuilder.Append(setting.SecretKey);
			hashBuilder.Append(HashAmpersand);
			var hash = CalculateSha1(hashBuilder.ToString(), Encoding.GetEncoding("UTF-8"));
			AddParameter("pmt_hash", hash, false, parameters, hashBuilder);

			AddParameter("pmtq_hash", hash, false, parameters, hashBuilder);
			AddParameter("pmtq_keygeneration", setting.KeyGeneration, false, parameters, hashBuilder);

			return parameters;
		}
		private XmlNode GetResponseXml(string response, string nodeName)
		{
			var doc = new XmlDocument();
			doc.LoadXml(response);
			XmlNode node = doc.SelectSingleNode(nodeName);
			return node;
		}
		private bool ValidatePaymentStatusHash(IMaksuturvaSetting setting, XmlNode response)
		{
			var hashBuilder = new StringBuilder();
			AddToHash(response["pmtq_action"], true, hashBuilder);
			AddToHash(response["pmtq_version"], true, hashBuilder);
			AddToHash(response["pmtq_sellerid"], true, hashBuilder);
			AddToHash(response["pmtq_id"], true, hashBuilder);
			AddToHash(response["pmtq_amount"], true, hashBuilder);
			AddToHash(response["pmtq_returncode"], true, hashBuilder);
			AddToHash(response["pmtq_returntext"], true, hashBuilder);
			AddToHash(response["pmtq_sellercosts"], true, hashBuilder);
			AddToHash(response["pmtq_paymentmethod"], true, hashBuilder);
			AddToHash(response["pmtq_escrow"], true, hashBuilder);
			AddToHash(response["pmtq_certification"], true, hashBuilder);
			AddToHash(response["pmtq_paymentdate"], true, hashBuilder);
			AddToHash(setting.SecretKey, true, hashBuilder);

			var hash = CalculateSha1(hashBuilder.ToString(), Encoding.GetEncoding("UTF-8"));
			if (response["pmtq_hash"] != null)
			{
				return hash == response["pmtq_hash"].InnerText;
			}
			return false;
		}
		private MaksuturvaResponseCode GetResponseCode(XmlNode response)
		{
			int responseCode = 100;
			if (response["pmtq_returncode"] == null)
			{
				return MaksuturvaResponseCode.Unknown;
			}

			int.TryParse(response["pmtq_returncode"].InnerText, out responseCode);
			if (responseCode == 00)
			{
				if (response["pmtq_amount"] != null && response["pmtq_amount"].InnerText == "0,00")
				{
					return MaksuturvaResponseCode.NotPaid;
				}

				if (response["pmtq_paymentmethod"] != null && !string.IsNullOrEmpty(response["pmtq_paymentmethod"].InnerText))
				{
					return MaksuturvaResponseCode.NotPaidYet;
				}

				return MaksuturvaResponseCode.Unknown;
			}

			return (MaksuturvaResponseCode) responseCode;
		}

		#endregion

		#region get compensation by time interval interface

		public string GetCompensationByTimeInterval(string channelCommonName, DateTime fetchDate)
		{
			var hashBuilder = new StringBuilder();
			try
			{
				Setting.Initialize(channelCommonName);

				// Prepare request data.
				var parameters = PrepareParameters(Setting, DateToString(fetchDate), hashBuilder);
				byte[] data = BuildRequestData(parameters);

				ServicePointManager.CertificatePolicy = new CertificatesPolicy();

				// Open the request url for the Web Payment Frontend
				var request = GetRequest(Setting.MaksuturvaCompensationUrl);

				// Send the request data
				Stream input = request.GetRequestStream();
				input.Write(data, 0, data.Length);
				input.Close();

				// Get the answer from the frontend server
				return GetResponse(request, true);
			}
			catch (Exception exception)
			{
				string logMessage = string.Format("Maksuturva returned exception on getting compensation by time interval for date {0}", fetchDate);
				_log.Error(logMessage, exception);
				return null;
			}
		}
		private IDictionary PrepareParameters(IMaksuturvaSetting setting, string fetchDate, StringBuilder hashBuilder)
		{
			IDictionary parameters = new Hashtable();

			AddParameter("gc_action", "GET_SETTLEMENTS_DIBS", true, parameters, hashBuilder);
			AddParameter("gc_version", "0001", true, parameters, hashBuilder);
			AddParameter("gc_sellerid", setting.UserId, true, parameters, hashBuilder);
			AddParameter("gc_begindate", fetchDate, true, parameters, hashBuilder);
			AddParameter("gc_enddate", fetchDate, true, parameters, hashBuilder);
			AddParameter("gc_hashversion", "SHA-1", true, parameters, hashBuilder);
			AddParameter("gc_keygeneration", setting.KeyGeneration, true, parameters, hashBuilder);

			hashBuilder.Append(setting.SecretKey);
			hashBuilder.Append(HashAmpersand);
			var hash = CalculateSha1(hashBuilder.ToString(), Encoding.GetEncoding("UTF-8"));
			AddParameter("gc_hash", hash, false, parameters, hashBuilder);

			return parameters;
		}

		#endregion


		private string DateToString(DateTime date)
		{
			return date.ToString("dd.MM.yyyy");
		}
		private string CalculateSha1(string text, Encoding enc)
		{
			var buffer = enc.GetBytes(text);
			var cryptoServiceProvider = new SHA1CryptoServiceProvider();
			var hash = BitConverter.ToString(cryptoServiceProvider.ComputeHash(buffer)).Replace("-", "");
			return hash;
		}

		private void AddParameter(string name, int value, bool addToHash, StringBuilder hashBuilder)
		{
			AddParameter(name, value.ToString(CultureInfo.InvariantCulture), addToHash, hashBuilder);
		}
		private void AddParameter(string name, decimal value, bool addToHash, StringBuilder hashBuilder)
		{
			AddParameter(name, ConvertToCorrectFormat(value), addToHash, hashBuilder);
		}
		private void AddParameter(string name, string value, bool addToHash, int maxLenght, StringBuilder hashBuilder)
		{
			if (value.Length > maxLenght)
			{
				value = value.Substring(0, maxLenght);
			}

			AddParameter(name, value, addToHash, hashBuilder);
		}
		private void AddParameter(string name, string value, bool addToHash, StringBuilder hashBuilder)
		{
			_parameters.Add(name, value);
			AddToHash(value, addToHash, hashBuilder);
		}
		private void AddParameter(string name, string value, bool addToHash, IDictionary parameters, StringBuilder hashBuilder)
		{
			parameters[name] = value;
			AddToHash(value, addToHash, hashBuilder);
		}
		private void AddToHash(XmlNode value, bool addToHash, StringBuilder hashBuilder)
		{
			if (value != null)
			{
				AddToHash(value.InnerText, addToHash, hashBuilder);
			}
		}
		private void AddToHash(string value, bool addToHash, StringBuilder hashBuilder)
		{
			if (addToHash && !string.IsNullOrEmpty(value))
			{
				hashBuilder.Append(value);
				hashBuilder.Append(HashAmpersand);
			}
		}

		private byte[] BuildRequestData(IDictionary parameters)
		{
			var stringBuilder = new StringBuilder();
			foreach (DictionaryEntry entry in parameters)
			{
				stringBuilder.AppendFormat(CultureInfo.CurrentCulture, "{0}={1}&", entry.Key, entry.Value);
			}

			string requestData = stringBuilder.ToString();
			requestData = requestData.Substring(0, requestData.Length - 1);

			return Encoding.UTF8.GetBytes(requestData);
		}
		private HttpWebRequest GetRequest(string url)
		{
			var request = (HttpWebRequest)WebRequest.Create(url);
			request.Method = "POST";
			request.ContentType = "application/x-www-form-urlencoded";
			return request;
		}
		private string GetResponse(HttpWebRequest request, bool isResponseXml)
		{
			string responseProperties = string.Empty;

			var response = (HttpWebResponse)request.GetResponse();
			var reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
			string initialResponse = reader.ReadToEnd();
			reader.Close();
			response.Close();

			if (isResponseXml)
			{
				return initialResponse;
			}

			string[] initialResponseParts = initialResponse.Split('&');
			if (initialResponseParts.Length > 0)
			{
				responseProperties = initialResponseParts[0];
			}

			return responseProperties;
		}
	}

	class CertificatesPolicy : ICertificatePolicy
	{
		public bool CheckValidationResult(ServicePoint sp, X509Certificate cert, WebRequest request, int problem)
		{
			return true;
		}
	}
}