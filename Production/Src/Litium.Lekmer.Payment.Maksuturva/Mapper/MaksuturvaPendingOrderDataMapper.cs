using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Maksuturva.Mapper
{
	public class MaksuturvaPendingOrderDataMapper : DataMapperBase<IMaksuturvaPendingOrder>
	{
		public MaksuturvaPendingOrderDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override IMaksuturvaPendingOrder Create()
		{
			var maksuturvaPendingOrder = IoC.Resolve<IMaksuturvaPendingOrder>();

			maksuturvaPendingOrder.Id = MapValue<int>("MaksuturvaPendingOrder.MaksuturvaPendingOrderId");
			maksuturvaPendingOrder.ChannelId = MapValue<int>("MaksuturvaPendingOrder.ChannelId");
			maksuturvaPendingOrder.OrderId = MapValue<int>("MaksuturvaPendingOrder.OrderId");
			maksuturvaPendingOrder.StatusId = MapValue<int>("MaksuturvaPendingOrder.StatusId");
			maksuturvaPendingOrder.FirstAttempt = MapNullableValue<DateTime?>("MaksuturvaPendingOrder.FirstAttempt");
			maksuturvaPendingOrder.LastAttempt = MapNullableValue<DateTime?>("MaksuturvaPendingOrder.LastAttempt");
			maksuturvaPendingOrder.NextAttempt = MapNullableValue<DateTime?>("MaksuturvaPendingOrder.NextAttempt");

			return maksuturvaPendingOrder;
		}
	}
}