﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingFolderSecureService
	{
		IRatingFolder Create();

		IRatingFolder Save(ISystemUserFull systemUserFull, IRatingFolder ratingFolder);

		bool TryDelete(ISystemUserFull systemUserFull, int ratingFolderId);

		IRatingFolder GetById(int ratingFolderId);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		Collection<IRatingFolder> GetAll();

		Collection<IRatingFolder> GetAllByParent(int parentRatingFolderId);

		Collection<INode> GetTree(int? selectedId);
	}
}