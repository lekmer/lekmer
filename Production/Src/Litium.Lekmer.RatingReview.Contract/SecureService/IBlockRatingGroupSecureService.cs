﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRatingGroupSecureService
	{
		void SaveAll(ISystemUserFull systemUserFull, int blockId, Collection<IBlockRatingGroup> blockRatingGroups);

		void Delete(ISystemUserFull systemUserFull, int blockId);

		Collection<IBlockRatingGroup> GetAllByBlock(int blockId);
	}
}