﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupProductSecureService
	{
		void Save(ISystemUserFull systemUserFull, IRatingGroupProduct ratingGroupProduct);

		void SaveAll(ISystemUserFull systemUserFull, int productId, Collection<IRatingGroupProduct> ratingGroupProducts);

		void Delete(ISystemUserFull systemUserFull, int ratingGroupId, int productId);

		Collection<IRatingGroupProduct> GetAllByGroup(int ratingGroupId);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		Collection<IRatingGroupProduct> GetAllByProduct(int productId);
	}
}