﻿using System.Collections.Generic;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewFeedbackSecureService
	{
		bool Save(ISystemUserFull systemUserFull, int id, string title, string message, int statusId, bool isInappropriate);

		void SetStatus(ISystemUserFull systemUserFull, IEnumerable<int> ratingReviewFeedbackIds, int statusId);

		void Delete(ISystemUserFull systemUserFull, IEnumerable<int> ratingReviewFeedbackIds);

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Flag")]
		void RemoveInappropriateFlag(ISystemUserFull systemUserFull, IEnumerable<int> ratingReviewFeedbackIds);

		RatingReviewFeedbackRecordCollection Search(IReviewSearchCriteria searchCriteria, int page, int pageSize);

		IRatingReviewFeedback GetById(int ratingReviewFeedbackId);

		IRatingReviewFeedbackRecord GetRecordById(int ratingReviewFeedbackId);
	}
}