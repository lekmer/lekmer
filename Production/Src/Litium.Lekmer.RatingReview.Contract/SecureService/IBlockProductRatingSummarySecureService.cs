using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductRatingSummarySecureService
	{
		IBlockProductRatingSummary Create();

		IBlockProductRatingSummary GetById(int id);

		int Save(ISystemUserFull systemUserFull, IBlockProductRatingSummary block);
	}
}