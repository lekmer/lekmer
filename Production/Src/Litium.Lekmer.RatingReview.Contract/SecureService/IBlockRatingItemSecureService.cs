﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockRatingItemSecureService
	{
		void SaveAll(ISystemUserFull systemUserFull, int blockId, Collection<IBlockRatingItem> blockRatingItems);

		void Delete(ISystemUserFull systemUserFull, int blockId);

		Collection<IBlockRatingItem> GetAllByBlock(int blockId);
	}
}