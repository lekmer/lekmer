﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupRatingSecureService
	{
		void Save(ISystemUserFull systemUserFull, IRatingGroupRating ratingGroupRating);

		void Delete(ISystemUserFull systemUserFull, int ratingGroupId, int ratingId);

		Collection<IRatingGroupRating> GetAllByGroup(int ratingGroupId);

		Collection<IRatingGroupRating> GetAllByRating(int ratingId);
	}
}