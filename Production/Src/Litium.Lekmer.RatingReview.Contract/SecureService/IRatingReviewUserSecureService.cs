﻿namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewUserSecureService
	{
		IRatingReviewUser GetById(int ratingReviewUserId);
	}
}