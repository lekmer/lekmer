using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockBestRatedProductListSecureService
	{
		IBlockBestRatedProductList Create();

		IBlockBestRatedProductList GetById(int blockId);

		int Save(ISystemUserFull systemUserFull, IBlockBestRatedProductList block);
	}
}