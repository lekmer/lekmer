﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewFeedbackRecord : IRatingReviewFeedback
	{
		string ProductTitle { get; set; }
		string ProductErpId { get; set; }
		string UserName { get; set; }
		IReview Review { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<IRatingItemProductVote> RatingItemProductVotes { get; set; }
	}
}