﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IProductRatingSummaryInfo
	{
		int ChannelId { get; set; }
		int ProductId { get; set; }
		int RatingId { get; set; }
		int TotalNumberOfVotes { get; set; }
		int AverageRatingItemId { get; set; }
		decimal AverageRatingScore { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<IRatingItemSummary> RatingItemSummaryCollection { get; set; }
	}
}