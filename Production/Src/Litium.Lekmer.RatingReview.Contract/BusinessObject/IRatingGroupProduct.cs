﻿using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupProduct : IBusinessObjectBase
	{
		int RatingGroupId { get; set; }
		int ProductId { get; set; }
	}
}