﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingItem : IBusinessObjectBase
	{
		int Id { get; set; }
		int RatingId { get; set; }
		string Title { get; set; }
		string Description { get; set; }
		int Score { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<ITranslationGeneric> TitleTranslations { get; set; }
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		Collection<ITranslationGeneric> DescriptionTranslations { get; set; }
	}
}