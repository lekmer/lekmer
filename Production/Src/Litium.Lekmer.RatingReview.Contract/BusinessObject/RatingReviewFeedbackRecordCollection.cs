﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public class RatingReviewFeedbackRecordCollection : Collection<IRatingReviewFeedbackRecord>
	{
		public RatingReviewFeedbackRecordCollection()
		{
		}

		public RatingReviewFeedbackRecordCollection(IList<IRatingReviewFeedbackRecord> list) 
			: base(list)
		{
		}

		public RatingReviewFeedbackRecordCollection(IEnumerable<IRatingReviewFeedbackRecord> list)
			: base(new List<IRatingReviewFeedbackRecord>(list))
		{
		}

		/// <summary>
		/// The total number of items of a query.
		/// Used with paging.
		/// </summary>
		public int TotalCount { get; set; }
	}
}