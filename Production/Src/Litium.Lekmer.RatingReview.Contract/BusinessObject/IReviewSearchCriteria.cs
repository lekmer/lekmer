namespace Litium.Lekmer.RatingReview
{
	public interface IReviewSearchCriteria
	{
		string Author { get; set; }
		string Message { get; set; }
		string StatusId { get; set; }
		string CreatedFrom { get; set; }
		string CreatedTo { get; set; }
		string ProductId { get; set; }
		string ProductTitle { get; set; }
		string OrderId { get; set; }
		string ChannelId { get; set; }
		string RatingId { get; set; }
		string InappropriateContent { get; set; }
		string SortBy { get; set; }
		string SortByDescending { get; set; }
	}
}