using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductFeedbackService
	{
		IBlockProductFeedback GetById(IUserContext context, int id);
	}
}