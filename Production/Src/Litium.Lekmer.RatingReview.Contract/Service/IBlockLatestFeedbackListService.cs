using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockLatestFeedbackListService
	{
		IBlockLatestFeedbackList GetById(IUserContext context, int blockId);
	}
}