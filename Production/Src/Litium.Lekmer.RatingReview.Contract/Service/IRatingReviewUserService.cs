﻿using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingReviewUserService
	{
		IRatingReviewUser Create(IUserContext context);

		IRatingReviewUser Save(IRatingReviewUser ratingReviewUser);

		IRatingReviewUser GetById(int ratingReviewUserId);

		IRatingReviewUser GetByCustomer(int customerId);

		IRatingReviewUser GetByToken(string token);
	}
}