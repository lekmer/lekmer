﻿using System.Collections.ObjectModel;

namespace Litium.Lekmer.RatingReview
{
	public interface IRatingGroupProductService
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1702:CompoundWordsShouldBeCasedCorrectly", MessageId = "ByProduct")]
		Collection<IRatingGroupProduct> GetAllByProduct(int productId);
	}
}