﻿using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockLatestFeedbackListBrandService
	{
		Collection<IBlockLatestFeedbackListBrand> GetAllByBlock(IUserContext context, int blockId);
	}
}