using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public interface IBlockProductMostHelpfulRatingService
	{
		IBlockProductMostHelpfulRating GetById(IUserContext context, int id);
	}
}