﻿using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public interface ILekmerImageSecureService : IImageSecureService
	{
		void SaveMovie(ILekmerImage movie);
	}
}
