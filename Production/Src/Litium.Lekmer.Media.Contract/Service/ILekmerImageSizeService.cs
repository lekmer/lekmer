﻿using Litium.Scensum.Media;

namespace Litium.Lekmer.Media
{
	public interface ILekmerImageSizeService : IImageSizeService
	{
		/// <summary>
		/// Retrieves an <see cref="T:Litium.Scensum.Media.IImageSize"/> by its common name.
		/// </summary>
		/// <param name="commonName">The common name of the <see cref="T:Litium.Scensum.Media.IImageSize"/> to be retrieved.</param>
		/// <param name="mobileVersion">When need mobile version of image, send true.</param>
		/// <returns>
		/// The <see cref="T:Litium.Scensum.Media.IImageSize"/> item that was requested, or null if none was found.
		/// </returns>
		/// <exception cref="T:System.ArgumentException">The supplied <paramref name="commonName"/> was null or empty.</exception>
		IImageSize GetByCommonName(string commonName, bool mobileVersion);
	}
}
