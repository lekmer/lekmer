﻿using Litium.Lekmer.Order;
using Litium.Scensum.Core;

namespace Litium.Lekmer.RatingReview
{
	public class GiftCardViaEmailMessageArgs
	{
		public IChannel Channel { get; set; }
		public ILekmerOrderFull Order { get; set; }
		public int CampaignId { get; set; }
		public int? TemplateId { get; set; }
		public decimal DiscountValue { get; set; }
		public string VoucherCode { get; set; }

		public GiftCardViaEmailMessageArgs(
			IChannel channel,
			ILekmerOrderFull order,
			int campaignId,
			int? templateId,
			decimal discountValue,
			string voucherCode)
		{
			Channel = channel;
			Order = order;
			CampaignId = campaignId;
			TemplateId = templateId;
			DiscountValue = discountValue;
			VoucherCode = voucherCode;
		}
	}
}