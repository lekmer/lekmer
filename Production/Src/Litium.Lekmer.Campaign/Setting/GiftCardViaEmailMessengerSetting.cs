﻿using Litium.Framework.Setting;

namespace Litium.Lekmer.RatingReview
{
	public class GiftCardViaEmailMessengerSetting : SettingBase
	{
		private const string _storageName = "GiftCardViaEmailMessagingService";

		protected override string StorageName
		{
			get { return _storageName; }
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public static string GetStorageName()
		{
			return _storageName;
		}

		private const string GROUP_NAME = "GiftCardViaEmailMessenger";

		public int MaxVoucherQuantity()
		{
			return GetInt32(GROUP_NAME, "MaxVoucherQuantity", 0);
		}

		public int ValidInDays()
		{
			return GetInt32(GROUP_NAME, "ValidInDays", 365);
		}

		public int NumberOfTokens()
		{
			return GetInt32(GROUP_NAME, "NbrOfTokens", 8);
		}
	}
}