﻿using System.Data;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class LekmerPercentagePriceDiscountActionDataMapper : ProductActionDataMapper<IPercentagePriceDiscountAction>
	{
		public LekmerPercentagePriceDiscountActionDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IPercentagePriceDiscountAction Create()
		{
			var percentagePriceDiscountAction = (ILekmerPercentagePriceDiscountAction) base.Create();
			percentagePriceDiscountAction.DiscountAmount = MapValue<decimal>("PercentagePriceDiscountAction.DiscountAmount");
			percentagePriceDiscountAction.CampaignConfigId = MapValue<int>("PercentagePriceDiscountAction.ConfigId");
			return percentagePriceDiscountAction;
		}
	}
}