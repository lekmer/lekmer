﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CartItemDiscountCartActionDataMapper : CartActionDataMapper<ICartItemDiscountCartAction>
	{
		public CartItemDiscountCartActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICartItemDiscountCartAction Create()
		{
			var cartItemDiscountCartAction = base.Create();

			cartItemDiscountCartAction.Quantity = MapValue<int>("CartItemDiscountCartAction.Quantity");
			cartItemDiscountCartAction.ApplyForCheapest = MapValue<bool>("CartItemDiscountCartAction.ApplyForCheapest");
			cartItemDiscountCartAction.IsPercentageDiscount = MapValue<bool>("CartItemDiscountCartAction.IsPercentageDiscount");
			cartItemDiscountCartAction.PercentageDiscountAmount = MapNullableValue<decimal?>("CartItemDiscountCartAction.PercentageDiscountAmount");
			cartItemDiscountCartAction.IncludeAllProducts = MapNullableValue<bool?>("CartItemDiscountCartAction.IncludeAllProducts") ?? true;

			return cartItemDiscountCartAction;
		}
	}
}