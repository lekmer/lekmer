﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CartItemFixedDiscountActionDataMapper : CartActionDataMapper<ICartItemFixedDiscountAction>
	{
		public CartItemFixedDiscountActionDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICartItemFixedDiscountAction Create()
		{
			var action = base.Create();
			action.IncludeAllProducts = MapValue<bool>("CartItemFixedDiscountAction.IncludeAllProducts");
			return action;
		}
	}
}