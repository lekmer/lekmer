﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class ProductDiscountActionDataMapper : ProductActionDataMapper<IProductDiscountAction>
	{
		public ProductDiscountActionDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override IProductDiscountAction Create()
		{
			var action = base.Create();
			return action;
		}
	}
}