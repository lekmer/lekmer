﻿using System.Data;
using Litium.Scensum.Campaign.Mapper;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CartDoesNotContainConditionDataMapper : ConditionDataMapper<ICartDoesNotContainCondition>
	{
		public CartDoesNotContainConditionDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ICartDoesNotContainCondition Create()
		{
			var condition = base.Create();
			condition.CampaignConfigId = MapValue<int>("CartDoesNotContainCondition.ConfigId");
			condition.SetUntouched();
			return condition;
		}
	}
}