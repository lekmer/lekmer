﻿using System;
using System.Data;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CampaignToTagDataMapper : DataMapperBase<ICampaignToTag>
	{
		public CampaignToTagDataMapper(IDataReader dataReader)
			: base(dataReader)
		{
		}

		protected override ICampaignToTag Create()
		{
			var campaignToTag = IoC.Resolve<ICampaignToTag>();

			campaignToTag.Id = MapValue<int>("Id");
			campaignToTag.CampaignId = MapValue<int>("CampaignId");
			campaignToTag.ProcessingDate = MapNullableValue<DateTime?>("ProcessingDate");

			return campaignToTag;
		}
	}
}