﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Mapper
{
	public class CampaignHyIdDataMapper : DataMapperBase<ICampaignHyId>
	{
		public CampaignHyIdDataMapper(IDataReader dataReader) : base(dataReader)
		{
		}

		protected override ICampaignHyId Create()
		{
			var campaign = IoC.Resolve<ICampaignHyId>();

			campaign.Id = MapValue<int>("Campaign.Id");
			campaign.Title = MapValue<string>("Campaign.Title");
			campaign.ErpId = MapValue<string>("ErpId");
			campaign.Exclusive = MapValue<bool>("Campaign.Exclusive");
			campaign.FolderId = MapValue<int>("Campaign.FolderId");
			campaign.CampaignStatusId = MapValue<int>("CampaignStatus.Id");
			campaign.CampaignStatusTitle = MapValue<string>("CampaignStatus.Title");
			campaign.CampaignStatusCommonName = MapValue<string>("CampaignStatus.CommonName");
			campaign.Priority = MapValue<int>("Campaign.Priority");
			campaign.EndDate = MapNullableValue<DateTime?>("Campaign.EndDate");
			campaign.StartDate = MapNullableValue<DateTime?>("Campaign.StartDate");

			campaign.CampaignStatus = new CampaignStatus()
			{
				Id = campaign.CampaignStatusId,
				Title = campaign.CampaignStatusTitle,
				CommonName = campaign.CampaignStatusCommonName
			};


			return campaign;
		}
	}
}
