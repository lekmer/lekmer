﻿using System.Collections.ObjectModel;
using Litium.Framework.Transaction;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerCartCampaignSecureService : CartCampaignSecureService, ILekmerCampaignSecureService
	{
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Flag")]
		protected IFlagSecureService FlagSecureService { get; private set; }
		protected ICampaignRegistryCampaignSecureService CampaignRegistryCampaignSecureService { get; private set; }
		protected ICampaignLandingPageSecureService CampaignLandingPageSecureService { get; private set; }

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "flag")]
		public LekmerCartCampaignSecureService(
			IAccessValidator accessValidator,
			CartCampaignRepository cartCampaignRepository,
			ICampaignSecureService campaignSecureService,
			IConditionSecureService conditionSecureService,
			ICartActionSecureService cartActionSecureService,
			ICampaignStatusSecureService campaignStatusSecureService,
			IFlagSecureService flagSecureService,
			ICampaignRegistryCampaignSecureService campaignRegistryCampaignSecureService,
			ICampaignLandingPageSecureService campaignLandingPageSecureService)
			: base(accessValidator, cartCampaignRepository, campaignSecureService, conditionSecureService, cartActionSecureService, campaignStatusSecureService)
		{
			FlagSecureService = flagSecureService;
			CampaignRegistryCampaignSecureService = campaignRegistryCampaignSecureService;
			CampaignLandingPageSecureService = campaignLandingPageSecureService;
		}

		/// <summary>
		/// Saves CartCampaigns to database.
		/// </summary>
		/// <returns>First saved CartCampaign</returns>
		public ICampaign Save(
			ISystemUserFull systemUserFull,
			ICampaign campaign,
			int? campaignFlagId,
			Collection<int> campaignRegistries,
			ICampaignLandingPage landingPage,
			Collection<ITranslationGeneric> webTitleTranslations,
			Collection<ITranslationGeneric> descriptionTranslations)
		{
			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				int campaignId = Save(systemUserFull, (ICartCampaign) campaign);
				FlagSecureService.SaveCampaignFlag(systemUserFull, campaignId, campaignFlagId);
				CampaignRegistryCampaignSecureService.Save(campaignId, campaignRegistries);
				campaign.Id = campaignId;

				if (campaignId > 0 && landingPage != null)
				{
					landingPage.CampaignId = campaignId;
					CampaignLandingPageSecureService.Save(systemUserFull, landingPage, webTitleTranslations, descriptionTranslations);
				}
				transactedOperation.Complete();
			}

			return campaign.Id > 0 ? GetById(campaign.Id) : null;
		}
	}
}