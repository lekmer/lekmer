﻿using System;
using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class FixedPriceActionSecureService : IProductActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected FixedPriceActionRepository Repository { get; private set; }
		protected IProductActionTypeSecureService ProductActionTypeSecureService { get; set; }

		public FixedPriceActionSecureService(
			IAccessValidator accessValidator,
			FixedPriceActionRepository fixedPriceActionRepository,
			IProductActionTypeSecureService productActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = fixedPriceActionRepository;
			ProductActionTypeSecureService = productActionTypeSecureService;
		}

		public IProductAction Create()
		{
			if (ProductActionTypeSecureService == null)
			{
				throw new InvalidOperationException("ProductActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<IFixedPriceAction>();

			action.ActionType = ProductActionTypeSecureService.GetByCommonName("FixedPrice");
			action.Amounts = new LekmerCurrencyValueDictionary();
			action.IncludeProducts = new ProductIdDictionary();
			action.ExcludeProducts = new ProductIdDictionary();
			action.IncludeCategories = new CategoryIdDictionary();
			action.ExcludeCategories = new CategoryIdDictionary();
			action.IncludeBrands = new BrandIdDictionary();
			action.ExcludeBrands = new BrandIdDictionary();
			action.Status = BusinessObjectStatus.New;

			return action;
		}

		public void Save(ISystemUserFull user, IProductAction action)
		{
			Repository.EnsureNotNull();

			var fixedPriceAction = action as IFixedPriceAction;
			if (fixedPriceAction == null)
			{
				throw new InvalidOperationException("action is not IFixedPriceAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(fixedPriceAction);

				SaveAmounts(fixedPriceAction);

				SaveIncludeProducts(fixedPriceAction);
				SaveExcludeProducts(fixedPriceAction);
				SaveIncludeCategories(fixedPriceAction);
				SaveExcludeCategories(fixedPriceAction);
				SaveIncludeBrands(fixedPriceAction);
				SaveExcludeBrands(fixedPriceAction);

				transactedOperation.Complete();
			}
		}

		public void Delete(ISystemUserFull user, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public IProductAction GetById(int id)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(id);

			if (action == null)
			{
				return null;
			}

			action.Amounts = Repository.GetCurrencyValuesByAction(id);
			action.IncludeProducts = Repository.GetIncludeProducts(id);
			action.ExcludeProducts = Repository.GetExcludeProducts(id);
			action.IncludeCategories = Repository.GetIncludeCategories(id);
			action.ExcludeCategories = Repository.GetExcludeCategories(id);
			action.IncludeBrands = Repository.GetIncludeBrands(id);
			action.ExcludeBrands = Repository.GetExcludeBrands(id);

			return action;
		}


		protected virtual void SaveAmounts(IFixedPriceAction fixedPriceAction)
		{
			Repository.DeleteAllCurrencyValue(fixedPriceAction.Id);

			foreach (var amount in fixedPriceAction.Amounts)
			{
				Repository.SaveCurrencyValue(fixedPriceAction.Id, amount);
			}
		}


		protected virtual void SaveIncludeProducts(IFixedPriceAction fixedPriceAction)
		{
			Repository.DeleteIncludeProducts(fixedPriceAction.Id);

			if (fixedPriceAction.IncludeAllProducts)
			{
				return;
			}

			ProductIdDictionary productIdDictionary = fixedPriceAction.IncludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertIncludeProduct(fixedPriceAction.Id, productIdPair.Key);
			}
		}

		protected virtual void SaveExcludeProducts(IFixedPriceAction fixedPriceAction)
		{
			Repository.DeleteExcludeProducts(fixedPriceAction.Id);

			ProductIdDictionary productIdDictionary = fixedPriceAction.ExcludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				Repository.InsertExcludeProduct(fixedPriceAction.Id, productIdPair.Key);
			}
		}


		protected virtual void SaveIncludeCategories(IFixedPriceAction fixedPriceAction)
		{
			Repository.DeleteIncludeCategories(fixedPriceAction.Id);

			if (fixedPriceAction.IncludeAllProducts)
			{
				return;
			}

			CategoryIdDictionary categoryIds = fixedPriceAction.IncludeCategories;
			if (categoryIds == null || categoryIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> categoryId in categoryIds)
			{
				Repository.InsertIncludeCategory(fixedPriceAction.Id, categoryId.Key);
			}
		}

		protected virtual void SaveExcludeCategories(IFixedPriceAction fixedPriceAction)
		{
			Repository.DeleteExcludeCategories(fixedPriceAction.Id);

			CategoryIdDictionary categoryIds = fixedPriceAction.ExcludeCategories;
			if (categoryIds == null || categoryIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> categoryId in categoryIds)
			{
				Repository.InsertExcludeCategory(fixedPriceAction.Id, categoryId.Key);
			}
		}


		protected virtual void SaveIncludeBrands(IFixedPriceAction fixedPriceAction)
		{
			Repository.DeleteIncludeBrands(fixedPriceAction.Id);

			if (fixedPriceAction.IncludeAllProducts)
			{
				return;
			}

			BrandIdDictionary brandIds = fixedPriceAction.IncludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				Repository.InsertIncludeBrand(fixedPriceAction.Id, brandId.Key);
			}
		}

		protected virtual void SaveExcludeBrands(IFixedPriceAction fixedPriceAction)
		{
			Repository.DeleteExcludeBrands(fixedPriceAction.Id);

			BrandIdDictionary brandIds = fixedPriceAction.ExcludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				Repository.InsertExcludeBrand(fixedPriceAction.Id, brandId.Key);
			}
		}
	}
}