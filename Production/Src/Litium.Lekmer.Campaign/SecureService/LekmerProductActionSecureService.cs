﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Framework.Transaction;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Product.Constant;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class LekmerProductActionSecureService : ProductActionSecureService
	{
		protected IProductActionTargetProductTypeSecureService ProductActionTargetProductTypeSecureService { get; private set; }

		public LekmerProductActionSecureService(
			IAccessValidator accessValidator,
			ProductActionRepository productActionRepository,
			ProductActionSecureServiceLocator secureServiceLocator,
			IProductActionTargetProductTypeSecureService productActionTargetProductTypeSecureService)
			: base(accessValidator, productActionRepository, secureServiceLocator)
		{
			ProductActionTargetProductTypeSecureService = productActionTargetProductTypeSecureService;
		}

		/// <summary>
		/// Creates a new <see cref="IProductAction"/> object.
		/// </summary>
		/// <param name="typeCommonName">Product action type common name</param>
		/// <returns>The <see cref="IProductAction"/> object.</returns>
		public override IProductAction Create(string typeCommonName)
		{
			var productAction = (ILekmerProductAction)base.Create(typeCommonName);

			productAction.TargetProductTypes = new IdDictionary { (int)ProductType.Product }; // Target = Product by default

			return productAction;
		}

		/// <summary>
		/// Gets a collection of <see cref="IProductAction"/> objects assigned to proper campaign.
		/// </summary>
		/// <param name="campaignId">Campaign Id.</param>
		/// <returns>The collection of <see cref="IProductAction"/> objects.</returns>
		public override Collection<IProductAction> GetAllByCampaign(int campaignId)
		{
			Collection<IProductAction> productActions = base.GetAllByCampaign(campaignId);

			var actionTargetProductTypes = ProductActionTargetProductTypeSecureService.GetAllByCampaign(campaignId);

			foreach (ILekmerProductAction productAction in productActions)
			{
				int productActionId = productAction.Id;
				IEnumerable<int> productTypes = actionTargetProductTypes.Where(t => t.ProductActionId == productActionId).Select(t => t.ProductTypeId);

				productAction.TargetProductTypes = new IdDictionary(productTypes);
			}

			return productActions;
		}

		/// <summary>
		/// Saves a product action.
		/// </summary>
		/// <param name="systemUserFull">Logged in system user</param>
		/// <param name="productAction">Product action</param>
		/// <returns>The product action identifier.</returns>
		public override int Save(ISystemUserFull systemUserFull, IProductAction productAction)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				base.Save(systemUserFull, productAction);

				ProductActionTargetProductTypeSecureService.Save(systemUserFull, productAction);

				transactedOperation.Complete();
			}

			return productAction.Id;
		}

		/// <summary>
		/// Deletes product action.
		/// </summary>
		/// <param name="systemUserFull">Logged in system user</param>
		/// <param name="id">Product action identifier</param>
		/// <param name="typeCommonName">Product action type common name</param>
		public override void Delete(ISystemUserFull systemUserFull, int id, string typeCommonName)
		{
			using (var transactedOperation = new TransactedOperation())
			{
				ProductActionTargetProductTypeSecureService.Delete(systemUserFull, id);

				base.Delete(systemUserFull, id, typeCommonName);

				transactedOperation.Complete();
			}
		}
	}
}
