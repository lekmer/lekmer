﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class FixedDiscountCartActionSecureService : ICartActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected FixedDiscountCartActionRepository Repository { get; private set; }
		protected ICartActionTypeSecureService CartActionTypeSecureService { get; private set; }

		public FixedDiscountCartActionSecureService(
			IAccessValidator accessValidator,
			FixedDiscountCartActionRepository fixedDiscountCartActionRepository,
			ICartActionTypeSecureService cartActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = fixedDiscountCartActionRepository;
			CartActionTypeSecureService = cartActionTypeSecureService;
		}

		public ICartAction Create()
		{
			if (CartActionTypeSecureService == null)
			{
				throw new InvalidOperationException("CartActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<IFixedDiscountCartAction>();

			action.ActionType = CartActionTypeSecureService.GetByCommonName("FixedCartDiscount");
			action.Amounts = new LekmerCurrencyValueDictionary();
			action.Status = BusinessObjectStatus.New;

			return action;
		}

		public void Save(ISystemUserFull user, ICartAction action)
		{
			Repository.EnsureNotNull();

			var fixedDiscountCartAction = action as IFixedDiscountCartAction;
			if (fixedDiscountCartAction == null)
			{
				throw new InvalidOperationException("action is not IFixedDiscountCartAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(fixedDiscountCartAction);

				SaveAmounts(fixedDiscountCartAction);

				transactedOperation.Complete();
			}
		}

		public void Delete(ISystemUserFull systemUserFull, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public ICartAction GetById(int id)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(id);
			if (action == null)
			{
				return null;
			}

			action.Amounts = Repository.GetCurrencyValuesByAction(id);

			return action;
		}


		protected virtual void SaveAmounts(IFixedDiscountCartAction fixedDiscountCartAction)
		{
			Repository.DeleteAllCurrencyValue(fixedDiscountCartAction.Id);

			foreach (var amount in fixedDiscountCartAction.Amounts)
			{
				Repository.SaveCurrencyValue(fixedDiscountCartAction.Id, amount);
			}
		}
	}
}