﻿using System.Collections.Generic;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class CampaignConfigSecureService : ICampaignConfigSecureService
	{
		protected CampaignConfigRepository Repository { get; private set; }
		protected CampaignActionRepository CampaignActionRepository { get; private set; }

		public CampaignConfigSecureService(CampaignConfigRepository repository, CampaignActionRepository campaignActionRepository)
		{
			Repository = repository;
			CampaignActionRepository = campaignActionRepository;
		}

		public virtual ICampaignConfig Create()
		{
			var campaignConfig = IoC.Resolve<ICampaignConfig>();

			campaignConfig.IncludeProducts = new ProductIdDictionary();
			campaignConfig.ExcludeProducts = new ProductIdDictionary();

			campaignConfig.IncludeCategories = new CampaignCategoryDictionary();
			campaignConfig.ExcludeCategories = new CampaignCategoryDictionary();

			campaignConfig.IncludeBrands = new BrandIdDictionary();
			campaignConfig.ExcludeBrands = new BrandIdDictionary();

			campaignConfig.Status = BusinessObjectStatus.New;

			return campaignConfig;
		}

		public virtual ICampaignConfig GetById(int campaignConfigId)
		{
			Repository.EnsureNotNull();
			CampaignActionRepository.EnsureNotNull();

			ICampaignConfig campaignConfig = Repository.GetById(campaignConfigId);

			campaignConfig.IncludeProducts = CampaignActionRepository.GetIncludeProducts(campaignConfigId);
			campaignConfig.ExcludeProducts = CampaignActionRepository.GetExcludeProducts(campaignConfigId);
			campaignConfig.IncludeCategories = CampaignActionRepository.GetIncludeCategories(campaignConfigId);
			campaignConfig.ExcludeCategories = CampaignActionRepository.GetExcludeCategories(campaignConfigId);
			campaignConfig.IncludeBrands = CampaignActionRepository.GetIncludeBrands(campaignConfigId);
			campaignConfig.ExcludeBrands = CampaignActionRepository.GetExcludeBrands(campaignConfigId);

			// campaignConfig.SetUntouched(); //TODO: Makes problems to save

			return campaignConfig;
		}

		public virtual int Save(ICampaignConfig campaignConfig)
		{
			Repository.EnsureNotNull();
			CampaignActionRepository.EnsureNotNull();

			using (var transactedOperation = new TransactedOperation())
			{
				// Save config
				int configId = Repository.Save(campaignConfig);

				campaignConfig.Id = configId;

				SaveIncludeProducts(campaignConfig);
				SaveExcludeProducts(campaignConfig);
				SaveIncludeCategories(campaignConfig);
				SaveExcludeCategories(campaignConfig);
				SaveIncludeBrands(campaignConfig);
				SaveExcludeBrands(campaignConfig);

				transactedOperation.Complete();
			}

			return campaignConfig.Id;
		}


		protected virtual void SaveIncludeProducts(ICampaignConfig campaignConfig)
		{
			CampaignActionRepository.DeleteIncludeProducts(campaignConfig.Id);

			if (campaignConfig.IncludeAllProducts)
			{
				return;
			}

			ProductIdDictionary productIdDictionary = campaignConfig.IncludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				CampaignActionRepository.InsertIncludeProduct(campaignConfig.Id, productIdPair.Key);
			}
		}

		protected virtual void SaveExcludeProducts(ICampaignConfig campaignConfig)
		{
			CampaignActionRepository.DeleteExcludeProducts(campaignConfig.Id);

			ProductIdDictionary productIdDictionary = campaignConfig.ExcludeProducts;
			if (productIdDictionary == null || productIdDictionary.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> productIdPair in productIdDictionary)
			{
				CampaignActionRepository.InsertExcludeProduct(campaignConfig.Id, productIdPair.Key);
			}
		}


		protected virtual void SaveIncludeCategories(ICampaignConfig campaignConfig)
		{
			CampaignActionRepository.DeleteIncludeCategories(campaignConfig.Id);

			if (campaignConfig.IncludeAllProducts)
			{
				return;
			}

			CampaignCategoryDictionary categories = campaignConfig.IncludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, bool> category in categories)
			{
				CampaignActionRepository.InsertIncludeCategory(campaignConfig.Id, category.Key, category.Value);
			}
		}

		protected virtual void SaveExcludeCategories(ICampaignConfig campaignConfig)
		{
			CampaignActionRepository.DeleteExcludeCategories(campaignConfig.Id);

			CampaignCategoryDictionary categories = campaignConfig.ExcludeCategories;
			if (categories == null || categories.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, bool> category in categories)
			{
				CampaignActionRepository.InsertExcludeCategory(campaignConfig.Id, category.Key, category.Value);
			}
		}


		protected virtual void SaveIncludeBrands(ICampaignConfig campaignConfig)
		{
			CampaignActionRepository.DeleteIncludeBrands(campaignConfig.Id);

			if (campaignConfig.IncludeAllProducts)
			{
				return;
			}

			BrandIdDictionary brandIds = campaignConfig.IncludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				CampaignActionRepository.InsertIncludeBrand(campaignConfig.Id, brandId.Key);
			}
		}

		protected virtual void SaveExcludeBrands(ICampaignConfig campaignConfig)
		{
			CampaignActionRepository.DeleteExcludeBrands(campaignConfig.Id);

			BrandIdDictionary brandIds = campaignConfig.ExcludeBrands;
			if (brandIds == null || brandIds.Count == 0)
			{
				return;
			}

			foreach (KeyValuePair<int, int> brandId in brandIds)
			{
				CampaignActionRepository.InsertExcludeBrand(campaignConfig.Id, brandId.Key);
			}
		}
	}
}