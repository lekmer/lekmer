﻿using System;
using Litium.Framework.Transaction;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	public class GiftCardViaMailCartActionSecureService : ICartActionPluginSecureService
	{
		protected IAccessValidator AccessValidator { get; private set; }
		protected GiftCardViaMailCartActionRepository Repository { get; private set; }
		protected ICartActionTypeSecureService CartActionTypeSecureService { get; private set; }

		public GiftCardViaMailCartActionSecureService(
			IAccessValidator accessValidator,
			GiftCardViaMailCartActionRepository giftCardViaMailCartActionRepository,
			ICartActionTypeSecureService cartActionTypeSecureService)
		{
			AccessValidator = accessValidator;
			Repository = giftCardViaMailCartActionRepository;
			CartActionTypeSecureService = cartActionTypeSecureService;
		}

		public ICartAction Create()
		{
			if (CartActionTypeSecureService == null)
			{
				throw new InvalidOperationException("CartActionTypeSecureService cannot be null.");
			}

			var action = IoC.Resolve<IGiftCardViaMailCartAction>();

			action.ActionType = CartActionTypeSecureService.GetByCommonName("GiftCardViaMailCart");
			action.Amounts = new LekmerCurrencyValueDictionary();
			action.Status = BusinessObjectStatus.New;

			return action;
		}

		public void Save(ISystemUserFull user, ICartAction action)
		{
			Repository.EnsureNotNull();

			var giftCardViaMailCartAction = action as IGiftCardViaMailCartAction;
			if (giftCardViaMailCartAction == null)
			{
				throw new InvalidOperationException("action is not IGiftCardViaMailCartAction type");
			}

			AccessValidator.ForceAccess(user, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Save(giftCardViaMailCartAction);

				SaveAmounts(giftCardViaMailCartAction);

				transactedOperation.Complete();
			}
		}

		public void Delete(ISystemUserFull systemUserFull, int id)
		{
			Repository.EnsureNotNull();

			AccessValidator.ForceAccess(systemUserFull, PrivilegeConstant.Campaign);

			using (var transactedOperation = new TransactedOperation())
			{
				Repository.Delete(id);
				transactedOperation.Complete();
			}
		}

		public ICartAction GetById(int id)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(id);
			if (action == null)
			{
				return null;
			}

			action.Amounts = Repository.GetCurrencyValuesByAction(id);

			return action;
		}


		protected virtual void SaveAmounts(IGiftCardViaMailCartAction giftCardViaMailCartAction)
		{
			Repository.DeleteAllCurrencyValue(giftCardViaMailCartAction.Id);

			foreach (var amount in giftCardViaMailCartAction.Amounts)
			{
				Repository.SaveCurrencyValue(giftCardViaMailCartAction.Id, amount);
			}
		}
	}
}