using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class VoucherDiscountActionRepository
	{
		protected virtual DataMapperBase<IVoucherDiscountAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IVoucherDiscountAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}

		public virtual IVoucherDiscountAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", cartActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pVoucherDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int actionId)
		{
			IEnumerable<CurrencyValue> currencyValues;
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.GetCurrencyValuesByAction");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pVoucherActionValuesGetByActionId]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}
			var dictionary = new LekmerCurrencyValueDictionary();
			foreach (var currencyValue in currencyValues)
			{
				dictionary.Add(currencyValue.Currency.Id, currencyValue.MonetaryValue);
			}
			return dictionary;
		}

		public virtual void Save(IVoucherDiscountAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DiscountValue", action.DiscountValue, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("@Fixed", action.FixedDiscount, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@Percentage", action.PercentageDiscount, SqlDbType.Bit),
					ParameterHelper.CreateParameter("@AllowSpecialOffer", action.AllowSpecialOffer, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pVoucherActionSave]", parameters, dbSettings);
		}

		public virtual void SaveCurrencyValue(int actionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", actionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("@CurrencyId", currencyValue.Key, SqlDbType.Int),
				ParameterHelper.CreateParameter("@Value", currencyValue.Value, SqlDbType.Decimal)
			};
			var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.SaveCurrencyValue");
			new DataHandler().ExecuteCommand("[lekmer].[pVoucherActionValueSave]", parameters, dbSettings);
		}

		public virtual void Delete(int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", id, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pVoucherActionDelete]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValues(int id)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@CartActionId", id, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("VoucherDiscountActionRepository.DeleteAllCurrencyValues");
			new DataHandler().ExecuteCommand("[lekmer].[pVoucherActionValueDeleteAll]", parameters, dbSettings);
		}
	}
}