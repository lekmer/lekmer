﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class ConditionTargetProductTypeRepository
	{
		protected virtual DataMapperBase<IConditionTargetProductType> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IConditionTargetProductType>(dataReader);
		}

		public virtual Collection<IConditionTargetProductType> GetAllByCampaign(int campaignId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ConditionTargetProductTypeRepository.GetAllByCampaign");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pConditionTargetProductTypeGetAllByCampaign]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		public virtual void Save(int conditionId, string targetProductTypeIds, char delimiter)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("TargetProductTypeIds", targetProductTypeIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", delimiter, SqlDbType.Char, 1)
				};

			var dbSettings = new DatabaseSetting("ConditionTargetProductTypeRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pConditionTargetProductTypeSave]", parameters, dbSettings);
		}

		public virtual void Delete(int conditionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConditionId", conditionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("ConditionTargetProductTypeRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pConditionTargetProductTypeDelete]", parameters, dbSettings);
		}
	}
}