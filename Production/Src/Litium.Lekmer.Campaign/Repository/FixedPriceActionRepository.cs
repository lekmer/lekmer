﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class FixedPriceActionRepository
	{
		protected virtual DataMapperBase<IFixedPriceAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IFixedPriceAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}


		public virtual void Save(IFixedPriceAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeAllProducts", action.IncludeAllProducts, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionDelete]", parameters, dbSettings);
		}

		public virtual IFixedPriceAction GetById(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<int> GetProductIds(int productActionId)
		{
			var productIds = new Collection<int>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ActionId ", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetProductIds");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionGetProductIds]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// CurrencyValue.

		public virtual void SaveCurrencyValue(int productActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("Value", currencyValue.Value, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.SaveCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.DeleteAllCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int productActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetCurrencyValuesByAction");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionCurrencyGetByAction]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();

			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}

			return lekmerCurrencyValueDictionary;
		}


		// Products.

		public virtual void InsertIncludeProduct(int productActionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.InsertIncludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedPriceActionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int productActionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.InsertExcludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedPriceActionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeProducts(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.DeleteIncludeProducts");
			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.DeleteExcludeProducts");
			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual ProductIdDictionary GetIncludeProducts(int productActionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetIncludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int productActionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetExcludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// Campaigns.

		public virtual void InsertIncludeCategory(int productActionId, int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.InsertIncludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedPriceActionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int productActionId, int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.InsertExcludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedPriceActionExcludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.DeleteIncludeCategories");
			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.DeleteExcludeCategories");
			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual CategoryIdDictionary GetIncludeCategories(int productActionId)
		{
			var categoryIds = new CategoryIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetIncludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categoryIds.Add(dataReader.GetInt32(0));
				}
			}

			return categoryIds;
		}

		public virtual CategoryIdDictionary GetExcludeCategories(int productActionId)
		{
			var categoryIds = new CategoryIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetExcludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categoryIds.Add(dataReader.GetInt32(0));
				}
			}

			return categoryIds;
		}


		// Brands.

		public virtual void InsertIncludeBrand(int productActionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.InsertIncludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedPriceActionIncludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeBrand(int productActionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.InsertExcludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedPriceActionExcludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeBrands(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.DeleteIncludeBrands");
			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionIncludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeBrands(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.DeleteExcludeBrands");
			new DataHandler().ExecuteCommand("[campaignlek].[pFixedPriceActionExcludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual BrandIdDictionary GetIncludeBrands(int productActionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetIncludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionIncludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}

		public virtual BrandIdDictionary GetExcludeBrands(int productActionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedPriceActionRepository.GetExcludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedPriceActionExcludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}
	}
}