using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CampaignLandingPageRepository
	{
		const string DefaultMediaId = "-1";

		protected virtual DataMapperBase<ICampaignLandingPage> CreateCampaignLandingPageDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICampaignLandingPage>(dataReader);
		}

		protected virtual DataMapperBase<ICampaignRegistryCampaignLandingPage> CreateCampaignRegistryCampaignLandingPageDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICampaignRegistryCampaignLandingPage>(dataReader);
		}

		protected virtual DataMapperBase<ITranslationGeneric> CreateTranslationDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ITranslationGeneric>(dataReader);
		}

		public virtual int Save(ICampaignLandingPage campaignLandingPage)
		{
			string campaignRegistryIds = string.Empty, iconIds = string.Empty, imageIds = string.Empty, contentNodeIds = string.Empty;
			var registryDataList = campaignLandingPage.RegistryDataList;
			if (registryDataList != null && registryDataList.Count > 0)
			{
				CampaignLandingPagesToString(registryDataList, out campaignRegistryIds, out iconIds, out imageIds, out contentNodeIds);
			}

			var dbSettings = new DatabaseSetting("CampaignLandingPageRepository.Save");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignLandingPageId", campaignLandingPage.CampaignLandingPageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CampaignId", campaignLandingPage.CampaignId, SqlDbType.Int),
					ParameterHelper.CreateParameter("WebTitle", campaignLandingPage.WebTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", campaignLandingPage.Description, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("CampaignRegistryIds", campaignRegistryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("IconIds", iconIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ImageIds", imageIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("ContentNodeIds", contentNodeIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", Convert.DefaultListSeparator, SqlDbType.Char)
				};
			return new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCampaignLandingPageSave]", parameters, dbSettings);
		}
		public virtual void Delete(int campaignId)
		{
			var dbSettings = new DatabaseSetting("CampaignLandingPageRepository.Delete");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignLandingPageDelete]", parameters, dbSettings);
		}
		public virtual void DeleteCampaignRegistryCampaignLandingPage(int campaignLandingPageId, string campaignRegistryIds)
		{
			var dbSettings = new DatabaseSetting("CampaignLandingPageRepository.DeleteCampaignRegistryCampaignLandingPage");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignLandingPageId", campaignLandingPageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CampaignRegistryIds", campaignRegistryIds, SqlDbType.VarChar),
					ParameterHelper.CreateParameter("Delimiter", Convert.DefaultListSeparator, SqlDbType.Char)
				};
			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignRegistryCampaignLandingPageDelete]", parameters, dbSettings);
		}
		public virtual ICampaignLandingPage GetByCampaignIdSecure(int campaignId)
		{
			ICampaignLandingPage campaignLandingPage;

			var dbSettings = new DatabaseSetting("CampaignLandingPageRepository.GetByCampaignIdSecure");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignLandingPageGetByCampaignIdSecure]", parameters, dbSettings))
			{
				campaignLandingPage = CreateCampaignLandingPageDataMapper(dataReader).ReadRow();

				if (campaignLandingPage != null && dataReader.NextResult())
				{
					campaignLandingPage.RegistryDataList = CreateCampaignRegistryCampaignLandingPageDataMapper(dataReader).ReadMultipleRows();
				}
			}

			return campaignLandingPage;
		}
		public virtual ICampaignLandingPage GetByCampaignId(int channelId, int campaignId)
		{
			ICampaignLandingPage campaignLandingPage;

			var dbSettings = new DatabaseSetting("CampaignLandingPageRepository.GetByCampaignId");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignId", campaignId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ChannelId", channelId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignLandingPageGetByCampaignId]", parameters, dbSettings))
			{
				campaignLandingPage = CreateCampaignLandingPageDataMapper(dataReader).ReadRow();

				if (campaignLandingPage != null && dataReader.NextResult())
				{
					campaignLandingPage.RegistryDataList = CreateCampaignRegistryCampaignLandingPageDataMapper(dataReader).ReadMultipleRows();
				}
			}

			return campaignLandingPage;
		}

		// Translations.
		public virtual void SaveTranslation(int campaignLandingPageId, int languageId, string webTitle, string description)
		{
			var dbSettings = new DatabaseSetting("CampaignLandingPageRepository.SaveTranslation");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignLandingPageId", campaignLandingPageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("LanguageId", languageId, SqlDbType.Int),
					ParameterHelper.CreateParameter("WebTitle", webTitle, SqlDbType.NVarChar),
					ParameterHelper.CreateParameter("Description", description, SqlDbType.NVarChar)
				};
			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignLandingPageTranslationSave]", parameters, dbSettings);
		}
		public virtual void DeleteTranslations(int campaignLandingPageId)
		{
			var dbSettings = new DatabaseSetting("CampaignLandingPageRepository.DeleteTranslations");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignLandingPageId", campaignLandingPageId, SqlDbType.Int)
				};
			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignLandingPageTranslationDelete]", parameters, dbSettings);
		}
		public virtual Collection<ITranslationGeneric> GetAllWebTitleTranslations(int campaignLandingPageId)
		{
			return GetAllTranslations(campaignLandingPageId, "[campaignlek].[pCampaignLandingPageTranslationWebTitleGetAll]");
		}
		public virtual Collection<ITranslationGeneric> GetAllDescriptionTranslations(int campaignLandingPageId)
		{
			return GetAllTranslations(campaignLandingPageId, "[campaignlek].[pCampaignLandingPageTranslationDescriptionGetAll]");
		}
		protected virtual Collection<ITranslationGeneric> GetAllTranslations(int campaignLandingPageId, string storedProcedureName)
		{
			var dbSettings = new DatabaseSetting("CampaignLandingPageRepository.GetAllTranslations");
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CampaignLandingPageId", campaignLandingPageId, SqlDbType.Int)
				};
			using (IDataReader dataReader = new DataHandler().ExecuteSelect(storedProcedureName, parameters, dbSettings))
			{
				return CreateTranslationDataMapper(dataReader).ReadMultipleRows();
			}
		}

		// Private methods.
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1822:MarkMembersAsStatic")]
		private void CampaignLandingPagesToString(
			IEnumerable<ICampaignRegistryCampaignLandingPage> registryDataList,
			out string campaignRegistryIds,
			out string iconIds,
			out string imageIds,
			out string contentNodeIds)
		{
			campaignRegistryIds = string.Empty;
			iconIds = string.Empty;
			imageIds = string.Empty;
			contentNodeIds = string.Empty;

			foreach (var data in registryDataList)
			{
				campaignRegistryIds = campaignRegistryIds + data.CampaignRegistryId + Convert.DefaultListSeparator;
				iconIds = iconIds + (data.IconMediaId.HasValue && data.IconMediaId > 0 ? data.IconMediaId.ToString() : DefaultMediaId) + Convert.DefaultListSeparator;
				imageIds = imageIds + (data.ImageMediaId.HasValue && data.ImageMediaId > 0 ? data.ImageMediaId.ToString() : DefaultMediaId) + Convert.DefaultListSeparator;
				contentNodeIds = contentNodeIds + data.ContentNodeId + Convert.DefaultListSeparator;
			}

			campaignRegistryIds = campaignRegistryIds.Substring(0, campaignRegistryIds.Length-1);
			iconIds = iconIds.Substring(0, iconIds.Length-1);
			imageIds = imageIds.Substring(0, imageIds.Length-1);
			contentNodeIds = contentNodeIds.Substring(0, contentNodeIds.Length-1);
		}
	}
}