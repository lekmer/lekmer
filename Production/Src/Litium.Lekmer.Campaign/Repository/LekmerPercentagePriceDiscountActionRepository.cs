﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Scensum.Campaign;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class LekmerPercentagePriceDiscountActionRepository : PercentagePriceDiscountActionRepository
	{
		public new virtual void Save(IPercentagePriceDiscountAction action)
		{
			if (action == null) throw new ArgumentNullException("action");

			var lekmerAction = action as ILekmerPercentagePriceDiscountAction;
			if (lekmerAction == null) throw new ArgumentException("lekmerAction should be of ILekmerPercentagePriceDiscountAction type");

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("DiscountAmount", action.DiscountAmount, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("ConfigId", lekmerAction.CampaignConfigId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerPercentagePriceDiscountActionRepository.Save");
			new DataHandler().ExecuteCommand("[campaign].[pPercentagePriceDiscountActionSave]", parameters, dbSettings);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1725:ParameterNamesShouldMatchBaseDeclaration", MessageId = "0#")]
		public override IPercentagePriceDiscountAction GetById(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId ", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerPercentagePriceDiscountActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaign].[pPercentagePriceDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<int> GetProductIds(int actionId)
		{
			var productIds = new Collection<int>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ActionId ", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("LekmerPercentagePriceDiscountActionRepository.GetProductIds");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaign].[pPercentagePriceDiscountActionGetProductIds]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}
	}
}