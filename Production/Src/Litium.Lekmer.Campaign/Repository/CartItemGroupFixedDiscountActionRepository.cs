﻿using System.Collections.Generic;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CartItemGroupFixedDiscountActionRepository
	{
		protected DataMapperBase<ICartItemGroupFixedDiscountAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICartItemGroupFixedDiscountAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}

		public virtual void Save(ICartItemGroupFixedDiscountAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeAllProducts", action.IncludeAllProducts, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionDelete]", parameters, dbSettings);
		}

		public virtual ICartItemGroupFixedDiscountAction GetById(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}


		// CurrencyValue.

		public virtual void SaveCurrencyValue(int cartActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("MonetaryValue", currencyValue.Value, SqlDbType.Decimal)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.SaveCurrencyValue");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int cartActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.DeleteAllCurrencyValue");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int cartActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", cartActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetCurrencyValuesByAction");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionCurrencyGetByAction]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();

			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}

			return lekmerCurrencyValueDictionary;
		}


		// Products

		public virtual void InsertIncludeProduct(int actionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.InsertIncludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemGroupFixedDiscountActionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int actionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.InsertExcludeProduct");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemGroupFixedDiscountActionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeProducts(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.DeleteIncludeProducts");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.DeleteExcludeProducts");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual ProductIdDictionary GetIncludeProducts(int actionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetIncludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int actionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetExcludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// Categories

		public virtual void InsertIncludeCategory(int actionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.InsertIncludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int actionId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubcategories", includeSubcategories, SqlDbType.Bit)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.InsertExcludeCategory");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.DeleteIncludeCategories");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.DeleteExcludeCategories");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual CampaignCategoryDictionary GetIncludeCategories(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetIncludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetIncludeCategoriesRecursive(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetIncludeCategoriesRecursive");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionIncludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategories(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetExcludeCategories");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategoriesRecursive(int actionId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetExcludeCategoriesRecursive");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionExcludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}


		// Brands

		public virtual void InsertIncludeBrand(int actionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.InsertIncludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeBrand(int actionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.InsertExcludeBrand");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeBrands(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.DeleteIncludeBrands");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeBrands(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.DeleteExcludeBrands");
			new DataHandler().ExecuteCommand("[lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual BrandIdDictionary GetIncludeBrands(int actionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetIncludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionIncludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}

		public virtual BrandIdDictionary GetExcludeBrands(int actionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("CartActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("CartItemGroupFixedDiscountActionRepository.GetExcludeBrands");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pCartItemGroupFixedDiscountActionExcludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}
	}
}