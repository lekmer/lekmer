﻿using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class GiftCardViaMailInfoRepository
	{
		protected virtual DataMapperBase<IGiftCardViaMailInfo> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IGiftCardViaMailInfo>(dataReader);
		}


		public virtual void Save(IGiftCardViaMailInfo giftCardViaMailInfo)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", giftCardViaMailInfo.OrderId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CampaignId", giftCardViaMailInfo.CampaignId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ActionId", giftCardViaMailInfo.ActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("DiscountValue", giftCardViaMailInfo.DiscountValue, SqlDbType.Decimal),
					ParameterHelper.CreateParameter("DateToSend", giftCardViaMailInfo.DateToSend, SqlDbType.DateTime),
					ParameterHelper.CreateParameter("StatusId", giftCardViaMailInfo.StatusId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailInfoRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailInfoInsert]", parameters, dbSettings);
		}

		public virtual void Update(IGiftCardViaMailInfo giftCardViaMailInfo)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("GiftCardViaMailInfoId", giftCardViaMailInfo.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("StatusId", giftCardViaMailInfo.StatusId, SqlDbType.Int),
					ParameterHelper.CreateParameter("VoucherInfoId", giftCardViaMailInfo.VoucherInfoId, SqlDbType.Int),
					ParameterHelper.CreateParameter("VoucherCode", giftCardViaMailInfo.VoucherCode, SqlDbType.NVarChar)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailInfoRepository.Update");

			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailInfoUpdate]", parameters, dbSettings);
		}

		public virtual void DeleteByOrder(int orderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("GiftCardViaMailInfoRepository.DeleteByOrder");
			new DataHandler().ExecuteCommand("[campaignlek].[pGiftCardViaMailInfoDeleteByOrder]", parameters, dbSettings);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IGiftCardViaMailInfo> GetAllToSend()
		{
			var dbSettings = new DatabaseSetting("GiftCardViaMailInfoRepository.GetAllToSend");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pGiftCardViaMailInfoGetAllToSend]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public virtual Collection<IGiftCardViaMailInfo> GetAllByOrder(int orderId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("OrderId", orderId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("GiftCardViaMailInfoRepository.GetAllByOrder");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pGiftCardViaMailInfoGetAllByOrder]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}