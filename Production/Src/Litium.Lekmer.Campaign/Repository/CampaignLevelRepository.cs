using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CampaignLevelRepository
	{
		protected virtual DataMapperBase<ICampaignLevel> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<ICampaignLevel>(dataReader);
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1024:UsePropertiesWhereAppropriate")]
		public Collection<ICampaignLevel> GetAll()
		{
			var dbSettings = new DatabaseSetting("CampaignLevelRepository.GetAll");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignLevelGetAll]", dbSettings))
			{
				return CreateDataMapper(dataReader).ReadMultipleRows();
			}
		}
	}
}