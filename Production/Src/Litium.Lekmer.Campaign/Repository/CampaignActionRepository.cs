﻿using System.Data;
using Litium.Framework.DataAccess;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class CampaignActionRepository
	{
		// Products.

		public virtual void InsertIncludeProduct(int configId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.InsertIncludeProduct");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCampaignActionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int configId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.InsertExcludeProduct");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCampaignActionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeProducts(int configId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.DeleteIncludeProducts");

			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignActionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int configId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.DeleteExcludeProducts");

			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignActionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual ProductIdDictionary GetIncludeProducts(int configId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.GetIncludeProducts");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int configId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
			};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.GetExcludeProducts");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// Categories.

		public virtual void InsertIncludeCategory(int configId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubCategories", includeSubcategories, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.InsertIncludeCategory");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCampaignActionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int configId, int categoryId, bool includeSubcategories)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeSubCategories", includeSubcategories, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.InsertExcludeCategory");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCampaignActionExcludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int configId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.DeleteIncludeCategories");

			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignActionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int configId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.DeleteExcludeCategories");

			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignActionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual CampaignCategoryDictionary GetIncludeCategories(int configId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.GetIncludeCategories");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetIncludeCategoriesRecursive(int configId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.GetIncludeCategoriesRecursive");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionIncludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategories(int configId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.GetExcludeCategories");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}

		public virtual CampaignCategoryDictionary GetExcludeCategoriesRecursive(int configId)
		{
			var categories = new CampaignCategoryDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.GetExcludeCategoriesRecursive");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionExcludeCategoryGetIdAllRecursive]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categories.Add(dataReader.GetInt32(0), dataReader.GetBoolean(1));
				}
			}

			return categories;
		}


		// Brands.

		public virtual void InsertIncludeBrand(int configId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.InsertIncludeBrand");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCampaignActionIncludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeBrand(int configId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.InsertExcludeBrand");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pCampaignActionExcludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeBrands(int configId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.DeleteIncludeBrands");

			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignActionIncludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeBrands(int configId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.DeleteExcludeBrands");

			new DataHandler().ExecuteCommand("[campaignlek].[pCampaignActionExcludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual BrandIdDictionary GetIncludeBrands(int configId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.GetIncludeBrands");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionIncludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}

		public virtual BrandIdDictionary GetExcludeBrands(int configId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ConfigId", configId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("CampaignActionRepository.GetExcludeBrands");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pCampaignActionExcludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}
	}
}