﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Campaign.Mapper;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class ProductDiscountActionRepository
	{
		protected virtual DataMapperBase<IProductDiscountAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IProductDiscountAction>(dataReader);
		}

		protected virtual DataMapperBase<ProductDiscountActionItem> CreateItemDataMapper(IDataReader dataReader)
		{
			return new ProductDiscountActionItemDataMapper(dataReader);
		}

		public virtual IProductDiscountAction GetById(int actionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.GetById");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Dictionary<int, LekmerCurrencyValueDictionary> GetItems(int actionId, string sortBy)
		{
			var productDiscounts = new Dictionary<int, LekmerCurrencyValueDictionary>();
			Collection<ProductDiscountActionItem> data;
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ActionId", actionId, SqlDbType.Int),
				ParameterHelper.CreateParameter("SortBy", sortBy, SqlDbType.VarChar)
			};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.GetItems");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductDiscountActionItemGetAll]", parameters, dbSettings))
			{
				data = CreateItemDataMapper(dataReader).ReadMultipleRows();
			}

			Dictionary<int, Collection<ProductDiscountActionItem>> groupedByProduct = data.GroupBy(p => p.ProductId).ToDictionary(p => p.Key, p => new Collection<ProductDiscountActionItem>(p.ToArray()));
			foreach (var groupedItem in groupedByProduct)
			{
				var dictionary = new LekmerCurrencyValueDictionary();
				foreach (var item in groupedItem.Value)
				{
					dictionary.AddItem(item.CurrencyId, item.MonetaryValue);
				}
				productDiscounts.Add(groupedItem.Key, dictionary);
			}

			return productDiscounts;
		}

		public virtual void Save(IProductDiscountAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ActionId", action.Id, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.Save");
			new DataHandler().ExecuteCommand("[lekmer].[pProductDiscountActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int actionId)
		{
			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.Delete");
			new DataHandler().ExecuteCommand("[lekmer].[pProductDiscountActionDelete]", parameters, dbSettings);
		}

		public virtual void DeleteItems(int actionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.DeleteItems");
			new DataHandler().ExecuteCommand("[lekmer].[pProductDiscountActionItemDeleteAll]", parameters, dbSettings);
		}

		public virtual void InsertItem(int actionId, int productId, int currencyId, decimal discountPrice)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("@ActionId", actionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@ProductId", productId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@CurrencyId", currencyId, SqlDbType.Int),
					ParameterHelper.CreateParameter("@DiscountPrice", discountPrice, SqlDbType.Decimal)
				};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.InsertItem");
			new DataHandler().ExecuteCommandWithReturnValue("[lekmer].[pProductDiscountActionItemInsert]", parameters, dbSettings);
		}

		public virtual Collection<int> GetProductIds(int actionId)
		{
			var productIds = new Collection<int>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ActionId ", actionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("ProductDiscountActionRepository.GetProductIds");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[lekmer].[pProductDiscountActionGetProductIds]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}
	}
}
