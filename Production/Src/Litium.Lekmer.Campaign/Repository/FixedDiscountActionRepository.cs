﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using Litium.Framework.DataAccess;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign.Repository
{
	public class FixedDiscountActionRepository
	{
		protected virtual DataMapperBase<IFixedDiscountAction> CreateDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<IFixedDiscountAction>(dataReader);
		}

		protected virtual DataMapperBase<CurrencyValue> CreateCurrencyValueDataMapper(IDataReader dataReader)
		{
			return DataMapperResolver.Resolve<CurrencyValue>(dataReader);
		}


		public virtual void Save(IFixedDiscountAction action)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", action.Id, SqlDbType.Int),
					ParameterHelper.CreateParameter("IncludeAllProducts", action.IncludeAllProducts, SqlDbType.Bit)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.Save");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionSave]", parameters, dbSettings);
		}

		public virtual void Delete(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.Delete");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionDelete]", parameters, dbSettings);
		}

		public virtual IFixedDiscountAction GetById(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetById");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionGetById]", parameters, dbSettings))
			{
				return CreateDataMapper(dataReader).ReadRow();
			}
		}

		public virtual Collection<int> GetProductIds(int productActionId)
		{
			var productIds = new Collection<int>();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ActionId ", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetProductIds");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionGetProductIds]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// CurrencyValue.

		public virtual void SaveCurrencyValue(int productActionId, KeyValuePair<int, decimal> currencyValue)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CurrencyId", currencyValue.Key, SqlDbType.Int),
					ParameterHelper.CreateParameter("Value", currencyValue.Value, SqlDbType.Decimal)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.SaveCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionCurrencyInsert]", parameters, dbSettings);
		}

		public virtual void DeleteAllCurrencyValue(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.DeleteAllCurrencyValue");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionCurrencyDeleteAll]", parameters, dbSettings);
		}

		public virtual LekmerCurrencyValueDictionary GetCurrencyValuesByAction(int productActionId)
		{
			IEnumerable<CurrencyValue> currencyValues;

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetCurrencyValuesByAction");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionCurrencyGetByAction]", parameters, dbSettings))
			{
				currencyValues = CreateCurrencyValueDataMapper(dataReader).ReadMultipleRows();
			}

			var lekmerCurrencyValueDictionary = new LekmerCurrencyValueDictionary();

			foreach (var currencyValueRange in currencyValues)
			{
				lekmerCurrencyValueDictionary.Add(currencyValueRange.Currency.Id, currencyValueRange.MonetaryValue);
			}

			return lekmerCurrencyValueDictionary;
		}


		// Products.

		public virtual void InsertIncludeProduct(int productActionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.InsertIncludeProduct");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedDiscountActionIncludeProductInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeProduct(int productActionId, int productId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("ProductId", productId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.InsertExcludeProduct");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedDiscountActionExcludeProductInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeProducts(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.DeleteIncludeProducts");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionIncludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeProducts(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.DeleteExcludeProducts");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionExcludeProductDeleteAll]", parameters, dbSettings);
		}

		public virtual ProductIdDictionary GetIncludeProducts(int productActionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};
			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetIncludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionIncludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}

		public virtual ProductIdDictionary GetExcludeProducts(int productActionId)
		{
			var productIds = new ProductIdDictionary();

			IDataParameter[] parameters =
			{
				ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
			};
			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetExcludeProducts");
			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionExcludeProductGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					productIds.Add(dataReader.GetInt32(0));
				}
			}

			return productIds;
		}


		// Campaigns.

		public virtual void InsertIncludeCategory(int productActionId, int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.InsertIncludeCategory");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedDiscountActionIncludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeCategory(int productActionId, int categoryId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("CategoryId", categoryId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.InsertExcludeCategory");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedDiscountActionExcludeCategoryInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeCategories(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.DeleteIncludeCategories");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionIncludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeCategories(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.DeleteExcludeCategories");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionExcludeCategoryDeleteAll]", parameters, dbSettings);
		}

		public virtual CategoryIdDictionary GetIncludeCategories(int productActionId)
		{
			var categoryIds = new CategoryIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetIncludeCategories");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionIncludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categoryIds.Add(dataReader.GetInt32(0));
				}
			}

			return categoryIds;
		}

		public virtual CategoryIdDictionary GetExcludeCategories(int productActionId)
		{
			var categoryIds = new CategoryIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetExcludeCategories");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionExcludeCategoryGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					categoryIds.Add(dataReader.GetInt32(0));
				}
			}

			return categoryIds;
		}


		// Brands.

		public virtual void InsertIncludeBrand(int productActionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.InsertIncludeBrand");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedDiscountActionIncludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void InsertExcludeBrand(int productActionId, int brandId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int),
					ParameterHelper.CreateParameter("BrandId", brandId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.InsertExcludeBrand");

			new DataHandler().ExecuteCommandWithReturnValue("[campaignlek].[pFixedDiscountActionExcludeBrandInsert]", parameters, dbSettings);
		}

		public virtual void DeleteIncludeBrands(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.DeleteIncludeBrands");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionIncludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual void DeleteExcludeBrands(int productActionId)
		{
			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.DeleteExcludeBrands");

			new DataHandler().ExecuteCommand("[campaignlek].[pFixedDiscountActionExcludeBrandDeleteAll]", parameters, dbSettings);
		}

		public virtual BrandIdDictionary GetIncludeBrands(int productActionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetIncludeBrands");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionIncludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}

		public virtual BrandIdDictionary GetExcludeBrands(int productActionId)
		{
			var brandIds = new BrandIdDictionary();

			IDataParameter[] parameters =
				{
					ParameterHelper.CreateParameter("ProductActionId", productActionId, SqlDbType.Int)
				};

			var dbSettings = new DatabaseSetting("FixedDiscountActionRepository.GetExcludeBrands");

			using (IDataReader dataReader = new DataHandler().ExecuteSelect("[campaignlek].[pFixedDiscountActionExcludeBrandGetIdAll]", parameters, dbSettings))
			{
				while (dataReader.Read())
				{
					brandIds.Add(dataReader.GetInt32(0));
				}
			}

			return brandIds;
		}
	}
}