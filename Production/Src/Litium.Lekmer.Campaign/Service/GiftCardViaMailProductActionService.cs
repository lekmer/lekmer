﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class GiftCardViaMailProductActionService : IProductActionPluginService, IGiftCardViaMailProductActionService
	{
		protected GiftCardViaMailProductActionRepository Repository { get; private set; }
		protected ICampaignConfigService CampaignConfigService { get; private set; }

		public GiftCardViaMailProductActionService(GiftCardViaMailProductActionRepository giftCardViaMailProductActionRepository, ICampaignConfigService campaignConfigService)
		{
			Repository = giftCardViaMailProductActionRepository;
			CampaignConfigService = campaignConfigService;
		}


		public IProductAction GetById(IUserContext context, int productActionId)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(productActionId);
			if (action == null)
			{
				return null;
			}

			action.Amounts = GetCurrencyValues(productActionId);
			action.CampaignConfig = CampaignConfigService.GetById(action.CampaignConfigId);
			return action;
		}

		public virtual Collection<int> GetProductIds(int productActionId)
		{
			Repository.EnsureNotNull();
			return Repository.GetProductIds(productActionId);
		}


		protected virtual LekmerCurrencyValueDictionary GetCurrencyValues(int productActionId)
		{
			return Repository.GetCurrencyValuesByAction(productActionId);
		}
	}
}