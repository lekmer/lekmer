﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Order.Campaign;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using Litium.Scensum.Order.Campaign;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	public class GiftCardViaMailInfoService : IGiftCardViaMailInfoService
	{
		protected GiftCardViaMailInfoRepository Repository { get; private set; }
		protected ILekmerOrderCartCampaignService LekmerOrderCartCampaignService { get; private set; }
		protected ILekmerOrderProductCampaignService LekmerOrderProductCampaignService { get; private set; }
		protected IProductCampaignService ProductCampaignService { get; private set; }
		protected ICartCampaignService CartCampaignService { get; private set; }
		protected IProductService ProductService { get; private set; }

		public GiftCardViaMailInfoService(
			GiftCardViaMailInfoRepository giftCardViaMailInfoRepository,
			IOrderCartCampaignService orderCartCampaignService,
			IOrderProductCampaignService orderProductCampaignService,
			IProductCampaignService productCampaignService,
			ICartCampaignService cartCampaignService,
			IProductService productService)
		{
			Repository = giftCardViaMailInfoRepository;
			LekmerOrderCartCampaignService = (ILekmerOrderCartCampaignService) orderCartCampaignService;
			LekmerOrderProductCampaignService = (ILekmerOrderProductCampaignService) orderProductCampaignService;
			ProductCampaignService = productCampaignService;
			CartCampaignService = cartCampaignService;
			ProductService = productService;
		}


		public IGiftCardViaMailInfo Create(int orderId, int campaignId, int actionId, DateTime dateToSend, decimal discountValue)
		{
			var giftCardViaMailInfo = IoC.Resolve<IGiftCardViaMailInfo>();

			giftCardViaMailInfo.OrderId = orderId;
			giftCardViaMailInfo.CampaignId = campaignId;
			giftCardViaMailInfo.ActionId = actionId;
			giftCardViaMailInfo.DateToSend = dateToSend;
			giftCardViaMailInfo.DiscountValue = discountValue;
			giftCardViaMailInfo.StatusId = (int)GiftCardViaMailInfoStatus.ReadyToSend;

			giftCardViaMailInfo.Status = BusinessObjectStatus.New;

			return giftCardViaMailInfo;
		}

		public void Save(IGiftCardViaMailInfo giftCardViaMailInfo)
		{
			Repository.EnsureNotNull();

			Repository.Save(giftCardViaMailInfo);
		}

		public void Save(IUserContext context, IOrderFull order)
		{
			var giftCardViaMailInfos = GetGiftCardViaMailInfoCollection(context, order);

			foreach (var giftCardViaMailInfo in giftCardViaMailInfos)
			{
				Save(giftCardViaMailInfo);
			}
		}

		public void Update(IGiftCardViaMailInfo giftCardViaMailInfo)
		{
			Repository.EnsureNotNull();

			Repository.Update(giftCardViaMailInfo);
		}

		public Collection<IGiftCardViaMailInfo> GetAllToSend()
		{
			Repository.EnsureNotNull();

			return Repository.GetAllToSend();
		}

		public Collection<IGiftCardViaMailInfo> GetAllByOrder(int orderId)
		{
			Repository.EnsureNotNull();

			return Repository.GetAllByOrder(orderId);
		}


		protected virtual Collection<IGiftCardViaMailInfo> GetGiftCardViaMailInfoCollection(IUserContext context, IOrderFull order)
		{
			var giftCardViaMailInfos = new Collection<IGiftCardViaMailInfo>();

			giftCardViaMailInfos.AddRange(GetGiftCardInfoFromCartCampaigns(context, order));
			giftCardViaMailInfos.AddRange(GetGiftCardInfoFromProductCampaigns(context, order));

			return giftCardViaMailInfos;
		}

		protected virtual Collection<IGiftCardViaMailInfo> GetGiftCardInfoFromCartCampaigns(IUserContext context, IOrderFull order)
		{
			var giftCardViaMailInfos = new Collection<IGiftCardViaMailInfo>();

			var cartCampaigns = LekmerOrderCartCampaignService.GetAllByOrder(order.Id);
			if (cartCampaigns != null)
			{
				foreach (var campaign in cartCampaigns)
				{
					var cartCampaign = CartCampaignService.GetById(context, campaign.CampaignId);
					if (cartCampaign != null)
					{
						foreach (var cartAction in cartCampaign.Actions)
						{
							if (cartAction.ActionType.CommonName != "GiftCardViaMailCart")
							{
								continue;
							}

							var giftCardViaMailCartAction = (IGiftCardViaMailCartAction)cartAction;

							decimal discountValue;
							if (!giftCardViaMailCartAction.Amounts.TryGetValue(context.Channel.Currency.Id, out discountValue))
							{
								continue;
							}

							var giftCardViaMailInfo = Create(
								order.Id,
								campaign.CampaignId,
								cartAction.Id,
								DateTime.Now.AddDays(giftCardViaMailCartAction.SendingInterval),
								discountValue);

							giftCardViaMailInfos.Add(giftCardViaMailInfo);
						}
					}
				}
			}

			return giftCardViaMailInfos;
		}

		protected virtual Collection<IGiftCardViaMailInfo> GetGiftCardInfoFromProductCampaigns(IUserContext context, IOrderFull order)
		{
			var giftCardViaMailInfos = new Collection<IGiftCardViaMailInfo>();
			var campaignProductIds = new List<KeyValuePair<int, int>>();
			var appliedCampaigns = new Collection<int>();

			var orderItems = order.GetOrderItems();
			foreach (var orderItem in orderItems)
			{
				var productCampaigns = LekmerOrderProductCampaignService.GetAllByOrderItem(orderItem.Id);
				if (productCampaigns != null)
				{
					foreach (var productCampaign in productCampaigns)
					{
						if (!campaignProductIds.Contains(new KeyValuePair<int, int>(productCampaign.CampaignId, orderItem.ProductId)))
						{
							campaignProductIds.Add(new KeyValuePair<int, int>(productCampaign.CampaignId, orderItem.ProductId));
						}
					}
				}
			}

			foreach (var campaignProductId in campaignProductIds)
			{
				if (appliedCampaigns.Contains(campaignProductId.Key))
				{
					continue;
				}

				var productCampaign = ProductCampaignService.GetById(context, campaignProductId.Key);
				if (productCampaign != null)
				{
					foreach (var productAction in productCampaign.Actions)
					{
						if (productAction.ActionType.CommonName != "GiftCardViaMailProduct")
						{
							continue;
						}

						var product = ProductService.GetById(context, campaignProductId.Value);
						if (product == null)
						{
							continue;
						}

						var giftCardViaMailProductAction = (IGiftCardViaMailProductAction) productAction;
						if (!giftCardViaMailProductAction.Apply(context, product))
						{
							continue;
						}

						decimal discountValue;
						if (!giftCardViaMailProductAction.Amounts.TryGetValue(context.Channel.Currency.Id, out discountValue))
						{
							continue;
						}

						var giftCardViaMailInfo = Create(
							order.Id,
							campaignProductId.Key,
							productAction.Id,
							DateTime.Now.AddDays(giftCardViaMailProductAction.SendingInterval),
							discountValue);

						giftCardViaMailInfos.Add(giftCardViaMailInfo);

						appliedCampaigns.Add(campaignProductId.Key);
					}
				}
			}

			return giftCardViaMailInfos;
		}
	}
}