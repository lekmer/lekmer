﻿using System.Collections.ObjectModel;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class FixedDiscountActionService : IProductActionPluginService, IFixedDiscountActionService
	{
		protected FixedDiscountActionRepository Repository { get; private set; }

		public FixedDiscountActionService(FixedDiscountActionRepository fixedDiscountActionRepository)
		{
			Repository = fixedDiscountActionRepository;
		}


		public IProductAction GetById(IUserContext context, int productActionId)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(productActionId);

			if (action == null)
			{
				return null;
			}

			action.Amounts = GetCurrencyValues(productActionId);

			action.IncludeProducts = GetIncludeProducts(context, productActionId);
			action.ExcludeProducts = GetExcludeProducts(context, productActionId);
			action.IncludeCategories = GetIncludeCategories(productActionId);
			action.ExcludeCategories = GetExcludeCategories(productActionId);
			action.IncludeBrands = GetIncludeBrands(productActionId);
			action.ExcludeBrands = GetExcludeBrands(productActionId);

			return action;
		}

		public Collection<int> GetProductIds(int productActionId)
		{
			Repository.EnsureNotNull();
			return Repository.GetProductIds(productActionId);
		}


		protected virtual LekmerCurrencyValueDictionary GetCurrencyValues(int productActionId)
		{
			return Repository.GetCurrencyValuesByAction(productActionId);
		}


		protected virtual ProductIdDictionary GetIncludeProducts(IUserContext context, int productActionId)
		{
			return Repository.GetIncludeProducts(productActionId);
		}

		protected virtual ProductIdDictionary GetExcludeProducts(IUserContext context, int productActionId)
		{
			return Repository.GetExcludeProducts(productActionId);
		}


		protected virtual CategoryIdDictionary GetIncludeCategories(int productActionId)
		{
			return Repository.GetIncludeCategories(productActionId);
		}

		protected virtual CategoryIdDictionary GetExcludeCategories(int productActionId)
		{
			return Repository.GetExcludeCategories(productActionId);
		}


		protected virtual BrandIdDictionary GetIncludeBrands(int productActionId)
		{
			return Repository.GetIncludeBrands(productActionId);
		}

		protected virtual BrandIdDictionary GetExcludeBrands(int productActionId)
		{
			return Repository.GetExcludeBrands(productActionId);
		}
	}
}