﻿using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class CartItemsGroupValueConditionService : IConditionPluginService
	{
		protected CartItemsGroupValueConditionRepository Repository { get; private set; }
		protected ICampaignConfigService CampaignConfigService { get; private set; }

		public CartItemsGroupValueConditionService(CartItemsGroupValueConditionRepository cartItemsGroupValueConditionRepository, ICampaignConfigService campaignConfigService)
		{
			Repository = cartItemsGroupValueConditionRepository;
			CampaignConfigService = campaignConfigService;
		}


		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int conditionId).")]
		public virtual ICondition GetById(int conditionId)
		{
			throw new NotImplementedException();
		}

		public virtual ICondition GetById(IUserContext userContext, int conditionId)
		{
			Repository.EnsureNotNull();

			var condition = Repository.GetById(conditionId);
			if (condition == null)
			{
				return null;
			}

			condition.Amounts = Repository.GetCurrencyValuesByCondition(conditionId);
			condition.CampaignConfig = CampaignConfigService.GetById(condition.CampaignConfigId);
			return condition;
		}
	}
}