using System;
using Litium.Lekmer.Campaign.Repository;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;

namespace Litium.Lekmer.Campaign
{
	public class CartItemGroupFixedDiscountActionService : ICartActionPluginService
	{
		protected CartItemGroupFixedDiscountActionRepository Repository { get; private set; }

		public CartItemGroupFixedDiscountActionService(CartItemGroupFixedDiscountActionRepository cartItemGroupFixedDiscountActionRepository)
		{
			Repository = cartItemGroupFixedDiscountActionRepository;
		}

		[Obsolete("Obsolete since 2.1.2. Use method GetById(IUserContext userContext, int cartActionId).")]
		public virtual ICartAction GetById(int cartActionId)
		{
			throw new NotSupportedException();
		}

		public virtual ICartAction GetById(IUserContext userContext, int cartActionId)
		{
			Repository.EnsureNotNull();

			var action = Repository.GetById(cartActionId);
			if (action == null)
			{
				return null;
			}

			action.Amounts = Repository.GetCurrencyValuesByAction(cartActionId);
			action.IncludeProducts = GetIncludeProducts(cartActionId);
			action.ExcludeProducts = GetExcludeProducts(cartActionId);
			action.IncludeCategories = GetIncludeCategories(cartActionId);
			action.ExcludeCategories = GetExcludeCategories(cartActionId);
			action.IncludeBrands = GetIncludeBrands(cartActionId);
			action.ExcludeBrands = GetExcludeBrands(cartActionId);
			return action;
		}

		protected ProductIdDictionary GetIncludeProducts(int actionId)
		{
			return Repository.GetIncludeProducts(actionId);
		}

		protected ProductIdDictionary GetExcludeProducts(int actionId)
		{
			return Repository.GetExcludeProducts(actionId);
		}

		protected CampaignCategoryDictionary GetIncludeCategories(int actionId)
		{
			return Repository.GetIncludeCategoriesRecursive(actionId);
		}

		protected CampaignCategoryDictionary GetExcludeCategories(int actionId)
		{
			return Repository.GetExcludeCategoriesRecursive(actionId);
		}

		protected BrandIdDictionary GetIncludeBrands(int actionId)
		{
			return Repository.GetIncludeBrands(actionId);
		}

		protected BrandIdDictionary GetExcludeBrands(int actionId)
		{
			return Repository.GetExcludeBrands(actionId);
		}
	}
}