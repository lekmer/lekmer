﻿using System;
using Litium.Lekmer.Product;
using Litium.Scensum.Core;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CartDoesNotContainCondition : LekmerCondition, ICartDoesNotContainCondition
	{
		private int _campaignConfigId;
		private ICampaignConfig _campaignConfig;

		public int CampaignConfigId
		{
			get { return _campaignConfigId; }
			set
			{
				CheckChanged(_campaignConfigId, value);
				_campaignConfigId = value;
			}
		}
		public ICampaignConfig CampaignConfig
		{
			get { return _campaignConfig; }
			set
			{
				CheckChanged(_campaignConfig, value);
				_campaignConfig = value;
			}
		}

		public override bool Fulfilled(IUserContext context)
		{
			if (CampaignConfig == null)
			{
				throw new InvalidOperationException("The CampaignConfig property must be set.");
			}
			CampaignConfig.Validate();

			ICartFull cart = context.Cart;
			if (cart == null)
			{
				return false;
			}

			foreach (ICartItem item in cart.GetCartItems())
			{
				var product = (ILekmerProduct) item.Product;

				if (!IsTargetType(product)) continue; // true
				if (!IsRangeMember(product)) continue; // true

				return false;
			}

			return true;
		}

		protected virtual bool IsRangeMember(ILekmerProduct product)
		{
			return CampaignConfig.IsRangeMember(product.Id, product.CategoryId, product.BrandId);
		}

		protected override bool IsTargetType(IProduct product)
		{
			return CampaignConfig.IsTargetType(product.Id) || base.IsTargetType(product);
		}

		public override object[] GetInfoArguments()
		{
			return new object[0];
		}
	}
}