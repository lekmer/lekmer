using System;
using Litium.Lekmer.Order.Campaign;
using Litium.Scensum.Order.Campaign;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerOrderCampaignInfo : OrderCampaignInfo, ILekmerOrderCampaignInfo
	{
	}
}