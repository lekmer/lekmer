﻿using System;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerProductCampaign : ProductCampaign, ILekmerProductCampaign
	{
		private int _levelId;
		private bool _useLandingPage;
		private int? _priceTypeId;
		private int? _tagId;

		public int LevelId
		{
			get { return _levelId; }
			set
			{
				CheckChanged(_levelId, value);
				_levelId = value;
			}
		}

		public bool UseLandingPage
		{
			get { return _useLandingPage; }
			set
			{
				CheckChanged(_useLandingPage, value);
				_useLandingPage = value;
			}
		}

		public int? PriceTypeId
		{
			get { return _priceTypeId; }
			set
			{
				CheckChanged(_priceTypeId, value);
				_priceTypeId = value;
			}
		}

		public int? TagId
		{
			get { return _tagId; }
			set
			{
				CheckChanged(_tagId, value);
				_tagId = value;
			}
		}

		public override bool Apply(IUserContext context, IProduct product)
		{
			if (context == null) throw new ArgumentNullException("context");
			if (product == null) throw new ArgumentNullException("product");

			var productActionService = (ILekmerProductActionService) IoC.Resolve<IProductActionService>();
			var campaignInfo = (ILekmerProductCampaignInfo) product.CampaignInfo;

			bool productAffected = false;
			foreach (var action in Actions)
			{
				if (action.Apply(context, product))
				{
					productAffected = true;
					campaignInfo.CampaignActionsApplied.Add(productActionService.CreateProductActionAppliedItem().Cast(action));
				}
			}
			return productAffected;
		}
	}
}