﻿using System;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignToTag : ICampaignToTag
	{
		public int Id { get; set; }
		public int CampaignId { get; set; }
		public DateTime? ProcessingDate { get; set; }
	}
}