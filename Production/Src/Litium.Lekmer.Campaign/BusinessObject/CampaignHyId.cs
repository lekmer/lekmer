﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignHyId : ICampaignHyId
	{
		public string ErpId { get; set; }
		public ICampaignStatus CampaignStatus  { get; set; }
		public int CampaignStatusId  { get; set; }
		public string CampaignStatusTitle { get; set; }
		public string CampaignStatusCommonName { get; set; }
		public DateTime? EndDate  { get; set; }
		public bool Exclusive { get; set; }
		public int FolderId  { get; set; }
		public int Id { get; set; }
		public int Priority { get; set; }
		public DateTime? StartDate { get; set; }
		public string Title { get; set; }

	}
}
