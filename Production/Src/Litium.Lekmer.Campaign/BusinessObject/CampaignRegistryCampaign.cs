﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CampaignRegistryCampaign : BusinessObjectBase, ICampaignRegistryCampaign
	{
		private int _campaignRegistryId;
		public int CampaignRegistryId
		{
			get { return _campaignRegistryId; }
			set
			{
				CheckChanged(_campaignRegistryId, value);
				_campaignRegistryId = value;
			}
		}

		private int _campaignId;
		public int CampaignId
		{
			get { return _campaignId; }
			set
			{
				CheckChanged(_campaignId, value);
				_campaignId = value;
			}
		}

		private int? _channelId;
		public int? ChannelId
		{
			get { return _channelId; }
			set
			{
				CheckChanged(_channelId, value);
				_channelId = value;
			}
		}

		private string _countryIso;
		public string CountryIso {
			get { return _countryIso; }
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				CheckChanged(_countryIso, value);
				_countryIso = value;
			}
		}
	}
}