﻿using System;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class ConditionTargetProductType : BusinessObjectBase, IConditionTargetProductType
	{
		private int _ConditionId;
		private int _productTypeId;

		public int ConditionId
		{
			get { return _ConditionId; }
			set
			{
				CheckChanged(_ConditionId, value);
				_ConditionId = value;
			}
		}

		public int ProductTypeId
		{
			get { return _productTypeId; }
			set
			{
				CheckChanged(_productTypeId, value);
				_productTypeId = value;
			}
		}
	}
}