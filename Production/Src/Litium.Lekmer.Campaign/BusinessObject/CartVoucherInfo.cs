using System;
using Litium.Lekmer.Voucher;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CartVoucherInfo : ICartVoucherInfo
	{
		public string VoucherCode { get; set; }
		public VoucherValueType DiscountType { get; set; }
		/// <summary>
		/// Original discount value that comes from Voucher.
		/// </summary>
		public decimal? DiscountValue { get; set; }
		/// <summary>
		/// Discount amount, which was applied in cart.
		/// </summary>
		public decimal? AmountUsed { get; set; }
		/// <summary>
		/// Discount amount, with is still available after voucher was used in cart.
		/// </summary>
		public decimal? AmountLeft { get; set; }
	}
}