using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Litium.Lekmer.Campaign.Extensions;
using Litium.Lekmer.Common.Extensions;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Order;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Foundation.Utilities;
using Litium.Scensum.Order;
using Litium.Scensum.Product;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class CartItemDiscountCartAction : LekmerCartAction, ICartItemDiscountCartAction
	{
		private int _quantity;
		private bool _applyForCheapest;
		private bool _isPercentageDiscount;
		private decimal? _percentageDiscountAmount;
		private LekmerCurrencyValueDictionary _amounts;
		private bool _includeAllProducts;
		private ProductIdDictionary _includeProducts;
		private ProductIdDictionary _excludeProducts;
		private CampaignCategoryDictionary _includeCategories;
		private CampaignCategoryDictionary _excludeCategories;
		private BrandIdDictionary _includeBrands;
		private BrandIdDictionary _excludeBrands;
		
		public int Quantity
		{
			get { return _quantity; }
			set
			{
				CheckChanged(_quantity, value);
				_quantity = value;
			}
		}

		public bool ApplyForCheapest
		{
			get { return _applyForCheapest; }
			set
			{
				CheckChanged(_applyForCheapest, value);
				_applyForCheapest = value;
			}
		}

		public bool IsPercentageDiscount
		{
			get { return _isPercentageDiscount; }
			set
			{
				CheckChanged(_isPercentageDiscount, value);
				_isPercentageDiscount = value;
			}
		}

		public decimal? PercentageDiscountAmount
		{
			get { return _percentageDiscountAmount; }
			set
			{
				CheckChanged(_percentageDiscountAmount, value);
				_percentageDiscountAmount = value;
			}
		}

		public LekmerCurrencyValueDictionary Amounts
		{
			get { return _amounts; }
			set
			{
				CheckChanged(_amounts, value);
				_amounts = value;
			}
		}

		public bool IncludeAllProducts
		{
			get { return _includeAllProducts; }
			set
			{
				CheckChanged(_includeAllProducts, value);
				_includeAllProducts = value;
			}
		}

		public ProductIdDictionary IncludeProducts
		{
			get { return _includeProducts; }
			set
			{
				CheckChanged(_includeProducts, value);
				_includeProducts = value;
			}
		}

		public ProductIdDictionary ExcludeProducts
		{
			get { return _excludeProducts; }
			set
			{
				CheckChanged(_excludeProducts, value);
				_excludeProducts = value;
			}
		}

		public CampaignCategoryDictionary IncludeCategories
		{
			get { return _includeCategories; }
			set
			{
				CheckChanged(_includeCategories, value);
				_includeCategories = value;
			}
		}

		public CampaignCategoryDictionary ExcludeCategories
		{
			get { return _excludeCategories; }
			set
			{
				CheckChanged(_excludeCategories, value);
				_excludeCategories = value;
			}
		}

		public BrandIdDictionary IncludeBrands
		{
			get { return _includeBrands; }
			set
			{
				CheckChanged(_includeBrands, value);
				_includeBrands = value;
			}
		}

		public BrandIdDictionary ExcludeBrands
		{
			get { return _excludeBrands; }
			set
			{
				CheckChanged(_excludeBrands, value);
				_excludeBrands = value;
			}
		}

		public override object[] GetInfoArguments()
		{
			var infoArguments = new List<object>();

			if (IsPercentageDiscount)
			{
				if (PercentageDiscountAmount.HasValue)
				{
					infoArguments.Add(PercentageDiscountAmount.Value);
				}
				infoArguments.Add("%");
			}
			else
			{
				infoArguments.Add(Amounts.ElementAt(0).Value);
				infoArguments.Add(Amounts.ElementAt(0).Key);
			}

			infoArguments.Add(Quantity);

			return infoArguments.ToArray();
		}

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			decimal discountValue;
			if (!IsValid(context, out discountValue))
			{
				return false;
			}

			var singleCartItems = new Collection<ILekmerCartItem>();
			singleCartItems.SplitIntoSingleLekmerItems(cart.GetCartItems());

			var targetCartItem = FindTargetCartItem(singleCartItems);

			if (targetCartItem == null)
			{
				return false;
			}

			return PerformAction(cart, singleCartItems, targetCartItem, discountValue);
		}


		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1021:AvoidOutParameters", MessageId = "1#")]
		protected virtual bool IsValid(IUserContext context, out decimal value)
		{
			value = 0;

			if (IsPercentageDiscount)
			{
				if (PercentageDiscountAmount == null || PercentageDiscountAmount < 0)
				{
					return false;
				}
			}
			else
			{
				if (Amounts == null || !Amounts.TryGetValue(context.Channel.Currency.Id, out value))
				{
					return false;
				}
			}

			return true;
		}

		protected virtual ILekmerCartItem FindTargetCartItem(Collection<ILekmerCartItem> cartItems)
		{
			var targetCartItem = cartItems.FirstOrDefault(ci => !ci.IsAffectedByCampaign && IsTargetType(ci.Product) && IsInRange(ci.Product));

			if (targetCartItem == null)
			{
				return null;
			}

			if (ApplyForCheapest)
			{
				foreach (var item in cartItems)
				{
					if (!IsTargetType(item.Product)) continue;
					
					if (targetCartItem.Product.CampaignInfo.Price.IncludingVat > item.Product.CampaignInfo.Price.IncludingVat && 
						!item.IsAffectedByCampaign && IsInRange(item.Product))
					{
						targetCartItem = item;
					}
				}
			}
			else
			{
				foreach (var item in cartItems)
				{
					if (!IsTargetType(item.Product)) continue;

					if (targetCartItem.Product.CampaignInfo.Price.IncludingVat < item.Product.CampaignInfo.Price.IncludingVat &&
						!item.IsAffectedByCampaign && IsInRange(item.Product))
					{
						targetCartItem = item;
					}
				}
			}

			// Need to clone item to have fixed conditions
			return ObjectCopier.Clone(targetCartItem);
		}

		protected virtual bool PerformAction(ICartFull cart, Collection<ILekmerCartItem> itemsToApply, ILekmerCartItem targetCartItem, decimal discountValue)
		{
			var affectedItemsCount = 0;
			foreach (var item in itemsToApply)
			{
				if (affectedItemsCount < Quantity && Verify(item, targetCartItem))
				{
					SetCampaignPrice(item, discountValue);
					affectedItemsCount++;
				}
			}

			if (affectedItemsCount > 0)
			{
				cart.DeleteAllItems();
				foreach (var item in itemsToApply)
				{
					cart.AddItem(item);
				}
			}

			return affectedItemsCount > 0;
		}

		protected virtual bool Verify(ILekmerCartItem cartItem, ILekmerCartItem targetCartItem)
		{
			return !cartItem.IsAffectedByCampaign
				&& cartItem.Product.Id == targetCartItem.Product.Id
				&& cartItem.SizeId == targetCartItem.SizeId
				&& cartItem.Product.CampaignInfo.Price.IncludingVat == targetCartItem.Product.CampaignInfo.Price.IncludingVat;
		}

		protected virtual void SetCampaignPrice(ICartItem item, decimal discountValue)
		{
			Price price = item.Product.CampaignInfo.Price;

			decimal priceIncludingVat;

			if (IsPercentageDiscount)
			{
				if (PercentageDiscountAmount.HasValue)
				{
					priceIncludingVat = (price.IncludingVat * (1 - PercentageDiscountAmount.Value / 100)).Round();
				}
				else
				{
					priceIncludingVat = price.IncludingVat;
				}
			}
			else
			{
				priceIncludingVat = (price.IncludingVat - discountValue).Round();
			}

			if (priceIncludingVat < 0)
			{
				priceIncludingVat = 0;
			}

			var priceExcludingVat = (priceIncludingVat / (1 + item.Product.Price.VatPercentage / 100)).Round();

			item.Product.CampaignInfo.Price = new Price(priceIncludingVat, priceExcludingVat);
		}

		protected virtual bool IsInRange(IProduct product)
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");
			if (ExcludeProducts == null) throw new InvalidOperationException("The ExcludeProducts property must be set.");
			if (IncludeCategories == null) throw new InvalidOperationException("The IncludeCategories property must be set.");
			if (ExcludeCategories == null) throw new InvalidOperationException("The ExcludeCategories property must be set.");
			if (IncludeBrands == null) throw new InvalidOperationException("The IncludeBrands property must be set.");
			if (ExcludeBrands == null) throw new InvalidOperationException("The ExcludeBrands property must be set.");

			var lekmerProduct = (ILekmerProduct) product;
			return IsIncluded(lekmerProduct) && !IsExcluded(lekmerProduct);
		}

		protected virtual bool IsIncluded(ILekmerProduct product)
		{
			return IncludeAllProducts ||
			       IncludeCategories.ContainsKey(product.CategoryId) ||
			       (product.BrandId.HasValue && IncludeBrands.ContainsKey(product.BrandId.Value)) ||
			       IncludeProducts.ContainsKey(product.Id);
		}

		protected virtual bool IsExcluded(ILekmerProduct product)
		{
			return ExcludeCategories.ContainsKey(product.CategoryId) ||
			       (product.BrandId.HasValue && ExcludeBrands.ContainsKey(product.BrandId.Value)) ||
			       ExcludeProducts.ContainsKey(product.Id);
		}

		protected override bool IsTargetType(IProduct product)
		{
			if (IncludeProducts == null) throw new InvalidOperationException("The IncludeProducts property must be set.");

			// When product resides in "IncludeProducts" list explicitly, we ignore target options and return true !!!
			if (IncludeAllProducts == false)
			{
				if (IncludeProducts.ContainsKey(product.Id))
				{
					return true;
				}
			}

			return base.IsTargetType(product);
		}
	}
}