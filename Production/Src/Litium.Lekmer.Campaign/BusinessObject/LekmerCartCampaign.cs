﻿using System;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerCartCampaign : Scensum.Campaign.CartCampaign, ILekmerCartCampaign
	{
		private int _levelId;
		private bool _useLandingPage;
		private int? _priceTypeId;
		private int? _tagId;

		public int LevelId
		{
			get { return _levelId; }
			set
			{
				CheckChanged(_levelId, value);
				_levelId = value;
			}
		}

		public bool UseLandingPage
		{
			get { return _useLandingPage; }
			set
			{
				CheckChanged(_useLandingPage, value);
				_useLandingPage = value;
			}
		}

		public int? PriceTypeId
		{
			get { return _priceTypeId; }
			set
			{
				CheckChanged(_priceTypeId, value);
				_priceTypeId = value;
			}
		}

		public int? TagId
		{
			get { return _tagId; }
			set
			{
				CheckChanged(_tagId, value);
				_tagId = value;
			}
		}
	}
}