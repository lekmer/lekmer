﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Voucher;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Campaign
{
	[Serializable]
	public class LekmerFreightValueAction : FreightValueAction, ILekmerFreightValueAction
	{
		private IdDictionary _deliveryMethodIds;
		private IdDictionary _targetProductTypes;
		private LekmerCurrencyValueDictionary _amounts;

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public IdDictionary DeliveryMethodIds
		{
			get { return _deliveryMethodIds; }
			set
			{
				CheckChanged(_deliveryMethodIds, value);
				_deliveryMethodIds = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public IdDictionary TargetProductTypes
		{
			get { return _targetProductTypes; }
			set
			{
				CheckChanged(_targetProductTypes, value);
				_targetProductTypes = value;
			}
		}

		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
		public new LekmerCurrencyValueDictionary Amounts
		{
			get { return _amounts; }
			set
			{
				CheckChanged(_amounts, value);
				_amounts = value;
			}
		}

		public override bool Apply(IUserContext context, ICartFull cart)
		{
			decimal discountValue;
			if (!Amounts.TryGetValue(context.Channel.Currency.Id, out discountValue))
			{
				return false;
			}

			if (DeliveryMethodIds == null || DeliveryMethodIds.Count == 0)
			{
				return false;
			}

			var campaignInfo = cart.CampaignInfo as ILekmerCartCampaignInfo;
			if (campaignInfo == null)
			{
				return false;
			}

			bool applied = false;

			foreach (KeyValuePair<int, int> pair in DeliveryMethodIds)
			{
				int deliveryMethodId = pair.Key;

				if (campaignInfo.CampaignFreightCostDictionary.ContainsKey(deliveryMethodId) == false)
				{
					campaignInfo.CampaignFreightCostDictionary.Add(deliveryMethodId, discountValue);
					applied = true;
				}
				else if (discountValue < campaignInfo.CampaignFreightCostDictionary[deliveryMethodId])
				{
					campaignInfo.CampaignFreightCostDictionary[deliveryMethodId] = discountValue;
					applied = true;
				}
			}

			return applied;
		}

		public virtual Price ApplyDiscount(Price priceSummary, ICurrency currency)
		{
			throw new NotImplementedException();
		}

		public override object[] GetInfoArguments()
		{
			if (Amounts == null || Amounts.Count == 0)
			{
				throw new InvalidOperationException("Info arguments can not be created, " + "since the Amounts list is null or empty.");
			}

			return new object[]
				{
					Amounts.ElementAt(0).Value,
					Amounts.ElementAt(0).Key
				};
		}
	}
}