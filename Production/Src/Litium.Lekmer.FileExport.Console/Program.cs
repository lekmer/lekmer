﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Reflection;
using Litium.Lekmer.FileExport.Exporter;
using Litium.Scensum.Foundation;
using log4net;

namespace Litium.Lekmer.FileExport
{
	internal class Program
	{
		private static readonly ILog _log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

		private static void Main()
		{
			try
			{
				_log.InfoFormat("FileExport.Console started.");
				Stopwatch stopwatch = Stopwatch.StartNew();

				ExecuteExporters();

				_log.InfoFormat("FileExport.Console completed.");
				stopwatch.Stop();
				_log.InfoFormat("FileExport.Console elapsed time - {0}", stopwatch.Elapsed);
			}
			catch(Exception ex)
			{
				_log.Error("FileExport.Console failed.", ex);
				throw;
			}
		}

		private static void ExecuteExporters()
		{
			var exporters = GetExporters();

			foreach (IExporter exporter in exporters)
			{
				try
				{
					exporter.Execute();
				}
				catch (Exception ex)
				{
					_log.Error("Exporter execution failed.", ex);
				}
			}
		}

		private static IEnumerable<IExporter> GetExporters()
		{
			var exporters = new Collection<IExporter>();
			
			var sitemapExporter = IoC.Resolve<IExporter>("SitemapExporter");
			exporters.Add(sitemapExporter);

			var productInfoExporter = IoC.Resolve<IExporter>("ProductInfoExporter");
			exporters.Add(productInfoExporter);
			
			var keybrokerProductInfoExporter = IoC.Resolve<IExporter>("KeybrokerProductInfoExporter");
			exporters.Add(keybrokerProductInfoExporter);
			
			var marinProductInfoExporter = IoC.Resolve<IExporter>("MarinProductInfoExporter");
			exporters.Add(marinProductInfoExporter);
			
			return exporters;
		}
	}
}