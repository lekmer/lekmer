﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Scensum.Core;

namespace Litium.Lekmer.SiteStructure
{
	public interface IContentPageUrlHistorySecureService
	{
		Collection<IContentPageUrlHistory> GetAllByContentPage(int contentPageId);

		IContentPageUrlHistory Create(int contentPageId, string urlTitle, int typeId);

		int Save(ISystemUserFull systemUserFull, IContentPageUrlHistory contentPageUrlHistory);

		List<int> Save(ISystemUserFull systemUserFull, Collection<IContentPageUrlHistory> contentPageUrlHistoryCollection);

		void DeleteById(ISystemUserFull systemUserFull, int contentPageUrlHistoryId);

		void DeleteAllByContentPage(ISystemUserFull systemUserFull, int contentPageId);

		void DeleteAllByContentPageAndUrlTitle(ISystemUserFull systemUserFull, int contentPageId, string urlTitle);
	}
}