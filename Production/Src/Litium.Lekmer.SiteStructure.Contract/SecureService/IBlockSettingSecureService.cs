﻿namespace Litium.Lekmer.SiteStructure
{
	public interface IBlockSettingSecureService
	{
		IBlockSetting Create();
		void Save(IBlockSetting blockSetting);
		void Delete(int blockId);
	}
}