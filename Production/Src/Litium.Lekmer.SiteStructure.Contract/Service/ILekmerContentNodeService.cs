﻿using Litium.Scensum.Core;
using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure
{
	public interface ILekmerContentNodeService : IContentNodeService
	{
		ITreeItemsHash GetAllAsTreeItemsHash(IUserContext context);
	}
}
