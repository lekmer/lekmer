﻿using Litium.Scensum.SiteStructure;

namespace Litium.Lekmer.SiteStructure
{
	public interface ITreeItemsHash
	{
		void Initialize(IContentNodeTree contentNodeTree);
		bool ContainsKey(string pageUrlTitle);
		IContentNodeTreeItem FirstOrDefault(string pageUrlTitle);
	}
}
