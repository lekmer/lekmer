using System;

namespace Litium.Lekmer.Voucher
{
	public interface IVoucherService
	{
		IVoucherCheckResult VoucherCheck(string code, string siteApplicationName);
		void VoucherConsume(string code, int orderId, decimal usedAmount, string siteApplicationName);
		void VoucherRelease(string code);
		void VoucherReserve(string code);

		// Gift card via email.
		int GiftCardVoucherBatchIdGetByChannelCampaignDiscountValue(decimal discountValue, string appName, string createdBy, out int quantity);
		int VoucherCountGetByVoucherInfo(int voucherInfoId);
		int VoucherBatchCreate(DateTime validFrom, string prefix, string suffix, int quantity, 
			decimal discountValue, string appName, int nbrOfToken, string createdBy);
		string VoucherGenerateCodes(int voucherInfoId, int nbrOfVouchers, DateTime validTo);
	}
}