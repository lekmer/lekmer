SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE procedure [product].[pVariationGroupSetStatus]
	@VariationGroupId int,
	@VariationGroupStatusId int
as
begin
	set nocount off	
	
	update
		[product].[tVariationGroup]
	set
		[VariationGroupStatusId] = @VariationGroupStatusId
	where
		[VariationGroupId] = @VariationGroupId
end







GO
