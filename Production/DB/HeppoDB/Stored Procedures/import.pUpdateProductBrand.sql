
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [import].[pUpdateProductBrand]

AS
BEGIN

	begin try
	begin transaction

	
		UPDATE 
			lp
		SET 
			lp.BrandId = b.BrandId		
		FROM 
			[import].tProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = tp.HYErpId
			inner join lekmer.tBrand b
					on b.ErpId = tp.BrandId					
		WHERE 
			tp.ChannelId = 1
			AND 
			((lp.BrandId <> b.BrandId) or lp.BrandId is null)

	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
END
GO
