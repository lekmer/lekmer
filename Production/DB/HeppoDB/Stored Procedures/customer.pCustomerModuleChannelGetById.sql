SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerModuleChannelGetById]
	@ChannelId	int
as
begin
	select 
		*
	from 
		[customer].[vCustomCustomerModuleChannel]
	where 
		[CustomerModuleChannel.ChannelId] = @ChannelId
end

GO
