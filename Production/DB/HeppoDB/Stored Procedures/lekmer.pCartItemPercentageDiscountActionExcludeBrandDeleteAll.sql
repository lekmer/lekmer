SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemPercentageDiscountActionExcludeBrand]
	WHERE
		CartActionId = @CartActionId
END



GO
