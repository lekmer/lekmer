SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pRecommendedPriceGetAllByProduct]
	@ProductId	INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vRecommendedPrice]
	WHERE
		[ProductId] = @ProductId
END 

GO
