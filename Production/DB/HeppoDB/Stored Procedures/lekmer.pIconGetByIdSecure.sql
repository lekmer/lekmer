SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pIconGetByIdSecure]
	@IconId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		i.*
	FROM
		[lekmer].[vIconSecure] i
	WHERE
		[i].[Icon.Id] = @IconId
END
GO
