SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentPageGetById]
@ChannelId	int,
@ContentNodeId	int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentPage]
	where
		[ContentNode.ContentNodeId] = @ContentNodeId
		AND [ContentNode.ChannelId] = @ChannelId
end

GO
