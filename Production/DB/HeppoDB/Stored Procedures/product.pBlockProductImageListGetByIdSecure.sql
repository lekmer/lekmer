SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockProductImageListGetByIdSecure]
 @BlockId		int
as
begin
	select 
		bpil.*,
		b.*
	from 
		[product].[vCustomBlockProductImageList] as bpil
		inner join [sitestructure].[vCustomBlockSecure] as b on bpil.[BlockProductImageList.BlockId] = b.[Block.BlockId]
	where
		bpil.[BlockProductImageList.BlockId] = @BlockId
end

GO
