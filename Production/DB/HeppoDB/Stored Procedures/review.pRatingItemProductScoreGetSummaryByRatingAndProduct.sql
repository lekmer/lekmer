SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreGetSummaryByRatingAndProduct]
	@ChannelId INT,
	@RatingId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		ri.[RatingItem.RatingId] 'RatingItemProductScoreSummary.RatingId',
		ri.[RatingItem.RatingItemId] 'RatingItemProductScoreSummary.RatingItemId',
		ri.[RatingItem.Score] 'RatingItemProductScoreSummary.RatingItemScore',
		ISNULL(a0.[TotalHitCount], 0) 'RatingItemProductScoreSummary.TotalHitCount'
	FROM 
		[review].[vRatingItemSecure] ri
		OUTER APPLY
		(
			SELECT
				SUM(ps.[RatingItemProductScore.HitCount]) 'TotalHitCount'
			FROM
				[review].[vRatingItemProductScore] ps
			WHERE
				ps.[RatingItemProductScore.ChannelId] = @ChannelId
				AND ps.[RatingItemProductScore.RatingId] = ri.[RatingItem.RatingId]
				AND ps.[RatingItemProductScore.RatingItemId] = ri.[RatingItem.RatingItemId]
				AND ps.[RatingItemProductScore.ProductId] = @ProductId
		) a0
	WHERE
		ri.[RatingItem.RatingId] = @RatingId
END
GO
