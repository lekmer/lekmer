SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportNewBrandsByDate]
	@From		datetime,
	@To		datetime	

AS
begin
	set nocount on		
	begin try
		
		-- @gör ValidTo i tid 00:00:00  sen lägg till 23h 59min och 59 sec  i sekunder
		set @From = replace(cast(convert(varchar, @From, 102) as varchar), '.', '-') + ' 00:00:00'
		set @To = replace(cast(convert(varchar, @To, 102) as varchar), '.', '-') + ' 00:00:00'
		set @To = DATEADD(SS,86399,@To)
		
		select 
			b.BrandId,
			b.ErpId,
			b.Title,
			i.CreatedDate
		from 
			lekmer.tBrand b
			inner join integration.[tBrandCreation] i
				on b.BrandId = i.BrandId
		where
			(@From is null or i.CreatedDate > @From)
			and
			(@To is null or i.CreatedDate < @To)

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		--print ERROR_MESSAGE() + ' ' + GETDATE() + ' ' + ERROR_PROCEDURE()
	end catch		 
end
GO
