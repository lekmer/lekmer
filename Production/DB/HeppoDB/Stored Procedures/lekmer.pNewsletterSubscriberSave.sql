
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberSave]
	@ChannelId INT,
	@Email VARCHAR(320),
	@SubscriberTypeId INT,
	@CreatedDate DATETIME,
	@UpdateDate DATETIME
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @Id INT
	
	UPDATE
		[lekmer].[tNewsletterSubscriber]
	SET
		[SubscriberTypeId] = @SubscriberTypeId,
		[UpdatedDate] = @UpdateDate,
		@Id = [SubscriberId]
	WHERE
		[Email] = @Email AND
		[ChannelId] = @ChannelId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tNewsletterSubscriber]
		( 
			[ChannelId],
			[Email],
			[SubscriberTypeId],
			[CreatedDate],
			[UpdatedDate]
		)
		VALUES
		(
			@ChannelId,
			@Email,
			@SubscriberTypeId,
			@CreatedDate,
			@UpdateDate
		)
		
		SET @Id = SCOPE_IDENTITY()
	END
	
	RETURN @Id
END
GO
