
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pAvailGetValidData] 
AS
BEGIN
	SET NOCOUNT ON;

	SELECT	p.ProductId
	FROM	product.tProduct p
	WHERE	p.ProductStatusId = 0 AND p.NumberInStock > 0
		AND NOT EXISTS ( SELECT 1 FROM lekmer.tProductSize ps WHERE ps.ProductId = p.ProductId )
		
	UNION ALL
		
	SELECT	p.ProductId
	FROM	product.tProduct p
	WHERE	p.ProductStatusId = 0
		AND EXISTS ( SELECT 1 FROM lekmer.tProductSize ps WHERE ps.ProductId = p.ProductId AND ps.NumberInStock > 0 )
			
END
GO
