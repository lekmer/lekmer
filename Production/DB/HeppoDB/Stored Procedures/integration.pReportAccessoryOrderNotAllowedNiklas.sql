SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportAccessoryOrderNotAllowedNiklas]
	@From		datetime = NULL,
	@To			datetime = NULL	

AS
begin
	set nocount on
		
	begin try
		
		set @From = coalesce(@From, '2009-08-12 00:00:00.000')
		set @To = coalesce(@To, '2099-08-12 00:00:00.000')
		
		set @From = replace(cast(convert(varchar, @From, 102) as varchar), '.', '-') + ' 00:00:00'
		set @To = replace(cast(convert(varchar, @To, 102) as varchar), '.', '-') + ' 00:00:00'
		set @To = DATEADD(SS,86399,@To)
		
				--SELECT convert(varchar, getdate(), 110)
		select distinct
			o.OrderId,
			--convert(varchar, o.CreatedDate, 110) as CreatedDate,
			o.CreatedDate,
			o.Email,
			c.CommonName as Channel
		from 
			[order].tOrder o
			inner join [order].tOrderItem oi
				on oi.OrderId = o.OrderId
			inner join [order].tOrderItemProduct oip
				on oip.OrderItemId = oi.OrderItemId
			inner join product.tProduct p
				on p.ProductId = oip.ProductId
			inner join core.tChannel c
				on c.ChannelId = o.ChannelId
			inner join product.tCategory pc
				on pc.CategoryId = p.CategoryId
			inner join Voucher.product.tVoucherLog v
				on v.OrderId = o.OrderId
			inner join Voucher.product.tVoucher vo
				on vo.VoucherId = v.VoucherId
			inner join Voucher.product.tVoucherInfo vi
				on vi.VoucherInfoId = vo.VoucherInfoId
		where
		o.OrderStatusId = 4 
		--and o.OrderId in 
					--(select OrderId
						--from Voucher.product.tVoucherLog
						--where VoucherId in
										--(select VoucherId 
											--from Voucher.product.tVoucher
											--where VoucherInfoId = 268
											--or VoucherInfoId = 279
											--or VoucherInfoId = 280))
		and (vi.VoucherInfoId = 312 or vi.VoucherInfoId = 313 or vi.VoucherInfoId = 315
									or vi.VoucherInfoId = 314)
		and o.CreatedDate > @From
		and o.CreatedDate < @To
		and oi.Quantity = 1
		and p.CategoryId in
							(select categoryid from product.tCategory where parentCategoryid in
							(select categoryId from product.tCategory where parentCategoryid in
								(select CategoryId from product.tCategory where Title = 'Skotillbehör')))
		order by
			c.CommonName
			
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		--print ERROR_MESSAGE() + ' ' + GETDATE() + ' ' + ERROR_PROCEDURE()
	end catch		 
end
GO
