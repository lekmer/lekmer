SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockContactUsGetByIdSecure]
@BlockId INT
AS	
BEGIN
	SET NOCOUNT ON
	SELECT
		bc.*,
		b.*
	FROM
		[lekmer].[vBlockContactUs] AS bc
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON bc.[BlockContactUs.BlockId] = b.[Block.BlockId]
	WHERE
		bc.[BlockContactUs.BlockId] = @BlockId
END

GO
