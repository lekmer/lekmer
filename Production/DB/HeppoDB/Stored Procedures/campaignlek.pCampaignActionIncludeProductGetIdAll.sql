SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeProductGetIdAll]
	@ChannelId	INT,
	@CustomerId	INT,
	@ConfigId	INT
AS
BEGIN
	SELECT
		P.[Product.Id]
	FROM
		[campaignlek].[tCampaignActionIncludeProduct] caep
		INNER JOIN [product].[vCustomProduct] p ON p.[Product.Id] = caep.[ProductId]
		INNER JOIN [product].[vCustomPriceListItem] AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		caep.[ConfigId] = @ConfigId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
