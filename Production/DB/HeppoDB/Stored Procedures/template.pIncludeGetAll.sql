SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Roman G.		Date: 12/30/2008		Time: 14:48:25
Description: Created 

*****************  Version 2  *****************
User: Yuriy P.		Date: 23/01/2009
Description: Added Path 

*****************  Version 3  *****************
User: Egorov V.		Date: 26.01.2009
Description: Remove Path
*/

CREATE PROCEDURE [template].[pIncludeGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[template].[vCustomInclude]
END

GO
