SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartItemPriceActionExcludeCategoryGetIdAll]
	@CartActionId int
AS
BEGIN
	SELECT 
		c.CategoryId
	FROM 
		[addon].tCartItemPriceActionExcludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.CartActionId = @CartActionId
END
GO
