SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeProductInsert]
	@ConfigId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tCampaignActionIncludeProduct] (
		ConfigId,
		ProductId
	)
	VALUES (
		@ConfigId,
		@ProductId
	)
END
GO
