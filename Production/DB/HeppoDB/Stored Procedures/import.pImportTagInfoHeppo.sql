SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [import].[pImportTagInfoHeppo]
	
AS
BEGIN
	DECLARE 
		@TagGroup_Id INT,
		@TagGroup_CommonName VARCHAR(100),
		@TagGroup_Title NVARCHAR(100),
		@Tag_Id INT,
		@Tag_Value NVARCHAR(255),
		@Alias_ValueSE NVARCHAR(MAX),
		@Alias_ValueDK NVARCHAR(MAX),
		@Alias_ValueNO NVARCHAR(MAX),
		@Alias_ValueFI NVARCHAR(MAX),
		@Alias_ValueNL NVARCHAR(MAX),
		@Alias_ValueFR NVARCHAR(MAX),
		@Alias_CommonName VARCHAR(100),
		@Alias_Id INT
		
	DECLARE
		@LanguageSEId INT,
		@LanguageDKId INT,
		@LanguageNOId INT,
		@LanguageFIId INT,
		@LanguageNLId INT,
		@LanguageFRId INT
	
	SELECT @LanguageSEId = LanguageId FROM core.tLanguage WHERE ISO = 'SV'
	SELECT @LanguageDKId = LanguageId FROM core.tLanguage WHERE ISO = 'DA'
	SELECT @LanguageNOId = LanguageId FROM core.tLanguage WHERE ISO = 'NO'
	SELECT @LanguageFIId = LanguageId FROM core.tLanguage WHERE ISO = 'FI'
	SELECT @LanguageNLId = LanguageId FROM core.tLanguage WHERE ISO = 'NL'
	SELECT @LanguageFRId = LanguageId FROM core.tLanguage WHERE ISO = 'FR'
	
	DECLARE cur_tagInfo CURSOR FAST_FORWARD FOR
		SELECT DISTINCT
			[TagGroup.Id],
			[TagGroup.CommonName],
			[TagGroup.Title],
			[Tag.Id],
			[Tag.Value],
			[Alias.ValueSE],
			[Alias.ValueDK],
			[Alias.ValueNO],
			[Alias.ValueFI],
			[Alias.ValueNL],
			[Alias.ValueFR],
			[Alias.CommonName],
			[Alias.ID]
		FROM
			[import].[tTagInfoHeppo]
		
	OPEN cur_tagInfo
	FETCH NEXT FROM cur_tagInfo
		INTO 
			@TagGroup_Id,
			@TagGroup_CommonName,
			@TagGroup_Title,
			@Tag_Id,
			@Tag_Value,
			@Alias_ValueSE,
			@Alias_ValueDK,
			@Alias_ValueNO,
			@Alias_ValueFI,
			@Alias_ValueNL,
			@Alias_ValueFR,
			@Alias_CommonName,
			@Alias_Id
		
	WHILE @@FETCH_STATUS = 0
		BEGIN
			-- Update TagGroup Title
			UPDATE 
				lekmer.tTagGroup 
			SET 
				Title = @TagGroup_Title
			WHERE 
				TagGroupId = @TagGroup_Id
				AND CommonName = @TagGroup_CommonName
				AND Title <> @TagGroup_Title
				AND @TagGroup_Title IS NOT NULL
				AND @TagGroup_Title <> ''


			-- Update Tag Value
			UPDATE 
				lekmer.tTag
			SET 
				Value = @Tag_Value
			WHERE 
				TagId = @Tag_Id
				AND Value <> @Tag_Value
				AND @Tag_Value IS NOT NULL
				AND @Tag_Value <> ''


			-- Update/Insert Alias Translations
			if (@Alias_Id IS NULL OR @Alias_Id = '')
			BEGIN
				SELECT @Alias_Id = AliasId FROM template.tAlias WHERE CommonName = @Alias_CommonName
			END
			
			-- SE
			IF EXISTS(SELECT * FROM template.tAliasTranslation WHERE AliasId = @Alias_Id AND LanguageId = @LanguageSEId)
				BEGIN
					UPDATE 
						template.tAliasTranslation
					SET
						Value = @Alias_ValueSE
					WHERE 
						AliasId = @Alias_Id 
						AND LanguageId = @LanguageSEId
						AND @Alias_ValueSE IS NOT NULL
						AND @Alias_ValueSE <> ''
				END
			ELSE
				BEGIN
					INSERT INTO template.tAliasTranslation
					(
						AliasId,
						LanguageId,
						Value
					)
					VALUES
					(
						@Alias_Id,
						@LanguageSEId,
						@Alias_ValueSE
					)
				END
				
			-- DK
			IF EXISTS(SELECT * FROM template.tAliasTranslation WHERE AliasId = @Alias_Id AND LanguageId = @LanguageDKId)
				BEGIN
					UPDATE 
						template.tAliasTranslation
					SET
						Value = @Alias_ValueDK
					WHERE 
						AliasId = @Alias_Id 
						AND LanguageId = @LanguageDKId
						AND @Alias_ValueDK IS NOT NULL
						AND @Alias_ValueDK <> ''
				END
			ELSE
				BEGIN
					INSERT INTO template.tAliasTranslation
					(
						AliasId,
						LanguageId,
						Value
					)
					VALUES
					(
						@Alias_Id,
						@LanguageDKId,
						@Alias_ValueDK
					)
				END
				
			-- NO
			IF EXISTS(SELECT * FROM template.tAliasTranslation WHERE AliasId = @Alias_Id AND LanguageId = @LanguageNOId)
				BEGIN
					UPDATE 
						template.tAliasTranslation
					SET
						Value = @Alias_ValueNO
					WHERE 
						AliasId = @Alias_Id 
						AND LanguageId = @LanguageNOId
						AND @Alias_ValueNO IS NOT NULL
						AND @Alias_ValueNO <> ''
				END
			ELSE
				BEGIN
					INSERT INTO template.tAliasTranslation
					(
						AliasId,
						LanguageId,
						Value
					)
					VALUES
					(
						@Alias_Id,
						@LanguageNOId,
						@Alias_ValueNO
					)
				END
			
			-- FI
			IF EXISTS(SELECT * FROM template.tAliasTranslation WHERE AliasId = @Alias_Id AND LanguageId = @LanguageFIId)
				BEGIN
					UPDATE 
						template.tAliasTranslation
					SET
						Value = @Alias_ValueFI
					WHERE 
						AliasId = @Alias_Id 
						AND LanguageId = @LanguageFIId
						AND @Alias_ValueFI IS NOT NULL
						AND @Alias_ValueFI <> ''
				END
			ELSE
				BEGIN
					INSERT INTO template.tAliasTranslation
					(
						AliasId,
						LanguageId,
						Value
					)
					VALUES
					(
						@Alias_Id,
						@LanguageFIId,
						@Alias_ValueFI
					)
				END
				
			-- NL
			IF EXISTS(SELECT * FROM template.tAliasTranslation WHERE AliasId = @Alias_Id AND LanguageId = @LanguageNLId)
				BEGIN
					UPDATE 
						template.tAliasTranslation
					SET
						Value = @Alias_ValueNL
					WHERE 
						AliasId = @Alias_Id 
						AND LanguageId = @LanguageNLId
						AND @Alias_ValueNL IS NOT NULL
						AND @Alias_ValueNL <> ''
				END
			ELSE
				BEGIN
					INSERT INTO template.tAliasTranslation
					(
						AliasId,
						LanguageId,
						Value
					)
					VALUES
					(
						@Alias_Id,
						@LanguageNLId,
						@Alias_ValueNL
					)
				END
				
			-- FR
			IF EXISTS(SELECT * FROM template.tAliasTranslation WHERE AliasId = @Alias_Id AND LanguageId = @LanguageFRId)
				BEGIN
					UPDATE 
						template.tAliasTranslation
					SET
						Value = @Alias_ValueFR
					WHERE 
						AliasId = @Alias_Id 
						AND LanguageId = @LanguageFRId
						AND @Alias_ValueFR IS NOT NULL
						AND @Alias_ValueFR <> ''
				END
			ELSE
				BEGIN
					INSERT INTO template.tAliasTranslation
					(
						AliasId,
						LanguageId,
						Value
					)
					VALUES
					(
						@Alias_Id,
						@LanguageFRId,
						@Alias_ValueFR
					)
				END
				
			FETCH NEXT FROM cur_tagInfo
				INTO 
					@TagGroup_Id,
					@TagGroup_CommonName,
					@TagGroup_Title,
					@Tag_Id,
					@Tag_Value,
					@Alias_ValueSE,
					@Alias_ValueDK,
					@Alias_ValueNO,
					@Alias_ValueFI,
					@Alias_ValueNL,
					@Alias_ValueFR,
					@Alias_CommonName,
					@Alias_Id
		END
	CLOSE cur_tagInfo
	DEALLOCATE cur_tagInfo	   	              
END
GO
