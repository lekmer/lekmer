SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeCategoryDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[campaignlek].[tCartItemDiscountCartActionIncludeCategory]
	WHERE
		CartActionId = @CartActionId
END

GO
