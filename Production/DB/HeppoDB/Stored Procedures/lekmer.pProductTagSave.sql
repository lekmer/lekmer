SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTagSave]
	@ProductId	int,
	@TagId		int
AS
BEGIN 
	INSERT INTO lekmer.tProductTag
	(
		ProductId,
		TagId
	)
	VALUES
	(
		@ProductId,
		@TagId
	)
END 
GO
