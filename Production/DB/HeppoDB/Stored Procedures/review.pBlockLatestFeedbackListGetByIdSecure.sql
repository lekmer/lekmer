SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockLatestFeedbackListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		blfl.*,
		b.*
	FROM 
		[review].[vBlockLatestFeedbackList] AS blfl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON blfl.[BlockLatestFeedbackList.BlockId] = b.[Block.BlockId]
	WHERE
		blfl.[BlockLatestFeedbackList.BlockId] = @BlockId
END
GO
