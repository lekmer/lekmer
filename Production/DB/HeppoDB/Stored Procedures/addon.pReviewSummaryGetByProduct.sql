SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [addon].[pReviewSummaryGetByProduct]
         @ChannelId         int,
         @ProductId int
AS
BEGIN
         SELECT
                   avg(cast(r.[Review.Rating] AS decimal)) AS 'AverageRating',
                   count(r.[Review.ProductId]) AS 'ReviewCount'
         FROM 
                   addon.vCustomReview r
         WHERE 
                   r.[Review.ProductId] = @ProductId
                   AND r.[Review.ChannelId] = @ChannelId
                   AND r.[Review.ReviewStatusId] = 2 /*Approved status id*/
END

GO
