SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [template].[pAliasTranslationSave]
	@AliasId	INT,
	@LanguageId	INT,
	@Value NVARCHAR(MAX)
AS
begin
	set nocount on
	
	update
		[template].[tAliasTranslation]
	set
		[Value] = @Value	
	where
		[AliasId] = @AliasId AND
		[LanguageId] = @LanguageId
	if  @@ROWCOUNT = 0
	begin		
		insert into [template].[tAliasTranslation]
		(
			[AliasId],
			[LanguageId],
			[Value]				
		)
		values
		(
			@AliasId,
			@LanguageId,
			@Value
		)
	end
end	
GO
