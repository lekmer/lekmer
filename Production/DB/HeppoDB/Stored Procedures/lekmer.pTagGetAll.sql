
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pTagGetAll]
	@LanguageId INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		t.*
	FROM 
		lekmer.[vTag] t
	WHERE
		t.[Tag.LanguageId] = @LanguageId
END
GO
