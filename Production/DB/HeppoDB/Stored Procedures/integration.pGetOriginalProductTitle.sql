SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGetOriginalProductTitle]
	@Channel 	int
AS
BEGIN
				
		select t.ProductId, t.Title, t.WebShopTitle, t.LanguageId, l.HYErpId, pu.UrlTitle 
		from
			(select
				p.ProductId,
				coalesce(pt.Title, p.Title) as Title,
				coalesce(pt.WebShopTitle, p.WebShopTitle) as WebShopTitle,
				(select LanguageId from core.tChannel where ChannelId = @Channel) as LanguageId
			from
				product.tProduct p
				left outer join product.tProductTranslation pt
					on p.ProductId = pt.ProductId
					and pt.LanguageId = (select LanguageId from core.tChannel where ChannelId = @Channel)) t
					
		inner join lekmer.tLekmerProduct l
			on t.ProductId = l.ProductId
	
		left outer join lekmer.tProductUrl pu
			on t.ProductId = pu.ProductId
			and t.LanguageId = pu.LanguageId
		
END
GO
