SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [customer].[pCustomerCommentSave]
@CustomerCommentId	int,
@CustomerId			int,
@CreationDate		smalldatetime,
@Comment			nvarchar(250),
@AuthorId			int
AS	
BEGIN
	SET NOCOUNT ON
	
	--Check if customer exist
	IF NOT EXISTS ( 
					SELECT 
						1 
					FROM 
						[customer].[tCustomer] 
					WHERE 
						[tCustomer].[CustomerId] = @CustomerId 
					)
		RETURN -1
		
	UPDATE  [customer].[tCustomerComment] 
	SET 		
		Comment = @Comment
	WHERE
		CustomerCommentId = @CustomerCommentId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [customer].[tCustomerComment] 
		(
			CustomerId,
			CreationDate,
			Comment,
			AuthorId
		)
		VALUES
		(
			@CustomerId,
			@CreationDate,
			@Comment,
			@AuthorId
		)
		SET	@CustomerCommentId = SCOPE_IDENTITY()
	END

	RETURN @CustomerCommentId
END





GO
