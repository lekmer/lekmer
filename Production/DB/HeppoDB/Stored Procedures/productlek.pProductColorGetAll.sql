SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductColorGetAll]
AS
BEGIN
	SELECT 
		[ColorId] AS 'ProductColor.ColorId' ,
		[ProductId] AS 'ProductColor.ProductId'
	FROM [productlek].[tProductColor]
END
GO
