SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductStatusGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[product].[vCustomProductStatus]
	ORDER BY
		[ProductStatus.Title]
END

GO
