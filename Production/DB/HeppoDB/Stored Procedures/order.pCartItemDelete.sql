
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [order].[pCartItemDelete]
	@CartItemId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE FROM
		[order].tCartItem
	WHERE
		CartItemId = @CartItemId
END
GO
