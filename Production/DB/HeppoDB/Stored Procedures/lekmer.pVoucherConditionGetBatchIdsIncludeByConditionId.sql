SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionGetBatchIdsIncludeByConditionId]
	@ConditionId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		BatchId
	FROM
		lekmer.[tVoucherBatchesInclude]
	WHERE
		[ConditionId] = @ConditionId
END
GO
