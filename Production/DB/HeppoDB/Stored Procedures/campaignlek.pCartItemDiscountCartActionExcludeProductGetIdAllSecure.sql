SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeProductGetIdAllSecure]
	@CartActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tCartItemDiscountCartActionExcludeProduct] c
		INNER JOIN [product].[tProduct] p ON c.[ProductId] = p.[ProductId]
	WHERE
		c.[CartActionId] = @CartActionId
		AND p.[IsDeleted] = 0
END

GO
