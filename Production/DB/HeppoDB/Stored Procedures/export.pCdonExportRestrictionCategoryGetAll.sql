
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionCategoryGetAll]
AS
BEGIN
	SELECT 
		DISTINCT [src].[CategoryId] 'ItemId',
		[rc].[ProductRegistryId] 'ProductRegistryId',
		[rc].[Reason] 'Reason',
		[rc].[UserId] 'UserId',
		[rc].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportRestrictionCategory] rc
		 CROSS APPLY [product].[fnGetSubCategories] ([rc].[CategoryId]) src
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
END
GO
