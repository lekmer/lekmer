SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntitySave]
	@EntityId INT,
	@CommonName NVARCHAR(50),
	@Title NVARCHAR(50)
AS 
BEGIN
    SET NOCOUNT ON
    
    UPDATE  
		[template].[tEntity]
    SET 
		[CommonName] = @CommonName,
		[Title] = @Title
    WHERE
		[EntityId] = @EntityId
    
	IF @@ROWCOUNT = 0 
    BEGIN
        INSERT  [template].[tEntity] 
        (
			[CommonName],
			[Title]
		) 
		VALUES 
		( 
			@CommonName,
			@Title
		)
        SET  @EntityId = SCOPE_IDENTITY()
    END
    RETURN @EntityId
        
END
GO
