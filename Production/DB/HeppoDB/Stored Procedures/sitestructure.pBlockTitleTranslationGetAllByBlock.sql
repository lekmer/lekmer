SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockTitleTranslationGetAllByBlock]
@BlockId	int
AS
BEGIN
	set nocount on

	SELECT 
		[BlockTranslation.BlockId] AS 'Id',
		[BlockTranslation.LanguageId] AS 'LanguageId',
		[BlockTranslation.Title] AS 'Value'
	FROM
		[sitestructure].vCustomBlockTranslation
	WHERE 
		[BlockTranslation.BlockId] = @BlockId
END

GO
