SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductMeasurementTranslationGetAllByProduct]
@ProductId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[Product.Measurement] AS 'Value'
	FROM
	    lekmer.vLekmerProductTranslation where [Product.Id] = @ProductId

END




GO
