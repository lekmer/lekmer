SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageUpdate]
AS
BEGIN
	DECLARE @PackageId INT
	DECLARE @MinNumberInStock INT

	DECLARE cur_package CURSOR FAST_FORWARD FOR
	SELECT [PackageId] FROM [productlek].[tPackage]

	OPEN cur_package

	FETCH NEXT FROM cur_package 
	INTO @PackageId

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SET @MinNumberInStock = (SELECT
									 MIN(COALESCE(p.NumberInStock, a1.[NumberInStock]))
								 FROM [product].[tProduct] p
								 CROSS APPLY (SELECT SUM(ps.[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE [ps].[ProductId] = [p].[ProductId]) a1
								 WHERE [p].[ProductId] IN (SELECT [pp].[ProductId] FROM [productlek].[tPackageProduct] pp WHERE [pp].[PackageId] = @PackageId))
		
		UPDATE p
		SET [p].[NumberInStock] = @MinNumberInStock
		FROM [product].[tProduct] p
			 INNER JOIN [productlek].[tPackage] pac ON [pac].[MasterProductId] = [p].[ProductId]
		WHERE [pac].[PackageId] = @PackageId

		FETCH NEXT FROM cur_package INTO @PackageId
	END

	CLOSE cur_package
	DEALLOCATE cur_package
END
GO
