SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




-- =============================================
-- Author:		Anders Brandtsfridh
-- Create date: 2010-05-20
-- Description:	
-- =============================================
CREATE PROCEDURE [lekmer].[pBrandGetById]
@BrandId	int,
@ChannelId	int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	     *
	FROM
	    lekmer.vBrand 
	WHERE 
		[Brand.BrandId] = @BrandId AND 
		ChannelId = @ChannelId
END



GO
