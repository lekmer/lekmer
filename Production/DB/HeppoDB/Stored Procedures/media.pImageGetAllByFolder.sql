SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [media].[pImageGetAllByFolder]
	@MediaFolderId  INT,
	@Page			int = NULL,
	@PageSize		int,
	@SortBy			varchar(20) = NULL,
	@SortDescending	bit = NULL
AS
BEGIN
	SET NOCOUNT ON; 

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

    DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)
	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY I.[' 
			+ COALESCE(@SortBy, 'Media.Title')
			+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
	       I.*    
	   	    FROM [media].[vCustomImageSecure] AS I
			WHERE I.[Media.FolderId] = @MediaFolderId'

	SET @sql = 
		'SELECT * FROM
		(' + @sqlFragment + '
		)
		AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
	WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'
	EXEC sp_executesql @sqlCount,
		N'	@MediaFolderId             INT',	
			@MediaFolderId
			     
	EXEC sp_executesql @sql, 
			N'	@MediaFolderId               INT',	
			@MediaFolderId
END

GO
