SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentRegionGetAllByTemplate]
	@TemplateId INT
AS 
BEGIN
    SET NOCOUNT ON

    SELECT  MFR.*
    FROM    [template].[vCustomModelFragmentRegion] MFR
            INNER JOIN [template].[tTemplate] AS T ON T.ModelId = MFR.[ModelFragmentRegion.ModelId]
    WHERE   T.[TemplateId] = @TemplateId
    ORDER BY MFR.[ModelFragmentRegion.Ordinal]
END

GO
