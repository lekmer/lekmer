
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateProductSeoShoesAccessories]
	@FormulaTitle		NVARCHAR(500),
	@FormulaDescription	NVARCHAR(700),
	@ChannelId			INT
AS
begin
	set nocount on
	begin try
		begin transaction				
				
		------------------------------------------------------------
		-- <Produktnivå Accessoarer skåvård>		
		------------------------------------------------------------
		DECLARE @NewTitle NVARCHAR(500)
		DECLARE @NewDescription NVARCHAR(700)
		DECLARE @SiteStructureRegistryId INT
		DECLARE @LanguageId int
		
		SET @LanguageId = (select LanguageId from core.tChannel where ChannelId = @ChannelId)
		
		INSERT INTO product.tProductSeoSettingTranslation(ProductId, LanguageId)
		SELECT
			p.ProductId,
			@LanguageId
		FROM
			product.tProduct p
		WHERE
			not exists (SELECT 1
							FROM product.tProductSeoSettingTranslation n
							WHERE n.ProductId = p.ProductId and
							n.LanguageId = @LanguageId)
							
															

		UPDATE
			pss
		SET
			@NewTitle = @FormulaTitle,
			@NewTitle = REPLACE(@NewTitle, '[BrandTitle]', b.Title),
			@NewTitle = REPLACE(@NewTitle, '[ProductTitle]', p.Title),
			@NewTitle = REPLACE(@NewTitle, '[CategoryLevelOne]', coalesce(ct.Title, c.Title)),
			
			pss.Title = @NewTitle,
						
			@NewDescription	= @FormulaDescription,
			@NewDescription = REPLACE(@NewDescription, '[BrandTitle]', b.Title),
			@NewDescription = REPLACE(@NewDescription, '[ProductTitle]', p.Title),
			@NewDescription = REPLACE(@NewDescription, '[CategoryLevelOne]', coalesce(ct.Title, c.Title)),

			pss.[Description] = @NewDescription
		FROM 
			product.tProductSeoSettingTranslation pss
				inner join lekmer.tLekmerProduct l
					on pss.ProductId = l.ProductId
				inner join product.tProduct p
					on p.ProductId = l.ProductId
				inner join lekmer.tBrand b
					on b.BrandId = l.BrandId
				---------------------------------	
				inner join product.tCategory c 
					on c.CategoryId = p.CategoryId
				inner join product.tCategory c2
					on c.ParentCategoryId = c2.CategoryId
				inner join product.tCategory c3
					on c2.ParentCategoryId = c3.CategoryId
				---------------------------------
				inner join product.tCategoryTranslation ct
				on c.CategoryId = ct.CategoryId
				
				inner join product.tCategoryTranslation ct2
				on c2.CategoryId = ct2.CategoryId
				
				left join lekmer.tProductTag pt
					on pt.ProductId = p.ProductId
				left join lekmer.tTag t
					on t.TagId = pt.TagId
				inner join lekmer.tTagTranslation tt
					on t.TagId = tt.TagId
		WHERE
			c3.CategoryId = 1000071 -- skotillbehör		
			and t.TagGroupId != 48 -- Strumpor storlekar
			and pss.LanguageId = @LanguageId
			and tt.LanguageId = @LanguageId
			and ct.LanguageId = @LanguageId
			and ct2.LanguageId = @LanguageId
			and
			((pss.Title is null or pss.Title = '')
				or (pss.[Description] is null or pss.[Description] = ''))


	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
