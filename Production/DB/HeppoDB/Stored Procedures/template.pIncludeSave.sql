SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pIncludeSave]
	@IncludeId			int,
	@IncludeFolderId	int,	
	@Description		nvarchar(50),
	@CommonName			varchar(50),
	@Content			nvarchar(max)
	
as
begin
	set nocount ON
				
	update
		[template].[tInclude]
	set
		[IncludeFolderId] = @IncludeFolderId,
		[Description] = @Description,
		[CommonName] = @CommonName,
		[Content] = @Content			
	where
		[IncludeId] = @IncludeId
		
	if  @@ROWCOUNT = 0
	begin
		insert into [template].[tInclude]
		(
			[IncludeFolderId],
			[Description],
			[CommonName],
			[Content]				
		)
		values
		(
			@IncludeFolderId,
			@Description,
			@CommonName,
			@Content
		)
		set @IncludeId = scope_identity()
	END
	
	return @IncludeId
end

GO
