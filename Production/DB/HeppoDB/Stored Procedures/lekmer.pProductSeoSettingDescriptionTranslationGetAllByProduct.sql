SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pProductSeoSettingDescriptionTranslationGetAllByProduct]
	@ProductId int
AS
BEGIN
	set nocount on

	SELECT 
		[Product.Id] AS 'Id',
		[Language.Id] AS 'LanguageId',
		[ProductSeoSetting.Description] AS 'Value'
	FROM
		lekmer.[vProductSeoSettingTranslation]
	WHERE 
		[Product.Id] = @ProductId
END

GO
