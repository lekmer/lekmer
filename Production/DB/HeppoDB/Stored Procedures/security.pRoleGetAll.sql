SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pRoleGetAll]
as
begin
	set nocount on

	select
		*
	from
		[security].[vCustomRole]
end

GO
