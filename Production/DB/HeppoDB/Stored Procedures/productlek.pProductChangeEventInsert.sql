
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pProductChangeEventInsert]
	@ProductId INT,
	@EventStatusId INT,
	@CdonExportEventStatusId INT,
	@CreatedDate DATETIME,
	@ActionAppliedDate DATETIME = NULL,
	@Reference NVARCHAR(100)
AS
BEGIN
	SET NOCOUNT ON

	INSERT [productlek].[tProductChangeEvent]
	(
		[ProductId],
		[EventStatusId],
		[CdonExportEventStatusId],
		[CreatedDate],
		[ActionAppliedDate],
		[Reference]
	)
	VALUES
	(
		@ProductId,
		@EventStatusId,
		@CdonExportEventStatusId,
		@CreatedDate,
		@ActionAppliedDate,
		@Reference
	)

	RETURN SCOPE_IDENTITY()
END
GO
