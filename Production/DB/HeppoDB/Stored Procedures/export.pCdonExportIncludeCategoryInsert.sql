SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportIncludeCategoryInsert]
	@ProductChannelIds	VARCHAR(MAX),
	@CategoryId			INT,
	@Reason				NVARCHAR(255),
	@UserId				INT,
	@CreatedDate		DATETIME,
	@Delimiter			CHAR(1)
AS
BEGIN
	INSERT [export].[tCdonExportIncludeCategory] (
		[ProductRegistryId],
		[CategoryId],
		[Reason],
		[UserId],
		[CreatedDate]
	)
	SELECT 
		[pmc].[ProductRegistryId],
		@CategoryId,
		@Reason,
		@UserId,
		@CreatedDate
	FROM [generic].[fnConvertIDListToTable](@ProductChannelIds, @Delimiter) c
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ChannelId] = [c].[ID]
END
GO
