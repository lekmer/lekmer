SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yura P.	Date: 10.02.2009
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pContentPageTypeGetAll]
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomContentPageType]
end

GO
