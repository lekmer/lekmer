SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pFreeFreightFlagActionCurrencyGetAllByActionId]
	@ActionId	int
as
begin
	SELECT
		*
	FROM
		[lekmer].[vFreeFreightFlagActionCurrency]
	WHERE 
		[FreeFreightFlagActionCurrency.ActionId] = @ActionId
end

GO
