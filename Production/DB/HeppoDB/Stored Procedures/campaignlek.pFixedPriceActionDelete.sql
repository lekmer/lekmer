SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionDelete]
	@ProductActionId INT
AS 
BEGIN
	EXEC [campaignlek].[pFixedPriceActionIncludeBrandDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedPriceActionExcludeBrandDeleteAll] @ProductActionId

	EXEC [campaignlek].[pFixedPriceActionIncludeCategoryDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedPriceActionExcludeCategoryDeleteAll] @ProductActionId

	EXEC [campaignlek].[pFixedPriceActionIncludeProductDeleteAll] @ProductActionId
	EXEC [campaignlek].[pFixedPriceActionExcludeProductDeleteAll] @ProductActionId
	
	EXEC [campaignlek].[pFixedPriceActionCurrencyDeleteAll] @ProductActionId

	DELETE FROM [campaignlek].[tFixedPriceAction]
	WHERE ProductActionId = @ProductActionId
END

GO
