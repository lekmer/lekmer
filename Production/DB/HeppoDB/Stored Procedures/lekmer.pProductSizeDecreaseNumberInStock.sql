
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSizeDecreaseNumberInStock]
	@ProductId INT,
	@SizeId INT,
	@Quantity INT
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @NewNumberInStock INT
	
	-- Update ProductSize NumberInSock
	UPDATE
		[lekmer].[tProductSize]
	SET
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0 
								ELSE [NumberInStock] - @Quantity END)
	WHERE
		[ProductId] = @ProductId
		AND [SizeId] = @SizeId
	
	SET @NewNumberInStock = (SELECT SUM([NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
	EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @ProductId, @NewNumberInStock
END
GO
