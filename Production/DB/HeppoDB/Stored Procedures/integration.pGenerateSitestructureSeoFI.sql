SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pGenerateSitestructureSeoFI]

AS
begin
	set nocount on
	begin try
		begin transaction				
				
			------------------------------------------------------------
		-- <Underkategori>
		-- Title>
		-- Formel: Underkategori Huvudkategori & Skor online på nätet från Heppo.se
		-- Exempel: Stövlar Damskor & Skor online på nätet från Heppo.se 


		
		-- Meta Desc>
		-- Formel:	Heppo.se din skobutik på internet - Köp underkategori och huvudkategori på nätet. 
		-- Fri frakt på skor, även fri retur. 30 dagars öppet köp när du köper underkategori och huvudkategori från Heppo.

		-- Exempel
		-- Heppo.se din skobutik på internet - Köp stövlar och damskor på nätet. Fri frakt på skor, även fri retur. 
		-- 30 dagars öppet köp när du köper på stövlar och damskor online från Heppo.  
		------------------------------------------------------------
		
		
		-- DEFAULT --
		-- Insert new contentnodeIds in tContentPageSeoSetting
		insert into sitestructure.tContentPageSeoSetting(ContentNodeId)
		select
			n.ContentNodeId
		from 
			sitestructure.tContentNode n --3
		where 
			n.SiteStructureRegistryId = 4
			and n.ContentNodeTypeId = 3 -- detta är contentpages
			and n.ContentNodeId not in 
								(
									select ContentNodeId
									from sitestructure.tContentPageSeoSetting
								)
								
								
									
		declare @LanguageId int
		set @LanguageId = 1000003 -- Finland
		
		--INSERT INTO sitestructure.tContentPageSeoSettingTranslation(ContentNodeId, LanguageId)
		--SELECT
		--	c.ContentNodeId,
		--	@languageId
		--FROM
		--	sitestructure.tContentPage c
		--WHERE
		--	not exists (SELECT 1
		--					FROM sitestructure.tContentPageSeoSettingTranslation n
		--					WHERE n.ContentNodeId = c.ContentNodeId and
		--					n.LanguageId = @LanguageId)
							
							
							
		UPDATE
			cps
		SET
			cps.Title = cn.Title  + ' ' + cn3.Title + ' ' + ' &  Kengät netistä Heppo.fi:stä',
			
			cps.[Description] = 'Heppo.fi kenkäkauppa netissä - Osta ' + cn.Title + ' ja ' + cn3.Title +
			'  netistä. Ilmainen toimitus ja ilmainen palautus. 30 päivän vaihto-oikeus ja palautusoikeus ostaessasi ' + cn.Title + ' ja ' + cn3.Title +
			'  Hepposta.'
			--select cps.title, cn.title, cn2.title, cn3.title, cn.contentnodeid, cps.languageid
		FROM
				--sitestructure.tContentPageSeoSettingTranslation cps
				sitestructure.tContentPageSeoSetting cps
				--inner join sitestructure.tContentPage cp
					--on cp.ContentNodeId = cps.ContentNodeId	
				------
				inner join sitestructure.tContentNode cn
					on cn.ContentNodeId = cps.ContentNodeId				
				inner join sitestructure.tContentNode cn2
				on cn.ParentContentNodeId = cn2.ContentNodeId
				inner join sitestructure.tContentNode cn3
				on cn.ParentContentNodeId = cn2.ContentNodeId
				------						
		WHERE
			cn.SiteStructureRegistryId = 1000001
			and ((cn3.ContentNodeId = 1003371 and cn2.ContentNodeId = 1002778) -- damskor, kategorier
					or (cn3.ContentNodeId = 1003372 and cn2.ContentNodeId = 1002839) -- herrskor, kategorier
					or (cn3.ContentNodeId = 1003863 and cn2.ContentNodeId = 1003874))
			--and LanguageId = @LanguageId	
			and
			((cps.Title is null or cps.Title = '')
				or (cps.[Description] is null or cps.[Description] = ''))

		--rollback
	commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
