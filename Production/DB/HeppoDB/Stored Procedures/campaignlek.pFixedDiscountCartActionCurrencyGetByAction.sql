SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountCartActionCurrencyGetByAction]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedDiscountCartActionCurrency]
	WHERE 
		[FixedDiscountCartActionCurrency.CartActionId] = @CartActionId
END
GO
