SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 2  *****************
User: Artymyshyn M.		Date: 13.02.2009		Time: 16:15
Description:
			Remove:
			TemplateId,
			TypePath					
*/

/*
*****************  Version 1  *****************
User: Victor E.		Date: 18.11.2008		Time: 13:40
Description:
			Rename:
					ModuleName on Name
					ModuleCN on CommonName
					ModuleTypePath on TypePath					
*/



CREATE PROCEDURE [template].[pComponentSave]
	@ComponentId			INT,
	@Title				NVARCHAR(50),
	@CommonName			VARCHAR(50)
AS
BEGIN
	IF EXISTS(
				SELECT 
					1 
				FROM 
					template.tComponent 
				WHERE 
					CommonName = @CommonName 
					AND ComponentId <> @ComponentId
		)
		return -1
		
	UPDATE
		template.tComponent
	SET				
		[Title]				= @Title,
		CommonName			= @CommonName
	WHERE
		ComponentId = @ComponentId
		
	IF @@ROWCOUNT = 0
		BEGIN
			INSERT INTO
				template.tComponent
				([Title], CommonName)
			VALUES
				(@Title, @CommonName)
				
			SET @ComponentId = SCOPE_IDENTITY()
		END	

	RETURN @ComponentId
END
	





GO
