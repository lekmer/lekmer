SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendGetByIdSecure]
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vBlockEsalesRecommendSecure] AS ber
	WHERE
		ber.[Block.BlockId] = @BlockId
END
GO
