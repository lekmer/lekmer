SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductGetIdByUrlTitle]
	@LanguageId	int,
	@UrlTitle nvarchar(256)
AS 
BEGIN 
	SELECT 
		[ProductUrl.ProductId]
	FROM 
		[lekmer].[vProductUrl]
	WHERE
		[ProductUrl.LanguageId] = @LanguageId AND
		[ProductUrl.UrlTitle] = @UrlTitle
END
GO
