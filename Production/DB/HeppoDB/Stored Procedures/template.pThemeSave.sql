SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pThemeSave]
@ThemeId int,
@Title nvarchar(50)
AS
BEGIN
	IF EXISTS (SELECT 1 FROM template.tTheme WHERE Title = @Title and ThemeId != @ThemeId)
		RETURN -1
	
	IF EXISTS (SELECT 1 FROM template.tTheme WHERE ThemeId = @ThemeId)
		BEGIN
			UPDATE template.tTheme
			SET Title = @Title
			WHERE ThemeId = @ThemeId
		END
	ELSE
		BEGIN
			INSERT INTO template.tTheme
			VALUES (@Title)
			SET @ThemeId = SCOPE_IDENTITY()
		END
	RETURN @ThemeId
END













GO
