SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreInsert]
	@ChannelId INT,
	@ProductId INT,
	@RatingId INT,
	@RatingItemId INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[review].[tRatingItemProductScore]
	SET
		HitCount = HitCount + 1
	WHERE
		ChannelId = @ChannelId
		AND
		ProductId = @ProductId
		AND
		RatingId = @RatingId
		AND
		RatingItemId = @RatingItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [review].[tRatingItemProductScore] (
			[ChannelId],
			[ProductId],
			[RatingId],
			[RatingItemId],
			[HitCount]
		)
		VALUES (
			@ChannelId,
			@ProductId,
			@RatingId,
			@RatingItemId,
			1
		)
	END
END
GO
