SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherConditionDeleteBatchesAll]
	@ConditionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE FROM lekmer.tVoucherBatchesInclude
	WHERE ConditionId = @ConditionId
	
	DELETE FROM lekmer.tVoucherBatchesExclude
	WHERE ConditionId = @ConditionId
END
GO
