SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionExcludeBrandGetIdAll]
	@ConditionId	INT
AS
BEGIN
	SELECT 
		ceb.[BrandId]
	FROM 
		[addon].[tCartContainsConditionExcludeBrand] ceb 
	WHERE 
		ceb.[ConditionId] = @ConditionId 
END
GO
