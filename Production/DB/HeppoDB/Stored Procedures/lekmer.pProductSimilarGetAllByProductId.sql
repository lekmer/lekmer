SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductSimilarGetAllByProductId]
	@ProductId	INT
AS
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vCustomProductSimilar]
	WHERE
		[ProductSimilar.ProductId] = @ProductId
	ORDER BY [ProductSimilar.Score] ASC
END 
GO
