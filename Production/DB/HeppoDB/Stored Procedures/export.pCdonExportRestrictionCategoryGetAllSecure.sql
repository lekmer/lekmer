SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pCdonExportRestrictionCategoryGetAllSecure]
AS
BEGIN
	SELECT 
		[rc].[CategoryId] 'ItemId',
		[rc].[ProductRegistryId] 'ProductRegistryId',
		[rc].[Reason] 'Reason',
		[rc].[UserId] 'UserId',
		[rc].[CreatedDate] 'CreatedDate',
		[pmc].[ChannelId] 'ChannelId'
	FROM [export].[tCdonExportRestrictionCategory] rc
		 INNER JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [rc].[ProductRegistryId]
END
GO
