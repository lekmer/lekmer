SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewUserGetByToken]
	@Token VARCHAR(50)
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewUser] rru
	WHERE
		rru.[RatingReviewUser.Token] = @Token
END
GO
