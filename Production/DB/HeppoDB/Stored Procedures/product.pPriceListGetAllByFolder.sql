SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pPriceListGetAllByFolder]
	@PriceListFolderId int
AS
BEGIN
	SELECT
		pl.*
	FROM
		[product].[vCustomPriceList] pl
	WHERE
		pl.[PriceList.FolderId] = @PriceListFolderId
END

GO
