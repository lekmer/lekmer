SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingFolderGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rf.*
	FROM
		[review].[vRatingFolder] rf
END
GO
