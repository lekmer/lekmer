SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionExcludeCategoryInsert]
	@CartActionId INT,
	@CategoryId INT,
	@IncludeSubcategories BIT
AS
BEGIN
	INSERT [campaignlek].[tCartItemDiscountCartActionExcludeCategory] (
		CartActionId,
		CategoryId,
		IncludeSubcategories
	)
	VALUES (
		@CartActionId,
		@CategoryId,
		@IncludeSubcategories
	)
END

GO
