SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockProductLatestFeedbackListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		bplfl.*,
		b.*
	FROM 
		[review].[vBlockProductLatestFeedbackList] AS bplfl
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON bplfl.[BlockProductLatestFeedbackList.BlockId] = b.[Block.BlockId]
	WHERE
		bplfl.[BlockProductLatestFeedbackList.BlockId] = @BlockId
END
GO
