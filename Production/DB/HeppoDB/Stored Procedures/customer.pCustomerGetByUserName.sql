SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGetByUserName]
	@UserName varchar(50),
	@ChannelId int
as	
begin
	set nocount on

	select
		*	
	from
		[customer].[vCustomCustomer]
	where
	    [CustomerUser.UserName] = @UserName
	    AND [Customer.CustomerRegistryId] IN
	    (
			SELECT CustomerRegistryId FROM [customer].[tCustomerModuleChannel]
			WHERE ChannelId = @ChannelId
	    )
end


GO
