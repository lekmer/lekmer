SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 5  *****************
User: Roman D.		Date:  13.01.09		Time: 10:10
Description:
			added inner join with template.tModelSettingType
************************************************
*****************  Version 4  *****************
User: Roman D.		Date: 23.12.2008		Time: 15:10
Description:
			added ModelSettingTypeId
************************************************
*****************  Version 3  *****************
User: Roman G.		Date: 22.12.2008		Time: 12:30
Description:
			Remove TemplateSettingId
************************************************

******************  Version 2  *****************
User: Victor E.		Date: 18.11.2008		Time: 15:40
Description:
			Add AlternateValue
************************************************

*****************  Version 1  *****************
User: Victor E.		Date: 14.10.2008		Time: 12:00
Description:
			Created 
*/

CREATE PROCEDURE [template].[pTemplateSettingGetAllByModel]
		@ModelId	int	
AS
begin
	set nocount on
	
	select	
		null AS 'TemplateSetting.TemplateId',
		null As 'TemplateSetting.Value',	
		null As 'TemplateSetting.AlternateValue',
		MS.[ModelSetting.Id] AS 'TemplateSetting.ModelSettingId',
		MS.[ModelSetting.CommonName],
		MS.[ModelSetting.Title],
		MS.[ModelSetting.TypeId],
		MST.[ModelSettingType.Title]
	from
		[template].[vCustomModelSetting] as MS
		inner join template.[vCustomModelSettingType] as MST
		on MST.[ModelSettingType.Id]= MS.[ModelSetting.TypeId]
   where
		MS.[ModelSetting.ModelId] = @ModelId
end

GO
