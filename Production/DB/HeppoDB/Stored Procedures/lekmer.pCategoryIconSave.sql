SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCategoryIconSave]
	@CategoryId	INT,
	@IconId		INT
AS 
BEGIN 
	INSERT [lekmer].[tCategoryIcon]
	VALUES (@CategoryId, @IconId)
END
GO
