SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pCategorySiteStructureRegistryDelete]
	@ContentNodeId	INT
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @tContentNode TABLE (tContentNodeId INT, tParentContentNodeId INT);

	WITH ContentNode (ContentNodeId, ParentContentNodeId) AS 
	(
		SELECT ContentNodeId, ParentContentNodeId
		FROM  sitestructure.tContentNode
		WHERE ParentContentNodeId = @ContentNodeId

		UNION ALL

		SELECT C.ContentNodeId, C.ParentContentNodeId
		FROM sitestructure.tContentNode C 
		INNER JOIN ContentNode OuterC ON OuterC.ContentNodeId = C.ParentContentNodeId
	)
	INSERT INTO @tContentNode (tContentNodeId, tParentContentNodeId)
	SELECT ContentNodeId, ParentContentNodeId FROM ContentNode
		
	UNION ALL	

	SELECT ContentNodeId, ParentContentNodeId
	FROM  sitestructure.tContentNode
	WHERE ContentNodeId = @ContentNodeId

	UPDATE product.tCategorySiteStructureRegistry
	SET ProductParentContentNodeId = NULL
	FROM product.tCategorySiteStructureRegistry p
	INNER JOIN @tContentNode c ON c.tContentNodeId = p.ProductParentContentNodeId
	
	UPDATE product.tCategorySiteStructureRegistry
	SET ProductTemplateContentNodeId = NULL
	FROM product.tCategorySiteStructureRegistry p
	INNER JOIN @tContentNode c ON c.tContentNodeId = p.ProductTemplateContentNodeId
END

GO
