SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pBlockProductReviewGetByIdSecure]
         @BlockId    INT
AS BEGIN
         SELECT
                   r.*,
                   b.*
         FROM
                   [addon].[vCustomBlockProductReview] AS r
                   inner join [sitestructure].[vCustomBlockSecure] AS b ON r.[BlockProductReview.BlockId] = b.[Block.BlockId]
         WHERE
                   r.[BlockProductReview.BlockId] = @BlockId
END



GO
