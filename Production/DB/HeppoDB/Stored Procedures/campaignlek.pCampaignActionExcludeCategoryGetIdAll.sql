
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionExcludeCategoryGetIdAll]
	@ConfigId INT
AS
BEGIN
	SELECT 
		caec.CategoryId,
		caec.IncludeSubCategories
	FROM 
		[campaignlek].[tCampaignActionExcludeCategory] caec
		INNER JOIN [product].[tCategory] c ON [c].[CategoryId] = [caec].[CategoryId]
	WHERE 
		caec.ConfigId = @ConfigId
END
GO
