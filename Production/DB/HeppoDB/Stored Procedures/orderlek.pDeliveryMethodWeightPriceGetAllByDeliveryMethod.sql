
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pDeliveryMethodWeightPriceGetAllByDeliveryMethod]
	@DeliveryMethodId	INT,
	@ChannelId			INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM [orderlek].[vDeliveryMethodWeightPrice] dmwp
		 INNER JOIN [core].[tChannel] ch ON [ch].[CurrencyId] = [dmwp].[CurrencyId]
										 AND [ch].[CountryId] = [dmwp].[CountryId]
	WHERE
		[dmwp].[DeliveryMethodId] = @DeliveryMethodId
		AND [ch].[ChannelId] = @ChannelId
END
GO
