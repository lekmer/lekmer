SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionCurrencyGetByAction]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vGiftCardViaMailProductActionCurrency]
	WHERE 
		[GiftCardViaMailProductActionCurrency.ProductActionId] = @ProductActionId
END
GO
