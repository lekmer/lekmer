
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailInfoUpdate]
	@GiftCardViaMailInfoId	INT,
	@StatusId	INT,
	@VoucherInfoId	INT,
	@VoucherCode	NVARCHAR(100)
AS
BEGIN
	UPDATE	[campaignlek].[tGiftCardViaMailInfo]
	SET		[StatusId] = @StatusId
	WHERE	GiftCardViaMailInfoId = @GiftCardViaMailInfoId
		
	IF @VoucherInfoId IS NOT NULL
		UPDATE	[campaignlek].[tGiftCardViaMailInfo]
		SET		[VoucherInfoId] = @VoucherInfoId
		WHERE	GiftCardViaMailInfoId = @GiftCardViaMailInfoId
	
	IF @VoucherCode IS NOT NULL
		UPDATE	[campaignlek].[tGiftCardViaMailInfo]
		SET		[VoucherCode] = @VoucherCode
		WHERE	GiftCardViaMailInfoId = @GiftCardViaMailInfoId
END
GO
