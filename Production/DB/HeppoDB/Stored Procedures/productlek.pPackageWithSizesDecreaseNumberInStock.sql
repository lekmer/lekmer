SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
--DROP PROCEDURE [lekmer].[pPackageWithSizesDecreaseNumberInStock]

CREATE PROCEDURE [productlek].[pPackageWithSizesDecreaseNumberInStock]
	@PackageMasterProductId	INT,
	@Quantity				INT,
	@ItemsWithoutSizes		VARCHAR(max),
	@ItemsWithSizes			XML,
	@Delimiter				CHAR(1)
	
AS 
BEGIN
	SET NOCOUNT ON
	DECLARE @NewNumberInStock INT

	-- Update Product/Package NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @PackageMasterProductId

	-- products without sizes 
	DECLARE @tmpPackageProductWithoutSizes TABLE (ProductId INT)
	INSERT INTO @tmpPackageProductWithoutSizes (ProductId)
	SELECT [Id] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ItemsWithoutSizes, @Delimiter)
	
	-- Update Package Products NumberInSock		
	UPDATE
		p
	SET
		[p].[NumberInStock] = (CASE WHEN [p].[NumberInStock] - @Quantity < 0 THEN 0
									ELSE [p].[NumberInStock] - @Quantity
							   END)
	FROM
		[product].[tProduct] p
		INNER JOIN @tmpPackageProductWithoutSizes pp ON [pp].[ProductId] = [p].[ProductId]
	
	-- Update NumberInStock of Packages that contain current product
	DECLARE @tmpProductId INT
	WHILE ((SELECT count(*) FROM @tmpPackageProductWithoutSizes) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProductWithoutSizes)
		SET @NewNumberInStock = (SELECT [NumberInStock] FROM [product].[tProduct] WHERE [ProductId] = @tmpProductId)
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId, @NewNumberInStock
		DELETE @tmpPackageProductWithoutSizes WHERE [ProductId] = @tmpProductId
	END
	
	
	-- products with sizes 
	DECLARE @tmpPackageProductWithSizes TABLE (ProductId INT, SizeId INT)
	INSERT INTO @tmpPackageProductWithSizes (ProductId, [SizeId])
	SELECT c.value('@productId[1]', 'int'), c.value('@sizeId[1]', 'int') FROM @ItemsWithSizes.nodes('/items/item') T(c)
	
	-- Update ProductSize NumberInSock
	UPDATE
		ps
	SET
		[ps].[NumberInStock] = (CASE WHEN [ps].[NumberInStock] - @Quantity < 0 THEN 0
									 ELSE [ps].[NumberInStock] - @Quantity END)
	FROM
		[lekmer].[tProductSize] ps
		INNER JOIN @tmpPackageProductWithSizes pp ON [pp].[ProductId] = [ps].[ProductId] AND [pp].[SizeId] = [ps].[SizeId]
		
	-- Update NumberInStock of Packages that contain current product
	WHILE ((SELECT count(*) FROM @tmpPackageProductWithSizes) > 0)
	BEGIN
		SET @tmpProductId = (SELECT TOP 1 [ProductId] FROM @tmpPackageProductWithSizes)
		SET @NewNumberInStock = (SELECT SUM([NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] WHERE [ProductId] = @tmpProductId)
		EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @tmpProductId, @NewNumberInStock
		DELETE @tmpPackageProductWithSizes WHERE [ProductId] = @tmpProductId
	END
END
GO
