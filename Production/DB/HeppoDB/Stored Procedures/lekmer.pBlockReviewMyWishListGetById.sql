SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE [lekmer].[pBlockReviewMyWishListGetById]
	@BlockId	int,
	@LanguageId	int
as	
begin
	set nocount on
	select
		BRMWL.*,
		b.*
	from
		[lekmer].[tBlockReviewMyWishList] as BRMWL
		inner join [sitestructure].[vCustomBlock] as b on BRMWL.[BlockId] = b.[Block.BlockId]
	where
		BRMWL.[BlockId] = @BlockId
		and b.[Block.LanguageId] = @LanguageId 
end
GO
