SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [automatedtest].[pRoleGetByTitle]
	@Title nvarchar(50)
as
begin
	select
		*
	from
		[security].vCustomRole
	where
		[Role.Title] = @Title
end

GO
