SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberSetSentStatus]
	@SubscriberId INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE 
		[lekmer].[tNewsletterSubscriber]
	SET
		[SentStatus] = 1
	WHERE
		[SubscriberId] = @SubscriberId
END
GO
