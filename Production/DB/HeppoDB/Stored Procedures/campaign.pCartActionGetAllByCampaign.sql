SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCartActionGetAllByCampaign]
	@CampaignId int
as
begin
	select
		*
	from
		campaign.vCustomCartAction
	where
		[CartAction.CampaignId] = @CampaignId
end

GO
