SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserGetByUserName]
	@Username nvarchar(100)
as
begin
	set nocount on

	select
		*
	from
		security.[vCustomSystemUser]
	where
		[SystemUser.Username] = @Username
end

GO
