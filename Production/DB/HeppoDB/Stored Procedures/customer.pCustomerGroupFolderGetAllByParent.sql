SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupFolderGetAllByParent]
@ParentId	int
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerGroupFolder]
	WHERE
		[CustomerGroupFolder.ParentId] = @ParentId
		OR (@ParentId IS NULL AND [CustomerGroupFolder.ParentId] IS NULL)
END

GO
