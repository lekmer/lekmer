
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportProductsLackingImagesMan]

AS
begin
	set nocount on

	DECLARE @ProductsNeedingImages TABLE 
	(
		ProductId int primary key with (ignore_dup_key = on)
	)
	INSERT INTO @ProductsNeedingImages (ProductId)
	SELECT
		ProductId
	FROM
		lekmer.tProductSize
	WHERE
		NumberInStock > 0
	AND	SizeId = 19	
	
	-- Remove 3 test products
	DELETE FROM @ProductsNeedingImages WHERE ProductId in (1002039,1002040,1002042)
	
	
	DELETE
		i
	FROM
		@ProductsNeedingImages i inner join product.tProduct p on i.ProductId = p.ProductId
	WHERE
		p.MediaId IS NOT NULL
	
	;WITH UniqueErpId
		AS
		(
			SELECT
				l.HYErpId,
				psp.Lagerplats,
				psp.Lagerplatstyp,
				psp.LagerplatstypBenamning,
				p.Title,
				b.Title as BrandTitle,
				--c2.Title as CategoryTitle,				
				row_number() over(partition by HYErpId order by HYErpId) RowNo
			FROM
				@ProductsNeedingImages a 
				inner join lekmer.tLekmerProduct l with (nolock) on a.ProductId = l.ProductId
				inner join product.tProduct p with (nolock) on l.ProductId = p.ProductId
				inner join lekmer.tBrand b with (nolock) on l.BrandId = b.BrandId
				inner join product.tCategory c1 with (nolock) on p.CategoryId = c1.CategoryId
				inner join product.tCategory c2 with (nolock) on c1.ParentCategoryId = c2.CategoryId
				left join lekmer.tproductsize s with (nolock) on p.ProductId = s.ProductId
				left join integration.tProductStockPosition psp with (nolock) on isnull(s.ErpId, l.HYErpId + '-251') = psp.HYarticleId
			WHERE
				s.SizeId = 19
			AND c2.CategoryId = 1000060	
			--and Lagerplats is not null			
		)
		
	SELECT
		HYErpId,
		Lagerplats,
		Lagerplatstyp,
		LagerplatstypBenamning,
		Title,
		BrandTitle			
	FROM 
		UniqueErpId			
	WHERE 
		RowNo = 1
	ORDER BY HYErpId
	
end
GO
