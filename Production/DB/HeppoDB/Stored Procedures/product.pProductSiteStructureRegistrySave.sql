SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pProductSiteStructureRegistrySave]
	@ProductId					int,
	@SiteStructureRegistryId	int,
	@ParentContentNodeId		int,
	@TemplateContentNodeId		int
AS
BEGIN

	UPDATE 
		product.tProductSiteStructureRegistry
	SET 
		ParentContentNodeId = @ParentContentNodeId,
		TemplateContentNodeId = @TemplateContentNodeId
	WHERE 
		ProductId = @ProductId
		AND SiteStructureRegistryId = @SiteStructureRegistryId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT
			product.tProductSiteStructureRegistry
			(
				[ProductId],
				[SiteStructureRegistryId],
				[ParentContentNodeId],
				[TemplateContentNodeId]
			)
		VALUES
			(
				@ProductId,
				@SiteStructureRegistryId,
				@ParentContentNodeId,
				@TemplateContentNodeId
			)
	END
END
GO
