
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductChangeCdonExportEventGetAllInQueue]
	@NumberOfItems INT,
	@CdonExportEventStatusId INT
AS 
BEGIN
	SET NOCOUNT ON

	CREATE TABLE #ProductIds(ProductId INT)
	INSERT INTO [#ProductIds]([ProductId])EXEC [export].[pProductGetIdAllAvaliableForCdonExport]

	SELECT TOP (@NumberOfItems)
		[pce].*
	FROM
		[productlek].[vProductChangeEvent] pce
	WHERE
		[pce].[ProductChangeEvent.CdonExportEventStatusId] = @CdonExportEventStatusId
		AND [pce].[ProductChangeEvent.ProductId] IN (
			SELECT DISTINCT ([up].[ProductId])
			FROM (SELECT * FROM #ProductIds) up)
	
	DROP TABLE #ProductIds
END
GO
