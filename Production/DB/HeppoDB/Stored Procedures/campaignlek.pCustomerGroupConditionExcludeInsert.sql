SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionExcludeInsert]
	@ConditionId		INT,
	@CustomerGroupId	INT
AS
BEGIN
	INSERT [campaignlek].[tCustomerGroupConditionExclude] (
		ConditionId,
		CustomerGroupId
	)
	VALUES (
		@ConditionId,
		@CustomerGroupId
	)
END
GO
