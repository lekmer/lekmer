SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportOrdersBy15Min]
	@ChannelId int,
	@StartDate datetime,
	@EndDate datetime
AS
begin
	set nocount on

	select
	   x.[Hour],
	   isnull(sum(x.Q1), 0) as 'Q1',
	   isnull(sum(x.Q2), 0) as 'Q2',
	   isnull(sum(x.Q3), 0) as 'Q3',
	   isnull(sum(x.Q4), 0)	as 'Q4'
	from
	(
	   select
		  --cast(year(Createddate) as varchar(4)) + '-' + cast(month(Createddate) as varchar(2)) + '-' + cast(day(Createddate) as varchar(2)) as 'Date',
		  datepart(hour, o.CreatedDate) as 'Hour',
		  case when DATEPART(minute, o.CreatedDate) < 15 then 1 end as 'Q1',
		  case when DATEPART(minute, o.CreatedDate) >= 15 AND DATEPART(minute, o.CreatedDate) < 30 then 1 end as 'Q2',
		  case when DATEPART(minute, o.CreatedDate) >= 30 AND DATEPART(minute, o.CreatedDate) < 45 then 1 end as 'Q3',
		  case when DATEPART(minute, o.CreatedDate) >= 45 AND DATEPART(minute, o.CreatedDate) <= 59 then 1 end as 'Q4',
		  o.OrderId,
		  g.CommonName
	   from
		  [order].tOrder o
		  inner join core.tChannel g
		  on o.ChannelId = g.ChannelId
	   where
		  o.Createddate between '2011-07-03 00:00:00.527' and '2011-07-03 23:59:00.527'
		  and o.OrderStatusId = 4
		  and o.ChannelId = 1000003
	) x
	group by
	   x.[Hour]
	order by
	   x.[Hour]

end
GO
