SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionIncludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		CategoryId,
		IncludeSubcategories
	FROM 
		[lekmer].[tCartItemFixedDiscountActionIncludeCategory]
	WHERE 
		CartActionId = @CartActionId
END


GO
