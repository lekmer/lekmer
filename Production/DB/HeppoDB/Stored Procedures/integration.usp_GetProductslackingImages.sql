SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_GetProductslackingImages]

AS
begin
	set nocount on
	begin try
		--begin transaction
		
		--declare @Return int
		--set @Return = 1
			
		-- ändra if-satserna
		--if @Return = 1
		--begin
		SELECT 
			l.ProductId, l.HYErpId 
        FROM
			lekmer.tLekmerProduct l 
        WHERE 
			l.ProductId not in (select p.ProductId 
                                    from product.tProduct  p 
										inner join product.tProductImage pp 
											on p.ProductId = pp.ProductId)
		--end
		-- if customer id is sent search after a specific code valid only to the id
		-- and see if it is still valid
		--else if @Return > 0
		--begin
			
				
		--end
		
	--commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction

		declare @ErrMsg nvarchar(2048), @SP nvarchar(256), @Severity int, @State int      
		select @ErrMsg = error_message(), @SP = error_procedure(), @Severity = error_severity(), @State = error_state()  
		
		--return @Return
	end catch		 
end
GO
