SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [template].[pTemplateDelete]
	@TemplateId		int
as
BEGIN
		UPDATE [template].[tThemeModel]
		SET [TemplateId] = NULL
		WHERE [TemplateId] = @TemplateId
	
		delete from [template].[tTemplateSetting] where [TemplateId] = @TemplateId
		delete from [template].[tTemplateFragment] where [TemplateId] = @TemplateId
		delete from [template].[tTemplate] where [TemplateId] = @TemplateId
end
GO
