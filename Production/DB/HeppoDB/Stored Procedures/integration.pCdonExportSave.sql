SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pCdonExportSave]
@ImportId			NVARCHAR(50),
@Type				VARCHAR(50),
@StatusCommonName	VARCHAR(50)
AS	
BEGIN
	SET NOCOUNT ON	
	
	DECLARE @StatusId INT
	SET @StatusId = (SELECT CdonExportStatusId FROM [integration].[tCdonExportStatus] WHERE CommonName = @StatusCommonName)
	
	INSERT INTO	[integration].[tCdonExport]
		(
			[ImportId],
			[Type],
			[StartDate],
			[Status]
		)
	VALUES
		(
			@ImportId,
			@Type,
			GETDATE(),
			@StatusId
		)		
	
	RETURN SCOPE_IDENTITY()
END
GO
