SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pInfoPointTagGroupGetAll]
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[InfoPointCommonName],
		[TagGroupCommonName]
	FROM
		[lekmer].[tTagGroupInfoPointMapping]
END
GO
