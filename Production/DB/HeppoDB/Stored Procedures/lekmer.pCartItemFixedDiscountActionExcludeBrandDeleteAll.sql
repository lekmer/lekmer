SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionExcludeBrandDeleteAll]
	@CartActionId INT
AS
BEGIN
	DELETE
		[lekmer].[tCartItemFixedDiscountActionExcludeBrand]
	WHERE
		CartActionId = @CartActionId
END



GO
