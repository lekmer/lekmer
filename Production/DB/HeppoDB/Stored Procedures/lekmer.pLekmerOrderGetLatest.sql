SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pLekmerOrderGetLatest]
	@Date DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.CreatedDate] >= @Date
		AND [Order.CreatedDate] < DATEADD(day, 1, @Date)
		AND [Lekmer.FeedbackToken] IS NULL
		AND [OrderStatus.CommonName] = 'OrderInHY'
END
GO
