SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCustomerForgotPasswordDelete]
	@CustomerId	int
AS 	
BEGIN 
	DELETE  
		[lekmer].[tCustomerForgotPassword]
	WHERE 
		[CustomerId] = @CustomerId
END
GO
