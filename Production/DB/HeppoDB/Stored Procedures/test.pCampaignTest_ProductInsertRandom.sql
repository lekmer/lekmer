SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [test].[pCampaignTest_ProductInsertRandom]
	@ChannelId int,
	@PriceIncludingVat decimal(16,2),
	@VatPercentage int,
	@ErpId varchar(50) = null
as
begin
	declare @CategoryId int
	select top 1
		@CategoryId = CategoryId
	from
		product.tCategory

	declare @PriceListId int
	select top 1
		@PriceListId = PriceListId
	from
		product.tPriceList p
		inner join product.tProductModuleChannel c on c.PriceListRegistryId = p.PriceListRegistryId
	where
		c.ChannelId = @ChannelId
		
	declare @ProductRegistryId int
	select
		@ProductRegistryId = ProductRegistryId
	from
		product.tProductModuleChannel
	where
		ChannelId = @ChannelId

	declare @ProductStatusId int
	select
		@ProductStatusId = ProductStatusId
	from
		product.tProductStatus
	where
		CommonName = 'Online'

	declare @ProductId int
	insert product.tProduct
	(
		ErpId,
		IsDeleted,
		NumberInStock,
		CategoryId,
		Title,
		ProductStatusId
	)
	values
	(
		@ErpId,
		0,
		1,
		@CategoryId,
		CAST(NEWID() AS VARCHAR(36)),
		@ProductStatusId
	)
	set @ProductId = scope_identity()
	
	insert product.tProductRegistryProduct
	(
		ProductRegistryId,
		ProductId
	)
	values
	(
		@ProductRegistryId,
		@ProductId
	)
	
	insert product.tPriceListItem
	(
		PriceListId,
		ProductId,
		PriceIncludingVat,
		PriceExcludingVat,
		VatPercentage
	)
	values
	(
		@PriceListId,
		@ProductId,
		@PriceIncludingVat,
		ROUND(@PriceIncludingVat * (1 - cast(@VatPercentage as decimal(16, 2)) / 100), 2),
		@VatPercentage
	)
	
	return @ProductId
end






GO
