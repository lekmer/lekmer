SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionSave]
	@ProductActionId	INT,
	@IncludeAllProducts BIT
AS
BEGIN
	UPDATE
		[campaignlek].[tFixedPriceAction]
	SET
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		ProductActionId = @ProductActionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tFixedPriceAction] (
			ProductActionId,
			IncludeAllProducts
		)
		VALUES (
			@ProductActionId,
			@IncludeAllProducts
		)
	END
END

GO
