SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductUrlHistoryDeleteExpiredItems]
AS
BEGIN
	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	DELETE 
		puh 
	FROM
		[lekmer].[tProductUrlHistory] puh
		INNER JOIN [lekmer].[tHistoryLifeIntervalType] hlit ON puh.HistoryLifeIntervalTypeId = hlit.HistoryLifeIntervalTypeId
	WHERE 
		@CurrentDate > DATEADD(DAY, hlit.DaysActive, puh.CreationDate)
END
GO
