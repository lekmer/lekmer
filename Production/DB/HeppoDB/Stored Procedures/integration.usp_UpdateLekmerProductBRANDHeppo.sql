SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_UpdateLekmerProductBRANDHeppo]

AS
BEGIN

	begin try
	begin transaction

	
	UPDATE 
			lp
		SET 
			lp.BrandId = b.BrandId
		--select  tp.BrandId, lp.BrandId, b.BrandId
		FROM 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,11)	
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join lekmer.tBrand b
					on b.ErpId = tp.BrandId
					
		WHERE 
			substring(tp.HYarticleId, 3,1) = 1
			AND (lp.BrandId <> b.BrandId or lp.BrandId is null)
		--GROUP BY lp.BrandId, b.BrandId, tp.BrandId
	

	commit
	end try
	begin catch
		if @@TRANCOUNT > 0 rollback
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
		values(Null, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch
END
GO
