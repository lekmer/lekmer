SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Roman D.	Date: 21.05.2009	Time: 16:00
Description:	Created
*/

create procedure [product].[pBlockProductSearchResultDelete]
@BlockId	int
as
begin
	delete
		[product].[tBlockProductSearchResult]
	where
		[BlockId] = @BlockId
end
GO
