SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionCurrencyInsert]
	@CartActionId	INT,
	@CurrencyId		INT,
	@MonetaryValue	DECIMAL(16,2)
AS
BEGIN
	INSERT [lekmer].[tCartItemFixedDiscountActionCurrency] (
		CartActionId,
		CurrencyId,
		MonetaryValue
	)
	VALUES (
		@CartActionId,
		@CurrencyId,
		@MonetaryValue
	)
END

GO
