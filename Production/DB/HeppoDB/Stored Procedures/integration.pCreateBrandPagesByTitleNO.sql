
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pCreateBrandPagesByTitleNO]
	@BrandTitle nvarchar(50)

AS
begin
	set nocount on
		
	begin try
		begin transaction
		
		--declare @BrandTitle nvarchar(50)
		--set @BrandTitle = 'Filippa K'
		
		DECLARE @tmp TABLE (ContentNodeId int not null)
		DECLARE @tmpBlockId TABLE (BlockId int not null)
		
		-- tContentNode
		insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
					Ordinal, CommonName, AccessId, SiteStructureRegistryId)
		output inserted.ContentNodeId INTO @tmp
		select
			1001324, -- Varemerker
			3, -- ?
			1, --Offline
			@BrandTitle,
			128,
			LOWER(@BrandTitle),
			1, --All
			2 -- no

		
		-- tContentPage
		insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
					UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
		select
			(select top 1 ContentNodeId from @tmp),
			2, -- no
			1000048, -- BrandsDetailPage
			1000508,
			@BrandTitle,
			REPLACE((REPLACE(LOWER(@BrandTitle), ' ', '-')), '&', 'and'),
			1, -- Content page
			0,
			1001323
			
		-- tBlock
		-- Brand Profile
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			18, --BrandList
			0,
			1000018,
			'brand profile',
			1,
			1,
			1000049
			
					-- Brand profile extra content
					insert into lekmer.tBlockBrandList(BlockId, ColumnCount, [RowCount], IncludeAllBrands, LinkContentNodeId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						1,
						1,
						0,
						NULL
				
						-- Brand profile BrandlistBrand extra content
					insert into lekmer.tBlockBrandListBrand(BlockId, BrandId, Ordinal)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle), -- NY
						10

					-- töm listan
					delete from @tmpBlockId
			
		-- Image
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			1, 
			0,
			1000018,
			'Image',
			2,
			1,
			1000096
			
					-- Image extra content
					insert into sitestructure.tBlockRichText(BlockId, Content)
					select
						(select top 1 BlockId from @tmpBlockId),
						NULL
		
					-- töm listan
					delete from @tmpBlockId
					
					
		-- Brand Info
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			18, --BrandList
			0,
			1000018,
			'brand info',
			3,
			1,
			1000095
				
					-- Brand Info extra content
					insert into lekmer.tBlockBrandList(BlockId, ColumnCount, [RowCount], IncludeAllBrands, LinkContentNodeId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						1,
						1,
						0,
						NULL
					
					-- Brand Info BrandListBrand extra content
					insert into lekmer.tBlockBrandListBrand(BlockId, BrandId, Ordinal)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle), -- NY
						10
						
					-- töm listan
					delete from @tmpBlockId
		
		-- Heppo tipsar
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			17, 
			0,
			1000019,
			'Heppo foreslår', -- en case här beroende på channel
			1,
			1,
			1000134
			
					-- Heppo Tipsar extra content
					insert into lekmer.tBlockBrandProductList(BlockId, ColumnCount, [RowCount], ProductSortOrderId)
					select
						(select top 1 BlockId from @tmpBlockId),
						2,
						2,
						2
					
					-- Heppo Tipsar BrandProductListBrand extra content
					insert into lekmer.tBlockBrandProductListBrand(BlockId, BrandId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle) -- NY

					
					-- töm listan
					delete from @tmpBlockId
			
		-- produkter från brand
		insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
					AccessId, TemplateId)
		output inserted.BlockId INTO @tmpBlockId
		select
			(select top 1 ContentNodeId from @tmp),
			17, 
			0,
			1000021,
			'Merkesko fra ' + @BrandTitle, -- en case här beroende på channel
			1,
			1,
			1000133
			
					-- produkter från brand extra content
					insert into lekmer.tBlockBrandProductList(BlockId, ColumnCount, [RowCount], ProductSortOrderId)
					select
						(select top 1 BlockId from @tmpBlockId),
						4,
						3,
						3
					
					--produkter från brand BrandProductListBrand extra content
					insert into lekmer.tBlockBrandProductListBrand(BlockId, BrandId)			
					select
						(select top 1 BlockId from @tmpBlockId),
						(select BrandId from lekmer.tBrand where Title = @BrandTitle) -- NY
						
						
					-- töm listan
					delete from @tmpBlockId
	
					
				---------------------------
				-- CHILDREN - MAN
				---------------------------
				DECLARE @tmpChildrenMan TABLE (ContentNodeId int not null)
				
				-- tContentNode
				insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
							Ordinal, CommonName, AccessId, SiteStructureRegistryId)
				output inserted.ContentNodeId INTO @tmpChildrenMan
				select
					(select top 1 ContentNodeId from @tmp), -- förlärder är ovan
					3, -- ?
					0, --Offline
					@BrandTitle + ' for menn',
					2,
					LOWER(@BrandTitle) + 'ForMenn',
					1, --All
					2 -- no

				
				-- tContentPage
				insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
							UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
				select
					(select top 1 ContentNodeId from @tmpChildrenMan),
					2, -- no
					1000713,
					1000708,
					@BrandTitle,
					REPLACE((REPLACE(LOWER(@BrandTitle), ' ', '-') + '-for-menn'), '&', 'and'),					
					1, -- Content page
					0,
					1019562
					
						-- tBlock
						-- Brand/category filter
						insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
									AccessId, TemplateId)
						output inserted.BlockId INTO @tmpBlockId
						select
							(select top 1 ContentNodeId from @tmpChildrenMan),
							1000001, 
							0,
							1000195,
							@BrandTitle + ' for menn',
							1,
							1,
							1000748
							
									-- Brand/category filter extra content
									insert into lekmer.tBlockProductFilter(BlockId, SecondaryTemplateId, DefaultCategoryId)			
									select
										(select top 1 BlockId from @tmpBlockId),
										1000747,
										1000059
									
									-- Brand/category filterBrand extra content
									insert into lekmer.tBlockProductFilterBrand(BlockId, BrandId)
									select
										(select top 1 BlockId from @tmpBlockId),
										(select BrandId from lekmer.tBrand where Title = @BrandTitle)
									
									-- Brand/category filterTags extra content
									insert into lekmer.tBlockProductFilterTag(BlockId, TagId)
									select
										(select top 1 BlockId from @tmpBlockId),
										1000009
									
									-- Rating
									DELETE FROM [review].[tBlockRating]
									WHERE [BlockId] = (select top 1 BlockId from @tmpBlockId)

									INSERT INTO [review].[tBlockRating] ([BlockId], [RatingId])
									select
										(select top 1 BlockId from @tmpBlockId),
										1
									
									-- töm listan
									delete from @tmpBlockId
						
				---------------------------
				-- CHILDREN - Woman
				---------------------------
				DECLARE @tmpChildrenWoman TABLE (ContentNodeId int not null)
				
				-- tContentNode
				insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
							Ordinal, CommonName, AccessId, SiteStructureRegistryId)
				output inserted.ContentNodeId INTO @tmpChildrenWoman
				select
					(select top 1 ContentNodeId from @tmp), -- förlärder är ovan
					3, -- ?
					0, --Offline
					@BrandTitle + ' for kvinner',
					2,
					LOWER(@BrandTitle) + 'ForKvinner',
					1, --All
					2 -- no

				
				-- tContentPage
				insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
							UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
				select
					(select top 1 ContentNodeId from @tmpChildrenWoman),
					2, -- no
					1000713,
					1000708,
					@BrandTitle,
					REPLACE((REPLACE(LOWER(@BrandTitle), ' ', '-') + '-for-kvinner'), '&', 'and'),										
					1, -- Content page
					0,
					1019562
					
						-- tBlock
						-- Brand/category filter
						insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
									AccessId, TemplateId)
						output inserted.BlockId INTO @tmpBlockId
						select
							(select top 1 ContentNodeId from @tmpChildrenWoman),
							1000001, 
							0,
							1000195,
							@BrandTitle + ' for kvinner',
							1,
							1,
							1000748
							
									-- Brand/category filter extra content
									insert into lekmer.tBlockProductFilter(BlockId, SecondaryTemplateId, DefaultCategoryId)			
									select
										(select top 1 BlockId from @tmpBlockId),
										1000747,
										1000059
									
									-- Brand/category filterBrand extra content
									insert into lekmer.tBlockProductFilterBrand(BlockId, BrandId)
									select
										(select top 1 BlockId from @tmpBlockId),
										(select BrandId from lekmer.tBrand where Title = @BrandTitle)
									
									-- Brand/category filterTags extra content
									insert into lekmer.tBlockProductFilterTag(BlockId, TagId)
									select
										(select top 1 BlockId from @tmpBlockId),
										1000010
										
									-- Rating
									DELETE FROM [review].[tBlockRating]
									WHERE [BlockId] = (select top 1 BlockId from @tmpBlockId)

									INSERT INTO [review].[tBlockRating] ([BlockId], [RatingId])
									select
										(select top 1 BlockId from @tmpBlockId),
										1
										
									-- töm listan
									delete from @tmpBlockId
									
									
				---------------------------
				-- CHILDREN - CHILD
				---------------------------
				DECLARE @tmpChildrenChild TABLE (ContentNodeId int not null)
				
				-- tContentNode
				insert into sitestructure.tContentNode(ParentContentNodeId, ContentNodeTypeId, ContentNodeStatusId, Title,
							Ordinal, CommonName, AccessId, SiteStructureRegistryId)
				output inserted.ContentNodeId INTO @tmpChildrenChild
				select
					(select top 1 ContentNodeId from @tmp), -- förlärder är ovan
					3, -- ?
					0, --Offline
					@BrandTitle + ' for barn',
					2,
					LOWER(@BrandTitle)+'ForBarn',
					1, --All
					2 -- no

				
				-- tContentPage
				insert into sitestructure.tContentPage(ContentNodeId, SiteStructureRegistryId, TemplateId, MasterTemplateId, Title,
							UrlTitle, ContentPageTypeId, IsMaster, MasterPageId)
				select
					(select top 1 ContentNodeId from @tmpChildrenChild),
					2, -- no
					1000713,
					1000708,
					@BrandTitle,
					(REPLACE(LOWER(@BrandTitle), ' ', '-') + '-for-barn'),
					1, -- Content page
					0,
					1019562
					
						-- tBlock
						-- Brand/category filter
						insert into sitestructure.tBlock(ContentNodeId, BlockTypeId, BlockStatusId, ContentAreaId, Title, Ordinal,
									AccessId, TemplateId)
						output inserted.BlockId INTO @tmpBlockId
						select
							(select top 1 ContentNodeId from @tmpChildrenChild),
							1000001, 
							0,
							1000195,
							@BrandTitle + ' for barn',
							1,
							1,
							1000748
							
									-- Brand/category filter extra content
									insert into lekmer.tBlockProductFilter(BlockId, SecondaryTemplateId, DefaultCategoryId)			
									select
										(select top 1 BlockId from @tmpBlockId),
										1000747,
										1000059
									
									-- Brand/category filterBrand extra content
									insert into lekmer.tBlockProductFilterBrand(BlockId, BrandId)
									select
										(select top 1 BlockId from @tmpBlockId),
										(select BrandId from lekmer.tBrand where Title = @BrandTitle)
									
									-- Rating
									DELETE FROM [review].[tBlockRating]
									WHERE [BlockId] = (select top 1 BlockId from @tmpBlockId)

									INSERT INTO [review].[tBlockRating] ([BlockId], [RatingId])
									select
										(select top 1 BlockId from @tmpBlockId),
										1
										
									-- töm listan
									delete from @tmpBlockId
			--rollback
									
			commit			
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
