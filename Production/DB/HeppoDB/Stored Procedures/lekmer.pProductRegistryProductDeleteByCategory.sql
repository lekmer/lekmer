
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryProductDeleteByCategory]
	@CategoryId			INT,
	@ProductRegistryIds	VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN 
	DELETE prp 
	FROM [product].[tProductRegistryProduct] prp
		 INNER JOIN [product].[tProduct] p ON p.[ProductId] = prp.[ProductId]
	WHERE p.[CategoryId] IN (SELECT CategoryId FROM [product].[fnGetSubCategories] (@CategoryId))
		  AND prp.[ProductRegistryId] IN (SELECT ID FROM [generic].[fnConvertIDListToTable](@ProductRegistryIds, @Delimiter))
		  
	EXEC [productlek].[pPackageRestrictionsUpdate] NULL, NULL, @CategoryId, @ProductRegistryIds, @Delimiter
END
GO
