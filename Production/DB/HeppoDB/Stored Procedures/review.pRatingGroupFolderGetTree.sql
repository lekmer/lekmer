SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupFolderGetTree]
	@SelectedId	INT
AS
BEGIN
	SET NOCOUNT ON

	DECLARE @tTree TABLE (Id INT, ParentId INT, Title NVARCHAR(50))

	INSERT INTO @tTree
	SELECT rgf.*
	FROM [review].[vRatingGroupFolder] AS rgf
	WHERE rgf.[RatingGroupFolder.ParentRatingGroupFolderId] IS NULL

	IF (@SelectedId IS NOT NULL)
	BEGIN
		INSERT INTO @tTree
		SELECT rgf.*
		FROM [review].[vRatingGroupFolder] AS rgf
		WHERE rgf.[RatingGroupFolder.ParentRatingGroupFolderId] = @SelectedId

		DECLARE @ParentId INT
		WHILE (@SelectedId IS NOT NULL)
		BEGIN
			SET @ParentId = (SELECT rgf.[RatingGroupFolder.ParentRatingGroupFolderId]
							 FROM [review].[vRatingGroupFolder] AS rgf
							 WHERE rgf.[RatingGroupFolder.RatingGroupFolderId] = @SelectedId)
							 
			INSERT INTO @tTree
			SELECT rgf.*
			FROM [review].[vRatingGroupFolder] AS rgf
			WHERE rgf.[RatingGroupFolder.ParentRatingGroupFolderId] = @ParentId
			
			SET @SelectedId = @ParentId
		END
	END
		
	SELECT
		Id,
		ParentId,
		Title,
		CAST((CASE WHEN EXISTS (SELECT 1 FROM [review].[vRatingGroupFolder] AS rgf
								WHERE rgf.[RatingGroupFolder.ParentRatingGroupFolderId] = Id)
		THEN 1 ELSE 0 END) AS BIT) AS 'HasChildren'
	FROM
		@tTree
	ORDER BY
		ParentId ASC
END
GO
