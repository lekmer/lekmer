SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/******************  Version 1  **********************************************
User: Volodymdyr Y.		Date: 20/01/2009                            >Creation*
*****************************************************************************/

Create PROCEDURE [template].[pModelFragmentRegionDelete] 
	@ModelFragmentRegionId INT
AS 
BEGIN
    SET NOCOUNT ON	

    IF EXISTS ( SELECT  *
                FROM    [template].[tModelFragment]
                WHERE   [ModelFragmentRegionId] = @ModelFragmentRegionId ) 
        RETURN 1

    DELETE  [template].[tModelFragmentRegion]
    WHERE   [ModelFragmentRegionId] = @ModelFragmentRegionId
    
    RETURN 0
END
GO
