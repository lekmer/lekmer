SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreStatusGetAll]
as
begin
	select
		*
	from
		[product].[vCustomStoreStatus] c
end

GO
