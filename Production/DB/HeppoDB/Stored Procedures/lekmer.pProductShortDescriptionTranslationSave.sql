SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pProductShortDescriptionTranslationSave]
	@ProductId		INT,
	@LanguageId		INT,
	@Value			NVARCHAR(MAX)
AS
begin
	set nocount on
	
	update
		product.[tProductTranslation]
	set
		[ShortDescription] = @Value	
	where
		[ProductId] = @ProductId
		AND [LanguageId] = @LanguageId
		
	if  @@ROWCOUNT = 0
	begin		
		insert into product.[tProductTranslation]
		(
			[ProductId],
			[LanguageId],
			[ShortDescription]				
		)
		values
		(
			@ProductId,
			@LanguageId,
			@Value
		)
	end
end	
GO
