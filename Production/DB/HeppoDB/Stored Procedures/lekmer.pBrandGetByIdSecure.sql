SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBrandGetByIdSecure]
@brandId int
AS
BEGIN
	SET NOCOUNT ON;
	
	SELECT
	     *
	FROM
	    lekmer.vBrandSecure where [Brand.BrandId] = @brandId

END




GO
