SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeProductGetIdAllSecure]
	@ProductActionId INT
AS
BEGIN
	SELECT
		p.[ProductId]
	FROM
		[campaignlek].[tFixedPriceActionIncludeProduct] fdaip
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = fdaip.[ProductId]
	WHERE
		fdaip.[ProductActionId] = @ProductActionId
		AND p.[IsDeleted] = 0
END

GO
