SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionGetById]
	@ActionId int
AS 
BEGIN 
	SELECT
		*
	FROM
		lekmer.vProductDiscountAction
	WHERE
		[ProductAction.Id] = @ActionId
END 


GO
