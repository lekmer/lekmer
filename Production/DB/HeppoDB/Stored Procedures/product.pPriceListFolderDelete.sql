SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pPriceListFolderDelete]
@PriceListFolderId	int
AS
BEGIN
	DELETE FROM 
		product.tPriceListFolder
	WHERE 
		PriceListFolderId = @PriceListFolderId
END
GO
