SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionIncludeBrandInsert]
	@ProductActionId INT,
	@BrandId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedPriceActionIncludeBrand] (
		ProductActionId,
		BrandId
	)
	VALUES (
		@ProductActionId,
		@BrandId
	)
END

GO
