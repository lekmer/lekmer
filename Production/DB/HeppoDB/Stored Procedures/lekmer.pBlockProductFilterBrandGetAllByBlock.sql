SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure lekmer.pBlockProductFilterBrandGetAllByBlock
	@BlockId int
as
begin
	select
		[BlockProductFilterBrand.BrandId]
	from
		lekmer.vBlockProductFilterBrand
	where
		[BlockProductFilterBrand.BlockId] = @BlockId
end
GO
