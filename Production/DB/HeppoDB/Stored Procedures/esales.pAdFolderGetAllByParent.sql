SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pAdFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rf.*
	FROM
		[esales].[vAdFolder] rf
	WHERE
		(rf.[AdFolder.ParentFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		(rf.[AdFolder.ParentFolderId] IS NULL AND @ParentId IS NULL)
END
GO
