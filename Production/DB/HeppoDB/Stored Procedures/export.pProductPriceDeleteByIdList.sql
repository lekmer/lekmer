SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [export].[pProductPriceDeleteByIdList]
	@ChannelId	INT,
	@ProductIds	VARCHAR(MAX),
	@Delimiter	CHAR(1)
AS
BEGIN
	SET NOCOUNT ON;

	DELETE
		pp
	FROM
		[export].[tProductPrice] pp
		INNER JOIN [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) AS pl ON pl.[ID] = pp.[ProductId]
	WHERE 
		pp.[ChannelId] = @ChannelId
END
GO
