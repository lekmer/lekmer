SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pTagGetAllSecure]
AS
BEGIN 
	SET NOCOUNT ON

	SELECT 
		*
	FROM 
		lekmer.[vTagSecure]
END 
GO
