SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pProductCampaignDelete]
	@CampaignId int
AS 
BEGIN
	DELETE FROM campaign.tProductCampaign
	WHERE CampaignId = @CampaignId
END
GO
