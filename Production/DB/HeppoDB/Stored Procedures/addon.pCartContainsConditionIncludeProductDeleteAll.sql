SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [addon].[pCartContainsConditionIncludeProductDeleteAll]
	@ConditionId int
AS 
BEGIN 
	DELETE
		[addon].tCartContainsConditionIncludeProduct
	WHERE
		ConditionId = @ConditionId
END 



GO
