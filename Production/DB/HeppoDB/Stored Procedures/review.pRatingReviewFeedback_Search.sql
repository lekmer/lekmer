
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewFeedback_Search]
	@Author					NVARCHAR(50),
	@Message				NVARCHAR(500),
	@StatusId				INT,
	@CreatedFrom			DATETIME,
	@CreatedTo				DATETIME,
	@ProductId				INT,
	@ProductTitle			NVARCHAR(256),
	@OrderId				INT,
	@InappropriateContent	BIT,
	@SortBy					VARCHAR(50) = NULL,
	@SortDescending			BIT = NULL,
	@Page					INT = NULL,
	@PageSize				INT
AS   
BEGIN
	SET NOCOUNT ON
	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END

	DECLARE @sql NVARCHAR(MAX)
	DECLARE @sqlCount NVARCHAR(MAX)
	DECLARE @sqlFragment NVARCHAR(MAX)
     
    SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' + COALESCE(@SortBy, 'RatingReviewFeedback.CreatedDate')+ ']'
			+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			rr.*
		FROM
			[review].[vReviewRecord] rr
		WHERE
			(@ProductId IS NULL OR rr.[RatingReviewFeedback.ProductId] = @ProductId)'
			+ CASE WHEN (@ProductTitle IS NOT NULL) THEN + 'AND rr.[ReviewRecord.Product.Title] LIKE ''%' + REPLACE(@ProductTitle, '''', '''''') + '%''' ELSE '' END
			+ CASE WHEN (@StatusId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.RatingReviewStatusId] = @StatusId ' ELSE '' END
			+ CASE WHEN (@Message IS NOT NULL) THEN + 'AND (rr.[Review.Title] LIKE ''%' + REPLACE(@Message, '''', '''''') + '%'' OR rr.[Review.Message] LIKE ''%' + REPLACE(@Message, '''', '''''') + '%'')' ELSE '' END
			+ CASE WHEN (@Author IS NOT NULL) THEN + 'AND rr.[Review.AuthorName] = @Author ' ELSE '' END
			+ CASE WHEN (@OrderId IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.OrderId] = @OrderId ' ELSE '' END
			+ CASE WHEN (@InappropriateContent = 1) THEN + 'AND rr.[RatingReviewFeedback.Impropriate] = 1 ' ELSE '' END
			+ CASE WHEN (@CreatedFrom IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.CreatedDate] >= @CreatedFrom ' ELSE '' END
			+ CASE WHEN (@CreatedTo IS NOT NULL) THEN + 'AND rr.[RatingReviewFeedback.CreatedDate] <= @CreatedTo ' ELSE '' END

     SET @sql = 'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

     IF @Page != 0 AND @Page IS NOT NULL 
     BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
     END
     
     SET @sqlCount = 'SELECT COUNT(1) FROM (' + @sqlFragment + ') AS CountResults'
     
	EXEC sp_executesql @sqlCount,
		N'      
			@Author					NVARCHAR(50),
			@Message				NVARCHAR(500),
			@StatusId				INT,
			@CreatedFrom			DATETIME,
            @CreatedTo				DATETIME,
            @ProductId				INT,
            @ProductTitle			NVARCHAR(256),
            @OrderId				INT,
            @Page					INT,
            @PageSize				INT',
            @Author,
            @Message,
            @StatusId,
            @CreatedFrom,
            @CreatedTo,
            @ProductId,
            @ProductTitle,
            @OrderId,
            @Page,
            @PageSize
                             
	EXEC sp_executesql @sql, 
		N'      
			@Author					NVARCHAR(50),
			@Message				NVARCHAR(500),
			@StatusId				INT,
			@CreatedFrom			DATETIME,
            @CreatedTo				DATETIME,
            @ProductId				INT,
            @ProductTitle			NVARCHAR(256),
            @OrderId				INT,
            @Page					INT,
            @PageSize				INT',
            @Author,
            @Message,
            @StatusId,
            @CreatedFrom,
            @CreatedTo,
            @ProductId,
            @ProductTitle,
            @OrderId,
            @Page,
            @PageSize
END
GO
