SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFreightValueActionDeliveryMethodInsert]
	@ActionId INT,
	@DeliveryMethodId INT
AS
BEGIN
	SET NOCOUNT ON

	INSERT INTO [campaignlek].[tFreightValueActionDeliveryMethod] (
		[CartActionId],
		[DeliveryMethodId]
	)
	VALUES (
		@ActionId,
		@DeliveryMethodId
	)
END
GO
