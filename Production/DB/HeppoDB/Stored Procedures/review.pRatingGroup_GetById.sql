SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroup_GetById]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rg.*
	FROM
		[review].[vRatingGroup] rg
	WHERE
		rg.[RatingGroup.RatingGroupId] = @RatingGroupId
END
GO
