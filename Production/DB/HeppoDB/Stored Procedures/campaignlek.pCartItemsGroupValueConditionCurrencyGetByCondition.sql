SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionCurrencyGetByCondition]
	@ConditionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vCartItemsGroupValueConditionCurrency]
	WHERE 
		[CartItemsGroupValueConditionCurrency.ConditionId] = @ConditionId
END
GO
