SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pGetProductGroupMapping]
     @ChannelId INT,
     @ProductId INT
         
AS BEGIN
         SELECT
                   TDPGM.ProductGroupId
         FROM
                   [Lekmer].[tTradeDoublerProductGroupMapping] AS TDPGM
         WHERE
                   TDPGM.[ChannelId] = @ChannelId
                   AND TDPGM.[ProductId] = @ProductId
END
GO
