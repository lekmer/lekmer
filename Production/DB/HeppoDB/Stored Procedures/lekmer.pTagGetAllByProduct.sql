SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pTagGetAllByProduct]
	@LanguageId INT,
	@ProductId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[t].*
	FROM
		[lekmer].[vTag] t
		INNER JOIN [lekmer].[tProductTag] AS pt ON t.[Tag.TagId] = pt.[TagId]
	WHERE 
		pt.[ProductId] = @ProductId
		AND
		t.[Tag.LanguageId] = @LanguageId
END
GO
