SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductImageGetAllByProductAndGroup]
	@ProductId INT,
	@ProductImageGroupId INT	
AS
BEGIN
	SELECT
		*
	FROM 
		product.vCustomProductImage i
	WHERE
		i.[ProductImage.ProductId] = @ProductId
		AND i.[ProductImage.GroupId] = @ProductImageGroupId
	ORDER BY i.[ProductImage.Ordinal]
END

GO
