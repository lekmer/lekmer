SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockBestRatedProductListBrandDeleteAll]
	@BlockId INT
AS
BEGIN
	DELETE [review].[tBlockBestRatedProductListBrand]
	WHERE  [BlockId] = @BlockId
END
GO
