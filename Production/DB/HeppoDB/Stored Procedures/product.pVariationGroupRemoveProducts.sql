SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [product].[pVariationGroupRemoveProducts]
	@VariationGroupId	int
as
begin
		update	[product].[tVariationGroup]
		Set
			[DefaultProductId] = NULL
		where
			[VariationGroupId] = @VariationGroupId

		delete from [product].[tVariationGroupProduct] where [VariationGroupId] = @VariationGroupId	
end

GO
