SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE PROCEDURE [order].[pOrderCommentSave]
@OrderCommentId		int,
@OrderId			int,
@CreationDate		smalldatetime,
@Comment			nvarchar(250),
@AuthorId			int
AS	
BEGIN
	SET NOCOUNT ON	
		
	UPDATE  [order].[tOrderComment] 
	SET 		
		Comment = @Comment
	WHERE
		OrderCommentId = @OrderCommentId
	
	IF @@ROWCOUNT = 0 
	BEGIN				
		INSERT INTO [order].[tOrderComment] 
		(
			OrderId,
			CreationDate,
			Comment,
			AuthorId
		)
		VALUES
		(
			@OrderId,
			@CreationDate,
			@Comment,
			@AuthorId
		)
	END
END





GO
