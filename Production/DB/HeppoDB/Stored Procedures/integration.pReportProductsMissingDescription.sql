SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportProductsMissingDescription]

AS
begin
	set nocount on
	begin try
				
			select distinct HYErpId 
			from 
				product.tProduct p
				inner join lekmer.tLekmerProduct l
					on p.ProductId = l.ProductId
				left join lekmer.tProductSize ps
					on p.ProductId = ps.ProductId
			where
				p.MediaId is not null
				and ((p.NumberInStock > 0 and ps.ProductId is null)
					or ps.NumberInStock > 0)
				and p.[Description] = ''
			group by 
				l.HYErpid
		

	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
