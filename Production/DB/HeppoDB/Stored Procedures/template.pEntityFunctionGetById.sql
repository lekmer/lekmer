SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityFunctionGetById]
	@FunctionId	int
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomEntityFunction] 
	where
		[EntityFunction.Id] = @FunctionId
end

GO
