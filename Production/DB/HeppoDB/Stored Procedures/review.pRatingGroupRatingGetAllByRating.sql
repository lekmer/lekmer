SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupRatingGetAllByRating]
	@RatingId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgr.*
	FROM
		[review].[vRatingGroupRating] rgr
	WHERE
		rgr.[RatingGroupRating.RatingId] = @RatingId
END
GO
