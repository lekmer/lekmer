SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [product].[pProductImageDeleteAllByProduct_]
	@productId INT
AS
BEGIN
	DELETE
		[product].[tProductImage]
	WHERE
		ProductId = @productId
END
GO
