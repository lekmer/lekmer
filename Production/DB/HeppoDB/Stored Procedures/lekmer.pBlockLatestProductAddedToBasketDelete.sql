SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE 	[lekmer].[pBlockLatestProductAddedToBasketDelete]
@BlockId	int
as
BEGIN
	
	

	DELETE 
		lekmer.[tBlockLatestProductAddedToBasket]
	where
		[BlockId] = @BlockId
end
GO
