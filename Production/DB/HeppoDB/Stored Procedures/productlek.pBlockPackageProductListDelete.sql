SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListDelete]
	@BlockId INT
AS
BEGIN
	DELETE [productlek].[tBlockPackageProductList]
	WHERE [BlockId] = @BlockId
END
GO
