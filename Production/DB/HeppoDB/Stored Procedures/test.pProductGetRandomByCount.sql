SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [test].[pProductGetRandomByCount]
	@Count INT,
	@ChannelId INT
as
begin
	SELECT TOP (@Count)
		*
	FROM 
		product.vCustomProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = P.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(p.[Product.CurrencyId], P.[Product.Id], p.[Product.PriceListRegistryId], NULL)
	WHERE  
		p.[Product.ChannelId] = @ChannelId
	ORDER By
		NEWID()
end

GO
