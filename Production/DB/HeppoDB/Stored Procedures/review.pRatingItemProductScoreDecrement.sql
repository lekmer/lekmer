SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingItemProductScoreDecrement]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DECLARE @ProductId INT
	DECLARE @ChannelId INT
	
	SELECT
		@ProductId = rrf.ProductId,
		@ChannelId = rrf.ChannelId
	FROM [review].[tRatingReviewFeedback] rrf
	WHERE rrf.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
	
	DECLARE @RelationListTypeId INT
	SET @RelationListTypeId = (SELECT RelationListTypeId FROM [product].[tRelationListType] WHERE CommonName = 'Variant')
	
	IF OBJECT_ID('tempdb..#VariantIds') IS NOT NULL
		DROP TABLE #VariantIds
	CREATE TABLE #VariantIds(VariantId INT);
	INSERT INTO #VariantIds EXEC [lekmer].[pProductVariantGetIdAllByProduct] @ProductId, @RelationListTypeId
	
	-- Add current product
	INSERT INTO #VARIANTIDS (VariantId) VALUES (@ProductId)

	UPDATE ps
	SET ps.[HitCount] = ps.[HitCount] - 1
	FROM [review].[tRatingItemProductScore] ps
		INNER JOIN [review].[tRatingItemProductVote] pv ON 
			pv.[RatingId] = ps.[RatingId] 
			AND pv.[RatingItemId] = ps.[RatingItemId]
		INNER JOIN #VariantIds vid ON
			vid.[VariantId] = ps.[ProductId]
	WHERE
		ps.[ChannelId] = @ChannelId
		AND pv.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
	
	DELETE ps
	FROM [review].[tRatingItemProductScore] ps
		INNER JOIN [review].[tRatingItemProductVote] pv ON 
			pv.[RatingId] = ps.[RatingId] 
			AND pv.[RatingItemId] = ps.[RatingItemId]
		INNER JOIN #VariantIds vid ON
			vid.[VariantId] = ps.[ProductId]
	WHERE
		ps.[ChannelId] = @ChannelId
		AND pv.[RatingReviewFeedbackId] = @RatingReviewFeedbackId
		AND ps.[HitCount] <= 0
	
	IF OBJECT_ID('tempdb..#VariantIds') IS NOT NULL
		DROP TABLE #VariantIds
END
GO
