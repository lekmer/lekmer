SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pCategoryGetRecordById]
	@CategoryId	int
as
begin
	select
		*
	from
		[product].[vCustomCategorySecure] c
	where
		c.[Category.Id] = @CategoryId
end

GO
