SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockGetAllByTemplate]
	@TemplateId	int
as
begin
	select 
		B.*
	from 
		[sitestructure].[vCustomBlockSecure] as B
	WHERE 
		B.[Block.TemplateId] = @TemplateId
end

GO
