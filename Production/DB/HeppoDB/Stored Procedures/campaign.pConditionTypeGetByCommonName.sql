SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pConditionTypeGetByCommonName]
	@ConditionTypeCommonName	varchar(50)
as
begin
	SELECT
		*
	FROM
		campaign.vCustomConditionType
	WHERE 
		[ConditionType.CommonName] = @ConditionTypeCommonName
end

GO
