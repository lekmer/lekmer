SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pProductActionSave]
	@Id int,
	@ProductActionTypeId int,
	@ProductCampaignId int,
	@Ordinal int
as
begin
	UPDATE
		campaign.tProductAction
	SET
		ProductActionTypeId = @ProductActionTypeId,
		CampaignId = @ProductCampaignId,
		Ordinal = @Ordinal
	WHERE
		ProductActionId = @Id
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			campaign.tProductAction
		(
			ProductActionTypeId,
			CampaignId,
			Ordinal
		)
		VALUES
		(
			@ProductActionTypeId,
			@ProductCampaignId,
			@Ordinal
		)
		SET @Id = CAST(SCOPE_IDENTITY() AS INT)
	END
	RETURN @Id
end



GO
