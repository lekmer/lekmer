SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pBlockProductSearchEsalesResultGetById]
	@LanguageId INT ,
	@BlockId INT
AS 
BEGIN
	SET NOCOUNT ON

	SELECT
		ba.* ,
		b.*
	FROM
		[productlek].[vBlockProductSearchEsalesResult] AS ba
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON ba.[BlockProductSearchEsalesResult.BlockId] = b.[Block.BlockId]
	WHERE
		ba.[BlockProductSearchEsalesResult.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END

GO
