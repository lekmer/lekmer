CREATE TABLE [review].[tBlockLatestFeedbackList]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NULL,
[NumberOfItems] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockLatestFeedbackList] ADD CONSTRAINT [PK_tBlockLatestFeedbackList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockLatestFeedbackList] ADD CONSTRAINT [FK_tBlockLatestFeedbackList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [review].[tBlockLatestFeedbackList] ADD CONSTRAINT [FK_tBlockLatestFeedbackList_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
