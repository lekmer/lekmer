CREATE TABLE [lekmer].[tFlagTranslation]
(
[FlagId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tFlagTranslation] ADD CONSTRAINT [PK_tFlagTranslation] PRIMARY KEY CLUSTERED  ([FlagId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tFlagTranslation] ADD CONSTRAINT [FK_tFlagTranslation_tFlag] FOREIGN KEY ([FlagId]) REFERENCES [lekmer].[tFlag] ([FlagId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tFlagTranslation] ADD CONSTRAINT [FK_tFlagTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId]) ON DELETE CASCADE
GO
