CREATE TABLE [orderlek].[tCartItemPackageElement]
(
[CartItemPackageElementId] [int] NOT NULL IDENTITY(1, 1),
[CartItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [orderlek].[tCartItemPackageElement] ADD 
CONSTRAINT [PK_tCartItemPackageElement] PRIMARY KEY CLUSTERED  ([CartItemPackageElementId]) ON [PRIMARY]
ALTER TABLE [orderlek].[tCartItemPackageElement] ADD
CONSTRAINT [FK_tCartItemPackageElement_tCartItem] FOREIGN KEY ([CartItemId]) REFERENCES [order].[tCartItem] ([CartItemId])
ALTER TABLE [orderlek].[tCartItemPackageElement] ADD
CONSTRAINT [FK_tCartItemPackageElement_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
