CREATE TABLE [export].[tCdonExportRestrictionBrand]
(
[ProductRegistryId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[Reason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportRestrictionBrand] ADD CONSTRAINT [PK_tCdonExportRestrictionBrand] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportRestrictionBrand] ADD CONSTRAINT [FK_tCdonExportRestrictionBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [export].[tCdonExportRestrictionBrand] ADD CONSTRAINT [FK_tCdonExportRestrictionBrand_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
