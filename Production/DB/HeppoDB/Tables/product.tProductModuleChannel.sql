CREATE TABLE [product].[tProductModuleChannel]
(
[ChannelId] [int] NOT NULL,
[ProductTemplateContentNodeId] [int] NULL,
[PriceListRegistryId] [int] NULL,
[ProductRegistryId] [int] NULL
) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tProductModuleChannel_ChannelId] ON [product].[tProductModuleChannel] ([ChannelId]) INCLUDE ([PriceListRegistryId], [ProductRegistryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProductModuleChannel_ProductRegistryId] ON [product].[tProductModuleChannel] ([ProductRegistryId]) INCLUDE ([ChannelId], [PriceListRegistryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tProductModuleChannel_ContentNodeId] ON [product].[tProductModuleChannel] ([ProductTemplateContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductModuleChannel] ADD CONSTRAINT [FK_tProductModuleChannel_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [product].[tProductModuleChannel] ADD CONSTRAINT [FK_tProductModuleChannel_tPriceListRegistry] FOREIGN KEY ([PriceListRegistryId]) REFERENCES [product].[tPriceListRegistry] ([PriceListRegistryId])
GO
ALTER TABLE [product].[tProductModuleChannel] ADD CONSTRAINT [FK_tProductModuleChannel_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
ALTER TABLE [product].[tProductModuleChannel] ADD CONSTRAINT [FK_tProductModuleChannel_tContentPage] FOREIGN KEY ([ProductTemplateContentNodeId]) REFERENCES [sitestructure].[tContentPage] ([ContentNodeId])
GO
