CREATE TABLE [sitestructure].[tBlockTranslation]
(
[BlockId] [int] NOT NULL,
[LanguageId] [int] NOT NULL,
[Title] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockTranslation] ADD CONSTRAINT [PK_tBlockTranslation] PRIMARY KEY CLUSTERED  ([BlockId], [LanguageId]) ON [PRIMARY]
GO
ALTER TABLE [sitestructure].[tBlockTranslation] ADD CONSTRAINT [FK_tBlockTranslation_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [sitestructure].[tBlockTranslation] ADD CONSTRAINT [FK_tBlockTranslation_tLanguage] FOREIGN KEY ([LanguageId]) REFERENCES [core].[tLanguage] ([LanguageId])
GO
