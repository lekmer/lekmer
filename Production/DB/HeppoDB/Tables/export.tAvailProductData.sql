CREATE TABLE [export].[tAvailProductData]
(
[ProductId] [int] NOT NULL,
[PriceSE] [decimal] (16, 2) NOT NULL,
[PriceDK] [decimal] (16, 2) NOT NULL,
[PriceNO] [decimal] (16, 2) NOT NULL,
[PriceFI] [decimal] (16, 2) NOT NULL,
[PriceFR] [decimal] (16, 2) NOT NULL,
[PriceNL] [decimal] (16, 2) NOT NULL,
[DiscountPriceSE] [decimal] (16, 2) NOT NULL,
[DiscountPriceDK] [decimal] (16, 2) NOT NULL,
[DiscountPriceNO] [decimal] (16, 2) NOT NULL,
[DiscountPriceFI] [decimal] (16, 2) NOT NULL,
[DiscountPriceFR] [decimal] (16, 2) NOT NULL,
[DiscountPriceNL] [decimal] (16, 2) NOT NULL,
[CategoryUrlSE] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlNO] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlDA] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlFI] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlFR] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CategoryUrlNL] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ImageUrlSE] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlNO] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlDA] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlFI] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlFR] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[ImageUrlNL] [varchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[TitleSE] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleNO] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleDA] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleFI] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleFR] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[TitleNL] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[NumberInStock] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tAvailProductData] ADD CONSTRAINT [PK_tAvailProductData] PRIMARY KEY CLUSTERED  ([ProductId]) ON [PRIMARY]
GO
