CREATE TABLE [template].[tModelSettingType]
(
[ModelSettingTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelSettingType] ADD CONSTRAINT [PK_tModelSettingType] PRIMARY KEY CLUSTERED  ([ModelSettingTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tModelSettingType_CommonName] ON [template].[tModelSettingType] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tModelSettingType_Title] ON [template].[tModelSettingType] ([Title]) ON [PRIMARY]
GO
