CREATE TABLE [lekmer].[tBatteryType]
(
[BatteryTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBatteryType] ADD CONSTRAINT [PK_tBatteryType] PRIMARY KEY CLUSTERED  ([BatteryTypeId]) ON [PRIMARY]
GO
