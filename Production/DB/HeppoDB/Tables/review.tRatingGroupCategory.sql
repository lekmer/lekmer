CREATE TABLE [review].[tRatingGroupCategory]
(
[RatingGroupId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubCategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingGroupCategory] ADD CONSTRAINT [PK_tRatingGroupCategory] PRIMARY KEY CLUSTERED  ([RatingGroupId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingGroupCategory] ADD CONSTRAINT [FK_tRatingGroupCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [review].[tRatingGroupCategory] ADD CONSTRAINT [FK_tRatingGroupCategory_tRatingGroup] FOREIGN KEY ([RatingGroupId]) REFERENCES [review].[tRatingGroup] ([RatingGroupId])
GO
