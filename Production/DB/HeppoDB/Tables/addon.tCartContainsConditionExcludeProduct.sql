CREATE TABLE [addon].[tCartContainsConditionExcludeProduct]
(
[ConditionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeProduct] ADD CONSTRAINT [PK_tCartContainsConditionExcludeProduct] PRIMARY KEY CLUSTERED  ([ConditionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeProduct] ADD CONSTRAINT [FK_tCartContainsConditionExcludeProduct_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId])
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeProduct] ADD CONSTRAINT [FK_tCartContainsConditionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId]) ON DELETE CASCADE
GO
