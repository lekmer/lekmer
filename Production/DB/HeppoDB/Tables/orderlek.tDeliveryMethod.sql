CREATE TABLE [orderlek].[tDeliveryMethod]
(
[DeliveryMethodId] [int] NOT NULL,
[Priority] [int] NOT NULL,
[IsCompany] [bit] NOT NULL CONSTRAINT [DF_tDeliveryMethod_IsCompany] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tDeliveryMethod] ADD CONSTRAINT [PK_tDeliveryMethod] PRIMARY KEY CLUSTERED  ([DeliveryMethodId]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tDeliveryMethod] ADD CONSTRAINT [FK_tDeliveryMethod(lek)_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
