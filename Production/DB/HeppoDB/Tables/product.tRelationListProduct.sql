CREATE TABLE [product].[tRelationListProduct]
(
[RelationListId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tRelationListProduct] ADD CONSTRAINT [PK_tRelationListProduct] PRIMARY KEY CLUSTERED  ([RelationListId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tRelationListProduct] ADD CONSTRAINT [FK_tRelationListProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tRelationListProduct] ADD CONSTRAINT [FK_tRelationListProduct_tRelationList] FOREIGN KEY ([RelationListId]) REFERENCES [product].[tRelationList] ([RelationListId])
GO
