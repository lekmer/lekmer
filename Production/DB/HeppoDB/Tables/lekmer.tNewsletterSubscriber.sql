CREATE TABLE [lekmer].[tNewsletterSubscriber]
(
[SubscriberId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SubscriberTypeId] [int] NOT NULL,
[SentStatus] [bit] NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tNewsletterSubscriber] ADD 
CONSTRAINT [PK_tNewsletterSubscriber] PRIMARY KEY CLUSTERED  ([SubscriberId]) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tNewsletterSubscriber_ChannelId] ON [lekmer].[tNewsletterSubscriber] ([ChannelId]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tNewsletterSubscriber_Email] ON [lekmer].[tNewsletterSubscriber] ([Email]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tNewsletterSubscriber_SentStatus] ON [lekmer].[tNewsletterSubscriber] ([SentStatus]) ON [PRIMARY]

ALTER TABLE [lekmer].[tNewsletterSubscriber] ADD
CONSTRAINT [FK_tNewsletterSubscriber_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
ALTER TABLE [lekmer].[tNewsletterSubscriber] ADD
CONSTRAINT [FK_tNewsletterSubscriber_tNewslettertSubscriberType] FOREIGN KEY ([SubscriberTypeId]) REFERENCES [lekmer].[tNewsletterSubscriberType] ([NewsletterSubscriberTypeId])




GO
