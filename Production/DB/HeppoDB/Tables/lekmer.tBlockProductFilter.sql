CREATE TABLE [lekmer].[tBlockProductFilter]
(
[BlockId] [int] NOT NULL,
[SecondaryTemplateId] [int] NULL,
[DefaultCategoryId] [int] NULL,
[DefaultPriceIntervalId] [int] NULL,
[DefaultAgeIntervalId] [int] NULL,
[DefaultSizeId] [int] NULL,
[ProductListCookie] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[DefaultSort] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD CONSTRAINT [PK_tBlockProductFilter] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductFilter_DefaultAgeIntervalId] ON [lekmer].[tBlockProductFilter] ([DefaultAgeIntervalId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductFilter_DefaultCategoryId] ON [lekmer].[tBlockProductFilter] ([DefaultCategoryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductFilter_DefaultPriceIntervalId] ON [lekmer].[tBlockProductFilter] ([DefaultPriceIntervalId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockProductFilter_SecondaryTemplateId] ON [lekmer].[tBlockProductFilter] ([SecondaryTemplateId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD CONSTRAINT [FK_tBlockProductFilter_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD CONSTRAINT [FK_tBlockProductFilter_tAgeInterval] FOREIGN KEY ([DefaultAgeIntervalId]) REFERENCES [lekmer].[tAgeInterval] ([AgeIntervalId])
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD CONSTRAINT [FK_tBlockProductFilter_tCategory] FOREIGN KEY ([DefaultCategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD CONSTRAINT [FK_tBlockProductFilter_tPriceInterval] FOREIGN KEY ([DefaultPriceIntervalId]) REFERENCES [lekmer].[tPriceInterval] ([PriceIntervalId])
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD CONSTRAINT [FK_tBlockProductFilter_tSize] FOREIGN KEY ([DefaultSizeId]) REFERENCES [lekmer].[tSize] ([SizeId])
GO
ALTER TABLE [lekmer].[tBlockProductFilter] ADD CONSTRAINT [FK_tBlockProductFilter_tTemplate] FOREIGN KEY ([SecondaryTemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
