CREATE TABLE [campaignlek].[tFixedPriceActionIncludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeBrand] ADD CONSTRAINT [PK_tFixedPriceActionIncludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeBrand] ADD CONSTRAINT [FK_tFixedPriceActionIncludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeBrand] ADD CONSTRAINT [FK_tFixedPriceActionIncludeBrand_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
GO
