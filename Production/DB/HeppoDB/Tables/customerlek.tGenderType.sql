CREATE TABLE [customerlek].[tGenderType]
(
[GenderTypeId] [int] NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customerlek].[tGenderType] ADD CONSTRAINT [PK_tGenderType] PRIMARY KEY CLUSTERED  ([GenderTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tGenderType_ErpId] ON [customerlek].[tGenderType] ([ErpId]) ON [PRIMARY]
GO
