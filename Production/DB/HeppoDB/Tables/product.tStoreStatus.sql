CREATE TABLE [product].[tStoreStatus]
(
[StoreStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tStoreStatus] ADD CONSTRAINT [PK_tStoreStatus] PRIMARY KEY CLUSTERED  ([StoreStatusId]) ON [PRIMARY]
GO
