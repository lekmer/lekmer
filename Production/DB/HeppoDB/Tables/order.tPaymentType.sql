CREATE TABLE [order].[tPaymentType]
(
[PaymentTypeId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ErpId] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tPaymentType] ADD CONSTRAINT [PK_tPaymentType] PRIMARY KEY CLUSTERED  ([PaymentTypeId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tPaymentType] ADD CONSTRAINT [UQ_tPaymentType_CommonName] UNIQUE NONCLUSTERED  ([CommonName]) ON [PRIMARY]
GO
ALTER TABLE [order].[tPaymentType] ADD CONSTRAINT [UQ_tPaymentType_Title] UNIQUE NONCLUSTERED  ([Title]) ON [PRIMARY]
GO
