CREATE TABLE [template].[tModelFragment]
(
[ModelFragmentId] [int] NOT NULL IDENTITY(1, 1),
[ModelFragmentRegionId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ModelId] [int] NOT NULL,
[Ordinal] [int] NULL,
[Size] [int] NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tModelFragment_ModelId_Title] ON [template].[tModelFragment] ([ModelId], [Title]) ON [PRIMARY]

GO
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [PK_tModelFragment] PRIMARY KEY CLUSTERED  ([ModelFragmentId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tModelFragment_ModelFragmentRegionId] ON [template].[tModelFragment] ([ModelFragmentRegionId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tModelFragment_ModelId_CommonName] ON [template].[tModelFragment] ([ModelId], [CommonName]) ON [PRIMARY]
GO

ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModelFragmentRegion] FOREIGN KEY ([ModelFragmentRegionId]) REFERENCES [template].[tModelFragmentRegion] ([ModelFragmentRegionId])
GO
ALTER TABLE [template].[tModelFragment] ADD CONSTRAINT [FK_tModelFragment_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
