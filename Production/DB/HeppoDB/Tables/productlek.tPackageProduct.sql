CREATE TABLE [productlek].[tPackageProduct]
(
[PackageId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [PK_tPackageProduct] PRIMARY KEY CLUSTERED  ([PackageId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [FK_tPackageProduct_tPackage] FOREIGN KEY ([PackageId]) REFERENCES [productlek].[tPackage] ([PackageId])
GO
ALTER TABLE [productlek].[tPackageProduct] ADD CONSTRAINT [FK_tPackageProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
