CREATE TABLE [lekmer].[tBlockBrandTopList]
(
[BlockId] [int] NOT NULL,
[ColumnCount] [int] NOT NULL,
[RowCount] [int] NOT NULL,
[TotalBrandCount] [int] NOT NULL,
[IncludeAllCategories] [bit] NOT NULL,
[OrderStatisticsDayCount] [int] NOT NULL,
[LinkContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandTopList] ADD CONSTRAINT [PK_tBlockBrandTopList] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tBlockBrandTopList] ADD CONSTRAINT [FK_tBlockBrandTopList_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [lekmer].[tBlockBrandTopList] ADD CONSTRAINT [FK_tBlockBrandTopList_tContentNode] FOREIGN KEY ([LinkContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
