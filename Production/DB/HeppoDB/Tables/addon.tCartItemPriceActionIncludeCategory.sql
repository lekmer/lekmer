CREATE TABLE [addon].[tCartItemPriceActionIncludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeCategory] ADD CONSTRAINT [PK_tCartItemPriceActionIncludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeCategory] ADD CONSTRAINT [FK_tCartItemPriceActionCategory_tCartItemPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId])
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeCategory] ADD CONSTRAINT [FK_tCartItemPriceActionCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
