CREATE TABLE [campaignlek].[tFixedPriceActionIncludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeProduct] ADD CONSTRAINT [PK_tFixedPriceActionIncludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeProduct] ADD CONSTRAINT [FK_tFixedPriceActionIncludeProduct_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
GO
ALTER TABLE [campaignlek].[tFixedPriceActionIncludeProduct] ADD CONSTRAINT [FK_tFixedPriceActionIncludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
