CREATE TABLE [lekmer].[tFreeFreightFlagAction]
(
[ProductActionId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tFreeFreightFlagAction] ADD CONSTRAINT [PK_tFreeFreightFlagAction] PRIMARY KEY CLUSTERED  ([ProductActionId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tFreeFreightFlagAction] ADD CONSTRAINT [FK_tFreeFreightFlagAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId]) ON DELETE CASCADE
GO
