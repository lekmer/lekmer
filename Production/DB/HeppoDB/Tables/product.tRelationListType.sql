CREATE TABLE [product].[tRelationListType]
(
[RelationListTypeId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tRelationListType] ADD CONSTRAINT [PK_tRelationListType] PRIMARY KEY CLUSTERED  ([RelationListTypeId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tRelationListType] ADD CONSTRAINT [UQ_tRelationListType_CommonName] UNIQUE NONCLUSTERED  ([RelationListTypeId]) ON [PRIMARY]
GO
