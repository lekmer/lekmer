CREATE TABLE [statistics].[tDailySearch]
(
[ChannelId] [int] NOT NULL,
[Query] [nvarchar] (400) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[HitCount] [int] NOT NULL,
[SearchCount] [int] NOT NULL,
[Date] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [statistics].[tDailySearch] ADD CONSTRAINT [PK_tDailySearch] PRIMARY KEY CLUSTERED  ([ChannelId], [Query], [Date]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tDailySearch] ON [statistics].[tDailySearch] ([ChannelId], [Date]) INCLUDE ([HitCount], [Query], [SearchCount]) ON [PRIMARY]
GO
