CREATE TABLE [review].[tRatingGroupFolder]
(
[RatingGroupFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentRatingGroupFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingGroupFolder] ADD CONSTRAINT [PK_tRatingGroupFolder] PRIMARY KEY CLUSTERED  ([RatingGroupFolderId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingGroupFolder] ADD CONSTRAINT [FK_tRatingGroupFolder_tRatingGroupFolder] FOREIGN KEY ([ParentRatingGroupFolderId]) REFERENCES [review].[tRatingGroupFolder] ([RatingGroupFolderId])
GO
