CREATE TABLE [lekmer].[tCartItemFixedDiscountAction]
(
[CartActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountAction] ADD CONSTRAINT [PK_tCartItemFixedDiscountAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemFixedDiscountAction] ADD CONSTRAINT [FK_tCartItemFixedDiscountAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
