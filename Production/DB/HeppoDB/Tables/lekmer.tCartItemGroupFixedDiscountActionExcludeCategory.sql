CREATE TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemGroupFixedDiscountActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeCategory_tCartItemGroupFixedDiscountAction] FOREIGN KEY ([CartActionId]) REFERENCES [lekmer].[tCartItemGroupFixedDiscountAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCartItemGroupFixedDiscountActionExcludeCategory] ADD CONSTRAINT [FK_tCartItemGroupFixedDiscountActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
