CREATE TABLE [lekmer].[tMonitorProduct]
(
[MonitorProductId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[SizeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tMonitorProduct] ADD CONSTRAINT [PK_tMonitorProduct] PRIMARY KEY CLUSTERED  ([MonitorProductId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tMonitorProduct] ADD CONSTRAINT [FK_tMonitorProduct_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [lekmer].[tMonitorProduct] ADD CONSTRAINT [FK_tMonitorProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [lekmer].[tMonitorProduct] ADD CONSTRAINT [FK_tMonitorProduct_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
