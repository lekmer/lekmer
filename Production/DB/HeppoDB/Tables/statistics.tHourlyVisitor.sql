CREATE TABLE [statistics].[tHourlyVisitor]
(
[ChannelId] [int] NOT NULL,
[Alternate] [bit] NOT NULL,
[Year] [smallint] NOT NULL,
[Month] [tinyint] NOT NULL,
[Day] [tinyint] NOT NULL,
[Hour] [tinyint] NOT NULL,
[VisitCount] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [statistics].[tHourlyVisitor] ADD CONSTRAINT [PK_tHourlyVisitor] PRIMARY KEY CLUSTERED  ([ChannelId], [Alternate], [Year], [Month], [Day], [Hour]) ON [PRIMARY]
GO
