CREATE TABLE [lekmer].[tVoucherBatchesInclude]
(
[ConditionId] [int] NOT NULL,
[BatchId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherBatchesInclude] ADD CONSTRAINT [PK_tVoucherBatchesInclude] PRIMARY KEY CLUSTERED  ([ConditionId], [BatchId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tVoucherBatchesInclude] ADD CONSTRAINT [FK_tVoucherBatchesInclude_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
