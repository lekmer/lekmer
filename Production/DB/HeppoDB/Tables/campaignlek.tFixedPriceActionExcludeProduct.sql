CREATE TABLE [campaignlek].[tFixedPriceActionExcludeProduct]
(
[ProductActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeProduct] ADD CONSTRAINT [PK_tFixedPriceActionExcludeProduct] PRIMARY KEY CLUSTERED  ([ProductActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeProduct] ADD CONSTRAINT [FK_tFixedPriceActionExcludeProduct_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeProduct] ADD CONSTRAINT [FK_tFixedPriceActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
