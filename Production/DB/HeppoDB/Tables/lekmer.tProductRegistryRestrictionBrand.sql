CREATE TABLE [lekmer].[tProductRegistryRestrictionBrand]
(
[ProductRegistryId] [int] NOT NULL,
[BrandId] [int] NOT NULL,
[RestrictionReason] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NULL,
[UserId] [int] NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionBrand] ADD CONSTRAINT [PK_tProductRegistryRestrictionBrand] PRIMARY KEY CLUSTERED  ([ProductRegistryId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionBrand] ADD CONSTRAINT [FK_tProductRegistryRestrictionBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [lekmer].[tProductRegistryRestrictionBrand] ADD CONSTRAINT [FK_tProductRegistryRestrictionBrand_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
