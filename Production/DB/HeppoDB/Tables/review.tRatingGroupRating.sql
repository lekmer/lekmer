CREATE TABLE [review].[tRatingGroupRating]
(
[RatingGroupId] [int] NOT NULL,
[RatingId] [int] NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingGroupRating] ADD CONSTRAINT [PK_tRatingGroupRating] PRIMARY KEY CLUSTERED  ([RatingGroupId], [RatingId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingGroupRating] ADD CONSTRAINT [FK_tRatingGroupRating_tRatingGroup] FOREIGN KEY ([RatingGroupId]) REFERENCES [review].[tRatingGroup] ([RatingGroupId])
GO
ALTER TABLE [review].[tRatingGroupRating] ADD CONSTRAINT [FK_tRatingGroupRating_tRating] FOREIGN KEY ([RatingId]) REFERENCES [review].[tRating] ([RatingId])
GO
