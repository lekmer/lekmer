CREATE TABLE [productlek].[tProductType]
(
[ProductTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tProductType] ADD CONSTRAINT [PK_tProductType] PRIMARY KEY CLUSTERED  ([ProductTypeId]) ON [PRIMARY]
GO
