CREATE TABLE [campaignlek].[tGiftCardViaMailInfoStatus]
(
[StatusId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailInfoStatus] ADD CONSTRAINT [PK_tGiftCardViaMailInfoStatus] PRIMARY KEY CLUSTERED  ([StatusId]) ON [PRIMARY]
GO
