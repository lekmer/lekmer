CREATE TABLE [product].[tProductRelationList]
(
[ProductId] [int] NOT NULL,
[RelationListId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductRelationList] ADD CONSTRAINT [PK_tProductRelationList] PRIMARY KEY CLUSTERED  ([ProductId], [RelationListId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tProductRelationList] ADD CONSTRAINT [FK_tProductRelationList_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
ALTER TABLE [product].[tProductRelationList] ADD CONSTRAINT [FK_tProductRelationList_tRelationList] FOREIGN KEY ([RelationListId]) REFERENCES [product].[tRelationList] ([RelationListId])
GO
