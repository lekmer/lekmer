CREATE TABLE [campaignlek].[tFixedDiscountAction]
(
[ProductActionId] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tFixedDiscountAction_IncludeAllProducts] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountAction] ADD CONSTRAINT [PK_tFixedDiscountAction] PRIMARY KEY CLUSTERED  ([ProductActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedDiscountAction] ADD CONSTRAINT [FK_tFixedDiscountAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
GO
