CREATE TABLE [core].[tCountry]
(
[CountryId] [int] NOT NULL IDENTITY(1, 1),
[ISO] [char] (2) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[IsInEU] [bit] NOT NULL CONSTRAINT [DF_tCountry_IsInEU] DEFAULT ((0)),
[PhonePrefix] [char] (4) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (64) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [core].[tCountry] ADD CONSTRAINT [PK_tCountry] PRIMARY KEY CLUSTERED  ([CountryId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCountry_PhonePrefix] ON [core].[tCountry] ([PhonePrefix]) ON [PRIMARY]
GO
