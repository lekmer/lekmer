CREATE TABLE [product].[tVariation]
(
[VariationGroupId] [int] NOT NULL,
[VariationId] [int] NOT NULL IDENTITY(1, 1),
[VariationTypeId] [int] NOT NULL,
[Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tVariation_VariationType_Value] ON [product].[tVariation] ([VariationTypeId], [Value]) ON [PRIMARY]

GO
ALTER TABLE [product].[tVariation] ADD CONSTRAINT [PK_tVariation] PRIMARY KEY CLUSTERED  ([VariationGroupId], [VariationId]) ON [PRIMARY]
GO
ALTER TABLE [product].[tVariation] ADD CONSTRAINT [UQ_tVariation] UNIQUE NONCLUSTERED  ([VariationId]) ON [PRIMARY]
GO

ALTER TABLE [product].[tVariation] ADD CONSTRAINT [FK_tVariation_tVariationType] FOREIGN KEY ([VariationGroupId], [VariationTypeId]) REFERENCES [product].[tVariationType] ([VariationGroupId], [VariationTypeId])
GO
