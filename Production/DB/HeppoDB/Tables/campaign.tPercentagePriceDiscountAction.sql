CREATE TABLE [campaign].[tPercentagePriceDiscountAction]
(
[ProductActionId] [int] NOT NULL,
[DiscountAmount] [decimal] (16, 2) NOT NULL,
[ConfigId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD 
CONSTRAINT [PK_tPercentagePriceDiscountAction] PRIMARY KEY CLUSTERED  ([ProductActionId]) ON [PRIMARY]
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD
CONSTRAINT [FK_tPercentagePriceDiscountAction_tProductAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaign].[tProductAction] ([ProductActionId])
ALTER TABLE [campaign].[tPercentagePriceDiscountAction] ADD
CONSTRAINT [FK_tPercentagePriceDiscountAction_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
