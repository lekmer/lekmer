CREATE TABLE [campaignlek].[tProductAutoFreeCartAction]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Quantity] [int] NOT NULL CONSTRAINT [DF_tProductAutoFreeCartAction_Quantity] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tProductAutoFreeCartAction] ADD CONSTRAINT [PK_tProductAutoFreeCartAction] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tProductAutoFreeCartAction] ADD CONSTRAINT [FK_tProductAutoFreeCartAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
ALTER TABLE [campaignlek].[tProductAutoFreeCartAction] ADD CONSTRAINT [FK_tProductAutoFreeCartAction_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
