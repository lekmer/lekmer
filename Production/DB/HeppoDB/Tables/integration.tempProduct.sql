CREATE TABLE [integration].[tempProduct]
(
[HYarticleId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[HYFokArticle] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ArticleTitle] [nvarchar] (1000) COLLATE Finnish_Swedish_CI_AS NULL,
[SizeLabel] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[SizeId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ColorLabel] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ColorId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Price] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[NoInStock] [nvarchar] (25) COLLATE Finnish_Swedish_CI_AS NULL,
[Length] [nvarchar] (4000) COLLATE Finnish_Swedish_CI_AS NULL,
[Width] [nvarchar] (4000) COLLATE Finnish_Swedish_CI_AS NULL,
[Hight] [decimal] (18, 0) NULL,
[Weight] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[PossibleToDisplay] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[InternetDate] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[PriceText] [nvarchar] (2500) COLLATE Finnish_Swedish_CI_AS NULL,
[Enh] [nvarchar] (4000) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleGroupId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleGroupTitle] [nvarchar] (40) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleClassId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleClassTitle] [nvarchar] (40) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleCodeId] [nvarchar] (10) COLLATE Finnish_Swedish_CI_AS NULL,
[ArticleCodeTitle] [nvarchar] (40) COLLATE Finnish_Swedish_CI_AS NULL,
[Ref1] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Ref2] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Ref3] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[SupplierId] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[Kalkyl] [nvarchar] (2500) COLLATE Finnish_Swedish_CI_AS NULL,
[BrandId] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[BrandTitle] [nvarchar] (2500) COLLATE Finnish_Swedish_CI_AS NULL,
[LagArtMarknTypeEan] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[EanCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[LagArtMarknType04] [nvarchar] (500) COLLATE Finnish_Swedish_CI_AS NULL,
[04] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[LagArtMarknTypeLekmerArtNo] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[LekmerArtNo] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[LagArtMarknTypeLekmerId] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[LekmerId] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[SupplierSize] [nvarchar] (15) COLLATE Finnish_Swedish_CI_AS NULL,
[Period] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tempProduct] ADD 
CONSTRAINT [PK_tempProduct] PRIMARY KEY CLUSTERED  ([HYarticleId]) ON [PRIMARY]
GO
