CREATE TABLE [corelek].[tSubDomainUrlType]
(
[SubDomainUrlTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [corelek].[tSubDomainUrlType] ADD 
CONSTRAINT [PK_tSubDomainUrlType] PRIMARY KEY CLUSTERED  ([SubDomainUrlTypeId]) ON [PRIMARY]
GO
