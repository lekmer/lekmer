CREATE TABLE [review].[tBlockBestRatedProductListBrand]
(
[BlockId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockBestRatedProductListBrand] ADD CONSTRAINT [PK_tBlockBestRatedProductListBrand] PRIMARY KEY CLUSTERED  ([BlockId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tBlockBestRatedProductListBrand] ADD CONSTRAINT [FK_tBlockBestRatedProductListBrand_tBlockBestRatedProductList] FOREIGN KEY ([BlockId]) REFERENCES [review].[tBlockBestRatedProductList] ([BlockId])
GO
ALTER TABLE [review].[tBlockBestRatedProductListBrand] ADD CONSTRAINT [FK_tBlockBestRatedProductListBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
