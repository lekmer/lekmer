CREATE TABLE [addon].[tCartContainsConditionExcludeBrand]
(
[ConditionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeBrand] ADD CONSTRAINT [PK_tCartContainsConditionExcludeBrand] PRIMARY KEY CLUSTERED  ([ConditionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeBrand] ADD CONSTRAINT [FK_tCartContainsConditionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId]) ON DELETE CASCADE
GO
ALTER TABLE [addon].[tCartContainsConditionExcludeBrand] ADD CONSTRAINT [FK_tCartContainsConditionExcludeBrand_tCartContainsCondition] FOREIGN KEY ([ConditionId]) REFERENCES [addon].[tCartContainsCondition] ([ConditionId])
GO
