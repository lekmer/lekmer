CREATE TABLE [customer].[tCustomerUser]
(
[CustomerId] [int] NOT NULL,
[CustomerRegistryId] [int] NOT NULL,
[UserName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Password] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[PasswordSalt] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerUser] ADD CONSTRAINT [PK_tCustomerUser] PRIMARY KEY CLUSTERED  ([CustomerId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tCustomerUser_UserName] ON [customer].[tCustomerUser] ([UserName]) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerUser] ADD CONSTRAINT [FK_tCustomer_tCustomerUser] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
