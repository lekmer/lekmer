SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [statistics].fnGetViewMode
(
	@DateTimeFrom	smalldatetime,
	@DateTimeTo		smalldatetime
)
RETURNS varchar(10) 
AS 
begin	 	
	RETURN CASE 1
		WHEN datediff(day, @DateTimeFrom, @DateTimeTo) THEN 'Day'
		WHEN datediff(week, @DateTimeFrom, @DateTimeTo) THEN 'Week'
		WHEN datediff(month, @DateTimeFrom, @DateTimeTo) THEN 'Month'
		WHEN datediff(year, @DateTimeFrom, @DateTimeTo) THEN 'Year'
	END
END 
GO
