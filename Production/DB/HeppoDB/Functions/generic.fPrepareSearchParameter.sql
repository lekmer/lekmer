SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
Create FUNCTION [generic].[fPrepareSearchParameter]
(
	@SearchName	VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @Result VARCHAR(MAX)
	
	SET @Result = REPLACE(@SearchName, '\', '\\')
	SET @Result = REPLACE(@Result, '_', '\_')
	SET @Result = REPLACE(@Result, '%', '\%')
	SET @Result = REPLACE(@Result, '[', '\[')
	SET @Result = REPLACE(@Result, ']', '\]')
	SET @Result = '%' + @Result + '%'
	
	RETURN @Result
END
GO
