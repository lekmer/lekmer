SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [product].[fGetVariantErpIdByProductId] 
(
	@ProductId	INT
)
RETURNS VARCHAR(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @VariantErpId VARCHAR(50)
	SET @VariantErpId = (SELECT [ErpId] FROM [product].[tProduct] WHERE [ProductId] = @ProductId)

	DECLARE @SeparatorPos INT
	SET @SeparatorPos = CHARINDEX('-', @VariantErpId)

	IF (@SeparatorPos > 0)
	BEGIN
		SET @VariantErpId = SUBSTRING(@VariantErpId, 0, @SeparatorPos)
	END
	ELSE
	BEGIN
		SET @VariantErpId = ''
	END

	-- Return the result of the function
	RETURN @VariantErpId
END
GO
