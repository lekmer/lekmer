SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [statistics].[fnGetDateTimeGrid](
	@StartDateTime smalldatetime,
	@EndDateTime smalldatetime,
	@Mode varchar(10)
)
RETURNS @tDateTimeGrid TABLE ([DateTime] smalldatetime)
AS 
BEGIN 
	DECLARE @TimeSpan smalldatetime
	SET @TimeSpan = [statistics].[fnGetDateTimeKeyDirectly](@StartDateTime,	@Mode)
	
	WHILE (@TimeSpan <= @EndDateTime)
	BEGIN
		INSERT INTO @tDateTimeGrid ([DateTime])
		SELECT @TimeSpan
		SET @TimeSpan = CASE @Mode
								WHEN 'Day' THEN dateadd(hour, 1, @TimeSpan)
								WHEN 'Week' THEN dateadd(day, 1, @TimeSpan)
								WHEN 'Month' THEN dateadd(day, 1, @TimeSpan)
								WHEN 'Year' THEN dateadd(month, 1, @TimeSpan)
							END
	END
	RETURN 
END 
GO
