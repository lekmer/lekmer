SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [generic].[fGetCategoryProductUrl]
(
	@ApplicationName VARCHAR(100),
	@CategoryPath VARCHAR(1000),
	@LanguageId INT,
	@ProductId INT
)
RETURNS VARCHAR(1000)
AS
BEGIN
	DECLARE @ProductUrlTitle VARCHAR(1000)

	SELECT	@ProductUrlTitle = UrlTitle 
	FROM	lekmer.tProductUrl 
	WHERE	ProductId = @ProductId AND LanguageId = @LanguageId

	SET @ProductUrlTitle = REPLACE(@ProductUrlTitle, ' ', '-')

	RETURN 'http://' + @ApplicationName + '/' + @CategoryPath + @ProductUrlTitle
END
GO
