SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vContentArea]
AS
	select 
		[ContentAreaId] 'ContentArea.Id',
		[TemplateId] 'ContentArea.TemplateId',
		[Title] 'ContentArea.Title',
		[CommonName] 'ContentArea.CommonName'
	from 
		[sitestructure].[tContentArea]

GO
