SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCampaignFolder]
as
	select
		*
	from
		[campaign].[vCampaignFolder]
GO
