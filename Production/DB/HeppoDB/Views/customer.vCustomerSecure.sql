SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [customer].[vCustomerSecure]
AS
SELECT     
	C.CustomerId AS 'Customer.CustomerId',
	C.ErpId AS 'Customer.ErpId',	
	C.CustomerStatusId AS 'Customer.CustomerStatusId',
	C.CustomerRegistryId AS 'Customer.CustomerRegistryId',	
	CI.CustomerId AS 'CustomerInformation.InformationId',
	CI.FirstName AS 'CustomerInformation.FirstName',
	CI.LastName AS 'CustomerInformation.LastName',
	CI.CivicNumber AS 'CustomerInformation.CivicNumber',
	CI.PhoneNumber AS 'CustomerInformation.PhoneNumber',
	CI.CellPhoneNumber AS 'CustomerInformation.CellPhoneNumber',
	CI.Email AS 'CustomerInformation.Email',
	CI.CreatedDate AS 'CustomerInformation.InformationCreatedDate',
	CI.DefaultBillingAddressId AS 'CustomerInformation.DefaultBillingAddressId',
	CI.DefaultDeliveryAddressId AS 'CustomerInformation.DefaultDeliveryAddressId',
	BA.[Address.AddressId] AS 'BillingAddress.AddressId',
	BA.[Address.CustomerId] AS  'BillingAddress.CustomerId',
	BA.[Address.AddressTypeId] AS 'BillingAddress.AddressTypeId',
	BA.[Address.Addressee] AS 'BillingAddress.Addressee',
	BA.[Address.StreetAddress] AS 'BillingAddress.StreetAddress',
	BA.[Address.StreetAddress2] AS 'BillingAddress.StreetAddress2',
	BA.[Address.PostalCode] AS 'BillingAddress.PostalCode',
	BA.[Address.City] AS 'BillingAddress.City',
	BA.[Address.CountryId] AS 'BillingAddress.CountryId',
	BA.[Address.PhoneNumber] AS 'BillingAddress.PhoneNumber',
	DA.[Address.AddressId] AS 'DeliveryAddress.AddressId',
	DA.[Address.CustomerId] AS  'DeliveryAddress.CustomerId',
	DA.[Address.AddressTypeId] AS 'DeliveryAddress.AddressTypeId',
	DA.[Address.Addressee] AS 'DeliveryAddress.Addressee',
	DA.[Address.StreetAddress] AS 'DeliveryAddress.StreetAddress',
	DA.[Address.StreetAddress2] AS 'DeliveryAddress.StreetAddress2',
	DA.[Address.PostalCode] AS 'DeliveryAddress.PostalCode',
	DA.[Address.City]  AS 'DeliveryAddress.City',
	DA.[Address.CountryId] AS 'DeliveryAddress.CountryId',
	DA.[Address.PhoneNumber] AS 'DeliveryAddress.PhoneNumber',
	CU.CustomerId AS 'CustomerUser.UserId',
	CU.CustomerRegistryId AS 'CustomerUser.UserRegistryId',
	CU.UserName AS 'CustomerUser.UserName',
	CU.Password AS 'CustomerUser.Password',
	CU.PasswordSalt AS 'CustomerUser.PasswordSalt',
	CU.CreatedDate AS 'CustomerUser.UserCreatedDate'
FROM
	[customer].[tCustomer] as C 
	INNER JOIN [customer].[tCustomerInformation] as CI on C.CustomerId = CI.CustomerId
	LEFT JOIN  [customer].tCustomerUser as CU ON C.CustomerId = CU.CustomerId
	LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = CI.DefaultBillingAddressId
	LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = CI.DefaultDeliveryAddressId

GO
