
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [campaignlek].[vCartItemDiscountCartAction]
AS
	SELECT
		cid.[CartActionId]				AS 'CartItemDiscountCartAction.CartActionId',
		cid.[Quantity]					AS 'CartItemDiscountCartAction.Quantity',
		cid.[ApplyForCheapest]			AS 'CartItemDiscountCartAction.ApplyForCheapest',
		cid.[IsPercentageDiscount]		AS 'CartItemDiscountCartAction.IsPercentageDiscount',
		cid.[PercentageDiscountAmount]	AS 'CartItemDiscountCartAction.PercentageDiscountAmount',
		cid.[IncludeAllProducts]		AS 'CartItemDiscountCartAction.IncludeAllProducts',
		ca.*
	FROM
		[campaignlek].[tCartItemDiscountCartAction] cid
		INNER JOIN [campaign].[vCustomCartAction] ca ON ca.[CartAction.Id] = cid.[CartActionId]


GO
