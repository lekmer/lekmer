SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vVoucherCondition]
AS
	SELECT
		c.*,
		[vc].[IncludeAllBatchIds] AS 'VoucherCondition.IncludeAllBatchIds'
	FROM
		[lekmer].[tVoucherCondition] vc
		INNER JOIN [campaign].[vCustomCondition] c ON [vc].[ConditionId] = [c].[Condition.Id]
GO
