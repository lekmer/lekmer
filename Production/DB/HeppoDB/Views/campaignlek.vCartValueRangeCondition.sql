SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vCartValueRangeCondition]
AS
	SELECT
		c.*
	FROM
		[campaignlek].[tCartValueRangeCondition] cvrc
		INNER JOIN [campaign].[vCustomCondition] c ON c.[Condition.Id] = cvrc.[ConditionId]
GO
