SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomPriceList]
as
	select
		*
	from
		[product].[vPriceList]
GO
