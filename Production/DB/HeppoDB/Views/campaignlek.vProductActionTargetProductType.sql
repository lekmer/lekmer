SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vProductActionTargetProductType]
AS
	SELECT
		[ProductActionId] AS 'ProductActionTargetProductType.ProductActionId',
		[ProductTypeId] AS 'ProductActionTargetProductType.ProductTypeId'
	FROM
		[campaignlek].[tProductActionTargetProductType] t

GO
