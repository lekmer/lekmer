SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [addon].[vBlockProductReview]
AS
 SELECT 
         r.[BlockId] 'BlockProductReview.BlockId',
         r.[RowCount] 'BlockProductReview.RowCount'
 FROM 
         addon.tBlockProductReview r

GO
