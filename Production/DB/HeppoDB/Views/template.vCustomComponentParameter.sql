SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomComponentParameter]
as
	select
		*
	from
		[template].[vComponentParameter]
GO
