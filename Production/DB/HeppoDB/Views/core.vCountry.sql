SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [core].[vCountry]
AS
select
	CountryId 'Country.Id',
	[Title] 'Country.Title',
	--CurrencyId,
	ISO  'Country.ISO',
	IsInEU  'Country.IsInEU',
	PhonePrefix   'Country.PhonePrefix'
from
	core.tCountry
GO
