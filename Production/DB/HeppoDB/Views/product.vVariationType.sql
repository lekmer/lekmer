SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [product].[vVariationType]
AS
SELECT 
		 t.[VariationGroupId] 'VariationType.VariationGroupId'
		,t.[VariationTypeId] 'VariationType.Id'
		,t.[Ordinal] 'VariationType.Ordinal'
		,COALESCE (transl.[Title],t.[Title]) AS 'VariationType.Title'
		,l.LanguageId 'VariationType.LanguageId'
FROM 
	  [product].[tVariationType] AS t
	  cross join  core.tLanguage AS l
      LEFT JOIN product.tVariationTypeTranslation AS transl ON transl.VariationTypeId = t.VariationTypeId AND transl.LanguageId = l.LanguageId


GO
