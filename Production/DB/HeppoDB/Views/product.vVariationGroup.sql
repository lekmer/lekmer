SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vVariationGroup]
AS
SELECT 
		[VariationGroupId] 'VariationGroup.Id',
		[Title] 'VariationGroup.Title',
		[DefaultProductId] 'VariationGroup.DefaultProductId',
		[VariationGroupStatusId] 'VariationGroup.StatusId'
	FROM 
		[product].[tVariationGroup]
GO
