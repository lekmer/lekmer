SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomStoreStatus]
as
	select
		*
	from
		[product].[vStoreStatus]
GO
