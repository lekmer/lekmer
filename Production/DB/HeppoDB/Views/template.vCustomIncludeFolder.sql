SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomIncludeFolder]
as
	select
		*
	from
		[template].[vIncludeFolder]
GO
