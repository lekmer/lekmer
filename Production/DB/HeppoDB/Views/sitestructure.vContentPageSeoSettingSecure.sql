SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vContentPageSeoSettingSecure]
AS
SELECT     
	cpss.ContentNodeId AS 'ContentPageSeoSetting.ContentNodeId', 
	cpss.Title AS 'ContentPageSeoSetting.Title', 
	cpss.Description AS 'ContentPageSeoSetting.Description', 
	cpss.Keywords AS 'ContentPageSeoSetting.Keywords'
FROM
	sitestructure.tContentPageSeoSetting AS cpss
GO
