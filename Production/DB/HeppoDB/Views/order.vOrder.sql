SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [order].[vOrder]
AS
SELECT
	OrderId AS 'Order.OrderId', 
	Number AS 'Order.Number',
	CustomerId AS 'Order.CustomerId',
	BillingAddressId AS 'Order.BillingAddressId',
	DeliveryAddressId AS 'Order.DeliveryAddressId',
	CreatedDate AS 'Order.CreatedDate',
	FreightCost AS 'Order.FreightCost',
	Email AS 'Order.Email',
	DeliveryMethodId AS 'Order.DeliveryMethodId',
	ChannelId  AS 'Order.ChannelId',
	OrderStatusId AS 'Order.OrderStatusId',
	IP AS 'Order.IP',
	DeliveryTrackingId AS 'Order.DeliveryTrackingId',
	vos.*
FROM
	[order].[tOrder] o 
	INNER JOIN [order].vCustomOrderStatus vos ON o.OrderStatusId = vos.[OrderStatus.Id]




GO
