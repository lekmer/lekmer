SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomBlockProductRelationListItem]
as
	select
		*
	from
		[product].[vBlockProductRelationListItem]
GO
