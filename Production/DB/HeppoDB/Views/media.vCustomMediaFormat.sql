SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [media].[vCustomMediaFormat]
as
	select
		*
	from
		[media].[vMediaFormat]
GO
