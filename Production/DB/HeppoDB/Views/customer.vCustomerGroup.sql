SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [customer].[vCustomerGroup]
AS
SELECT     
	CG.CustomerGroupId AS 'CustomerGroup.CustomerGroupId',
	CG.CustomerGroupFolderId AS 'CustomerGroup.FolderId',
	CG.Title AS 'CustomerGroup.Title',
	CG.StatusId AS 'CustomerGroup.StatusId',
	CG.ErpId AS 'CustomerGroup.ErpId'
FROM
	[customer].[tCustomerGroup] as CG
GO
