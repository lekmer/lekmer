SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [customer].[vCustomerStatus]
AS
SELECT     
	CS.CustomerStatusId AS 'CustomerStatus.CustomerStatusId',
	CS.Title AS 'CustomerStatus.Title',
	CS.CommonName AS 'CustomerStatus.CommonName'
FROM
	[customer].[tCustomerStatus] as CS
GO
