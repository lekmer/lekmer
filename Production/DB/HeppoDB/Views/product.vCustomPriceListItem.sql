SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomPriceListItem]
as
	select
		*
	from
		[product].[vPriceListItem]
GO
