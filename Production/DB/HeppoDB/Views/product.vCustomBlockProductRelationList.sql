SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomBlockProductRelationList]
as
	select
		*
	from
		[product].[vBlockProductRelationList]
GO
