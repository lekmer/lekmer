SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [lekmer].[vCustomContentPageUrlHistory]
AS
	SELECT
		*
	FROM
		[lekmer].[vContentPageUrlHistory]
GO
