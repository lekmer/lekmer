SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [media].[vCustomImageSize]
as
	select
		*
	from
		[media].[vImageSize]
GO
