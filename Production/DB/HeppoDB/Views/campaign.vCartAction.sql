SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vCartAction]
as
	select
		CartActionId as 'CartAction.Id',
		CartActionTypeId as 'CartAction.CartActionTypeId',
		CampaignId as 'CartAction.CampaignId',
		Ordinal as 'CartAction.Ordinal',
		t.*
	from
		campaign.tCartAction ca
		inner join campaign.vCustomCartActionType t on ca.CartActionTypeId = t.[CartActionType.Id]

GO
