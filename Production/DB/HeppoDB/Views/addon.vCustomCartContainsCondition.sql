SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [addon].[vCustomCartContainsCondition]
AS 
	SELECT 
		*
	FROM 
		[addon].[vCartContainsCondition]
GO
