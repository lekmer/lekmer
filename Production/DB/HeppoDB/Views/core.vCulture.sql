SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [core].[vCulture]
AS
SELECT
	CultureId AS [Culture.Id], 
	CommonName AS [Culture.CommonName], 
	Title AS [Culture.Title]
FROM
	core.tCulture
GO
