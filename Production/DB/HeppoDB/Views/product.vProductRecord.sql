
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [product].[vProductRecord]
AS
SELECT     
	P.ProductId 'Product.Id',
	P.ItemsInPackage 'Product.ItemsInPackage',
	P.ErpId 'Product.ErpId',
	P.EanCode 'Product.EanCode',
	P.NumberInStock 'Product.NumberInStock',
    P.CategoryId 'Product.CategoryId',
    P.WebShopTitle 'Product.WebShopTitle',
    P.Title 'Product.Title',
    P.ProductStatusId 'Product.ProductStatusId',
    P.Description 'Product.Description',
    P.ShortDescription 'Product.ShortDescription',
    P.IsDeleted 'Product.IsDeleted',
    I.*
FROM         
	product.tProduct AS P
	LEFT JOIN media.vCustomImageSecure AS I ON I.[Image.MediaId] = P.MediaId
WHERE     
	(P.IsDeleted = 0)


GO
