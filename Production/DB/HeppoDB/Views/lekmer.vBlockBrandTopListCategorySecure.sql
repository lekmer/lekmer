SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vBlockBrandTopListCategorySecure]
AS
	SELECT
		b.[BlockId]	AS 'BlockBrandTopListCategory.BlockId',
		b.[CategoryId] AS 'BlockBrandTopListCategory.CategoryId',
		b.[IncludeSubcategories] AS 'BlockBrandTopListCategory.IncludeSubcategories',
		c.*
	FROM
		[lekmer].[tBlockBrandTopListCategory] b
		INNER JOIN [product].[vCustomCategorySecure] c on b.[CategoryId] = c.[Category.Id]
GO
