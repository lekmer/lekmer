SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vAge]
AS
SELECT     
      AgeId AS 'Age.Id', 
      Months AS 'Age.Months', 
      Title AS 'Age.Title'
FROM
      [lekmer].[tAge]



GO
