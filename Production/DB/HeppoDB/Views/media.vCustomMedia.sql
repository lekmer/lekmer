SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [media].[vCustomMedia]
as
	select
		*
	from
		[media].[vMedia]
GO
