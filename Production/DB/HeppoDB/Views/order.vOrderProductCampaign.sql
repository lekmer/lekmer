SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [order].[vOrderProductCampaign]
AS
SELECT 
	tpc.OrderProductCampaignId AS 'OrderCampaign.Id',
	tpc.CampaignId AS 'OrderCampaign.CampaignId',
	tpc.OrderItemId AS 'OrderCampaign.OrderItemId', 
	tpc.Title AS 'OrderCampaign.Title'
FROM 
	[order].tOrderProductCampaign tpc






GO
