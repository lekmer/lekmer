SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vBlockBrandTopListCategory]
AS
	SELECT
		b.[BlockId] AS 'BlockBrandTopListCategory.BlockId',
		b.[CategoryId] AS 'BlockBrandTopListCategory.CategoryId',
		b.[IncludeSubcategories] AS 'BlockBrandTopListCategory.IncludeSubcategories',
		c.*
	FROM
		[lekmer].[tBlockBrandTopListCategory] b
		INNER JOIN [product].[vCustomCategory] c ON b.[CategoryId] = c.[Category.Id]
GO
