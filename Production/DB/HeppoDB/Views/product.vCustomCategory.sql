
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE view [product].[vCustomCategory]
as
	select
		c.*,
		l.AllowMultipleSizesPurchase as [LekmerCategory.AllowMultipleSizesPurchase]
	from
		[product].[vCategory] c
		left join [lekmer].[tLekmerCategory] l on c.[Category.Id] = l.CategoryId


GO
