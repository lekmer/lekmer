SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [order].[vCustomOrderModuleChannel]
as
	select
		*
	from
		[order].[vOrderModuleChannel]
GO
