
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [productlek].[vBlockProductSearchEsalesResult]
AS
	SELECT
		[BlockId] 'BlockProductSearchEsalesResult.BlockId' ,
		[SecondaryTemplateId] 'BlockProductSearchEsalesResult.SecondaryTemplateId'
	FROM
		[productlek].[tBlockProductSearchEsalesResult]
GO
