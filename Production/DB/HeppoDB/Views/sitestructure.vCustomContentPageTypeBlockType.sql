SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentPageTypeBlockType]
as
	select
		*
	from
		[sitestructure].[vContentPageTypeBlockType]
GO
