
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vSize]
AS
SELECT
	[SizeId] AS 'Size.Id',
	[ErpId] AS 'Size.ErpId',
	[ErpTitle] AS 'Size.ErpTitle',
	[EU] AS 'Size.EU',
	[EUTitle] AS 'Size.EUTitle',
	[USMale] AS 'Size.USMale',
	[USFemale] AS 'Size.USFemale',
	[UKMale] AS 'Size.UKMale',
	[UKFemale] AS 'Size.UKFemale',
	[Millimeter] AS 'Size.Millimeter'
FROM
	[lekmer].[tSize]

GO
