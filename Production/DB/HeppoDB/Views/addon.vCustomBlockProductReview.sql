SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [addon].[vCustomBlockProductReview]
as
         select
                   *
         from
                   [addon].[vBlockProductReview]

GO
