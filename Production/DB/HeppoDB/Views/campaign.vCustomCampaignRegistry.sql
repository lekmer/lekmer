SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomCampaignRegistry]
as
	select
		*
	from
		[campaign].[vCampaignRegistry]
GO
