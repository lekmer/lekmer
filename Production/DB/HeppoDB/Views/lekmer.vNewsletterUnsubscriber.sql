SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vNewsletterUnsubscriber]
AS
SELECT
	[UnsubscriberId] AS 'NewsletterUnsubscriber.UnsubscriberId',
	[ChannelId] AS 'NewsletterUnsubscriber.ChannelId',
	[Email] AS 'NewsletterUnsubscriber.Email',
	[UnregFromAll] AS 'NewsletterUnsubscriber.UnregFromAll',
	[CreatedDate] AS 'NewsletterUnsubscriber.CreatedDate',
	[UpdatedDate] AS 'NewsletterUnsubscriber.UpdatedDate'
FROM
	[lekmer].[tNewsletterUnsubscriber]
GO
