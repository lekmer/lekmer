SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [sitestructure].[vAccess]
AS
	select 
		[AccessId] 'Access.Id',
		[Title] 'Access.Title',
		[CommonName] 'Access.CommonName'
	from 
		[sitestructure].[tAccess]

GO
