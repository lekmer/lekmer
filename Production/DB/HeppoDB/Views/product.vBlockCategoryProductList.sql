SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vBlockCategoryProductList]
as
SELECT [BlockId] AS 'BlockCategoryProductList.BlockId'
      ,[ColumnCount] AS 'BlockCategoryProductList.ColumnCount'
      ,[RowCount] AS 'BlockCategoryProductList.RowCount'
      ,[ProductSortOrderId] AS 'BlockCategoryProductList.ProductSortOrderId'
  FROM [product].[tBlockCategoryProductList]
GO
