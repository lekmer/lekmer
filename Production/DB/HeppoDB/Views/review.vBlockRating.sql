SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vBlockRating]
AS
	SELECT
		[BlockId] AS 'BlockRating.BlockId',
		[RatingId] AS 'BlockRating.RatingId'
	FROM
		[review].[tBlockRating]
GO
