
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [customer].[vCustomCustomerSecure]
AS
	SELECT
		C.*,
		CI.[CustomerInformation.GenderTypeId],
		CI.[CustomerInformation.IsCompany],
		CI.[CustomerInformation.AlternateAddressId],
		BA.[Address.HouseNumber] AS 'BillingAddress.HouseNumber',
		BA.[Address.HouseExtension] AS 'BillingAddress.HouseExtension',
		BA.[Address.Reference] AS 'BillingAddress.Reference',
		DA.[Address.HouseNumber] AS 'DeliveryAddress.HouseNumber',
		DA.[Address.HouseExtension] AS  'DeliveryAddress.HouseExtension',
		DA.[Address.Reference] AS 'DeliveryAddress.Reference',
		AA.[Address.AddressId] AS 'AlternateAddress.AddressId',
		AA.[Address.CustomerId] AS  'AlternateAddress.CustomerId',
		AA.[Address.AddressTypeId] AS 'AlternateAddress.AddressTypeId',
		AA.[Address.Addressee] AS 'AlternateAddress.Addressee',
		AA.[Address.StreetAddress] AS 'AlternateAddress.StreetAddress',
		AA.[Address.StreetAddress2] AS 'AlternateAddress.StreetAddress2',
		AA.[Address.PostalCode] AS 'AlternateAddress.PostalCode',
		AA.[Address.City]  AS 'AlternateAddress.City',
		AA.[Address.CountryId] AS 'AlternateAddress.CountryId',
		AA.[Address.PhoneNumber] AS 'AlternateAddress.PhoneNumber',
		AA.[Address.HouseNumber] AS 'AlternateAddress.HouseNumber',
		AA.[Address.HouseExtension] AS 'AlternateAddress.HouseExtension',
		AA.[Address.Reference] AS 'AlternateAddress.Reference'
	FROM
		[customer].[vCustomerSecure] C
		LEFT JOIN [customerlek].vCustomerInformation CI ON CI.[CustomerInformation.CustomerId] = C.[CustomerInformation.InformationId]
		LEFT JOIN [customer].vCustomAddress BA ON BA.[Address.AddressId] = C.[CustomerInformation.DefaultBillingAddressId]
		LEFT JOIN [customer].vCustomAddress DA ON DA.[Address.AddressId] = C.[CustomerInformation.DefaultDeliveryAddressId]
		LEFT JOIN [customer].vCustomAddress AA ON AA.[Address.AddressId] = CI.[CustomerInformation.AlternateAddressId]

GO
