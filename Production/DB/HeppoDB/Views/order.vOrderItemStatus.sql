SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [order].[vOrderItemStatus]
AS 
SELECT 
	OrderItemStatusId AS 'OrderItemStatus.Id', 
	Title AS 'OrderItemStatus.Title', 
	CommonName AS 'OrderItemStatus.CommonName'
FROM 	  
	[order].[tOrderItemStatus]


GO
