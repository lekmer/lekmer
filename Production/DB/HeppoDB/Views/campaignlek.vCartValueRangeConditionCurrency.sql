SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaignlek].[vCartValueRangeConditionCurrency]
AS
	SELECT
		cvrcc.[ConditionId] AS 'CartValueRangeConditionCurrency.ConditionId',
		cvrcc.[CurrencyId] AS 'CartValueRangeConditionCurrency.CurrencyId',
		cvrcc.[MonetaryValueFrom] AS 'CartValueRangeConditionCurrency.MonetaryValueFrom',
		cvrcc.[MonetaryValueTo] AS 'CartValueRangeConditionCurrency.MonetaryValueTo',
		c.*
	FROM
		[campaignlek].[tCartValueRangeConditionCurrency] cvrcc
		INNER JOIN [core].[vCustomCurrency] c ON cvrcc.[CurrencyId] = c.[Currency.Id]
GO
