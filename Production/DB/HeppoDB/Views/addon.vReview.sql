SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [addon].[vReview]
AS
 SELECT
         r.[ReviewId] 'Review.ReviewId',
         r.[ProductId] 'Review.ProductId',
         r.[ReviewStatusId] 'Review.ReviewStatusId',
         r.[CustomerId] 'Review.CustomerId',
         r.[Rating] 'Review.Rating',
         r.[Message] 'Review.Message',
         r.[Author] 'Review.Author',
         r.[CreatedDate] 'Review.CreatedDate',
         r.[ChannelId] 'Review.ChannelId'
 FROM 
         addon.tReview r

GO
