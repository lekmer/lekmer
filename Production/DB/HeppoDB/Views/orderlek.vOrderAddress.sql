
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [orderlek].[vOrderAddress]
AS
	SELECT
		[OrderAddressId] AS 'OrderAddress.OrderAddressId',
		[HouseNumber] AS 'OrderAddress.HouseNumber',
		[HouseExtension] AS 'OrderAddress.HouseExtension',
		[Reference] AS 'OrderAddress.Reference'
	FROM
		[orderlek].[tOrderAddress]

GO
