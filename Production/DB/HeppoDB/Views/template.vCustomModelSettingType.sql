SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomModelSettingType]
as
	select
		*
	from
		[template].[vModelSettingType]
GO
