SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vProductUrl]
AS
SELECT 
	pu.[ProductId] AS 'ProductUrl.ProductId',
	pu.[LanguageId] AS 'ProductUrl.LanguageId',
	pu.[UrlTitle] AS 'ProductUrl.UrlTitle'
FROM 
	lekmer.[tProductUrl] AS pu

GO
