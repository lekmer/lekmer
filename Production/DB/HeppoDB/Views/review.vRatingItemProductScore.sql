SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vRatingItemProductScore]
AS
	SELECT
		rips.[ChannelId]	'RatingItemProductScore.ChannelId',
		rips.[ProductId]	'RatingItemProductScore.ProductId',
		rips.[RatingId]		'RatingItemProductScore.RatingId',
		rips.[RatingItemId]	'RatingItemProductScore.RatingItemId',
		rips.[HitCount]		'RatingItemProductScore.HitCount'
	FROM 
		[review].[tRatingItemProductScore] rips

GO
