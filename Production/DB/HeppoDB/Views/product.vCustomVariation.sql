SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomVariation]
as
	select
		*
	from
		[product].[vVariation]
GO
