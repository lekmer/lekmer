SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create VIEW [security].[vSystemUserStatus]
AS
select
	[SystemUserStatusId] 'SystemUserStatus.Id',
	[Title] 'SystemUserStatus.Title',
	[CommonName] 'SystemUserStatus.CommonName'
from
	[security].[tSystemUserStatus]
GO
