
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [product].[vProductRelationList]
AS
SELECT      
       prl.RelationListId 'ProductRelationList.RelationListId',
       p.*
       
FROM       
       [product].[tProductRelationList] prl       
		INNER JOIN 
		[product].[vCustomProductSecure] p
		ON p.[Product.Id] = prl.ProductId



GO
