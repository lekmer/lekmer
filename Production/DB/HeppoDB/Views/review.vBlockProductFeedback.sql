SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [review].[vBlockProductFeedback]
AS
	SELECT
		[BlockId] AS 'BlockProductFeedback.BlockId',
		[AllowReview] AS 'BlockProductFeedback.AllowReview'
	FROM
		[review].[tBlockProductFeedback]

GO
