SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vRatingGroupFolder]
AS
	SELECT
		rgf.[RatingGroupFolderId]		'RatingGroupFolder.RatingGroupFolderId',
		rgf.[ParentRatingGroupFolderId]	'RatingGroupFolder.ParentRatingGroupFolderId',
		rgf.[Title]						'RatingGroupFolder.Title'
	FROM 
		[review].[tRatingGroupFolder] rgf

GO
