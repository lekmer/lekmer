/*
Run this script on the client database.
You are recommended to back up your database before running this script.
*/

/* ---------------------------------------- SETUP SCHEMA ---------------------------------------- */

SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Creating schemata'
GO
IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'addon')
EXEC ('CREATE SCHEMA [addon] AUTHORIZATION [dbo]')
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartItemPriceActionExcludeProduct]'
GO
CREATE TABLE [addon].[tCartItemPriceActionExcludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPriceActionExcludeProduct] on [addon].[tCartItemPriceActionExcludeProduct]'
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeProduct] ADD CONSTRAINT [PK_tCartItemPriceActionExcludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartItemPriceActionCurrency]'
GO
CREATE TABLE [addon].[tCartItemPriceActionCurrency]
(
[CartActionId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[Value] [decimal] (16, 2) NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPriceActionCurrency] on [addon].[tCartItemPriceActionCurrency]'
GO
ALTER TABLE [addon].[tCartItemPriceActionCurrency] ADD CONSTRAINT [PK_tCartItemPriceActionCurrency] PRIMARY KEY CLUSTERED  ([CartActionId], [CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartItemPriceActionIncludeProduct]'
GO
CREATE TABLE [addon].[tCartItemPriceActionIncludeProduct]
(
[CartActionId] [int] NOT NULL,
[ProductId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPriceActionIncludeProduct] on [addon].[tCartItemPriceActionIncludeProduct]'
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeProduct] ADD CONSTRAINT [PK_tCartItemPriceActionIncludeProduct] PRIMARY KEY CLUSTERED  ([CartActionId], [ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionIncludeProductDeleteAll]'
GO

create procedure [addon].[pCartItemPriceActionIncludeProductDeleteAll]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionIncludeProduct
	where CartActionId = @CartActionId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionExcludeProductDeleteAll]'
GO

create procedure [addon].[pCartItemPriceActionExcludeProductDeleteAll]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionExcludeProduct
	where CartActionId = @CartActionId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartItemPriceActionIncludeCategory]'
GO
CREATE TABLE [addon].[tCartItemPriceActionIncludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPriceActionIncludeCategory] on [addon].[tCartItemPriceActionIncludeCategory]'
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeCategory] ADD CONSTRAINT [PK_tCartItemPriceActionIncludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionIncludeCategoryDeleteAll]'
GO
create procedure [addon].[pCartItemPriceActionIncludeCategoryDeleteAll]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionIncludeCategory
	where CartActionId = @CartActionId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartItemPriceActionExcludeCategory]'
GO
CREATE TABLE [addon].[tCartItemPriceActionExcludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPriceActionExcludeCategory] on [addon].[tCartItemPriceActionExcludeCategory]'
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeCategory] ADD CONSTRAINT [PK_tCartItemPriceActionExcludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionExcludeCategoryDeleteAll]'
GO

create procedure [addon].[pCartItemPriceActionExcludeCategoryDeleteAll]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionExcludeCategory
	where CartActionId = @CartActionId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionExcludeCategoryGetIdAll]'
GO
CREATE procedure [addon].[pCartItemPriceActionExcludeCategoryGetIdAll]
	@CartActionId int
AS
BEGIN
	SELECT 
		c.CategoryId
	FROM 
		[addon].tCartItemPriceActionExcludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionIncludeCategoryGetIdAll]'
GO
CREATE procedure [addon].[pCartItemPriceActionIncludeCategoryGetIdAll]
	@CartActionId int
AS
BEGIN
	SELECT 
		c.CategoryId
	FROM 
		[addon].tCartItemPriceActionIncludeCategory A INNER JOIN 
		product.tCategory c ON A.CategoryId = c.CategoryId
	WHERE 
		A.CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionExcludeCategoryInsert]'
GO
CREATE procedure [addon].[pCartItemPriceActionExcludeCategoryInsert]
	@CartActionId			int,
	@CategoryId				int
AS 
BEGIN 
	INSERT
		addon.tCartItemPriceActionExcludeCategory
	(
		CartActionId,
		CategoryId
	)
	VALUES
	(
		@CartActionId,
		@CategoryId
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionIncludeCategoryInsert]'
GO
CREATE procedure [addon].[pCartItemPriceActionIncludeCategoryInsert]
	@CartActionId			int,
	@CategoryId				int
AS 
BEGIN 
	INSERT
		addon.tCartItemPriceActionIncludeCategory
	(
		CartActionId,
		CategoryId
	)
	VALUES
	(
		@CartActionId,
		@CategoryId
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionIncludeProductInsert]'
GO
create procedure [addon].[pCartItemPriceActionIncludeProductInsert]
	@CartActionId			int,
	@ProductId				int
AS 
BEGIN 
	INSERT
		addon.tCartItemPriceActionIncludeProduct
	(
		CartActionId,
		ProductId
	)
	VALUES
	(
		@CartActionId,
		@ProductId
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionExcludeProductInsert]'
GO
create procedure [addon].[pCartItemPriceActionExcludeProductInsert]
	@CartActionId			int,
	@ProductId				int
AS 
BEGIN 
	INSERT
		addon.tCartItemPriceActionExcludeProduct
	(
		CartActionId,
		ProductId
	)
	VALUES
	(
		@CartActionId,
		@ProductId
	)
END 



GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceSave]'
GO
create procedure [addon].[pCartItemPriceSave]
	@CartActionId int,
	@CurrencyId int,
	@Value decimal(16,2)
AS 
BEGIN
	UPDATE
		addon.tCartItemPriceActionCurrency
	SET 
		[Value] = @Value
	WHERE 
		CartActionId = @CartActionId AND 
		CurrencyId = @CurrencyId
		
		
	IF  @@ROWCOUNT = 0
	BEGIN 
		INSERT 
			addon.tCartItemPriceActionCurrency
		( 
			CartActionId,
			CurrencyId,
			[Value]
		)
		VALUES 
		(
			@CartActionId,
			@CurrencyId,
			@Value
		)
	END
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionExcludeProductGetIdAll]'
GO
create procedure [addon].[pCartItemPriceActionExcludeProductGetIdAll]
	@CartActionId int
AS
BEGIN
	SELECT 
		p.ProductId
	FROM 
		[addon].tCartItemPriceActionExcludeProduct A INNER JOIN 
		product.tProduct p ON A.ProductId = p.ProductId
	WHERE 
		A.CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[tCartItemPriceAction]'
GO
CREATE TABLE [addon].[tCartItemPriceAction]
(
[CartActionId] [int] NOT NULL,
[MaxQuantity] [int] NOT NULL,
[IncludeAllProducts] [bit] NOT NULL CONSTRAINT [DF_tCartItemPriceAction_IncludeAllProducts] DEFAULT ((0))
)
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating primary key [PK_tCartItemPriceAction] on [addon].[tCartItemPriceAction]'
GO
ALTER TABLE [addon].[tCartItemPriceAction] ADD CONSTRAINT [PK_tCartItemPriceAction] PRIMARY KEY CLUSTERED  ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionSave]'
GO
create procedure [addon].[pCartItemPriceActionSave]
	@CartActionId		int,
	@MaxQuantity		int,
	@IncludeAllProducts	bit
AS 
BEGIN 
	UPDATE
		addon.tCartItemPriceAction
	SET
		MaxQuantity = @MaxQuantity,
		IncludeAllProducts = @IncludeAllProducts
	WHERE
		CartActionId = @CartActionId
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT
			addon.tCartItemPriceAction
		(
			CartActionId,
			MaxQuantity,
			IncludeAllProducts
		)
		VALUES
		(
			@CartActionId,
			@MaxQuantity,
			@IncludeAllProducts
		)
	END
END 
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [campaign].[vCartActionType]'
GO
EXEC sp_refreshview N'[campaign].[vCartActionType]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [campaign].[vCustomCartActionType]'
GO
EXEC sp_refreshview N'[campaign].[vCustomCartActionType]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [campaign].[vCartAction]'
GO
EXEC sp_refreshview N'[campaign].[vCartAction]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [core].[vCurrency]'
GO
EXEC sp_refreshview N'[core].[vCurrency]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionDelete]'
GO
CREATE procedure [addon].[pCartItemPriceActionDelete]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionIncludeProduct
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceActionExcludeProduct
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceActionIncludeCategory
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceActionExcludeCategory
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceActionCurrency
	where CartActionId = @CartActionId
	
	delete addon.tCartItemPriceAction
	where CartActionId = @CartActionId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionIncludeProductGetIdAll]'
GO
create procedure [addon].[pCartItemPriceActionIncludeProductGetIdAll]
	@CartActionId int
AS
BEGIN
	SELECT 
		p.ProductId
	FROM 
		[addon].tCartItemPriceActionIncludeProduct A INNER JOIN 
		product.tProduct p ON A.ProductId = p.ProductId
	WHERE 
		A.CartActionId = @CartActionId
END
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [campaign].[vCustomCartAction]'
GO
EXEC sp_refreshview N'[campaign].[vCustomCartAction]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[vCartItemPriceAction]'
GO

CREATE VIEW [addon].[vCartItemPriceAction]
AS 
	SELECT 
		p.MaxQuantity AS 'CartItemPriceAction.MaxQuantity',
		p.IncludeAllProducts AS 'CartItemPriceAction.IncludeAllProducts',
		a.*
	FROM 
		addon.tCartItemPriceAction p
		INNER JOIN campaign.vCustomCartAction a ON p.[CartActionId] = a.[CartAction.Id]

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[vCustomCartItemPriceAction]'
GO

CREATE VIEW [addon].[vCustomCartItemPriceAction]
AS 
	SELECT 
		*
	FROM 
		addon.vCartItemPriceAction

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPriceActionGetById]'
GO
create procedure [addon].[pCartItemPriceActionGetById]
	@CartActionId int
as
begin
	select
		*
	from
		addon.vCustomCartItemPriceAction
	where
		[CartAction.Id] = @CartActionId
end
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Refreshing [core].[vCustomCurrency]'
GO
EXEC sp_refreshview N'[core].[vCustomCurrency]'
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[vCartItemPriceActionCurrency]'
GO
create VIEW addon.[vCartItemPriceActionCurrency]
as
	select
		ac.CartActionId AS 'CartItemPriceAction.ActionId',
		ac.CurrencyId AS 'CartItemPriceAction.CurrencyId',
		ac.Value AS 'CurrencyValue.MonetaryValue',
		c.*
	from
		addon.tCartItemPriceActionCurrency ac
		inner join core.vCustomCurrency c on ac.CurrencyId = c.[Currency.Id]


GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Creating [addon].[pCartItemPricesGetByActionId]'
GO
CREATE PROCEDURE [addon].[pCartItemPricesGetByActionId]
	@CartActionId int
as
begin
	SELECT
		*
	FROM
		addon.[vCartItemPriceActionCurrency]
	WHERE 
		[CartItemPriceAction.ActionId] = @CartActionId
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartItemPriceAction]'
GO
ALTER TABLE [addon].[tCartItemPriceAction] ADD
CONSTRAINT [FK_tCartItemPriceAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartItemPriceActionIncludeCategory]'
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeCategory] ADD
CONSTRAINT [FK_tCartItemPriceActionCategory_tCartItemPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId]),
CONSTRAINT [FK_tCartItemPriceActionCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartItemPriceActionCurrency]'
GO
ALTER TABLE [addon].[tCartItemPriceActionCurrency] ADD
CONSTRAINT [FK_tCartItemPriceActionCurrency_tCartItemPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId]),
CONSTRAINT [FK_tCartItemPriceActionCurrency_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartItemPriceActionExcludeCategory]'
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeCategory] ADD
CONSTRAINT [FK_tCartItemPriceActionExcludeCategory_tProductPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId]),
CONSTRAINT [FK_tCartItemPriceActionExcludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartItemPriceActionExcludeProduct]'
GO
ALTER TABLE [addon].[tCartItemPriceActionExcludeProduct] ADD
CONSTRAINT [FK_tCartItemPriceActionExcludeProduct_tCartItemPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId]),
CONSTRAINT [FK_tCartItemPriceActionExcludeProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Adding foreign keys to [addon].[tCartItemPriceActionIncludeProduct]'
GO
ALTER TABLE [addon].[tCartItemPriceActionIncludeProduct] ADD
CONSTRAINT [FK_tCartItemPriceActionProduct_tCartItemPriceAction] FOREIGN KEY ([CartActionId]) REFERENCES [addon].[tCartItemPriceAction] ([CartActionId]),
CONSTRAINT [FK_tCartItemPriceActionProduct_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO









/* ---------------------------------------- SETUP DATA ---------------------------------------- */
SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS, NOCOUNT ON
GO
SET DATEFORMAT YMD
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
-- Pointer used for text / image updates. This might not be needed, but is declared here just in case
DECLARE @pv binary(16)
BEGIN TRANSACTION

-- Drop constraint FK_tCartAction_tCartActionType from [campaign].[tCartAction]
ALTER TABLE [campaign].[tCartAction] DROP CONSTRAINT [FK_tCartAction_tCartActionType]

-- Add 1 row to [campaign].[tCartActionType]
SET IDENTITY_INSERT [campaign].[tCartActionType] ON
INSERT INTO [campaign].[tCartActionType] ([CartActionTypeId], [Title], [CommonName]) VALUES (100002, N'Cart item price', N'CartItemPrice')
SET IDENTITY_INSERT [campaign].[tCartActionType] OFF

-- Add constraint FK_tCartAction_tCartActionType to [campaign].[tCartAction]
ALTER TABLE [campaign].[tCartAction] WITH NOCHECK ADD CONSTRAINT [FK_tCartAction_tCartActionType] FOREIGN KEY ([CartActionTypeId]) REFERENCES [campaign].[tCartActionType] ([CartActionTypeId])
COMMIT TRANSACTION
GO