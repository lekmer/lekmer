
declare @sql as varchar(max)
set @sql = ''

select
@sql = @sql + '
declare @'+table_name+'Seed int
select @'+table_name+'Seed = max(['+column_name+']) from ['+table_schema+'].['+table_name+']

if (@'+table_name+'Seed IS NULL OR @'+table_name+'Seed < 1000000) set @'+table_name+'Seed = 1000000

DBCC CHECKIDENT (''['+table_schema+'].['+table_name+']'', RESEED, @'+table_name+'Seed)

'

from information_schema.columns
where columnproperty(object_id(table_schema + '.' + table_name), column_name, 'IsIdentity') = 1
and table_name like 't%'
order by table_schema, table_name

--print @sql
exec (@sql)