SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [campaign].[pCampaignSearchProductCampaigns]'
GO
ALTER PROCEDURE [campaign].[pCampaignSearchProductCampaigns]
@Title	nvarchar(MAX)
AS
BEGIN
	SET @Title = [generic].[fPrepareSearchParameter](@Title)
	
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C INNER JOIN 
		campaign.tProductCampaign PC ON PC.CampaignId = C.[Campaign.Id]
	WHERE
		C.[Campaign.Title] LIKE @Title ESCAPE '\'
	ORDER BY
		C.[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignSearchCartCampaigns]'
GO
ALTER PROCEDURE [campaign].[pCampaignSearchCartCampaigns]
@Title	nvarchar(MAX)
AS
BEGIN
	SET @Title = [generic].[fPrepareSearchParameter](@Title)
	
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C INNER JOIN 
		campaign.tCartCampaign CC ON CC.CampaignId = C.[Campaign.Id]
	WHERE
		C.[Campaign.Title] LIKE @Title ESCAPE '\'
	ORDER BY
		C.[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignGetAllCartCampaigns]'
GO
ALTER PROCEDURE [campaign].[pCampaignGetAllCartCampaigns]
AS
BEGIN
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C 
		INNER JOIN campaign.tCartCampaign CC ON CC.CampaignId = C.[Campaign.Id]
	ORDER BY 
		C.[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignGetAllByFolder]'
GO
ALTER PROCEDURE [campaign].[pCampaignGetAllByFolder]
@FolderId	int
AS
BEGIN
	SELECT 
		*
	FROM 
		[campaign].[vCustomCampaign]
	WHERE 
		[Campaign.FolderId] = @FolderId
	ORDER BY 
		[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [campaign].[pCampaignGetAllProductCampaigns]'
GO
ALTER PROCEDURE [campaign].[pCampaignGetAllProductCampaigns]
AS
BEGIN
	SELECT 
		C.*
	FROM 
		campaign.vCustomCampaign C 
		INNER JOIN campaign.tProductCampaign PC ON PC.CampaignId = C.[Campaign.Id]
	ORDER BY 
		C.[Campaign.Priority] ASC
END

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO