SET NUMERIC_ROUNDABORT OFF
GO
SET ANSI_PADDING, ANSI_WARNINGS, CONCAT_NULL_YIELDS_NULL, ARITHABORT, QUOTED_IDENTIFIER, ANSI_NULLS ON
GO
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpErrors')) DROP TABLE #tmpErrors
GO
CREATE TABLE #tmpErrors (Error int)
GO
SET XACT_ABORT ON
GO
SET TRANSACTION ISOLATION LEVEL SERIALIZABLE
GO
BEGIN TRANSACTION
GO
PRINT N'Altering [security].[pSystemUserSearch]'
GO
ALTER PROCEDURE [security].[pSystemUserSearch]
	@UserName			nvarchar(100),
	@Name				nvarchar(100),
	@StatusId			int,
	@ActivationDate		smalldatetime,
	@ExpirationDate		smalldatetime,
	@Page				int = NULL,
	@PageSize			int,
	@SortBy				varchar(20) = NULL,
	@SortDescending		bit = NULL
as
begin
   set nocount on

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	IF (@UserName IS NOT NULL)
	BEGIN
		SET @UserName = generic.fPrepareSearchParameter(@UserName)
	END
	IF (@Name IS NOT NULL)
	BEGIN
		SET @Name = generic.fPrepareSearchParameter(@Name)
	END

	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' 
		+ COALESCE(@SortBy, 'SystemUser.Id')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			*
		from
			[security].[vCustomSystemUser]
		where 1=1'

		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
		AND [SystemUserStatus.Id] = @StatusId' ELSE '' END

		+ CASE WHEN (@Name IS NOT NULL) THEN + '
		AND [SystemUser.Name] LIKE @Name' ELSE '' END

		+ CASE WHEN (@UserName IS NOT NULL) THEN + '
		AND [SystemUser.UserName] LIKE @UserName' ELSE '' END

		+ CASE WHEN (@ActivationDate IS NOT NULL) THEN + '
		AND [SystemUser.ActivationDate] >= @ActivationDate ' ELSE '' END

		+ CASE WHEN (@ExpirationDate IS NOT NULL) THEN + '
		AND [SystemUser.ExpirationDate] <= @ExpirationDate ' ELSE '' END

	SET @sql = 'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END
	
	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	@UserName				nvarchar(100),
			@Name					nvarchar(100),
			@StatusId				int,
			@ActivationDate			smalldatetime,
			@ExpirationDate			SMALLDATETIME',
			@UserName,          
			@Name, 
			@StatusId,     
			@ActivationDate,
			@ExpirationDate
			
			     
	EXEC sp_executesql @sql, 
		N'	@UserName				nvarchar(100),
			@Name					nvarchar(100),
			@StatusId				int,
			@ActivationDate			smalldatetime,
			@ExpirationDate			SMALLDATETIME',
			@UserName,          
			@Name, 
			@StatusId,     
			@ActivationDate,
			@ExpirationDate
	end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
PRINT N'Altering [security].[pRoleSearch]'
GO
ALTER PROCEDURE [security].[pRoleSearch]
	@Title			nvarchar(50),
	@Page           int = NULL,
	@PageSize       int,
	@SortBy         varchar(20) = NULL,
	@SortDescending bit = NULL
AS 
BEGIN 
   SET nocount ON

	IF (generic.fnValidateColumnName(@SortBy) = 0) 
	BEGIN
		RAISERROR(N'Illegal characters in string (parameter @SortBy): %s', 16, 1, @SortBy);
		RETURN
	END
	
	IF (@Title IS NOT NULL)
	BEGIN
		SET @Title = generic.fPrepareSearchParameter(@Title)
	END

	DECLARE @sql nvarchar(max)
	DECLARE @sqlCount nvarchar(max)
	DECLARE @sqlFragment nvarchar(max)

	SET @sqlFragment = '
		SELECT ROW_NUMBER() OVER (ORDER BY [' 
		+ COALESCE(@SortBy, 'Role.Id')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
			*
		from
		   [security].[vCustomRole]'
		   + CASE WHEN (@Title IS NOT NULL) THEN + '
		WHERE [Role.Title] LIKE @Title' ELSE '' END
		
	SET @sql = 
		'SELECT * FROM (' + @sqlFragment + ') AS SearchResult'

	IF @Page != 0 AND @Page IS NOT NULL 
	BEGIN
		SET @sql = @sql + '
		WHERE Number > ' + CAST((@Page - 1) * @PageSize AS varchar(10)) + ' AND Number <= ' + CAST(@Page * @PageSize AS varchar(10))
	END

	SET @sqlCount = 'SELECT COUNT(1) FROM
		(
		' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'@Title nvarchar(100)',
		@Title          
			     
	EXEC sp_executesql @sql, 
		N'@Title nvarchar(100)',
		@Title          
end

GO
IF @@ERROR<>0 AND @@TRANCOUNT>0 ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT=0 BEGIN INSERT INTO #tmpErrors (Error) SELECT 1 BEGIN TRANSACTION END
GO
IF EXISTS (SELECT * FROM #tmpErrors) ROLLBACK TRANSACTION
GO
IF @@TRANCOUNT>0 BEGIN
PRINT 'The database update succeeded'
COMMIT TRANSACTION
END
ELSE PRINT 'The database update failed'
GO
DROP TABLE #tmpErrors
GO