SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionDelete]
	@ConditionId INT
AS 
BEGIN
	EXEC [campaignlek].[pCustomerGroupConditionIncludeDeleteAll] @ConditionId
	EXEC [campaignlek].[pCustomerGroupConditionExcludeDeleteAll] @ConditionId

	DELETE FROM [campaignlek].[tCustomerGroupCondition]
	WHERE ConditionId = @ConditionId
END
GO
