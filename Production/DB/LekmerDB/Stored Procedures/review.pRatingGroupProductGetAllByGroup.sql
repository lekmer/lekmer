SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroupProductGetAllByGroup]
	@RatingGroupId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rgp.*
	FROM
		[review].[vRatingGroupProduct] rgp
	WHERE
		rgp.[RatingGroupProduct.RatingGroupId] = @RatingGroupId
END
GO
