SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductRegistryDeliveryTimeGetAllById]
	@DeliveryTimeId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    *
	FROM
	    [productlek].[vProductRegistryDeliveryTime]
	WHERE
		[DeliveryTime.Id] = @DeliveryTimeId
	ORDER BY
		[DeliveryTime.ProductRegistryId]
END
GO
