
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pPackageOrderItemSave]
	@PackageOrderItemId			INT,
	@OrderItemId				INT,
	@OrderId					INT,
	@ProductId					INT,
	@Quantity					INT,
	@PackagePriceIncludingVat	DECIMAL(16,2),
	@ActualPriceIncludingVat	DECIMAL(16,2),
	@OriginalPriceIncludingVat	DECIMAL(16,2),
	@VAT						DECIMAL(16,2),
	@ErpId						VARCHAR(50),
	@EanCode					VARCHAR(20),
	@Title						NVARCHAR(50),
	@OrderItemStatusId			INT = 1,
	@SizeId						INT,
	@SizeErpId					VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE
		[orderlek].[tPackageOrderItem]
	SET
		[OrderItemId]				= @OrderItemId,
		[OrderId]					= @OrderId,
		[Quantity]					= @Quantity,
		[PackagePriceIncludingVat]	= @PackagePriceIncludingVat,
		[ActualPriceIncludingVat]	= @ActualPriceIncludingVat,
		[OriginalPriceIncludingVat] = @OriginalPriceIncludingVat,
		[VAT]						= @VAT,
		[OrderItemStatusId]			= @OrderItemStatusId
	WHERE
		[PackageOrderItemId] = @PackageOrderItemId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [orderlek].[tPackageOrderItem]
		(
			[OrderItemId],
			[OrderId],
			[Quantity],
			[PackagePriceIncludingVat],
			[ActualPriceIncludingVat],
			[OriginalPriceIncludingVat],
			[VAT],
			[OrderItemStatusId]
		)
		VALUES
		(
			@OrderItemId,
			@OrderId,
			@Quantity,
			@PackagePriceIncludingVat,
			@ActualPriceIncludingVat,
			@OriginalPriceIncludingVat,
			@VAT,
			@OrderItemStatusId
		)

		SET @PackageOrderItemId = SCOPE_IDENTITY();
		
		INSERT INTO [orderlek].[tPackageOrderItemProduct]
		(
			[PackageOrderItemId],
			[OrderItemId],
			[ProductId],
			[ErpId],
			[EanCode],
			[Title],
			[SizeId],
			[SizeErpId]
		)
		VALUES
		(
			@PackageOrderItemId,
			@OrderItemId,
			@ProductId,
			@ErpId,
			@EanCode,
			@Title,
			@SizeId,
			@SizeErpId
		)
	END
	
	RETURN @PackageOrderItemId
END
GO
