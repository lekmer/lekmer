SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pQliroPaymentTypeDelete]
	@QliroPaymentTypeId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [orderlek].[tQliroPaymentType]
	WHERE [QliroPaymentTypeId] = @QliroPaymentTypeId
END
GO
