
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pProductSizeSave]
	@ProductId				INT,
	@SizeId					INT,
	@OverrideMillimeter		INT,
	@MillimeterDeviation	INT,
	@OverrideEU				DECIMAL(3, 1)
AS 
BEGIN 
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tProductSize]
	SET
		MillimeterDeviation = @MillimeterDeviation,
		OverrideEU			= @OverrideEU,
		OverrideMillimeter	= @OverrideMillimeter
	WHERE 
		ProductId = @ProductId AND
		SizeId = @SizeId
END
GO
