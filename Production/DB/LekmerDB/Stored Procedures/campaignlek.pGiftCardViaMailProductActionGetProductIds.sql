SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionGetProductIds]
	@ActionId INT
AS
BEGIN
	DECLARE @ConfigId INT
	SET @ConfigId = (SELECT [ConfigId] FROM [campaignlek].[tGiftCardViaMailProductAction] WHERE [ProductActionId] = @ActionId)

	DECLARE @ProductIds TABLE (ProductId INT)
	INSERT INTO @ProductIds
	EXEC [campaignlek].[pCampaignActionGetProductIds] @ConfigId
	
	SELECT [ProductId] FROM @ProductIds
END
GO
