SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [addon].[pCartItemPriceActionIncludeCategoryDeleteAll]
	@CartActionId int
as
begin
	delete addon.tCartItemPriceActionIncludeCategory
	where CartActionId = @CartActionId
end
GO
