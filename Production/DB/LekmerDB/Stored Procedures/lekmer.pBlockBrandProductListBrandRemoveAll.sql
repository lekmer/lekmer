SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [lekmer].[pBlockBrandProductListBrandRemoveAll]
	@BlockId int
as
begin
	delete
		[lekmer].[tBlockBrandProductListBrand]
	where
		BlockId = @BlockId
end
GO
