
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionSaveReservationResponse]
	@TransactionId INT,
	@StatusCode INT,
	@ReservationNumber VARCHAR(50),
	@ReturnCodeId INT,
	@InvoiceStatus INT,
	@Message NVARCHAR(MAX),
	@CustomerMessage NVARCHAR(MAX),
	@Duration BIGINT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tQliroTransaction]
	SET
		[StatusCode] = @StatusCode,
		[ReservationNumber] = @ReservationNumber,
		[ReturnCodeId] = @ReturnCodeId,
		[InvoiceStatus] = @InvoiceStatus,
		[Message] = @Message,
		[CustomerMessage] = @CustomerMessage,
		[Duration] = @Duration
	WHERE
		[TransactionId] = @TransactionId
END
GO
