
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionSave]
	@ConditionId		INT,
	@ConfigId			INT
AS
BEGIN
	UPDATE
		[campaignlek].[tCartItemsGroupValueCondition]
	SET
		ConfigId = @ConfigId
	WHERE
		[ConditionId] = @ConditionId
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT [campaignlek].[tCartItemsGroupValueCondition] (
			ConditionId,
			ConfigId
		)
		VALUES (
			@ConditionId,
			@ConfigId
		)
	END
END
GO
