SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentFunctionGetAll]
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomModelFragmentFunction]	
end

GO
