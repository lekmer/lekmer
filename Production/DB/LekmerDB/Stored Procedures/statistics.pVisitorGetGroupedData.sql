SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pVisitorGetGroupedData]
	@ChannelId		int,
	@DateTimeFrom	smalldatetime, 
	@DateTimeTo		smalldatetime
AS
BEGIN
	DECLARE @Mode varchar(10)
	SET @Mode = [statistics].[fnGetViewMode](@DateTimeFrom, @DateTimeTo)
		
	DECLARE @tVisitor TABLE ([DateTime] smalldatetime, VisitCount int)
	INSERT INTO 
		@tVisitor
	(
		[DateTime], 
		VisitCount
	)
	SELECT
		[statistics].[fnGetDateTimeKey]([Year], [Month], [Day], [Hour], @Mode) AS 'DateTime',
		[VisitCount] AS 'VisitCount'
	FROM
		[statistics].[tHourlyVisitor] WITH(NOLOCK)
	WHERE
		[ChannelId] = @ChannelId
		AND [statistics].[fnGetDateTimeKey]([Year], [Month], [Day], [Hour], 'Day')
			BETWEEN [statistics].[fnGetDateTimeKeyDirectly](@DateTimeFrom, 'Day') 
			AND @DateTimeTo
	
	SELECT 
		d.[DateTime], 
		isnull(sum(v.VisitCount), 0) AS 'Value'
	FROM
		[statistics].[fnGetDateTimeGrid](@DateTimeFrom, @DateTimeTo, @Mode) AS d 
		LEFT JOIN @tVisitor v ON v.DateTime = d.DateTime
	GROUP BY 
		d.[DateTime]
	ORDER BY 
		d.[DateTime]
END
GO
