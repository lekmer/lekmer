SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pTemplateModuleChannelDelete]
@ChannelId	int
AS
BEGIN
	DELETE FROM template.tTemplateModuleChannel
	WHERE ChannelId = @ChannelId
END













GO
