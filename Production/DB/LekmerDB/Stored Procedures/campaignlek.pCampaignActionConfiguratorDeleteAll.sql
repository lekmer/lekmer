SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionConfiguratorDeleteAll]
	@ConfigId INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [campaignlek].[pCampaignActionIncludeBrandDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeBrandDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeCategoryDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeCategoryDeleteAll] @ConfigId

	EXEC [campaignlek].[pCampaignActionIncludeProductDeleteAll] @ConfigId
	EXEC [campaignlek].[pCampaignActionExcludeProductDeleteAll] @ConfigId
	
	DELETE FROM [campaignlek].[tCampaignActionConfigurator]
	WHERE CampaignActionConfiguratorId = @ConfigId
END
GO
