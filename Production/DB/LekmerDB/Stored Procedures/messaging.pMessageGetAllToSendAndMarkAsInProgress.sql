SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [messaging].[pMessageGetAllToSendAndMarkAsInProgress]
	@MessageTypeId					int,
	@NumberOfMessages				int,
	@ReadyToSendMessageStatusId		int,
	@InProgressMessageStatusId		int,
	@BatchId						uniqueidentifier
as
begin
	create table #Message (
		MessageId uniqueidentifier not null,
		UNIQUE (MessageId)
	)

	insert into #Message
	(
		MessageId
	)
	select top (@NumberOfMessages)
		MessageId
	from
		[messaging].[tMessage] WITH (UPDLOCK)
	where
		MessageTypeId = @MessageTypeId and
		MessageStatusId = @ReadyToSendMessageStatusId and
		BatchId is null
	order by
		CreatedDate

	update
		[messaging].[tMessage]
	set
		BatchId			= @BatchId,
		MessageStatusId	= @InProgressMessageStatusId
	where
		MessageId in (select MessageId from #Message)

	select
		MessageId,
		BatchId,
		MessageStatusId,
		MessageTypeId,
		ProviderName,
		Body,
		CreatedDate
	from
		[messaging].[tMessage]
	where
		MessageId in (select MessageId from #Message)

	select 
		MessageId,
		[Name],
		[Value]
	from 
		[messaging].[tMessageCustomField]
	where
		MessageId in (select MessageId from #Message)

	drop table #Message
end


GO
