SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pVariationGetByIdSecure]
	@VariationId	int
as
begin
	set nocount off
	SELECT 
		V.*	
	FROM 
		[product].[vCustomVariationSecure] V		
	where
		V.[Variation.Id] = @VariationId
	order by V.[Variation.Ordinal]
end

GO
