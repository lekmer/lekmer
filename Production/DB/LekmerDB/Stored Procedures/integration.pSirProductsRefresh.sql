
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pSirProductsRefresh]
AS
BEGIN
	-- Update tSirProductsFull table
	
	--SET NOCOUNT ON;

	MERGE [integration].[tSirProductsFull] AS t
	USING [integration].[vSirProductsSource] AS s
	ON (t.[ArticleNumber] = s.[ArticleNumber])
	WHEN NOT MATCHED BY TARGET
		THEN INSERT(
			[ArticleNumber],
			[ProductId],
			[ProductGroup],
			[ArticleDescription],
			[EanCode],
			[OurPrice],
			[ListPrice],
			[Supplier],
			[Category],
			[LekmerArtNo],
			[SupplierArtNo],
			[AveragePrice],
			[InsertedDate],
			[UpdatedDate]
		)
		VALUES(
			[s].[ArticleNumber],
			[s].[ProductId],
			[s].[ProductGroup],
			[s].[ArticleDescription],
			[s].[EanCode],
			[s].[OurPrice],
			[s].[ListPrice],
			[s].[Supplier],
			[s].[Category],
			[s].[LekmerArtNo],
			[s].[SupplierArtNo],
			[s].[AveragePrice],
			GETDATE(), -- Inserted date
			GETDATE()  -- Updated date
		)
	WHEN MATCHED AND (
		([s].[ProductGroup] != [t].[ProductGroup] OR [s].[ProductGroup] IS NULL OR [t].[ProductGroup] IS NULL) AND ([s].[ProductGroup] IS NOT NULL OR [t].[ProductGroup] IS NOT NULL)
		OR
		([s].[ArticleDescription] != [t].[ArticleDescription] OR [s].[ArticleDescription] IS NULL OR [t].[ArticleDescription] IS NULL) AND ([s].[ArticleDescription] IS NOT NULL OR [t].[ArticleDescription] IS NOT NULL)
		OR
		([s].[EanCode] != [t].[EanCode] OR [s].[EanCode] IS NULL OR [t].[EanCode] IS NULL) AND ([s].[EanCode] IS NOT NULL OR [t].[EanCode] IS NOT NULL)
		OR
		([s].[OurPrice] != [t].[OurPrice] OR [s].[OurPrice] IS NULL OR [t].[OurPrice] IS NULL) AND ([s].[OurPrice] IS NOT NULL OR [t].[OurPrice] IS NOT NULL)
		OR
		([s].[ListPrice] != [t].[ListPrice] OR [s].[ListPrice] IS NULL OR [t].[ListPrice] IS NULL) AND ([s].[ListPrice] IS NOT NULL OR [t].[ListPrice] IS NOT NULL)
		OR
		([s].[Supplier] != [t].[Supplier] OR [s].[Supplier] IS NULL OR [t].[Supplier] IS NULL) AND ([s].[Supplier] IS NOT NULL OR [t].[Supplier] IS NOT NULL)
		OR
		([s].[Category] != [t].[Category] OR [s].[Category] IS NULL OR [t].[Category] IS NULL) AND ([s].[Category] IS NOT NULL OR [t].[Category] IS NOT NULL)
		OR
		([s].[LekmerArtNo] != [t].[LekmerArtNo] OR [s].[LekmerArtNo] IS NULL OR [t].[LekmerArtNo] IS NULL) AND ([s].[LekmerArtNo] IS NOT NULL OR [t].[LekmerArtNo] IS NOT NULL)
		OR
		([s].[SupplierArtNo] != [t].[SupplierArtNo] OR [s].[SupplierArtNo] IS NULL OR [t].[SupplierArtNo] IS NULL) AND ([s].[SupplierArtNo] IS NOT NULL OR [t].[SupplierArtNo] IS NOT NULL)
		OR
		([s].[AveragePrice] != [t].[AveragePrice] OR [s].[AveragePrice] IS NULL OR [t].[AveragePrice] IS NULL) AND ([s].[AveragePrice] IS NOT NULL OR [t].[AveragePrice] IS NOT NULL)
		)
		THEN UPDATE SET
			[t].[ProductId] = [s].[ProductId],
			[t].[ArticleDescription] = [s].[ArticleDescription],
			[t].[EanCode] = [s].[EanCode],
			[t].[OurPrice] = [s].[OurPrice],
			[t].[ListPrice] = [s].[ListPrice],
			[t].[Supplier] = [s].[Supplier],
			[t].[Category] = [s].[Category],
			[t].[LekmerArtNo] = [s].[LekmerArtNo],
			[t].[SupplierArtNo] = [s].[SupplierArtNo],
			[t].[AveragePrice] = [s].[AveragePrice],
			[t].[UpdatedDate] = GETDATE()
		;
	
END
GO
