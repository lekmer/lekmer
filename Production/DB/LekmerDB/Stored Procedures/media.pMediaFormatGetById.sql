SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
-- =============================================
-- Author:		Pochapskyy Volodymyr
-- Create date: 2009-03-26
-- Description:	Gets a media format by id
-- =============================================
CREATE PROCEDURE [media].[pMediaFormatGetById]
	@MediaFormatId INT
AS
BEGIN
	SET NOCOUNT ON;
	SELECT 
		*
	FROM   [media].[vCustomMediaFormat] v
	WHERE  v.[MediaFormat.Id] = @MediaFormatId
END

GO
