SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pLekmerOrderGetByEmailAndDate]
	@Email VARCHAR(320),
	@Date DATETIME
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM
		[order].[vCustomOrder]
	WHERE
		[Order.CreatedDate] >= @Date
		AND [Order.Email] = @Email
		AND [OrderStatus.CommonName] = 'OrderInHY'
END
GO
