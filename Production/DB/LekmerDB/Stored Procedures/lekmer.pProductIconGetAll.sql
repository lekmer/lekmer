SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductIconGetAll]
	@ProductId	INT,
	@CategoryId	INT,
	@BrandId	INT = NULL,
	@LanguageId	INT
AS 
BEGIN 
	SELECT i.*
	FROM 
		[lekmer].[vIcon] i
		INNER JOIN [lekmer].[tLekmerProductIcon] lpi ON lpi.IconId = i.[Icon.Id]
	WHERE
		lpi.ProductId = @ProductId
		AND i.[Image.LanguageId] = @LanguageId
		
	UNION
	
	SELECT i.*
	FROM 
		[lekmer].[vIcon] i
		INNER JOIN [lekmer].[tBrandIcon] bi ON [bi].[IconId] = [i].[Icon.Id]
	WHERE
		[bi].[BrandId] = @BrandId
		AND i.[Image.LanguageId] = @LanguageId
		
	UNION
	
	SELECT i.*
	FROM 
		[lekmer].[vIcon] i
		INNER JOIN [lekmer].[tCategoryIcon] ci ON [ci].[IconId] = [i].[Icon.Id]
	WHERE
		[ci].[CategoryId] IN (SELECT CategoryId FROM [lekmer].[fnGetParentCategories](@CategoryId))
		AND i.[Image.LanguageId] = @LanguageId
END
GO
