SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingItemGetAllByRatingSecure]
	@RatingId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		ri.*
	FROM
		[review].[vRatingItemSecure] ri
	WHERE
		ri.[RatingItem.RatingId] = @RatingId
END
GO
