SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [test].[pCustomerGetAll]
	@CountryId int,
	@Count int
as
begin
	select top (@Count)
		*	
	from
		[customer].[vCustomCustomer]
	where
		[DeliveryAddress.CountryId] = @CountryId
		and [BillingAddress.CountryId] = @CountryId
end

GO
