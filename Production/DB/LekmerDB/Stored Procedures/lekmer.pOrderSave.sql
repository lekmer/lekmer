
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pOrderSave]
	@OrderId					INT,
	@PaymentCost				DECIMAL(16,2),
	@CustomerIdentificationKey	NCHAR(50),
	@AlternateAddressId			INT = NULL,
	@CivicNumber				NVARCHAR(50) = NULL,
	@OptionalDeliveryMethodId	INT,
	@OptionalFreightCost		DECIMAL(16, 2),
	@DiapersDeliveryMethodId	INT,
	@DiapersFreightCost			DECIMAL(16, 2),
	@KcoId						NVARCHAR(MAX) = NULL,
	@UserAgent					NVARCHAR(MAX) = NULL
AS 
BEGIN 
	UPDATE 
		[lekmer].[tLekmerOrder]
	SET 
		[PaymentCost] = @PaymentCost,
		[CustomerIdentificationKey] = @CustomerIdentificationKey,
		[AlternateAddressId] = @AlternateAddressId,
		[CivicNumber] = @CivicNumber,
		[OptionalDeliveryMethodId] = @OptionalDeliveryMethodId,
		[OptionalFreightCost] = @OptionalFreightCost,
		[DiapersDeliveryMethodId] = @DiapersDeliveryMethodId,
		[DiapersFreightCost] = @DiapersFreightCost,
		[UserAgent] = @UserAgent
	WHERE 
		[OrderId] = @OrderId

	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tLekmerOrder]
		(
			[OrderId],
			[PaymentCost],
			[CustomerIdentificationKey],
			[AlternateAddressId],
			[CivicNumber],
			[OptionalDeliveryMethodId],
			[OptionalFreightCost],
			[DiapersDeliveryMethodId],
			[DiapersFreightCost],
			[KcoId],
			[UserAgent]
		)
		VALUES
		(
			@OrderId,
			@PaymentCost,
			@CustomerIdentificationKey,
			@AlternateAddressId,
			@CivicNumber,
			@OptionalDeliveryMethodId,
			@OptionalFreightCost,
			@DiapersDeliveryMethodId,
			@DiapersFreightCost,
			@KcoId,
			@UserAgent
		)
	END
END
GO
