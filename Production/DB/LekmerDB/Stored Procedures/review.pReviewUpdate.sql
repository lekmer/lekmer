SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pReviewUpdate]
	@FeedbackIds	VARCHAR(MAX),
	@AuthorNames	NVARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	-- Feedback ids.
	DECLARE @tFeedback TABLE (FeedbackId INT, Ordinal INT)
	INSERT INTO @tFeedback ([FeedbackId], [Ordinal])
	SELECT [Id], [Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@FeedbackIds, @Delimiter)

	-- Author names.
	DECLARE @tAuthorNames TABLE (AuthorName NVARCHAR(50), Ordinal INT)
	INSERT INTO @tAuthorNames ([AuthorName], [Ordinal])
	SELECT [SepString], [Ordinal] FROM [generic].[fStringToStringTableWithOrdinal](@AuthorNames, @Delimiter)

	UPDATE r
	SET [r].[AuthorName] = [a].[AuthorName]
	FROM [review].[tReview] r
	INNER JOIN @tFeedback f ON [f].[FeedbackId] = [r].[RatingReviewFeedbackId]
	INNER JOIN @tAuthorNames a ON [a].[Ordinal] = [f].[Ordinal]
END
GO
