
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [addon].[pBlockTopListProductGetAllByBlock]
	@ChannelId INT,
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		p.*,
		pli.*
	FROM
		addon.vCustomBlockTopListProduct p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				NULL
			)
		CROSS APPLY (
			SELECT SUM(ps.[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[Product.Id]
		) a1
	WHERE
		p.[BlockTopListProduct.BlockId] = @BlockId
		AND p.[Product.ChannelId] = @ChannelId
		AND ISNULL(a1.[NumberInStock], p.[Product.NumberInStock]) > 0
	ORDER BY
		[BlockTopListProduct.Position]
END
GO
