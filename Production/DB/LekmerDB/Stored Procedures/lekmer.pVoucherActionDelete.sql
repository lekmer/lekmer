
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pVoucherActionDelete]
	@CartActionId INT
AS 
BEGIN
	SET NOCOUNT ON

	DELETE FROM [lekmer].tVoucherAction
	WHERE CartActionId = @CartActionId
END
GO
