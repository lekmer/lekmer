
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductRegistryDeliveryTimeSave]
	@Id					INT,
	@ProductRegistryId	INT,
	@FromDays			INT,
	@ToDays				INT,
	@Format				NVARCHAR(25)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[productlek].[tProductRegistryDeliveryTime]
	SET
		[From] = @FromDays,
		[To] = @ToDays,
		[Format] = @Format
	WHERE
		[DeliveryTimeId] = @Id AND
		[ProductRegistryId] = @ProductRegistryId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [productlek].[tProductRegistryDeliveryTime] (
			[ProductRegistryId] ,
			[DeliveryTimeId] ,
			[From] ,
			[To],
			[Format]
		)
		VALUES (
			@ProductRegistryId,
			@Id,
			@FromDays,
			@ToDays,
			@Format
		)
	END
END
GO
