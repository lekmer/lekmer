
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [cdon].[pProductTryDecreaseNumberInStock]
	@ArticleNumber VARCHAR(50),
	@QuantityToTakeFromStock INT
AS 
BEGIN
	BEGIN TRANSACTION
    
	DECLARE	@ProductId INT
	DECLARE	@SizeId INT
	DECLARE	@NumberInStockCurrent INT


	-- Product with size ?
	SELECT
		@ProductId = [ProductId],
		@SizeId = [SizeId],
		@NumberInStockCurrent = [NumberInStock]
	FROM
		[lekmer].[tProductSize] ps
	WHERE
		ps.[ErpId] = @ArticleNumber


	-- Product without size ?
	IF @SizeId IS NULL
	SELECT
		@ProductId = lp.[ProductId],
		@NumberInStockCurrent = p.[NumberInStock]
	FROM
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p ON p.[ProductId] = lp.[ProductId]
	WHERE
		lp.[HYErpId] = SUBSTRING(@ArticleNumber, 1, 12)  -- [0008706-1017]-223  // SELECT SUBSTRING('0008706-1017-223', 1, 12)


	-- Product doesn't exist or has zero stock ?
	IF ( @NumberInStockCurrent IS NULL OR @NumberInStockCurrent = 0 ) 
	BEGIN
		SELECT
			@ArticleNumber AS ArticleNumber,
			0 AS QuantityTakenFromStock,
			0 AS NewStockBalance,
			CAST(0 AS BIT) AS Success
	END
	ELSE 
	BEGIN
		IF @SizeId IS NULL
			EXEC [lekmer].[pProductDecreaseNumberInStock] @ProductId, @QuantityToTakeFromStock
		ELSE
			EXEC [lekmer].[pProductSizeDecreaseNumberInStock] @ProductId, @SizeId, @QuantityToTakeFromStock

		IF ( @NumberInStockCurrent - @QuantityToTakeFromStock < 0 )
		BEGIN
			SELECT
				@ArticleNumber AS ArticleNumber,
				@NumberInStockCurrent AS QuantityTakenFromStock,
				0 AS NewStockBalance,
				CAST(1 AS BIT) AS Success
		END
		ELSE
		BEGIN
			SELECT
				@ArticleNumber AS ArticleNumber,
				@QuantityToTakeFromStock AS QuantityTakenFromStock,
				@NumberInStockCurrent - @QuantityToTakeFromStock AS NewStockBalance,
				CAST(1 AS BIT) AS Success
		END
		
		--Clear cache
		EXEC [corelek].[pRemoveFromCacheProduct] @ProductId, @SizeId
		
		--TrackProductChanges
		INSERT [productlek].[tProductChangeEvent] ( [ProductId], [EventStatusId], [CdonExportEventStatusId], [CreatedDate], [ActionAppliedDate], [Reference] )
		VALUES (@ProductId, 0, 0, GETDATE(), NULL, 'cdon product try decrease number in stock')
	END
	COMMIT
END
GO
