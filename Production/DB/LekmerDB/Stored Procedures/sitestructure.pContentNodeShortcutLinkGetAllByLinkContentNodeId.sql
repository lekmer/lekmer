SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pContentNodeShortcutLinkGetAllByLinkContentNodeId]
@LinkContentNodeId int
as
BEGIN
	DECLARE @tContentNode TABLE (tContentNodeId INT, tParentContentNodeId INT);
	WITH ContentNode (ContentNodeId, ParentContentNodeId) AS 
	(
		SELECT ContentNodeId, ParentContentNodeId
		FROM  sitestructure.tContentNode
		WHERE ParentContentNodeId = @LinkContentNodeId
		UNION ALL
		SELECT C.ContentNodeId, C.ParentContentNodeId
		FROM sitestructure.tContentNode C 
		JOIN ContentNode OuterC ON OuterC.ContentNodeId = C.ParentContentNodeId
	)
	INSERT INTO @tContentNode (tContentNodeId, tParentContentNodeId)
	SELECT ContentNodeId, ParentContentNodeId FROM ContentNode
	UNION ALL
	SELECT [ContentNode.ContentNodeId], [ContentNode.ParentContentNodeId]
	FROM  sitestructure.vCustomContentNode
	WHERE [ContentNode.ContentNodeId] = @LinkContentNodeId
	
	SELECT 
		cnsl.*
	FROM
		sitestructure.vCustomContentNodeShortcutLinkSecure cnsl
		INNER JOIN @tContentNode cn ON cnsl.[ContentNodeShortcutLink.LinkContentNodeId] = cn.tContentNodeId
END

GO
