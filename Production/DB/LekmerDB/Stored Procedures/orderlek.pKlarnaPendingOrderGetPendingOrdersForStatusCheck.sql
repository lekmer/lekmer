
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pKlarnaPendingOrderGetPendingOrdersForStatusCheck]
	@CheckToDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[po].*
	FROM
		[orderlek].[vKlarnaPendingOrder] po
	WHERE
		[po].[KlarnaPendingOrder.LastAttempt] < @CheckToDate
END
GO
