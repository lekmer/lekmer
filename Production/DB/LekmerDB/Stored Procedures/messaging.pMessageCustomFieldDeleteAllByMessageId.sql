SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [messaging].[pMessageCustomFieldDeleteAllByMessageId]
@MessageId			uniqueidentifier
as
begin

	delete from
		[messaging].[tMessageCustomField]
	where
		MessageId = @MessageId

end




GO
