SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroupRatingSave]
	@RatingGroupId INT,
	@RatingId INT,
	@Ordinal INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tRatingGroupRating] (
		[RatingGroupId],
		[RatingId],
		[Ordinal]
	)
	VALUES (
		@RatingGroupId,
		@RatingId,
		@Ordinal
	)
END
GO
