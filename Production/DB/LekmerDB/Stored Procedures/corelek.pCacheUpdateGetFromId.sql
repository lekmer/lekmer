SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [corelek].[pCacheUpdateGetFromId]
	@FromId	BIGINT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[cu].[Id],
		[cu].[ManagerName],
		[cu].[UpdateType],
		[cu].[Key],
		[cu].[CreationDate]
	FROM
		[corelek].[tCacheUpdate] cu
	WHERE
		cu.[Id] > @FromId
END
GO
