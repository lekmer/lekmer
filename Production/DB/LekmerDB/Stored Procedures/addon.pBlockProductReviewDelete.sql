SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [addon].[pBlockProductReviewDelete]
@BlockId    int
AS
BEGIN 
         DELETE 
                   [addon].[tBlockProductReview]
         WHERE 
                   [BlockId] = @BlockId
END 
GO
