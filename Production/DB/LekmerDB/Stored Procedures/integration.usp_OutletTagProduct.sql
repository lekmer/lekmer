
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[usp_OutletTagProduct]
AS
BEGIN
	-- is product
	-- product is passive
	-- product has any discount
	-- stock > 0
	
	--> add tag 'outlet'

	SET NOCOUNT ON
	
	DECLARE @PassiveId INT
	SET @PassiveId = (SELECT [StockStatusId] FROM [productlek].[tStockStatus] WHERE [CommonName] = 'Passive')
	
	DECLARE @OutletTagId INT
	SET @OutletTagId = (SELECT TagId FROM lekmer.tTag WHERE [CommonName] = 'outlet')

	DECLARE @ProductId INT
	DECLARE @NumberInStock INT
	
	DECLARE cur_product CURSOR FAST_FORWARD FOR
	SELECT 
		[lp].[ProductId],
		[p].[NumberInStock]
	FROM
		[lekmer].[tLekmerProduct] lp
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
	WHERE
		[lp].[ProductTypeId] = 1 -- Is Product
		AND [lp].[StockStatusId] = @PassiveId -- Is Passive
		AND EXISTS ( -- has any discount
			SELECT 1 
			FROM [export].[tProductPrice] pp 
			WHERE [pp].[ProductId] = [lp].[ProductId]
				  AND [DiscountPriceIncludingVat] IS NOT NULL
				  AND [DiscountPriceExcludingVat] IS NOT NULL
		)

	OPEN cur_product

	FETCH NEXT FROM cur_product 
	INTO @ProductId,
		 @NumberInStock

	WHILE @@FETCH_STATUS = 0
	BEGIN
		-- without sizes
		IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId)
			BEGIN
				IF (@NumberInStock > 0)
					IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @OutletTagId)
						BEGIN
							INSERT INTO [lekmer].[tProductTag] ([ProductId], [TagId])
							VALUES (@ProductId, @OutletTagId)
						END
					EXEC [lekmer].[pProductTagUsageSave] @ProductId, @OutletTagId, -1
			END
		ELSE
			BEGIN
				-- with sizes				
				IF EXISTS (SELECT 1 FROM [lekmer].[tProductSize] WHERE [ProductId] = @ProductId AND [NumberInStock] > 0)
					IF NOT EXISTS (SELECT 1 FROM [lekmer].[tProductTag] WHERE [ProductId] = @ProductId AND [TagId] = @OutletTagId)
						BEGIN 
							INSERT INTO [lekmer].[tProductTag] ([ProductId], [TagId])
							VALUES (@ProductId, @OutletTagId)
						END
					EXEC [lekmer].[pProductTagUsageSave] @ProductId, @OutletTagId, -1
			END
		
		FETCH NEXT FROM cur_product 
		INTO @ProductId,
			 @NumberInStock
	END

	CLOSE cur_product
	DEALLOCATE cur_product
END
GO
