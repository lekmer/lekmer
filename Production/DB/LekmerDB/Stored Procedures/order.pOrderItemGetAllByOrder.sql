SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderItemGetAllByOrder]
	@OrderId	INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[order].[vCustomOrderItem]
	WHERE
		[OrderItem.OrderId] = @OrderId
END

GO
