
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludesGetAll]
	@ContentMessageId	INT
AS
BEGIN
	SELECT [ProductId] FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ContentMessageId] = @ContentMessageId
	SELECT [CategoryId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [ContentMessageId] = @ContentMessageId
	SELECT [BrandId] FROM [lekmer].[tContentMessageIncludeBrand] WHERE [ContentMessageId] = @ContentMessageId
	SELECT [SupplierId] FROM [lekmer].[tContentMessageIncludeSupplier] WHERE [ContentMessageId] = @ContentMessageId
END
GO
