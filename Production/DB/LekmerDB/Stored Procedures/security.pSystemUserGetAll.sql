SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserGetAll]
as
begin
	set nocount on

	select
		*
	from
		[security].[vCustomSystemUser]
end

GO
