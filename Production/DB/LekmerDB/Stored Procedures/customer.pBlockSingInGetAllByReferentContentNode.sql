SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pBlockSingInGetAllByReferentContentNode]
@ContentNodeId	INT
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @tContentNode TABLE (tContentNodeId INT, tParentContentNodeId INT);

	WITH ContentNode (ContentNodeId, ParentContentNodeId) AS 
	(
		SELECT ContentNodeId, ParentContentNodeId
		FROM  sitestructure.tContentNode
		WHERE ParentContentNodeId = @ContentNodeId

		UNION ALL

		SELECT C.ContentNodeId, C.ParentContentNodeId
		FROM sitestructure.tContentNode C 
		INNER JOIN ContentNode OuterC ON OuterC.ContentNodeId = C.ParentContentNodeId
	)
	INSERT INTO @tContentNode (tContentNodeId, tParentContentNodeId)
	SELECT ContentNodeId, ParentContentNodeId FROM ContentNode
		
	UNION ALL	
	SELECT ContentNodeId, ParentContentNodeId
	FROM  sitestructure.tContentNode
	WHERE ContentNodeId = @ContentNodeId
	
	SELECT
		br.*,
		b.*
	FROM
		[customer].[vCustomBlockSignIn] AS br
		INNER JOIN [sitestructure].[vCustomBlockSecure] AS b ON br.[BlockSignIn.BlockId] = b.[Block.BlockId]
		INNER JOIN @tContentNode cn ON br.[BlockSignIn.RedirectContentNodeId] = cn.tContentNodeId	
END

GO
