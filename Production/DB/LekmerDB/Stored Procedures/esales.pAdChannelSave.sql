SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [esales].[pAdChannelSave]
	@AdChannelId INT,
	@AdId INT,
	@RegistryId INT,
	@Title NVARCHAR(250),
	@ShowOnSite BIT,
	@LandingPageUrl NVARCHAR(MAX),
	@LandingPageId INT,
	@ImageUrl NVARCHAR(MAX),
	@ImageId INT
AS
BEGIN
	SET NOCOUNT ON

	IF @AdChannelId IS NOT NULL
		UPDATE
			[esales].[tAdChannel]
		SET
			[AdId] = @AdId,
			[RegistryId] = @RegistryId,
			[Title] = @Title,
			[ShowOnSite] = @ShowOnSite,
			[LandingPageUrl] = @LandingPageUrl,
			[LandingPageId] = @LandingPageId,
			[ImageUrl] = @ImageUrl,
			[ImageId] = @ImageId
		WHERE
			[AdChannelId] = @AdChannelId

	IF  @AdChannelId IS NULL OR @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [esales].[tAdChannel] (
			[AdId],
			[RegistryId],
			[Title],
			[ShowOnSite],
			[LandingPageUrl],
			[LandingPageId],
			[ImageUrl],
			[ImageId]
		)
		VALUES (
			@AdId,
			@RegistryId,
			@Title,
			@ShowOnSite,
			@LandingPageUrl,
			@LandingPageId,
			@ImageUrl,
			@ImageId
		)

		SET @AdChannelId = SCOPE_IDENTITY()
	END

	RETURN @AdChannelId
END
GO
