SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SELECT
		rf.*
	FROM
		[review].[vRatingFolder] rf
	WHERE
		(rf.[RatingFolder.ParentRatingFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		(rf.[RatingFolder.ParentRatingFolderId] IS NULL AND @ParentId IS NULL)
END
GO
