SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [productlek].[pProductChangeEventSetStatusByIdList]
	@ProductChangeEventIds VARCHAR(MAX) ,
	@Delimiter CHAR(1) ,
	@EventStatusId INT ,
	@ActionAppliedDate DATETIME = NULL
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@ProductChangeEventIds, @Delimiter)

	IF ( @ActionAppliedDate IS NULL ) 
	BEGIN
		UPDATE
			pce
		SET	
			[EventStatusId] = @EventStatusId
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[EventStatusId] != @EventStatusId
	END
	ELSE
	BEGIN
		UPDATE
			pce
		SET	
			[EventStatusId] = @EventStatusId,
			[ActionAppliedDate] = @ActionAppliedDate
		FROM
			[productlek].[tProductChangeEvent] pce
			INNER JOIN @IdList il ON [pce].[ProductChangeEventId] = [il].[Id]
		WHERE
			[pce].[EventStatusId] != @EventStatusId
	END
END
GO
