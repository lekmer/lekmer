SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionCurrencyInsert]
	@ProductActionId	INT,
	@CurrencyId			INT,
	@Value				DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionCurrency] (
		ProductActionId,
		CurrencyId,
		Value
	)
	VALUES (
		@ProductActionId,
		@CurrencyId,
		@Value
	)
END
GO
