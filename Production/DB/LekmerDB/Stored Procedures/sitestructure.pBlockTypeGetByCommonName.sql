SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockTypeGetByCommonName]
	@CommonName varchar(50)
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomBlockType]
	where
		[BlockType.CommonName] = @CommonName
end

GO
