SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pCartActionTargetProductTypeSave]
	@CartActionId INT,
	@TargetProductTypeIds VARCHAR(MAX),
	@Delimiter CHAR(1)
AS 
BEGIN
	SET NOCOUNT ON

	DECLARE	@IdList TABLE ( Id INT, PRIMARY KEY (Id) )

	INSERT	INTO @IdList
	SELECT Id FROM [generic].[fnConvertIDListToTableWithOrdinal](@TargetProductTypeIds, @Delimiter)
	
	DELETE [campaignlek].[tCartActionTargetProductType]
	WHERE
		[CartActionId] = @CartActionId
	
	INSERT [campaignlek].[tCartActionTargetProductType] ( [CartActionId], [ProductTypeId] )
	SELECT
		@CartActionId,
		Id
	FROM
		@IdList
END
GO
