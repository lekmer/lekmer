SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockRatingItemGetAllByBlock]
	@BlockId	INT
AS
BEGIN
	SELECT 
		bri.*
	FROM 
		[review].[vBlockRatingItem] AS bri
	WHERE
		bri.[BlockRatingItem.BlockId] = @BlockId
END
GO
