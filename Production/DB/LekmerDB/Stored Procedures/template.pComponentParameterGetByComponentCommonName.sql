SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Artymyshyn M.		Date: 17.02.2009		Time: 11:42
Description:
			create					
*/


CREATE PROCEDURE [template].[pComponentParameterGetByComponentCommonName]
	@CommonName VARCHAR(50)
AS
BEGIN
	SELECT
		cp.*
	FROM
		[template].vCustomComponentParameter cp
		INNER JOIN [template].[vCustomComponent] c ON cp.[ComponentParameter.Id] = c.[Component.Id]
	WHERE
		c.[Component.Id] = @CommonName
END

GO
