SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pReviewHistoryInsert]
	@ReviewId INT,
	@AuthorId INT
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [review].[tReviewHistory] (
		[ReviewId],
		[Title],
		[Message],
		[CreationDate],
		[AuthorId]
	)
	SELECT
		[r].[ReviewId],
		[r].[Title],
		[r].[Message],
		GETDATE(),
		@AuthorId
	FROM [review].[tReview] r
	WHERE [r].[ReviewId] = @ReviewId
	AND NOT EXISTS (SELECT 1 FROM [review].[tReviewHistory] rh 
					WHERE [rh].[ReviewId] = @ReviewId
							AND LTRIM(RTRIM([rh].[Title])) = LTRIM(RTRIM([r].[Title]))
							AND LTRIM(RTRIM([rh].[Message])) = LTRIM(RTRIM([r].[Message])))
END
GO
