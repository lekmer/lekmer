
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroup_Delete]
	@RatingGroupId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [review].[pRatingGroupCategoryDelete] @RatingGroupId, NULL
	EXEC [review].[pRatingGroupProductDelete] @RatingGroupId, NULL
	EXEC [review].[pRatingGroupRatingDelete] NULL, @RatingGroupId
	
	DELETE FROM [review].[tRatingGroup]
	WHERE [RatingGroupId] = @RatingGroupId
END
GO
