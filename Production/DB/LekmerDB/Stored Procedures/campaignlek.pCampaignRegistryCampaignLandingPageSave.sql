SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignLandingPageSave]
	@CampaignLandingPageId	INT,
	@CampaignRegistryIds	VARCHAR(MAX),
	@IconIds				VARCHAR(MAX),
	@ImageIds				VARCHAR(MAX),
	@ContentNodeIds		VARCHAR(MAX),
	@Delimiter				CHAR(1)
AS
BEGIN
	SET NOCOUNT ON
	
	-- Delete campaign landing page data specific for registry
	DELETE FROM [campaignlek].[tCampaignRegistryCampaignLandingPage] WHERE [CampaignLandingPageId] = @CampaignLandingPageId
	
	
	DECLARE @tCampaignRegistryIds TABLE (CampaignRegistryId INT, Ordinal INT)
	INSERT INTO @tCampaignRegistryIds ([CampaignRegistryId], [Ordinal])
	SELECT [p].[ID], [p].[Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@CampaignRegistryIds, @Delimiter) p
	
	DECLARE @tIconIds TABLE (IconId INT, Ordinal INT)
	INSERT INTO @tIconIds ([IconId], [Ordinal])
	SELECT [p].[ID], [p].[Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@IconIds, @Delimiter) p
	
	DECLARE @tImageIds TABLE (ImageId INT, Ordinal INT)
	INSERT INTO @tImageIds ([ImageId], [Ordinal])
	SELECT [p].[ID], [p].[Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ImageIds, @Delimiter) p
	
	DECLARE @tContentNodeIds TABLE (ContentNodeId INT, Ordinal INT)
	INSERT INTO @tContentNodeIds ([ContentNodeId], [Ordinal])
	SELECT [p].[ID], [p].[Ordinal] FROM [generic].[fnConvertIDListToTableWithOrdinal](@ContentNodeIds, @Delimiter) p
	
	
	INSERT INTO [campaignlek].[tCampaignRegistryCampaignLandingPage] (
		[CampaignLandingPageId],
		[CampaignRegistryId],
		[IconMediaId],
		[ImageMediaId],
		[LinkContentNodeId]
	)
	SELECT
		@CampaignLandingPageId,
		[cr].[CampaignRegistryId],
		CASE [ic].[IconId] WHEN -1 THEN NULL ELSE [ic].[IconId] END,
		CASE [im].[ImageId] WHEN -1 THEN NULL ELSE [im].[ImageId] END,
		[cn].[ContentNodeId]
	FROM @tCampaignRegistryIds cr
	INNER JOIN @tIconIds ic ON [ic].[Ordinal] = [cr].[Ordinal]
	INNER JOIN @tImageIds im ON [im].[Ordinal] = [cr].[Ordinal]
	INNER JOIN @tContentNodeIds cn ON [cn].[Ordinal] = [cr].[Ordinal]
END
GO
