SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [security].[pRoleDelete]
	@RoleId	int
as
begin
	delete from
		[security].[tRole]
	where
		[RoleId] = @RoleId
end

GO
