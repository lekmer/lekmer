
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pBlockEsalesRecommendV2Save]
	@BlockId INT,
	@PanelName NVARCHAR(MAX),
	@WindowLastEsalesValue INT
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[esales].[tBlockEsalesRecommendV2]
	SET
		[PanelName] = @PanelName,
		[WindowLastEsalesValue] = @WindowLastEsalesValue
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [esales].[tBlockEsalesRecommendV2]
	(
		[BlockId],
		[PanelName],
		[WindowLastEsalesValue]
	)
	VALUES
	(
		@BlockId,
		@PanelName,
		@WindowLastEsalesValue
	)
END
GO
