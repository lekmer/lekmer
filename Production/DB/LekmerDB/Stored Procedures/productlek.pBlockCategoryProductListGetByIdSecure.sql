SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockCategoryProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT
		[bcpl].*,
		[b].*
	FROM 
		[productlek].[vBlockCategoryProductList] bcpl
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [bcpl].[BlockCategoryProductList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bcpl].[BlockCategoryProductList.BlockId] = @BlockId
END
GO
