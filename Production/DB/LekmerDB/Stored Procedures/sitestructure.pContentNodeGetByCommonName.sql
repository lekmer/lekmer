SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [sitestructure].[pContentNodeGetByCommonName]
 @RegistryId int,
 @ContentNodeCommonName varchar(50)
as
begin
 SELECT
  cn.*
 FROM 
  [vContentNodeSecure] AS cn  
 WHERE
  cn.[ContentNode.SiteStructureRegistryId] = @RegistryId
  AND cn.[ContentNode.CommonName] = @ContentNodeCommonName
end
GO
