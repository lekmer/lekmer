SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pDealOfTheDayOrinalSort]

AS
begin
	set nocount on
	
	begin try
		begin transaction
				
		declare @BlockId int	
		declare @counter int
		declare @blockIdCount int
		set @counter = 1
		set @blockIdCount = (select COUNT(BlockId) from integration.tDealOfTheDay)
		
		while @counter <= @blockIdCount
		begin
		
			set @BlockId = (select BlockId from integration.tDealOfTheDay where Rownumber = @counter)
			
			declare @CurrentDealOfThedayOrdinal int
			declare @ProductId int
			
			set @CurrentDealOfThedayOrdinal = (select MIN(Ordinal) from product.tBlockProductListProduct where BlockId = @BlockId)
			set @ProductId = (select productid from product.tBlockProductListProduct where BlockId = @BlockId
															and Ordinal = @CurrentDealOfThedayOrdinal)
			
			update
				product.tBlockProductListProduct
			set 
				Ordinal = ((select MAX(ordinal) from product.tBlockProductListProduct where BlockId = @BlockId) + 10)
			where
				ProductId = @ProductId
				and BlockId = @BlockId
		
	
		
			update
				product.tBlockProductListProduct
			set
				Ordinal = (Ordinal - 10)
			where
				BlockId = @BlockId
					
					
			set @counter = @counter + 1
		end
		
		
		commit
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
		INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(error_message(), ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	end catch		 
end
GO
