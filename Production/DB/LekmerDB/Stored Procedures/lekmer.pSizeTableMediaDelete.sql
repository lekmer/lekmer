
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaDelete]
	@SizeTableMediaId INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableMediaProductRegistryDeleteAllBySizeTableMedia] @SizeTableMediaId

	DELETE FROM
		[lekmer].[tSizeTableMedia]
	WHERE
		[SizeTableMediaId] = @SizeTableMediaId
END
GO
