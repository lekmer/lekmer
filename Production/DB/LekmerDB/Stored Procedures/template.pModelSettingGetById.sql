SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************  Version 1  **********************************************
User: Volodymyr Y.   19.01.2009                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelSettingGetById]
    @ModelSettingId INT
AS 
BEGIN
    SET NOCOUNT ON
    
    SELECT  *
    FROM    [template].[vCustomModelSetting]
    WHERE   [ModelSetting.Id] = @ModelSettingId
END

GO
