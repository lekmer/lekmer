SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFixedPriceActionCurrencyGetByAction]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vFixedPriceActionCurrency]
	WHERE 
		[FixedPriceActionCurrency.ProductActionId] = @ProductActionId
END

GO
