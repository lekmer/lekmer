
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesAdsSave]
	@BlockId INT,
	@PanelPath NVARCHAR(MAX),
	@Format NVARCHAR(50),
	@Gender NVARCHAR(50),
	@ProductCategory NVARCHAR(250)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tBlockEsalesAds]
	SET
		[PanelPath]		= @PanelPath,
		[Format]		= @Format,
		[Gender]		= @Gender,
		[ProductCategory] = @ProductCategory
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockEsalesAds]
	(
		[BlockId],
		[PanelPath],
		[Format],
		[Gender],
		[ProductCategory]
	)
	VALUES
	(
		@BlockId,
		@PanelPath,
		@Format,
		@Gender,
		@ProductCategory
	)
END
GO
