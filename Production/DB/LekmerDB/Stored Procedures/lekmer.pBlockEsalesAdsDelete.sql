
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesAdsDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[lekmer].[tBlockEsalesAds]
	WHERE
		[BlockId] = @BlockId
END
GO
