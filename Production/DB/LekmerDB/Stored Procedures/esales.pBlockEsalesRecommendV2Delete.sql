SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [esales].[pBlockEsalesRecommendV2Delete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE
		[esales].[tBlockEsalesRecommendV2]
	WHERE
		[BlockId] = @BlockId
END
GO
