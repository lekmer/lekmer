SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pStoreProductGetAllByProduct]
	@ProductId int
AS 
BEGIN 
	SELECT 
		*
	FROM 
		[product].[vCustomStoreProduct] c
	WHERE 
		c.[StoreProduct.ProductId] = @ProductId
END 

GO
