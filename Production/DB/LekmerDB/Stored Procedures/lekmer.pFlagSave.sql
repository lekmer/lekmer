SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pFlagSave]
	@FlagId		INT,
	@Title		NVARCHAR(50),
	@Class		VARCHAR(50),
	@Ordinal	INT
AS
BEGIN
	
	IF EXISTS (SELECT 1 FROM lekmer.tFlag WHERE Title = @Title AND FlagId <> @FlagId)
		RETURN -1

	UPDATE
		[lekmer].[tFlag]
	SET
		[Title] = @Title,	
		[Class] = @Class,
		[Ordinal] = @Ordinal
	WHERE
		[FlagId] = @FlagId
		
	IF @@ROWCOUNT = 0
	BEGIN
		SET @Ordinal = (SELECT MAX(Ordinal) FROM [lekmer].[tFlag]) + 1
		IF @Ordinal is null
			BEGIN
				SET @Ordinal = 1
			END
		INSERT INTO [lekmer].[tFlag]
		( 
			[Title],
			[Class],
			[Ordinal]
		)
		VALUES
		(
			@Title,
			@Class ,
			@Ordinal
		)
		SET @FlagId = scope_identity()						
	END 
	RETURN @FlagId
END

GO
