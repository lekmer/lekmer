SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pAdChannelGetAllByAd]
	@AdId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[ac].*
	FROM
		[esales].[vAdChannel] ac
	WHERE
		[ac].[AdChannel.AdId] = @AdId
END
GO
