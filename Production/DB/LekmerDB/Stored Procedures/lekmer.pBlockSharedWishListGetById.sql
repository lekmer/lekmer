
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockSharedWishListGetById]
	@BlockId INT,
	@LanguageId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[b].*,
		[bs].*
	FROM
		[sitestructure].[vCustomBlock] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
