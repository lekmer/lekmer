
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pNewsletterSubscriberDelete]
	@Id	INT
AS
BEGIN
	DELETE
		[lekmer].[tNewsletterSubscriber]
	WHERE
		[SubscriberId] = @Id
	RETURN @@ROWCOUNT
END
GO
