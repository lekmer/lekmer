SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************  Version 1  **********************************************
User: Victor E.      16.10.2008                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelFragmentRegionGetBylId] 
	@ModelFragmentRegionId INT
AS 
BEGIN
    SET NOCOUNT ON

    SELECT  *
    FROM    [template].[vCustomModelFragmentRegion]
    WHERE   [ModelFragmentRegion.Id] = @ModelFragmentRegionId
END

GO
