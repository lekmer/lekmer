SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pBlockSignInGetById]
@LanguageId INT,
@BlockId int
as	
begin
	set nocount on
	select
		s.*,
		b.*
	from
		[customer].[vCustomBlockSignIn] as s
		inner join [sitestructure].[vCustomBlock] as b on s.[BlockSignIn.BlockId] = b.[Block.BlockId]
	where
		s.[BlockSignIn.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
end

GO
