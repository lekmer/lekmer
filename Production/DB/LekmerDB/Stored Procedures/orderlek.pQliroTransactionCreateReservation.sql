SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionCreateReservation]
	@ClientRef VARCHAR(50),
	@Mode INT,
	@TransactionTypeId INT,
	@CivicNumber VARCHAR(50),
	@OrderId INT,
	@Amount DECIMAL(16,2),
	@CurrencyCode VARCHAR(50),
	@PaymentType VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroTransaction]
			( [ClientRef],
			  [Mode],
			  [TransactionTypeId],
			  [CivicNumber],
			  [OrderId],
			  [Amount],
			  [CurrencyCode],
			  [PaymentType],
			  [Created]
			)
	VALUES
			( @ClientRef,
			  @Mode,
			  @TransactionTypeId,			  
			  @CivicNumber,
			  @OrderId,
			  @Amount,
			  @CurrencyCode,
			  @PaymentType,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
