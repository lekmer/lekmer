SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGetById] 
	@CustomerId int
as	
begin
	set nocount on

	select
		*	
	from
		[customer].[vCustomCustomer] 
	where
		[Customer.CustomerId] = @CustomerId
end

GO
