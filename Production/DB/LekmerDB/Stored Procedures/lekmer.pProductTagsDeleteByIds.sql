SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductTagsDeleteByIds]
	@ProductTagIds	VARCHAR(MAX),
	@Delimiter		CHAR(1) = ','
AS
BEGIN
	DELETE pt
	FROM [lekmer].[tProductTag] pt
	INNER JOIN [generic].[fnConvertIDListToTable](@ProductTagIds, @Delimiter) AS t ON [t].[ID] = [pt].[ProductTagId]
END
GO
