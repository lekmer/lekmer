SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customer].[pCustomerGroupSearch]
@SearchString	varchar(MAX)
AS
BEGIN
	SET @SearchString = [generic].[fPrepareSearchParameter](@SearchString)
	select
		c.*
	from
		[customer].[vCustomCustomerGroup] AS c
	where
		c.[CustomerGroup.Title] like @SearchString ESCAPE '\'
	order by
		c.[CustomerGroup.FolderId] asc
end

GO
