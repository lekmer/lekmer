SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pProductGetViewInclPackageProductsById]
	@ProductId	INT,
	@CustomerId INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	SELECT *		
	FROM 
		[product].[vCustomProductViewInclPackageProducts] AS p
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = p.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				p.[Product.CurrencyId],
				p.[Product.Id],
				p.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		p.[Product.Id] = @ProductId
		AND p.[Product.ChannelId] = @ChannelId
END
GO
