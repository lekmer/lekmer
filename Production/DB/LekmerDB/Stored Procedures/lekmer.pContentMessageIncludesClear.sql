
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludesClear]
	@ProductIds		VARCHAR(MAX),
	@CategoryIds	VARCHAR(MAX),
	@BrandIds		VARCHAR(MAX),
	@SupplierIds	VARCHAR(MAX),
	@Delimiter		CHAR(1)
AS
BEGIN
	DELETE ip FROM [lekmer].[tContentMessageIncludeProduct] ip
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter)) p ON [p].[ID] = [ip].[ProductId]

	DELETE ic FROM [lekmer].[tContentMessageIncludeCategory] ic
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter)) c ON [c].[ID] = [ic].[CategoryId]
	
	DELETE ib FROM [lekmer].[tContentMessageIncludeBrand] ib
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter)) b ON [b].[ID] = [ib].[BrandId]
	
	DELETE isup FROM [lekmer].[tContentMessageIncludeSupplier] isup
	INNER JOIN (SELECT [ID] FROM [generic].[fnConvertIDListToTable](@SupplierIds, @Delimiter)) b ON [b].[ID] = [isup].[SupplierId]
END
GO
