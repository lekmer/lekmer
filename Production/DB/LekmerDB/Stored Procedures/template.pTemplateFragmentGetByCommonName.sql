SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 3  *****************
User: Roman D.		Date: 27.01.2009		Time: 17:55
Description:
			add mf.[Size]
*****************  Version 2  *****************
User: Roman G.		Date: 23.12.2008		Time: 8:10
Description:
			Remove TemplateFragmentId
*****************  Version 1  *****************
User: Victor E.		Date: 07.10.2008		Time: 18:30
Description:
			Created 
*/

CREATE PROCEDURE [template].[pTemplateFragmentGetByCommonName]
	@TemplateId int,
	@CommonName	varchar(50) 
AS
begin
	set nocount on

	SELECT
	    tf.*,
		mf.*
	from
		[template].[vCustomModelFragment] as mf 
		inner join [template].[vCustomTemplateFragment] as tf on mf.[ModelFragment.Id] = tf.[TemplateFragment.ModelFragmentId]
	where
		tf.[TemplateFragment.TemplateId] = @TemplateId
		and	mf.[ModelFragment.CommonName] = @CommonName
end

GO
