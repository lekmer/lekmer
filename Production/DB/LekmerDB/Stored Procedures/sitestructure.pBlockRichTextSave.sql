SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

/*
*****************  Version 1  *****************
User: Yura P.	Date: 02.12.2008	Time: 12:00
Description:	Created

*****************  Version 2  *****************
User: Yura P.	Date: 09.12.2008	Time: 17:00
Description:	Edited (rename Body to Content)
*/

CREATE procedure [sitestructure].[pBlockRichTextSave]
	@BlockId		int,
	@Content		nvarchar(max)
as
begin
	
	update
		[sitestructure].[tBlockRichText]
	set
		[Content]		= @Content
	where
		[BlockId]		= @BlockId
		
	if @@ROWCOUNT = 0
	begin
		insert
			[sitestructure].[tBlockRichText]
		(
			[BlockId],
			[Content]
		)
		values
		(
			@BlockId,
			@Content
		)
	end
end
GO
