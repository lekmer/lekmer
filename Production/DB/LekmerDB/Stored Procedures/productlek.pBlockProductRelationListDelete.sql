SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductRelationListDelete]
	@BlockId	INT
AS
BEGIN
	DELETE FROM [productlek].[tBlockProductRelationList]
	WHERE [BlockId] = @BlockId
END
GO
