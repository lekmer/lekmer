
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteExpiredItems]
AS
BEGIN
	DECLARE @CurrentDate DATETIME
	SET @CurrentDate = GETDATE()

	DELETE 
		cpuh 
	FROM
		[lekmer].[tContentPageUrlHistory] cpuh
		INNER JOIN [lekmer].[tHistoryLifeIntervalType] hlit ON cpuh.HistoryLifeIntervalTypeId = hlit.HistoryLifeIntervalTypeId
	WHERE 
		@CurrentDate > DATEADD(DAY, hlit.DaysActive, cpuh.LastUsageDate)
END
GO
