
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateSeoContentPageSettingsFI]
AS 
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		BEGIN TRANSACTION				

		-- Insert new contentnodeIds in tContentPageSeoSetting
		INSERT INTO [sitestructure].[tContentPageSeoSetting] (ContentNodeId)
		SELECT
			[n].[ContentNodeId]
		FROM
			[sitestructure].[tContentNode] n --3
		WHERE
			[n].[SiteStructureRegistryId] = 4
			AND [n].[ContentNodeTypeId] = 3 -- detta är contentpages
			AND [n].[ContentNodeId] NOT IN (SELECT ContentNodeId FROM sitestructure.tContentPageSeoSetting)


		------------------------------------------------------------
		-- <Leksaker Kategori Actionfigurer - nivå 2 och nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ja lelut netistä. Lekmer.fi – lelukauppa netissä.',
			cps.[Description] = 'Osta ' + cn.Title + ' netistä. Tarjoamme laajan leluvalikoiman ja tunnetut tuottemerkit. '
			+ 'Toimitusaika on 3-5 arkipäivää - Lekmer.fi sinun lelukauppasi.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND cn.ParentContentNodeId = 1005110 -- Lelut
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Leksaker Underkategori Starwars - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = x.Title + ' ' + cn.Title + ' Lelut netistä. Lekmer.fi – lelut netissä.',
			cps.[Description] = 'Osta ' + x.Title + ' ' + cn.Title + ' Tarjoamme laajan leluvalikoiman ja tunnetut tuottemerkit. '
			+ 'Toimitusaika on 3-5 arkipäivää - Lekmer.fi sinun lelukauppasi.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1005110) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barn & Baby Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = 'Kun tarvitset ' + cn.Title + ' lastasi, Lapsi ja vauva Lekmer.fi - verkkokaupasta – Osta lastentuotteet netistä.',
			cps.[Description] = 'Osta ' + cn.Title + ' kaikki tarvikkeet netistä. '
			+ 'Saa lasten - ja vauvantuotteet toimitettua 3-5 arkipäivässä Lekmer.fi-verkkokaupasta.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND cn.ParentContentNodeId = 1004990 -- Lapset & vauvat
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barn & Baby Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = x.Title + ' ja muut tuotteet lapsen ' + cn.Title + ' ja istumiseen. '
			+ 'Kaikki Lapsi ja vauva netistä Lekmer.fi - verkkokaupasta - Osta lastentarvikkeet netistä.',
			cps.[Description] = 'Osta ' + x.Title + ' ' + cn.Title + ' netistä. '
			+ 'Tarjoamme laajan leluvalikoiman ja tunnetut tuottemerkit. Toimitusaika on 3-5 arkipäivää - Lekmer.fi sinun lelukauppasi.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1004990) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barnkläder Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET
			cps.Title = cn.Title + ' ja lastenvaatteet Lekmer.fi - verkkokaupasta – Lasten - ja vauvanvaatteet netistä.',
			cps.[Description] = 'Osta ' + cn.Title + ' netistä. '
			+ 'Laaja valikoima makeita lasten - ja vauvanvaatteita – osta lastenvaatteet netistä, '
			+ 'Lekmer.fi-verkkokaupasta ja saa ne toimitettua 3-5 arkipäivän sisällä.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND cn.ParentContentNodeId = 1007276 -- Lastenvaatteet
			AND ((cps.Title IS NULL OR cps.Title = '') OR ( cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Barnkläder Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title + ' Sisustus netistä. Lekmer.fi - Lasten- ja vauvanvaatteet netistä.',
			cps.[Description] = 'Osta ' + x.Title + ' ja ' + cn.Title + ' netistä. Saa Lekmer.fi - verkkokaupasta lastenvaatteet toimitettua 3-5 arkipäivässä.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1007276) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Inredning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' Lastenhuone netistä. Lekmer.fi Lastenhuoneen sisustustuotteet netistä.',
			cps.[Description] = 'Osta ' + cn.Title + ' netistä. Saa kaikki lastenhuoneen tarvikkeet toimitettua Lekmer.fi-verkkokaupasta 3-5 arkipäivässä'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND cn.ParentContentNodeId = 1005059 -- Lastenhuone
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Inredning Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = x.Title + ' ' + cn.Title + ' Lastenhuone netistä. Lekmer.fi – Lastenhuoneen tarvikkeet netistä.',
			cps.[Description] = 'Osta ' + x.Title + ' ja ' + cn.Title + ' netistä. Saa lastenhuoneen tarvikkeet toimitettua Lekmer.fi - verkkokaupasta 3-5 arkipäivässä.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1005059) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Underhållning Kategori Överkategori - nivå 2 >
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title+ ' peli ja viihde sekä lastenpelit netistä. Lekmer.fi – Tv-pelit netistä',
			cps.[Description] = 'Osta ' + cn.Title + ' netistä. Saa tv-pelit toimitettua Lekmer.fi-verkkokaupasta 3-5 arkipäivässä.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND cn.ParentContentNodeId = 1005510 -- Pelit & viihde
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))


		------------------------------------------------------------
		-- <Start Underhållning Underkategori - nivå 3>
		------------------------------------------------------------
		UPDATE
			cps
		SET	
			cps.Title = cn.Title + ' ' + x.Title + ' netistä. Lekmer.fi- Lautapelit netistä',
			cps.[Description] = 'Osta ' + cn.Title + ' ja ' + x.Title + ' netistä. Saa pelit toimitettua Lekmer.fi-verkkokaupasta 3-5 arkipäivässä.'
		FROM
			sitestructure.tContentPageSeoSetting cps
			INNER JOIN sitestructure.tContentNode cn ON cn.ContentNodeId = cps.ContentNodeId
			INNER JOIN (SELECT ContentNodeId, Title
						FROM sitestructure.tContentNode
						WHERE ParentContentNodeId = 1005510) x ON cn.ParentContentNodeId = x.ContentNodeId
		WHERE
			cn.SiteStructureRegistryId = 4
			AND ((cps.Title IS NULL OR cps.Title = '') OR (cps.[Description] IS NULL OR cps.[Description] = ''))

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION

		INSERT INTO [integration].[integrationLog] ([Data], [Message], [Date], [OcuredInProcedure])
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH
END
GO
