SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignDelete]
	@CampaignId		INT
AS
BEGIN
	DELETE FROM [campaignlek].[tCampaignRegistryCampaign] WHERE [CampaignId] = @CampaignId
END
GO
