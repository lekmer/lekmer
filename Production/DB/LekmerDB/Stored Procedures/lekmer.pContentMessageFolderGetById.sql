SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderGetById]
	@ContentMessageFolderId	INT
AS
BEGIN
	SELECT
		[cmf].*
	FROM
		[lekmer].[vContentMessageFolder] cmf
	WHERE
		[cmf].[ContentMessageFolder.ContentMessageFolderId] = @ContentMessageFolderId
END
GO
