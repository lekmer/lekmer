SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pOrderVoucherInfoUpdateStatus]
	@OrderId INT,
	@VoucherStatus INT
AS
BEGIN
	SET NOCOUNT ON;
	
	UPDATE	[orderlek].[tOrderVoucherInfo]
	SET 
			[VoucherStatus] = @VoucherStatus
	WHERE	
			[OrderId] = @OrderId
END
GO
