SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pStockRangeGetAll]
AS
BEGIN
	SELECT
		[sr].[StockRangeId] 'StockRange.StockRangeId',
		[sr].[CommonName] 'StockRange.CommonName',
		[sr].[StartValue] 'StockRange.StartValue',
		[sr].[EndValue] 'StockRange.EndValue'
	FROM
		[productlek].[tStockRange] sr
END
GO
