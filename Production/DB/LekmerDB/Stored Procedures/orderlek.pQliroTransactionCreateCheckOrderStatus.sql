SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionCreateCheckOrderStatus]
	@ClientRef VARCHAR(50),
	@TransactionTypeId INT,
	@OrderId INT,
	@ReservationNumber VARCHAR(50),
	@CreatedDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tQliroTransaction]
			( [ClientRef],
			  [TransactionTypeId],
			  [OrderId],
			  [ReservationNumber],
			  [Created]
			)
	VALUES
			( @ClientRef,
			  @TransactionTypeId,			  
			  @OrderId,
			  @ReservationNumber,
			  @CreatedDate
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
