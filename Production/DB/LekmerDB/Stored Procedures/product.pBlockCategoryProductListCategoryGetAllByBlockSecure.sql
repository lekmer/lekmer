SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pBlockCategoryProductListCategoryGetAllByBlockSecure]
	@BlockId int
as
begin
	
	SELECT 
		bc.*
	from 
		[product].[vCustomBlockCategoryProductListCategory] as bc
		inner join [product].[vCustomCategorySecure] as cv on bc.[BlockCategory.CategoryId] = cv.[Category.Id]
	where
		bc.[BlockCategory.BlockId] = @BlockId
end

GO
