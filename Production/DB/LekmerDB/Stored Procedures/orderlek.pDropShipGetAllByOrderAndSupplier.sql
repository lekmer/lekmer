SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pDropShipGetAllByOrderAndSupplier]
	@Orderid	INT,
	@SupplierNo	NVARCHAR(50)
AS
BEGIN
	SELECT
		[ds].*
	FROM
		[orderlek].[vDropShip] ds
	WHERE
		[ds].[DropShip.OrderId] = @OrderId
		AND [ds].[DropShip.SupplierNo] = @SupplierNo
END
GO
