SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pReviewSave]
	@ReviewId INT = NULL,
	@RatingReviewFeedbackId INT,
	@AuthorName NVARCHAR(50),
	@Title NVARCHAR(50),
	@Message NVARCHAR(3000),
	@Email VARCHAR(320)
AS
BEGIN
	SET NOCOUNT ON

	IF (@ReviewId IS NOT NULL)
	BEGIN
		UPDATE 
			[review].[tReview]
		SET 
			[Title] = @Title,
			[Message] = @Message
		WHERE
			[ReviewId] = @ReviewId
	END
	ELSE
	BEGIN
		INSERT INTO [review].[tReview] (
			[RatingReviewFeedbackId],
			[AuthorName],
			[Title],
			[Message],
			[Email]
		)
		VALUES (
			@RatingReviewFeedbackId,
			@AuthorName,
			@Title,
			@Message,
			@Email
		)

		SET @ReviewId = SCOPE_IDENTITY()
	END

	RETURN @ReviewId
END
GO
