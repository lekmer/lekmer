SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionGetProductIds]
	@ActionId INT
AS
BEGIN
	SELECT ProductId FROM [lekmer].[tProductDiscountActionItem]
	WHERE [ProductActionId] = @ActionId
END
GO
