SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [template].[pThemeTemplateSave]
@ThemeId	int,
@ModelId	int,
@TemplateId	int
AS
BEGIN
	IF EXISTS (SELECT 1 FROM template.tThemeModel WHERE ThemeId = @ThemeId AND ModelId = @ModelId)
		BEGIN
			UPDATE template.tThemeModel
			SET TemplateId = @TemplateId
			WHERE ThemeId = @ThemeId AND ModelId = @ModelId
		END
	ELSE
		BEGIN
			INSERT INTO template.tThemeModel
			VALUES (@ThemeId, @ModelId, @TemplateId)
		END
END













GO
