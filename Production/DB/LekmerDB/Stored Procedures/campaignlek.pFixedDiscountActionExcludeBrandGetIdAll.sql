
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionExcludeBrandGetIdAll]
	@ProductActionId INT
AS
BEGIN
	SELECT 
		[BrandId]
	FROM 
		[campaignlek].[tFixedDiscountActionExcludeBrand]
	WHERE 
		[ProductActionId] = @ProductActionId
END
GO
