
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignSave]
	@CampaignId			INT,
	@Title				VARCHAR(500),
	@CampaignStatusId	INT,
	@FolderId			INT,
	@StartDate			DATETIME,
	@EndDate			DATETIME,
	@Exclusive			BIT,
	@Priority			INT,
	@LevelId			INT,
	@UseLandingPage		BIT = 0,
	@PriceTypeId		INT = NULL,
	@TagId				INT = NULL
AS
BEGIN
	UPDATE
		[campaign].[tCampaign]
	SET
		[Title]					= @Title,
		[CampaignStatusId]		= @CampaignStatusId,
		[FolderId]				= @FolderId,
		[StartDate]				= @StartDate,
		[EndDate]				= @EndDate,
		[Exclusive]				= @Exclusive,
		[Priority]				= @Priority,
		[LevelId]				= @LevelId,
		[UseLandingPage]		= @UseLandingPage,
		[PriceTypeId]			= @PriceTypeId,
		[TagId]					= @TagId
	WHERE
		[CampaignId] = @CampaignId
		
	IF  @@ROWCOUNT = 0
	BEGIN
		SET @Priority = (SELECT MAX(PRIORITY) FROM campaign.tCampaign) + 1
		IF @Priority IS NULL
			BEGIN
				SET @Priority = 1
			END
			
		INSERT campaign.tCampaign (
			[Title],
			[CampaignStatusId],
			[FolderId],
			[StartDate],
			[EndDate],
			[Exclusive],
			[Priority],
			[LevelId],
			[UseLandingPage],
			[PriceTypeId],
			[TagId]
		)
		VALUES (
			@Title,
			@CampaignStatusId,
			@FolderId,
			@StartDate,
			@EndDate,
			@Exclusive,
			@Priority,
			@LevelId,
			@UseLandingPage,
			@PriceTypeId,
			@TagId
		)
		
		SET @CampaignId = CAST(SCOPE_IDENTITY() AS INT)
	END
	RETURN @CampaignId
END
GO
