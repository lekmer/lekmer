
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDecreaseNumberInStock]
	@ProductId			INT,
	@Quantity			INT
AS 
BEGIN
	SET NOCOUNT ON

	-- Update Product NumberInSock
	UPDATE
		[product].[tProduct]
	SET	
		[NumberInStock] = (CASE WHEN [NumberInStock] - @Quantity < 0 THEN 0
							    ELSE [NumberInStock] - @Quantity
						   END)
	WHERE
		[ProductId] = @ProductId
		
	-- Update NumberInStock of Packages that contain current product
	EXEC [productlek].[pPackageDecreaseNumberInStockByProduct] @ProductId
END
GO
