SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pFreightValueActionDeliveryMethodGetIdAll]
	@ActionId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT 
		[DeliveryMethodId]
	FROM 
		[campaignlek].[tFreightValueActionDeliveryMethod]
	WHERE 
		[CartActionId] = @ActionId
END
GO
