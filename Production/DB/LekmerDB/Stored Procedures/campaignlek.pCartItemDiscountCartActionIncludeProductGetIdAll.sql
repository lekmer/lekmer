
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemDiscountCartActionIncludeProductGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tCartItemDiscountCartActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [aip].[ProductId]
	WHERE
		[aip].[CartActionId] = @CartActionId
		AND [p].[IsDeleted] = 0
END
GO
