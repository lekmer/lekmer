SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pCategoryGetTree]
@SelectedId	int
AS
BEGIN
	DECLARE @tTree TABLE (Id int, ParentId int, Title nvarchar(max))
	INSERT INTO @tTree
		SELECT
			v.[Category.Id],
			v.[Category.ParentCategoryId],
			v.[Category.Title]
		FROM product.vCustomCategorySecure AS v
		WHERE v.[Category.ParentCategoryId] IS NULL

	IF (@SelectedId IS NOT NULL)
		BEGIN
			INSERT INTO @tTree
				SELECT
					v.[Category.Id],
					v.[Category.ParentCategoryId],
					v.[Category.Title]
				FROM product.vCustomCategorySecure AS v
				WHERE 
					v.[Category.ParentCategoryId] = @SelectedId

			DECLARE @ParentId int
			WHILE (@SelectedId IS NOT NULL)
				BEGIN
					SET @ParentId = (SELECT v.[Category.ParentCategoryId]
									 FROM product.vCustomCategorySecure AS v
									 WHERE v.[Category.Id] = @SelectedId)		 
					INSERT INTO @tTree
						SELECT 
							v.[Category.Id],
							v.[Category.ParentCategoryId],
							v.[Category.Title]
						FROM product.vCustomCategorySecure AS v
						WHERE v.[Category.ParentCategoryId] = @ParentId
					SET @SelectedId = @ParentId
				END
		END

		SELECT 
			Id,	ParentId, Title,
			CAST((CASE WHEN EXISTS (SELECT 1 FROM product.vCustomCategorySecure AS v
									WHERE v.[Category.ParentCategoryId] = Id)
			THEN 1 ELSE 0 END) AS bit) AS 'HasChildren'
		FROM @tTree
		ORDER BY Title ASC
END


GO
