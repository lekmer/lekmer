
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaSave]
	@SizeTableMediaId INT,
	@SizeTableId INT,
	@MediaId INT
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE 
		[lekmer].[tSizeTableMedia]
	SET 
		[SizeTableId] = @SizeTableId,
		[MediaId] = @MediaId
	WHERE
		[SizeTableMediaId] = @SizeTableMediaId
				
	IF  @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [lekmer].[tSizeTableMedia](
			[SizeTableId],
			[MediaId]
		)
		VALUES (
			@SizeTableId,
			@MediaId
		)

		SET @SizeTableMediaId = SCOPE_IDENTITY()
	END

	RETURN @SizeTableMediaId
END
GO
