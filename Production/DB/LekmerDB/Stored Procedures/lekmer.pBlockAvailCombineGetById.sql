
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockAvailCombineGetById]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON
	SELECT
		*
	FROM 
		[lekmer].[tBlockAvailCombine] bac
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bac].[BlockId] = [b].[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE 
		[bac].[BlockId] = @BlockId
END
GO
