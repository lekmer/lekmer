SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pRecommenderListSave]
	@RecommenderListId	INT,
	@SessionId	VARCHAR(50)
AS 
BEGIN
	SET NOCOUNT ON

	-- update session id if exists
	UPDATE [productlek].[tRecommenderList]
	SET 
		[SessionId] = @SessionId
	WHERE 
		[RecommenderListId] = @RecommenderListId

	IF @@ROWCOUNT <> 0
		RETURN @RecommenderListId


	-- find list id if session id exists
	SELECT
		@RecommenderListId = [RecommenderListId]
	FROM
		[productlek].[tRecommenderList]
	WHERE 
		[SessionId] = @SessionId

	IF @@ROWCOUNT <> 0
		RETURN @RecommenderListId

	
	-- find list id if session id is null
	IF @SessionId IS NULL
	BEGIN
		SELECT
			@RecommenderListId = [RecommenderListId]
		FROM
			[productlek].[tRecommenderList]
		WHERE 
			[SessionId] IS NULL

		IF @@ROWCOUNT <> 0
			RETURN @RecommenderListId
	END


	-- insert new session id
	INSERT [productlek].[tRecommenderList] (
		[SessionId],
		[CreatedDate]
	)
	VALUES (
		@SessionId,
		GETDATE()
	)

	SET @RecommenderListId = SCOPE_IDENTITY()

	RETURN @RecommenderListId

END 
GO
