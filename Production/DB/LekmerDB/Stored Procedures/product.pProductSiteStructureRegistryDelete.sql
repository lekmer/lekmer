SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [product].[pProductSiteStructureRegistryDelete]
	@ContentNodeId	INT
AS
BEGIN
	SET NOCOUNT ON
	DECLARE @tContentNode TABLE (tContentNodeId INT, tParentContentNodeId INT);

	WITH ContentNode (ContentNodeId, ParentContentNodeId) AS 
	(
		SELECT ContentNodeId, ParentContentNodeId
		FROM  sitestructure.tContentNode
		WHERE ParentContentNodeId = @ContentNodeId

		UNION ALL

		SELECT C.ContentNodeId, C.ParentContentNodeId
		FROM sitestructure.tContentNode C 
		INNER JOIN ContentNode OuterC ON OuterC.ContentNodeId = C.ParentContentNodeId
	)
	INSERT INTO @tContentNode (tContentNodeId, tParentContentNodeId)
	SELECT ContentNodeId, ParentContentNodeId FROM ContentNode
		
	UNION ALL	

	SELECT ContentNodeId, ParentContentNodeId
	FROM  sitestructure.tContentNode
	WHERE ContentNodeId = @ContentNodeId

	UPDATE product.tProductSiteStructureRegistry
	SET ParentContentNodeId = NULL
	FROM product.tProductSiteStructureRegistry p
	INNER JOIN @tContentNode c ON c.tContentNodeId = p.ParentContentNodeId
	
	UPDATE product.tProductSiteStructureRegistry
	SET TemplateContentNodeId = NULL
	FROM product.tProductSiteStructureRegistry p
	INNER JOIN @tContentNode c ON c.tContentNodeId = p.TemplateContentNodeId
END

GO
