SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
* *****************  Version 2  *****************
User: Victor E.		Date: 21.04.2010		Time: 18:40
Description:
			deleted field "Ordinal" 
*****************  Version 1  *****************
User: Victor E.		Date: 20.11.2008		Time: 13:40
Description:
			Created 
*/
Create PROCEDURE [customer].[pCustomerRegistryGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerRegistry]
END


GO
