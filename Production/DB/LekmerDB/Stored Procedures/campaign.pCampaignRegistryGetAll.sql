SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignRegistryGetAll]
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCampaignRegistry
end

GO
