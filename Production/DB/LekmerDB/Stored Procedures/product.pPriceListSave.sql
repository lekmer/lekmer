SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [product].[pPriceListSave]
	@PriceListId		int,
	@StartDateTime		datetime,
	@EndDateTime		datetime,
	@PriceListFolderId	int,
	@PriceListStatusId	int
AS
BEGIN
	UPDATE 
		product.tPriceList
	SET 
		StartDateTime = @StartDateTime,
		EndDateTime = @EndDateTime,
		PriceListFolderId = @PriceListFolderId,
		PriceListStatusId = @PriceListStatusId
	WHERE 
		PriceListId = @PriceListId
END










GO
