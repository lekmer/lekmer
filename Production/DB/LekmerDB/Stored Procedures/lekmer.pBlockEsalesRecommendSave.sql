
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockEsalesRecommendSave]
	@BlockId INT,
	@RecommendationType INT,
	@PanelPath NVARCHAR(MAX),
	@FallbackPanelPath NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tBlockEsalesRecommend]
	SET
		[EsalesRecommendationTypeId] = @RecommendationType,
		[PanelPath]		= @PanelPath,
		[FallbackPanelPath] = @FallbackPanelPath
	WHERE
		[BlockId] = @BlockId
		
	IF  @@ROWCOUNT <> 0
		RETURN
		
	INSERT [lekmer].[tBlockEsalesRecommend]
	(
		[BlockId],
		[EsalesRecommendationTypeId],
		[PanelPath],
		[FallbackPanelPath]		
	)
	VALUES
	(
		@BlockId,
		@RecommendationType,
		@PanelPath,
		@FallbackPanelPath
	)
END
GO
