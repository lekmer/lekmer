SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [lekmer].[pBlockProductFilterSaveBrand]
	@BlockId int,
	@BrandId int
as
begin
	insert
		lekmer.tBlockProductFilterBrand
	(
		BlockId,
		BrandId
	)
	values
	(
		@BlockId,
		@BrandId
	)
end
GO
