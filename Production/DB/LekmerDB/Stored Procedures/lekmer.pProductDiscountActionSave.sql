
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductDiscountActionSave]
	@ActionId	int
AS 
BEGIN
	IF (NOT EXISTS (SELECT * FROM lekmer.tProductDiscountAction WHERE ProductActionId = @ActionId))
	BEGIN
		INSERT 
			lekmer.tProductDiscountAction
		(
			ProductActionId
		)
		VALUES
		(
			@ActionId
		)
	END
END
GO
