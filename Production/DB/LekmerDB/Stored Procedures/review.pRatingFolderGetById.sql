SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingFolderGetById]
	@RatingFolderId	INT
AS
BEGIN
	SELECT
		rf.*
	FROM
		[review].[vRatingFolder] rf
	WHERE
		rf.[RatingFolder.RatingFolderId] = @RatingFolderId
END
GO
