
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeCategoryGetIdAll]
	@CartActionId INT
AS
BEGIN
	SELECT 
		[CategoryId],
		[IncludeSubcategories]
	FROM 
		[lekmer].[tCartItemPercentageDiscountActionExcludeCategory]
	WHERE 
		[CartActionId] = @CartActionId
END
GO
