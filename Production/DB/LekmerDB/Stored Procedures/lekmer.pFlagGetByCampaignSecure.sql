SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pFlagGetByCampaignSecure]
@CampaignId int
AS
BEGIN
	SET NOCOUNT ON
	SELECT f.*
	FROM lekmer.vFlagSecure f inner join lekmer.tCampaignFlag cf on f.[Flag.Id] = cf.FlagId
	WHERE cf.CampaignId = @CampaignId
END

GO
