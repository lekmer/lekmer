SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeCategoryInsert]
	@ProductActionId INT,
	@CategoryId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionIncludeCategory] (
		ProductActionId,
		CategoryId
	)
	VALUES (
		@ProductActionId,
		@CategoryId
	)
END
GO
