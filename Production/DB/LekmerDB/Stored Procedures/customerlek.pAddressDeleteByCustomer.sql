SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [customerlek].[pAddressDeleteByCustomer]
	@CustomerId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE [customerlek].[tAddress]
	WHERE [CustomerId] = @CustomerId
	
	DELETE [customer].[tAddress]
	WHERE [CustomerId] = @CustomerId
END
GO
