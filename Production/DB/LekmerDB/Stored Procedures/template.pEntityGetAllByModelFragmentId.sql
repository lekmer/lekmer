SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pEntityGetAllByModelFragmentId]
	@ModelFragmentId	int
AS
begin
	set nocount on
	select
		E.*
	from
		[template].[vCustomEntity] E
		INNER JOIN [template].tModelFragmentEntity M ON M.EntityId = E.[Entity.Id]
	where
		M.ModelFragmentId = @ModelFragmentId
	ORDER BY E.[Entity.Title]
end

GO
