
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeCategoryGetIdAllRecursive]
	@ConfigId INT
AS
BEGIN
	DECLARE @tCategoryIteration TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryIteration (CategoryId, IncludeSubCategories)
	SELECT [CategoryId], [IncludeSubCategories]
	FROM [campaignlek].[tCampaignActionIncludeCategory]
	WHERE [ConfigId] = @ConfigId

	DECLARE @tCategoryResult TABLE (CategoryId INT, IncludeSubCategories BIT)
	INSERT INTO @tCategoryResult (CategoryId, IncludeSubCategories)
	SELECT CategoryId, IncludeSubCategories FROM @tCategoryIteration
	
	DECLARE @CategoryId INT, @IncludeSubCategories BIT

	WHILE ((SELECT COUNT(*) FROM @tCategoryIteration) > 0)
	BEGIN
		SET @CategoryId = (SELECT TOP 1 CategoryId FROM @tCategoryIteration)
		SET @IncludeSubCategories = (SELECT TOP 1 IncludeSubCategories FROM @tCategoryIteration)
		
		IF (@IncludeSubCategories = 1)
		BEGIN
			WITH Category (CategoryId, ParentCategoryId) AS 
			(
				SELECT [CategoryId], [ParentCategoryId]
				FROM  [product].[tCategory]
				WHERE [ParentCategoryId] = @CategoryId
				UNION ALL
				SELECT [c].[CategoryId], [c].[ParentCategoryId]
				FROM [product].[tCategory] c
				JOIN Category OuterC ON [OuterC].[CategoryId] = [c].[ParentCategoryId]
			)
			INSERT INTO @tCategoryResult ([CategoryId], [IncludeSubCategories])
			SELECT CategoryId, 0 FROM Category		
		END
		
		DELETE @tCategoryIteration WHERE [CategoryId] = @CategoryId
	END

	SELECT * FROM @tCategoryResult
END
GO
