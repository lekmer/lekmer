SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pKlarnaTransactionCreateCancelReservation]
	@KlarnaShopId INT,
	@KlarnaMode INT,
	@KlarnaTransactionTypeId INT,
	@CreatedDate DATETIME,
	@OrderId INT,
	@ReservationNumber VARCHAR(50)
AS 
BEGIN
	SET NOCOUNT ON
	
	INSERT	INTO [orderlek].[tKlarnaTransaction]
			( [KlarnaShopId],
			  [KlarnaMode],
			  [KlarnaTransactionTypeId],
			  [Created],
			  [OrderId],
			  [ReservationNumber]
			)
	VALUES
			( @KlarnaShopId,
			  @KlarnaMode,
			  @KlarnaTransactionTypeId,
			  @CreatedDate,
			  @OrderId,
			  @ReservationNumber
			)
			
	RETURN SCOPE_IDENTITY()
END
GO
