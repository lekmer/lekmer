SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageFolderGetAllByParent]
	@ParentId	INT
AS
BEGIN
	SELECT
		[cmf].*
	FROM
		[lekmer].[vContentMessageFolder] cmf
	WHERE
		([cmf].[ContentMessageFolder.ParentContentMessageFolderId] = @ParentId AND @ParentId IS NOT NULL)
		OR
		([cmf].[ContentMessageFolder.ParentContentMessageFolderId] IS NULL AND @ParentId IS NULL)
END
GO
