SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE PROCEDURE [lekmer].[pBlockBrandGetAllByBlockInvertedSecure]
	@BlockId int, 
	@ChannelId int
as
begin
	
	SELECT 
	  *
	from 
		[lekmer].[vBrand] b 
	where
		b.[Brand.BrandId] NOT in (Select BrandId From [lekmer].[tBlockBrandProductListBrand] where BlockId = @BlockId) 
		and ChannelId = @ChannelId
		
end




GO
