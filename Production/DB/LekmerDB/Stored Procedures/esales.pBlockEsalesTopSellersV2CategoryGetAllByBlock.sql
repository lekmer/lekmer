SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesTopSellersV2CategoryGetAllByBlock]
	@BlockId	INT,
	@LanguageId INT	
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT
		*
	FROM
		[esales].[vBlockEsalesTopSellersV2Category]
	WHERE
		[BlockEsalesTopSellersV2Category.BlockId] = @BlockId
		AND [Category.LanguageId] = @LanguageId
END
GO
