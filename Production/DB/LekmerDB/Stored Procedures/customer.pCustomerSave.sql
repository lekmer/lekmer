SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


Create PROCEDURE [customer].[pCustomerSave]
@CustomerId			INT,
@CustomerStatusId	INT,
@ERPId				VARCHAR(50),
@CustomerRegistryId	INT
AS	
BEGIN
	SET NOCOUNT ON	
	
		UPDATE  [customer].[tCustomer]
		SET 
				[CustomerStatusId] = @CustomerStatusId,
				[ErpId] = @ErpId,
				[CustomerRegistryId]= @CustomerRegistryId
		WHERE
				CustomerId = @CustomerId
				
	IF @@ROWCOUNT = 0 
	BEGIN		
		INSERT INTO	[customer].[tCustomer]
			(
				[CustomerStatusId],
				[ErpId],
				[CustomerRegistryId]
			)
		VALUES
			(
				@CustomerStatusId,
				@ErpId,
				@CustomerRegistryId
			)		
			SET	@CustomerId = SCOPE_IDENTITY()
	END
	RETURN  @CustomerId	
END

GO
