SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignStatusGetAll]
as
begin
	SELECT
		*
	FROM
		campaign.vCustomCampaignStatus
	ORDER BY
		[CampaignStatus.Title] ASC
end

GO
