
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [export].[pOrderInfoGetForTwosell]
	@DaysAfterPurchase	INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @CurrentDate DATETIME, @DateFrom DATETIME
	SET @CurrentDate = CONVERT (DATE, GETDATE())
	SET @DateFrom = (SELECT DATEADD(DAY, @DaysAfterPurchase * -1, @CurrentDate))

	DECLARE @OrderInfo TABLE (
		OrderDate DATETIME,
		OrderId INT,
		ProductId INT,
		ProductErpId VARCHAR(50),
		Quantity INT,
		Price DECIMAL(16,2),
		IP VARCHAR(15),
		Email VARCHAR(320),
		LekmerId VARCHAR(50)
	)
	INSERT INTO @OrderInfo
	SELECT
		[o].[CreatedDate],
		[o].[OrderId],
		[p].[ProductId],
		[p].[ErpId],
		[oi].[Quantity],
		[oi].[ActualPriceIncludingVat],
		[o].[IP],
		[o].[Email],
		[lo].[CustomerIdentificationKey]
	FROM [order].[tOrder] o
		INNER JOIN [lekmer].[tLekmerOrder] lo ON [lo].[OrderId] = [o].[OrderId]
		INNER JOIN [order].[tOrderItem] oi ON [oi].[OrderId] = [o].[OrderId]
		INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[OrderItemId] = [oi].[OrderItemId]
		INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [oip].[ProductId]
	WHERE
		[o].[CreatedDate] >= @DateFrom
		AND [o].[CreatedDate] < @CurrentDate
		AND [o].[OrderStatusId] = 4

	IF (SELECT COUNT(1) FROM @OrderInfo) > 0
		BEGIN
			;WITH tempRepeat AS (
				SELECT TOP (SELECT MAX([t].[Quantity]) FROM @OrderInfo t) RowNumber = ROW_NUMBER()
				OVER (ORDER BY [object_id])
				FROM sys.all_columns 
				ORDER BY [object_id]
			)
			SELECT 
				* 
			FROM tempRepeat tr
				CROSS JOIN @OrderInfo AS oi
			WHERE [tr].[RowNumber] <= [oi].[Quantity]
			ORDER BY [oi].[OrderDate], [oi].[ProductId];
		END
	ELSE
		SELECT * FROM @OrderInfo
END
GO
