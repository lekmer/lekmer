SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pCollectorPaymentTypeGetAllByChannel]
	@ChannelId int
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		*
	FROM 
		[orderlek].[vCollectorPaymentType]
	WHERE
		[CollectorPaymentType.ChannelId] = @ChannelId
	ORDER BY
		[CollectorPaymentType.Ordinal]
END
GO
