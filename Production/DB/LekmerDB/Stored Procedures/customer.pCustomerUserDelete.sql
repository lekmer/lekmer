SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [customer].[pCustomerUserDelete]
	@CustomerId int
as	
begin
	set nocount on	
	delete	[customer].[tCustomerUser]	
	WHERE   CustomerId = @CustomerId
end

GO
