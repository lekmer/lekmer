SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportRelationListsWithOutOfStockProducts]
AS
BEGIN
	DECLARE @tRelationListProduct TABLE (
			RelationListId INT,
			RelationListTitle NVARCHAR(256),
			RelationListCommonName VARCHAR(50),
			ProductId INT,
			ProductErpId VARCHAR(50),
			ProductTitle NVARCHAR(256))

	INSERT INTO @tRelationListProduct (
		[RelationListId],
		[RelationListTitle],
		[RelationListCommonName],
		[ProductId],
		[ProductErpId],
		[ProductTitle])
	SELECT
		[rl].[RelationListId] 'RelationListId',
		[rl].[Title] 'RelationListTitle',
		[rlt].[CommonName] 'RelationListCommonName',
		[rlp].[ProductId] 'ProductId',
		[p].[ErpId],
		[p].[Title]
	FROM [product].[tRelationList] rl
		 INNER JOIN [product].[tRelationListType] rlt ON [rlt].[RelationListTypeId] = [rl].[RelationListTypeId]
		 INNER JOIN [product].[tRelationListProduct] rlp ON [rlp].[RelationListId] = [rl].[RelationListId]
		 INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [rlp].[ProductId]
		 CROSS APPLY (
				SELECT SUM(ps.[NumberInStock]) AS 'NumberInStock' FROM [lekmer].[tProductSize] ps WHERE ps.[ProductId] = p.[ProductId]
			) s
	WHERE
		(([s].[NumberInStock] IS NULL AND [p].[NumberInStock] < 1)
		OR
		([s].[NumberInStock] IS NOT NULL AND [s].[NumberInStock] < 1))
		AND 
		[rl].[RelationListTypeId] <> 1000001
	ORDER BY
		[rl].[RelationListId]


	SELECT * FROM @tRelationListProduct
END
GO
