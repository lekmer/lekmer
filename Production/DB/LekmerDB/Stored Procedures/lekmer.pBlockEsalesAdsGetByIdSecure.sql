SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockEsalesAdsGetByIdSecure]
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vBlockEsalesAdsSecure] AS bea
	WHERE
		bea.[Block.BlockId] = @BlockId
END
GO
