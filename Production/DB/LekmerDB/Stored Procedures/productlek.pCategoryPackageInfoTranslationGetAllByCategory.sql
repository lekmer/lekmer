
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pCategoryPackageInfoTranslationGetAllByCategory]
	@CategoryId int
AS
BEGIN
	SET NOCOUNT ON;
	SELECT
	    [CategoryId] AS 'Id',
		[LanguageId] AS 'LanguageId',
		[PackageInfo] AS 'Value'
	FROM
	    [productlek].[tLekmerCategoryTranslation] 
	WHERE
		[CategoryId] = @CategoryId
	ORDER BY
		[LanguageId]
END
GO
