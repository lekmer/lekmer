SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
* *****************  Version 2  *****************
User: Roman Dovhun.		Date: 02.03.2009		Time: 17.55
Description:
			deleted field "Ordinal" 
*****************  Version 1  *****************
User: Victor E.		Date: 20.11.2008		Time: 13:40
Description:
			Created 
*/
CREATE PROCEDURE [customer].[pCustomerStatusGetAll]
AS
BEGIN
	SELECT
		*
	FROM
		[customer].[vCustomCustomerStatus]
END

GO
