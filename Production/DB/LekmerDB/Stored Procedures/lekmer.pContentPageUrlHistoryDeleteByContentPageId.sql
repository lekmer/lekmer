SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pContentPageUrlHistoryDeleteByContentPageId]
	@ContentPageId INT
AS
BEGIN
	DELETE [lekmer].[tContentPageUrlHistory]
	WHERE ContentPageId = @ContentPageId
END
GO
