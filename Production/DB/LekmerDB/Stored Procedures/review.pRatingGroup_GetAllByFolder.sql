SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingGroup_GetAllByFolder]
	@RatingGroupFolderId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		rg.*
	FROM
		[review].[vRatingGroup] rg
	WHERE
		rg.[RatingGroup.RatingGroupFolderId] = @RatingGroupFolderId
END
GO
