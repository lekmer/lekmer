SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [customer].[pUserSave]
	@CustomerId int,
	@CustomerRegistryId int,
	@UserName nvarchar(50),
	@Password nvarchar(50),
	@PasswordSalt nvarchar(50),
	@CreatedDate datetime
as	
begin
	set nocount on	
	UPDATE
		[customer].[tCustomerUser]
	SET    
		UserName = @UserName,
		CustomerRegistryId = @CustomerRegistryId, 
		[Password] = @Password,
		PasswordSalt = @PasswordSalt
	WHERE 
		CustomerId = @CustomerId
		
	IF @@ROWCOUNT = 0	
	BEGIN
		INSERT INTO	[customer].[tCustomerUser]
		(
			CustomerId,
			CustomerRegistryId,
			UserName,
			[Password],
			PasswordSalt,
			CreatedDate
		)
		VALUES
		(
			@CustomerId,
			@CustomerRegistryId,
			@UserName,
			@Password,
			@PasswordSalt,
			@CreatedDate
		)
	END
	RETURN @CustomerId
end

GO
