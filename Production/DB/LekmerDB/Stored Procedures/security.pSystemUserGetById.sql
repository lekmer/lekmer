SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [security].[pSystemUserGetById]
	@SystemUserId	int
as
begin
	set nocount on

	select
		*
	from
		security.[vCustomSystemUser]
	where
		[SystemUser.Id] = @SystemUserId
end

GO
