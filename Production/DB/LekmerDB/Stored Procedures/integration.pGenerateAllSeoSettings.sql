
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pGenerateAllSeoSettings]
AS
BEGIN
	SET NOCOUNT ON
	BEGIN TRY
		
		-- SE Seo
		EXEC [integration].[pGenerateProductSeoSE]
		EXEC [integration].[pGenerateSeoContentPageSettingsSE]
		
		-- NO Seo
		EXEC [integration].[pGenerateProductSeoNO]
		EXEC [integration].[pGenerateSeoContentPageSettingsNO]
		
		-- DK Seo
		EXEC [integration].[pGenerateProductSeoDK]
		EXEC [integration].[pGenerateSeoContentPageSettingsDK]
		
		-- FI Seo
		EXEC [integration].[pGenerateProductSeoFI]
		EXEC [integration].[pGenerateSeoContentPageSettingsFI]
		
		-- NL Seo
		EXEC [integration].[pGenerateProductSeoNL]
		EXEC [integration].[pGenerateSeoContentPageSettingsNL]

	END TRY
	BEGIN CATCH
		-- If transaction is active, roll it back.
		IF @@TRANCOUNT > 0 ROLLBACK TRANSACTION
		
		INSERT INTO [integration].[integrationLog] ([Data], [Message], [Date], [OcuredInProcedure])
		VALUES ('', ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
	END CATCH		 
END
GO
