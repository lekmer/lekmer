SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [corelek].[pSubDomainGetAllByChannelAndContentType]
	@ChannelId		INT,
	@ContentTypeId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[corelek].[vSubDomain] sd
		INNER JOIN [corelek].[tSubDomainChannel] sdc ON sdc.[SubDomainId] = sd.[SubDomain.SubDomainId]
	WHERE
		[sdc].[ChannelId] = @ChannelId
		AND
		[sd].[SubDomain.ContentTypeId] = @ContentTypeId
END
GO
