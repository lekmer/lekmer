SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [campaign].[pCartValueConditionSave]
	@ConditionId int
AS 
BEGIN
		IF EXISTS (SELECT 1 FROM campaign.tCartValueCondition WHERE ConditionId = @ConditionId)
			RETURN
		
		INSERT campaign.tCartValueCondition
		( 
			ConditionId
		)
		VALUES 
		(
			@ConditionId
		)
END
GO
