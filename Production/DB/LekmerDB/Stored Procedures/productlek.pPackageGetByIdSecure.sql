SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageGetByIdSecure]
	@PackageId	INT,
	@ChannelId	INT
AS
BEGIN
	SET NOCOUNT ON;	
	
	DECLARE @ProductId INT, @CurrencyId INT, @PriceListRegistryId INT, @PriceListId INT
	
	SELECT @ProductId = [MasterProductId] FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId
	
	SELECT @CurrencyId = [CurrencyId], @PriceListRegistryId = [PriceListRegistryId]
	FROM [product].[fnGetCurrencyAndPriceListRegistry](@ProductId, @ChannelId)
	
	SET @PriceListId = (SELECT product.fnGetPriceListIdOfItemWithLowestPrice(@CurrencyId, @ProductId, @PriceListRegistryId, NULL))
	
	SELECT
		*
	FROM
		[productlek].[vPackageSecure] p 
		LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[Package.MasterProductId]
														  AND [pli].[Price.PriceListId] = @PriceListId
	WHERE
		[p].[Package.PackageId] = @PackageId
END
GO
