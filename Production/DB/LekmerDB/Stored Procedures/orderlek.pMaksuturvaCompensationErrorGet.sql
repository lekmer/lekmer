SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pMaksuturvaCompensationErrorGet]
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[MaksuturvaCompensationErrorId]	'MaksuturvaCompensationError.MaksuturvaCompensationErrorId',
		[ChannelCommonName] 'MaksuturvaCompensationError.ChannelCommonName',
		[FetchDate] 'MaksuturvaCompensationError.FetchDate',
		[LastAttempt] 'MaksuturvaCompensationError.LastAttempt'
	FROM
		[orderlek].[tMaksuturvaCompensationError]
	ORDER BY
		[MaksuturvaCompensationErrorId] ASC
END
GO
