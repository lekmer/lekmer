SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingGroupCategoryDelete]
	@RatingGroupId	INT,
	@CategoryId		INT
AS
BEGIN
	SET NOCOUNT ON

	IF (@RatingGroupId IS NULL AND @CategoryId IS NULL)
	RETURN

	DELETE FROM [review].[tRatingGroupCategory]
	WHERE
		([RatingGroupId] = @RatingGroupId OR @RatingGroupId IS NULL)
		AND
		([CategoryId] = @CategoryId OR @CategoryId IS NULL)
END
GO
