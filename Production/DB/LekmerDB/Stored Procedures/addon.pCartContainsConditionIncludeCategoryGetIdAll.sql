
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [addon].[pCartContainsConditionIncludeCategoryGetIdAll]
	@ConditionId INT
AS
BEGIN
	SELECT 
		[cic].[CategoryId],
		[cic].[IncludeSubcategories]
	FROM 
		[addon].[tCartContainsConditionIncludeCategory] cic
	WHERE 
		[cic].[ConditionId] = @ConditionId
END
GO
