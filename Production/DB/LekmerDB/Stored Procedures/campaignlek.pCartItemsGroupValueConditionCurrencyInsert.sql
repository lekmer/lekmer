SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemsGroupValueConditionCurrencyInsert]
	@ConditionId	INT,
	@CurrencyId		INT,
	@Value			DECIMAL(16,2)
AS
BEGIN
	INSERT [campaignlek].[tCartItemsGroupValueConditionCurrency] (
		ConditionId,
		CurrencyId,
		Value
	)
	VALUES (
		@ConditionId,
		@CurrencyId,
		@Value
	)
END
GO
