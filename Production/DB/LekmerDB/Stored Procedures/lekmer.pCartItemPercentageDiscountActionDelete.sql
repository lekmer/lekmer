SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionDelete]
	@CartActionId INT
AS 
BEGIN
	DELETE FROM [lekmer].[tCartItemPercentageDiscountAction]
	WHERE CartActionId = @CartActionId
END

GO
