SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartItemOptionDelete]
	@CartItemOptionId INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM 
		[campaignlek].[tCartItemOption]
	WHERE
		CartItemOptionId = @CartItemOptionId
END
GO
