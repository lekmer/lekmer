SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowDelete]
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	EXEC [lekmer].[pSizeTableRowTranslationDeleteBySizeTable] @SizeTableId

	DELETE FROM [lekmer].[tSizeTableRow]
	WHERE [SizeTableId] = @SizeTableId
END
GO
