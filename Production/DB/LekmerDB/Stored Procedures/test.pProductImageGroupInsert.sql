SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE procedure [test].[pProductImageGroupInsert]
as
begin
	declare @id int
	set @id = null

	select
		@id = ProductImageGroupId
	from
		product.tProductImageGroup
	where
		Title = 'Test'

	if (@id is not null)
	begin
		return @id
	end

	insert product.tProductImageGroup
	(
		Title,
		CommonName,
		MaxCount
	)
	values
	(
		'Test',
		'Test',
		3
	)
	return scope_identity()
end
GO
