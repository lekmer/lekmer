SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yura P.	Date: 21.11.2008	Time: 12:00
Description:	Created
*/

CREATE PROCEDURE [sitestructure].[pBlockStatusGetById]
	@BlockStatusId	int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomBlockStatus]
	where
		[BlockStatus.BlockStatusId] = @BlockStatusId
end

GO
