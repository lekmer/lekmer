SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCustomerGroupConditionIncludeDeleteAll]
	@ConditionId INT
AS 
BEGIN 
	DELETE
		[campaignlek].[tCustomerGroupConditionInclude]
	WHERE
		ConditionId = @ConditionId
END
GO
