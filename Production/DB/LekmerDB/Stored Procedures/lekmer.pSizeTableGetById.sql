
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableGetById]
	@LanguageId		INT,
	@SizeTableId	INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vSizeTable]
	WHERE
		[SizeTable.SizeTableId] = @SizeTableId
		AND [SizeTable.LanguageId] = @LanguageId
		
	SELECT
		*
	FROM
		[lekmer].[vSizeTableRow]
	WHERE
		[SizeTableRow.SizeTableId] = @SizeTableId
		AND [SizeTableRow.LanguageId] = @LanguageId
	ORDER BY [SizeTableRow.Ordinal]
END
GO
