
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageDelete]
	@PackageId	INT
AS
BEGIN
	DECLARE @MasterProductId INT 
	SET @MasterProductId = (SELECT [MasterProductId] FROM [productlek].[tPackage] WHERE [PackageId] = @PackageId)
	
	UPDATE [product].[tProduct]
	SET [IsDeleted] = 1
	WHERE [ProductId] = @MasterProductId
	
	DELETE FROM [productlek].[tPackageTranslation]
	WHERE [PackageId] = @PackageId
END
GO
