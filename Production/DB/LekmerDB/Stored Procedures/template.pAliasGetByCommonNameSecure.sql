
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Victor E.		Date: 19.09.2008		Time: 11:20
Description:
			Created 
*****************  Version 2  *****************
User: Victor E.		Date: 23.01.2009
Description: Add Path
*****************  Version 3  *****************
User: Yuriy P.		Date: 26.01.2009
Description: Remove Path
*****************  Version 4  *****************
User: Yuriy P.		Date: 19.04.2013
Description: use [vCustomAliasSecure] instead [vCustomAlias]
*/
CREATE PROCEDURE [template].[pAliasGetByCommonNameSecure]
	@CommonName	varchar(100)
AS
begin
	set nocount on

	select
		*
	from
		[template].[vCustomAliasSecure]
	where
		[Alias.CommonName] = @CommonName
end

GO
