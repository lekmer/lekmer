SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockCategoryProductListGetById]
	@LanguageId	INT,
	@BlockId	INT
AS
BEGIN
	SELECT
		[bcpl].*,
		[b].*
	FROM 
		[productlek].[vBlockCategoryProductList] bcpl
		INNER JOIN [sitestructure].[vCustomBlock] b ON [bcpl].[BlockCategoryProductList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bcpl].[BlockCategoryProductList.BlockId] = @BlockId 
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
