
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockPackageProductListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		[b].*,
		[bs].*
	FROM 
		[sitestructure].[vCustomBlock] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
		AND [b].[Block.LanguageId] = @LanguageId
END
GO
