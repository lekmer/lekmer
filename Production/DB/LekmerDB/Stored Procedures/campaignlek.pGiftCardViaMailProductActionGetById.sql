SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [campaignlek].[pGiftCardViaMailProductActionGetById]
	@ProductActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[campaignlek].[vGiftCardViaMailProductAction]
	WHERE
		[GiftCardViaMailProductAction.ProductActionId] = @ProductActionId
END

GO
