SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pBlockProductLatestFeedbackListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		bplfl.*,
		b.*
	FROM 
		[review].[vBlockProductLatestFeedbackList] AS bplfl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON bplfl.[BlockProductLatestFeedbackList.BlockId] = b.[Block.BlockId]
	WHERE
		bplfl.[BlockProductLatestFeedbackList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
