SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[usp_ImportUpdateProductSizesHeppo]

AS
BEGIN
		--raiserror('errorMSG', 16, 1)
		
		-- tProductSize update
		--print 'Start update product'
		UPDATE 
			ps
		SET 
			ps.NumberInStock = tp.NoInStock
			--ProductStatusId = case when tp.NoInStock > 0 then 0 else 1 end
		FROM
			integration.tempProduct tp
			inner join lekmer.tProductSize ps
				on ps.ErpId = substring(tp.HYarticleId, 5,17)
			/* 
			[integration].tempProduct tp
			inner join [lekmer].tLekmerProduct lp
					on lp.HYErpId = substring(tp.HYarticleId, 5,11)	-- 5,17 innan
			inner join product.tProduct p
					on p.ProductId = lp.ProductId
			inner join [product].tProductRegistryProduct pli
					on pli.ProductId = lp.ProductId and
					pli.ProductRegistryId = substring(tp.HYarticleId, 3,1)*/
		WHERE 
			ps.NumberInStock <> tp.NoInStock
			AND
			substring(tp.HYarticleId, 3,1) = 1
			
		


	
			--print 'Skapa product'
		declare @HYarticleId nvarchar(50),
				@HYarticleIdNoFoK nvarchar(50),
				@HYarticleIdNoFokNoSize nvarchar (50),
				@HYarticleSize nvarchar(50),
				@NoInStock nvarchar(250),
				@HYSizeId nvarchar (50),	
				@Data nvarchar(4000)

		
				----Variabler för FoK (Split--------
				DECLARE @Fok NVARCHAR(40)
				DECLARE @Pos INT
				DECLARE @String NVARCHAR(40)
				DECLARE @Delimiter NVARCHAR(40)
				DECLARE @SizeChar NVARCHAR (20)
				DECLARE @SizeNo NVARCHAR (20)
				------------------------------------
		
		declare cur_productHeppo cursor fast_forward for
			select
				tp.HYarticleId,
				tp.NoInStock,
				tp.SizeId
			from
				[integration].tempProduct tp
			where not exists(select 1
						from lekmer.tProductSize ps
						where ps.ErpId = substring(tp.HYarticleId, 5,17))
						AND tp.SizeId != 'Onesize' -- ny
						AND substring(tp.HYarticleId, 3,1) = 1 -- ny
			
			
		--print 'Open Cursor'
		open cur_productHeppo
		fetch next from cur_productHeppo
			into @HYArticleId,			 
				 @NoInStock,
				 @HYSizeId
		
		
		--print @@FETCH_STATUS
		while @@FETCH_STATUS = 0
		begin
			begin try
				begin transaction
		
				-- DEKLARERA en variabel här som splittar HYErpId och lagrar försäljningskanalen
				-- på så sätt vet man vilken prislista produkten ska ha
				SET @String = @HYarticleId
				SET @Delimiter = '-'
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @Fok = substring(@String,1,@Pos - 1) -- Fok här är nu 001 eller 002 osv beroende på vilket land artikeln tillhör
				SET @HYarticleIdNoFoK = substring(@HYarticleId, 5,17)			
				--SET @HYarticleSize = substring(@HYarticleId, 17,3)-- FEL
				SET @HYarticleIdNoFokNoSize = substring(@HYarticleId, 5,11)
				
				
				
				--print 'Start @SizeChar = ' + @Sizechar
				-- splitta sizeid på semi kolon
				SET @String = @HYSizeId -- EU-45-
				SET @String = @String + @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @SizeChar = substring(@String,1,@Pos - 1)
				--print 'End @SizeChar = ' + @Sizechar
			
				--print 'Start @SizeNo = ' + @SizeNo
				SET @String = @HYSizeId
				SET @String = @String --+ @Delimiter
				SET @Pos = charindex(@Delimiter,@String)

				SET @SizeNo = substring(@String,(@Pos+1),(len(@String)- (@pos)))
				--print 'End @SizeNo = ' + @SizeNo
				
				-- om den specifika sizen av produkten inte finns i tProductSize
				if @SizeChar not in ('XS','S','M','L','XL','Onesize', 'Storlekslös')

				begin
				
					-- Handel the @sizeChar so when it is USM it presents as USMale
					SET @SizeChar = dbo.fx_SizeStandard(@SizeChar)
					
					
					-- Prepare the query that retrives SizeId from tSize
					-- executes a string and returns sizeId
					DECLARE @tSizeIdResult int
					DECLARE @SizeId as int,@sqlQ as nvarchar(200)
					SET @sqlQ=N'select @SizeId = SizeId FROM lekmer.tSize WHERE ' + @SizeChar + ' = ' + REPLACE(@SizeNo, ',', '.')
					EXEC sp_executesql @sqlQ, N'@SizeId int OUTPUT', @SizeId= @SizeId OUTPUT
					--SELECT @SizeId
					SET @tSizeIdResult = @SizeId
					
					set @Data = 'NEW tProductSIZE Shoes: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' @SizeChar ' + @SizeChar + ' @SizeNo ' + @SizeNo
					
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
						@tSizeIdResult, 
						@HYarticleIdNoFoK,
						@NoInStock			
				end
				
				else if @SizeChar not in ('Onesize', 'Storlekslös')
				begin
					
					set @Data = 'NEW tProductSIZE Accessories: @HYarticleIdNoFoK ' + @HYarticleIdNoFoK + ' @Fok ' + @Fok + ' @SizeChar ' + @SizeChar
					
					
					INSERT INTO 
					[lekmer].tProductSize(ProductId, SizeId, ErpId, NumberInStock)
					-- MilimeterDiviation?
					-- OverrideEU?
					SELECT 
						(select ProductId from lekmer.tLekmerProduct where HYErpId = @HYarticleIdNoFokNoSize),
						(select AccessoriesSizeId from integration.tAccessoriesSize where AccessoriesSize = @SizeChar), 
						@HYarticleIdNoFoK,
						@NoInStock
				end
			
				commit	
			end try
			
			begin catch
				if @@TRANCOUNT > 0 rollback
				-- LOG Here
				print 'Loging error'
					INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
					values(@Data, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())

			end catch
			
			fetch next from cur_productHeppo into
				 @HYArticleId,	 
				 @NoInStock,	
				 @HYSizeId
	
		end
		
		close cur_productHeppo
		deallocate cur_productHeppo

END
GO
