
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [orderlek].[pCollectorPaymentTypeSave]
	@CollectorPaymentTypeId INT,
	@ChannelId INT,
	@Code VARCHAR(50),
	@Description NVARCHAR(100),
	@StartFee DECIMAL(16,2),
	@InvoiceFee DECIMAL(16,2),
	@MonthlyInterestRate DECIMAL(16,2),
	@NoOfMonths INT,
	@MinPurchaseAmount DECIMAL(16,2),
	@LowestPayment DECIMAL(16,2),
	@PartPaymentType INT,
	@InvoiceType INT,
	@Ordinal INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @Id INT
	
	SET @Id = @CollectorPaymentTypeId
	
	UPDATE
		[orderlek].[tCollectorPaymentType]
	SET
		[Description] = @Description,
		[StartFee] = @StartFee,
		[InvoiceFee] = @InvoiceFee,
		[MonthlyInterestRate] = @MonthlyInterestRate,
		[NoOfMonths] = @NoOfMonths,
		[MinPurchaseAmount]= @MinPurchaseAmount,
		[LowestPayment] = @LowestPayment,
		[PartPaymentType] = @PartPaymentType,
		[InvoiceType] = @InvoiceType,
		[Ordinal] = @Ordinal
	WHERE
		[ChannelId] = @ChannelId
		AND
		[Code] = @Code
		
	IF @@ROWCOUNT = 0
	BEGIN
		INSERT INTO [orderlek].[tCollectorPaymentType]
				( [ChannelId],
				  [Code],
				  [Description],
				  [StartFee],
				  [InvoiceFee],
				  [MonthlyInterestRate],
				  [NoOfMonths],
				  [MinPurchaseAmount],
				  [LowestPayment],
				  [PartPaymentType],
				  [InvoiceType],
				  [Ordinal]
				)
		VALUES
				( @ChannelId,
				  @Code,
				  @Description,
				  @StartFee,
				  @InvoiceFee,
				  @MonthlyInterestRate,
				  @NoOfMonths,
				  @MinPurchaseAmount,
				  @LowestPayment,
				  @PartPaymentType,
				  @InvoiceType,
				  @Ordinal
				)

		SET @Id = SCOPE_IDENTITY()
	END

	RETURN @Id
END
GO
