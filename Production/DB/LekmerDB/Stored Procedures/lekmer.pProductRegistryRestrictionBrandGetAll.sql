SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pProductRegistryRestrictionBrandGetAll]
AS
BEGIN
	SELECT BrandId, ProductRegistryId 
	FROM [lekmer].[tProductRegistryRestrictionBrand]
END
GO
