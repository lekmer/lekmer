SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pWishListPackageItemGetAllByWishListItem]
	@WishListItemId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[tWishListPackageItem]
	WHERE
		[WishListItemId] = @WishListItemId
END
GO
