SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemGroupFixedDiscountActionIncludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemGroupFixedDiscountActionIncludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
