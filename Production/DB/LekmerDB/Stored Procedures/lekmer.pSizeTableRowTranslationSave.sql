SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableRowTranslationSave]
	@SizeTableRowId	INT,
	@LanguageId		INT,
	@Column1Value	NVARCHAR(50),
	@Column2Value	NVARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	INSERT INTO [lekmer].[tSizeTableRowTranslation] (
		[SizeTableRowId],
		[LanguageId],
		[Column1Value],
		[Column2Value]
	)
	VALUES (
		@SizeTableRowId,
		@LanguageId,
		@Column1Value,
		@Column2Value
	)
END
GO
