SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pOrderPaymentDelete]
	@OrderPaymentId INT
AS
BEGIN
	SET NOCOUNT ON;
	
	DELETE
		[order].tOrderPayment
	WHERE
		OrderPaymentId = @OrderPaymentId
END


GO
