SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCartValueRangeConditionCurrencyDeleteAll]
	@ConditionId INT
AS 
BEGIN
	DELETE FROM [campaignlek].[tCartValueRangeConditionCurrency]
	WHERE ConditionId = @ConditionId
END
GO
