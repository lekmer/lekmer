SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [test].[pImageGetAll]
as
begin
	SELECT
		*
	FROM 
		media.vCustomImage
end

GO
