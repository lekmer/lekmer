SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


/*
*****************  Version 1  *****************
User: Roman A.		Date: 04.09.2008		Time: 13:00
Description: 
*/

CREATE procedure [product].[pRelationListTypeDelete]
	@RelationListTypeId		int
as
begin
	set nocount on

	delete from
		product.tRelationListType
	where
		RelationListTypeId = @RelationListTypeId
end




GO
