SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pModelFragmentFunctionGetById]
	@FunctionId	int
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomModelFragmentFunction]
	where
		[ModelFragmentFunction.Id] = @FunctionId
end

GO
