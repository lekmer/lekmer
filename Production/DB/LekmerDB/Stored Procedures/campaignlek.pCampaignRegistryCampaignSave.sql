SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignRegistryCampaignSave]
	@CampaignId				INT,
	@CampaignRegistryIds	VARCHAR(MAX),
	@Delimiter				CHAR(1)
AS
BEGIN
	EXEC [campaignlek].[pCampaignRegistryCampaignDelete] @CampaignId	

	INSERT INTO [campaignlek].[tCampaignRegistryCampaign] ([CampaignRegistryId], [CampaignId])
	SELECT [crc].[ID], @CampaignId FROM [generic].[fnConvertIDListToTable](@CampaignRegistryIds, @Delimiter) crc
END
GO
