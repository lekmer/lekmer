SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [test].[pCategoryGetRandomByCount]
	@Count INT
as
begin
	SELECT TOP (@Count)
		*
	FROM 
		product.vCustomCategory c
	ORDER By
		NEWID()
end

GO
