SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRatingReviewFeedback_GetById]
	@RatingReviewFeedbackId INT
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		rrf.*
	FROM
		[review].[vRatingReviewFeedback] rrf
	WHERE 
		rrf.[RatingReviewFeedback.RatingReviewFeedbackId] = @RatingReviewFeedbackId
END
GO
