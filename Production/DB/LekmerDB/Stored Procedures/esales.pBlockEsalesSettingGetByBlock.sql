SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesSettingGetByBlock]
	@BlockId INT
AS
BEGIN
	SELECT 
		*		
	FROM
		[esales].[vBlockEsalesSetting]
	WHERE
		[BlockEsalesSetting.BlockId] = @BlockId
END
GO
