SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pBlockProductImageListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT
		[bpil].*,
		[b].*
	FROM
		[productlek].[vBlockProductImageList] bpil
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [bpil].[BlockProductImageList.BlockId] = [b].[Block.BlockId]
	WHERE
		[bpil].[BlockProductImageList.BlockId] = @BlockId
END
GO
