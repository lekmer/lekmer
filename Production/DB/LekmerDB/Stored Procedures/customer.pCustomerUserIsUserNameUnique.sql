SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE procedure [customer].[pCustomerUserIsUserNameUnique]
	@CustomerId int,
	@UserName nvarchar(50)
as	
begin
	
	if exists (
				select 
					1 
				from 
					[customer].[tCustomerUser] 
				where 
					[UserName]= @UserName 
					AND [CustomerId] <> @CustomerId
				)
	BEGIN
		return -1
	end
	
end

GO
