
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pCampaignFolderGetAllByParent]
@ParentId	int
AS
BEGIN
	SELECT *
	FROM [campaign].[vCustomCampaignFolder]
	WHERE [CampaignFolder.ParentId] = @ParentId
		OR (@ParentId IS NULL AND [CampaignFolder.ParentId] IS NULL)
	ORDER BY [CampaignFolder.Ordinal]
END

GO
