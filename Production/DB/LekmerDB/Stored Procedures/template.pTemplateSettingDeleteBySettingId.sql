SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pTemplateSettingDeleteBySettingId]
	@ModelSettingId	int	
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM [template].[tTemplateSetting] WHERE ModelSettingId = @ModelSettingId
	RETURN SCOPE_IDENTITY()
END





GO
