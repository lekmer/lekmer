SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesSettingDelete]
	@BlockId INT
AS
BEGIN
	SET NOCOUNT ON

	DELETE [esales].[tBlockEsalesSetting]
	WHERE [BlockId] = @BlockId
END
GO
