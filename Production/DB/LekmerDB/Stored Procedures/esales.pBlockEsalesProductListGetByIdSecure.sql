SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [esales].[pBlockEsalesProductListGetByIdSecure]
	@BlockId	INT
AS
BEGIN
	SELECT 
		[b].*,
		[bs].*,
		[bes].*
	FROM 
		[sitestructure].[vCustomBlockSecure] b
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
		INNER JOIN [esales].[vBlockEsalesSetting] bes ON [bes].[BlockEsalesSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		[b].[Block.BlockId] = @BlockId
END
GO
