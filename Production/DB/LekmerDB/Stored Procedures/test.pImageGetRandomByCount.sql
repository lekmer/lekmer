SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [test].[pImageGetRandomByCount]
	@Count INT,
	@ChannelId INT
as
begin
	SELECT TOP (@Count)
		*
	FROM 
		media.vCustomImage i
	ORDER By
		NEWID()
end

GO
