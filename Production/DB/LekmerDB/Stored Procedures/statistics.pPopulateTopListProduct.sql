SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [statistics].[pPopulateTopListProduct]
	@Date					DATE,
	@KeepOrderStatisticDays INT
AS
BEGIN
	SET NOCOUNT ON
	
	DELETE FROM [statistics].[tTopListProduct]
	WHERE [Date] < DATEADD(DAY, -@KeepOrderStatisticDays, @Date)

	DECLARE @ChannelId INT
	DECLARE channel_cursor CURSOR FOR SELECT [ChannelId] FROM [core].[tChannel]
	OPEN channel_cursor
	FETCH NEXT FROM channel_cursor INTO @ChannelId
	WHILE @@FETCH_STATUS = 0
	BEGIN
		INSERT INTO [statistics].[tTopListProduct] (
			[ProductId],
			[ChannelId],
			[Date],
			[PurchaseSum]
		)
		SELECT
			[p].[ProductId],
			@ChannelId,
			@Date,
			SUM([oi].[Quantity] * [oi].[ActualPriceIncludingVat])
		FROM
			[product].[tProduct] p
			INNER JOIN [order].[tOrderItemProduct] oip ON [oip].[ProductId] = [p].[ProductId]
			INNER JOIN [order].[tOrderItem] oi  ON [oi].[OrderItemId] = [oip].[OrderItemId]
			INNER JOIN [order].[tOrder] o ON [o].[OrderId] = [oi].[OrderId]
		WHERE
			[p].[IsDeleted] = 0 AND
			[o].[ChannelId] = @ChannelId AND
			[o].[CreatedDate] >= @Date AND
			[o].[CreatedDate] < DATEADD(DAY, 1, @Date) AND
			[o].[OrderStatusId] = 4
		GROUP BY
			[p].[ProductId]		

		FETCH NEXT FROM channel_cursor INTO @ChannelId
	END
	CLOSE channel_cursor
	DEALLOCATE channel_cursor
END
GO
