
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pOfflineProductScript]

AS
BEGIN	
	IF OBJECT_ID('tempdb..#tProductsToUpdateStatusIncrement') IS NOT NULL
	DROP TABLE #tProductsToUpdateStatusIncrement

	CREATE TABLE #tProductsToUpdateStatusIncrement
	(
		ProductId INT NOT NULL,
		CONSTRAINT PK_#tProductsToUpdateStatusIncrement PRIMARY KEY(ProductId)
	)

	DECLARE @Counter INT
	DECLARE @TotalAmountProducts INT
	DECLARE @HighestCurrentProductId INT
	DECLARE @RowSize INT
	SET @Counter = 1
	SET @TotalAmountProducts = (SELECT COUNT(ProductId) FROM product.tProduct)
	SET @HighestCurrentProductId = 1
	SET @RowSize = 200

	WHILE @counter <= @TotalAmountProducts
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION

			-- Fetch @RowSize products at the time
			INSERT INTO #tProductsToUpdateStatusIncrement(ProductId)
			SELECT TOP (@RowSize)
				ProductId
			FROM
				product.tProduct
			WHERE
				ProductId > @HighestCurrentProductId
				--and isdeleted = false (0?)
			ORDER BY
				ProductId


			-- Get highest productId from tmpTable
			SET @HighestCurrentProductId = (SELECT MAX(ProductId) FROM #tProductsToUpdateStatusIncrement)


			-- set products online that fulfill the criterias
			UPDATE
				p
			SET
				ProductStatusId = 0
			FROM
				product.tProduct p
				INNER JOIN #tProductsToUpdateStatusIncrement i ON p.ProductId = i.ProductId
				LEFT JOIN lekmer.tProductSize s ON p.ProductId = s.ProductId
			WHERE
				p.Mediaid IS NOT NULL
				AND
				p.ProductStatusId = 1
				AND
				[Description] IS NOT NULL
				AND
				(
					(p.NumberInStock > 0 AND s.ProductId IS NULL)
					OR
					(s.NumberInStock > 0)
				)


			-- set products offline that fulfill the criterias
			UPDATE
				p
			SET
				ProductStatusId = 1
			FROM
				#tProductsToUpdateStatusIncrement i
				INNER JOIN [lekmer].[tLekmerProduct] lp ON [lp].[ProductId] = [i].[ProductId]
				INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [lp].[ProductId]
				INNER JOIN product.tCategory c1 ON c1.CategoryId = p.CategoryId
				INNER JOIN product.tCategory c2 ON c1.ParentCategoryId = c2.CategoryId
				INNER JOIN product.tCategory c3 ON c2.ParentCategoryId = c3.CategoryId
			WHERE
				(
					c3.[ErpId] = 'C_50' -- Barnkläder
					OR
					c1.[ErpId] = 'C_10-323-2200' -- Barn & Baby > Graviditet & amning > Gravidkläder & tillbehör
					OR
					c1.[ErpId] = 'C_10-323-2202' -- Barn & Baby > Graviditet & amning > Amningskläder
				)
				AND
				p.ProductStatusId = 0
				AND
				p.NumberInStock = 0
				AND
				[lp].[ProductTypeId] = 1
				AND NOT EXISTS
				(
					SELECT 1
					FROM lekmer.tProductSize ps
					WHERE
						ps.ProductId = i.ProductId
						AND
						ps.NumberInStock > 0
				)


			SET @Counter = @Counter + @RowSize
			COMMIT

			-- Clear tmp table
			DELETE FROM #tProductsToUpdateStatusIncrement

		END TRY
		BEGIN CATCH
			IF @@TRANCOUNT > 0 ROLLBACK
			-- LOG here
			INSERT INTO [integration].[integrationLog](Data, [Message], [Date], OcuredInProcedure)
			VALUES(NULL, ERROR_MESSAGE(), GETDATE(), ERROR_PROCEDURE())
		END CATCH
	END
END
GO
