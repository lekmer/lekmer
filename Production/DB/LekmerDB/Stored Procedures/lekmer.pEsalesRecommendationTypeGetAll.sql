SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pEsalesRecommendationTypeGetAll]
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[lekmer].[vEsalesRecommendationType]
END
GO
