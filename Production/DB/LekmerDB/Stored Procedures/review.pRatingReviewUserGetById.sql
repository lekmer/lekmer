SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [review].[pRatingReviewUserGetById]
	@RatingReviewUserId INT
AS    
BEGIN
	SELECT
		*
	FROM
		[review].[vRatingReviewUser] rru
	WHERE
		rru.[RatingReviewUser.RatingReviewUserId] = @RatingReviewUserId
END
GO
