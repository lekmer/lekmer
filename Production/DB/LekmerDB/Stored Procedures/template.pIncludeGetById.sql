SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/*
*****************  Version 1  *****************
User: Yuriy P.		Date: 19.09.2008		Time: 17:00
Description:
			Created 
*****************  Version 2  *****************
User: Egorov V.		Date: 20.11.2008		Time: 11:50
Description:
			Delete ChannelId.
			Rename:
				    Code on Content 
					SystemName on CommonName

*****************  Version 3  *****************
User: Yuriy P.		Date: 23/01/2009
Description: Added Path 

*****************  Version 4  *****************
User: Egorov V.		Date: 26.01.2009
Description: Remove Path
*/
CREATE PROCEDURE [template].[pIncludeGetById]
	@IncludeId	int
AS
begin
	set nocount on
	select
		*
	from
		[template].[vCustomInclude]
	where
		[Include.Id] = @IncludeId
end

GO
