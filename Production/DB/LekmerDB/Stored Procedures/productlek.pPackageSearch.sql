
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [productlek].[pPackageSearch]
	@CategoryId		INT,
	@Title			NVARCHAR(256),
	@BrandId		INT,
	@StatusId		INT,
	@PriceFrom		DECIMAL(16,2),
	@PriceTo		DECIMAL(16,2),
	@ErpId			VARCHAR(MAX),
	@ChannelId		INT,
	@Page			INT = NULL,
	@PageSize		INT,
	@SortBy			VARCHAR(20) = NULL,
	@SortDescending BIT = NULL
AS
BEGIN
	DECLARE @sqlGeneral NVARCHAR(MAX), @sql NVARCHAR(MAX), @sqlCount NVARCHAR(MAX), @sqlFragment NVARCHAR(MAX)
	DECLARE @CurrencyId INT, @ProductRegistryId INT

	SELECT @CurrencyId = [CurrencyId] FROM [core].[tChannel] WHERE [ChannelId] = @ChannelId
	SELECT @ProductRegistryId = [ProductRegistryId] FROM [product].[tProductModuleChannel] WHERE [ChannelId] = @ChannelId
	
	SET @sqlFragment = '
				SELECT ROW_NUMBER() OVER (ORDER BY [p].[' 
		+ COALESCE(@SortBy, 'ErpId')
		+ ']'
		+ CASE WHEN (@SortDescending = 1) THEN ' DESC' ELSE ' ASC' END + ') AS Number,
					[pak].[PackageId] ''Package.PackageId'',
					[pak].[MasterProductId] ''Package.MasterProductId'',
					[pak].[GeneralInfo] ''Package.GeneralInfo'',
					[p].[ProductId],
					[pli].*
				FROM
					[productlek].[tPackage] pak
					INNER JOIN [product].[tProduct] p ON [p].[ProductId] = [pak].[MasterProductId]
					LEFT JOIN [product].[tProductRegistryProduct] prp ON [prp].[ProductId] = [p].[ProductId] AND [prp].[ProductRegistryId] = @ProductRegistryId
					LEFT JOIN [product].[tProductModuleChannel] pmc ON [pmc].[ProductRegistryId] = [prp].[ProductRegistryId] AND [pmc].[ChannelId] = @ChannelId
					LEFT JOIN [product].[vCustomPriceListItem] pli ON [pli].[Price.ProductId] = [p].[ProductId]
																	  AND [pli].[Price.PriceListId] = [product].[fnGetPriceListIdOfItemWithLowestPrice] (
																										@CurrencyId,
																										[p].[ProductId],
																										[pmc].[PriceListRegistryId],
																										NULL)
					INNER JOIN [lekmer].[tLekmerProduct] AS lp ON [lp].[ProductId] = [p].[ProductId]
				WHERE
					[p].[IsDeleted] = 0'
		
		+ CASE WHEN (@CategoryId IS NOT NULL) THEN + '
					AND [p].[CategoryId] IN (SELECT * FROM product.fnGetSubCategories(@CategoryId))' ELSE '' END 
				
		+ CASE WHEN (@Title IS NOT NULL) THEN + '
					AND CONTAINS([p].*, @Title)' ELSE '' END
		
		+ CASE WHEN (@PriceFrom IS NOT NULL) THEN + '
					AND [pli].[Price.PriceIncludingVAT] >= @PriceFrom' ELSE '' END
		
		+ CASE WHEN (@PriceTo IS NOT NULL) THEN + '
					AND [pli].[Price.PriceIncludingVAT] <= @PriceTo' ELSE '' END
		
		+ CASE WHEN (@ErpId IS NOT NULL) THEN + '
					AND [p].[ErpId] IN (SELECT * FROM generic.fnConvertIDListToTableString(@ErpId,'',''))' ELSE '' END
		
		+ CASE WHEN (@StatusId IS NOT NULL) THEN + '
					AND [p].[ProductStatusId] = @StatusId' ELSE '' END
		
		+ CASE WHEN (@BrandId IS NOT NULL) THEN + '
					AND [lp].[BrandId] = @BrandId' ELSE '' END

	SET @sqlGeneral = '
		IF (@Title IS NOT NULL)
		BEGIN
			SET @Title = generic.fPrepareFulltextSearchParameter(@Title)
		END'
		
	SET @sql = @sqlGeneral + '
		SELECT
			p.*,
			vp.*
		FROM
			(' + @sqlFragment + '
			) AS p
			INNER JOIN [product].[vCustomProductRecord] vp on [vp].[Product.Id] = [p].[ProductId]'

	IF @Page != 0 AND @Page IS NOT NULL
	BEGIN
		SET @sql = @sql + '
		WHERE Number > (@Page - 1) * @PageSize AND Number <= @Page * @PageSize'
	END
	
	SET @sqlCount = @sqlGeneral + '
		SELECT COUNT(1) FROM
		(' + @sqlFragment + '
		)
		AS CountResults'

	EXEC sp_executesql @sqlCount,
		N'	
			@CategoryId int,
			@Title nvarchar(256),
			@BrandId int,
			@StatusId int,
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@ChannelId	int,			
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@CategoryId,
			@Title,
			@BrandId,
			@StatusId,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@ChannelId,			
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId

	EXEC sp_executesql @sql, 
		N'	
			@CategoryId int,
			@Title nvarchar(256),
			@BrandId int,
			@StatusId int,
			@PriceFrom decimal(16,2),
			@PriceTo decimal(16,2),
			@ErpId varchar(max),
			@ChannelId	int,			
			@Page int,
			@PageSize int,
			@CurrencyId int,
			@ProductRegistryId int',
			@CategoryId,
			@Title,
			@BrandId,
			@StatusId,
			@PriceFrom,
			@PriceTo,
			@ErpId,
			@ChannelId,			
			@Page,
			@PageSize,
			@CurrencyId,
			@ProductRegistryId
END
GO
