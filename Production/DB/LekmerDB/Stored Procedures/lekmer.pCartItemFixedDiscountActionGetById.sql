SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemFixedDiscountActionGetById]
	@CartActionId INT
AS
BEGIN
	SELECT
		*
	FROM
		[lekmer].[vCartItemFixedDiscountAction]
	WHERE
		[CartItemFixedDiscountAction.CartActionId] = @CartActionId
END

GO
