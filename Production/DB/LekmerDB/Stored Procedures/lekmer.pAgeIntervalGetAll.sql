SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create PROCEDURE lekmer.[pAgeIntervalGetAll]
	@LanguageId	int
AS
begin
	set nocount on

	select
		*
	from
		lekmer.[vAgeInterval]
	where
		[AgeInterval.LanguageId] = @LanguageId
end

GO
