SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeTableMediaGetAllBySizeTable]
	@SizeTableId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		[stm].*
	FROM
		[lekmer].[vSizeTableMedia] stm
	WHERE
		stm.[SizeTableMedia.SizeTableId] = @SizeTableId
END
GO
