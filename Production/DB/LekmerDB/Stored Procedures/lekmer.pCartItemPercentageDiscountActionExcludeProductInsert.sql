SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE PROCEDURE [lekmer].[pCartItemPercentageDiscountActionExcludeProductInsert]
	@CartActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [lekmer].[tCartItemPercentageDiscountActionExcludeProduct] (
		CartActionId,
		ProductId
	)
	VALUES (
		@CartActionId,
		@ProductId
	)
END


GO
