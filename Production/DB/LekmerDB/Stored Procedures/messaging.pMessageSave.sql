SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [messaging].[pMessageSave]
    @MessageId UNIQUEIDENTIFIER,
    @BatchId UNIQUEIDENTIFIER,
    @MessageStatusId INT,
    @MessageTypeId INT,
    @ProviderName VARCHAR(100),
    @Body NVARCHAR(MAX),
    @CreatedDate DATETIME
AS 
    UPDATE  [messaging].[tMessage]
    SET     BatchId = @BatchId,
            MessageStatusId = @MessageStatusId,
            MessageTypeId = @MessageTypeId,
            ProviderName = @ProviderName,
            Body = @Body,
            CreatedDate = @CreatedDate
    WHERE   MessageId = @MessageId						
    
    IF @@ROWCOUNT = 0
    INSERT  INTO [messaging].[tMessage]
            (
              MessageId,
              BatchId,
              MessageStatusId,
              MessageTypeId,
              ProviderName,
              Body,
              CreatedDate
				
            )
    VALUES  (
              @MessageId,
              @BatchId,
              @MessageStatusId,
              @MessageTypeId,
              @ProviderName,
              @Body,
              @CreatedDate
				
            )
GO
