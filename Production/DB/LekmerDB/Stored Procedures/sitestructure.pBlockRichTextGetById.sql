SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pBlockRichTextGetById]
	@LanguageId	int,
	@BlockId	int
as
begin
	select 
		brt.*	
	from 
		[sitestructure].[vCustomBlockRichText] as brt
	where
		brt.[Block.BlockId] = @BlockId 
		AND brt.[Block.LanguageId] = @LanguageId
end

GO
