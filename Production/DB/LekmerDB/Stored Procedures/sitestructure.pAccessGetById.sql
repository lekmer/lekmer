SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [sitestructure].[pAccessGetById]
@AccessId	int
as
begin
	select 
		*
	from 
		[sitestructure].[vCustomAccess]
	where
		[Access.Id] = @AccessId
end

GO
