SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroTransactionSaveCheckOrderStatusResponse]
	@TransactionId INT,
	@StatusCode INT,
	@ReturnCodeId INT,
	@InvoiceStatus INT,
	@Message NVARCHAR(MAX),
	@Duration BIGINT
AS 
BEGIN
	SET NOCOUNT ON
	
	UPDATE [orderlek].[tQliroTransaction]
	SET
		[StatusCode] = @StatusCode,
		[ReturnCodeId] = @ReturnCodeId,
		[InvoiceStatus] = @InvoiceStatus,
		[Message] = @Message,
		[Duration] = @Duration
	WHERE
		[TransactionId] = @TransactionId
END
GO
