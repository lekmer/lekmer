SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageTranslationSave]
	@ContentMessageId	INT,
	@LanguageId		INT,
	@Message	NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON
	
	UPDATE
		[lekmer].[tContentMessageTranslation]
	SET
		[Message] = @Message
	WHERE
		[ContentMessageId] = @ContentMessageId 
		AND [LanguageId] = @LanguageId
		
	IF @@ROWCOUNT = 0
	BEGIN 		
		INSERT INTO [lekmer].[tContentMessageTranslation] (
			[ContentMessageId],
			[LanguageId],
			[Message]
		)
		VALUES (
			@ContentMessageId,
			@LanguageId,
			@Message
		)
	END
END
GO
