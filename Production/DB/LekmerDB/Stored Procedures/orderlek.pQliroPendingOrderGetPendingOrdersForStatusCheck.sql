SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [orderlek].[pQliroPendingOrderGetPendingOrdersForStatusCheck]
	@CheckToDate DATETIME
AS 
BEGIN
	SET NOCOUNT ON;

	SELECT
		[po].*
	FROM
		[orderlek].[vQliroPendingOrder] po
	WHERE
		[po].[QliroPendingOrder.LastAttempt] < @CheckToDate
END
GO
