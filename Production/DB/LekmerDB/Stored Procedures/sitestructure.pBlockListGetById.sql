SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [sitestructure].[pBlockListGetById]
	@BlockId INT
AS 
BEGIN 
	SET NOCOUNT ON

	SELECT
		*
	FROM
		[sitestructure].[vBlockList]
	WHERE
		[BlockId] = @BlockId
END
GO
