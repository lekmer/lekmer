
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [order].[pCartItemGetAllByCart]
	@ChannelId INT,
	@CustomerId INT,
	@CartId INT
AS 
BEGIN
	SET NOCOUNT ON
	
	SELECT
		TOP 100
		*
	FROM
		[order].vCustomCartItem c
		INNER JOIN product.vCustomPriceListItem AS pli
			ON pli.[Price.ProductId] = c.[Product.Id]
			AND pli.[Price.PriceListId] = product.fnGetPriceListIdOfItemWithLowestPrice(
				c.[Product.CurrencyId],
				c.[Product.Id],
				c.[Product.PriceListRegistryId],
				@CustomerId
			)
	WHERE
		c.[CartItem.CartId] = @CartId
		AND c.[Product.ChannelId] = @ChannelId
	ORDER BY
		c.[CartItem.CreatedDate] ASC
END
GO
