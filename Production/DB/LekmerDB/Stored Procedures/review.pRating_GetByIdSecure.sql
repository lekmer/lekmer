SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [review].[pRating_GetByIdSecure]
	@RatingId INT
AS
BEGIN
	SET NOCOUNT ON

	SELECT
		r.*
	FROM
		[review].[vRatingSecure] r
	WHERE
		r.[Rating.RatingId] = @RatingId
END
GO
