
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pBlockBrandListSave]
	@BlockId			INT,
	@IncludeAllBrands	BIT,
	@LinkContentNodeId	INT
AS 
BEGIN 
	UPDATE lekmer.tBlockBrandList
	SET 
		IncludeAllBrands = @IncludeAllBrands,
		LinkContentNodeId = @LinkContentNodeId
	WHERE 
		BlockId = @BlockId
		
	IF @@ROWCOUNT <> 0
		RETURN
		
	INSERT lekmer.tBlockBrandList (
		BlockId,
		IncludeAllBrands,
		LinkContentNodeId
	)
	VALUES (
		@BlockId,
		@IncludeAllBrands,
		@LinkContentNodeId
	)
END 
GO
