SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create PROCEDURE [integration].[pDealOfTheDayGetNumberInStock]
	@HyId		nvarchar(100)
	--@ProductXml		int --xml
AS
begin
	set nocount on
	
	/*if object_id('tempdb..#tAdventskalender') is not null
		drop table #tAdventskalender

	create table #tAdventskalender
	(
		ProductId int not null,
		--Title nvarchar (250) COLLATE Finnish_Swedish_CI_AS not null,
		constraint PK_#tProductUrls primary key(ProductId)
	)*/
	
	begin try
		--begin transaction
		
		/*insert #tAdventskalender(ProductId)		
		select 
		T.c.value('@id[1]', 'int')
		--T.c.value('@title[1]', 'nvarchar(250)'),
		--T.c.value('@languageid[1]', 'int')
		from @ProductXml.nodes('/products/product') T(c)*/
		
		
		select
			--p.ProductId,
			NumberInStock 			
		from 
			product.tProduct p
			inner join lekmer.tLekmerProduct l
				on p.ProductId = l.ProductId
			--inner join #tAdventskalender i
				--on p.ProductId = i.ProductId
		where
			l.HYErpId = @HyId
		
							

	--commit transaction
	end try
	begin catch
		-- If transaction is active, roll it back.
		if @@trancount > 0 rollback transaction
		
	end catch		 
end
GO
