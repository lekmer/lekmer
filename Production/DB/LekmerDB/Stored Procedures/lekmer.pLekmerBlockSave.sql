
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pLekmerBlockSave]
	@BlockId					INT,
	@StartDate					DATETIME = NULL,
	@EndDate					DATETIME = NULL,
	@StartDailyIntervalMinutes	INT = NULL,
	@EndDailyIntervalMinutes	INT = NULL,
	@ShowOnDesktop				BIT = NULL,
	@ShowOnMobile				BIT = NULL,
	@CanBeUsedAsFallbackOption	BIT = NULL
AS 
BEGIN
	SET NOCOUNT ON

	UPDATE
		[lekmer].[tLekmerBlock]
	SET	
		[StartDate]					= @StartDate,
		[EndDate]					= @EndDate,
		[StartDailyIntervalMinutes] = @StartDailyIntervalMinutes,
		[EndDailyIntervalMinutes]	= @EndDailyIntervalMinutes,
		[ShowOnDesktop]				= @ShowOnDesktop,
		[ShowOnMobile]				= @ShowOnMobile,
		[CanBeUsedAsFallbackOption] = @CanBeUsedAsFallbackOption
	WHERE
		[BlockId] = @BlockId
  
	IF @@ROWCOUNT = 0 
	BEGIN
		INSERT	[lekmer].[tLekmerBlock] (
			[BlockId],
			[StartDate],
			[EndDate],
			[StartDailyIntervalMinutes],
			[EndDailyIntervalMinutes],
			[ShowOnDesktop],
			[ShowOnMobile],
			[CanBeUsedAsFallbackOption]
		)
		VALUES (
			@BlockId,
			@StartDate,
			@EndDate,
			@StartDailyIntervalMinutes,
			@EndDailyIntervalMinutes,
			@ShowOnDesktop,
			@ShowOnMobile,
			@CanBeUsedAsFallbackOption
		)
	END
END
GO
