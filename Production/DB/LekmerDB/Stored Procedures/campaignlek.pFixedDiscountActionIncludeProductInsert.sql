SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pFixedDiscountActionIncludeProductInsert]
	@ProductActionId INT,
	@ProductId INT
AS
BEGIN
	INSERT [campaignlek].[tFixedDiscountActionIncludeProduct] (
		ProductActionId,
		ProductId
	)
	VALUES (
		@ProductActionId,
		@ProductId
	)
END
GO
