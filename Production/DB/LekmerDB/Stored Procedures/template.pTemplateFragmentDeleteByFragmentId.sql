SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [template].[pTemplateFragmentDeleteByFragmentId]
	@ModelFragmentId	int	
AS
BEGIN
	SET NOCOUNT ON
	DELETE FROM [template].[tTemplateFragment] WHERE ModelFragmentId = @ModelFragmentId
END





GO
