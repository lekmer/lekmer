SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pSizeSetOrdinals]
	@SizeIdList VARCHAR(MAX),
	@Separator CHAR(1)
AS
BEGIN
	UPDATE
		s
	SET
		s.[Ordinal] = t.[Ordinal]
	FROM
		[lekmer].[tSize] s
		INNER JOIN generic.fnConvertIDListToTableWithOrdinal(@SizeIdList, @Separator) t ON s.[SizeId] = t.[Id]
END
GO
