SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [integration].[pReportCustomerSegmentListGetBrandAllByIdList]
	@BrandIds	VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		[b].[Brand.BrandId],
		[b].[Brand.Title],
		[b].[Brand.ErpId]
	FROM
		[lekmer].[vBrandSecure] b
		INNER JOIN [generic].[fnConvertIDListToTable](@BrandIds, ',') AS bl ON [bl].[ID] = [b].[Brand.BrandId]
END
GO
