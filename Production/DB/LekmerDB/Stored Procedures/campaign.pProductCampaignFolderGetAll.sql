
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaign].[pProductCampaignFolderGetAll]
AS
BEGIN
	SELECT *
	FROM campaign.vCustomCampaignFolder
	WHERE [CampaignFolder.TypeId] = 1
	ORDER BY [CampaignFolder.Ordinal]
END

GO
