SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
/******************  Version 1  **********************************************
User: Volodymyr Y.   19.01.2009                                      >Created*
*****************************************************************************/

CREATE PROCEDURE [template].[pModelSettingGetByCommonName]
    @CommonName VARCHAR(50)
AS 
BEGIN
    SET NOCOUNT ON
    
    SELECT  *
    FROM    [template].[vCustomModelSetting]
    WHERE   [ModelSetting.CommonName] = @CommonName
END

GO
