SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pWishListPackageItemSave]
	@WishListItemId	INT,
	@ProductId		INT,
	@SizeId			INT = NULL
AS
BEGIN
	INSERT INTO [lekmer].[tWishListPackageItem] (
		[WishListItemId],
		[ProductId],
		[SizeId]
	)
	VALUES (
		@WishListItemId,
		@ProductId,
		@SizeId
	)

	RETURN SCOPE_IDENTITY()
END
GO
