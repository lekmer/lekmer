SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create procedure [campaign].[pFreightValueActionCurrencyDeleteAll]
	@CartActionId int
as
begin
	DELETE
		campaign.tFreightValueActionCurrency
	WHERE
		CartActionId = @CartActionId
end



GO
