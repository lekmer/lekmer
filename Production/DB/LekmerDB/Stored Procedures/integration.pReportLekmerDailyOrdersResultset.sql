
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [integration].[pReportLekmerDailyOrdersResultset]
	@DateFrom	DATETIME = NULL,
	@DateTo		DATETIME = NULL
AS
BEGIN
	SET NOCOUNT ON

	SET @DateTo = DATEADD(day, 1, CONVERT(VARCHAR, @DateTo, 112))
	
	IF @DateFrom IS NULL
	BEGIN
		SET @DateFrom = DATEADD(day, -1, CONVERT(VARCHAR, GETDATE(), 112))
	END
	
	IF @DateTo IS NULL
	BEGIN
		SET @DateTo = DATEADD(day, 1, CONVERT(VARCHAR, GETDATE(), 112))
	END
	
	IF OBJECT_ID('tempdb..#tTmpResult') IS NOT NULL
	DROP TABLE #tTmpResult
	
	CREATE TABLE #tTmpResult
	(			
		[Date] DATETIME NOT NULL,
		NumberOfOrders INT NOT NULL,
		AverageOrderPrice DECIMAL(16,2) NOT NULL,
		TotalOrderValue DECIMAL(16,2) NOT NULL,
		Channel NVARCHAR (50)
		--constraint PK_#tProductPriceIncludingVatLess primary key(ProductId, PricelistId)
	)
	
	INSERT INTO #tTmpResult([Date], NumberOfOrders, AverageOrderPrice, TotalOrderValue, Channel)
	SELECT
		x.[Date] AS 'Date',
		COUNT(x.OrderId) AS 'NumberOfOrders',
		(SUM(x.Price)/COUNT(x.OrderId) * 0.8) AS 'AvrageOrderPrice',
		(SUM(x.Price) * 0.8) AS 'TotalOrderValue',
		CASE x.ChannelId 
			WHEN 1 THEN 'Sweden'
			WHEN 2 THEN 'Norway'
			WHEN 3 THEN 'Denmark'
			WHEN 4 THEN 'Finland'
			WHEN 1000005 THEN 'Netherlands'
		END AS Channel
		--ROW_NUMBER() OVER(ORDER BY channelid) AS 'RowNumber'
	FROM
		(
		SELECT
			CAST(YEAR(Createddate) AS VARCHAR(4)) + '-' + CAST(MONTH(Createddate) AS VARCHAR(2)) + '-' + CAST(DAY(Createddate) AS VARCHAR(2)) AS 'Date',
			o.OrderId,
			op.Price,
			ChannelId
		FROM 
		   [order].tOrder o
		   INNER JOIN [order].tOrderPayment op
					  ON o.OrderId = op.OrderId

		WHERE 
			--Createddate > DATEADD(day, -@NumberOfDaysBack, Convert(varchar, GETDATE(), 112))
			Createddate > @DateFrom
			AND Createddate < @DateTo
			AND o.OrderStatusId = 4

		) x
	GROUP BY
		x.[Date], x.ChannelId					  
	ORDER BY
		ChannelId
		
	-- Hårdkoda generella valutor (FEL!!)
	UPDATE #tTmpResult
	SET AverageOrderPrice = (AverageOrderPrice * 8.9),
		TotalOrderValue = (TotalOrderValue * 8.9)
	WHERE
		Channel = 'Finland'
		
	UPDATE #tTmpResult
	SET AverageOrderPrice = (AverageOrderPrice * 8.9),
		TotalOrderValue = (TotalOrderValue * 8.9)
	WHERE
		Channel = 'Netherlands'
	
	UPDATE #tTmpResult
	SET AverageOrderPrice = (AverageOrderPrice * 1.1),
		TotalOrderValue = (TotalOrderValue * 1.1)
	WHERE
		Channel = 'Norway'
	
	UPDATE #tTmpResult
	SET AverageOrderPrice = (AverageOrderPrice * 1.2),
		TotalOrderValue = (TotalOrderValue * 1.2)
	WHERE
		Channel = 'Denmark'	
	--- RESULT
	
	SELECT 
		CONVERT(VARCHAR (10), [Date], 120) AS [Date],
		--CONVERT(VARCHAR(10), GETDATE(), 120) AS [YYYY-MM-DD]
		NumberOfOrders, 
		CONVERT(DECIMAL(16,2),AverageOrderPrice) AS AverageOrderPrice,
		TotalOrderValue, Channel
	FROM
		(SELECT * FROM #tTmpResult
		UNION		
		SELECT [Date], SUM(NumberOfOrders) AS Total, (SUM(TotalOrderValue)/SUM(NumberOfOrders)), SUM(TotalOrderValue),
		 'Total' FROM #tTmpResult
		GROUP BY
				[Date]) v
	ORDER BY
		Channel					
		 
END
GO
