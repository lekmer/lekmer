
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pSizeGetAll]
AS 
BEGIN
	SET NOCOUNT ON
	
	SELECT 
		*
	FROM 
		[lekmer].[vSize]
	ORDER BY
		[Size.Ordinal]
END
GO
