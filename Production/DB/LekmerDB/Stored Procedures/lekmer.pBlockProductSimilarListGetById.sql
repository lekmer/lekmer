
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [lekmer].[pBlockProductSimilarListGetById]
	@LanguageId INT,
	@BlockId	INT
AS
BEGIN
	SELECT 
		[psl].*,
		[b].*,
		[bs].*
	FROM 
		[lekmer].[vCustomBlockProductSimilarList] AS psl
		INNER JOIN [sitestructure].[vCustomBlock] AS b ON psl.[BlockProductSimilarList.BlockId] = b.[Block.BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
	WHERE
		psl.[BlockProductSimilarList.BlockId] = @BlockId
		AND b.[Block.LanguageId] = @LanguageId
END
GO
