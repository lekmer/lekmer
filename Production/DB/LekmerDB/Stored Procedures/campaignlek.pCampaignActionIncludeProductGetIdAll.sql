
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [campaignlek].[pCampaignActionIncludeProductGetIdAll]
	@ConfigId	INT
AS
BEGIN
	SELECT
		[p].[ProductId]
	FROM
		[campaignlek].[tCampaignActionIncludeProduct] aip
		INNER JOIN [product].[tProduct] p ON [aip].[ProductId] = [p].[ProductId]
	WHERE
		[aip].[ConfigId] = @ConfigId
		AND [p].[IsDeleted] = 0
END
GO
