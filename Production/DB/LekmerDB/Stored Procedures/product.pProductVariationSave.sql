SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE procedure [product].[pProductVariationSave]
	@VariationId		int,
	@VariationGroupId	int,
	@VariationTypeId	int,	
	@ProductId			int
as
BEGIN
	IF (
		SELECT
			COUNT(*)
		FROM 
			product.tVariationGroupProduct 
	    WHERE 
			ProductId = @ProductId 
			AND VariationGroupId = @VariationGroupId
		) < 1
	BEGIN
		INSERT INTO [product].[tVariationGroupProduct] 
		(
			[VariationGroupId], 
			[ProductId]
		)
		VALUES 
		(
			@VariationGroupId, 
			@ProductId
		)
	END
		
	insert into product.[tProductVariation]
	(
		[VariationGroupId],
		[VariationTypeId],
		[VariationId],
		[ProductId]
	)
	values
	(
		@VariationGroupId,
		@VariationTypeId,
		@VariationId,
		@ProductId
	)
	return scope_identity();

end








GO
