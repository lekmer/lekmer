
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE PROCEDURE [lekmer].[pContentMessageIncludesSave]
	@ContentMessageId	INT,
	@ProductIds			VARCHAR(MAX),
	@CategoryIds		VARCHAR(MAX),
	@BrandIds			VARCHAR(MAX),
	@SupplierIds		VARCHAR(MAX),
	@Delimiter			CHAR(1)
AS
BEGIN
	EXEC [lekmer].[pContentMessageIncludeProductDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeCategoryDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeBrandDelete] @ContentMessageId
	EXEC [lekmer].[pContentMessageIncludeSupplierDelete] @ContentMessageId

	--EXEC [lekmer].[pContentMessageIncludesClear] @ProductIds, @CategoryIds, @BrandIds, @Delimiter
	
	INSERT [lekmer].[tContentMessageIncludeProduct] ([ContentMessageId], [ProductId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@ProductIds, @Delimiter) p 
	WHERE [p].[ID] NOT IN (SELECT [ProductId] FROM [lekmer].[tContentMessageIncludeProduct] WHERE [ContentMessageId] = @ContentMessageId)
	
	INSERT [lekmer].[tContentMessageIncludeCategory] ([ContentMessageId], [CategoryId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@CategoryIds, @Delimiter) p
	WHERE [p].[ID] NOT IN (SELECT [CategoryId] FROM [lekmer].[tContentMessageIncludeCategory] WHERE [ContentMessageId] = @ContentMessageId)
	
	INSERT [lekmer].[tContentMessageIncludeBrand] ([ContentMessageId], [BrandId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@BrandIds, @Delimiter) p
	WHERE [p].[ID] NOT IN (SELECT [BrandId] FROM [lekmer].[tContentMessageIncludeBrand] WHERE [ContentMessageId] = @ContentMessageId)
	
	INSERT [lekmer].[tContentMessageIncludeSupplier] ([ContentMessageId], [SupplierId])
	SELECT @ContentMessageId, [p].[ID] FROM [generic].[fnConvertIDListToTable](@SupplierIds, @Delimiter) p
	WHERE [p].[ID] NOT IN (SELECT [SupplierId] FROM [lekmer].[tContentMessageIncludeSupplier] WHERE [ContentMessageId] = @ContentMessageId)
END
GO
