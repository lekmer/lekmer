SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vSizeTableMediaProductRegistry]
AS
	SELECT
		[stmpr].[SizeTableMediaProductRegistryId] AS 'SizeTableMediaProductRegistry.SizeTableMediaProductRegistryId',
		[stmpr].[SizeTableMediaId] AS 'SizeTableMediaProductRegistry.SizeTableMediaId',
		[stmpr].[ProductRegistryId] AS 'SizeTableMediaProductRegistry.ProductRegistryId'
	FROM 
		[lekmer].[tSizeTableMediaProductRegistry] stmpr
GO
