
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [integration].[vSirProductsSource]
AS
SELECT
	p.[ErpId] + '-' + ISNULL(s.[ErpId], '000') AS 'ArticleNumber',
	[p].[ProductId] AS 'ProductId',
	SUBSTRING(c.[ErpId], 3, 12) AS 'ProductGroup',
	COALESCE(pt.[WebShopTitle], p.[WebShopTitle], pt.[Title], p.[Title]) AS 'ArticleDescription',
	p.[EanCode] AS 'EanCode',
	ISNULL(pp.[DiscountPriceIncludingVat], pli.[PriceIncludingVat]) AS 'OurPrice', -- Incl VAT
	pli.[PriceIncludingVat] AS 'ListPrice', -- Incl VAT
	lp.[SupplierId] AS 'Supplier',
	ISNULL(bt.[Title], b.[Title]) AS 'Category',
	lp.[LekmerErpId] AS 'LekmerArtNo',
	lp.[SupplierArticleNumber] AS 'SupplierArtNo',
	lp.[AveragePrice] AS 'AveragePrice'
FROM
	[product].[tProduct] p
	INNER JOIN [product].[tCategory] c ON c.[CategoryId] = p.[CategoryId]
	INNER JOIN [lekmer].[tLekmerProduct] lp ON lp.[ProductId] = p.[ProductId]
	INNER JOIN [lekmer].[tBrand] b ON lp.[BrandId] = b.[BrandId]
	LEFT OUTER JOIN [product].[tProductTranslation] pt ON pt.[ProductId] = p.[ProductId] AND pt.[LanguageId] = 1 -- SE only
	LEFT OUTER JOIN [lekmer].[tBrandTranslation] bt ON bt.[BrandId] = b.[BrandId]     AND bt.[LanguageId] = 1 -- SE only
	LEFT OUTER JOIN [product].[tPriceListItem] pli ON pli.[ProductId] = p.[ProductId] AND pli.[PriceListId] = 1 -- SE only
	LEFT OUTER JOIN [export].[tProductPrice] pp ON pp.[ProductId] = p.[ProductId] AND pp.[ChannelId] = 1 -- SE only
	LEFT OUTER JOIN [lekmer].[tProductSize] ps ON ps.[ProductId] = p.[ProductId]
	LEFT OUTER JOIN [lekmer].[tSize] s ON s.[SizeId] = ps.[SizeId]
WHERE
	p.[IsDeleted] = 0 -- Skip deleted products
	AND
	lp.[ProductTypeId] = 1 -- Only products, skip packages

GO
