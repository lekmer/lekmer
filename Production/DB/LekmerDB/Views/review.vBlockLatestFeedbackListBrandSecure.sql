SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [review].[vBlockLatestFeedbackListBrandSecure]
AS
	SELECT
		blflb.[BlockId] AS 'BlockLatestFeedbackListBrand.BlockId',
		blflb.[BrandId] AS 'BlockLatestFeedbackListBrand.BrandId',
		b.*
	FROM
		[review].[tBlockLatestFeedbackListBrand] blflb
		INNER JOIN [lekmer].[vBrandSecure] b ON b.[Brand.BrandId] = blflb.[BrandId]

GO
