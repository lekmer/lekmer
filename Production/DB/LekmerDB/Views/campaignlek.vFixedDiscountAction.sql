SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vFixedDiscountAction]
AS
	SELECT
		fda.[ProductActionId] AS 'FixedDiscountAction.ProductActionId',
		fda.[IncludeAllProducts] AS 'FixedDiscountAction.IncludeAllProducts',
		pa.*
	FROM
		[campaignlek].[tFixedDiscountAction] fda
		INNER JOIN [campaign].[vCustomProductAction] pa ON pa.[ProductAction.Id] = fda.[ProductActionId]
GO
