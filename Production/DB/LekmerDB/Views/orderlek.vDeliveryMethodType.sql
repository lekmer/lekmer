SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [orderlek].[vDeliveryMethodType]
AS
SELECT
	[DeliveryMethodTypeId] AS 'DeliveryMethodType.Id',
	[Title] AS 'DeliveryMethodType.Title',
	[CommonName] AS 'DeliveryMethodType.CommonName'
FROM
	[orderlek].[tDeliveryMethodType]

GO
