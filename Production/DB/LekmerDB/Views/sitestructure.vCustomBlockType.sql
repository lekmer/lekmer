SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomBlockType]
as
	select
		*
	from
		[sitestructure].[vBlockType]
GO
