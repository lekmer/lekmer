SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vBrandSiteStructureRegistry]
AS
SELECT     
	BrandId 'BrandSiteStructureRegistry.BrandId',
	SiteStructureRegistryId 'BrandSiteStructureRegistry.SiteStructureRegistryId',
	ContentNodeId 'BrandSiteStructureRegistry.ContentNodeId'
FROM
	[lekmer].tBrandSiteStructureRegistry

GO
