
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [media].[vImageSize]
AS
SELECT 
	 [ImageSizeId] 'ImageSize.Id',
	 [Title] 'ImageSize.Title',
	 [CommonName] 'ImageSize.CommonName',
	 [Width] 'ImageSize.Width',
	 [Height] 'ImageSize.Height',
	 [Quality] 'ImageSize.Quality',
	 [MobileWidth] 'ImageSize.MobileWidth',
	 [MobileHeight] 'ImageSize.MobileHeight',
	 [MobileQuality] 'ImageSize.MobileQuality'
FROM   
	[media].[tImageSize]

GO
