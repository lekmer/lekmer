SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [esales].[vEsalesModel]
AS
	SELECT
		[em].[ModelId] AS 'EsalesModel.ModelId',
		[em].[Title] AS 'EsalesModel.Title',
		[em].[CommonName] AS 'EsalesModel.CommonName',
		[em].[Ordinal] AS 'EsalesModel.Ordinal'
	FROM
		[esales].[tEsalesModel] em

GO
