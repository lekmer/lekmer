
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vProductSize]
AS
SELECT     
	ps.[ProductId] AS 'ProductSize.ProductId',
	ps.[SizeId] AS 'ProductSize.SizeId',
	ps.[ErpId] AS 'ProductSize.ErpId',
	ps.[NumberInStock] AS 'ProductSize.NumberInStock',
	ps.[MillimeterDeviation] AS 'ProductSize.MillimeterDeviation',
	ps.[OverrideEU] AS 'ProductSize.OverrideEU',
	ps.[OverrideMillimeter] AS 'ProductSize.OverrideMillimeter',
	[ps].[Weight] AS 'ProductSize.Weight',
	[ps].[StockStatusId] AS 'ProductSize.StockStatusId',
	s.*
FROM
	[lekmer].[tProductSize] ps
	INNER JOIN [lekmer].[vSize] s ON ps.[SizeId] = s.[Size.Id]

GO
