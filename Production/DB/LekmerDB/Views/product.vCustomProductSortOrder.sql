SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductSortOrder]
as
	select
		*
	from
		[product].[vProductSortOrder]
GO
