SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [orderlek].[vMaksuturvaPendingOrder]
AS
SELECT
	[po].[MaksuturvaPendingOrderId] AS 'MaksuturvaPendingOrder.MaksuturvaPendingOrderId',
	[po].[ChannelId] AS 'MaksuturvaPendingOrder.ChannelId',
	[po].[OrderId] AS 'MaksuturvaPendingOrder.OrderId',
	[po].[StatusId] AS 'MaksuturvaPendingOrder.StatusId',
	[po].[FirstAttempt] AS 'MaksuturvaPendingOrder.FirstAttempt',
	[po].[LastAttempt] AS 'MaksuturvaPendingOrder.LastAttempt',
	[po].[NextAttempt] AS 'MaksuturvaPendingOrder.NextAttempt'
FROM
	[orderlek].[tMaksuturvaPendingOrder] po
GO
