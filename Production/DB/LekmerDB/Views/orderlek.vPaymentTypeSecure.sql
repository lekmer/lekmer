
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [orderlek].[vPaymentTypeSecure]
AS
SELECT
	[pt].[PaymentTypeId]	'PaymentType.PaymentTypeId',
	[pt].[CommonName]		'PaymentType.CommonName',
	[pt].[ErpId]			'PaymentType.ErpId',
	[pt].[Title]			'PaymentType.Title',
	[pt].[Ordinal]			'PaymentType.Ordinal'
FROM
	[order].[tPaymentType] pt
GO
