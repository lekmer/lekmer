
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO










CREATE VIEW [lekmer].[vCustomProductWithoutStatusFilterInclPackageProducts]
AS 
	SELECT
		p.*,
		lp.[Lekmer.BrandId],
		lp.[Lekmer.IsBookable],
		lp.[Lekmer.IsNewFrom],
		lp.[Lekmer.IsNewTo],
		lp.[Lekmer.CreatedDate],
		lp.[Lekmer.HasSizes],
		lp.[Lekmer.ShowVariantRelations],
		lp.[Lekmer.LekmerErpId],
		lp.[Lekmer.Weight],
		lp.[Lekmer.ProductTypeId],
		lp.[Lekmer.IsAbove60L],
		lp.[Lekmer.StockStatusId],
		lp.[Lekmer.MonitorThreshold],
		lp.[Lekmer.MaxQuantityPerOrder],
		[lp].[Lekmer.DeliveryTimeId],
		[lp].[Lekmer.SellOnlyInPackage],
		[lp].[Lekmer.IsDropShip],
		pu.[ProductUrl.UrlTitle] AS 'Lekmer.UrlTitle',
		pssr.[ParentContentNodeId] AS 'Product.ParentContentNodeId',
		i.*,
		[dt].*,
		rp.[Price] AS 'Lekmer.RecommendedPrice'
	FROM
		[lekmer].[vProductWithoutStatusFilter] p
		INNER JOIN [lekmer].[vLekmerProduct] lp ON p.[Product.Id] = lp.[Lekmer.ProductId]
		INNER JOIN [core].[vCustomChannel] AS c ON p.[Product.ChannelId] = c.[Channel.Id]
		INNER JOIN [lekmer].[vProductUrl] pu ON lp.[Lekmer.ProductId] = pu.[ProductUrl.ProductId] AND c.[Language.Id] = pu.[ProductUrl.LanguageId]
		LEFT JOIN [sitestructure].[tSiteStructureModuleChannel] AS ssmc ON ssmc.[ChannelId] = c.[Channel.Id]
		LEFT JOIN [product].[tProductSiteStructureRegistry] AS pssr	ON p.[Product.Id] = pssr.[ProductId] AND ssmc.[SiteStructureRegistryId] = pssr.[SiteStructureRegistryId]
		LEFT JOIN [media].[vCustomImage] AS i ON i.[Image.MediaId] = p.[Product.MediaId] AND i.[Image.LanguageId] = c.[Language.Id]
		LEFT JOIN [lekmer].[vRecommendedPrice] AS rp ON p.[Product.Id] = rp.[ProductId] AND c.[Channel.Id] = rp.[ChannelId]
		LEFT JOIN [productlek].[vDeliveryTime] dt ON [dt].[DeliveryTime.Id] = [lp].[Lekmer.DeliveryTimeId] AND [dt].[DeliveryTime.ChannelId] = [c].[Channel.Id]









GO
