SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view lekmer.vBlockProductFilterBrand
as
	select
		BlockId 'BlockProductFilterBrand.BlockId',
		BrandId 'BlockProductFilterBrand.BrandId'
	from
		lekmer.tBlockProductFilterBrand
GO
