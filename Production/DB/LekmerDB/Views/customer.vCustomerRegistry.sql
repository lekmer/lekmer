SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

Create VIEW [customer].[vCustomerRegistry]
AS
SELECT     
	CR.CustomerRegistryId AS 'CustomerRegistry.CustomerRegistryId',
	CR.Title AS 'CustomerRegistry.Title'
FROM
	[customer].[tCustomerRegistry] as CR

GO
