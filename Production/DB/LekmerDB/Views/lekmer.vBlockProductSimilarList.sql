
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [lekmer].[vBlockProductSimilarList]
AS
	SELECT
		[BlockId] as 'BlockProductSimilarList.BlockId',
		[PriceRangeType] as 'BlockProductSimilarList.PriceRangeType'
	FROM
		[lekmer].[tBlockProductSimilarList]

GO
