SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [orderlek].[vQliroPendingOrder]
AS
SELECT
	[po].[QliroPendingOrderId] AS 'QliroPendingOrder.QliroPendingOrderId',
	[po].[ChannelId] AS 'QliroPendingOrder.ChannelId',
	[po].[OrderId] AS 'QliroPendingOrder.OrderId',
	[po].[FirstAttempt] AS 'QliroPendingOrder.FirstAttempt',
	[po].[LastAttempt] AS 'QliroPendingOrder.LastAttempt'
FROM
	[orderlek].[tQliroPendingOrder] po


GO
