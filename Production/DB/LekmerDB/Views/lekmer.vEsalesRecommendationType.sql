SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vEsalesRecommendationType]
AS
	SELECT
		ert.[EsalesRecommendationTypeId] AS 'EsalesRecommendationType.Id',
		ert.[CommonName] AS 'EsalesRecommendationType.CommonName',
		ert.[Title] AS 'EsalesRecommendationType.Title'
	FROM
		[lekmer].[tEsalesRecommendationType] ert

GO
