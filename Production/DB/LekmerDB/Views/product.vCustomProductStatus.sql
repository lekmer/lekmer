SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [product].[vCustomProductStatus]
as
	select
		*
	from
		[product].[vProductStatus]
GO
