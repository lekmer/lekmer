SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vCartItemGroupFixedDiscountActionCurrency]
AS
	SELECT
		ci.[CartActionId]	AS 'CartItemGroupFixedDiscountActionCurrency.CartActionId',
		ci.[CurrencyId]		AS 'CartItemGroupFixedDiscountActionCurrency.CurrencyId',
		ci.[MonetaryValue]	AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[lekmer].[tCartItemGroupFixedDiscountActionCurrency] ci
		INNER JOIN [core].[vCustomCurrency] c ON ci.[CurrencyId] = c.[Currency.Id]


GO
