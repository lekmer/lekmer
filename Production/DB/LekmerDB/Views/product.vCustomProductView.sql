
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [product].[vCustomProductView]
AS
	SELECT
		[p].*
	FROM
		[product].[vCustomProductViewInclPackageProducts] p
	WHERE
		[p].[Lekmer.SellOnlyInPackage] = 0
GO
