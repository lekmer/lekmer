SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [esales].[vBlockEsalesTopSellersV2Secure]
AS
	SELECT
		[bets].[BlockId]				AS 'BlockEsalesTopSellersV2.BlockId',
		[bets].[PanelName]				AS 'BlockEsalesTopSellersV2.PanelName',
		[bets].[WindowLastEsalesValue]	AS 'BlockEsalesTopSellersV2.WindowLastEsalesValue',
		[bets].[IncludeAllCategories]	AS 'BlockEsalesTopSellersV2.IncludeAllCategories',
		[bets].[LinkContentNodeId]		AS 'BlockEsalesTopSellersV2.LinkContentNodeId',
		[bets].[CustomUrl]				AS 'BlockEsalesTopSellersV2.CustomUrl',
		[bets].[UrlTitle]				AS 'BlockEsalesTopSellersV2.UrlTitle',
		[b].*,
		[bs].*
	FROM
		[esales].[tBlockEsalesTopSellersV2] bets
		INNER JOIN [sitestructure].[vCustomBlockSecure] b ON [b].[Block.BlockId] = [bets].[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]
GO
