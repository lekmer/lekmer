
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [review].[vRatingItemProductVote]
AS
	SELECT
		ripv.[RatingReviewFeedbackId]	'RatingItemProductVote.RatingReviewFeedbackId',
		ripv.[RatingId]					'RatingItemProductVote.RatingId',
		ripv.[RatingItemId]				'RatingItemProductVote.RatingItemId',
		r.*,
		ri.*
	FROM 
		[review].[tRatingItemProductVote] ripv
		INNER JOIN [review].[vRatingReviewFeedback] rrf ON rrf.[RatingReviewFeedback.RatingReviewFeedbackId] = ripv.[RatingReviewFeedbackId]
		INNER JOIN [review].[vRating] r ON r.[Rating.RatingId] = ripv.[RatingId] AND r.[Rating.Channel.Id] = rrf.[RatingReviewFeedback.ChannelId]
		INNER JOIN [review].[vRatingItem] ri ON ri.[RatingItem.RatingItemId] = ripv.[RatingItemId] AND ri.[RatingItem.Channel.Id] = rrf.[RatingReviewFeedback.ChannelId]


GO
