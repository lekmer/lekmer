SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vCustomerForgotPassword]
AS
SELECT     
      CustomerId AS 'ForgotPassword.CustomerId', 
      [Guid] AS 'ForgotPassword.Guid', 
      CreatedDate AS 'ForgotPassword.CreatedDate'
FROM
      [lekmer].[tCustomerForgotPassword]



GO
