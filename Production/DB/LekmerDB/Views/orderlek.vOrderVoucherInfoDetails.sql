SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [orderlek].[vOrderVoucherInfoDetails]
AS
SELECT 
	[ovi].[OrderId] AS 'OrderVoucherInfo.OrderId',
	[ovi].[VoucherCode] AS 'OrderVoucherInfo.VoucherCode',
	[ovi].[DiscountType] AS 'OrderVoucherInfo.DiscountType',
	
	CASE [ovi].[DiscountType]
		WHEN 0 THEN 'Unknown'
		WHEN 1 THEN 'Percentage'
		WHEN 2 THEN 'Price'
		WHEN 3 THEN 'GiftCard'
		ELSE 'Unknown'
	END AS 'OrderVoucherInfo.DiscountType.Name',
	
	[ovi].[DiscountValue] AS 'OrderVoucherInfo.DiscountValue',
	[ovi].[AmountUsed] AS 'OrderVoucherInfo.AmountUsed',
	[ovi].[AmountLeft] AS 'OrderVoucherInfo.AmountLeft',
	[ovi].[VoucherStatus] AS 'OrderVoucherInfo.VoucherStatus',
	
	CASE [ovi].[VoucherStatus]
		WHEN 0 THEN 'Unknown'
		WHEN 1 THEN 'Reserved'
		WHEN 2 THEN 'Released'
		WHEN 3 THEN 'Consumed'
		ELSE 'Unknown'
	END AS 'OrderVoucherInfo.VoucherStatus.Name'
FROM
	[orderlek].[tOrderVoucherInfo] ovi

GO
