
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vLekmerChannel]
AS
SELECT
	[ChannelId]		AS [Channel.Id],
	[TimeFormat]	AS [Channel.TimeFormat],
	[WeekDayFormat]	AS [Channel.WeekDayFormat],
	[DayFormat]		AS [Channel.DayFormat],
	[DateTimeFormat]AS [Channel.DateTimeFormat],
	[TimeZoneDiff]	AS [Channel.TimeZoneDiff],
	[ErpId]			AS [Channel.ErpId],
	[VatPercentage]	AS [Channel.VatPercentage]
FROM
	[lekmer].[tLekmerChannel]

GO
