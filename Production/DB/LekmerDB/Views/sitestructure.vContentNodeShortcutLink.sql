SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [sitestructure].[vContentNodeShortcutLink]
AS
	select 
		cn.*,
		cnsl.[LinkContentNodeId] 'ContentNodeShortcutLink.LinkContentNodeId'
	from 
		[sitestructure].[tContentNodeShortcutLink] as cnsl
		inner join [sitestructure].[vCustomContentNode] as cn on cnsl.[ContentNodeId] = cn.[ContentNode.ContentNodeId]

GO
