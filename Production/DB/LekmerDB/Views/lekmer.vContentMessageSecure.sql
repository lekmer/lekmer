
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [lekmer].[vContentMessageSecure]
AS
	SELECT
		[cm].[ContentMessageId]				'ContentMessage.ContentMessageId',
		[cm].[ContentMessageFolderId]		'ContentMessage.ContentMessageFolderId',
		[cm].[CommonName]					'ContentMessage.CommonName',
		[cm].[Message]						'ContentMessage.Message',
		[cm].[IsDropShip]					'ContentMessage.IsDropShip',
		[cm].[StartDate]					'EntityTimeLimit.StartDate',
		[cm].[EndDate]						'EntityTimeLimit.EndDate',
		[cm].[StartDailyIntervalMinutes]	'EntityTimeLimit.StartDailyIntervalMinutes',
		[cm].[EndDailyIntervalMinutes]		'EntityTimeLimit.EndDailyIntervalMinutes'
	FROM 
		[lekmer].[tContentMessage] cm
GO
