SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentNodeSecure]
as
	select
		*
	from
		[sitestructure].[vContentNodeSecure]
GO
