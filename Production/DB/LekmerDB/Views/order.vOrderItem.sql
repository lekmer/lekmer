
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [order].[vOrderItem]
AS
SELECT
	[oi].[OrderItemId]					AS 'OrderItem.OrderItemId',
	[oi].[OrderId]						AS 'OrderItem.OrderId',
	[oi].[Quantity]						AS 'OrderItem.Quantity',
	[oi].[OriginalPriceIncludingVat]	AS 'OrderItem.OriginalPriceIncludingVat',
	[oi].[ActualPriceIncludingVat]		AS 'OrderItem.ActualPriceIncludingVat',
	[oi].[VAT]							AS 'OrderItem.VAT',
	[oip].[ProductId]					AS 'OrderItem.ProductId',
	[oip].[ErpId]						AS 'OrderItem.ErpId',
	[oip].[EanCode]						AS 'OrderItem.EanCode',
	[oip].[Title]						AS 'OrderItem.Title',
	[oip].[ProductTypeId]				AS 'OrderItem.ProductTypeId',
	[oip].[IsDropShip]					AS 'OrderItem.IsDropShip',
	[ois].*
FROM 
	[order].[tOrderItem] oi
	INNER JOIN [order].[tOrderItemProduct] oip ON [oi].[OrderItemId] = [oip].[OrderItemId]
	INNER JOIN [order].[vCustomOrderItemStatus] ois ON [oi].[OrderItemStatusId] = [ois].[OrderItemStatus.Id]
GO
