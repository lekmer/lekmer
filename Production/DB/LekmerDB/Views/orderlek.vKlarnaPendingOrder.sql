SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [orderlek].[vKlarnaPendingOrder]
AS
SELECT
	[po].[KlarnaPendingOrderId] AS 'KlarnaPendingOrder.KlarnaPendingOrderId',
	[po].[ChannelId] AS 'KlarnaPendingOrder.ChannelId',
	[po].[OrderId] AS 'KlarnaPendingOrder.OrderId',
	[po].[FirstAttempt] AS 'KlarnaPendingOrder.FirstAttempt',
	[po].[LastAttempt] AS 'KlarnaPendingOrder.LastAttempt'
FROM
	[orderlek].[tKlarnaPendingOrder] po

GO
