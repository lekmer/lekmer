
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [order].[vCustomCartItem]
AS
	SELECT
		c.*,
		l.SizeId AS [LekmerCartItem.SizeId],
		l.IsAffectedByCampaign AS [LekmerCartItem.IsAffectedByCampaign],
		[l].[IPAddress] AS  [LekmerCartItem.IPAddress]
	FROM
		[order].[vCartItem] c
		INNER JOIN [lekmer].[tLekmerCartItem] l ON c.[CartItem.CartItemId] = l.[CartItemId]

GO
