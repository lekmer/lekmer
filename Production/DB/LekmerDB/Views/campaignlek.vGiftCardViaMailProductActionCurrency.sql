SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vGiftCardViaMailProductActionCurrency]
AS
	SELECT
		gcpac.[ProductActionId] AS 'GiftCardViaMailProductActionCurrency.ProductActionId',
		gcpac.[CurrencyId] AS 'GiftCardViaMailProductActionCurrency.CurrencyId',
		gcpac.[Value] AS 'CurrencyValue.MonetaryValue',
		c.*
	FROM
		[campaignlek].[tGiftCardViaMailProductActionCurrency] gcpac
		INNER JOIN [core].[vCustomCurrency] c ON gcpac.[CurrencyId] = c.[Currency.Id]

GO
