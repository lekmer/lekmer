SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vBlockCategoryProductListCategory]
AS
	SELECT
		[BlockId] 'BlockCategory.BlockId'
		,[CategoryId] 'BlockCategory.CategoryId'
		,[IncludeSubcategories] 'BlockCategory.IncludeSubcategories'
	FROM
		[product].[tBlockCategoryProductListCategory]
GO
