SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [product].[vProductImageGroup]
AS
	SELECT 
		ProductImageGroupId 'ProductImageGroup.Id',
		Title 'ProductImageGroup.Title',
		CommonName 'ProductImageGroup.CommonName',
		MaxCount 'ProductImageGroup.MaxCount'
	FROM 
		product.tProductImageGroup
GO
