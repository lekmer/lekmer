SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomPercentagePriceDiscountAction]
as
	select
		*
	from
		[campaign].[vPercentagePriceDiscountAction]
GO
