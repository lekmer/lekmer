SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [sitestructure].[vCustomContentPageType]
as
	select
		*
	from
		[sitestructure].[vContentPageType]
GO
