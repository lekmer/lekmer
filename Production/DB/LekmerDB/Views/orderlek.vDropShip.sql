
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO




CREATE VIEW [orderlek].[vDropShip]
AS
SELECT
	[ds].[DropShipId] 'DropShip.Id',
	[ds].[ExternalOrderId] 'DropShip.ExternalOrderId',
	[ds].[OfferId] 'DropShip.OfferId',
	[ds].[Email] 'DropShip.Email',
	[ds].[Phone] 'DropShip.Phone',
	[ds].[SocialSecurityNumber] 'DropShip.SocialSecurityNumber',
	[ds].[BillingFirstName] 'DropShip.BillingFirstName',
	[ds].[BillingLastName] 'DropShip.BillingLastName',
	[ds].[BillingCompany] 'DropShip.BillingCompany',
	[ds].[BillingCo] 'DropShip.BillingCo',
	[ds].[BillingStreet] 'DropShip.BillingStreet',
	[ds].[BillingStreetNr] 'DropShip.BillingStreetNr',
	[ds].[BillingEntrance] 'DropShip.BillingEntrance',
	[ds].[BillingFloor] 'DropShip.BillingFloor',
	[ds].[BillingApartmentNumber] 'DropShip.BillingApartmentNumber',
	[ds].[BillingZip] 'DropShip.BillingZip',
	[ds].[BillingCity] 'DropShip.BillingCity',
	[ds].[BillingCountry] 'DropShip.BillingCountry',
	[ds].[DeliveryFirstName] 'DropShip.DeliveryFirstName',
	[ds].[DeliveryLastName] 'DropShip.DeliveryLastName',
	[ds].[DeliveryCompany] 'DropShip.DeliveryCompany',
	[ds].[DeliveryCo] 'DropShip.DeliveryCo',
	[ds].[DeliveryStreet] 'DropShip.DeliveryStreet',
	[ds].[DeliveryStreetNr] 'DropShip.DeliveryStreetNr',
	[ds].[DeliveryFloor] 'DropShip.DeliveryFloor',
	[ds].[DeliveryEntrance] 'DropShip.DeliveryEntrance',
	[ds].[DeliveryZip] 'DropShip.DeliveryZip',
	[ds].[DeliveryCity] 'DropShip.DeliveryCity',
	[ds].[DeliveryCountry] 'DropShip.DeliveryCountry',
	[ds].[Discount] 'DropShip.Discount',
	[ds].[Comment] 'DropShip.Comment',
	[ds].[SupplierNo] 'DropShip.SupplierNo',
	[ds].[OrderId] 'DropShip.OrderId',
	[ds].[OrderDate] 'DropShip.OrderDate',
	[ds].[CustomerNumber] 'DropShip.CustomerNumber',
	[ds].[NotificationNumber] 'DropShip.NotificationNumber',
	[ds].[PaymentMethod] 'DropShip.PaymentMethod',
	[ds].[ProductArticleNumber] 'DropShip.ProductArticleNumber',
	[ds].[ProductTitle] 'DropShip.ProductTitle',
	[ds].[Quantity] 'DropShip.Quantity',
	[ds].[Price] 'DropShip.Price',
	[ds].[ProductDiscount] 'DropShip.ProductDiscount',
	[ds].[ProductSoldSum] 'DropShip.ProductSoldSum',
	[ds].[ProductStatus] 'DropShip.ProductStatus',
	[ds].[InvoiceDate] 'DropShip.InvoiceDate',
	[ds].[VATproc] 'DropShip.VATproc',
	[ds].[CreatedDate] 'DropShip.CreatedDate',
	[ds].[SendDate] 'DropShip.SendDate',
	[ds].[CsvFileName] 'DropShip.CsvFileName',
	[ds].[PdfFileName] 'DropShip.PdfFileName'
FROM
	[orderlek].[tDropShip] ds



GO
