
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO






CREATE VIEW [esales].[vBlockEsalesRecommendV2]
AS
	SELECT
		[b].*,
		[ber].[PanelName] AS 'BlockEsalesRecommendV2.PanelName',
		[ber].[WindowLastEsalesValue] AS 'BlockEsalesRecommendV2.WindowLastEsalesValue',
		[bs].*
	FROM
		[esales].[tBlockEsalesRecommendV2] ber
		INNER JOIN [sitestructure].[vCustomBlock] b ON [b].[Block.BlockId] = [ber].[BlockId]
		INNER JOIN [sitestructurelek].[vBlockSetting] bs ON [bs].[BlockSetting.BlockId] = [b].[Block.BlockId]





GO
