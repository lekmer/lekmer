SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
create view campaign.vCartActionType
as
	select
		CartActionTypeId as 'CartActionType.Id',
		Title as 'CartActionType.Title',
		CommonName as 'CartActionType.CommonName'
	from
		campaign.tCartActionType
GO
