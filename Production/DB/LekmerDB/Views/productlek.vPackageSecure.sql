
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO



CREATE VIEW [productlek].[vPackageSecure]
AS
SELECT
	[p].[PackageId] 'Package.PackageId',
	[p].[MasterProductId] 'Package.MasterProductId',
	[p].[GeneralInfo] 'Package.GeneralInfo',
	[cpr].*
FROM
	[productlek].[tPackage] p
	INNER JOIN [product].[vCustomProductRecord] cpr ON [cpr].[Product.Id] = [p].[MasterProductId]

GO
