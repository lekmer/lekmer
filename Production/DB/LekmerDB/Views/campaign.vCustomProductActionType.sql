SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomProductActionType]
as
	select
		*
	from
		[campaign].[vProductActionType]
GO
