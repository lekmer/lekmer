SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [esales].[vAd]
AS
	SELECT
		[a].[AdId] 'Ad.AdId',
		[a].[FolderId] 'Ad.FolderId',
		[a].[Key] 'Ad.Key',
		[a].[Title] 'Ad.Title',
		[a].[DisplayFrom] 'Ad.DisplayFrom',
		[a].[DisplayTo] 'Ad.DisplayTo',
		[a].[ProductCategory] 'Ad.ProductCategory',
		[a].[Gender] 'Ad.Gender',
		[a].[Format] 'Ad.Format',
		[a].[SearchTags] 'Ad.SearchTags',
		[a].[CreatedDate] 'Ad.CreatedDate',
		[a].[UpdatedDate] 'Ad.UpdatedDate'
	FROM 
		[esales].[tAd] a

GO
