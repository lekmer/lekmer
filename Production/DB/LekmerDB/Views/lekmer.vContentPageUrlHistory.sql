
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE view [lekmer].[vContentPageUrlHistory]
AS
	SELECT
		[ContentPageUrlHistoryId] AS 'ContentPageUrlHistory.Id',
		[ContentPageId] AS 'ContentPageUrlHistory.ContentPageId',
		[UrlTitleOld] AS 'ContentPageUrlHistory.UrlTitleOld',
		[CreationDate] AS 'ContentPageUrlHistory.CreationDate',
		[LastUsageDate] AS 'ContentPageUrlHistory.LastUsageDate',
		[UsageAmount] AS 'ContentPageUrlHistory.UsageAmount',
		[HistoryLifeIntervalTypeId] AS 'ContentPageUrlHistory.HistoryLifeIntervalTypeId'
	FROM
		[lekmer].[tContentPageUrlHistory]

GO
