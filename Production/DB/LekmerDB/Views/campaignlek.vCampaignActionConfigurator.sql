SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaignlek].[vCampaignActionConfigurator]
AS
	SELECT
		[cac].[CampaignActionConfiguratorId] AS 'CampaignActionConfigurator.CampaignActionConfiguratorId',
		[cac].[IncludeAllProducts] AS 'CampaignActionConfigurator.IncludeAllProducts'
	FROM
		[campaignlek].[tCampaignActionConfigurator] cac
GO
