
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [campaign].[vCampaign]
AS
	SELECT
		[c].[CampaignId]			AS 'Campaign.Id',
		[c].[FolderId]				AS 'Campaign.FolderId',
		[c].[Title]					AS 'Campaign.Title',
		[c].[CampaignStatusId]		AS 'Campaign.CampaignStatusId',
		[c].[StartDate]				AS 'Campaign.StartDate',
		[c].[EndDate]				AS 'Campaign.EndDate',
		[c].[Priority]				AS 'Campaign.Priority',
		[c].[Exclusive]				AS 'Campaign.Exclusive',
		[c].[LevelId]				AS 'Campaign.LevelId',
		[cl].[Priority]				AS 'Campaign.LevelPriority',
		[c].[UseLandingPage]		AS 'Campaign.UseLandingPage',
		[c].[PriceTypeId]			AS 'Campaign.PriceTypeId',
		[c].[TagId]					AS 'Campaign.TagId',
		s.*
	FROM
		[campaign].[tCampaign] c
		INNER JOIN [campaign].[vCustomCampaignStatus] s ON [c].[CampaignStatusId] = [s].[CampaignStatus.Id]
		INNER JOIN [campaignlek].[tCampaignLevel] cl ON [cl].[LevelId] = [c].[LevelId]

GO
