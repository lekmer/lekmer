SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [addon].[vCartItemPriceAction]
AS 
	SELECT 
		p.MaxQuantity AS 'CartItemPriceAction.MaxQuantity',
		p.IncludeAllProducts AS 'CartItemPriceAction.IncludeAllProducts',
		a.*
	FROM 
		addon.tCartItemPriceAction p
		INNER JOIN campaign.vCustomCartAction a ON p.[CartActionId] = a.[CartAction.Id]

GO
