SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [template].[vCustomModelFragment]
as
	select
		*
	from
		[template].[vModelFragment]
GO
