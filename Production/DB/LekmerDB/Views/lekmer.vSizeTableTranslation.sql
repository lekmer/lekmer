
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [lekmer].[vSizeTableTranslation]
AS
SELECT 
	[SizeTableId] AS 'SizeTableTranslation.SizeTableId',
	[LanguageId] AS 'SizeTableTranslation.LanguageId',
	[Title] AS 'SizeTableTranslation.Title',
	[Description] AS 'SizeTableTranslation.Description',
	[Column1Title] AS 'SizeTableTranslation.Column1Title',
	[Column2Title] AS 'SizeTableTranslation.Column2Title'
FROM 
	[lekmer].[tSizeTableTranslation]
GO
