SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO







CREATE VIEW [lekmer].[vBlockProductFilter]
AS
	SELECT   
		[BlockId] AS 'BlockProductFilter.BlockId',
		SecondaryTemplateId AS 'BlockProductFilter.SecondaryTemplateId',
		DefaultCategoryId AS 'BlockProductFilter.DefaultCategoryId',
		DefaultPriceIntervalId AS 'BlockProductFilter.DefaultPriceIntervalId',
		DefaultAgeIntervalId AS 'BlockProductFilter.DefaultAgeIntervalId',
		DefaultSizeId AS 'BlockProductFilter.DefaultSizeId',
		ProductListCookie AS 'BlockProductFilter.ProductListCookie',
		DefaultSort AS 'BlockProductFilter.DefaultSort'
	FROM
		lekmer.[tBlockProductFilter]





GO
