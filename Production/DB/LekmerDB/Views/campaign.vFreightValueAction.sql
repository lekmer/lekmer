SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE VIEW [campaign].[vFreightValueAction]
as
	select
		a.*
	from
		campaign.tFreightValueAction fa
		inner join campaign.vCustomCartAction a on fa.CartActionId = a.[CartAction.Id]

GO
