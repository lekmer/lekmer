SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [security].[vPrivilege]
AS
select
	[PrivilegeId] 'Privilege.Id',
	[CommonName] 'Privilege.CommonName',
	[Title] 'Privilege.Title'
from
	[security].[tPrivilege]
GO
