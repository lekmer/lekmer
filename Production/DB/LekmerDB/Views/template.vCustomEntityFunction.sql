SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE view [template].[vCustomEntityFunction]
as
	select
		*
	from
		[template].[vEntityFunction]
GO
