SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO


CREATE VIEW [lekmer].[vSizeTableMedia]
AS
	SELECT
		[stm].[SizeTableMediaId] AS 'SizeTableMedia.SizeTableMediaId',
		[stm].[SizeTableId] AS 'SizeTableMedia.SizeTableId',
		[stm].[MediaId] AS 'SizeTableMedia.MediaId'
	FROM 
		[lekmer].[tSizeTableMedia] stm

GO
