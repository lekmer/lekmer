
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO





CREATE VIEW [productlek].[vDeliveryTimeSecure]
AS
SELECT     
      [DeliveryTimeId] AS 'DeliveryTime.Id',
      NULL AS 'DeliveryTime.From',
      NULL AS 'DeliveryTime.To',
      NULL AS 'DeliveryTime.Format',
      NULL AS 'DeliveryTime.ProductRegistryId',
	  NULL AS 'DeliveryTime.ProductRegistryChannelId'
FROM
      [productlek].[tDeliveryTime]




GO
