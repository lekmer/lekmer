SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [productlek].[vRecommenderList]
AS
	SELECT
		[RecommenderListId] AS 'RecommenderList.RecommenderListId',
		[SessionId] AS 'RecommenderList.SessionId',
		[CreatedDate] AS 'RecommenderList.CreatedDate'
	FROM
		[productlek].[tRecommenderList]

GO
