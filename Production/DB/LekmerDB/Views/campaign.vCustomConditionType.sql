SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

create view [campaign].[vCustomConditionType]
as
	select
		*
	from
		[campaign].[vConditionType]
GO
