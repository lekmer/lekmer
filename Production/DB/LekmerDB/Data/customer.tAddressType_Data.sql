SET IDENTITY_INSERT [customer].[tAddressType] ON
INSERT INTO [customer].[tAddressType] ([AddressTypeId], [Title], [CommonName]) VALUES (1, N'Billing address', N'BillingAddress')
INSERT INTO [customer].[tAddressType] ([AddressTypeId], [Title], [CommonName]) VALUES (2, N'Delivery address', N'DeliveryAddress')
INSERT INTO [customer].[tAddressType] ([AddressTypeId], [Title], [CommonName]) VALUES (1000001, N'Alternate address', N'AlternateAddress')
SET IDENTITY_INSERT [customer].[tAddressType] OFF
