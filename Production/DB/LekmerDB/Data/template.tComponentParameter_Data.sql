SET IDENTITY_INSERT [template].[tComponentParameter] ON
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000011, 1000006, N'TemplateId', 0, N'Sets the identifier of the component template.')
SET IDENTITY_INSERT [template].[tComponentParameter] OFF
SET IDENTITY_INSERT [template].[tComponentParameter] ON
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (9, 1, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (10, 2, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (11, 3, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (12, 55, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (13, 143, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (14, 145, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (15, 1, N'StartFromId', 0, N'Sets the root content node by identifier.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (16, 2, N'StartFromId', 0, N'Sets the root content node by identifier.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (17, 1, N'StartFromCommonName', 0, N'Sets the root content node by common name.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (18, 2, N'StartFromCommonName', 0, N'Sets the root content node by common name.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (19, 1, N'StartFromLevel', 0, N'Sets the start level value.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (20, 2, N'StartFromLevel', 0, N'Sets the start level value.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (21, 2, N'AlwaysExpanded', 0, N'Defines if nodes should always be expanded.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000002, 1000004, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000003, 1000005, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000004, 1000003, N'TemplateId', 0, N'Sets the identifier of the component template.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000005, 1000003, N'ColumnCount', 0, N'Sets the columns count value.')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000006, 1000003, N'RowCount', 0, N'Sets the rows count value')
INSERT INTO [template].[tComponentParameter] ([ComponentParameterId], [ComponentId], [Title], [DataType], [Description]) VALUES (1000007, 1, N'MaxLinkTextLength', 0, N'Limit for long link texts. Delimits them with ...')
SET IDENTITY_INSERT [template].[tComponentParameter] OFF
