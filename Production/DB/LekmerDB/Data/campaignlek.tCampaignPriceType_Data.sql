INSERT INTO [campaignlek].[tCampaignPriceType] ([CampaignPriceTypeId], [Title], [CommonName]) VALUES (1, N'Outlet price', N'OutletPrice')
INSERT INTO [campaignlek].[tCampaignPriceType] ([CampaignPriceTypeId], [Title], [CommonName]) VALUES (2, N'Campaign price', N'CampaignPrice')
INSERT INTO [campaignlek].[tCampaignPriceType] ([CampaignPriceTypeId], [Title], [CommonName]) VALUES (3, N'Lowered price', N'LoweredPrice')
