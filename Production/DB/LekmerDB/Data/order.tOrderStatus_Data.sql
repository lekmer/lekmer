INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (11, N'Payment timeout', N'PaymentTimeout')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (1, N'Payment pending', N'PaymentPending')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (2, N'Payment confirmed', N'PaymentConfirmed')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (3, N'Canceled', N'Canceled')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (4, N'Order in HY', N'OrderInHY')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (5, N'Error moving order into HY', N'OrderError')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (6, N'Order row limit exceeded', N'OrderRowItemOverflow')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (7, N'Rejected By Payment Provider', N'RejectedByPaymentProvider')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (8, N'KCO deleted', N'KCODeleted')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (9, N'KCO payment pending', N'KCOPaymentPending')
INSERT INTO [order].[tOrderStatus] ([OrderStatusId], [Title], [CommonName]) VALUES (10, N'Payment on hold', N'PaymentOnHold')
