SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE function [statistics].fnGetDateTimeKey
(
	@Year	smallint,
	@Month	tinyint,
	@Day	tinyint,
	@Hour	tinyint,
	@Mode	varchar(10)
)
RETURNS smalldatetime
AS 
BEGIN 
	RETURN  (cast(CONVERT(varchar(4), @Year)+'-'
		+CONVERT(varchar(2), @Month)+'-'
		+CONVERT(varchar(2), (CASE WHEN @Mode <> 'Year' THEN @Day ELSE 1 END))+' '
		+CONVERT(varchar(2), (CASE WHEN @Mode = 'Day' THEN @Hour ELSE 0 END))+':00:00' 
		AS smalldatetime))
END
GO
