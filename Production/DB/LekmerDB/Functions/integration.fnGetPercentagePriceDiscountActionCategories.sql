SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE FUNCTION [integration].[fnGetPercentagePriceDiscountActionCategories](
	@ProductActionId INT,
	@IsIncludeCategories BIT
)
RETURNS @tCategories TABLE (CategoryId INT)
AS 
BEGIN
	IF @IsIncludeCategories = 1
	BEGIN
		INSERT @tCategories ([CategoryId])
		SELECT 
			[ic].[CategoryId] 
		FROM 
			[campaignlek].[tCampaignActionIncludeCategory] ic
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ic].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
		UNION
		SELECT 
			[sc].[CategoryId] 
		FROM 
			[campaignlek].[tCampaignActionIncludeCategory] ic
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ic].[ConfigId]
			CROSS APPLY [product].[fnGetSubCategories] ([ic].[CategoryId]) sc
		WHERE
			a.ProductActionId = @ProductActionId
			AND [ic].[IncludeSubCategories] = 1
	END
	ELSE
	BEGIN
		INSERT @tCategories ([CategoryId])
		SELECT 
			[ec].[CategoryId] 
		FROM 
			[campaignlek].[tCampaignActionExcludeCategory] ec
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ec].[ConfigId]
		WHERE
			a.ProductActionId = @ProductActionId
		UNION
		SELECT 
			[sc].[CategoryId] 
		FROM 
			[campaignlek].[tCampaignActionExcludeCategory] ec
			INNER JOIN [campaign].[tPercentagePriceDiscountAction] a ON [a].[ConfigId] = [ec].[ConfigId]
			CROSS APPLY [product].[fnGetSubCategories] ([ec].[CategoryId]) sc
		WHERE
			a.ProductActionId = @ProductActionId
			AND [ec].[IncludeSubCategories] = 1
	END
	
	RETURN
END
GO
