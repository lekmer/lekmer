SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO
CREATE function [product].[fnGetCurrencyAndPriceListRegistry](
	@ProductId int,
	@ChannelId int
)
RETURNS @tPriceListData TABLE (CurrencyId int, PriceListRegistryId int)
AS 
BEGIN
	DECLARE @CurrencyId int
	SELECT @CurrencyId = CurrencyId FROM core.tChannel WHERE ChannelId = @ChannelId
	
	INSERT INTO @tPriceListData (CurrencyId, PriceListRegistryId)
	SELECT @CurrencyId, PMC.PriceListRegistryId 
	FROM product.tProductRegistryProduct AS PRP INNER JOIN product.tProductModuleChannel AS PMC ON 
			PRP.ProductRegistryId = PMC.ProductRegistryId AND PMC.ChannelId = @ChannelId AND PRP.ProductId = @ProductId	
	RETURN 
END 
GO
