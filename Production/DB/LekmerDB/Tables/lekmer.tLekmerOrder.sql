CREATE TABLE [lekmer].[tLekmerOrder]
(
[OrderId] [int] NOT NULL,
[PaymentCost] [decimal] (16, 2) NOT NULL,
[CustomerIdentificationKey] [nchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[FeedbackToken] [uniqueidentifier] NULL,
[AlternateAddressId] [int] NULL,
[NeedSendInsuranceInfo] [bit] NOT NULL CONSTRAINT [DF_tLekmerOrder_NeedSendInsuranceInfo] DEFAULT ((1)),
[CivicNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[OptionalDeliveryMethodId] [int] NULL,
[OptionalFreightCost] [decimal] (16, 2) NULL,
[DiapersDeliveryMethodId] [int] NULL,
[DiapersFreightCost] [decimal] (16, 2) NULL,
[KcoId] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[UserAgent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tLekmerOrder] ADD
CONSTRAINT [FK_tLekmerOrder_tOrderAddress] FOREIGN KEY ([AlternateAddressId]) REFERENCES [order].[tOrderAddress] ([OrderAddressId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD CONSTRAINT [PK_tLekmerOrder] PRIMARY KEY CLUSTERED  ([OrderId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerOrder] ADD CONSTRAINT [FK_tLekmerOrder_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
GO
