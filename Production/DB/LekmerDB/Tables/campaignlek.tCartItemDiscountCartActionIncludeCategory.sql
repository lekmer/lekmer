CREATE TABLE [campaignlek].[tCartItemDiscountCartActionIncludeCategory]
(
[CartActionId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionIncludeCategory] ADD CONSTRAINT [PK_tCartItemDiscountCartActionIncludeCategory] PRIMARY KEY CLUSTERED  ([CartActionId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionIncludeCategory] ADD CONSTRAINT [FK_tCartItemDiscountCartActionIncludeCategory_tCartItemDiscountCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaignlek].[tCartItemDiscountCartAction] ([CartActionId]) ON DELETE CASCADE
GO
ALTER TABLE [campaignlek].[tCartItemDiscountCartActionIncludeCategory] ADD CONSTRAINT [FK_tCartItemDiscountCartActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId]) ON DELETE CASCADE
GO
