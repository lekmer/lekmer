CREATE TABLE [lekmer].[tSizeTableIncludeCategoryBrand]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[SizeTableId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[BrandId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategoryBrand] ADD CONSTRAINT [PK_tSizeTableIncludeCategoryBrand] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategoryBrand] ADD CONSTRAINT [FK_tSizeTableIncludeCategoryBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategoryBrand] ADD CONSTRAINT [FK_tSizeTableIncludeCategoryBrand_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [lekmer].[tSizeTableIncludeCategoryBrand] ADD CONSTRAINT [FK_tSizeTableIncludeCategoryBrand_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId])
GO
