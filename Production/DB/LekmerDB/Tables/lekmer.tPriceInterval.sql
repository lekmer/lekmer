CREATE TABLE [lekmer].[tPriceInterval]
(
[PriceIntervalId] [int] NOT NULL IDENTITY(1, 1),
[From] [decimal] (16, 2) NOT NULL,
[To] [decimal] (16, 2) NOT NULL,
[Title] [nvarchar] (255) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CurrencyId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tPriceInterval] ADD CONSTRAINT [PK_tPriceInterval] PRIMARY KEY CLUSTERED  ([PriceIntervalId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tPriceInterval] ON [lekmer].[tPriceInterval] ([CurrencyId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tPriceInterval] ADD CONSTRAINT [FK_tPriceInterval_tCurrency] FOREIGN KEY ([CurrencyId]) REFERENCES [core].[tCurrency] ([CurrencyId])
GO
