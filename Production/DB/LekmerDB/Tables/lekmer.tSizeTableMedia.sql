CREATE TABLE [lekmer].[tSizeTableMedia]
(
[SizeTableMediaId] [int] NOT NULL IDENTITY(1, 1),
[SizeTableId] [int] NOT NULL,
[MediaId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTableMedia] ADD 
CONSTRAINT [PK_tSizeTableMedia] PRIMARY KEY CLUSTERED  ([SizeTableMediaId]) ON [PRIMARY]
ALTER TABLE [lekmer].[tSizeTableMedia] ADD
CONSTRAINT [FK_tSizeTableMedia_tSizeTable] FOREIGN KEY ([SizeTableId]) REFERENCES [lekmer].[tSizeTable] ([SizeTableId]) ON DELETE CASCADE
ALTER TABLE [lekmer].[tSizeTableMedia] ADD
CONSTRAINT [FK_tSizeTableMedia_tMedia] FOREIGN KEY ([MediaId]) REFERENCES [media].[tMedia] ([MediaId]) ON DELETE CASCADE
GO
