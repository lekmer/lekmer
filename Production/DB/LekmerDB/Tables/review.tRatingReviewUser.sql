CREATE TABLE [review].[tRatingReviewUser]
(
[RatingReviewUserId] [int] NOT NULL IDENTITY(1, 1),
[CustomerId] [int] NULL,
[Token] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingReviewUser] ADD CONSTRAINT [PK_tRatingReviewUser] PRIMARY KEY CLUSTERED  ([RatingReviewUserId]) ON [PRIMARY]
GO
ALTER TABLE [review].[tRatingReviewUser] ADD CONSTRAINT [FK_tRatingReviewUser_tCustomer] FOREIGN KEY ([CustomerId]) REFERENCES [customer].[tCustomer] ([CustomerId])
GO
