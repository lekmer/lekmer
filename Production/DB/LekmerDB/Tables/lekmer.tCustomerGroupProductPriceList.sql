CREATE TABLE [lekmer].[tCustomerGroupProductPriceList]
(
[PriceListId] [int] NOT NULL,
[PriceListRegistryId] [int] NOT NULL,
[CurrencyId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[CustomerGroupId] [int] NULL,
[PriceIncludingVat] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
