CREATE TABLE [lekmer].[tSizeTableFolder]
(
[SizeTableFolderId] [int] NOT NULL IDENTITY(1, 1),
[ParentSizeTableFolderId] [int] NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableFolder] ADD CONSTRAINT [PK_tSizeTableFolder] PRIMARY KEY CLUSTERED  ([SizeTableFolderId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableFolder] ADD CONSTRAINT [FK_tSizeTableFolder_tSizeTableFolder] FOREIGN KEY ([ParentSizeTableFolderId]) REFERENCES [lekmer].[tSizeTableFolder] ([SizeTableFolderId])
GO
