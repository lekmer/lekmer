CREATE TABLE [lekmer].[tSizeTableMediaProductRegistry]
(
[SizeTableMediaProductRegistryId] [int] NOT NULL IDENTITY(1, 1),
[SizeTableMediaId] [int] NOT NULL,
[ProductRegistryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableMediaProductRegistry] ADD CONSTRAINT [PK_tSizeTableMediaProductRegistry] PRIMARY KEY CLUSTERED  ([SizeTableMediaProductRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableMediaProductRegistry] ADD CONSTRAINT [IX_tSizeTableMediaProductRegistry_SizeTableMediaId_ProductRegistryId] UNIQUE NONCLUSTERED  ([SizeTableMediaId], [ProductRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tSizeTableMediaProductRegistry] ADD CONSTRAINT [FK_tSizeTableMediaProductRegistry_tProductRegistry] FOREIGN KEY ([ProductRegistryId]) REFERENCES [product].[tProductRegistry] ([ProductRegistryId])
GO
ALTER TABLE [lekmer].[tSizeTableMediaProductRegistry] ADD CONSTRAINT [FK_tSizeTableMediaProductRegistry_tSizeTableMedia] FOREIGN KEY ([SizeTableMediaId]) REFERENCES [lekmer].[tSizeTableMedia] ([SizeTableMediaId])
GO
