CREATE TABLE [orderlek].[tQliroPaymentType]
(
[QliroPaymentTypeId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Code] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Description] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[RegistrationFee] [decimal] (16, 2) NOT NULL,
[SettlementFee] [decimal] (16, 2) NOT NULL,
[InterestRate] [decimal] (16, 2) NOT NULL,
[InterestType] [int] NOT NULL,
[InterestCalculation] [int] NOT NULL,
[NoOfMonths] [int] NOT NULL,
[MinPurchaseAmount] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tQliroPaymentType] ADD CONSTRAINT [PK_tQliroPaymentType] PRIMARY KEY CLUSTERED  ([QliroPaymentTypeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_tQliroPaymentType_ChannelId_Code] ON [orderlek].[tQliroPaymentType] ([ChannelId], [Code]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tQliroPaymentType] ADD CONSTRAINT [FK_tQliroPaymentType_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
