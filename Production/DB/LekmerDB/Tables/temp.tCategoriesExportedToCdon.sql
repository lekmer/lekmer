CREATE TABLE [temp].[tCategoriesExportedToCdon]
(
[CategoryId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [temp].[tCategoriesExportedToCdon] ADD CONSTRAINT [PK__tCategor__19093A0B752E08D8] PRIMARY KEY CLUSTERED  ([CategoryId]) ON [PRIMARY]
GO
