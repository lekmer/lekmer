CREATE TABLE [campaignlek].[tGiftCardViaMailCartAction]
(
[CartActionId] [int] NOT NULL,
[SendingInterval] [int] NOT NULL,
[TemplateId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [campaignlek].[tGiftCardViaMailCartAction] ADD
CONSTRAINT [FK_tGiftCardViaMailCartAction_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartAction] ADD CONSTRAINT [PK_tGiftCardViaMailCartAction] PRIMARY KEY CLUSTERED  ([CartActionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tGiftCardViaMailCartAction] ADD CONSTRAINT [FK_GiftCardViaMailCartAction_tCartAction] FOREIGN KEY ([CartActionId]) REFERENCES [campaign].[tCartAction] ([CartActionId])
GO
