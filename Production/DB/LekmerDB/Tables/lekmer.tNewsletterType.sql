CREATE TABLE [lekmer].[tNewsletterType]
(
[NewsletterTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tNewsletterType] ADD CONSTRAINT [PK_tNewsletterType] PRIMARY KEY CLUSTERED  ([NewsletterTypeId]) ON [PRIMARY]
GO
