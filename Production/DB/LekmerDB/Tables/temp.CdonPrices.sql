CREATE TABLE [temp].[CdonPrices]
(
[ChannelId] [int] NULL,
[ProductId] [int] NOT NULL,
[PriceListId] [int] NOT NULL,
[Price] [decimal] (16, 2) NOT NULL,
[Vat] [decimal] (17, 2) NULL,
[DiscountPrice] [decimal] (18, 2) NULL,
[DiscountPriceVat] [decimal] (38, 7) NULL
) ON [PRIMARY]
GO
