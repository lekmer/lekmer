CREATE TABLE [campaignlek].[tCampaignActionIncludeCategory]
(
[ConfigId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubCategories] [bit] NOT NULL CONSTRAINT [DF_tCampaignActionIncludeCategory_IncludeSubCategories] DEFAULT ((0))
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeCategory] ADD CONSTRAINT [PK_tCampaignActionIncludeCategory] PRIMARY KEY CLUSTERED  ([ConfigId], [CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeCategory] ADD CONSTRAINT [FK_tCampaignActionIncludeCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
ALTER TABLE [campaignlek].[tCampaignActionIncludeCategory] ADD CONSTRAINT [FK_tCampaignActionIncludeCategory_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
