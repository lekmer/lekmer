CREATE TABLE [lekmer].[tLekmerCategory]
(
[CategoryId] [int] NOT NULL,
[AllowMultipleSizesPurchase] [bit] NULL CONSTRAINT [DF_tLekmerCategory_AllowMultipleSizesPurchase] DEFAULT ((0)),
[PackageInfo] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[MonitorThreshold] [int] NULL,
[MaxQuantityPerOrder] [int] NULL,
[DeliveryTimeId] [int] NULL,
[MinQuantityInStock] [int] NULL CONSTRAINT [DF_tLekmerCategory_MinQuantityInStock] DEFAULT ((1))
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [lekmer].[tLekmerCategory] ADD
CONSTRAINT [FK_tLekmerCategory_tDeliveryTime] FOREIGN KEY ([DeliveryTimeId]) REFERENCES [productlek].[tDeliveryTime] ([DeliveryTimeId])
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD CONSTRAINT [PK_tLekmerCategory] PRIMARY KEY CLUSTERED  ([CategoryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerCategory] ADD CONSTRAINT [FK_tLekmerCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
