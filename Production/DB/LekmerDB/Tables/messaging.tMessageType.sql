CREATE TABLE [messaging].[tMessageType]
(
[MessageTypeId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [messaging].[tMessageType] ADD CONSTRAINT [PK_tMessageType] PRIMARY KEY NONCLUSTERED  ([MessageTypeId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UQ_tMessageType_CommonName] ON [messaging].[tMessageType] ([CommonName]) ON [PRIMARY]
GO
