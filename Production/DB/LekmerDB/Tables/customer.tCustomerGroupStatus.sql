CREATE TABLE [customer].[tCustomerGroupStatus]
(
[CustomerGroupStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerGroupStatus] ADD CONSTRAINT [PK_tCustomerGroupStatus] PRIMARY KEY CLUSTERED  ([CustomerGroupStatusId]) ON [PRIMARY]
GO
