CREATE TABLE [productlek].[tTagFlag]
(
[TagId] [int] NOT NULL,
[FlagId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tTagFlag] ADD CONSTRAINT [PK_tTagFlag] PRIMARY KEY CLUSTERED  ([TagId], [FlagId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tTagFlag] ADD CONSTRAINT [FK_tTagFlag_tFlag] FOREIGN KEY ([FlagId]) REFERENCES [lekmer].[tFlag] ([FlagId]) ON DELETE CASCADE
GO
ALTER TABLE [productlek].[tTagFlag] ADD CONSTRAINT [FK_tTagFlag_tTag] FOREIGN KEY ([TagId]) REFERENCES [lekmer].[tTag] ([TagId]) ON DELETE CASCADE
GO
