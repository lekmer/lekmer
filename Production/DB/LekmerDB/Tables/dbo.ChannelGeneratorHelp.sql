CREATE TABLE [dbo].[ChannelGeneratorHelp]
(
[SitestructureRegistry] [int] NULL,
[SystemPagesRootId] [int] NULL,
[MasterPagesRootId] [int] NULL,
[PriceRootContenNodeId] [int] NULL,
[AgeRootContentNodeId] [int] NULL,
[ThemeRootContentNodeId] [int] NULL,
[ContenMasterPageId] [int] NULL,
[HomePageId] [int] NULL,
[SalePageId] [int] NULL,
[CustomerPagesRootId] [int] NULL,
[OrderPagesRootId] [int] NULL
) ON [PRIMARY]
GO
