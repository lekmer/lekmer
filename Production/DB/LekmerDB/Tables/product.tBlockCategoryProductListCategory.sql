CREATE TABLE [product].[tBlockCategoryProductListCategory]
(
[BlockId] [int] NOT NULL,
[CategoryId] [int] NOT NULL,
[IncludeSubcategories] [bit] NOT NULL
) ON [PRIMARY]
ALTER TABLE [product].[tBlockCategoryProductListCategory] ADD
CONSTRAINT [FK_tBlockCategoryProductListCategory_tBlockCategoryProductList] FOREIGN KEY ([BlockId]) REFERENCES [productlek].[tBlockCategoryProductList] ([BlockId])
GO
ALTER TABLE [product].[tBlockCategoryProductListCategory] ADD CONSTRAINT [PK_tBlockCategoryProductListCategory] PRIMARY KEY CLUSTERED  ([BlockId], [CategoryId]) ON [PRIMARY]
GO

ALTER TABLE [product].[tBlockCategoryProductListCategory] ADD CONSTRAINT [FK_tBlockCategoryProductListCategory_tCategory] FOREIGN KEY ([CategoryId]) REFERENCES [product].[tCategory] ([CategoryId])
GO
