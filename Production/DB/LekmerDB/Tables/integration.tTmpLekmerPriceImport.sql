CREATE TABLE [integration].[tTmpLekmerPriceImport]
(
[HyErpId] [nvarchar] (15) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[PriceIncludingVat] [decimal] (16, 2) NULL,
[ChannelId] [int] NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tTmpLekmerPriceImport] ADD 
CONSTRAINT [PK__tTmpLekm__6F1E91DE44145662] PRIMARY KEY CLUSTERED  ([HyErpId]) WITH (IGNORE_DUP_KEY=ON) ON [PRIMARY]
GO
