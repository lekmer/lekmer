CREATE TABLE [order].[tOrderAddress]
(
[OrderAddressId] [int] NOT NULL IDENTITY(1, 1),
[Addressee] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StreetAddress] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[StreetAddress2] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[PostalCode] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[City] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CountryId] [int] NOT NULL,
[PhoneNumber] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderAddress] ADD CONSTRAINT [PK_tOrderAddress] PRIMARY KEY CLUSTERED  ([OrderAddressId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tOrderAddress_CountryId] ON [order].[tOrderAddress] ([CountryId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tOrderAddress] ADD CONSTRAINT [FK_tOrderAddress_tCountry] FOREIGN KEY ([CountryId]) REFERENCES [core].[tCountry] ([CountryId])
GO
