CREATE TABLE [lekmer].[tLekmerCartItem]
(
[CartItemId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[SizeId] [int] NULL,
[IsAffectedByCampaign] [bit] NOT NULL CONSTRAINT [DF_tLekmerCartItem_IsAffectedByCampaign] DEFAULT ((0)),
[IPAddress] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tLekmerCartItem] ADD
CONSTRAINT [FK_tLekmerCartItem_tProductSize] FOREIGN KEY ([ProductId], [SizeId]) REFERENCES [lekmer].[tProductSize] ([ProductId], [SizeId])
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [PK_tLekmerCartItem] PRIMARY KEY CLUSTERED  ([CartItemId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tLekmerCartItem] ADD CONSTRAINT [FK_tLekmerCartItem_tCartItem] FOREIGN KEY ([CartItemId]) REFERENCES [order].[tCartItem] ([CartItemId])
GO
