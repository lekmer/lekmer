CREATE TABLE [esales].[tEsalesModel]
(
[ModelId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModel] ADD CONSTRAINT [PK_tEsalesModel] PRIMARY KEY CLUSTERED  ([ModelId]) ON [PRIMARY]
GO
