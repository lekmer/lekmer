CREATE TABLE [productlek].[tProductChangeEvent]
(
[ProductChangeEventId] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[EventStatusId] [int] NOT NULL,
[CdonExportEventStatusId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL,
[ActionAppliedDate] [datetime] NULL,
[Reference] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY]
ALTER TABLE [productlek].[tProductChangeEvent] ADD
CONSTRAINT [FK_tProductChangeEvent_tProductEventStatus_CdonExport] FOREIGN KEY ([CdonExportEventStatusId]) REFERENCES [productlek].[tProductEventStatus] ([ProductEventStatusId])
GO
ALTER TABLE [productlek].[tProductChangeEvent] ADD CONSTRAINT [PK_tProductChangeEvent] PRIMARY KEY CLUSTERED  ([ProductChangeEventId]) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tProductChangeEvent] ADD CONSTRAINT [FK_tProductChangeEvent_tProductEventStatus] FOREIGN KEY ([EventStatusId]) REFERENCES [productlek].[tProductEventStatus] ([ProductEventStatusId])
GO
