CREATE TABLE [orderlek].[tPackageOrderItem]
(
[PackageOrderItemId] [int] NOT NULL IDENTITY(1, 1),
[OrderItemId] [int] NOT NULL,
[OrderId] [int] NOT NULL,
[Quantity] [int] NOT NULL,
[PackagePriceIncludingVat] [decimal] (16, 2) NOT NULL,
[ActualPriceIncludingVat] [decimal] (16, 2) NOT NULL,
[OriginalPriceIncludingVat] [decimal] (16, 2) NOT NULL,
[VAT] [decimal] (16, 2) NOT NULL,
[OrderItemStatusId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [orderlek].[tPackageOrderItem] ADD 
CONSTRAINT [PK_tPackageOrderItem] PRIMARY KEY CLUSTERED  ([PackageOrderItemId]) ON [PRIMARY]
ALTER TABLE [orderlek].[tPackageOrderItem] ADD
CONSTRAINT [CK_tPackageOrderItem_Quantity] CHECK (([Quantity]>(0)))

ALTER TABLE [orderlek].[tPackageOrderItem] ADD
CONSTRAINT [FK_tPackageOrderItem_tOrderItem] FOREIGN KEY ([OrderItemId]) REFERENCES [order].[tOrderItem] ([OrderItemId])
ALTER TABLE [orderlek].[tPackageOrderItem] ADD
CONSTRAINT [FK_tPackageOrderItem_tOrder] FOREIGN KEY ([OrderId]) REFERENCES [order].[tOrder] ([OrderId])
ALTER TABLE [orderlek].[tPackageOrderItem] ADD
CONSTRAINT [FK_tPackageOrderItem_tOrderItemStatus] FOREIGN KEY ([OrderItemStatusId]) REFERENCES [order].[tOrderItemStatus] ([OrderItemStatusId])
GO
