CREATE TABLE [lekmer].[tNewsletterUnsubscriber]
(
[UnsubscriberId] [int] NOT NULL IDENTITY(1, 1),
[ChannelId] [int] NOT NULL,
[Email] [varchar] (320) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[SentStatus] [bit] NULL,
[CreatedDate] [datetime] NOT NULL,
[UpdatedDate] [datetime] NOT NULL
) ON [PRIMARY]
CREATE NONCLUSTERED INDEX [IX_tNewsletterUnsubscriber_ChannelId] ON [lekmer].[tNewsletterUnsubscriber] ([ChannelId]) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [IX_tNewsletterUnsubscriber_Email] ON [lekmer].[tNewsletterUnsubscriber] ([Email]) ON [PRIMARY]

ALTER TABLE [lekmer].[tNewsletterUnsubscriber] ADD 
CONSTRAINT [PK_tNewsletterUnsubscriber] PRIMARY KEY CLUSTERED  ([UnsubscriberId]) ON [PRIMARY]
ALTER TABLE [lekmer].[tNewsletterUnsubscriber] ADD
CONSTRAINT [FK_tNewsletterUnsubscriber_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])


GO
