CREATE TABLE [lekmer].[tCampaignFlag]
(
[CampaignId] [int] NOT NULL,
[FlagId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCampaignFlag] ADD CONSTRAINT [PK_tCampaignFlag] PRIMARY KEY CLUSTERED  ([CampaignId], [FlagId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tCampaignFlag] ADD CONSTRAINT [FK_tCampaignFlag_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tCampaignFlag] ADD CONSTRAINT [FK_tCampaignFlag_tFlag] FOREIGN KEY ([FlagId]) REFERENCES [lekmer].[tFlag] ([FlagId]) ON DELETE CASCADE
GO
