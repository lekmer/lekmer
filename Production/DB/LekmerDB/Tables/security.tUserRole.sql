CREATE TABLE [security].[tUserRole]
(
[SystemUserId] [int] NOT NULL,
[RoleId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [security].[tUserRole] ADD CONSTRAINT [PK_tUserRole] PRIMARY KEY CLUSTERED  ([SystemUserId], [RoleId]) ON [PRIMARY]
GO
ALTER TABLE [security].[tUserRole] ADD CONSTRAINT [FK_tUserRole_tRole] FOREIGN KEY ([RoleId]) REFERENCES [security].[tRole] ([RoleId])
GO
ALTER TABLE [security].[tUserRole] ADD CONSTRAINT [FK_tUserRole_tSystemUser] FOREIGN KEY ([SystemUserId]) REFERENCES [security].[tSystemUser] ([SystemUserId])
GO
