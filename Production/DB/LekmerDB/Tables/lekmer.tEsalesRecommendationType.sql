CREATE TABLE [lekmer].[tEsalesRecommendationType]
(
[EsalesRecommendationTypeId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tEsalesRecommendationType] ADD CONSTRAINT [PK_EsalesRecommendationType] PRIMARY KEY CLUSTERED  ([EsalesRecommendationTypeId]) ON [PRIMARY]
GO
