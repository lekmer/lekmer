CREATE TABLE [export].[tCdonExportProductMapping]
(
[CdonProductId] [int] NOT NULL,
[CdonArticleId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportProductMapping] ADD CONSTRAINT [UC_CdonArticleId] UNIQUE NONCLUSTERED  ([CdonArticleId]) ON [PRIMARY]
GO
ALTER TABLE [export].[tCdonExportProductMapping] ADD CONSTRAINT [UC_CdonProductId] UNIQUE NONCLUSTERED  ([CdonProductId]) ON [PRIMARY]
GO
