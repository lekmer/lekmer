CREATE TABLE [integration].[tProductRegistryRestrictions]
(
[ProductId] [int] NOT NULL,
[ProductRegistryId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tProductRegistryRestrictions] ADD 
CONSTRAINT [PK_tProductRegistryRestrictions] PRIMARY KEY CLUSTERED  ([ProductId], [ProductRegistryId]) ON [PRIMARY]
GO
