CREATE TABLE [orderlek].[tCollectorTransaction]
(
[TransactionId] [int] NOT NULL IDENTITY(1, 1),
[StoreId] [int] NOT NULL,
[TransactionTypeId] [int] NOT NULL,
[TimeoutProcessed] [int] NULL,
[StatusCode] [int] NULL,
[Created] [datetime] NOT NULL,
[CivicNumber] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Duration] [bigint] NULL,
[OrderId] [int] NULL,
[ProductCode] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[AvailableReservationAmount] [decimal] (16, 2) NULL,
[InvoiceNo] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[InvoiceStatus] [int] NULL,
[InvoiceUrl] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[FaultCode] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[FaultMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ErrorMessage] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL,
[ResponseContent] [nvarchar] (max) COLLATE Finnish_Swedish_CI_AS NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
ALTER TABLE [orderlek].[tCollectorTransaction] ADD 
CONSTRAINT [PK_tCollectorTransaction] PRIMARY KEY CLUSTERED  ([TransactionId]) ON [PRIMARY]

GO
