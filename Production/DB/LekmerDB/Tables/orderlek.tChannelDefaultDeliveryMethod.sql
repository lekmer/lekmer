CREATE TABLE [orderlek].[tChannelDefaultDeliveryMethod]
(
[ChannelId] [int] NOT NULL,
[DeliveryMethodId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tChannelDefaultDeliveryMethod] ADD CONSTRAINT [PK_ChannelDefaultDeliveryMethod] PRIMARY KEY CLUSTERED  ([ChannelId]) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tChannelDefaultDeliveryMethod] ADD CONSTRAINT [FK_tChannelDefaultDeliveryMethod_tChannel] FOREIGN KEY ([ChannelId]) REFERENCES [core].[tChannel] ([ChannelId])
GO
ALTER TABLE [orderlek].[tChannelDefaultDeliveryMethod] ADD CONSTRAINT [FK_tChannelDefaultDeliveryMethod_tDeliveryMethod] FOREIGN KEY ([DeliveryMethodId]) REFERENCES [order].[tDeliveryMethod] ([DeliveryMethodId])
GO
