CREATE TABLE [campaignlek].[tCustomerGroupConditionInclude]
(
[ConditionId] [int] NOT NULL,
[CustomerGroupId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionInclude] ADD CONSTRAINT [PK_tCustomerGroupConditionInclude] PRIMARY KEY CLUSTERED  ([ConditionId], [CustomerGroupId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionInclude] ADD CONSTRAINT [FK_tCustomerGroupConditionInclude_tCustomerGroupCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaignlek].[tCustomerGroupCondition] ([ConditionId])
GO
ALTER TABLE [campaignlek].[tCustomerGroupConditionInclude] ADD CONSTRAINT [FK_tCustomerGroupConditionInclude_tCustomerGroup] FOREIGN KEY ([CustomerGroupId]) REFERENCES [customer].[tCustomerGroup] ([CustomerGroupId]) ON DELETE CASCADE
GO
