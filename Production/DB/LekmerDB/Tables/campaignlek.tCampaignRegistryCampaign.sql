CREATE TABLE [campaignlek].[tCampaignRegistryCampaign]
(
[CampaignRegistryId] [int] NOT NULL,
[CampaignId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaign] ADD CONSTRAINT [PK_tCampaignRegistryCampaign] PRIMARY KEY CLUSTERED  ([CampaignRegistryId], [CampaignId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaign] ADD CONSTRAINT [FK_tCampaignRegistryCampaign_tCampaign] FOREIGN KEY ([CampaignId]) REFERENCES [campaign].[tCampaign] ([CampaignId])
GO
ALTER TABLE [campaignlek].[tCampaignRegistryCampaign] ADD CONSTRAINT [FK_tCampaignRegistryCampaign_tCampaignRegistry] FOREIGN KEY ([CampaignRegistryId]) REFERENCES [campaign].[tCampaignRegistry] ([CampaignRegistryId])
GO
