CREATE TABLE [addon].[tReviewStatus]
(
[StatusId] [int] NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [addon].[tReviewStatus] ADD CONSTRAINT [PK_tReviewStatus] PRIMARY KEY CLUSTERED  ([StatusId]) ON [PRIMARY]
GO
