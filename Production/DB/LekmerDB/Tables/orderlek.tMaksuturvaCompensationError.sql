CREATE TABLE [orderlek].[tMaksuturvaCompensationError]
(
[MaksuturvaCompensationErrorId] [int] NOT NULL IDENTITY(1, 1),
[ChannelCommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[FetchDate] [datetime] NOT NULL,
[LastAttempt] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tMaksuturvaCompensationError] ADD CONSTRAINT [PK_tMaksuturvaCompensationError] PRIMARY KEY CLUSTERED  ([MaksuturvaCompensationErrorId]) ON [PRIMARY]
GO
