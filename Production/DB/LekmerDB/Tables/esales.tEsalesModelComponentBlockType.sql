CREATE TABLE [esales].[tEsalesModelComponentBlockType]
(
[ModelComponentId] [int] NOT NULL,
[BlockTypeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModelComponentBlockType] ADD CONSTRAINT [PK_tEsalesModelComponentBlockType] PRIMARY KEY CLUSTERED  ([ModelComponentId], [BlockTypeId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tEsalesModelComponentBlockType] ADD CONSTRAINT [FK_tEsalesModelComponentBlockType_tBlockType] FOREIGN KEY ([BlockTypeId]) REFERENCES [sitestructure].[tBlockType] ([BlockTypeId])
GO
ALTER TABLE [esales].[tEsalesModelComponentBlockType] ADD CONSTRAINT [FK_tEsalesModelComponentBlockType_tEsalesModelComponent] FOREIGN KEY ([ModelComponentId]) REFERENCES [esales].[tEsalesModelComponent] ([ModelComponentId])
GO
