CREATE TABLE [campaignlek].[tFixedPriceActionExcludeBrand]
(
[ProductActionId] [int] NOT NULL,
[BrandId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeBrand] ADD CONSTRAINT [PK_tFixedPriceActionExcludeBrand] PRIMARY KEY CLUSTERED  ([ProductActionId], [BrandId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeBrand] ADD CONSTRAINT [FK_tFixedPriceActionExcludeBrand_tBrand] FOREIGN KEY ([BrandId]) REFERENCES [lekmer].[tBrand] ([BrandId])
GO
ALTER TABLE [campaignlek].[tFixedPriceActionExcludeBrand] ADD CONSTRAINT [FK_tFixedPriceActionExcludeBrand_tFixedPriceAction] FOREIGN KEY ([ProductActionId]) REFERENCES [campaignlek].[tFixedPriceAction] ([ProductActionId])
GO
