CREATE TABLE [integration].[tTradeDoublerProductGroupMapping]
(
[ChannelId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[ProductGroupId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[HYArticleClassId] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tTradeDoublerProductGroupMapping] ADD 
CONSTRAINT [PK_tTradeDoublerProductGroupMapping] PRIMARY KEY CLUSTERED  ([HYArticleClassId], [ChannelId]) ON [PRIMARY]
GO
