CREATE TABLE [product].[tVariationGroup]
(
[VariationGroupId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (256) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[DefaultProductId] [int] NULL,
[VariationGroupStatusId] [int] NOT NULL
) ON [PRIMARY]
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tVariationGroup_Title] ON [product].[tVariationGroup] ([Title]) ON [PRIMARY]

GO
ALTER TABLE [product].[tVariationGroup] ADD CONSTRAINT [PK_tVariationGroup] PRIMARY KEY CLUSTERED  ([VariationGroupId]) ON [PRIMARY]
GO

ALTER TABLE [product].[tVariationGroup] ADD CONSTRAINT [FK_tVariationGroup_tVariationGroupStatus] FOREIGN KEY ([VariationGroupStatusId]) REFERENCES [product].[tVariationGroupStatus] ([VariationGroupStatusId])
GO
