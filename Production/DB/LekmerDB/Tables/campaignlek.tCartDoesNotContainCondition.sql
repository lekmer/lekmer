CREATE TABLE [campaignlek].[tCartDoesNotContainCondition]
(
[ConditionId] [int] NOT NULL,
[ConfigId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartDoesNotContainCondition] ADD CONSTRAINT [PK_tCartDoesNotContainCondition] PRIMARY KEY CLUSTERED  ([ConditionId]) ON [PRIMARY]
GO
ALTER TABLE [campaignlek].[tCartDoesNotContainCondition] ADD CONSTRAINT [FK_tCartDoesNotContainCondition_tCondition] FOREIGN KEY ([ConditionId]) REFERENCES [campaign].[tCondition] ([ConditionId])
GO
ALTER TABLE [campaignlek].[tCartDoesNotContainCondition] ADD CONSTRAINT [FK_tCartDoesNotContainCondition_tCampaignActionConfigurator] FOREIGN KEY ([ConfigId]) REFERENCES [campaignlek].[tCampaignActionConfigurator] ([CampaignActionConfiguratorId])
GO
