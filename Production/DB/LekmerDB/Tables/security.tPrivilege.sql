CREATE TABLE [security].[tPrivilege]
(
[PrivilegeId] [int] NOT NULL IDENTITY(1, 1),
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [security].[tPrivilege] ADD CONSTRAINT [PK_tPrivilege] PRIMARY KEY CLUSTERED  ([PrivilegeId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_tPrivilege_CommonName] ON [security].[tPrivilege] ([CommonName]) ON [PRIMARY]
GO
