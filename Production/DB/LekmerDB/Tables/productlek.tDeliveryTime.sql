CREATE TABLE [productlek].[tDeliveryTime]
(
[DeliveryTimeId] [int] NOT NULL IDENTITY(1, 1)
) ON [PRIMARY]
GO
ALTER TABLE [productlek].[tDeliveryTime] ADD CONSTRAINT [PK_tDeliveryTime] PRIMARY KEY CLUSTERED  ([DeliveryTimeId]) ON [PRIMARY]
GO
