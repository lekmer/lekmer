CREATE TABLE [lekmer].[tNewsletterUnsubscriberOption]
(
[UnsubscriberOprionId] [int] NOT NULL IDENTITY(1, 1),
[UnsubscriberId] [int] NOT NULL,
[NewsletterTypeId] [int] NOT NULL,
[CreatedDate] [datetime] NOT NULL
) ON [PRIMARY]
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD
CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterUnsubscriber] FOREIGN KEY ([UnsubscriberId]) REFERENCES [lekmer].[tNewsletterUnsubscriber] ([UnsubscriberId])

ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD
CONSTRAINT [FK_tNewsletterUnsubscriberOption_tNewsletterType] FOREIGN KEY ([NewsletterTypeId]) REFERENCES [lekmer].[tNewsletterType] ([NewsletterTypeId])
GO
ALTER TABLE [lekmer].[tNewsletterUnsubscriberOption] ADD CONSTRAINT [PK_tNewsletterUnsubscriberOption] PRIMARY KEY CLUSTERED  ([UnsubscriberOprionId]) ON [PRIMARY]
GO
