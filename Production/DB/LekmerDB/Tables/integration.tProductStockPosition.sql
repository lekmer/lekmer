CREATE TABLE [integration].[tProductStockPosition]
(
[HYarticleId] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Lagerplatstyp] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NULL,
[LagerplatstypBenamning] [nvarchar] (200) COLLATE Finnish_Swedish_CI_AS NULL,
[Lagerplats] [nvarchar] (100) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ImportDate] [datetime] NULL
) ON [PRIMARY]
ALTER TABLE [integration].[tProductStockPosition] ADD 
CONSTRAINT [PK_tProductStockPosition] PRIMARY KEY CLUSTERED  ([HYarticleId], [Lagerplats]) ON [PRIMARY]
GO
