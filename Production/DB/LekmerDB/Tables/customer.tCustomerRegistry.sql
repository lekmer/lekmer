CREATE TABLE [customer].[tCustomerRegistry]
(
[CustomerRegistryId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerRegistry] ADD CONSTRAINT [PK_tCustomerRegistry] PRIMARY KEY CLUSTERED  ([CustomerRegistryId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UQ_Title] ON [customer].[tCustomerRegistry] ([Title]) ON [PRIMARY]
GO
