CREATE TABLE [template].[tModelFragmentRegion]
(
[ModelFragmentRegionId] [int] NOT NULL IDENTITY(1, 1),
[ModelId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[Ordinal] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [PK_tModelFragmentRegion] PRIMARY KEY CLUSTERED  ([ModelFragmentRegionId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [UQ_tModelFragmentRegion_ModelId_Title] UNIQUE NONCLUSTERED  ([ModelId], [Title]) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelFragmentRegion] ADD CONSTRAINT [FK_tModelFragmentRegion_tModel] FOREIGN KEY ([ModelId]) REFERENCES [template].[tModel] ([ModelId])
GO
