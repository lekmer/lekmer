CREATE TABLE [statistics].[tTopListProduct]
(
[Id] [int] NOT NULL IDENTITY(1, 1),
[ProductId] [int] NOT NULL,
[ChannelId] [int] NOT NULL,
[Date] [datetime] NOT NULL,
[PurchaseSum] [decimal] (16, 2) NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [statistics].[tTopListProduct] ADD CONSTRAINT [PK_tTopListProduct] PRIMARY KEY CLUSTERED  ([Id]) ON [PRIMARY]
GO
