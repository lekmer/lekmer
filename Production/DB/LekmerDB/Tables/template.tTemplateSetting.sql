CREATE TABLE [template].[tTemplateSetting]
(
[TemplateId] [int] NOT NULL,
[ModelSettingId] [int] NOT NULL,
[Value] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[AlternateValue] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [template].[tTemplateSetting] ADD CONSTRAINT [PK_tTemplateSetting] PRIMARY KEY CLUSTERED  ([TemplateId], [ModelSettingId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tTemplateSetting] ADD CONSTRAINT [FK_tTemplateSetting_tModelSetting] FOREIGN KEY ([ModelSettingId]) REFERENCES [template].[tModelSetting] ([ModelSettingId])
GO
ALTER TABLE [template].[tTemplateSetting] ADD CONSTRAINT [FK_tTemplateSetting_tTemplate] FOREIGN KEY ([TemplateId]) REFERENCES [template].[tTemplate] ([TemplateId])
GO
