CREATE TABLE [temp].[CdonPriceNewCalc2]
(
[ChannelId] [int] NULL,
[ProductId] [int] NOT NULL,
[Price] [decimal] (16, 2) NOT NULL,
[Vat] [decimal] (17, 2) NULL,
[DiscountPrice] [decimal] (16, 2) NULL,
[DiscountPriceVat] [decimal] (37, 8) NULL
) ON [PRIMARY]
GO
