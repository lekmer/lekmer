CREATE TABLE [esales].[tBlockEsalesTopSellersV2Product]
(
[BlockId] [int] NOT NULL,
[ProductId] [int] NOT NULL,
[Position] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Product] ADD CONSTRAINT [PK_tBlockEsalesTopSellersV2Product] PRIMARY KEY CLUSTERED  ([BlockId], [ProductId]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Product] ADD CONSTRAINT [IX_tBlockEsalesTopSellersV2Product] UNIQUE NONCLUSTERED  ([BlockId], [Position]) ON [PRIMARY]
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Product] ADD CONSTRAINT [FK_tBlockEsalesTopSellersV2Product_tBlockEsalesTopSellersV2] FOREIGN KEY ([BlockId]) REFERENCES [esales].[tBlockEsalesTopSellersV2] ([BlockId])
GO
ALTER TABLE [esales].[tBlockEsalesTopSellersV2Product] ADD CONSTRAINT [FK_tBlockEsalesTopSellersV2Product_tProduct] FOREIGN KEY ([ProductId]) REFERENCES [product].[tProduct] ([ProductId])
GO
