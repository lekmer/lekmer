CREATE TABLE [customer].[tCustomerStatus]
(
[CustomerStatusId] [int] NOT NULL,
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [customer].[tCustomerStatus] ADD CONSTRAINT [PK_tCustomerStatus] PRIMARY KEY CLUSTERED  ([CustomerStatusId]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_CommonName] ON [customer].[tCustomerStatus] ([CommonName]) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UQ_Title] ON [customer].[tCustomerStatus] ([Title]) ON [PRIMARY]
GO
