CREATE TABLE [lekmer].[tAgeIntervalSiteStructureRegistry]
(
[AgeIntervalId] [int] NOT NULL,
[SiteStructureRegistryId] [int] NOT NULL,
[ContentNodeId] [int] NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tAgeIntervalSiteStructureRegistry] ADD CONSTRAINT [PK_tAgeIntervalSiteStructureRegistry] PRIMARY KEY CLUSTERED  ([AgeIntervalId], [SiteStructureRegistryId]) ON [PRIMARY]
GO
ALTER TABLE [lekmer].[tAgeIntervalSiteStructureRegistry] ADD CONSTRAINT [FK_tAgeIntervalSiteStructureRegistry_tAgeInterval] FOREIGN KEY ([AgeIntervalId]) REFERENCES [lekmer].[tAgeInterval] ([AgeIntervalId])
GO
ALTER TABLE [lekmer].[tAgeIntervalSiteStructureRegistry] ADD CONSTRAINT [FK_tAgeIntervalSiteStructureRegistry_tContentNode] FOREIGN KEY ([ContentNodeId], [SiteStructureRegistryId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId], [SiteStructureRegistryId]) ON DELETE CASCADE
GO
ALTER TABLE [lekmer].[tAgeIntervalSiteStructureRegistry] ADD CONSTRAINT [FK_tAgeIntervalSiteStructureRegistry_tSiteStructureRegistry] FOREIGN KEY ([SiteStructureRegistryId]) REFERENCES [sitestructure].[tSiteStructureRegistry] ([SiteStructureRegistryId])
GO
