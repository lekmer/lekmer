CREATE TABLE [template].[tModelFragmentEntity]
(
[ModelFragmentId] [int] NOT NULL,
[EntityId] [int] NOT NULL
) ON [PRIMARY]
ALTER TABLE [template].[tModelFragmentEntity] WITH NOCHECK ADD
CONSTRAINT [FK_tModelFragmentEntity_tModelFragment] FOREIGN KEY ([ModelFragmentId]) REFERENCES [template].[tModelFragment] ([ModelFragmentId])
GO
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [PK_tModelFragmentEntity] PRIMARY KEY CLUSTERED  ([ModelFragmentId], [EntityId]) ON [PRIMARY]
GO
ALTER TABLE [template].[tModelFragmentEntity] ADD CONSTRAINT [FK_tModelFragmentEntity_tEntity] FOREIGN KEY ([EntityId]) REFERENCES [template].[tEntity] ([EntityId])
GO
