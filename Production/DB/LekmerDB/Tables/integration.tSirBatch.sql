CREATE TABLE [integration].[tSirBatch]
(
[SirBatchId] [int] NOT NULL IDENTITY(1, 1),
[ChangesFromDate] [datetime] NULL,
[StartedDate] [datetime] NOT NULL,
[CompletedDate] [datetime] NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tSirBatch] ADD CONSTRAINT [PK_tSirBatch] PRIMARY KEY CLUSTERED  ([SirBatchId]) ON [PRIMARY]
GO
