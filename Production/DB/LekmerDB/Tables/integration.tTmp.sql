CREATE TABLE [integration].[tTmp]
(
[HYErpId] [nvarchar] (15) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[ChannelName] [nvarchar] (15) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tTmp] ADD CONSTRAINT [PK__tTmp__937EE05D4B3A564E] PRIMARY KEY CLUSTERED  ([HYErpId], [ChannelName]) WITH (IGNORE_DUP_KEY=ON) ON [PRIMARY]
GO
