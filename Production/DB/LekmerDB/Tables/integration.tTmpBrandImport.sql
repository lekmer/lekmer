CREATE TABLE [integration].[tTmpBrandImport]
(
[HyId] [nvarchar] (15) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [integration].[tTmpBrandImport] ADD CONSTRAINT [PK__tTmpBran__67B31171223EC90E] PRIMARY KEY CLUSTERED  ([HyId]) WITH (IGNORE_DUP_KEY=ON) ON [PRIMARY]
GO
