CREATE TABLE [orderlek].[tDeliveryMethodType]
(
[DeliveryMethodTypeId] [int] NOT NULL IDENTITY(1, 1),
[Title] [nvarchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL,
[CommonName] [varchar] (50) COLLATE Finnish_Swedish_CI_AS NOT NULL
) ON [PRIMARY]
GO
ALTER TABLE [orderlek].[tDeliveryMethodType] ADD CONSTRAINT [PK_tDeliveryMethodType] PRIMARY KEY CLUSTERED  ([DeliveryMethodTypeId]) ON [PRIMARY]
GO
