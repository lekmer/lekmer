CREATE TABLE [order].[tBlockCheckout]
(
[BlockId] [int] NOT NULL,
[RedirectContentNodeId] [int] NULL
) ON [PRIMARY]
GO
ALTER TABLE [order].[tBlockCheckout] ADD CONSTRAINT [PK_tBlockCheckout] PRIMARY KEY CLUSTERED  ([BlockId]) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [IX_tBlockCheckout_RedirectContentNodeId] ON [order].[tBlockCheckout] ([RedirectContentNodeId]) ON [PRIMARY]
GO
ALTER TABLE [order].[tBlockCheckout] ADD CONSTRAINT [FK_tBlockCheckout_tBlock] FOREIGN KEY ([BlockId]) REFERENCES [sitestructure].[tBlock] ([BlockId])
GO
ALTER TABLE [order].[tBlockCheckout] ADD CONSTRAINT [FK_tBlockCheckout_tContentNode] FOREIGN KEY ([RedirectContentNodeId]) REFERENCES [sitestructure].[tContentNode] ([ContentNodeId])
GO
