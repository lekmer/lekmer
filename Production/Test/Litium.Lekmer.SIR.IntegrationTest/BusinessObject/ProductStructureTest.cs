using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SIR.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductStructureTest
	{
		[Test]
		[Category("IoC")]
		public void ProductStructure_Resolve_Resolved()
		{
			var info = IoC.Resolve<IProductStructure>();
			Assert.IsInstanceOf<IProductStructure>(info);
		}

		[Test]
		[Category("IoC")]
		public void ProductStructure_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<IProductStructure>();
			var info2 = IoC.Resolve<IProductStructure>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}