using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SIR.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SirBatchTest
	{
		[Test]
		[Category("IoC")]
		public void SirBatch_Resolve_Resolved()
		{
			var info = IoC.Resolve<ISirBatch>();
			Assert.IsInstanceOf<ISirBatch>(info);
		}

		[Test]
		[Category("IoC")]
		public void SirBatch_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ISirBatch>();
			var info2 = IoC.Resolve<ISirBatch>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}