﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CartCampaignSharedServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartCampaignSharedService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartCampaignSharedService>("CartCampaignSharedService");

			Assert.IsInstanceOf<ICartCampaignSharedService>(service);
			Assert.IsInstanceOf<CartCampaignSharedService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartCampaignSharedService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartCampaignSharedService>("CartCampaignSharedService");
			var instance2 = IoC.Resolve<ICartCampaignSharedService>("CartCampaignSharedService");

			Assert.AreEqual(instance1, instance2);
		}
	}
}