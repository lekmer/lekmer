﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerPercentagePriceDiscountActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerPercentagePriceDiscountActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<PercentagePriceDiscountActionRepository>();

			Assert.IsInstanceOf<LekmerPercentagePriceDiscountActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerPercentagePriceDiscountActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<PercentagePriceDiscountActionRepository>();
			var instance2 = IoC.Resolve<PercentagePriceDiscountActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}