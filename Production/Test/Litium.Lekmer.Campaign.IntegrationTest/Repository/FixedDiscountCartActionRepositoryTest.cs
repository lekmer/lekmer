﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class FixedDiscountCartActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void FixedDiscountCartActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<FixedDiscountCartActionRepository>();

			Assert.IsInstanceOf<FixedDiscountCartActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void FixedDiscountCartActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<FixedDiscountCartActionRepository>();
			var instance2 = IoC.Resolve<FixedDiscountCartActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}