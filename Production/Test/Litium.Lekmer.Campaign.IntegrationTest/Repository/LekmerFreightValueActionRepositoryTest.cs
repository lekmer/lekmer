﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerFreightValueActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerFreightValueActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<FreightValueActionRepository>();

			Assert.IsInstanceOf<LekmerFreightValueActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerFreightValueActionRepository_ResolveLekmer_Resolved()
		{
			var repository = IoC.Resolve<LekmerFreightValueActionRepository>();

			Assert.IsInstanceOf<LekmerFreightValueActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerFreightValueActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<FreightValueActionRepository>();
			var instance2 = IoC.Resolve<FreightValueActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}