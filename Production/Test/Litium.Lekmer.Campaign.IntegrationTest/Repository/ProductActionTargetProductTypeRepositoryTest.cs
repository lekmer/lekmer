﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductActionTargetProductTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductActionTargetProductTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductActionTargetProductTypeRepository>();

			Assert.IsInstanceOf<ProductActionTargetProductTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductActionTargetProductTypeRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ProductActionTargetProductTypeRepository>();
			var instance2 = IoC.Resolve<ProductActionTargetProductTypeRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}