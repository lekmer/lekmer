﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CampaignRegistryCampaignRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CampaignRegistryCampaignRepository>();

			Assert.IsInstanceOf<CampaignRegistryCampaignRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CampaignRegistryCampaignRepository>();
			var instance2 = IoC.Resolve<CampaignRegistryCampaignRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}