﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductAutoFreeCartActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductAutoFreeCartActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductAutoFreeCartActionRepository>();

			Assert.IsInstanceOf<ProductAutoFreeCartActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductAutoFreeCartActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ProductAutoFreeCartActionRepository>();
			var instance2 = IoC.Resolve<ProductAutoFreeCartActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}