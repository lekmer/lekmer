﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CustomerGroupConditionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CustomerGroupConditionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CustomerGroupConditionRepository>();

			Assert.IsInstanceOf<CustomerGroupConditionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CustomerGroupConditionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CustomerGroupConditionRepository>();
			var instance2 = IoC.Resolve<CustomerGroupConditionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}