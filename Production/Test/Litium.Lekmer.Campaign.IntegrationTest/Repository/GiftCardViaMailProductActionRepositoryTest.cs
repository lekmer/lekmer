﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class GiftCardViaMailProductActionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductActionRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<GiftCardViaMailProductActionRepository>();

			Assert.IsInstanceOf<GiftCardViaMailProductActionRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void GiftCardViaMailProductActionRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<GiftCardViaMailProductActionRepository>();
			var instance2 = IoC.Resolve<GiftCardViaMailProductActionRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}