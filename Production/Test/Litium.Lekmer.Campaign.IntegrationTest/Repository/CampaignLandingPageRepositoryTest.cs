﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CampaignLandingPageRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignLandingPageRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CampaignLandingPageRepository>();

			Assert.IsInstanceOf<CampaignLandingPageRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CampaignLandingPageRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CampaignLandingPageRepository>();
			var instance2 = IoC.Resolve<CampaignLandingPageRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}