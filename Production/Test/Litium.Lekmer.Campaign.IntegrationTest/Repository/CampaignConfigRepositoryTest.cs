﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CampaignConfigRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignConfigRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CampaignConfigRepository>();

			Assert.IsInstanceOf<CampaignConfigRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CampaignConfigRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CampaignConfigRepository>();
			var instance2 = IoC.Resolve<CampaignConfigRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}