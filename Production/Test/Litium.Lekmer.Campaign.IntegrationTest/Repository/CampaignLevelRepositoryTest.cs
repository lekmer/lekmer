﻿using Litium.Lekmer.Campaign.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Repository
{
	[TestFixture]
	public class CampaignLevelRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignLevelRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CampaignLevelRepository>();

			Assert.IsInstanceOf<CampaignLevelRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CampaignLevelRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<CampaignLevelRepository>();
			var instance2 = IoC.Resolve<CampaignLevelRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}