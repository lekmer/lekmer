﻿using System.Data;
using Litium.Lekmer.Campaign.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Campaign.IntegrationTest.Mapper
{
	[TestFixture]
	public class ConditionTargetProductTypeDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void ConditionTargetProductTypeDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IConditionTargetProductType>(_dataReader);
			Assert.IsInstanceOf<ConditionTargetProductTypeDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void ConditionTargetProductTypeDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IConditionTargetProductType>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IConditionTargetProductType>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}