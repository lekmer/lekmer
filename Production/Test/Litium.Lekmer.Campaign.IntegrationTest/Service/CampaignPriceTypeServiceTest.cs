﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CampaignPriceTypeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignPriceTypeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignPriceTypeService>();

			Assert.IsInstanceOf<ICampaignPriceTypeService>(service);
			Assert.IsInstanceOf<CampaignPriceTypeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignPriceTypeService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignPriceTypeService>();
			var instance2 = IoC.Resolve<ICampaignPriceTypeService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}