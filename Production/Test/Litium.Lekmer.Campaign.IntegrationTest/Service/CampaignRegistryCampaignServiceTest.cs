﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class CampaignRegistryCampaignServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignRegistryCampaignService>();

			Assert.IsInstanceOf<ICampaignRegistryCampaignService>(service);
			Assert.IsInstanceOf<CampaignRegistryCampaignService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignRegistryCampaignService>();
			var instance2 = IoC.Resolve<ICampaignRegistryCampaignService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}