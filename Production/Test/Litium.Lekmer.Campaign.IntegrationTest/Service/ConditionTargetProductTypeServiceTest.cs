﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.Service
{
	[TestFixture]
	public class ConditionTargetProductTypeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ConditionTargetProductTypeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionTargetProductTypeService>();

			Assert.IsInstanceOf<IConditionTargetProductTypeService>(service);
			Assert.IsInstanceOf<ConditionTargetProductTypeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ConditionTargetProductTypeService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionTargetProductTypeService>();
			var instance2 = IoC.Resolve<IConditionTargetProductTypeService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}