﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CartValueRangeConditionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartValueRangeConditionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginSecureService>("secure_CartValueRange");

			Assert.IsInstanceOf<IConditionPluginSecureService>(service);
			Assert.IsInstanceOf<CartValueRangeConditionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartValueRangeConditionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginSecureService>("secure_CartValueRange");
			var instance2 = IoC.Resolve<IConditionPluginSecureService>("secure_CartValueRange");

			Assert.AreEqual(instance1, instance2);
		}
	}
}