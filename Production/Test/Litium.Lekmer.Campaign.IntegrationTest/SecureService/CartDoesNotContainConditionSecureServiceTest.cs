﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CartDoesNotContainConditionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CartDoesNotContainConditionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionPluginSecureService>("secure_CartDoesNotContain");

			Assert.IsInstanceOf<IConditionPluginSecureService>(service);
			Assert.IsInstanceOf<CartDoesNotContainConditionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CartDoesNotContainConditionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionPluginSecureService>("secure_CartDoesNotContain");
			var instance2 = IoC.Resolve<IConditionPluginSecureService>("secure_CartDoesNotContain");

			Assert.AreEqual(instance1, instance2);
		}
	}
}