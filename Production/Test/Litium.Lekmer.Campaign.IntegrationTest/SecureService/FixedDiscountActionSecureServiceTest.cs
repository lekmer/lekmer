﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class FixedDiscountActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void FixedDiscountActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionPluginSecureService>("secure_FixedDiscount");

			Assert.IsInstanceOf<IProductActionPluginSecureService>(service);
			Assert.IsInstanceOf<FixedDiscountActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void FixedDiscountActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionPluginSecureService>("secure_FixedDiscount");
			var instance2 = IoC.Resolve<IProductActionPluginSecureService>("secure_FixedDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}