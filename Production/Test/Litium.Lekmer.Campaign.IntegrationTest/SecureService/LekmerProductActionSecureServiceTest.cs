﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerProductActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerProductActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductActionSecureService>();

			Assert.IsInstanceOf<IProductActionSecureService>(service);
			Assert.IsInstanceOf<LekmerProductActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductActionSecureService>();
			var instance2 = IoC.Resolve<IProductActionSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}