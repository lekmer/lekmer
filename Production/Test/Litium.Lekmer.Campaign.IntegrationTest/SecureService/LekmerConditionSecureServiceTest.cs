﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerConditionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerConditionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IConditionSecureService>();

			Assert.IsInstanceOf<IConditionSecureService>(service);
			Assert.IsInstanceOf<LekmerConditionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerConditionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IConditionSecureService>();
			var instance2 = IoC.Resolve<IConditionSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}