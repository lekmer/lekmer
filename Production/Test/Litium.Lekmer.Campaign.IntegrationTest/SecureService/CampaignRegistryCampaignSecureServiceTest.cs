﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class CampaignRegistryCampaignSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICampaignRegistryCampaignSecureService>();

			Assert.IsInstanceOf<ICampaignRegistryCampaignSecureService>(service);
			Assert.IsInstanceOf<CampaignRegistryCampaignSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CampaignRegistryCampaignSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICampaignRegistryCampaignSecureService>();
			var instance2 = IoC.Resolve<ICampaignRegistryCampaignSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}