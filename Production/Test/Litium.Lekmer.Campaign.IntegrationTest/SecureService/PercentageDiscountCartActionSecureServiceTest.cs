﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.SecureService
{
	[TestFixture]
	public class PercentageDiscountCartActionSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void PercentageDiscountCartActionSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICartActionPluginSecureService>("secure_PercentageDiscount");

			Assert.IsInstanceOf<ICartActionPluginSecureService>(service);
			Assert.IsInstanceOf<PercentageDiscountCartActionSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void PercentageDiscountCartActionSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICartActionPluginSecureService>("secure_PercentageDiscount");
			var instance2 = IoC.Resolve<ICartActionPluginSecureService>("secure_PercentageDiscount");

			Assert.AreEqual(instance1, instance2);
		}
	}
}