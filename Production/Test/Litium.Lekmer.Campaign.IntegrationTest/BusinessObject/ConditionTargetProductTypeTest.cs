﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ConditionTargetProductTypeTest
	{
		[Test]
		[Category("IoC")]
		public void ConditionTargetProductType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IConditionTargetProductType>();

			Assert.IsInstanceOf<IConditionTargetProductType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ConditionTargetProductType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IConditionTargetProductType>();
			var instance2 = IoC.Resolve<IConditionTargetProductType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}