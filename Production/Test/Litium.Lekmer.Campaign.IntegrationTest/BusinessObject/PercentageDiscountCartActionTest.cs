﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class PercentageDiscountCartActionTest
	{
		[Test]
		[Category("IoC")]
		public void PercentageDiscountCartAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IPercentageDiscountCartAction>();

			Assert.IsInstanceOf<IPercentageDiscountCartAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void PercentageDiscountCartAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IPercentageDiscountCartAction>();
			var instance2 = IoC.Resolve<IPercentageDiscountCartAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}