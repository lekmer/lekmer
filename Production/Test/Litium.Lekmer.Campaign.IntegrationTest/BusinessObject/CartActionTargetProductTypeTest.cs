﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartActionTargetProductTypeTest
	{
		[Test]
		[Category("IoC")]
		public void CartActionTargetProductType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartActionTargetProductType>();

			Assert.IsInstanceOf<ICartActionTargetProductType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartActionTargetProductType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartActionTargetProductType>();
			var instance2 = IoC.Resolve<ICartActionTargetProductType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}