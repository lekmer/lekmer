﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartDoesNotContainConditionTest
	{
		[Test]
		[Category("IoC")]
		public void CartDoesNotContainCondition_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartDoesNotContainCondition>();

			Assert.IsInstanceOf<ICartDoesNotContainCondition>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartDoesNotContainCondition_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartDoesNotContainCondition>();
			var instance2 = IoC.Resolve<ICartDoesNotContainCondition>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}