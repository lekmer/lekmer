﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerProductActionTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerProductAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductAction>();

			Assert.IsInstanceOf<IProductAction>(instance);
			Assert.IsInstanceOf<ILekmerProductAction>(instance);
			Assert.IsInstanceOf<LekmerProductAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductAction>();
			var instance2 = IoC.Resolve<IProductAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}