﻿using Litium.Scensum.Campaign;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerCartValueConditionTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCartValueCondition_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartValueCondition>();

			Assert.IsInstanceOf<ICartValueCondition>(instance);
			Assert.IsInstanceOf<ILekmerCartValueCondition>(instance);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCartValueCondition_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartValueCondition>();
			var instance2 = IoC.Resolve<ICartValueCondition>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}