﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartItemGroupFixedDiscountActionTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemGroupFixedDiscountAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartItemGroupFixedDiscountAction>();

			Assert.IsInstanceOf<ICartItemGroupFixedDiscountAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartItemGroupFixedDiscountAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartItemGroupFixedDiscountAction>();
			var instance2 = IoC.Resolve<ICartItemGroupFixedDiscountAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}