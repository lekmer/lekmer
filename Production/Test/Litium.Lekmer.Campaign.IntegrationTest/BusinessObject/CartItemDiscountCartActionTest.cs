﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CartItemDiscountCartActionTest
	{
		[Test]
		[Category("IoC")]
		public void CartItemDiscountCartAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICartItemDiscountCartAction>();

			Assert.IsInstanceOf<ICartItemDiscountCartAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CartItemDiscountCartAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICartItemDiscountCartAction>();
			var instance2 = IoC.Resolve<ICartItemDiscountCartAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}