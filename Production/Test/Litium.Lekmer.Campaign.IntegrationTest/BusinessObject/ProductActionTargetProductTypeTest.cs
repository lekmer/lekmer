﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductActionTargetProductTypeTest
	{
		[Test]
		[Category("IoC")]
		public void ProductActionTargetProductType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductActionTargetProductType>();

			Assert.IsInstanceOf<IProductActionTargetProductType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductActionTargetProductType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductActionTargetProductType>();
			var instance2 = IoC.Resolve<IProductActionTargetProductType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}