﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class FixedDiscountActionTest
	{
		[Test]
		[Category("IoC")]
		public void FixedDiscountAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IFixedDiscountAction>();

			Assert.IsInstanceOf<IFixedDiscountAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void FixedDiscountAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IFixedDiscountAction>();
			var instance2 = IoC.Resolve<IFixedDiscountAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}