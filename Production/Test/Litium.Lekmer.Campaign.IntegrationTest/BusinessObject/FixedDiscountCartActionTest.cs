﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Campaign.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class FixedDiscountCartActionTest
	{
		[Test]
		[Category("IoC")]
		public void FixedDiscountCartAction_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IFixedDiscountCartAction>();

			Assert.IsInstanceOf<IFixedDiscountCartAction>(instance);
		}

		[Test]
		[Category("IoC")]
		public void FixedDiscountCartAction_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IFixedDiscountCartAction>();
			var instance2 = IoC.Resolve<IFixedDiscountCartAction>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}