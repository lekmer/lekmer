﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Template;
using NUnit.Framework;

namespace Litium.Lekmer.Template.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerAliasSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerAliasSecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<IAliasSecureService>();

			Assert.IsInstanceOf<IAliasSecureService>(secureService);
			Assert.IsInstanceOf<LekmerAliasSecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAliasSecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<IAliasSecureService>();
			var secureService2 = IoC.Resolve<IAliasSecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}
	}
}