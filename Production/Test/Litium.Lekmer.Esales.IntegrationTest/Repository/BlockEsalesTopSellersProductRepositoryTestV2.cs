﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockEsalesTopSellersProductRepositoryTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductRepositoryV2_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockEsalesTopSellersProductRepositoryV2>();

			Assert.IsInstanceOf<BlockEsalesTopSellersProductRepositoryV2>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductRepositoryV2_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<BlockEsalesTopSellersProductRepositoryV2>();
			var repository2 = IoC.Resolve<BlockEsalesTopSellersProductRepositoryV2>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}