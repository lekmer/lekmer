﻿using Litium.Lekmer.Esales.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Repository
{
	[TestFixture]
	public class EsalesRecommendationTypeRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesRecommendationTypeRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<EsalesRecommendationTypeRepository>();

			Assert.IsInstanceOf<EsalesRecommendationTypeRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void EsalesRecommendationTypeRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<EsalesRecommendationTypeRepository>();
			var repository2 = IoC.Resolve<EsalesRecommendationTypeRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}