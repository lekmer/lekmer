﻿using Litium.Lekmer.Esales.Exporter;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Exporter
{
	[TestFixture]
	public class EsalesProductInfoExporterTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesProductInfoExporter_Resolve_Resolved()
		{
			var service = IoC.Resolve<IExporter>("EsalesProductInfoExporter");

			Assert.IsInstanceOf<IExporter>(service);
			Assert.IsInstanceOf<EsalesProductInfoExporter>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesProductInfoExporter_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<IExporter>("EsalesProductInfoExporter");
			var service2 = IoC.Resolve<IExporter>("EsalesProductInfoExporter");

			Assert.AreNotEqual(service1, service2);
		}
	}
}