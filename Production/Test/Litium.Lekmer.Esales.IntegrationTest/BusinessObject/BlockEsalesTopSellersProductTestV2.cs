﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockEsalesTopSellersProductTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductV2_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockEsalesTopSellersProductV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersProductV2>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductV2_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockEsalesTopSellersProductV2>();
			var block2 = IoC.Resolve<IBlockEsalesTopSellersProductV2>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}