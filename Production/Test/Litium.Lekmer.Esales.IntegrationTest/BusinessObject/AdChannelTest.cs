﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class AdChannelTest
	{
		[Test]
		[Category("IoC")]
		public void AdChannel_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IAdChannel>();

			Assert.IsInstanceOf<IAdChannel>(instance);
			Assert.IsInstanceOf<IAdChannel>(instance);
		}

		[Test]
		[Category("IoC")]
		public void AdChannel_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IAdChannel>();
			var instance2 = IoC.Resolve<IAdChannel>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}