﻿using System.Collections.Generic;
using System.Linq;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation.Tree;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Helper
{
	public class AdFolderTestHelper
	{
		public ISystemUserFull SystemUserFull { get; private set; }
		public IAdFolderSecureService AdFolderSecureService { get; private set; }

		public AdFolderTestHelper(IAdFolderSecureService adFolderSecureService, ISystemUserFull systemUserFull)
		{
			AdFolderSecureService = adFolderSecureService;
			SystemUserFull = systemUserFull;
		}


		public IAdFolder Create(string title)
		{
			var adFolder = AdFolderSecureService.Create();
			adFolder.Title = title;
			return adFolder;
		}

		public IAdFolder Create(string title, int parentId)
		{
			var adFolder = AdFolderSecureService.Create();
			adFolder.ParentFolderId = parentId;
			adFolder.Title = title;
			return adFolder;
		}

		public IAdFolder Create(string title, IAdFolder parent)
		{
			return Create(title, parent.Id);
		}

		public IAdFolder CreateAndSave(string title)
		{
			var adFolder = Create(title);
			return AdFolderSecureService.Save(SystemUserFull, adFolder);
		}

		public IAdFolder CreateAndSave(string title, int parentId)
		{
			var adFolder = Create(title, parentId);
			return AdFolderSecureService.Save(SystemUserFull, adFolder);
		}

		public IAdFolder CreateAndSave(string title, IAdFolder parent)
		{
			return CreateAndSave(title, parent.Id);
		}


		// Asserts
		public void AssertContains(IAdFolder expected, IEnumerable<IAdFolder> actual)
		{
			Assert.IsNotNull(expected, "AdFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<AdFolder> actual");

			var actualAdFolder = actual.FirstOrDefault(rf => rf.Id == expected.Id);

			Assert.IsNotNull(actualAdFolder, "AdFolder actual");

			AssertAreEquel(expected, actualAdFolder);
		}

		public void AssertContains(IAdFolder expected, IEnumerable<INode> actual)
		{
			Assert.IsNotNull(expected, "AdFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<AdFolder> actual");

			var actualNode = actual.FirstOrDefault(n => n.Id == expected.Id);

			Assert.IsNotNull(actualNode, "AdFolder actual");

			AssertAreEquel(expected, actualNode);
		}

		public void AssertDoesNotContain(IAdFolder expected, IEnumerable<IAdFolder> actual)
		{
			Assert.IsNotNull(expected, "AdFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<AdFolder> actual");

			var actualAdFolder = actual.FirstOrDefault(rf => rf.Id == expected.Id);

			Assert.IsNull(actualAdFolder, "AdFolder actual");
		}

		public void AssertDoesNotContain(IAdFolder expected, IEnumerable<INode> actual)
		{
			Assert.IsNotNull(expected, "AdFolder expected");
			Assert.IsNotNull(actual, "IEnumerable<INode> actual");

			var actualNode = actual.FirstOrDefault(n => n.Id == expected.Id);

			Assert.IsNull(actualNode, "AdFolder actual");
		}

		public void AssertAreEquel(IAdFolder expected, IAdFolder actual)
		{
			Assert.IsNotNull(expected, "AdFolder expected");
			Assert.IsNotNull(actual, "AdFolder actual");

			Assert.AreEqual(expected.Id, actual.Id, "AdFolder.Id");
			Assert.AreEqual(expected.ParentFolderId, actual.ParentFolderId, "AdFolder.ParentFolderId");
			Assert.AreEqual(expected.Title, actual.Title, "AdFolder.Title");
		}

		public void AssertAreEquel(IAdFolder expected, INode actual)
		{
			Assert.IsNotNull(expected, "AdFolder expected");
			Assert.IsNotNull(actual, "AdFolder actual");

			Assert.AreEqual(expected.Id, actual.Id, "AdFolder.Id");
			Assert.AreEqual(expected.ParentFolderId, actual.ParentId, "AdFolder.ParentFolderId");
			Assert.AreEqual(expected.Title, actual.Title, "AdFolder.Title");
		}
	}
}