﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class EsalesNotifyServiceTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesNotifyService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IEsalesNotifyService>();

			Assert.IsInstanceOf<IEsalesNotifyService>(service);
			Assert.IsInstanceOf<EsalesNotifyService>(service);
		}

		[Test]
		[Category("IoC")]
		public void EsalesNotifyService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IEsalesNotifyService>();
			var service2 = IoC.Resolve<IEsalesNotifyService>();

			Assert.AreEqual(service1, service2);
		}
	}
}