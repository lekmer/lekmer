﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class BlockEsalesAdsServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesAdsService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesAdsService>();

			Assert.IsInstanceOf<IBlockEsalesAdsService>(service);
			Assert.IsInstanceOf<BlockEsalesAdsService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesAdsService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockEsalesAdsService>();
			var instance2 = IoC.Resolve<IBlockEsalesAdsService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}