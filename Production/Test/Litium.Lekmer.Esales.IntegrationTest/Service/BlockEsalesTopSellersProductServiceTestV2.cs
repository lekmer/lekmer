﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.Service
{
	[TestFixture]
	public class BlockEsalesTopSellersProductServiceTestV2
	{
		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductServiceV2_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockEsalesTopSellersProductServiceV2>();

			Assert.IsInstanceOf<IBlockEsalesTopSellersProductServiceV2>(service);
			Assert.IsInstanceOf<BlockEsalesTopSellersProductServiceV2>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesTopSellersProductServiceV2_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockEsalesTopSellersProductServiceV2>();
			var instance2 = IoC.Resolve<IBlockEsalesTopSellersProductServiceV2>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}