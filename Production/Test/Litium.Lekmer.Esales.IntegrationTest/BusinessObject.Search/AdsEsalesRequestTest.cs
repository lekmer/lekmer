using System.Collections.Generic;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class AdsEsalesRequestTest
	{
		[Test]
		[Category("IoC")]
		public void AdsEsalesRequest_Resolve_Resolved()
		{
			var info = IoC.Resolve<IAdsEsalesRequest>();

			Assert.IsInstanceOf<IAdsEsalesRequest>(info);
			Assert.IsInstanceOf<AdsEsalesRequest>(info);
		}

		[Test]
		[Category("IoC")]
		public void AdsEsalesRequest_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IAdsEsalesRequest>();
			var esalesInfo2 = IoC.Resolve<IAdsEsalesRequest>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}

		[Test]
		public void AdsEsalesRequest_SetChannel_CorrectFilter()
		{
			var esalesRequest = IoC.Resolve<IAdsEsalesRequest>();
			esalesRequest.Site = "SE";

			Dictionary<string, string> arguments = esalesRequest.ComposeArguments();

			Assert.AreEqual("SE", arguments["site"]);
		}

		[Test]
		public void AdsEsalesRequest_SetFormatFilter_CorrectFilter()
		{
			var esalesRequest = IoC.Resolve<IAdsEsalesRequest>();
			esalesRequest.Site = "SE";

			esalesRequest.FormatList = new List<string>{"Main"};

			Dictionary<string, string> arguments = esalesRequest.ComposeArguments();

			Assert.AreEqual("(format:'Main')", arguments["ad_filter"]);
		}

		[Test]
		public void AdsEsalesRequest_SetAllFilters_CorrectFilter()
		{
			var esalesRequest = IoC.Resolve<IAdsEsalesRequest>();
			esalesRequest.Site = "SE";

			esalesRequest.FormatList = new List<string> { "Main" };
			esalesRequest.GenderList = new List<string> { "All", "Man" };
			esalesRequest.ProductCategoryList = new List<string> { "Toys", "Skor", "Bear" };

			Dictionary<string, string> arguments = esalesRequest.ComposeArguments();

			Assert.AreEqual("(format:'Main') AND (gender:'All' OR gender:'Man') AND (product_category:'Toys' OR product_category:'Skor' OR product_category:'Bear')", arguments["ad_filter"]);
		}
	}
}