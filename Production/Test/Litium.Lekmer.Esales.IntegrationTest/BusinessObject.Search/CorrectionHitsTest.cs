using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class CorrectionHitsTest
	{
		[Test]
		[Category("IoC")]
		public void CorrectionHits_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICorrectionHits>();

			Assert.IsInstanceOf<ICorrectionHits>(info);
			Assert.IsInstanceOf<CorrectionHits>(info);
		}

		[Test]
		[Category("IoC")]
		public void CorrectionHits_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<ICorrectionHits>();
			var esalesInfo2 = IoC.Resolve<ICorrectionHits>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}