using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class EsalesResponseTest
	{
		[Test]
		[Category("IoC")]
		public void EsalesResponse_Resolve_Resolved()
		{
			var info = IoC.Resolve<IEsalesResponse>();

			Assert.IsInstanceOf<IEsalesResponse>(info);
			Assert.IsInstanceOf<EsalesResponse>(info);
		}

		[Test]
		[Category("IoC")]
		public void EsalesResponse_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<IEsalesResponse>();
			var esalesInfo2 = IoC.Resolve<IEsalesResponse>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}