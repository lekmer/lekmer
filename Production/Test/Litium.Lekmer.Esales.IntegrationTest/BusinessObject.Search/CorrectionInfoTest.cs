using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Esales.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class CorrectionInfoTest
	{
		[Test]
		[Category("IoC")]
		public void CorrectionInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICorrectionInfo>();

			Assert.IsInstanceOf<ICorrectionInfo>(info);
			Assert.IsInstanceOf<CorrectionInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void CorrectionInfo_ResolveTwice_DifferentObjects()
		{
			var esalesInfo1 = IoC.Resolve<ICorrectionInfo>();
			var esalesInfo2 = IoC.Resolve<ICorrectionInfo>();

			Assert.AreNotEqual(esalesInfo1, esalesInfo2);
		}
	}
}