﻿using System.Data;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class AdFolderDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void AdFolderDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IAdFolder>(_dataReader);

			Assert.IsInstanceOf<AdFolderDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void AdFolderDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IAdFolder>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IAdFolder>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}