﻿using System.Data;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class AdDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void AdDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IAd>(_dataReader);

			Assert.IsInstanceOf<AdDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void AdDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IAd>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IAd>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}