﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Esales.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Esales.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockEsalesAdsDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesAdsDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockEsalesAds> dataMapper = DataMapperResolver.Resolve<IBlockEsalesAds>(_dataReader);

			Assert.IsInstanceOf<BlockEsalesAdsDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockEsalesAdsDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockEsalesAds> dataMapper1 = DataMapperResolver.Resolve<IBlockEsalesAds>(_dataReader);
			DataMapperBase<IBlockEsalesAds> dataMapper2 = DataMapperResolver.Resolve<IBlockEsalesAds>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}