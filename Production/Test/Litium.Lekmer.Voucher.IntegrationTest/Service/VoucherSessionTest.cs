﻿using Litium.Lekmer.Common;
using Litium.Lekmer.Core.Web;
using Litium.Lekmer.Voucher.Web;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Voucher.IntegrationTest.Service
{
	[TestFixture]
	public class VoucherSessionTest
	{
		private static MockRepository _mocker;

		[Test]
		[Category("IoC")]
		public void VoucherSession_Resolve_Resolved()
		{
			var service = IoC.Resolve<IVoucherSession>();

			Assert.IsInstanceOf<IVoucherSession>(service);
			Assert.IsInstanceOf<VoucherSession>(service);
		}

		[Test]
		[Category("IoC")]
		public void VoucherSession_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IVoucherSession>();
			var instance2 = IoC.Resolve<IVoucherSession>();

			Assert.AreEqual(instance1, instance2);
		}


		[SetUp]
		[Category("IoC")]
		public void SetUp()
		{
			_mocker = new MockRepository();
		}


		[Test]
		public void WasUsed_GetValueWhenSessionNotContain_ReturnDefaultValue()
		{
			var sessionStateService = _mocker.DynamicMock<ISessionStateService>();

			sessionStateService.Expect(s => s.Available).Return(true);
			sessionStateService.Expect(s => s["Key"]).Return(null).IgnoreArguments();

			_mocker.ReplayAll();

			var voucherSession = new VoucherSession(sessionStateService);

			Assert.AreEqual(false, voucherSession.WasUsed);

			sessionStateService.VerifyAllExpectations();
		}

		[Test]
		public void VoucherCode_GetValueWhenSessionNotContain_ReturnDefaultValue()
		{
			var sessionStateService = _mocker.DynamicMock<ISessionStateService>();

			sessionStateService.Expect(s => s.Available).Return(true);
			sessionStateService.Expect(s => s["Key"]).Return(null).IgnoreArguments();

			_mocker.ReplayAll();

			var voucherSession = new VoucherSession(sessionStateService);

			Assert.AreEqual(null, voucherSession.VoucherCode);

			sessionStateService.VerifyAllExpectations();
		}

		[Test]
		public void Voucher_GetValueWhenSessionNotContain_ReturnDefaultValue()
		{
			var sessionStateService = _mocker.DynamicMock<ISessionStateService>();

			sessionStateService.Expect(s => s.Available).Return(true);
			sessionStateService.Expect(s => s["Key"]).Return(null).IgnoreArguments();

			_mocker.ReplayAll();

			var voucherSession = new VoucherSession(sessionStateService);

			Assert.AreEqual(null, voucherSession.Voucher);

			sessionStateService.VerifyAllExpectations();
		}
	}
}