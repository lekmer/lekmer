﻿using Litium.Lekmer.Customer.Cache;
using Litium.Lekmer.Customer.SecureService;
using Litium.Lekmer.Customer.Service;
using Litium.Scensum.Core;
using Litium.Scensum.Customer;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Customer.UnitTest.Service
{
	[TestFixture]
	public class LekmerBlockSignInServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int BLOCK_ID_1 = 514;
		private static readonly int BLOCK_ID_2 = 515;
		private static readonly int BLOCK_ID_3 = 516;
		private static readonly int BLOCK_ID_4 = 517;

		private static MockRepository _mocker;
		private static IUserContext _userContext;
		private static IAccessValidator _accessValidator;
		private static ISystemUserFull _systemUser;
		private static BlockSignInRepository _blockSignInRepository;
		private static LekmerBlockSignInService _lekmerBlockSignInService;
		private static LekmerBlockSignInSecureService _lekmerBlockSignInSecureService;
		private static IChannel _channel;
		private static ILanguage _language;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_userContext = _mocker.Stub<IUserContext>();
			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUser = _mocker.Stub<ISystemUserFull>();
			_blockSignInRepository = _mocker.DynamicMock<BlockSignInRepository>();
			_lekmerBlockSignInService = new LekmerBlockSignInService(_blockSignInRepository);
			_lekmerBlockSignInSecureService = new LekmerBlockSignInSecureService(
				_accessValidator,
				_blockSignInRepository,
				_mocker.Stub<IAccessSecureService>(),
				_mocker.Stub<IBlockSecureService>());

			_language = _mocker.Stub<ILanguage>();
			_language.Id = CHANNEL_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			_userContext.Channel = _channel;

			BlockSignInCache.Instance.Flush();
		}

		[Test]
		public void GetById_SameId_CacheUsed()
		{
			Expect.Call(_blockSignInRepository.GetById(_channel, BLOCK_ID_1)).Return(new BlockSignIn { Id = BLOCK_ID_1 });

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_1, _lekmerBlockSignInService.GetById(_userContext, BLOCK_ID_1).Id);
			Assert.AreEqual(BLOCK_ID_1, _lekmerBlockSignInService.GetById(_userContext, BLOCK_ID_1).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_TwoId_CacheIsNotUsed()
		{
			Expect.Call(_blockSignInRepository.GetById(_channel, BLOCK_ID_2)).Return(new BlockSignIn { Id = BLOCK_ID_2 });
			Expect.Call(_blockSignInRepository.GetById(_channel, BLOCK_ID_3)).Return(new BlockSignIn { Id = BLOCK_ID_3 });

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_2, _lekmerBlockSignInService.GetById(_userContext, BLOCK_ID_2).Id);
			Assert.AreEqual(BLOCK_ID_3, _lekmerBlockSignInService.GetById(_userContext, BLOCK_ID_3).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_BlockDeleted_CacheRemoved()
		{
			IBlockSignIn blockSignIn = new BlockSignIn { Id = BLOCK_ID_4 };

			Expect.Call(() => _blockSignInRepository.Delete(BLOCK_ID_4));
			Expect.Call(_blockSignInRepository.GetById(_channel, BLOCK_ID_4)).Return(blockSignIn).Repeat.Twice();

			_mocker.ReplayAll();

			Assert.AreEqual(BLOCK_ID_4, _lekmerBlockSignInService.GetById(_userContext, BLOCK_ID_4).Id);
			_lekmerBlockSignInSecureService.Delete(_systemUser, BLOCK_ID_4);
			Assert.AreEqual(BLOCK_ID_4, _lekmerBlockSignInService.GetById(_userContext, BLOCK_ID_4).Id);

			_mocker.VerifyAll();
		}
	}
}