using Litium.Lekmer.Order;
using Litium.Lekmer.Product.Constant;
using Litium.Scensum.Foundation;

namespace Litium.Lekmer.Payment.Qliro.UnitTest
{
	public class OrderItemCreator
	{
		private readonly LekmerOrderItem _orderItem;

		public OrderItemCreator()
		{
			_orderItem = new LekmerOrderItem();
			_orderItem.ProductTypeId = (int)ProductType.Product;
			_orderItem.OrderItemSize = new OrderItemSize();
		}

		private LekmerOrderItem OrderItem
		{
			get { return _orderItem; }
		}

		public OrderItemCreator OriginalPrice(decimal includingVat, decimal excludingVat)
		{
			OrderItem.OriginalPrice = new Price(includingVat, excludingVat);

			return this;
		}

		public OrderItemCreator ActualPrice(decimal includingVat, decimal excludingVat)
		{
			OrderItem.ActualPrice = new Price(includingVat, excludingVat);

			return this;
		}

		public OrderItemCreator Quantity(int quantity)
		{
			OrderItem.Quantity = quantity;

			return this;
		}

		public LekmerOrderItem Create()
		{
			return OrderItem;
		}

		public static OrderItemCreator New()
		{
			return new OrderItemCreator();
		}
	}
}