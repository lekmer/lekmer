﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class PackageOrderItemSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void PackageOrderItemSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IPackageOrderItemSecureService>();

			Assert.IsInstanceOf<IPackageOrderItemSecureService>(service);
			Assert.IsInstanceOf<PackageOrderItemSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void PackageOrderItemSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IPackageOrderItemSecureService>();
			var service2 = IoC.Resolve<IPackageOrderItemSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}