﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerDeliveryMethodSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethodSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IDeliveryMethodSecureService>();

			Assert.IsInstanceOf<IDeliveryMethodSecureService>(service);
			Assert.IsInstanceOf<DeliveryMethodSecureService>(service);
			Assert.IsInstanceOf<LekmerDeliveryMethodSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethodSecureService_ResolveLekmer_Resolved()
		{
			var service = IoC.Resolve<ILekmerDeliveryMethodSecureService>();

			Assert.IsInstanceOf<IDeliveryMethodSecureService>(service);
			Assert.IsInstanceOf<DeliveryMethodSecureService>(service);
			Assert.IsInstanceOf<LekmerDeliveryMethodSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerDeliveryMethodSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IDeliveryMethodSecureService>();
			var service2 = IoC.Resolve<IDeliveryMethodSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}