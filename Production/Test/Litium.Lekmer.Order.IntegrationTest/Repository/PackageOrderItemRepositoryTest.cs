﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class PackageOrderItemRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void PackageOrderItemRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<PackageOrderItemRepository>();

			Assert.IsInstanceOf<PackageOrderItemRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void PackageOrderItemRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<PackageOrderItemRepository>();
			var instance2 = IoC.Resolve<PackageOrderItemRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}