﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class DeliveryMethodWeightPriceRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void DeliveryMethodWeightPriceRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<DeliveryMethodWeightPriceRepository>();

			Assert.IsInstanceOf<DeliveryMethodWeightPriceRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void DeliveryMethodWeightPriceRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<DeliveryMethodWeightPriceRepository>();
			var instance2 = IoC.Resolve<DeliveryMethodWeightPriceRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}