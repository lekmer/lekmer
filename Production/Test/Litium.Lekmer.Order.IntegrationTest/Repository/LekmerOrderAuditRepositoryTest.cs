﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order.Repository;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerOrderAuditRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderAuditRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<OrderAuditRepository>();

			Assert.IsInstanceOf<OrderAuditRepository>(repository);
			Assert.IsInstanceOf<LekmerOrderAuditRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderAuditRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<OrderAuditRepository>();
			var instance2 = IoC.Resolve<OrderAuditRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}