﻿using Litium.Lekmer.Order.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Repository
{
	[TestFixture]
	public class OrderVoucherInfoRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void OrderVoucherInfoRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<OrderVoucherInfoRepository>();

			Assert.IsInstanceOf<OrderVoucherInfoRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void OrderVoucherInfoRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<OrderVoucherInfoRepository>();
			var instance2 = IoC.Resolve<OrderVoucherInfoRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}