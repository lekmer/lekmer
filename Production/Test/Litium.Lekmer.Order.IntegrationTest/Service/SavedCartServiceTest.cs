﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class SavedCartServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SavedCartService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISavedCartObjectService>();

			Assert.IsInstanceOf<ISavedCartObjectService>(service);
			Assert.IsInstanceOf<SavedCartObjectService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SavedCartService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ISavedCartObjectService>();
			var instance2 = IoC.Resolve<ISavedCartObjectService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}