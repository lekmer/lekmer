﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class OrderVoucherInfoServiceTest
	{
		[Test]
		[Category("IoC")]
		public void OrderVoucherInfoService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderVoucherInfoService>();

			Assert.IsInstanceOf<IOrderVoucherInfoService>(service);
			Assert.IsInstanceOf<OrderVoucherInfoService>(service);
		}

		[Test]
		[Category("IoC")]
		public void OrderVoucherInfoService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderVoucherInfoService>();
			var service2 = IoC.Resolve<IOrderVoucherInfoService>();

			Assert.AreEqual(service1, service2);
		}
	}
}