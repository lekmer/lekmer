﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.Service
{
	[TestFixture]
	public class DeliveryMethodWeightPriceServiceTest
	{
		[Test]
		[Category("IoC")]
		public void DeliveryMethodWeightPriceService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IDeliveryMethodWeightPriceService>();

			Assert.IsInstanceOf<IDeliveryMethodWeightPriceService>(service);
			Assert.IsInstanceOf<DeliveryMethodWeightPriceService>(service);
		}

		[Test]
		[Category("IoC")]
		public void DeliveryMethodWeightPriceService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IDeliveryMethodWeightPriceService>();
			var instance2 = IoC.Resolve<IDeliveryMethodWeightPriceService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}