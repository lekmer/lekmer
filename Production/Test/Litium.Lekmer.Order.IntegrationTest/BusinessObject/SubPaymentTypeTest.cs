﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SubPaymentTypeTest
	{
		[Test]
		[Category("IoC")]
		public void SubPaymentType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISubPaymentType>();
			Assert.IsInstanceOf<ISubPaymentType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SubPaymentType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISubPaymentType>();
			var instance2 = IoC.Resolve<ISubPaymentType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}