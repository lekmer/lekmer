﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CheckoutProfileTest
	{
		[Test]
		[Category("IoC")]
		public void CheckoutProfile_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ICheckoutProfile>();

			Assert.IsInstanceOf<ICheckoutProfile>(instance);
			Assert.IsInstanceOf<CheckoutProfile>(instance);
		}

		[Test]
		[Category("IoC")]
		public void CheckoutProfile_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ICheckoutProfile>();
			var instance2 = IoC.Resolve<ICheckoutProfile>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}