﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProfileCustomerInformationTest
	{
		[Test]
		[Category("IoC")]
		public void ProfileCustomerInformation_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProfileCustomerInformation>();

			Assert.IsInstanceOf<IProfileCustomerInformation>(instance);
			Assert.IsInstanceOf<ProfileCustomerInformation>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProfileCustomerInformation_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProfileCustomerInformation>();
			var instance2 = IoC.Resolve<IProfileCustomerInformation>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}