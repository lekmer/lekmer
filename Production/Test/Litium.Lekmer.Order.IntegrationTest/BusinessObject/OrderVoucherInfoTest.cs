﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class OrderVoucherInfoTest
	{
		[Test]
		[Category("IoC")]
		public void OrderVoucherInfo_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IOrderVoucherInfo>();

			Assert.IsInstanceOf<IOrderVoucherInfo>(instance);
			Assert.IsInstanceOf<OrderVoucherInfo>(instance);
		}

		[Test]
		[Category("IoC")]
		public void OrderVoucherInfo_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IOrderVoucherInfo>();
			var instance2 = IoC.Resolve<IOrderVoucherInfo>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}