﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Order;
using NUnit.Framework;

namespace Litium.Lekmer.Order.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerOrderPaymentTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerOrderPayment_Resolve_Resolved()
		{
			var address = IoC.Resolve<IOrderPayment>();

			Assert.IsInstanceOf<IOrderPayment>(address);
			Assert.IsInstanceOf<ILekmerOrderPayment>(address);
		}

		[Test]
		[Category("IoC")]
		public void LekmerOrderPayment_ResolveTwice_DifferentObjects()
		{
			var address1 = IoC.Resolve<IOrderPayment>();
			var address2 = IoC.Resolve<IOrderPayment>();

			Assert.AreNotEqual(address1, address2);
		}
	}
}
