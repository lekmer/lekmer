﻿using System.Data;
using Litium.Lekmer.Order.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Order.IntegrationTest.Mapper
{
	[TestFixture]
	public class SavedCartDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void SavedCartDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ISavedCartObject>(_dataReader);
			Assert.IsInstanceOf<SavedCartObjectDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void SavedCartDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ISavedCartObject>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ISavedCartObject>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}