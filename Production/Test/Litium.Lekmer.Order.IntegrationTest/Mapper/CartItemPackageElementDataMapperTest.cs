﻿using System.Data;
using Litium.Lekmer.Order.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Order.IntegrationTest.Mapper
{
	[TestFixture]
	public class CartItemPackageElementDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void CartItemPackageElementDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ICartItemPackageElement>(_dataReader);

			Assert.IsInstanceOf<CartItemPackageElementDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void CartItemPackageElementDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ICartItemPackageElement>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ICartItemPackageElement>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}