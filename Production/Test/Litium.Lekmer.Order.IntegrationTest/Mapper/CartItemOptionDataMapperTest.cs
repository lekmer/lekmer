﻿using System.Data;
using Litium.Lekmer.Contract;
using Litium.Lekmer.Order.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Order.IntegrationTest.Mapper
{
	[TestFixture]
	public class CartItemOptionDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void CartItemOptionDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ICartItemOption>(_dataReader);

			Assert.IsInstanceOf<CartItemOptionDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void CartItemOptionDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ICartItemOption>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ICartItemOption>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}