﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockRatingDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockRating> blockRatingDataMapper = DataMapperResolver.Resolve<IBlockRating>(_dataReader);

			Assert.IsInstanceOf<BlockRatingDataMapper>(blockRatingDataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockRating> blockRatingDataMapper1 = DataMapperResolver.Resolve<IBlockRating>(_dataReader);
			DataMapperBase<IBlockRating> blockRatingDataMapper2 = DataMapperResolver.Resolve<IBlockRating>(_dataReader);

			Assert.AreNotEqual(blockRatingDataMapper1, blockRatingDataMapper2);
		}
	}
}