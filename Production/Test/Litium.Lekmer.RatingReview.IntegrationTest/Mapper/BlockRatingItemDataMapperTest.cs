﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockRatingItemDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingItemDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockRatingItem> dataMapper = DataMapperResolver.Resolve<IBlockRatingItem>(_dataReader);

			Assert.IsInstanceOf<BlockRatingItemDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingItemDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockRatingItem> dataMapper1 = DataMapperResolver.Resolve<IBlockRatingItem>(_dataReader);
			DataMapperBase<IBlockRatingItem> dataMapper2 = DataMapperResolver.Resolve<IBlockRatingItem>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}