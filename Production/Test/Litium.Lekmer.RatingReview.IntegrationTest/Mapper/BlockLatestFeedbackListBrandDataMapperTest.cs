﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockLatestFeedbackListBrandDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrandDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockLatestFeedbackListBrand> dataMapper = DataMapperResolver.Resolve<IBlockLatestFeedbackListBrand>(_dataReader);

			Assert.IsInstanceOf<BlockLatestFeedbackListBrandDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListBrandDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockLatestFeedbackListBrand> dataMapper1 = DataMapperResolver.Resolve<IBlockLatestFeedbackListBrand>(_dataReader);
			DataMapperBase<IBlockLatestFeedbackListBrand> dataMapper2 = DataMapperResolver.Resolve<IBlockLatestFeedbackListBrand>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}