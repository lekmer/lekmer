﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.RatingReview.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockLatestFeedbackListDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListDataMapper_Resolve_Resolved()
		{
			DataMapperBase<IBlockLatestFeedbackList> dataMapper = DataMapperResolver.Resolve<IBlockLatestFeedbackList>(_dataReader);

			Assert.IsInstanceOf<BlockLatestFeedbackListDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockLatestFeedbackListDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<IBlockLatestFeedbackList> dataMapper1 = DataMapperResolver.Resolve<IBlockLatestFeedbackList>(_dataReader);
			DataMapperBase<IBlockLatestFeedbackList> dataMapper2 = DataMapperResolver.Resolve<IBlockLatestFeedbackList>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}