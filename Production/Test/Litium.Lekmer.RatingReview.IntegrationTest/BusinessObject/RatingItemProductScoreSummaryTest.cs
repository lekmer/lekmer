﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingItemProductScoreSummaryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemProductScoreSummary_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingItemProductScoreSummary>();

			Assert.IsInstanceOf<IRatingItemProductScoreSummary>(instance);
			Assert.IsInstanceOf<IRatingItemProductScoreSummary>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemProductScoreSummary_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingItemProductScoreSummary>();
			var instance2 = IoC.Resolve<IRatingItemProductScoreSummary>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}