﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductMostHelpfulRatingTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductMostHelpfulRating_Resolve_Resolved()
		{
			IBlockProductMostHelpfulRating block = IoC.Resolve<IBlockProductMostHelpfulRating>();

			Assert.IsInstanceOf<IBlockProductMostHelpfulRating>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductMostHelpfulRating_ResolveTwice_DifferentObjects()
		{
			IBlockProductMostHelpfulRating block1 = IoC.Resolve<IBlockProductMostHelpfulRating>();
			IBlockProductMostHelpfulRating block2 = IoC.Resolve<IBlockProductMostHelpfulRating>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}