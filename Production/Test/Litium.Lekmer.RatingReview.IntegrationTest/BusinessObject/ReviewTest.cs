﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ReviewTest
	{
		[Test]
		[Category("IoC")]
		public void Review_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IReview>();

			Assert.IsInstanceOf<IReview>(instance);
			Assert.IsInstanceOf<IReview>(instance);
		}

		[Test]
		[Category("IoC")]
		public void Review_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IReview>();
			var instance2 = IoC.Resolve<IReview>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}