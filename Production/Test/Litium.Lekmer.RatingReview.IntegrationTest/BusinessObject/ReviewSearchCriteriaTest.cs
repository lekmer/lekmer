﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ReviewSearchCriteriaTest
	{
		[Test]
		[Category("IoC")]
		public void ReviewSearchCriteria_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IReviewSearchCriteria>();

			Assert.IsInstanceOf<IReviewSearchCriteria>(instance);
			Assert.IsInstanceOf<IReviewSearchCriteria>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ReviewSearchCriteria_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IReviewSearchCriteria>();
			var instance2 = IoC.Resolve<IReviewSearchCriteria>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}