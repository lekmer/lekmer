﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingReviewUserTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewUser_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRatingReviewUser>();

			Assert.IsInstanceOf<IRatingReviewUser>(instance);
			Assert.IsInstanceOf<IRatingReviewUser>(instance);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewUser_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewUser>();
			var instance2 = IoC.Resolve<IRatingReviewUser>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}