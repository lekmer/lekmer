﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockRatingTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRating_Resolve_Resolved()
		{
			IBlockRating blockRating = IoC.Resolve<IBlockRating>();

			Assert.IsInstanceOf<IBlockRating>(blockRating);
		}

		[Test]
		[Category("IoC")]
		public void BlockRating_ResolveTwice_DifferentObjects()
		{
			IBlockRating blockRating1 = IoC.Resolve<IBlockRating>();
			IBlockRating blockRating2 = IoC.Resolve<IBlockRating>();

			Assert.AreNotEqual(blockRating1, blockRating2);
		}
	}
}