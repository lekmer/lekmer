﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class RatingTest
	{
		[Test]
		[Category("IoC")]
		public void Rating_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IRating>();

			Assert.IsInstanceOf<IRating>(instance);
			Assert.IsInstanceOf<IRating>(instance);
		}

		[Test]
		[Category("IoC")]
		public void Rating_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IRating>();
			var instance2 = IoC.Resolve<IRating>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}