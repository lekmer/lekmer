﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingSecureService>();

			Assert.IsInstanceOf<IRatingSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingSecureService>();
			var instance2 = IoC.Resolve<IRatingSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}