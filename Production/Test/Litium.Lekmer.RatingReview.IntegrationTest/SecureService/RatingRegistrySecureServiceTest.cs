﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingRegistrySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingRegistrySecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingRegistrySecureService>();

			Assert.IsInstanceOf<IRatingRegistrySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingRegistrySecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingRegistrySecureService>();
			var instance2 = IoC.Resolve<IRatingRegistrySecureService>();

			Assert.AreEqual(instance1, instance2);
		}

		[Test]
		[Category("Database.Get")]
		public void GetAll_GetAllRatingRegistries_AllReturned()
		{
			var service = IoC.Resolve<IRatingRegistrySecureService>();

			var ratingRegistries = service.GetAll();

			Assert.IsNotNull(ratingRegistries, "Collection<IRatingRegistry>");
			Assert.Greater(ratingRegistries.Count, 0, "Collection<IRatingRegistry>.Count");

			foreach (var ratingRegistry in ratingRegistries)
			{
				Assert.IsNotNullOrEmpty(ratingRegistry.CommonName);
				Assert.IsNotNullOrEmpty(ratingRegistry.Title);
			}
		}
	}
}