﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.SecureService
{
	[TestFixture]
	public class RatingGroupSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingGroupSecureService>();

			Assert.IsInstanceOf<IRatingGroupSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupSecureService>();
			var instance2 = IoC.Resolve<IRatingGroupSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}