﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingGroupProductRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupProductRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingGroupProductRepository>();

			Assert.IsInstanceOf<RatingGroupProductRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupProductRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingGroupProductRepository>();
			var instance2 = IoC.Resolve<RatingGroupProductRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}