﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingGroupCategoryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupCategoryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingGroupCategoryRepository>();

			Assert.IsInstanceOf<RatingGroupCategoryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupCategoryRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingGroupCategoryRepository>();
			var instance2 = IoC.Resolve<RatingGroupCategoryRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}