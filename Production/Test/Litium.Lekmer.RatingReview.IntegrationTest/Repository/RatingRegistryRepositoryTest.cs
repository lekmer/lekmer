﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingRegistryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingRegistryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingRegistryRepository>();

			Assert.IsInstanceOf<RatingRegistryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingRegistryRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingRegistryRepository>();
			var instance2 = IoC.Resolve<RatingRegistryRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}