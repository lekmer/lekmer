﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockRatingGroupRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingGroupRepository_Resolve_Resolved()
		{
			BlockRatingGroupRepository blockRatingGroupRepository = IoC.Resolve<BlockRatingGroupRepository>();

			Assert.IsInstanceOf<BlockRatingGroupRepository>(blockRatingGroupRepository);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingGroupRepository_ResolveTwice_SameObjects()
		{
			BlockRatingGroupRepository blockRatingGroupRepository1 = IoC.Resolve<BlockRatingGroupRepository>();
			BlockRatingGroupRepository blockRatingGroupRepository2 = IoC.Resolve<BlockRatingGroupRepository>();

			Assert.AreEqual(blockRatingGroupRepository1, blockRatingGroupRepository2);
		}
	}
}