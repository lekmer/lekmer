﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockRatingItemRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingItemRepository_Resolve_Resolved()
		{
			BlockRatingItemRepository repository = IoC.Resolve<BlockRatingItemRepository>();

			Assert.IsInstanceOf<BlockRatingItemRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingItemRepository_ResolveTwice_SameObjects()
		{
			BlockRatingItemRepository repository1 = IoC.Resolve<BlockRatingItemRepository>();
			BlockRatingItemRepository repository2 = IoC.Resolve<BlockRatingItemRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}