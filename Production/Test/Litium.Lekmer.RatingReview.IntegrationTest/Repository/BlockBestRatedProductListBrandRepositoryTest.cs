﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockBestRatedProductListBrandRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrandRepository_Resolve_Resolved()
		{
			BlockBestRatedProductListBrandRepository repository = IoC.Resolve<BlockBestRatedProductListBrandRepository>();

			Assert.IsInstanceOf<BlockBestRatedProductListBrandRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockBestRatedProductListBrandRepository_ResolveTwice_SameObjects()
		{
			BlockBestRatedProductListBrandRepository repository1 = IoC.Resolve<BlockBestRatedProductListBrandRepository>();
			BlockBestRatedProductListBrandRepository repository2 = IoC.Resolve<BlockBestRatedProductListBrandRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}