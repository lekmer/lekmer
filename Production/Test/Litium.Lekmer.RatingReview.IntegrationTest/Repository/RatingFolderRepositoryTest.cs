﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingFolderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingFolderRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingFolderRepository>();

			Assert.IsInstanceOf<RatingFolderRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingFolderRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingFolderRepository>();
			var instance2 = IoC.Resolve<RatingFolderRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}