﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingItemRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingItemRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingItemRepository>();

			Assert.IsInstanceOf<RatingItemRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingItemRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingItemRepository>();
			var instance2 = IoC.Resolve<RatingItemRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}