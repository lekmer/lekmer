﻿using Litium.Lekmer.RatingReview.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Repository
{
	[TestFixture]
	public class RatingGroupRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<RatingGroupRepository>();

			Assert.IsInstanceOf<RatingGroupRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<RatingGroupRepository>();
			var instance2 = IoC.Resolve<RatingGroupRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}