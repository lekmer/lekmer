﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingReviewFeedbackLikeServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackLikeService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingReviewFeedbackLikeService>();

			Assert.IsInstanceOf<IRatingReviewFeedbackLikeService>(service);
			Assert.IsInstanceOf<RatingReviewFeedbackLikeService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingReviewFeedbackLikeService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingReviewFeedbackLikeService>();
			var instance2 = IoC.Resolve<IRatingReviewFeedbackLikeService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}