﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class ProductRatingSummaryProcessorTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRatingSummaryProcessor_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductRatingSummaryProcessor>();

			Assert.IsInstanceOf<IProductRatingSummaryProcessor>(service);
			Assert.IsInstanceOf<ProductRatingSummaryProcessor>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductRatingSummaryProcessor_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductRatingSummaryProcessor>();
			var instance2 = IoC.Resolve<IProductRatingSummaryProcessor>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}