﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockProductFeedbackServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductFeedbackService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductFeedbackService>();

			Assert.IsInstanceOf<IBlockProductFeedbackService>(service);
			Assert.IsInstanceOf<BlockProductFeedbackService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductFeedbackService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductFeedbackService>();
			var instance2 = IoC.Resolve<IBlockProductFeedbackService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}