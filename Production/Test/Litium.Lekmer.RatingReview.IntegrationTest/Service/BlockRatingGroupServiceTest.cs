﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockRatingGroupServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingGroupService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockRatingGroupService>();

			Assert.IsInstanceOf<IBlockRatingGroupService>(service);
			Assert.IsInstanceOf<BlockRatingGroupService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingGroupService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockRatingGroupService>();
			var instance2 = IoC.Resolve<IBlockRatingGroupService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}