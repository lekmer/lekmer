﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockRatingItemServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockRatingItemService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockRatingItemService>();

			Assert.IsInstanceOf<IBlockRatingItemService>(service);
			Assert.IsInstanceOf<BlockRatingItemService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockRatingItemService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockRatingItemService>();
			var instance2 = IoC.Resolve<IBlockRatingItemService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}