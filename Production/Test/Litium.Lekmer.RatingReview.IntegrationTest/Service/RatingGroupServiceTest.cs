﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class RatingGroupServiceTest
	{
		[Test]
		[Category("IoC")]
		public void RatingGroupService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IRatingGroupService>();

			Assert.IsInstanceOf<IRatingGroupService>(service);
			Assert.IsInstanceOf<RatingGroupService>(service);
		}

		[Test]
		[Category("IoC")]
		public void RatingGroupService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IRatingGroupService>();
			var instance2 = IoC.Resolve<IRatingGroupService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}