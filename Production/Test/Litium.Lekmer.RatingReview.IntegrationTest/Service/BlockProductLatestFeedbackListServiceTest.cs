﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.RatingReview.IntegrationTest.Service
{
	[TestFixture]
	public class BlockProductLatestFeedbackListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductLatestFeedbackListService>();

			Assert.IsInstanceOf<IBlockProductLatestFeedbackListService>(service);
			Assert.IsInstanceOf<BlockProductLatestFeedbackListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductLatestFeedbackListService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductLatestFeedbackListService>();
			var instance2 = IoC.Resolve<IBlockProductLatestFeedbackListService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}