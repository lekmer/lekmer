﻿using Litium.Scensum.Foundation;
using Litium.Scensum.Product.Cache;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Cache
{
	[TestFixture]
	public class ProductCacheCleanerTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerProductCacheCleaner_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductCacheCleaner>();

			Assert.IsInstanceOf<IProductCacheCleaner>(service);
			Assert.IsInstanceOf<ProductCacheCleaner>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerProductCacheCleaner_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductCacheCleaner>();
			var service2 = IoC.Resolve<IProductCacheCleaner>();

			Assert.AreEqual(service1, service2);
		}
	}
}