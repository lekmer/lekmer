﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class SupplierServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SupplierService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ISupplierService>();

			Assert.IsInstanceOf<ISupplierService>(service);
			Assert.IsInstanceOf<SupplierService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SupplierService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ISupplierService>();
			var service2 = IoC.Resolve<ISupplierService>();

			Assert.AreEqual(service1, service2);
		}
	}
}