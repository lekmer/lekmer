﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class TagGroupServiceTest
	{
		private static readonly int CHANNEL_ID = 1;
		private static readonly int LANGUAGE_ID = 1;

		private MockRepository _mocker;
		private IUserContext _userContext;
		private IChannel _channel;
		private ILanguage _language;

		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{
			_mocker = new MockRepository();

			_userContext = _mocker.Stub<IUserContext>();

			_language = _mocker.Stub<ILanguage>();
			_language.Id = LANGUAGE_ID;

			_channel = _mocker.Stub<IChannel>();
			_channel.Id = CHANNEL_ID;
			_channel.Language = _language;

			_userContext.Channel = _channel;
		}

		[Test]
		[Category("IoC")]
		public void TagGroupService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ITagGroupService>();

			Assert.IsInstanceOf<ITagGroupService>(service);
			Assert.IsInstanceOf<TagGroupService>(service);
		}

		[Test]
		[Category("IoC")]
		public void TagGroupService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ITagGroupService>();
			var service2 = IoC.Resolve<ITagGroupService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void GetAll_ExecuteGetAllByContext_ReturnedList()
		{
			var service = IoC.Resolve<ITagGroupService>();

			Stopwatch timer = Stopwatch.StartNew();
			Collection<ITagGroup> tags = service.GetAll(_userContext);
			timer.Stop();

			string s = timer.Elapsed.ToString();

			Assert.IsNotNull(tags, "Tags collection is null");
			Assert.IsTrue(tags.Count > 0, "Tags collection is empty");
		}

		[Test]
		public void GetAll_UseTagsGetAll_CheckExecutionTime()
		{
			var tagGroupRepository = new TagGroupRepository();
			var tagRepository = new TagRepository();

			Stopwatch timer = Stopwatch.StartNew();

			Collection<ITagGroup> tagGroups = tagGroupRepository.GetAll();
			Collection<ITag> tags = tagRepository.GetAll(LANGUAGE_ID);

			Stopwatch timerGrouping = Stopwatch.StartNew();

			Dictionary<int, Collection<ITag>> tagsDictionary = tags.GroupBy(t => t.TagGroupId).ToDictionary(t => t.Key, t => new Collection<ITag>(t.ToArray()));

			foreach (ITagGroup tagGroup in tagGroups)
			{
				if (tagsDictionary.ContainsKey(tagGroup.Id))
				{
					tagGroup.Tags = tagsDictionary[tagGroup.Id];
				}
				else
				{
					tagGroup.Tags = new Collection<ITag>();
				}
			}

			timerGrouping.Stop();
			timer.Stop();

			string s = timer.Elapsed.ToString();
			string s1 = timerGrouping.Elapsed.ToString();

			Assert.IsNotNull(tags, "Tags collection is null");
			Assert.IsTrue(tags.Count > 0, "Tags collection is empty");
		}

		[Test]
		public void GetAll_UseTagsGetAllByGroup_CheckExecutionTime()
		{
			var tagGroupRepository = new TagGroupRepository();
			var tagRepository = new TagRepository();

			Stopwatch timer = Stopwatch.StartNew();

			Collection<ITagGroup> tagGroups = tagGroupRepository.GetAll();

			foreach (ITagGroup tagGroup in tagGroups)
			{
				tagGroup.Tags = tagRepository.GetAllByTagGroup(LANGUAGE_ID, tagGroup.Id);
			}

			timer.Stop();

			string s = timer.Elapsed.ToString();

			Assert.IsNotNull(tagGroups, "Tag groups collection is null");
			Assert.IsTrue(tagGroups.Count > 0, "Tag groups collection is empty");
		}
	}
}