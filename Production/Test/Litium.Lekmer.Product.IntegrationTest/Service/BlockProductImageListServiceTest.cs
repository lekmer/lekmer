﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class BlockProductImageListServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductImageListService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductImageListService>();

			Assert.IsInstanceOf<IBlockProductImageListService>(service);
			Assert.IsInstanceOf<BlockProductImageListService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductImageListService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockProductImageListService>();
			var service2 = IoC.Resolve<IBlockProductImageListService>();

			Assert.AreEqual(service1, service2);
		}
	}
}