﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Service
{
	[TestFixture]
	public class ContentMessageServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ContentMessageService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IContentMessageService>();

			Assert.IsInstanceOf<IContentMessageService>(service);
			Assert.IsInstanceOf<ContentMessageService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IContentMessageService>();
			var service2 = IoC.Resolve<IContentMessageService>();

			Assert.AreEqual(service1, service2);
		}
	}
}