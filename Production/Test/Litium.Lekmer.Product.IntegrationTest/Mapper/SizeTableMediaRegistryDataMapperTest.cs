﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class SizeTableMediaRegistryDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaRegistryDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ISizeTableMediaRegistry>(_dataReader);

			Assert.IsInstanceOf<SizeTableMediaRegistryDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaRegistryDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ISizeTableMediaRegistry>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ISizeTableMediaRegistry>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}