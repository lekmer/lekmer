﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class SizeTableDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void SizeTableDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<ISizeTable>(_dataReader);

			Assert.IsInstanceOf<SizeTableDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<ISizeTable>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<ISizeTable>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}