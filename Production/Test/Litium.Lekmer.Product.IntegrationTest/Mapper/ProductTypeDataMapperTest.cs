﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class ProductTypeDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void ProductType_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IProductType>(_dataReader);

			Assert.IsInstanceOf<ProductTypeDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void ProductType_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IProductType>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IProductType>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}