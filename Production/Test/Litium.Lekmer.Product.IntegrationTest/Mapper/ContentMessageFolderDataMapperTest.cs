﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class ContentMessageFolderDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageFolderDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IContentMessageFolder>(_dataReader);

			Assert.IsInstanceOf<ContentMessageFolderDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageFolderDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IContentMessageFolder>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IContentMessageFolder>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}