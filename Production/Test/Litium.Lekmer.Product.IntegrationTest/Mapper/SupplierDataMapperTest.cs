﻿using System.Data;
using Litium.Lekmer.Product.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Product.IntegrationTest.Mapper
{
	[TestFixture]
	public class SupplierDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void Supplier_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<ISupplier>(_dataReader);

			Assert.IsInstanceOf<SupplierDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void Supplier_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<ISupplier>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<ISupplier>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}