﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject.Search
{
	[TestFixture]
	public class BlockProductSearchResultTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSearchResult_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockProductSearchResult>();

			Assert.IsInstanceOf<IBlockProductSearchResult>(block);
			Assert.IsInstanceOf<BlockProductSearchResult>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchResult_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockProductSearchResult>();
			var block2 = IoC.Resolve<IBlockProductSearchResult>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}