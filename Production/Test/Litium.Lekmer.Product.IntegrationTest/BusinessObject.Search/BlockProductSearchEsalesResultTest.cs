﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockProductSearchEsalesResultTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResult_Resolve_Resolved()
		{
			var block = IoC.Resolve<IBlockProductSearchEsalesResult>();

			Assert.IsInstanceOf<IBlockProductSearchEsalesResult>(block);
			Assert.IsInstanceOf<BlockProductSearchEsalesResult>(block);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchEsalesResult_ResolveTwice_DifferentObjects()
		{
			var block1 = IoC.Resolve<IBlockProductSearchEsalesResult>();
			var block2 = IoC.Resolve<IBlockProductSearchEsalesResult>();

			Assert.AreNotEqual(block1, block2);
		}
	}
}