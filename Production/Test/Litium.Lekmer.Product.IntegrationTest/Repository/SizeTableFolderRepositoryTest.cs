﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class SizeTableFolderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableFolderRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SizeTableFolderRepository>();

			Assert.IsInstanceOf<SizeTableFolderRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableFolderRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<SizeTableFolderRepository>();
			var instance2 = IoC.Resolve<SizeTableFolderRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}