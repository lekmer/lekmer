﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class SizeTableRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SizeTableRepository>();

			Assert.IsInstanceOf<SizeTableRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<SizeTableRepository>();
			var instance2 = IoC.Resolve<SizeTableRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}