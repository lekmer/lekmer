﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockProductRelationListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductRelationListRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockProductRelationListRepository>();

			Assert.IsInstanceOf<BlockProductRelationListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductRelationListRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<BlockProductRelationListRepository>();
			var instance2 = IoC.Resolve<BlockProductRelationListRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}