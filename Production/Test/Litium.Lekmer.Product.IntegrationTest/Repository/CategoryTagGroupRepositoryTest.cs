﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class CategoryTagGroupRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CategoryTagGroupRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<CategoryTagGroupRepository>();

			Assert.IsInstanceOf<CategoryTagGroupRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void CategoryTagGroupRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<CategoryTagGroupRepository>();
			var repository2 = IoC.Resolve<CategoryTagGroupRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}