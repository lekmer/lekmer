﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ColorRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ColorRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ColorRepository>();

			Assert.IsInstanceOf<ColorRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ColorRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ColorRepository>();
			var repository2 = IoC.Resolve<ColorRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}