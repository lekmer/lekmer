﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockProductListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductListRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockProductListRepository>();

			Assert.IsInstanceOf<BlockProductListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductListRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<BlockProductListRepository>();
			var instance2 = IoC.Resolve<BlockProductListRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}