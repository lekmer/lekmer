﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class WishListPackageItemRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void WishListPackageItemRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<WishListPackageItemRepository>();

			Assert.IsInstanceOf<WishListPackageItemRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void WishListPackageItemRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<WishListPackageItemRepository>();
			var instance2 = IoC.Resolve<WishListPackageItemRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}