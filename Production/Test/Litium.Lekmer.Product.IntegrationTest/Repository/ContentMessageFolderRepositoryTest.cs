﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ContentMessageFolderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ContentMessageFolderRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ContentMessageFolderRepository>();

			Assert.IsInstanceOf<ContentMessageFolderRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageFolderRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ContentMessageFolderRepository>();
			var instance2 = IoC.Resolve<ContentMessageFolderRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}