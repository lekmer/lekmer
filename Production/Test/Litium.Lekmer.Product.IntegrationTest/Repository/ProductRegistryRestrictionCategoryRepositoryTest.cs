﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductRegistryRestrictionCategoryRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionCategoryRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductRegistryRestrictionCategoryRepository>();

			Assert.IsInstanceOf<ProductRegistryRestrictionCategoryRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionCategoryRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductRegistryRestrictionCategoryRepository>();
			var repository2 = IoC.Resolve<ProductRegistryRestrictionCategoryRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}