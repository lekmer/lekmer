﻿using Litium.Lekmer.Product.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.Repository
{
	[TestFixture]
	public class SizeTableRowRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableRowRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<SizeTableRowRepository>();

			Assert.IsInstanceOf<SizeTableRowRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableRowRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<SizeTableRowRepository>();
			var instance2 = IoC.Resolve<SizeTableRowRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}