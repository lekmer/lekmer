﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService.Search
{
	[TestFixture]
	public class BlockProductSearchResultSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSearchResultSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockProductSearchResultSecureService>();

			Assert.IsInstanceOf<IBlockProductSearchResultSecureService>(service);
			Assert.IsInstanceOf<BlockProductSearchResultSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSearchResultSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockProductSearchResultSecureService>();
			var instance2 = IoC.Resolve<IBlockProductSearchResultSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}