﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductRegistryRestrictionCategorySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionCategorySecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductRegistryRestrictionCategorySecureService>();

			Assert.IsInstanceOf<IProductRegistryRestrictionCategorySecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionCategorySecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductRegistryRestrictionCategorySecureService>();
			var instance2 = IoC.Resolve<IProductRegistryRestrictionCategorySecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}