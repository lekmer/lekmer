﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class BlockPackageProductListSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockPackageProductListSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockPackageProductListSecureService>();

			Assert.IsInstanceOf<IBlockPackageProductListSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockPackageProductListSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockPackageProductListSecureService>();
			var instance2 = IoC.Resolve<IBlockPackageProductListSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}