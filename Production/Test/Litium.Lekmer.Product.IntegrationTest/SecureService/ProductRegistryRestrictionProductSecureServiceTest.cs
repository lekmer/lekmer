﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class ProductRegistryRestrictionProductSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionProductSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductRegistryRestrictionProductSecureService>();

			Assert.IsInstanceOf<IProductRegistryRestrictionProductSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductRegistryRestrictionProductSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IProductRegistryRestrictionProductSecureService>();
			var instance2 = IoC.Resolve<IProductRegistryRestrictionProductSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}