﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.SecureService
{
	[TestFixture]
	public class CategoryTagGroupSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CategoryTagGroupSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICategoryTagGroupSecureService>();

			Assert.IsInstanceOf<ICategoryTagGroupSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CategoryTagGroupSecureService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ICategoryTagGroupSecureService>();
			var instance2 = IoC.Resolve<ICategoryTagGroupSecureService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}