﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class BlockPackageProductListTest
	{
		[Test]
		[Category("IoC")]
		public void BlockPackageProductList_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IBlockPackageProductList>();

			Assert.IsInstanceOf<IBlockPackageProductList>(instance);
		}

		[Test]
		[Category("IoC")]
		public void BlockPackageProductList_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IBlockPackageProductList>();
			var instance2 = IoC.Resolve<IBlockPackageProductList>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}