﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ContentMessageTranslationTest
	{
		[Test]
		[Category("IoC")]
		public void ContentMessageTranslation_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IContentMessageTranslation>();

			Assert.IsInstanceOf<IContentMessageTranslation>(instance);
			Assert.IsInstanceOf<IContentMessageTranslation>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ContentMessageTranslation_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IContentMessageTranslation>();
			var instance2 = IoC.Resolve<IContentMessageTranslation>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}