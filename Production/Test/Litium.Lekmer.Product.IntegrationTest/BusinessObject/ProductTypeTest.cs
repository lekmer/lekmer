﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductTypeTest
	{
		[Test]
		[Category("IoC")]
		public void ProductType_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IProductType>();

			Assert.IsInstanceOf<IProductType>(instance);
		}

		[Test]
		[Category("IoC")]
		public void ProductType_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IProductType>();
			var instance2 = IoC.Resolve<IProductType>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}