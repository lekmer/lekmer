﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class ProductUrlHistoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductUrlHistory_Resolve_Resolved()
		{
			var productUrlHistory = IoC.Resolve<IProductUrlHistory>();

			Assert.IsInstanceOf<IProductUrlHistory>(productUrlHistory);
		}

		[Test]
		[Category("IoC")]
		public void ProductUrlHistory_ResolveTwice_DifferentObjects()
		{
			var productUrlHistory1 = IoC.Resolve<IProductUrlHistory>();
			var productUrlHistory2 = IoC.Resolve<IProductUrlHistory>();

			Assert.AreNotEqual(productUrlHistory1, productUrlHistory2);
		}
	}
}