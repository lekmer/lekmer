﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class StockRangeTest
	{
		[Test]
		[Category("IoC")]
		public void StockRange_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IStockRange>();
			Assert.IsInstanceOf<IStockRange>(instance);
		}

		[Test]
		[Category("IoC")]
		public void StockRange_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IStockRange>();
			var instance2 = IoC.Resolve<IStockRange>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}