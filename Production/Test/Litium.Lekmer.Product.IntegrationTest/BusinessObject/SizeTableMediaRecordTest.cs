﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableMediaRecordTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableMediaRecord_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTableMediaRecord>();

			Assert.IsInstanceOf<ISizeTableMediaRecord>(instance);
			Assert.IsInstanceOf<ISizeTableMediaRecord>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableMediaRecord_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableMediaRecord>();
			var instance2 = IoC.Resolve<ISizeTableMediaRecord>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}