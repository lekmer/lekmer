﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class WishListPackageItemTest
	{
		[Test]
		[Category("IoC")]
		public void WishListPackageItem_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IWishListPackageItem>();

			Assert.IsInstanceOf<IWishListPackageItem>(instance);
			Assert.IsInstanceOf<IWishListPackageItem>(instance);
		}

		[Test]
		[Category("IoC")]
		public void WishListPackageItem_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IWishListPackageItem>();
			var instance2 = IoC.Resolve<IWishListPackageItem>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}