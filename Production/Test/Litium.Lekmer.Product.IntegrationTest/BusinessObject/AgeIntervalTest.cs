﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class AgeIntervalTest
	{
		[Test]
		[Category("IoC")]
		public void AgeInterval_Resolve_Resolved()
		{
			var ageInterval = IoC.Resolve<IAgeInterval>();

			Assert.IsInstanceOf<IAgeInterval>(ageInterval);
		}

		[Test]
		[Category("IoC")]
		public void AgeInterval_ResolveTwice_DifferentObjects()
		{
			var ageInterval1 = IoC.Resolve<IAgeInterval>();
			var ageInterval2 = IoC.Resolve<IAgeInterval>();

			Assert.AreNotEqual(ageInterval1, ageInterval2);
		}
	}
}