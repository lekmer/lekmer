﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Product.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class SizeTableTranslationTest
	{
		[Test]
		[Category("IoC")]
		public void SizeTableTranslation_Resolve_Resolved()
		{
			var instance = IoC.Resolve<ISizeTableTranslation>();

			Assert.IsInstanceOf<ISizeTableTranslation>(instance);
			Assert.IsInstanceOf<ISizeTableTranslation>(instance);
		}

		[Test]
		[Category("IoC")]
		public void SizeTableTranslation_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<ISizeTableTranslation>();
			var instance2 = IoC.Resolve<ISizeTableTranslation>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}