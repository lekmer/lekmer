using System.Collections.Generic;
using System.Collections.ObjectModel;
using Litium.Lekmer.Product;
using Litium.Lekmer.ProductFilter;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Lekmer.ProductFilter.UnitTest
{
	public class FilterProductHelper
	{
		//private static readonly Random _random = new Random();
		private static readonly IEnumerable<FilterProduct> _fiveProducts = PopulateProducts();
		private static readonly Collection<IPriceInterval> _priceIntervals = PopulatePriceIntervals();
		private static readonly Collection<IAgeInterval> _ageIntervals = PopulateAgeIntervals();

		public const int BrioBrandId = 1;
		public const int LegoBrandId = 2;
		public const int SportCategoryId = 1;
		public const int FootballCategoryId = 11;
		public const int FloorballCategoryId = 12;
		public const int BookCategoryId = 2;
		public const int DollCategoryId = 3;
		public const int SummerTagId = 1;
		public const int WinterTagId = 2;
		public const int Size1Id = 1;
		public const int Size2Id = 2;

		public static FilterProduct CreateProduct(int id, int brandId, int ageFrom, int ageTo, decimal price, decimal discountPrice,
			int categoryId, int? parentCategoryId, int? tagId0, int? tagId1, int? sizeId0, int? sizeId1, int productTypeId)
		{
			var product = new FilterProduct
			{
				Id = id,
				BrandId = brandId,
				AgeFrom = ageFrom,
				AgeTo = ageTo,
				Price = price,
				DiscountPrice = discountPrice,
				CategoryId = categoryId,
				ProductTypeId = productTypeId
			};

			product.AddCategory(categoryId);

			if (parentCategoryId.HasValue)
			{
				product.AddCategory(parentCategoryId.Value);
			}

			if (tagId0.HasValue)
			{
				product.AddTag(tagId0.Value);
			}

			if (tagId1.HasValue)
			{
				product.AddTag(tagId1.Value);
			}

			if (sizeId0.HasValue)
			{
				product.AddSize(sizeId0.Value);
			}

			if (sizeId1.HasValue)
			{
				product.AddSize(sizeId1.Value);
			}

			return product;
		}

		public static IEnumerable<FilterProduct> PopulateProducts()
		{
			return new List<FilterProduct>
			{
				CreateProduct(1, BrioBrandId, 3, 6, 99.0m, 99.0m, FootballCategoryId, SportCategoryId, SummerTagId, null, Size1Id, null, (int)ProductTypeEnum.Product),
				CreateProduct(2, BrioBrandId, 3, 6, 119.0m, 99.0m, DollCategoryId, null, WinterTagId, null, Size2Id, null, (int)ProductTypeEnum.Product),
				CreateProduct(3, LegoBrandId, 6, 18, 199.0m, 189.0m, FloorballCategoryId, SportCategoryId, SummerTagId, null, null, Size1Id, (int)ProductTypeEnum.Product),
				CreateProduct(4, BrioBrandId, 13, 20, 199.0m, 199.0m, SportCategoryId, null, SummerTagId, WinterTagId, Size1Id, Size2Id, (int)ProductTypeEnum.Product),
				CreateProduct(5, LegoBrandId, 13, 20, 299.0m, 299.0m, BookCategoryId, null, null, null, null, null, (int)ProductTypeEnum.Package)
			};
		}

		public static IEnumerable<FilterProduct> FiveProducts
		{
			get { return _fiveProducts; }
		}

		public static Collection<IPriceInterval> PriceIntervals
		{
			get { return _priceIntervals; }
		}

		public static Collection<IAgeInterval> AgeIntervals
		{
			get { return _ageIntervals; }
		}

		private static Collection<IAgeInterval> PopulateAgeIntervals()
		{
			return new Collection<IAgeInterval>
			{
				new AgeInterval {Id = 1, FromMonth = 0, ToMonth = 12},
				new AgeInterval {Id = 2, FromMonth = 12, ToMonth = 24},
				new AgeInterval {Id = 3, FromMonth = 24, ToMonth = 36},
				new AgeInterval {Id = 4, FromMonth = 36, ToMonth = 48},
				new AgeInterval {Id = 5, FromMonth = 48, ToMonth = 72},
				new AgeInterval {Id = 6, FromMonth = 72, ToMonth = 120},
				new AgeInterval {Id = 7, FromMonth = 120, ToMonth = 1000000}
			};
		}

		private static Collection<IPriceInterval> PopulatePriceIntervals()
		{
			return new Collection<IPriceInterval>
			{
				new PriceInterval {Id = 1, From = 0, To = 100},
				new PriceInterval {Id = 2, From = 101, To = 200},
				new PriceInterval {Id = 3, From = 201, To = 300},
				new PriceInterval {Id = 4, From = 301, To = 500},
				new PriceInterval {Id = 5, From = 501, To = 1000},
				new PriceInterval {Id = 6, From = 1001, To = 2000},
				new PriceInterval {Id = 7, From = 2001, To = 1000000}
			};
		}

		//private static DateTime GetRandomDate()
		//{
		//	DateTime from = DateTime.Now.AddDays(-100);
		//	TimeSpan range = new TimeSpan(DateTime.Now.Ticks - from.Ticks);
		//	return from + new TimeSpan((long)(range.Ticks * _random.NextDouble()));
		//}
	}
}