﻿using Litium.Scensum.Foundation;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Service
{
	[TestFixture]
	public class ContentPageServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ContentPageService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IContentPageService>();

			Assert.IsInstanceOf<IContentPageService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ContentPageService_Resolve_BaseResolved()
		{
			var service = IoC.Resolve<IContentPageService>();

			Assert.IsNotInstanceOf<LekmerContentPageService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ContentPageService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IContentPageService>();
			var service2 = IoC.Resolve<IContentPageService>();

			Assert.AreEqual(service1, service2);
		}
	}
}