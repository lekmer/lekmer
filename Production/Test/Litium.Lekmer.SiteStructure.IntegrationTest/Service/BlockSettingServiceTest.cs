﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.SiteStructure.IntegrationTest.Service
{
	[TestFixture]
	public class BlockSettingServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockSettingService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockSettingService>();

			Assert.IsInstanceOf<IBlockSettingService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockSettingService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IBlockSettingService>();
			var service2 = IoC.Resolve<IBlockSettingService>();

			Assert.AreEqual(service1, service2);
		}
	}
}