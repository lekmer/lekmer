using Litium.Lekmer.Campaign;
using Litium.Scensum.Foundation;
using Litium.Scensum.Order;

namespace Litium.Lekmer.Order.UnitTest
{
	public class OrderCreator
	{
		private readonly LekmerOrderFull _order;

		public OrderCreator()
		{
			_order = new LekmerOrderFull();
			_order.ChannelId = 1;
		}

		private LekmerOrderFull Order
		{
			get { return _order; }
		}

		public OrderCreator FreightCost(decimal freightCost)
		{
			Order.FreightCost = freightCost;
			return this;
		}

		public OrderCreator CampaignFreightCost(decimal campaignFreightCost)
		{
			if (Order.CampaignInfo == null)
			{
				Order.CampaignInfo = new LekmerOrderCampaignInfo();
			}

			Order.CampaignInfo.CampaignFreightCost = campaignFreightCost;
			return this;
		}

		public OrderCreator PaymentCost(decimal paymentCost)
		{
			Order.PaymentCost = paymentCost;
			return this;
		}

		public OrderCreator OrderItem(Price actualPrice, int quantity)
		{
			return OrderItem(new OrderItem{ActualPrice = actualPrice, Quantity = quantity});
		}

		public OrderCreator OrderItem(IOrderItem orderItem)
		{
			Order.AddOrderItem(orderItem);
			return this;
		}

		public LekmerOrderFull Create()
		{
			return Order;
		}

		public static OrderCreator New()
		{
			return new OrderCreator();
		}
	}
}