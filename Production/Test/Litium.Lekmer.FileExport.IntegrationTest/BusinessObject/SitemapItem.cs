using System;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.FileExport.IntegrationTest
{
	[TestFixture]
	public class SitemapItemTest
	{
		[Test]
		[Category("IoC")]
		public void SitemapItem_Resolve_Resolved()
		{
			var sitemapItem = IoC.Resolve<ISitemapItem>();

			Assert.IsInstanceOf<ISitemapItem>(sitemapItem);
		}
	}
}