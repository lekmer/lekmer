﻿using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerCountrySecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCountrySecureService_Resolve_Resolved()
		{
			var secureService = IoC.Resolve<ICountrySecureService>();

			Assert.IsInstanceOf<LekmerCountrySecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCountrySecureService_ResolveLekmer_Resolved()
		{
			var secureService = IoC.Resolve<ILekmerCountrySecureService>();

			Assert.IsInstanceOf<LekmerCountrySecureService>(secureService);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCountrySecureService_ResolveTwice_SameObjects()
		{
			var secureService1 = IoC.Resolve<ICountrySecureService>();
			var secureService2 = IoC.Resolve<ICountrySecureService>();

			Assert.AreEqual(secureService1, secureService2);
		}
	}
}