﻿using System.Collections.ObjectModel;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Core.IntegrationTest.SecureService
{
	[TestFixture]
	public class SubDomainSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void SubDomainSecureService_ResolveService_ServiceResolved()
		{
			var service = IoC.Resolve<ISubDomainSecureService>();

			Assert.IsInstanceOf<ISubDomainSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void SubDomainSecureService_ResolveServiceTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ISubDomainSecureService>();
			var service2 = IoC.Resolve<ISubDomainSecureService>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		[Category("IoC")]
		public void GetAllByContentType_ExecuteWithDifferentContentType_CorrectResults()
		{
			var service = IoC.Resolve<ISubDomainSecureService>();

			Collection<ISubDomain> subDomains = service.GetAllByContentType((int)ContentType.MediaArchiveBackOffice);

			Assert.IsTrue(subDomains.Count == 1, "BackOffice: count != 1");
		}
	}
}