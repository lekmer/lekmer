using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonExportRestrictionTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestriction_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonExportRestriction>();
			Assert.IsInstanceOf<ICdonExportRestriction>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestriction_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonExportRestriction>();
			var info2 = IoC.Resolve<ICdonExportRestriction>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}