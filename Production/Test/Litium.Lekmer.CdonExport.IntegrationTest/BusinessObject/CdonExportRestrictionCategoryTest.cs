using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonExportRestrictionCategoryTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionCategory_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonExportRestrictionCategory>();
			Assert.IsInstanceOf<ICdonExportRestrictionCategory>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionCategory_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonExportRestrictionCategory>();
			var info2 = IoC.Resolve<ICdonExportRestrictionCategory>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}