using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonExportRestrictionProductTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProduct_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonExportRestrictionProduct>();
			Assert.IsInstanceOf<ICdonExportRestrictionProduct>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionProduct_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonExportRestrictionProduct>();
			var info2 = IoC.Resolve<ICdonExportRestrictionProduct>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}