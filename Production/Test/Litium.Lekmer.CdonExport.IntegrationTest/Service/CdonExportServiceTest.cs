﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.Service
{
	[TestFixture]
	public class CdonExportServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICdonExportService>();

			Assert.IsInstanceOf<ICdonExportService>(service);
			Assert.IsInstanceOf<CdonExportService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICdonExportService>();
			var service2 = IoC.Resolve<ICdonExportService>();
			Assert.AreEqual(service1, service2);
		}
	}
}