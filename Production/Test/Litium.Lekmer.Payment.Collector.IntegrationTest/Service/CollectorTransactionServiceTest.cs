﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.Service
{
	[TestFixture]
	public class CollectorTransactionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorTransactionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICollectorTransactionService>();

			Assert.IsInstanceOf<ICollectorTransactionService>(service);
			Assert.IsInstanceOf<CollectorTransactionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorTransactionService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICollectorTransactionService>();
			var service2 = IoC.Resolve<ICollectorTransactionService>();

			Assert.AreEqual(service1, service2);
		}
	}
}