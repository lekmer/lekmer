﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.API
{
	[TestFixture]
	public class CollectorCalculationTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorCalculation_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICollectorCalculation>();

			Assert.IsInstanceOf<ICollectorCalculation>(service);
			Assert.IsInstanceOf<CollectorCalculation>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorCalculation_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICollectorCalculation>();
			var service2 = IoC.Resolve<ICollectorCalculation>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void CalculateMonthlyCostForInterestFree_InterestFreeInvoice_CorrectMontlyCost()
		{
			var collectorCalculation = IoC.Resolve<ICollectorCalculation>();

			ICollectorPaymentType paymentType = new CollectorPaymentType
			{
				Id = 1,
				ChannelId = 1,
				Code = "LKMR_IF_001",
				StartFee = 395,
				InvoiceFee = 29,
				MonthlyInterestRate = 0,
				NoOfMonths = 12,
				MinPurchaseAmount = 1000,
				LowestPayment = 0,
				PartPaymentType = CollectorPartPaymentType.InterestFree
			};

			decimal orderSum = 3218;

			decimal monthlyCostActual = collectorCalculation.CalculateMonthlyCostForInterestFree(orderSum, paymentType);

			Assert.AreEqual(331, monthlyCostActual);
		}

		[Test]
		public void CalculateMonthlyCostForAnnuity_AnnuityInvoice_CorrectMontlyCost()
		{
			var collectorCalculation = IoC.Resolve<ICollectorCalculation>();

			ICollectorPaymentType paymentType = new CollectorPaymentType
			{
				Id = 2,
				ChannelId = 1,
				Code = "LKMR_AN_001",
				StartFee = 395,
				InvoiceFee = 29,
				MonthlyInterestRate = 1.60m,
				NoOfMonths = 18,
				MinPurchaseAmount = 1000,
				LowestPayment = 0,
				PartPaymentType = CollectorPartPaymentType.Annuity
			};

			decimal orderSum = 3218;

			decimal monthlyCostActual = collectorCalculation.CalculateMonthlyCostForAnnuity(orderSum, paymentType);

			Assert.AreEqual(237, monthlyCostActual);
		}

		[Test]
		public void CalculateMonthlyCostForAccount_AccountInvoice_CorrectMontlyCost()
		{
			var collectorCalculation = IoC.Resolve<ICollectorCalculation>();

			ICollectorPaymentType paymentType = new CollectorPaymentType
			{
				Id = 3,
				ChannelId = 1,
				Code = "LKMR_AC_001",
				StartFee = 0,
				InvoiceFee = 29,
				MonthlyInterestRate = 1.60m,
				NoOfMonths = 12,
				MinPurchaseAmount = 0,
				LowestPayment = 50,
				PartPaymentType = CollectorPartPaymentType.Account
			};

			decimal orderSum = 457;

			decimal monthlyCostActual = collectorCalculation.CalculateMonthlyCostForAccount(orderSum, paymentType);

			Assert.AreEqual(75, monthlyCostActual);
		}

		[Test]
		public void CalculateMonthlyCostForAccount_ProductCost_CorrectMontlyCost()
		{
			var collectorCalculation = IoC.Resolve<ICollectorCalculation>();

			ICollectorPaymentType paymentType = new CollectorPaymentType
			{
				Id = 3,
				ChannelId = 1,
				Code = "LKMR_AC_001",
				StartFee = 0,
				InvoiceFee = 29,
				MonthlyInterestRate = 1.60m,
				NoOfMonths = 12,
				MinPurchaseAmount = 0,
				LowestPayment = 50,
				PartPaymentType = CollectorPartPaymentType.Account
			};

			decimal orderSum = 199;

			decimal monthlyCostActual = collectorCalculation.CalculateMonthlyCostForAccount(orderSum, paymentType);

			Assert.AreEqual(50, monthlyCostActual);
		}
	}
}