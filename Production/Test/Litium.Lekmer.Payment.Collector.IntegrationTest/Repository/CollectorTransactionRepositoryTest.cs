﻿using Litium.Lekmer.Payment.Collector.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Collector.IntegrationTest.Repository
{
	[TestFixture]
	public class CollectorTransactionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void CollectorTransactionRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<CollectorTransactionRepository>();

			Assert.IsInstanceOf<CollectorTransactionRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void CollectorTransactionRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<CollectorTransactionRepository>();
			var service2 = IoC.Resolve<CollectorTransactionRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}