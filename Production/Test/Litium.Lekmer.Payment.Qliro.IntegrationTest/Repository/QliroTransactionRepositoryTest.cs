﻿using Litium.Lekmer.Payment.Qliro.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Repository
{
	[TestFixture]
	public class QliroTransactionRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void QliroTransactionRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<QliroTransactionRepository>();

			Assert.IsInstanceOf<QliroTransactionRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroTransactionRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<QliroTransactionRepository>();
			var service2 = IoC.Resolve<QliroTransactionRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}