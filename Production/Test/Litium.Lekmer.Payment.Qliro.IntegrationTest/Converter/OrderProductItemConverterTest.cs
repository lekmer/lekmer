﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Converter
{
	[TestFixture]
	public class OrderProductItemConverterTest
	{
		[Test]
		[Category("IoC")]
		public void OrderProductItemConverter_Resolve_Resolved()
		{
			var service = IoC.Resolve<IOrderProductItemConverter>();

			Assert.IsInstanceOf<IOrderProductItemConverter>(service);
			Assert.IsInstanceOf<OrderProductItemConverter>(service);
		}

		[Test]
		[Category("IoC")]
		public void OrderProductItemConverter_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IOrderProductItemConverter>();
			var service2 = IoC.Resolve<IOrderProductItemConverter>();

			Assert.AreEqual(service1, service2);
		}
	}
}