﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.Converter
{
	[TestFixture]
	public class PackageOrderItemConverterTest
	{
		[Test]
		[Category("IoC")]
		public void PackageOrderItemConverter_Resolve_Resolved()
		{
			var service = IoC.Resolve<IPackageOrderItemConverter>();

			Assert.IsInstanceOf<IPackageOrderItemConverter>(service);
			Assert.IsInstanceOf<PackageOrderItemConverter>(service);
		}

		[Test]
		[Category("IoC")]
		public void PackageOrderItemConverter_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IPackageOrderItemConverter>();
			var service2 = IoC.Resolve<IPackageOrderItemConverter>();

			Assert.AreEqual(service1, service2);
		}
	}
}