﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Qliro.IntegrationTest.API
{
	[TestFixture]
	public class QliroCalculationTest
	{
		[Test]
		[Category("IoC")]
		public void QliroCalculation_Resolve_Resolved()
		{
			var service = IoC.Resolve<IQliroCalculation>();

			Assert.IsInstanceOf<IQliroCalculation>(service);
			Assert.IsInstanceOf<QliroCalculation>(service);
		}

		[Test]
		[Category("IoC")]
		public void QliroCalculation_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IQliroCalculation>();
			var service2 = IoC.Resolve<IQliroCalculation>();

			Assert.AreEqual(service1, service2);
		}

		[Test]
		public void CalculateMonthlyCostForAnnuity_AnnuityInvoice_CorrectMontlyCost()
		{
			var collectorCalculation = IoC.Resolve<IQliroCalculation>();

			IQliroPaymentType paymentType = new QliroPaymentType
			{
				Id = 1,
				ChannelId = 1,
				Code = "600",
				Description = "36 mån, 9,95% ränta",
				RegistrationFee = 395,
				SettlementFee = 29,
				InterestRate = 9.95m,
				InterestType = 1,
				InterestCalculation = QliroInterestCalculationType.Annuity,
				NoOfMonths = 36,
				MinPurchaseAmount = 2500
			};

			decimal orderSum = 3218;

			decimal monthlyCostActual = collectorCalculation.CalculateMonthlyCostForAnnuity(orderSum, paymentType);

			Assert.AreEqual(139, monthlyCostActual);
		}

		[Test]
		public void CalculateMonthlyCostForAccount_AccountInvoice_CorrectMontlyCost()
		{
			var collectorCalculation = IoC.Resolve<IQliroCalculation>();

			IQliroPaymentType paymentType = new QliroPaymentType
			{
				Id = 13,
				ChannelId = 1,
				Code = "ACCOUNT",
				Description = "Konto",
				RegistrationFee = 0,
				SettlementFee = 29,
				InterestRate = 19.90m,
				InterestType = 1,
				InterestCalculation = QliroInterestCalculationType.Flat,
				NoOfMonths = 24,
				MinPurchaseAmount = 0
			};

			decimal orderSum = 1457;

			decimal monthlyCostActual = collectorCalculation.CalculateMonthlyCostForAccount(orderSum, paymentType);

			Assert.AreEqual(63, monthlyCostActual);
		}

		[Test]
		public void CalculateMonthlyCostForAccount_ProductCost_CorrectMontlyCost()
		{
			var collectorCalculation = IoC.Resolve<IQliroCalculation>();

			IQliroPaymentType paymentType = new QliroPaymentType
			{
				Id = 13,
				ChannelId = 1,
				Code = "ACCOUNT",
				Description = "Konto",
				RegistrationFee = 0,
				SettlementFee = 29,
				InterestRate = 19.90m,
				InterestType = 1,
				InterestCalculation = QliroInterestCalculationType.Flat,
				NoOfMonths = 24,
				MinPurchaseAmount = 0
			};

			decimal orderSum = 199;

			decimal monthlyCostActual = collectorCalculation.CalculateMonthlyCostForAccount(orderSum, paymentType);

			Assert.AreEqual(50, monthlyCostActual);
		}
	}
}