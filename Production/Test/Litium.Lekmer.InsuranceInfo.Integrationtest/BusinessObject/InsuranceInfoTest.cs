using Litium.Lekmer.InsuranceInfo.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.InsuranceInfo.Integrationtest.BusinessObject
{
	[TestFixture]
	public class InsuranceInfoTest
	{
		[Test]
		[Category("IoC")]
		public void InsuranceInfo_Resolve_Resolved()
		{
			var info = IoC.Resolve<IInsuranceInfo>();
			Assert.IsInstanceOf<IInsuranceInfo>(info);
		}

		[Test]
		[Category("IoC")]
		public void InsuranceInfo_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<IInsuranceInfo>();
			var info2 = IoC.Resolve<IInsuranceInfo>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}