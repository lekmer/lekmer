﻿using Litium.Lekmer.InsuranceInfo.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.InsuranceInfo.Integrationtest.Service
{
	[TestFixture]
	public class InsuranceInfoExportServiceTest
	{
		[Test]
		[Category("IoC")]
		public void InsuranceInfoExportService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IInsuranceInfoExportService>();

			Assert.IsInstanceOf<IInsuranceInfoExportService>(service);
			Assert.IsInstanceOf<InsuranceInfoExportService>(service);
		}

		[Test]
		[Category("IoC")]
		public void InsuranceInfoExportService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IInsuranceInfoExportService>();
			var service2 = IoC.Resolve<IInsuranceInfoExportService>();
			Assert.AreEqual(service1, service2);
		}
	}
}