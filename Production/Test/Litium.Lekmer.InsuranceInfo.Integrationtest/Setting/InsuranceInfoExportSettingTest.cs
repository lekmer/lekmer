﻿using Litium.Lekmer.InsuranceInfo.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.InsuranceInfo.Integrationtest.Setting
{
	[TestFixture]
	public class InsuranceInfoExportSettingTest
	{
		[Test]
		[Category("IoC")]
		public void InsuranceInfoExportSetting_Resolve_Resolved()
		{
			var setting = IoC.Resolve<IInsuranceInfoExportSetting>();
			Assert.IsInstanceOf<IInsuranceInfoExportSetting>(setting);
		}

		[Test]
		[Category("IoC")]
		public void InsuranceInfoExportSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<IInsuranceInfoExportSetting>();
			var setting2 = IoC.Resolve<IInsuranceInfoExportSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}