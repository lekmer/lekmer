﻿using Litium.Lekmer.DataExport.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DataExport.IntegrationTest.Repository
{
	[TestFixture]
	public class ProductPriceInfoRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void ProductPriceInfoRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<ProductPriceInfoRepository>();

			Assert.IsInstanceOf<ProductPriceInfoRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void ProductPriceInfoRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<ProductPriceInfoRepository>();
			var repository2 = IoC.Resolve<ProductPriceInfoRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}