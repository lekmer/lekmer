﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.DataExport.IntegrationTest.Service
{
	[TestFixture]
	public class ProductPriceInfoExportServiceTest
	{
		[Test]
		[Category("IoC")]
		public void ProductPriceInfoExportService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IProductPriceInfoExportService>();

			Assert.IsInstanceOf<IProductPriceInfoExportService>(service);
		}

		[Test]
		[Category("IoC")]
		public void ProductPriceInfoExportService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<IProductPriceInfoExportService>();
			var service2 = IoC.Resolve<IProductPriceInfoExportService>();

			Assert.AreEqual(service1, service2);
		}
	}
}