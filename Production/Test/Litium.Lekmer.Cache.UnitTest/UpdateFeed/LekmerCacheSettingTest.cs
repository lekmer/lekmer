﻿using System;
using System.Collections.Generic;
using System.Linq;
using Litium.Framework.Cache.UpdateFeed;
using Litium.Lekmer.Cache.UpdateFeed;
using NUnit.Framework;

namespace Litium.Lekmer.Cache.UnitTest.UpdateFeed
{
	[TestFixture]
	public class LekmerCacheUpdateFeedTest
	{
		[Test]
		public void AddUpdateItem_AddSomeItem_ItemAdded()
		{
			var feed = new LekmerCacheUpdateFeed();

			var feedItem = new CacheUpdateFeedItem("test_manager_name", CacheUpdateType.Remove, "test_key_" + DateTime.UtcNow.Ticks, DateTime.UtcNow);

			feed.AddUpdateItem(feedItem);

			IList<ICacheUpdateFeedItem> feedItems = feed.GetNewUpdateItems(DateTime.Now);

			var expectedFeedItem = feedItems.FirstOrDefault(fi => fi.ManagerName == feedItem.ManagerName && fi.Key == feedItem.Key) as ILekmerCacheUpdateFeedItem;

			Assert.IsNotNull(expectedFeedItem);
			Assert.Greater(expectedFeedItem.Id, 0);
		}

		[Test]
		public void GetNewUpdateItems_GetNewItemsFewTimes_OnlyNewItemsReturnedEachTime()
		{
			var feed = new LekmerCacheUpdateFeed();

			// 1.

			var feedItem1 = new CacheUpdateFeedItem("test_manager_name", CacheUpdateType.Remove, "test_key_" + DateTime.UtcNow.Ticks, DateTime.UtcNow);

			feed.AddUpdateItem(feedItem1);

			IList<ICacheUpdateFeedItem> feedItems1 = feed.GetNewUpdateItems(DateTime.Now);

			// 2.

			var feedItem2 = new CacheUpdateFeedItem("test_manager_name", CacheUpdateType.Remove, "test_key_" + DateTime.UtcNow.Ticks, DateTime.UtcNow);

			feed.AddUpdateItem(feedItem2);

			IList<ICacheUpdateFeedItem> feedItems2 = feed.GetNewUpdateItems(DateTime.Now);

			var expectedFeedItem1 = feedItems2.FirstOrDefault(fi => fi.ManagerName == feedItem1.ManagerName && fi.Key == feedItem1.Key) as ILekmerCacheUpdateFeedItem;
			var expectedFeedItem2 = feedItems2.FirstOrDefault(fi => fi.ManagerName == feedItem2.ManagerName && fi.Key == feedItem2.Key) as ILekmerCacheUpdateFeedItem;

			Assert.IsNull(expectedFeedItem1);
			Assert.IsNotNull(expectedFeedItem2);
		}

		[Test]
		public void DeleteOldUpdateItems_DeleteOldItems_OldItemDeleted()
		{
			var feed = new LekmerCacheUpdateFeed();
			var feedSecure = new LekmerCacheUpdateFeedSecure();

			// 1.

			var feedItem = new CacheUpdateFeedItem("test_manager_name", CacheUpdateType.Remove, "test_key_" + DateTime.UtcNow.Ticks, DateTime.UtcNow);

			feed.AddUpdateItem(feedItem);

			feedSecure.DeleteOldUpdateItems(DateTime.Now.AddYears(1));

			IList<ICacheUpdateFeedItem> feedItems = feed.GetNewUpdateItems(DateTime.Now);

			var expectedFeedItem = feedItems.FirstOrDefault(fi => fi.ManagerName == feedItem.ManagerName && fi.Key == feedItem.Key) as ILekmerCacheUpdateFeedItem;

			Assert.IsNull(expectedFeedItem);
		}
	}
}
