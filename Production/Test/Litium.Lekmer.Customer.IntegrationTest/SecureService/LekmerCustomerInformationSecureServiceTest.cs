﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.SecureService
{
	[TestFixture]
	public class LekmerCustomerInformationSecureServiceTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformationSecureService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICustomerInformationSecureService>();

			Assert.IsInstanceOf<ICustomerInformationSecureService>(service);
			Assert.IsInstanceOf<CustomerInformationSecureService>(service);
			Assert.IsInstanceOf<LekmerCustomerInformationSecureService>(service);
		}

		[Test]
		[Category("IoC")]
		public void LekmerCustomerInformationSecureService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICustomerInformationSecureService>();
			var service2 = IoC.Resolve<ICustomerInformationSecureService>();

			Assert.AreEqual(service1, service2);
		}
	}
}