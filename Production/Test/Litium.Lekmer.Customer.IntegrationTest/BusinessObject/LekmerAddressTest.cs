﻿using Litium.Scensum.Customer;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class LekmerAddressTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerAddress_Resolve_Resolved()
		{
			var address = IoC.Resolve<IAddress>();

			Assert.IsInstanceOf<IAddress>(address);
			Assert.IsInstanceOf<ILekmerAddress>(address);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddress_ResolveTwice_DifferentObjects()
		{
			var address1 = IoC.Resolve<IAddress>();
			var address2 = IoC.Resolve<IAddress>();

			Assert.AreNotEqual(address1, address2);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddress_ResolveBillingAddress_Resolved()
		{
			var address = IoC.Resolve<IBillingAddress>();

			Assert.IsInstanceOf<IAddress>(address);
			Assert.IsInstanceOf<ILekmerAddress>(address);
			Assert.IsInstanceOf<IBillingAddress>(address);
			Assert.IsInstanceOf<ILekmerBillingAddress>(address);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddress_ResolveDeliveryAddress_Resolved()
		{
			var address = IoC.Resolve<IDeliveryAddress>();

			Assert.IsInstanceOf<IAddress>(address);
			Assert.IsInstanceOf<ILekmerAddress>(address);
			Assert.IsInstanceOf<IDeliveryAddress>(address);
			Assert.IsInstanceOf<ILekmerDeliveryAddress>(address);
		}
	}
}
