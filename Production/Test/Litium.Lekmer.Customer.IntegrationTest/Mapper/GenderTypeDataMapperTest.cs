﻿using System.Data;
using Litium.Lekmer.Customer.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Customer.IntegrationTest.Mapper
{
	[TestFixture]
	public class GenderTypeDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void GenderTypeDataMapper_Resolve_Resolved()
		{
			var dataMapper = DataMapperResolver.Resolve<IGenderType>(_dataReader);

			Assert.IsInstanceOf<GenderTypeDataMapper>(dataMapper);
		}

		[Test]
		[Category("IoC")]
		public void GenderTypeDataMapper_ResolveTwice_DifferentObjects()
		{
			var dataMapper1 = DataMapperResolver.Resolve<IGenderType>(_dataReader);
			var dataMapper2 = DataMapperResolver.Resolve<IGenderType>(_dataReader);

			Assert.AreNotEqual(dataMapper1, dataMapper2);
		}
	}
}