﻿using Litium.Lekmer.Customer.Repository;
using Litium.Scensum.Customer.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Customer.IntegrationTest.Repository
{
	[TestFixture]
	public class LekmerAddressRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void LekmerAddressRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<AddressRepository>();

			Assert.IsInstanceOf<AddressRepository>(repository);
			Assert.IsInstanceOf<LekmerAddressRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void LekmerAddressRepository_ResolveTwice_SameObjects()
		{
			var repository1 = IoC.Resolve<AddressRepository>();
			var repository2 = IoC.Resolve<AddressRepository>();

			Assert.AreEqual(repository1, repository2);
		}
	}
}