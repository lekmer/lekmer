﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class MaksuturvaCompensationErrorTest
	{
		[Test]
		[Category("IoC")]
		public void MaksuturvaCompensationError_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IMaksuturvaCompensationError>();

			Assert.IsInstanceOf<IMaksuturvaCompensationError>(instance);
		}

		[Test]
		[Category("IoC")]
		public void MaksuturvaCompensationError_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IMaksuturvaCompensationError>();
			var instance2 = IoC.Resolve<IMaksuturvaCompensationError>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}