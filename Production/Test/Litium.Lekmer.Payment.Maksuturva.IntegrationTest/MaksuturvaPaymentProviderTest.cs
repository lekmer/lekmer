﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Maksuturva.IntegrationTest
{
	[TestFixture]
	public class MaksuturvaPaymentProviderTest
	{
		[Test]
		[Category("IoC")]
		public void MaksuturvaPaymentProvider_Resolve_Resolved()
		{
			var service = IoC.Resolve<IMaksuturvaPaymentProvider>();

			Assert.IsInstanceOf<IMaksuturvaPaymentProvider>(service);
			Assert.IsInstanceOf<MaksuturvaPaymentProvider>(service);
		}
	}
}