﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockBrandTopListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListRepository_Resolve_Resolved()
		{
			var repository = IoC.Resolve<BlockBrandTopListRepository>();

			Assert.IsInstanceOf<BlockBrandTopListRepository>(repository);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListRepository_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<BlockBrandTopListRepository>();
			var instance2 = IoC.Resolve<BlockBrandTopListRepository>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}