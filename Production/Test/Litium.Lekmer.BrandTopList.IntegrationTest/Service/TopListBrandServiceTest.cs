﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Service
{
	[TestFixture]
	public class TopListBrandServiceTest
	{
		[Test]
		[Category("IoC")]
		public void TopListBrandService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ITopListBrandService>();

			Assert.IsInstanceOf<ITopListBrandService>(service);
			Assert.IsInstanceOf<TopListBrandService>(service);
		}

		[Test]
		[Category("IoC")]
		public void TopListBrandService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<ITopListBrandService>();
			var instance2 = IoC.Resolve<ITopListBrandService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}