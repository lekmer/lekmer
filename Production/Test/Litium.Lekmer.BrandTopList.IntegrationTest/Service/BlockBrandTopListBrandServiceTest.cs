﻿using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Service
{
	[TestFixture]
	public class BlockBrandTopListBrandServiceTest
	{
		[Test]
		[Category("IoC")]
		public void BlockBrandTopListBrandService_Resolve_Resolved()
		{
			var service = IoC.Resolve<IBlockBrandTopListBrandService>();

			Assert.IsInstanceOf<IBlockBrandTopListBrandService>(service);
			Assert.IsInstanceOf<BlockBrandTopListBrandService>(service);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListBrandService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<IBlockBrandTopListBrandService>();
			var instance2 = IoC.Resolve<IBlockBrandTopListBrandService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}