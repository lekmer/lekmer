﻿using System.Data;
using Litium.Lekmer.BrandTopList.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.BrandTopList.IntegrationTest.Mapper
{
	[TestFixture]
	public class BlockBrandTopListDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IBlockBrandTopList>(_dataReader);

			Assert.IsInstanceOf<BlockBrandTopListDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void BlockBrandTopListDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IBlockBrandTopList>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IBlockBrandTopList>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}