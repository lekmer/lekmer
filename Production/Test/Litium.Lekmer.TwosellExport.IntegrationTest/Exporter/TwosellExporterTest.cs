﻿using Litium.Lekmer.TwosellExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.TwosellExport.IntegrationTest.Exporter
{
	[TestFixture]
	public class OrderInfoExporterTest
	{
		[Test]
		[Category("IoC")]
		public void OrderInfoExporter_Resolve_Resolved()
		{
			var exporter = IoC.Resolve<IExporter>("OrderInfoExporter");

			Assert.IsInstanceOf<IExporter>(exporter);
			Assert.IsInstanceOf<OrderInfoExporter>(exporter);
		}

		[Test]
		[Category("IoC")]
		public void OrderInfoExporter_ResolveTwice_SameObjects()
		{
			var exporter1 = IoC.Resolve<IExporter>("OrderInfoExporter");
			var exporter2 = IoC.Resolve<IExporter>("OrderInfoExporter");

			Assert.AreEqual(exporter1, exporter2);
		}
	}
}