﻿using Litium.Lekmer.Contract;
using Litium.Lekmer.Product;
using Litium.Scensum.Campaign;
using Litium.Scensum.Core;
using Litium.Scensum.Foundation;
using Litium.Scensum.Product;
using NUnit.Framework;
using ProductTypeEnum = Litium.Lekmer.Product.Constant.ProductType;

namespace Litium.Lekmer.Campaign.UnitTest
{
	[TestFixture]
	public class FreeFreightFlagActionTest
	{
		private const int _swedishKronorCurrencyId = 1;

		[Test]
		public void Apply_EmptyTargetProductTypes_ActionApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Product
			};

			var action = new FreeFreightFlagAction
			{
				ProductPrices = new LekmerCurrencyValueDictionary { new CurrencyValue { Currency = TestHelper.GetSwedishKronorCurrency(), MonetaryValue = 100m } },
				TargetProductTypes = new IdDictionary()
			};

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(true, isApplied);
		}

		[Test]
		public void Apply_DifferentTargetProductTypes_ActionApplied()
		{
			var swedishContext = new UserContext { Channel = new Channel { Currency = new Currency { Id = _swedishKronorCurrencyId } } };

			var product = new LekmerProduct
			{
				Id = 1,
				CampaignInfo = new LekmerProductCampaignInfo { Price = new Price(200, 160) },
				Price = new PriceListItem { VatPercentage = 25 },
				ProductTypeId = (int)ProductTypeEnum.Product
			};

			var action = new FreeFreightFlagAction
			{
				ProductPrices = new LekmerCurrencyValueDictionary { new CurrencyValue { Currency = TestHelper.GetSwedishKronorCurrency(), MonetaryValue = 100m } },
				TargetProductTypes = new IdDictionary { (int)ProductTypeEnum.Package }
			};

			var isApplied = action.Apply(swedishContext, product);

			Assert.AreEqual(true, isApplied);
		}
	}
}