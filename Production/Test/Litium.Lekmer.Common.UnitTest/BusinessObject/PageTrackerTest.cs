﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Common.UnitTest.BusinessObject
{
	[TestFixture]
	public class PageTrackerTest
	{
		[Test]
		[Category("IoC")]
		public void PageTracker_Resolve_Resolved()
		{
			var pageTracker = IoC.Resolve<IPageTracker>();

			Assert.IsInstanceOf<IPageTracker>(pageTracker);
		}

		[Test]
		[Category("IoC")]
		public void PageTracker_ResolveTwice_DifferentObjects()
		{
			var pageTracker1 = IoC.Resolve<IPageTracker>();
			var pageTracker2 = IoC.Resolve<IPageTracker>();

			Assert.AreNotEqual(pageTracker1, pageTracker2);
		}
	}
}