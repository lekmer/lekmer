﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Common.UnitTest.Service
{
	[TestFixture]
	public class CookieServiceTest
	{
		[Test]
		[Category("IoC")]
		public void CookieService_Resolve_Resolved()
		{
			var service = IoC.Resolve<ICookieService>();

			Assert.IsInstanceOf<ICookieService>(service);
			Assert.IsInstanceOf<CookieService>(service);
		}

		[Test]
		[Category("IoC")]
		public void CookieService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<ICookieService>();
			var service2 = IoC.Resolve<ICookieService>();

			Assert.AreEqual(service1, service2);
		}
	}
}