﻿using System.Data;
using Litium.Lekmer.Payment.Klarna.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Mapper
{
	[TestFixture]
	public class KlarnaPendingOrderDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void KlarnaPendingOrderDataMapper_Resolve_Resolved()
		{
			var mapper = DataMapperResolver.Resolve<IKlarnaPendingOrder>(_dataReader);

			Assert.IsInstanceOf<KlarnaPendingOrderDataMapper>(mapper);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaPendingOrderDataMapper_ResolveTwice_DifferentObjects()
		{
			var instance1 = DataMapperResolver.Resolve<IKlarnaPendingOrder>(_dataReader);
			var instance2 = DataMapperResolver.Resolve<IKlarnaPendingOrder>(_dataReader);

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}