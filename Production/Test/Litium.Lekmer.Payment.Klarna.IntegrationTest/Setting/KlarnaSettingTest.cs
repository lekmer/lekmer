﻿using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Setting
{
	[TestFixture]
	public class KlarnaSettingTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaSetting>();

			Assert.IsInstanceOf<IKlarnaSetting>(service);
			Assert.IsInstanceOf<IKlarnaSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaSetting_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<IKlarnaSetting>();
			var service2 = IoC.Resolve<IKlarnaSetting>();

			Assert.AreNotEqual(service1, service2);
		}
	}
}