﻿using Litium.Lekmer.Payment.Klarna.Setting;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Setting
{
	[TestFixture]
	public class KlarnaCheckoutSettingTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaCheckoutSetting_Resolve_Resolved()
		{
			var service = IoC.Resolve<IKlarnaCheckoutSetting>();

			Assert.IsInstanceOf<IKlarnaCheckoutSetting>(service);
			Assert.IsInstanceOf<IKlarnaCheckoutSetting>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaCheckoutSetting_ResolveTwice_DifferentObjects()
		{
			var service1 = IoC.Resolve<IKlarnaCheckoutSetting>();
			var service2 = IoC.Resolve<IKlarnaCheckoutSetting>();

			Assert.AreNotEqual(service1, service2);
		}
	}
}