﻿using Litium.Lekmer.Payment.Klarna.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Payment.Klarna.IntegrationTest.Repository
{
	[TestFixture]
	public class KlarnaPendingOrderRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void KlarnaPendingOrderRepository_Resolve_Resolved()
		{
			var service = IoC.Resolve<KlarnaPendingOrderRepository>();

			Assert.IsInstanceOf<KlarnaPendingOrderRepository>(service);
		}

		[Test]
		[Category("IoC")]
		public void KlarnaPendingOrderRepository_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<KlarnaPendingOrderRepository>();
			var service2 = IoC.Resolve<KlarnaPendingOrderRepository>();

			Assert.AreEqual(service1, service2);
		}
	}
}