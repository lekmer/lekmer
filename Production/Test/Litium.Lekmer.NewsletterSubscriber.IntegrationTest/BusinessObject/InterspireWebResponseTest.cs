﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class InterspireWebResponseTest
	{
		[Test]
		[Category("IoC")]
		public void InterspireWebResponse_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireWebResponse>();

			Assert.IsInstanceOf<IInterspireWebResponse>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireWebResponse_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IInterspireWebResponse>();
			var instance2 = IoC.Resolve<IInterspireWebResponse>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}