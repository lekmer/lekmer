﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class InterspireResponseGetListsTest
	{
		[Test]
		[Category("IoC")]
		public void InterspireResponseGetLists_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireResponseGetLists>();

			Assert.IsInstanceOf<IInterspireResponseGetLists>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireResponseGetLists_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IInterspireResponseGetLists>();
			var instance2 = IoC.Resolve<IInterspireResponseGetLists>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}