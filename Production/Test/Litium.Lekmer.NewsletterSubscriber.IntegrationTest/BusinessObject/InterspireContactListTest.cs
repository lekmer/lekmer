﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class InterspireContactListTest
	{
		[Test]
		[Category("IoC")]
		public void InterspireContactList_Resolve_Resolved()
		{
			var instance = IoC.Resolve<IInterspireContactList>();

			Assert.IsInstanceOf<IInterspireContactList>(instance);
		}

		[Test]
		[Category("IoC")]
		public void InterspireContactList_ResolveTwice_DifferentObjects()
		{
			var instance1 = IoC.Resolve<IInterspireContactList>();
			var instance2 = IoC.Resolve<IInterspireContactList>();

			Assert.AreNotEqual(instance1, instance2);
		}
	}
}