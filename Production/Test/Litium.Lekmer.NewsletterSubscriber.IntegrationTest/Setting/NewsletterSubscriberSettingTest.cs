﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.XmlFile
{
	[TestFixture]
	public class NewsletterSubscriberSettingTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterSubscriberSetting_Resolve_Resolved()
		{
			var instance = IoC.Resolve<INewsletterSubscriberSetting>();

			Assert.IsInstanceOf<INewsletterSubscriberSetting>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterSubscriberSetting_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<INewsletterSubscriberSetting>();
			var instance2 = IoC.Resolve<INewsletterSubscriberSetting>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}