﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Service
{
	[TestFixture]
	public class NewsletterSubscriberServiceTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterSubscriberService_Resolve_Resolved()
		{
			var instance = IoC.Resolve<INewsletterSubscriberService>();

			Assert.IsInstanceOf<INewsletterSubscriberService>(instance);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterSubscriberService_ResolveTwice_SameObjects()
		{
			var instance1 = IoC.Resolve<INewsletterSubscriberService>();
			var instance2 = IoC.Resolve<INewsletterSubscriberService>();

			Assert.AreEqual(instance1, instance2);
		}
	}
}