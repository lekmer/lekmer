﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.NewsletterSubscriber.IntegrationTest.Service
{
	[TestFixture]
	public class NewsletterUnsubscriberOptionServiceTest
	{
		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberOptionService_Resolve_Resolved()
		{
			var service = IoC.Resolve<INewsletterUnsubscriberOptionService>();

			Assert.IsInstanceOf<INewsletterUnsubscriberOptionService>(service);
			Assert.IsInstanceOf<NewsletterUnsubscriberOptionService>(service);
		}

		[Test]
		[Category("IoC")]
		public void NewsletterUnsubscriberOptionService_ResolveTwice_SameObjects()
		{
			var service1 = IoC.Resolve<INewsletterUnsubscriberOptionService>();
			var service2 = IoC.Resolve<INewsletterUnsubscriberOptionService>();

			Assert.AreEqual(service1, service2);
		}
	}
}