using Litium.Lekmer.CdonExport.Contract;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.CdonExport.IntegrationTest.BusinessObject
{
	[TestFixture]
	public class CdonExportRestrictionBrandTest
	{
		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrand_Resolve_Resolved()
		{
			var info = IoC.Resolve<ICdonExportRestrictionBrand>();
			Assert.IsInstanceOf<ICdonExportRestrictionBrand>(info);
		}

		[Test]
		[Category("IoC")]
		public void CdonExportRestrictionBrand_ResolveTwice_DifferentObjects()
		{
			var info1 = IoC.Resolve<ICdonExportRestrictionBrand>();
			var info2 = IoC.Resolve<ICdonExportRestrictionBrand>();

			Assert.AreNotEqual(info1, info2);
		}
	}
}