﻿using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.GoogleFeed.IntegrationTest.Setting
{
	[TestFixture]
	public class GoogleFeedSettingTest
	{
		[Test]
		[Category("IoC")]
		public void GoogleFeedSetting_Resolve_Resolved()
		{
			var setting = IoC.Resolve<IGoogleFeedSetting>();
			Assert.IsInstanceOf<IGoogleFeedSetting>(setting);
		}

		[Test]
		[Category("IoC")]
		public void GoogleFeedSetting_ResolveTwice_SameObjects()
		{
			var setting1 = IoC.Resolve<IGoogleFeedSetting>();
			var setting2 = IoC.Resolve<IGoogleFeedSetting>();

			Assert.AreEqual(setting1, setting2);
		}
	}
}