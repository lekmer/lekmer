﻿using System.Data;
using Litium.Framework.DataMapper;
using Litium.Lekmer.Oculus.Mapper;
using Litium.Scensum.Foundation;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Oculus.IntegrationTest.Mapper
{
	[TestFixture]
	public class SimilarProductItemDataMapperTest
	{
		private static MockRepository _mocker;
		private static IDataReader _dataReader;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_dataReader = _mocker.Stub<IDataReader>();
		}

		[Test]
		[Category("IoC")]
		public void SimilarProductItemDataMapper_Resolve_Resolved()
		{
			DataMapperBase<ISimilarProductItem> similarProductItemDataMapper = DataMapperResolver.Resolve<ISimilarProductItem>(_dataReader);

			Assert.IsInstanceOf<SimilarProductItemDataMapper>(similarProductItemDataMapper);
		}

		[Test]
		[Category("IoC")]
		public void SimilarProductItemDataMapper_ResolveTwice_DifferentObjects()
		{
			DataMapperBase<ISimilarProductItem> similarProductItemDataMapper1 = DataMapperResolver.Resolve<ISimilarProductItem>(_dataReader);
			DataMapperBase<ISimilarProductItem> similarProductItemDataMapper2 = DataMapperResolver.Resolve<ISimilarProductItem>(_dataReader);

			Assert.AreNotEqual(similarProductItemDataMapper1, similarProductItemDataMapper2);
		}
	}
}