﻿using Litium.Lekmer.Oculus.Repository;
using Litium.Scensum.Foundation;
using NUnit.Framework;

namespace Litium.Lekmer.Oculus.IntegrationTest.Repository
{
	[TestFixture]
	public class BlockProductSimilarListRepositoryTest
	{
		[Test]
		[Category("IoC")]
		public void BlockProductSimilarListRepository_Resolve_Resolved()
		{
			BlockProductSimilarListRepository blockProductSimilarListRepository = IoC.Resolve<BlockProductSimilarListRepository>();

			Assert.IsInstanceOf<BlockProductSimilarListRepository>(blockProductSimilarListRepository);
		}

		[Test]
		[Category("IoC")]
		public void BlockProductSimilarListRepository_ResolveTwice_SameObjects()
		{
			BlockProductSimilarListRepository blockProductSimilarListRepository1 = IoC.Resolve<BlockProductSimilarListRepository>();
			BlockProductSimilarListRepository blockProductSimilarListRepository2 = IoC.Resolve<BlockProductSimilarListRepository>();

			Assert.AreEqual(blockProductSimilarListRepository1, blockProductSimilarListRepository2);
		}
	}
}