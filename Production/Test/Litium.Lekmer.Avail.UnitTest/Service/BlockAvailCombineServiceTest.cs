﻿using Litium.Lekmer.Avail.Cache;
using Litium.Lekmer.Avail.Repository;
using Litium.Lekmer.SiteStructure;
using Litium.Scensum.Core;
using Litium.Scensum.SiteStructure;
using NUnit.Framework;
using Rhino.Mocks;

namespace Litium.Lekmer.Avail.UnitTest.Service
{
	[TestFixture]
	public class BlockAvailCombineServiceTest
	{
		private static readonly int TEST_BLOCK_ID_1 = 1000732;
		private static readonly int TEST_BLOCK_ID_2 = 1000733;
		private static readonly int TEST_BLOCK_ID_3 = 1000734;
		private static readonly int TEST_BLOCK_ID_4 = 1000735;

		private static MockRepository _mocker;
		private static IUserContext _userContext;
		private static IAccessValidator _accessValidator;
		private static ISystemUserFull _systemUserFull;
		private static BlockAvailCombineRepository _blockAvailCombineRepository;
		private static IBlockSecureService _blockSecureService;
		private static BlockAvailCombineService _blockAvailCombineService;
		private static BlockAvailCombineSecureService _blockAvailCombineSecureService;
		private static IBlockSettingSecureService _blockSettingSecureService;

		[SetUp]
		public void SetUp()
		{
			_mocker = new MockRepository();
			_userContext = _mocker.Stub<IUserContext>();
			_blockAvailCombineRepository = _mocker.DynamicMock<BlockAvailCombineRepository>();
			_blockAvailCombineService = new BlockAvailCombineService(_blockAvailCombineRepository);

			BlockAvailCombineCache.Instance.Flush();
		}

		[Test]
		public void GetById_SameId_CacheUsed()
		{
			Expect.Call(_blockAvailCombineRepository.GetById(TEST_BLOCK_ID_1)).Return(new BlockAvailCombine { Id = TEST_BLOCK_ID_1 });

			_mocker.ReplayAll();

			Assert.AreEqual(TEST_BLOCK_ID_1, _blockAvailCombineService.GetById(_userContext, TEST_BLOCK_ID_1).Id);
			Assert.AreEqual(TEST_BLOCK_ID_1, _blockAvailCombineService.GetById(_userContext, TEST_BLOCK_ID_1).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_TwoId_CacheIsNotUsed()
		{
			Expect.Call(_blockAvailCombineRepository.GetById(TEST_BLOCK_ID_2)).Return(new BlockAvailCombine { Id = TEST_BLOCK_ID_2 });
			Expect.Call(_blockAvailCombineRepository.GetById(TEST_BLOCK_ID_3)).Return(new BlockAvailCombine { Id = TEST_BLOCK_ID_3 });

			_mocker.ReplayAll();

			Assert.AreEqual(TEST_BLOCK_ID_2, _blockAvailCombineService.GetById(_userContext, TEST_BLOCK_ID_2).Id);
			Assert.AreEqual(TEST_BLOCK_ID_3, _blockAvailCombineService.GetById(_userContext, TEST_BLOCK_ID_3).Id);

			_mocker.VerifyAll();
		}

		[Test]
		public void GetById_BlockAvailUpdated_CacheRemoved()
		{
			_accessValidator = _mocker.Stub<IAccessValidator>();
			_systemUserFull = _mocker.Stub<ISystemUserFull>();
			_blockSecureService = _mocker.Stub<IBlockSecureService>();
			_blockSettingSecureService = _mocker.Stub<IBlockSettingSecureService>();
			_blockAvailCombineSecureService = new BlockAvailCombineSecureService(_accessValidator, _blockAvailCombineRepository, null, _blockSecureService, _blockSettingSecureService);

			IBlockAvailCombine blockAvailCombine = new BlockAvailCombine { Id = TEST_BLOCK_ID_4, Setting = new BlockSetting()};

			Expect.Call(_blockAvailCombineRepository.GetById(TEST_BLOCK_ID_4)).Return(blockAvailCombine).Repeat.Twice();
			Expect.Call(_blockSecureService.Save(_systemUserFull, blockAvailCombine)).Return(TEST_BLOCK_ID_4).IgnoreArguments().Repeat.Once();

			_mocker.ReplayAll();

			Assert.AreEqual(TEST_BLOCK_ID_4, _blockAvailCombineService.GetById(_userContext, TEST_BLOCK_ID_4).Id);
			Assert.AreEqual(TEST_BLOCK_ID_4, _blockAvailCombineSecureService.Save(_systemUserFull, blockAvailCombine));
			Assert.AreEqual(TEST_BLOCK_ID_4, _blockAvailCombineService.GetById(_userContext, TEST_BLOCK_ID_4).Id);

			_mocker.VerifyAll();
		}
	}
}