﻿using NUnit.Framework;

namespace Litium.Lekmer.Core.UnitTest.Util
{
	[TestFixture]
	public class PriceCalcTest
	{
		private PriceInfo[] _priceInfoArray = new[]
			{
				new PriceInfo(125.00m, 100.00m, 25.00m, 0.25m, 25m),
				new PriceInfo(138.89m, 111.11m, 27.78m, 0.25m, 25m)
			};

		private PriceInfo _priceInfoZero = new PriceInfo(0.00m, 0.00m, 0.00m, 0.25m, 25m);

		[Test]
		public void VatPercentage_SpecifyArguments_CorrectResult()
		{
			foreach (PriceInfo testPrice in _priceInfoArray)
			{
				decimal vatPercentageActual = PriceCalc.VatPercentage1(testPrice.PriceIncludingVat, testPrice.VatAmount);
				Assert.AreEqual(testPrice.VatPercentage, vatPercentageActual);
			}

			foreach (PriceInfo testPrice in _priceInfoArray)
			{
				decimal vatPercentageActual = PriceCalc.VatPercentage2(testPrice.PriceIncludingVat, testPrice.PriceExcludingVat);
				Assert.AreEqual(testPrice.VatPercentage, vatPercentageActual);
			}

			foreach (PriceInfo testPrice in _priceInfoArray)
			{
				decimal vatPercentageActual = PriceCalc.VatPercentage100(testPrice.PriceIncludingVat, testPrice.PriceExcludingVat);
				Assert.AreEqual(testPrice.VatPercentage100, vatPercentageActual);
			}
		}

		[Test]
		public void VatPercentage1_SpecifyZeroArguments_CorrectResult()
		{
			decimal vatPercentageActual = PriceCalc.VatPercentage1(_priceInfoZero.PriceIncludingVat, _priceInfoZero.VatAmount);
			Assert.AreEqual(_priceInfoZero.VatPercentage, vatPercentageActual);
		}

		[Test]
		public void VatPercentage2_SpecifyZeroArguments_CorrectResult()
		{
			decimal vatPercentageActual = PriceCalc.VatPercentage2(_priceInfoZero.PriceIncludingVat, _priceInfoZero.PriceExcludingVat);
			Assert.AreEqual(_priceInfoZero.VatPercentage, vatPercentageActual);
		}

		[Test]
		public void VatPercentage100_SpecifyZeroArguments_CorrectResult()
		{
			decimal vatPercentageActual = PriceCalc.VatPercentage100(_priceInfoZero.PriceIncludingVat, _priceInfoZero.PriceExcludingVat);
			Assert.AreEqual(_priceInfoZero.VatPercentage100, vatPercentageActual);
		}

		[Test]
		public void PriceExcludingVat_SpecifyArguments_CorrectResult()
		{
			foreach (PriceInfo testPrice in _priceInfoArray)
			{
				decimal priceExcludingVatActual = PriceCalc.PriceExcludingVat1(testPrice.PriceIncludingVat, testPrice.VatPercentage);
				Assert.AreEqual(testPrice.PriceExcludingVat, priceExcludingVatActual);
			}

			foreach (PriceInfo testPrice in _priceInfoArray)
			{
				decimal priceExcludingVatActual = PriceCalc.PriceExcludingVat2(testPrice.PriceIncludingVat, testPrice.VatAmount);
				Assert.AreEqual(testPrice.PriceExcludingVat, priceExcludingVatActual);
			}
		}

		private class PriceInfo
		{
			public PriceInfo(decimal priceIncludingVat, decimal priceExcludingVat, decimal vatAmount, decimal vatPercentage, decimal vatPercentage100)
			{
				PriceIncludingVat = priceIncludingVat;
				PriceExcludingVat = priceExcludingVat;
				VatAmount = vatAmount;
				VatPercentage = vatPercentage;
				VatPercentage100 = vatPercentage100;
			}

			public decimal PriceIncludingVat { get; private set; }
			public decimal PriceExcludingVat { get; private set; }
			public decimal VatAmount { get; private set; }
			public decimal VatPercentage { get; private set; }
			public decimal VatPercentage100 { get; private set; }
		}
	}
}